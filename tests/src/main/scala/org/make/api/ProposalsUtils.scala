/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api

import scala.io.Source
import com.github.tototoshi.csv.CSVReader
import org.make.core.proposal.{ProposalId, ProposalKeywordKey, QualificationKey, VoteKey}

import java.time.ZonedDateTime
import org.make.core.proposal.VoteKey.{Agree, Disagree, Neutral}
import org.make.core.user.UserId
import org.make.core.proposal._
import org.make.core.proposal.QualificationKey._
import org.make.core.proposal.indexed.{IndexedProposal, IndexedProposalKeyword}

object ProposalsUtils {

  private def parseVote(values: Map[String, String], key: VoteKey): Vote =
    Vote(
      key = key,
      count = values(s"vote_verified_${key.value}").toInt,
      countVerified = values(s"vote_verified_${key.value}").toInt,
      countSequence = values(s"vote_sequence_${key.value}").toInt,
      countSegment = 0,
      Seq.empty
    )

  private def parseQualification(
    values: Map[String, String],
    voteKey: VoteKey,
    qualificationKey: QualificationKey,
    label: String
  ): Qualification =
    Qualification(
      key = qualificationKey,
      count = values(s"vote_verified_${voteKey.value}_$label").toInt,
      countVerified = values(s"vote_verified_${voteKey.value}_$label").toInt,
      countSequence = values(s"vote_sequence_${voteKey.value}_$label").toInt,
      countSegment = 0
    )

  private def parseVotes(values: Map[String, String]): VotingOptions =
    VotingOptions(
      agreeVote = AgreeWrapper(
        vote = parseVote(values, Agree),
        likeIt = parseQualification(values, Agree, LikeIt, "like"),
        platitudeAgree = parseQualification(values, Agree, PlatitudeAgree, "platitudeagree"),
        doable = parseQualification(values, Agree, Doable, "doable")
      ),
      neutralVote = NeutralWrapper(
        vote = parseVote(values, Neutral),
        noOpinion = parseQualification(values, Neutral, NoOpinion, "noopinion"),
        doNotUnderstand = parseQualification(values, Neutral, DoNotUnderstand, "donotunderstand"),
        doNotCare = parseQualification(values, Neutral, DoNotCare, "donotcare")
      ),
      disagreeVote = DisagreeWrapper(
        vote = parseVote(values, Disagree),
        impossible = parseQualification(values, Disagree, Impossible, "impossible"),
        noWay = parseQualification(values, Disagree, NoWay, "noway"),
        platitudeDisagree = parseQualification(values, Disagree, PlatitudeDisagree, "platitudedisagree")
      )
    )

  private def parseProposal(values: Map[String, String]): IndexedProposal = {
    TestUtils.indexedProposal(
      id = ProposalId(values("id")),
      userId = UserId(values("id")),
      votingOptions = this.parseVotes(values),
      createdAt = ZonedDateTime.parse(values("date_created")),
      keywords = values("keywords")
        .split('|')
        .map(k => IndexedProposalKeyword(ProposalKeywordKey(k), k))
        .toSeq
    )
  }

  def getProposalsFromCsv(csvName: String): Seq[IndexedProposal] = {
    CSVReader
      .open(Source.fromResource(csvName))
      .iteratorWithHeaders
      .map(parseProposal)
      .toSeq
  }

  private val maxVotes = 2000

  def getNewAndTestedProposals(csvName: String): (Seq[IndexedProposal], Seq[IndexedProposal]) = {
    getProposalsFromCsv(csvName)
      .filter(_.votesSequenceCount < maxVotes)
      .partition(_.votesSequenceCount <= 10)
  }
}
