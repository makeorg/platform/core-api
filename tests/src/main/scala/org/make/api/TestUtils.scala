/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api

import cats.data.NonEmptyList
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._
import eu.timepit.refined.collection._
import eu.timepit.refined.refineV
import eu.timepit.refined.types.numeric.NonNegInt
import eu.timepit.refined.string.Url
import org.make.api.proposal.ProposalScorer
import org.make.api.proposal.ProposalScorer.VotesCounter
import org.make.core.sequence.{
  ExplorationSequenceConfiguration,
  ExplorationSequenceConfigurationId,
  SequenceConfiguration,
  SpecificSequenceConfiguration,
  SpecificSequenceConfigurationId
}
import org.make.core.auth.{Client, ClientId}
import org.make.core.demographics.DemographicsCard.Layout
import org.make.core.demographics.{DemographicsCard, DemographicsCardId}
import org.make.core.idea.{Idea, IdeaId, IdeaStatus}
import org.make.core.keyword.Keyword
import org.make.core.operation._
import org.make.core.operation.indexed.IndexedOperationOfQuestion
import org.make.core.partner.{Partner, PartnerId, PartnerKind}
import org.make.core.post.indexed.IndexedPost
import org.make.core.post.{Post, PostId}
import org.make.core.profile.Profile
import org.make.core.proposal._
import org.make.core.technical.Multilingual
import org.make.core.proposal.indexed._
import org.make.core.proposal.ProposalStatus.Accepted
import org.make.core.proposal.QualificationKey._
import org.make.core.proposal.VoteKey.{Agree, Disagree, Neutral}
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import org.make.core.tag.{Tag, TagDisplay, TagId, TagType, TagTypeId}
import org.make.core.user.Role.RoleCitizen
import org.make.core.user._
import org.make.core.widget.{Source, SourceId}
import org.make.core.{DateHelper, RequestContext, SlugHelper}

import java.net.URI
import java.time.{ZoneId, ZonedDateTime}
import org.make.core.demographics.LabelsValue

trait TestUtils {

  private val defaultVotes: VotingOptions = VotingOptions(
    AgreeWrapper(
      vote = Vote(
        key = Agree,
        count = 0,
        countVerified = 0,
        countSegment = 0,
        countSequence = 0,
        qualifications = Seq.empty
      ),
      likeIt = Qualification(key = LikeIt, count = 0, countVerified = 0, countSequence = 0, countSegment = 0),
      doable = Qualification(key = Doable, count = 0, countVerified = 0, countSequence = 0, countSegment = 0),
      platitudeAgree =
        Qualification(key = PlatitudeAgree, count = 0, countVerified = 0, countSequence = 0, countSegment = 0)
    ),
    NeutralWrapper(
      vote = Vote(
        key = Neutral,
        count = 0,
        countVerified = 0,
        countSegment = 0,
        countSequence = 0,
        qualifications = Seq.empty
      ),
      doNotUnderstand =
        Qualification(key = DoNotUnderstand, count = 0, countVerified = 0, countSequence = 0, countSegment = 0),
      noOpinion = Qualification(key = NoOpinion, count = 0, countVerified = 0, countSequence = 0, countSegment = 0),
      doNotCare = Qualification(key = DoNotCare, count = 0, countVerified = 0, countSequence = 0, countSegment = 0)
    ),
    DisagreeWrapper(
      vote = Vote(
        key = Disagree,
        count = 0,
        countVerified = 0,
        countSegment = 0,
        countSequence = 0,
        qualifications = Seq.empty
      ),
      noWay = Qualification(key = NoWay, count = 0, countVerified = 0, countSequence = 0, countSegment = 0),
      impossible = Qualification(key = Impossible, count = 0, countVerified = 0, countSequence = 0, countSegment = 0),
      platitudeDisagree =
        Qualification(key = PlatitudeDisagree, count = 0, countVerified = 0, countSequence = 0, countSegment = 0)
    )
  )

  def proposal(
    id: ProposalId,
    votes: VotingOptions = defaultVotes,
    author: UserId = UserId("author"),
    tags: Seq[TagId] = Seq.empty,
    organisations: Seq[OrganisationInfo] = Seq.empty,
    questionId: QuestionId = QuestionId("question"),
    operationId: Option[OperationId] = Some(OperationId("operation")),
    requestContext: RequestContext = RequestContext.empty,
    content: String = "Il faut tester l'indexation des propositions",
    status: ProposalStatus = Accepted,
    refusalReason: Option[String] = None,
    idea: Option[IdeaId] = None,
    events: List[ProposalAction] = Nil,
    proposalType: ProposalType = ProposalType.ProposalTypeSubmitted,
    createdAt: Option[ZonedDateTime] = Some(ZonedDateTime.parse("2019-10-10T10:10:10.000Z")),
    updatedAt: Option[ZonedDateTime] = Some(ZonedDateTime.parse("2019-10-10T15:10:10.000Z")),
    validatedAt: Option[ZonedDateTime] = Some(ZonedDateTime.parse("2019-10-10T15:10:10.000Z")),
    postponedAt: Option[ZonedDateTime] = Some(ZonedDateTime.parse("2019-10-10T15:10:10.000Z")),
    keywords: Seq[ProposalKeyword] = Nil
  ): Proposal = {
    Proposal(
      proposalId = id,
      votingOptions = Some(votes),
      content = content,
      submittedAsLanguage = Some(Language("fr")),
      contentTranslations = None,
      slug = SlugHelper(content),
      author = author,
      status = status,
      refusalReason = refusalReason,
      tags = tags,
      organisationIds = organisations.map(_.organisationId),
      questionId = Some(questionId),
      creationContext = requestContext,
      idea = idea,
      operation = operationId,
      proposalType = proposalType,
      createdAt = createdAt,
      updatedAt = updatedAt,
      validatedAt = validatedAt.filter(_ => status == ProposalStatus.Accepted || status == ProposalStatus.Refused),
      postponedAt = postponedAt.filter(_ => status == ProposalStatus.Postponed),
      events = events,
      keywords = keywords
    )
  }

  private val defaultAuthor: IndexedAuthor = IndexedAuthor(
    userId = UserId("12345"),
    firstName = Some("firstname"),
    displayName = Some("firstname"),
    organisationName = None,
    organisationSlug = None,
    postalCode = Some("12345"),
    age = Some(25),
    avatarUrl = Some("http://some-url"),
    userType = UserType.UserTypeUser,
    profession = Some("Tarot Reader")
  )

  def indexedProposal(
    id: ProposalId,
    votingOptions: VotingOptions = VotingOptions.empty,
    userId: UserId = UserId("author"),
    author: IndexedAuthor = defaultAuthor,
    questionId: QuestionId = QuestionId("question-id"),
    operationId: Option[OperationId] = Some(OperationId("operation-id")),
    startDate: ZonedDateTime = ZonedDateTime.parse("1968-07-03T00:00:00.000Z"),
    endDate: ZonedDateTime = ZonedDateTime.parse("2068-07-03T00:00:00.000Z"),
    requestContext: Option[RequestContext] = None,
    content: String = "Il faut tester l'indexation des propositions",
    countries: NonEmptyList[Country] = NonEmptyList.of(Country("FR")),
    defaultLanguage: Language = Language("fr"),
    languages: NonEmptyList[Language] = NonEmptyList.of(Language("fr")),
    status: ProposalStatus = Accepted,
    refusalReason: Option[String] = None,
    ideaId: Option[IdeaId] = None,
    selectedStakeTag: Option[IndexedTag] = None,
    initialProposal: Boolean = false,
    proposalType: ProposalType = ProposalType.ProposalTypeSubmitted,
    createdAt: ZonedDateTime = ZonedDateTime.parse("2019-10-10T10:10:10.000Z"),
    updatedAt: Option[ZonedDateTime] = Some(ZonedDateTime.parse("2019-10-10T15:10:10.000Z")),
    validatedAt: Option[ZonedDateTime] = Some(ZonedDateTime.parse("2019-10-10T15:10:10.000Z")),
    postponedAt: Option[ZonedDateTime] = Some(ZonedDateTime.parse("2019-10-10T15:10:10.000Z")),
    toEnrich: Boolean = false,
    keywords: Seq[IndexedProposalKeyword] = Nil,
    tags: Seq[IndexedTag] = Nil,
    sequencePool: SequencePool = SequencePool.New,
    sequenceSegmentPool: SequencePool = SequencePool.New,
    segment: Option[String] = None
  ): IndexedProposal = {
    val regularScore = ProposalScorer(votingOptions, VotesCounter.SequenceVotesCounter, 0.5)
    val segmentScore = ProposalScorer(votingOptions, VotesCounter.SegmentVotesCounter, 0.5)
    val authorWithId = author.copy(userId = userId)

    IndexedProposal(
      id = id,
      submittedAsLanguage = Some(defaultLanguage),
      content = Multilingual((defaultLanguage, content)),
      contentGeneral = content,
      contentKeyword = content,
      slug = SlugHelper(content),
      status = status,
      createdAt = createdAt,
      updatedAt = updatedAt,
      validatedAt = validatedAt,
      postponedAt = postponedAt,
      votes = votingOptions,
      votesCount = votingOptions.counts.totalVotes,
      votesVerifiedCount = votingOptions.verifiedCounts.totalVotes,
      votesSequenceCount = votingOptions.sequenceCounts.totalVotes,
      votesSegmentCount = votingOptions.segmentCounts.totalVotes,
      toEnrich = toEnrich,
      scores = IndexedScores(regularScore),
      segmentScores = IndexedScores(segmentScore),
      agreementRate = BaseVote.rate(votingOptions, VoteKey.Agree),
      context = requestContext.map(IndexedContext.apply(_)),
      isAnonymous = false,
      author = authorWithId,
      organisations = Seq.empty,
      tags = tags,
      selectedStakeTag = selectedStakeTag,
      ideaId = ideaId,
      operationId = operationId,
      question = Some(
        IndexedProposalQuestion(
          questionId = questionId,
          slug = questionId.value,
          titles = Multilingual((defaultLanguage, questionId.value)),
          questions = Multilingual((defaultLanguage, refineV[NonEmpty].unsafeFrom(questionId.value))),
          countries = countries,
          languages = languages,
          startDate = startDate,
          endDate = endDate,
          isOpen = true
        )
      ),
      sequencePool = sequencePool,
      sequenceSegmentPool = sequenceSegmentPool,
      initialProposal = initialProposal,
      proposalType = proposalType,
      refusalReason = refusalReason,
      operationKind = None,
      segment = segment,
      keywords = keywords
    )
  }

  def user(
    id: UserId,
    email: String = "test@make.org",
    firstName: Option[String] = Some("Joe"),
    lastName: Option[String] = Some("Chip"),
    lastIp: Option[String] = None,
    hashedPassword: Option[String] = None,
    enabled: Boolean = true,
    emailVerified: Boolean = true,
    lastConnection: Option[ZonedDateTime] = Some(ZonedDateTime.parse("1992-08-23T02:02:02.020Z")),
    verificationToken: Option[String] = None,
    verificationTokenExpiresAt: Option[ZonedDateTime] = None,
    resetToken: Option[String] = None,
    resetTokenExpiresAt: Option[ZonedDateTime] = None,
    roles: Seq[Role] = Seq(RoleCitizen),
    country: Country = Country("FR"),
    language: Language = Language("fr"),
    profile: Option[Profile] = None,
    createdAt: Option[ZonedDateTime] = Some(DateHelper.now()),
    updatedAt: Option[ZonedDateTime] = None,
    isHardBounce: Boolean = false,
    lastMailingError: Option[MailingErrorLog] = None,
    organisationName: Option[String] = None,
    availableQuestions: Seq[QuestionId] = Seq.empty,
    userType: UserType = UserType.UserTypeUser,
    privacyPolicyApprovalDate: Option[ZonedDateTime] = None,
    lastFailedConnectionAttempt: Option[ZonedDateTime] = None,
    connectionAttemptsSinceLastSuccessful: Int = 1
  ): User = {
    User(
      userId = id,
      email = email,
      firstName = firstName,
      lastName = lastName,
      lastIp = lastIp,
      hashedPassword = hashedPassword,
      enabled = enabled,
      emailVerified = emailVerified,
      lastConnection = lastConnection,
      verificationToken = verificationToken,
      verificationTokenExpiresAt = verificationTokenExpiresAt,
      resetToken = resetToken,
      resetTokenExpiresAt = resetTokenExpiresAt,
      roles = roles,
      country = country,
      language = language,
      profile = profile,
      createdAt = createdAt,
      updatedAt = updatedAt,
      isHardBounce = isHardBounce,
      lastMailingError = lastMailingError,
      organisationName = organisationName,
      availableQuestions = availableQuestions,
      userType = userType,
      privacyPolicyApprovalDate = privacyPolicyApprovalDate,
      lastFailedConnectionAttempt = lastFailedConnectionAttempt,
      connectionAttemptsSinceLastSuccessful = connectionAttemptsSinceLastSuccessful
    )
  }

  def question(
    id: QuestionId,
    slug: String = "question-slug",
    countries: NonEmptyList[Country] = NonEmptyList.of(Country("FR")),
    defaultLanguage: Language = Language("fr"),
    languages: NonEmptyList[Language] = NonEmptyList.of(Language("fr")),
    questions: Multilingual[String Refined NonEmpty] = Multilingual(
      Language("fr") -> ("How to ask a question ?": String Refined NonEmpty)
    ),
    shortTitles: Option[Multilingual[String Refined NonEmpty]] = None,
    operationId: Option[OperationId] = None
  ): Question =
    Question(
      questionId = id,
      slug = slug,
      countries = countries,
      defaultLanguage = defaultLanguage,
      languages = languages,
      questions = questions,
      shortTitles = shortTitles,
      operationId = operationId
    )

  def simpleOperation(
    id: OperationId,
    slug: String = "operation-slug",
    operationKind: OperationKind = OperationKind.BusinessConsultation,
    createdAt: Option[ZonedDateTime] = None,
    updatedAt: Option[ZonedDateTime] = None
  ): SimpleOperation =
    SimpleOperation(
      operationId = id,
      slug = slug,
      operationKind = operationKind,
      createdAt = createdAt,
      updatedAt = updatedAt
    )

  def operation(
    operationId: OperationId,
    slug: String = "operation-slug",
    events: List[OperationAction] = List.empty,
    questions: Seq[QuestionWithDetails] = Seq.empty,
    operationKind: OperationKind = OperationKind.BusinessConsultation,
    createdAt: Option[ZonedDateTime] = None,
    updatedAt: Option[ZonedDateTime] = None
  ): Operation = Operation(
    operationId = operationId,
    slug = slug,
    operationKind = operationKind,
    createdAt = createdAt,
    updatedAt = updatedAt,
    events = events,
    questions = questions
  )

  val defaultMetas: Metas =
    Metas(
      titles = Some(Multilingual.fromDefault("Metas title")),
      descriptions = Some(Multilingual.fromDefault("Meta description")),
      picture = None
    )
  def operationOfQuestion(
    questionId: QuestionId,
    operationId: OperationId,
    startDate: ZonedDateTime = ZonedDateTime.parse("1968-07-03T00:00:00.000Z"),
    endDate: ZonedDateTime = ZonedDateTime.parse("2068-07-03T00:00:00.000Z"),
    operationTitles: Multilingual[String] = Multilingual.fromDefault("operation title"),
    proposalPrefixes: Multilingual[String] = Multilingual.fromDefault("Il faut"),
    canPropose: Boolean = true,
    sequenceCardsConfiguration: SequenceCardsConfiguration = SequenceCardsConfiguration.default,
    aboutUrls: Option[Multilingual[String]] = None,
    metas: Metas = defaultMetas,
    theme: QuestionTheme = QuestionTheme.default,
    descriptions: Option[Multilingual[String]] = None,
    consultationImages: Option[Multilingual[String]] = Some(Multilingual.fromDefault("image-url")),
    consultationImageAlts: Option[Multilingual[String Refined MaxSize[130]]] = Some(
      Multilingual.fromDefault("image alternative")
    ),
    descriptionImages: Option[Multilingual[String]] = None,
    descriptionImageAlts: Option[Multilingual[String Refined MaxSize[130]]] = None,
    partnersLogos: Option[String] = None,
    partnersLogosAlt: Option[String Refined MaxSize[130]] = None,
    initiatorsLogos: Option[String] = None,
    initiatorsLogosAlt: Option[String Refined MaxSize[130]] = None,
    consultationHeader: Option[String] = None,
    consultationHeaderAlts: Option[Multilingual[String Refined MaxSize[130]]] = None,
    cobrandingLogo: Option[String] = None,
    cobrandingLogoAlt: Option[String Refined MaxSize[130]] = None,
    resultsLink: Option[ResultsLink] = None,
    proposalsCount: Int = 42,
    participantsCount: Int = 84,
    actions: Option[String] = None,
    featured: Boolean = false,
    votesCount: Int = 0,
    votesTarget: Int = 100_000,
    timeline: OperationOfQuestionTimeline = OperationOfQuestionTimeline(None, None, None),
    createdAt: ZonedDateTime = ZonedDateTime.parse("1968-06-03T00:00:00.000Z"),
    sessionBindingMode: Boolean = false,
    reportUrl: Option[String] = None,
    actionsUrl: Option[String] = None
  ): OperationOfQuestion = OperationOfQuestion(
    questionId = questionId,
    operationId = operationId,
    startDate = startDate,
    endDate = endDate,
    operationTitles = operationTitles,
    proposalPrefixes = proposalPrefixes,
    canPropose = canPropose,
    sequenceCardsConfiguration = sequenceCardsConfiguration,
    aboutUrls = aboutUrls,
    metas = metas,
    theme = theme,
    descriptions = descriptions,
    consultationImages = consultationImages,
    consultationImageAlts = consultationImageAlts,
    descriptionImages = descriptionImages,
    descriptionImageAlts = descriptionImageAlts,
    partnersLogos = partnersLogos,
    partnersLogosAlt = partnersLogosAlt,
    initiatorsLogos = initiatorsLogos,
    initiatorsLogosAlt = initiatorsLogosAlt,
    consultationHeader = consultationHeader,
    consultationHeaderAlts = consultationHeaderAlts,
    cobrandingLogo = cobrandingLogo,
    cobrandingLogoAlt = cobrandingLogoAlt,
    resultsLink = resultsLink,
    proposalsCount = proposalsCount,
    participantsCount = participantsCount,
    actions = actions,
    featured = featured,
    votesCount = votesCount,
    votesTarget = votesTarget,
    timeline = timeline,
    createdAt = createdAt,
    sessionBindingMode = sessionBindingMode,
    reportUrl = reportUrl,
    actionsUrl = actionsUrl
  )

  def indexedOperationOfQuestion(
    questionId: QuestionId,
    operationId: OperationId,
    questions: Multilingual[String Refined NonEmpty] =
      Multilingual.fromDefault("What's the question?": String Refined NonEmpty),
    questionGeneral: String Refined NonEmpty = "What's the question?": String Refined NonEmpty,
    questionKeyword: String Refined NonEmpty = "What's the question?": String Refined NonEmpty,
    slug: String = "question-slug",
    questionShortTitles: Option[Multilingual[String Refined NonEmpty]] = Some(
      Multilingual.fromDefault("short title": String Refined NonEmpty)
    ),
    startDate: ZonedDateTime = ZonedDateTime.parse("1968-07-03T00:00:00.000Z"),
    endDate: ZonedDateTime = ZonedDateTime.parse("2068-07-03T00:00:00.000Z"),
    status: OperationOfQuestion.Status = OperationOfQuestion.Status.Open,
    theme: QuestionTheme = QuestionTheme.default,
    descriptions: Option[Multilingual[String]] = None,
    consultationImages: Option[Multilingual[String]] = Some(Multilingual.fromDefault("image-url")),
    consultationImageAlts: Option[Multilingual[String Refined MaxSize[130]]] = Some(
      Multilingual.fromDefault("image alternative")
    ),
    descriptionImages: Option[Multilingual[String]] = None,
    descriptionImageAlts: Option[Multilingual[String Refined MaxSize[130]]] = None,
    countries: NonEmptyList[Country] = NonEmptyList.of(Country("FR")),
    defaultLanguage: Language = Language("fr"),
    languages: NonEmptyList[Language] = NonEmptyList.of(Language("fr")),
    operationTitles: Multilingual[String] = Multilingual.fromDefault("operation title"),
    operationKind: String = OperationKind.GreatCause.value,
    aboutUrls: Option[Multilingual[String]] = None,
    resultsLink: Option[String] = None,
    proposalsCount: Int = 42,
    participantsCount: Int = 84,
    votesCount: Int = 2048,
    actions: Option[String] = None,
    featured: Boolean = true,
    top20ConsensusThreshold: Option[Double] = None
  ): IndexedOperationOfQuestion = IndexedOperationOfQuestion(
    questionId = questionId,
    questions = questions,
    questionGeneral = questionGeneral,
    questionKeyword = questionKeyword,
    slug = slug,
    questionShortTitles = questionShortTitles,
    startDate = startDate,
    endDate = endDate,
    status = status,
    theme = theme,
    descriptions = descriptions,
    consultationImages = consultationImages,
    consultationImageAlts = consultationImageAlts,
    descriptionImages = descriptionImages,
    descriptionImageAlts = descriptionImageAlts,
    countries = countries,
    languages = languages,
    defaultLanguage = defaultLanguage,
    operationId = operationId,
    operationTitles = operationTitles,
    operationKind = operationKind,
    aboutUrls = aboutUrls,
    resultsLink = resultsLink,
    proposalsCount = proposalsCount,
    participantsCount = participantsCount,
    votesCount = votesCount,
    actions = actions,
    featured = featured,
    top20ConsensusThreshold = top20ConsensusThreshold
  )

  def postGen(
    postId: PostId,
    name: String = "post name",
    slug: String = "post-slug",
    displayHome: Boolean = true,
    postDate: ZonedDateTime = ZonedDateTime.parse("2020-06-10T10:10:10.000Z"),
    thumbnailUrl: URI = new URI("https://example.com/thumbnail"),
    thumbnailAlt: Option[String] = Some("image alternative"),
    sourceUrl: URI = new URI("https://example.com/source"),
    summary: String = "This is a summary for an awesome post.",
    country: Country = Country("FR")
  ): Post = Post(
    postId = postId,
    name = name,
    slug = slug,
    displayHome = displayHome,
    postDate = postDate,
    thumbnailUrl = thumbnailUrl,
    thumbnailAlt = thumbnailAlt,
    sourceUrl = sourceUrl,
    summary = summary,
    country = country
  )

  def indexedPost(
    postId: PostId,
    name: String = "post name",
    slug: String = "post-slug",
    displayHome: Boolean = true,
    postDate: ZonedDateTime = ZonedDateTime.parse("2020-06-10T10:10:10.000Z"),
    thumbnailUrl: URI = new URI("https://example.com/thumbnail"),
    thumbnailAlt: Option[String] = Some("image alternative"),
    sourceUrl: URI = new URI("https://example.com/source"),
    summary: String = "This is a summary for an awesome post.",
    country: Country = Country("FR")
  ): IndexedPost =
    IndexedPost(
      postId = postId,
      name = name,
      slug = slug,
      displayHome = displayHome,
      postDate = postDate,
      thumbnailUrl = thumbnailUrl,
      thumbnailAlt = thumbnailAlt,
      sourceUrl = sourceUrl,
      summary = summary,
      country = country
    )

  def client(
    clientId: ClientId,
    name: String = "default",
    allowedGrantTypes: Seq[String] = Seq.empty,
    secret: Option[String] = None,
    scope: Option[String] = None,
    redirectUri: Option[String] = None,
    defaultUserId: Option[UserId] = None,
    roles: Seq[Role] = Seq.empty,
    tokenExpirationSeconds: Int = 300,
    refreshExpirationSeconds: Int = 400,
    reconnectExpirationSeconds: Int = 900
  ): Client = {
    Client(
      clientId = clientId,
      name = name,
      allowedGrantTypes = allowedGrantTypes,
      secret = secret,
      scope = scope,
      redirectUri = redirectUri,
      defaultUserId = defaultUserId,
      roles = roles,
      tokenExpirationSeconds = tokenExpirationSeconds,
      refreshExpirationSeconds = refreshExpirationSeconds,
      reconnectExpirationSeconds = reconnectExpirationSeconds
    )
  }

  def idea(
    ideaId: IdeaId,
    name: String = "idea name",
    operationId: Option[OperationId] = None,
    questionId: Option[QuestionId] = None,
    status: IdeaStatus = IdeaStatus.Activated,
    createdAt: Option[ZonedDateTime] = None,
    updatedAt: Option[ZonedDateTime] = None
  ): Idea =
    Idea(
      ideaId = ideaId,
      name = name,
      operationId = operationId,
      questionId = questionId,
      status = status,
      createdAt = createdAt,
      updatedAt = updatedAt
    )

  def tag(
    tagId: TagId,
    label: String = "tag label",
    display: TagDisplay = TagDisplay.Inherit,
    tagTypeId: TagTypeId = TagType.LEGACY.tagTypeId,
    weight: Float = 42f,
    operationId: Option[OperationId] = None,
    questionId: Option[QuestionId] = None
  ): Tag = Tag(
    tagId = tagId,
    label = label,
    display = display,
    tagTypeId = tagTypeId,
    weight = weight,
    operationId = operationId,
    questionId = questionId
  )

  def sequenceConfiguration(
    questionId: QuestionId,
    mainSequence: ExplorationSequenceConfiguration =
      ExplorationSequenceConfiguration.default(ExplorationSequenceConfigurationId("main-id")),
    controversial: SpecificSequenceConfiguration = SpecificSequenceConfiguration(
      SpecificSequenceConfigurationId("controversial-id")
    ),
    popular: SpecificSequenceConfiguration = SpecificSequenceConfiguration(
      SpecificSequenceConfigurationId("popular-id")
    ),
    keyword: SpecificSequenceConfiguration = SpecificSequenceConfiguration(
      SpecificSequenceConfigurationId("keyword-id")
    ),
    newProposalsVoteThreshold: Int = 10,
    testedProposalsEngagementThreshold: Option[Double] = None,
    testedProposalsScoreThreshold: Option[Double] = None,
    testedProposalsControversyThreshold: Option[Double] = None,
    testedProposalsMaxVotesThreshold: Option[Int] = None,
    nonSequenceVotesWeight: Double = 0.5
  ): SequenceConfiguration =
    SequenceConfiguration(
      questionId = questionId,
      mainSequence = mainSequence,
      controversial = controversial,
      popular = popular,
      keyword = keyword,
      newProposalsVoteThreshold = newProposalsVoteThreshold,
      testedProposalsEngagementThreshold = testedProposalsEngagementThreshold,
      testedProposalsScoreThreshold = testedProposalsScoreThreshold,
      testedProposalsControversyThreshold = testedProposalsControversyThreshold,
      testedProposalsMaxVotesThreshold = testedProposalsMaxVotesThreshold,
      nonSequenceVotesWeight = nonSequenceVotesWeight
    )

  def keyword(
    questionId: QuestionId,
    key: String,
    label: String = "label",
    score: Float = 0.42f,
    count: NonNegInt = 21,
    topKeyword: Boolean = false
  ): Keyword =
    Keyword(questionId = questionId, key = key, label = label, score = score, count = count, topKeyword = topKeyword)

  def source(
    id: SourceId,
    userId: UserId,
    name: String = "name",
    source: String = "source",
    createdAt: ZonedDateTime = DateHelper.now(),
    updatedAt: ZonedDateTime = DateHelper.now()
  ): Source = Source(id, name, source, createdAt, updatedAt, userId)

  def demographicsCard(
    id: DemographicsCardId,
    name: String = "Demo name",
    layout: Layout = Layout.ThreeColumnsRadio,
    dataType: String = "data-type",
    languages: NonEmptyList[Language] = NonEmptyList.of(Language("fr"), Language("de")),
    titles: Multilingual[String] = Multilingual((Language("fr") -> "Titre démo"), (Language("de") -> "Demo titel")),
    sessionBindingMode: Boolean = false,
    parameters: NonEmptyList[LabelsValue] = NonEmptyList.of(
      LabelsValue(label = Multilingual((Language("fr") -> "valeur"), (Language("de") -> "Wert")), value = "value")
    ),
    createdAt: ZonedDateTime = DateHelper.now().withZoneSameInstant(ZoneId.systemDefault()),
    updatedAt: ZonedDateTime = DateHelper.now().withZoneSameInstant(ZoneId.systemDefault())
  ): DemographicsCard =
    DemographicsCard(
      id = id,
      name = name,
      layout = layout,
      dataType = dataType,
      languages = languages,
      titles = titles,
      sessionBindingMode = sessionBindingMode,
      parameters = parameters,
      createdAt = createdAt,
      updatedAt = updatedAt
    )

  def partner(
    partnerId: PartnerId,
    questionId: QuestionId,
    name: String = "partner-name",
    logo: Option[String] = None,
    link: Option[String] = None,
    organisationId: Option[UserId] = None,
    partnerKind: PartnerKind = PartnerKind.Founder,
    weight: Float = 0f
  ): Partner = Partner(
    partnerId = partnerId,
    name = name,
    logo = logo,
    link = link,
    organisationId = organisationId,
    partnerKind = partnerKind,
    questionId = questionId,
    weight = weight
  )
}

object TestUtils extends TestUtils
