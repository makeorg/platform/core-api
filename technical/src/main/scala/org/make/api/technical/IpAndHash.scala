/*
 *  Make.org Core API
 *  Copyright (C) 2021 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.technical

import scala.util.chaining.scalaUtilChainingOps

final case class IpAndHash(ip: String, hash: String)

object IpAndHash {
  private val IPv6SafeSegments = 3
  private val IPv6SegmentsCount = 8

  private def obfuscateIpV4(ip: String): String =
    s"${ip.take(ip.lastIndexOf("."))}.x"

  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  private val obfuscateIpV6: String => String =
    _.split("::")
      .map(_.split(':').filterNot(_.isEmpty))
      .pipe({
        case Array()     => Array[String]()
        case Array(elem) => elem
        case Array(left, right) =>
          val numberOfSegments = left.length + right.length
          val numberOfCompressedSegments = IPv6SegmentsCount - numberOfSegments
          left ++ Array.fill(numberOfCompressedSegments)("0") ++ right
        case _ => throw new Error("Invalid IPv6 format")
      })
      .take(IPv6SafeSegments)
      .mkString(":")
      .concat("::")

  def obfuscateIp(ip: String): String = ip match {
    case ip if ip.contains(":") => obfuscateIpV6(ip)
    case _                      => obfuscateIpV4(ip)
  }
}
