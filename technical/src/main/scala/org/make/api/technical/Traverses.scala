package org.make.api.technical

import cats.data.Chain

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object Traverses {
  implicit class RichTraverse[A](as: List[A]) {
    def sequentialTraverse[B](f: A => Future[B]): Future[List[B]] = {
      as.foldLeft(Future.successful(Chain.empty[B])) { (accM, a) =>
          for {
            acc <- accM
            b   <- f(a)
          } yield acc :+ b
        }
        .map(_.toList)
    }
  }

}
