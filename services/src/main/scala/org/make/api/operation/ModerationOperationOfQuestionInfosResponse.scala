/*
 *  Make.org Core API
 *  Copyright (C) 2021 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.operation

import cats.data.NonEmptyList
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import io.swagger.annotations.ApiModelProperty
import org.make.api.question.SimpleQuestionWordingResponse
import org.make.core.technical.MultilingualType
import org.make.core.operation.indexed.IndexedOperationOfQuestion
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, Language}

import java.time.ZonedDateTime
import scala.annotation.meta.field

final case class ModerationOperationOfQuestionInfosResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  slug: String,
  @(ApiModelProperty @field)(dataType = "org.make.api.question.SimpleQuestionWordingResponse", required = true)
  wording: SimpleQuestionWordingResponse,
  @(ApiModelProperty @field)(dataType = "list[string]", example = "FR", required = true)
  countries: NonEmptyList[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  override val defaultLanguage: Language,
  override val languages: NonEmptyList[Language],
  startDate: ZonedDateTime,
  endDate: ZonedDateTime,
  totalProposalCount: Int,
  proposalToModerateCount: Int,
  hasTags: Boolean
) extends MultilingualType

object ModerationOperationOfQuestionInfosResponse {
  def apply(
    question: IndexedOperationOfQuestion,
    proposalToModerateCount: Int,
    totalProposalCount: Int,
    hasTags: Boolean
  ): ModerationOperationOfQuestionInfosResponse = {

    ModerationOperationOfQuestionInfosResponse(
      questionId = question.questionId,
      slug = question.slug,
      wording = SimpleQuestionWordingResponse(
        question.operationTitles.getTranslationUnsafe(question.defaultLanguage),
        question.questions.getTranslationUnsafe(question.defaultLanguage)
      ),
      countries = question.countries,
      defaultLanguage = question.defaultLanguage,
      languages = question.languages,
      startDate = question.startDate,
      endDate = question.endDate,
      totalProposalCount = totalProposalCount,
      proposalToModerateCount = proposalToModerateCount,
      hasTags = hasTags
    )
  }

  implicit val codec: Codec[ModerationOperationOfQuestionInfosResponse] = deriveCodec
}
