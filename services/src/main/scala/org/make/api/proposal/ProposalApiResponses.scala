/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.proposal

import enumeratum.values.{StringCirceEnum, StringEnum, StringEnumEntry}
import cats.implicits._
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import io.swagger.annotations.{ApiModel, ApiModelProperty}
import org.make.api.proposal.ProposalActorResponse.{VoteActorResponse, VotesActorResponse}
import org.make.api.question.{SimpleQuestionResponse, SimpleQuestionWordingResponse}
import org.make.core.technical.Multilingual
import org.make.core.history.HistoryActions.VoteAndQualifications
import org.make.core.idea.IdeaId
import org.make.core.operation.OperationId
import org.make.core.proposal.VoteKey.{Agree, Disagree, Neutral}
import org.make.core.proposal._
import org.make.core.proposal.indexed._
import org.make.core.question.QuestionId
import org.make.core.reference._
import org.make.core.tag.{Tag, TagId, TagTypeId}
import org.make.core.user.{User, UserId, UserType}
import org.make.core.{CirceFormatters, RequestContext, SlugHelper}

import java.time.temporal.ChronoUnit
import java.time.{LocalDate, ZonedDateTime}
import scala.annotation.meta.field

final case class ModerationProposalAuthorResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  userId: UserId,
  firstName: Option[String],
  lastName: Option[String],
  displayName: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "12345")
  postalCode: Option[String],
  @(ApiModelProperty @field)(dataType = "int")
  age: Option[Int],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png")
  avatarUrl: Option[String],
  organisationName: Option[String],
  organisationSlug: Option[String],
  reachable: Boolean
)

object ModerationProposalAuthorResponse {

  def apply(user: User): ModerationProposalAuthorResponse =
    ModerationProposalAuthorResponse(
      userId = user.userId,
      firstName = user.firstName,
      lastName = user.lastName,
      displayName = user.displayName,
      postalCode = user.profile.flatMap(_.postalCode),
      age = user.profile
        .flatMap(_.dateOfBirth)
        .map(date => ChronoUnit.YEARS.between(date, LocalDate.now()).toInt),
      avatarUrl = user.profile.flatMap(_.avatarUrl),
      organisationName = user.organisationName,
      organisationSlug = user.organisationName.map(SlugHelper.apply),
      reachable = !user.isHardBounce
    )

  implicit val codec: Codec[ModerationProposalAuthorResponse] = deriveCodec
}

final case class ModerationAuthorResponse(
  author: ModerationProposalAuthorResponse,
  proposals: Seq[ModerationProposalResponse],
  total: Int
)

object ModerationAuthorResponse {
  implicit val codec: Codec[ModerationAuthorResponse] = deriveCodec
}

final case class ModerationProposalResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: ProposalId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  proposalId: ProposalId,
  slug: String,
  content: String,
  contentTranslations: Option[Multilingual[String]],
  submittedAsLanguage: Option[Language],
  author: ModerationProposalAuthorResponse,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = ProposalStatus.swaggerAllowableValues
  )
  status: ProposalStatus,
  proposalType: ProposalType,
  @(ApiModelProperty @field)(dataType = "string", example = "other")
  refusalReason: Option[String] = None,
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  tags: Seq[TagId] = Seq.empty,
  votes: Seq[ModerationVoteResponse],
  context: RequestContext,
  createdAt: Option[ZonedDateTime],
  updatedAt: Option[ZonedDateTime],
  events: Seq[ProposalActionResponse],
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  idea: Option[IdeaId],
  @(ApiModelProperty @field)(hidden = true)
  ideaProposals: Seq[IndexedProposal],
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  questionId: Option[QuestionId],
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  operationId: Option[OperationId],
  keywords: Seq[ProposalKeyword],
  zone: Zone
)

object ModerationProposalResponse extends CirceFormatters {
  implicit val codec: Codec[ModerationProposalResponse] = deriveCodec
}

final case class ModerationVoteResponse(
  @(ApiModelProperty @field)(dataType = "string", required = true, allowableValues = VoteKey.swaggerAllowableValues)
  voteKey: VoteKey,
  score: Double,
  qualifications: Seq[QualificationResponse]
)

object ModerationVoteResponse {
  implicit val codec: Codec[ModerationVoteResponse] = deriveCodec

  def apply(wrapper: VotingOptionWrapper, score: Double): ModerationVoteResponse =
    ModerationVoteResponse(
      voteKey = wrapper.vote.key,
      score = score,
      qualifications = wrapper.deprecatedQualificationsSeq.map(QualificationResponse.parseQualification(_, false))
    )
}

final case class ProposalActionResponse(
  date: ZonedDateTime,
  user: Option[ProposalActionAuthorResponse],
  actionType: String,
  arguments: Map[String, String]
)

object ProposalActionResponse extends CirceFormatters {
  implicit val codec: Codec[ProposalActionResponse] = deriveCodec
}

final case class ProposalActionAuthorResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: UserId,
  displayName: Option[String]
)

object ProposalActionAuthorResponse {
  implicit val codec: Codec[ProposalActionAuthorResponse] = deriveCodec
}

@ApiModel
final case class ProposalIdResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: ProposalId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  proposalId: ProposalId
)

object ProposalIdResponse {
  implicit val codec: Codec[ProposalIdResponse] = deriveCodec

  def apply(id: ProposalId): ProposalIdResponse = ProposalIdResponse(id, id)
}

final case class AuthorResponse(
  userId: UserId,
  firstName: Option[String],
  displayName: Option[String],
  organisationName: Option[String],
  organisationSlug: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "12345")
  postalCode: Option[String],
  @(ApiModelProperty @field)(dataType = "int", example = "21")
  age: Option[Int],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png")
  avatarUrl: Option[String],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = UserType.swaggerAllowableValues)
  userType: UserType
)

object AuthorResponse {
  implicit val codec: Codec[AuthorResponse] = deriveCodec

  def fromIndexedAuthor(author: IndexedAuthor): AuthorResponse =
    AuthorResponse(
      userId = author.userId,
      firstName = author.firstName,
      displayName = author.displayName,
      organisationName = author.organisationName,
      organisationSlug = author.organisationSlug,
      postalCode = author.postalCode,
      age = author.age,
      avatarUrl = author.avatarUrl,
      userType = author.userType
    )
}

final case class ProposalContextResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  operation: Option[OperationId],
  source: Option[String],
  location: Option[String],
  questionSlug: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "FR")
  country: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr")
  questionLanguage: Option[Language],
  proposalLanguage: Option[Language],
  clientLanguage: Option[Language],
  getParameters: Seq[GetParameterResponse]
)

object ProposalContextResponse {
  implicit val codec: Codec[ProposalContextResponse] = deriveCodec

  def fromIndexedContext(context: IndexedContext): ProposalContextResponse = {
    ProposalContextResponse(
      context.operation,
      context.source,
      context.location,
      context.questionSlug,
      context.country,
      context.questionLanguage,
      context.proposalLanguage,
      context.clientLanguage,
      context.getParameters.map(GetParameterResponse.fromIndexedGetParameters)
    )
  }
}

final case class GetParameterResponse(key: String, value: String)

object GetParameterResponse {
  implicit val codec: Codec[GetParameterResponse] = deriveCodec

  def fromIndexedGetParameters(parameter: IndexedGetParameters): GetParameterResponse = {
    GetParameterResponse(key = parameter.key, value = parameter.value)
  }
}

final case class OrganisationInfoResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  organisationId: UserId,
  organisationName: Option[String],
  organisationSlug: Option[String]
)

object OrganisationInfoResponse {
  implicit val codec: Codec[OrganisationInfoResponse] = deriveCodec

  def fromIndexedOrganisationInfo(indexedOrganisationInfo: IndexedOrganisationInfo): OrganisationInfoResponse = {
    OrganisationInfoResponse(
      indexedOrganisationInfo.organisationId,
      indexedOrganisationInfo.organisationName,
      indexedOrganisationInfo.organisationSlug
    )
  }
}

@ApiModel
final case class ProposalResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: ProposalId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  content: String,
  contentLanguage: Language,
  translatedContent: Option[String],
  translatedLanguage: Option[Language],
  slug: String,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = ProposalStatus.swaggerAllowableValues
  )
  status: ProposalStatus,
  createdAt: ZonedDateTime,
  updatedAt: Option[ZonedDateTime],
  votes: Seq[VoteResponse],
  context: Option[ProposalContextResponse],
  author: Option[AuthorResponse],
  organisations: Seq[OrganisationInfoResponse],
  tags: Seq[IndexedTag],
  selectedStakeTag: Option[IndexedTag],
  myProposal: Boolean,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  idea: Option[IdeaId],
  question: Option[SimpleQuestionResponse],
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  operationId: Option[OperationId],
  proposalKey: String,
  keywords: Seq[IndexedProposalKeyword]
)

object ProposalResponse extends CirceFormatters {
  implicit val codec: Codec[ProposalResponse] = deriveCodec

  def apply(
    indexedProposal: IndexedProposal,
    myProposal: Boolean,
    voteAndQualifications: Option[VoteAndQualifications],
    proposalKey: String,
    newProposalsVoteThreshold: Int,
    preferredLanguage: Option[Language],
    questionDefaultLanguage: Option[Language]
  ): ProposalResponse = {
    val scores = VotingOptionsScores(indexedProposal.votes, newProposalsVoteThreshold)
    val votesResponses =
      indexedProposal.votes.wrappers.map(wrapper => {
        val hasVoted =
          voteAndQualifications match {
            case Some(VoteAndQualifications(wrapper.vote.key, _, _, _)) => true
            case _                                                      => false
          }
        val qualifications = wrapper.deprecatedQualificationsSeq
        val score = scores.get(wrapper.vote.key)
        VoteResponse.parseVote(wrapper.vote, qualifications, hasVoted, voteAndQualifications, score)
      })
    val mainLanguage =
      indexedProposal.submittedAsLanguage
      // When a legacy proposal does not have a submittedAsLanguage, we get the first one from the
      // question. This is reasonably safe because legacy questions will not be multilingual.
        .orElse(indexedProposal.question.map(_.languages.head))
        .getOrElse(Language("fr"))

    // if preferredLanguage if not defined or is equal to the main language: no translations
    // else translate with preferredLanguage if possible or with the question default language
    // if this language is not equal to the main language
    val (translatedContent, translatedLanguage) =
      preferredLanguage
        .filter(_ != mainLanguage)
        .flatMap(
          pl =>
            indexedProposal.content
              .getTranslation(pl)
              .tupleRight(pl)
              .orElse(
                questionDefaultLanguage
                  .filter(_ != mainLanguage)
                  .flatMap(qdl => indexedProposal.content.getTranslation(qdl).tupleRight(qdl))
              )
        )
        .separate
    val returnedLanguage = translatedLanguage.getOrElse(mainLanguage)
    ProposalResponse(
      id = indexedProposal.id,
      content = indexedProposal.contentGeneral,
      contentLanguage = mainLanguage,
      translatedContent = translatedContent,
      translatedLanguage = translatedLanguage,
      slug = indexedProposal.slug,
      status = indexedProposal.status,
      createdAt = indexedProposal.createdAt,
      updatedAt = indexedProposal.updatedAt,
      votes = votesResponses,
      context = indexedProposal.context.map(ProposalContextResponse.fromIndexedContext),
      author = Option.unless(indexedProposal.isAnonymous)(AuthorResponse.fromIndexedAuthor(indexedProposal.author)),
      organisations = indexedProposal.organisations.map(OrganisationInfoResponse.fromIndexedOrganisationInfo),
      tags = indexedProposal.tags,
      selectedStakeTag = indexedProposal.selectedStakeTag,
      myProposal = myProposal,
      idea = indexedProposal.ideaId,
      question = indexedProposal.question.map { proposalQuestion =>
        SimpleQuestionResponse(
          questionId = proposalQuestion.questionId,
          slug = proposalQuestion.slug,
          wording = SimpleQuestionWordingResponse(
            proposalQuestion.titles
              .getTranslation(returnedLanguage)
              .getOrElse(proposalQuestion.titles.getTranslationUnsafe(proposalQuestion.languages.head)),
            proposalQuestion.questions
              .getTranslation(returnedLanguage)
              .getOrElse(proposalQuestion.questions.getTranslationUnsafe(proposalQuestion.languages.head))
          ),
          countries = proposalQuestion.countries,
          preferredLanguage = preferredLanguage,
          returnedLanguage = returnedLanguage,
          startDate = proposalQuestion.startDate,
          endDate = proposalQuestion.endDate
        )
      },
      operationId = indexedProposal.operationId,
      proposalKey = proposalKey,
      keywords = indexedProposal.keywords
    )
  }
}

@ApiModel
final case class ProposalsResultResponse(total: Long, results: Seq[ProposalResponse])

object ProposalsResultResponse {
  implicit val codec: Codec[ProposalsResultResponse] = deriveCodec
}

final case class ProposalsResultSeededResponse(
  total: Long,
  results: Seq[ProposalResponse],
  @(ApiModelProperty @field)(dataType = "int", example = "42") seed: Option[Int]
)

object ProposalsResultSeededResponse {
  implicit val codec: Codec[ProposalsResultSeededResponse] = deriveCodec

  val empty: ProposalsResultSeededResponse = ProposalsResultSeededResponse(0, Seq.empty, None)
}

final case class ProposalResultWithUserVote(
  proposal: ProposalResponse,
  @(ApiModelProperty @field)(dataType = "string", required = true, allowableValues = VoteKey.swaggerAllowableValues)
  vote: VoteKey,
  voteDate: ZonedDateTime,
  voteDetails: Option[VoteResponse]
)
object ProposalResultWithUserVote extends CirceFormatters {
  implicit val codec: Codec[ProposalResultWithUserVote] = deriveCodec
}

@ApiModel
final case class ProposalsResultWithUserVoteSeededResponse(
  total: Long,
  results: Seq[ProposalResultWithUserVote],
  @(ApiModelProperty @field)(dataType = "int", example = "42")
  seed: Option[Int]
)

object ProposalsResultWithUserVoteSeededResponse {
  implicit val codec: Codec[ProposalsResultWithUserVoteSeededResponse] = deriveCodec
}

@ApiModel
final case class VotesResponse(
  agree: VoteResponse,
  disagree: VoteResponse,
  neutral: VoteResponse,
  @(ApiModelProperty @field)(dataType = "string", example = "agree")
  voteKey: VoteKey,
  score: Double,
  qualifications: Seq[QualificationResponse],
  hasVoted: Boolean
)

object VotesResponse {

  implicit val codec: Codec[VotesResponse] = deriveCodec

  def parseVotes(votes: VotesActorResponse, hasVoted: Map[VoteKey, Boolean], votedKey: VoteKey): VotesResponse = {
    val agree = VoteResponse.parseVote(votes.agree, hasVoted.getOrElse(Agree, false))
    val disagree = VoteResponse.parseVote(votes.disagree, hasVoted.getOrElse(Disagree, false))
    val neutral = VoteResponse.parseVote(votes.neutral, hasVoted.getOrElse(Neutral, false))
    val voted = Seq(agree, disagree, neutral).find(vote => vote.voteKey == votedKey).getOrElse(agree)
    VotesResponse(agree, disagree, neutral, voted.voteKey, voted.score, voted.qualifications, voted.hasVoted)
  }
}

@ApiModel
final case class VoteResponse(
  @(ApiModelProperty @field)(dataType = "string", required = true, allowableValues = VoteKey.swaggerAllowableValues)
  voteKey: VoteKey,
  count: Int,
  score: Double,
  qualifications: Seq[QualificationResponse],
  hasVoted: Boolean
)

object VoteResponse {

  implicit val codec: Codec[VoteResponse] = deriveCodec

  def parseVote(vote: VoteActorResponse, hasVoted: Boolean): VoteResponse =
    VoteResponse(
      voteKey = vote.key,
      count = vote.count,
      score = vote.score,
      qualifications = vote.qualifications
        .map(qualification => QualificationResponse.parseQualification(qualification, hasQualified = false)),
      hasVoted = hasVoted
    )

  def parseVote(
    vote: Vote,
    qualifications: Seq[Qualification],
    hasVoted: Boolean,
    voteAndQualifications: Option[VoteAndQualifications],
    score: Double
  ): VoteResponse =
    VoteResponse(
      voteKey = vote.key,
      count = vote.countVerified,
      score = score,
      qualifications = qualifications
        .map(
          qualification =>
            QualificationResponse.parseQualification(qualification, hasQualified = voteAndQualifications match {
              case Some(VoteAndQualifications(_, keys, _, _)) if keys.contains(qualification.key) => true
              case _                                                                              => false
            })
        ),
      hasVoted = hasVoted
    )
}

@ApiModel
final case class QualificationResponse(
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = QualificationKey.swaggerAllowableValues
  )
  qualificationKey: QualificationKey,
  count: Int,
  hasQualified: Boolean
)

object QualificationResponse {
  implicit val codec: Codec[QualificationResponse] = deriveCodec

  def parseQualification(qualification: Qualification, hasQualified: Boolean): QualificationResponse =
    QualificationResponse(
      qualificationKey = qualification.key,
      count = qualification.count,
      hasQualified = hasQualified
    )
}

final case class DuplicateResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  ideaId: IdeaId,
  ideaName: String,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  proposalId: ProposalId,
  proposalContent: String,
  score: Double
)

object DuplicateResponse {
  implicit val codec: Codec[DuplicateResponse] = deriveCodec
}

@ApiModel
final case class TagForProposalResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "tag-slug", required = true) id: TagId,
  label: String,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  tagTypeId: TagTypeId,
  weight: Float,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  questionId: Option[QuestionId],
  checked: Boolean,
  predicted: Boolean
)

object TagForProposalResponse {
  implicit val codec: Codec[TagForProposalResponse] = deriveCodec

  def apply(tag: Tag, checked: Boolean, predicted: Boolean): TagForProposalResponse =
    TagForProposalResponse(
      id = tag.tagId,
      label = tag.label,
      tagTypeId = tag.tagTypeId,
      weight = tag.weight,
      questionId = tag.questionId,
      checked = checked,
      predicted = predicted
    )
}

final case class TagsForProposalResponse(
  tags: Seq[TagForProposalResponse],
  @(ApiModelProperty @field)(dataType = "string", example = "auto") modelName: String
)

object TagsForProposalResponse {
  implicit val codec: Codec[TagsForProposalResponse] = deriveCodec

  val empty: TagsForProposalResponse = TagsForProposalResponse(tags = Seq.empty, modelName = "")
}

sealed abstract class ProposalKeywordsResponseStatus(val value: String) extends StringEnumEntry

object ProposalKeywordsResponseStatus
    extends StringEnum[ProposalKeywordsResponseStatus]
    with StringCirceEnum[ProposalKeywordsResponseStatus] {

  case object Ok extends ProposalKeywordsResponseStatus("Ok")
  case object Error extends ProposalKeywordsResponseStatus("Error")

  override val values: IndexedSeq[ProposalKeywordsResponseStatus] = findValues
  final val swaggerAllowableValues = "Ok,Error"
}

final case class ProposalKeywordsResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  proposalId: ProposalId,
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = ProposalKeywordsResponseStatus.swaggerAllowableValues,
    required = true
  )
  status: ProposalKeywordsResponseStatus,
  message: Option[String]
)

object ProposalKeywordsResponse {
  implicit val codec: Codec[ProposalKeywordsResponse] = deriveCodec[ProposalKeywordsResponse]
}

final case class BulkActionResponse(
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  successes: Seq[ProposalId],
  failures: Seq[SingleActionResponse]
)

object BulkActionResponse {
  implicit val codec: Codec[BulkActionResponse] = deriveCodec[BulkActionResponse]
}

final case class SingleActionResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  proposalId: ProposalId,
  @(ApiModelProperty @field)(dataType = "string", required = true, allowableValues = ActionKey.swaggerAllowableValues)
  key: ActionKey,
  message: Option[String]
)

object SingleActionResponse extends CirceFormatters {
  implicit val codec: Codec[SingleActionResponse] = deriveCodec[SingleActionResponse]
}

sealed abstract class ActionKey(val value: String) extends StringEnumEntry

object ActionKey extends StringEnum[ActionKey] with StringCirceEnum[ActionKey] {
  final case class ValidationError(key: String) extends ActionKey(key)
  case object NotFound extends ActionKey("not_found")
  case object OK extends ActionKey("ok")
  case object QuestionNotFound extends ActionKey("question_not_found")
  case object Unknown extends ActionKey("unknown")

  override val values: IndexedSeq[ActionKey] = findValues
  final val swaggerAllowableValues = "not_found,ok,question_not_found,unknown,other"
}
