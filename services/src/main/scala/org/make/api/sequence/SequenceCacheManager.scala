/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.sequence

import akka.actor.typed.receptionist.ServiceKey
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import cats.data.{NonEmptyList => Nel}
import org.make.api.sequence.SequenceBehaviour.ConsensusParam
import org.make.api.technical.ActorProtocol
import org.make.api.technical.sequence.SequenceCacheConfiguration
import org.make.core.proposal.indexed.{IndexedProposal, Zone}
import org.make.core.question.QuestionId
import org.make.core.sequence.{SequenceConfiguration, SequenceKind}
import scalacache._
import scalacache.guava._
import scalacache.modes.scalaFuture._
import scalacache.memoization.memoizeF

import scala.concurrent.duration.DurationInt
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object SequenceCacheManager {

  type ConfiguredBehaviour = SequenceConfiguration => Future[SequenceBehaviour]

  def apply(
    config: SequenceCacheConfiguration,
    sequenceService: SequenceService,
    sequenceConfigurationService: SequenceConfigurationService
  ): Behavior[Protocol] = {
    Behaviors.withTimers { timers =>
      timers.startTimerWithFixedDelay(ExpireChildren, config.checkInactivityTimer)
      handleMessage(Map.empty)(reloadProposals(config, sequenceService, sequenceConfigurationService), config)
    }
  }

  def reloadProposals(
    sequenceCacheConfiguration: SequenceCacheConfiguration,
    sequenceService: SequenceService,
    sequenceConfigurationService: SequenceConfigurationService
  )(key: CacheKey, behaviour: ConfiguredBehaviour)(): Future[Nel[IndexedProposal]] = {
    @SuppressWarnings(Array("org.wartremover.warts.Recursion"))
    def simpleSequence(behaviour: SequenceBehaviour, retries: Int = 3): Future[Nel[IndexedProposal]] = {
      sequenceService.simpleSequence(Seq.empty, behaviour, Seq.empty, None).flatMap {
        case Seq() =>
          if (retries > 0) {
            simpleSequence(behaviour, retries - 1)
          } else {
            Future.failed(
              new IllegalStateException(
                s"Question ${key.questionId.value} might not have any proposals eligible for sequence."
              )
            )
          }
        case head +: tail => Future.successful(Nel(head, tail.toList))
      }
    }

    sequenceConfigurationService.getSequenceConfigurationByQuestionId(key.questionId).flatMap { config =>
      val customConfig: SequenceConfiguration = config
        .copy(mainSequence = config.mainSequence.copy(sequenceSize = sequenceCacheConfiguration.proposalsPoolSize))
      behaviour(customConfig).flatMap(simpleSequence(_))
    }
  }

  @SuppressWarnings(Array("org.wartremover.warts.Recursion"))
  def handleMessage(configCache: Map[CacheKey, ActorRef[SequenceCacheActor.Protocol]])(
    implicit reloadProposals: (CacheKey, ConfiguredBehaviour) => () => Future[Nel[IndexedProposal]],
    config: SequenceCacheConfiguration
  ): Behavior[Protocol] = {
    Behaviors.setup { context =>
      def getProposal(cmd: GetProposalCmd, replyTo: ActorRef[IndexedProposal]): Behavior[Protocol] = {
        configCache.get(cmd.key) match {
          case None =>
            val cache: ActorRef[SequenceCacheActor.Protocol] =
              context.spawn(
                SequenceCacheActor(reloadProposals(cmd.key, cmd.behaviour), config),
                SequenceCacheActor.name(cmd.key)
              )
            context.watchWith(cache, ChildTerminated(cmd.key))
            cache ! SequenceCacheActor.GetProposal(replyTo)
            handleMessage(configCache ++ Map(cmd.key -> cache))
          case Some(actorRef) =>
            actorRef ! SequenceCacheActor.GetProposal(replyTo)
            Behaviors.same
        }
      }

      Behaviors
        .receiveMessage[Protocol] {
          case cmd: GetProposalCmd => getProposal(cmd, cmd.replyTo)
          case ExpireChildren =>
            configCache.values.foreach { ref =>
              ref ! SequenceCacheActor.Expire
            }
            Behaviors.same
          case ChildTerminated(questionId) =>
            handleMessage(configCache - questionId)
        }
    }
  }

  implicit val guavaCache: Cache[ConsensusParam] = GuavaCache[ConsensusParam]

  def getCachedThreshold(getThreshold: () => Future[ConsensusParam]): Future[ConsensusParam] = {
    memoizeF[Future, ConsensusParam](Some(1.hour)) {
      getThreshold()
    }
  }

  sealed trait Protocol extends ActorProtocol

  sealed trait Command extends Protocol

  sealed trait GetProposalCmd extends Command {
    val replyTo: ActorRef[IndexedProposal]
    val key: CacheKey
    def behaviour: ConfiguredBehaviour
  }

  final case class GetProposal(id: QuestionId, replyTo: ActorRef[IndexedProposal]) extends GetProposalCmd {
    override val key: CacheKey = CacheKey(id, SequenceKind.Standard)
    override def behaviour: ConfiguredBehaviour = { config =>
      Future.successful(SequenceBehaviourProvider[Unit].behaviour((), config, key.questionId, None))
    }
  }

  final case class GetConsensusProposal(
    id: QuestionId,
    threshold: () => Future[ConsensusParam],
    replyTo: ActorRef[IndexedProposal]
  ) extends GetProposalCmd {
    override val key: CacheKey = CacheKey(id, SequenceKind.Consensus)
    override def behaviour: ConfiguredBehaviour = { config =>
      getCachedThreshold(threshold).map { param =>
        SequenceBehaviourProvider[ConsensusParam].behaviour(param, config, key.questionId, None)
      }
    }
  }

  final case class GetControversyProposal(id: QuestionId, replyTo: ActorRef[IndexedProposal]) extends GetProposalCmd {
    override val key: CacheKey = CacheKey(id, SequenceKind.Controversy)
    override def behaviour: ConfiguredBehaviour = { config =>
      Future.successful(
        SequenceBehaviourProvider[Zone.Controversy.type].behaviour(Zone.Controversy, config, key.questionId, None)
      )
    }
  }
  case object ExpireChildren extends Command
  final case class ChildTerminated(key: CacheKey) extends Command

  val name: String = "sequence-cache-manager"
  val SequenceCacheActorKey: ServiceKey[Protocol] = ServiceKey(name)

  final case class CacheKey(questionId: QuestionId, kind: SequenceKind)
}
