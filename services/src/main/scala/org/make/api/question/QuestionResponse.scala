/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.question

import java.time.ZonedDateTime
import cats.data.NonEmptyList
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection._
import io.circe.{Codec, Decoder, Encoder}
import io.circe.refined._
import io.swagger.annotations.{ApiModel, ApiModelProperty}
import org.make.core.CirceFormatters
import org.make.core.question.QuestionId
import io.circe.generic.semiauto.{deriveCodec, deriveDecoder, deriveEncoder}
import org.make.core.idea.{CommentQualificationKey, CommentVoteKey, IdeaId, TopIdeaCommentId, TopIdeaId, TopIdeaScores}
import org.make.core.operation.indexed.IndexedOperationOfQuestion
import org.make.core.operation.{OperationId, OperationOfQuestion, QuestionTheme}
import org.make.core.profile.Gender
import org.make.core.reference.{Country, Language}
import org.make.core.user.UserId

import scala.annotation.meta.field

@ApiModel
final case class SimpleQuestionResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  slug: String,
  @(ApiModelProperty @field)(dataType = "org.make.api.question.SimpleQuestionWordingResponse")
  wording: SimpleQuestionWordingResponse,
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  countries: NonEmptyList[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  preferredLanguage: Option[Language],
  returnedLanguage: Language,
  startDate: ZonedDateTime,
  endDate: ZonedDateTime
)

object SimpleQuestionResponse extends CirceFormatters {

  implicit val codec: Codec[SimpleQuestionResponse] = deriveCodec
}

final case class SimpleQuestionWordingResponse(title: String, question: String Refined NonEmpty)

object SimpleQuestionWordingResponse extends CirceFormatters {

  implicit val codec: Codec[SimpleQuestionWordingResponse] = deriveCodec
}

final case class QuestionPersonalityResponseWithTotal(total: Int, results: Seq[QuestionPersonalityResponse])

object QuestionPersonalityResponseWithTotal {

  implicit val codec: Codec[QuestionPersonalityResponseWithTotal] = deriveCodec
}

final case class QuestionPersonalityResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true) userId: UserId,
  firstName: Option[String],
  lastName: Option[String],
  politicalParty: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png") avatarUrl: Option[String],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = Gender.swaggerAllowableValues) gender: Option[
    String
  ]
)

object QuestionPersonalityResponse {
  implicit val decoder: Decoder[QuestionPersonalityResponse] = deriveDecoder[QuestionPersonalityResponse]
  implicit val encoder: Encoder[QuestionPersonalityResponse] = deriveEncoder[QuestionPersonalityResponse]
}

@ApiModel
final case class QuestionTopIdeaWithAvatarResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: TopIdeaId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  ideaId: IdeaId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  name: String,
  label: String,
  scores: TopIdeaScores,
  proposalsCount: Int,
  avatars: Seq[String],
  weight: Float,
  commentsCount: Int
)

object QuestionTopIdeaWithAvatarResponse {
  implicit val encoder: Encoder[QuestionTopIdeaWithAvatarResponse] = deriveEncoder[QuestionTopIdeaWithAvatarResponse]
  implicit val decoder: Decoder[QuestionTopIdeaWithAvatarResponse] = deriveDecoder[QuestionTopIdeaWithAvatarResponse]
}

final case class QuestionTopIdeaCommentsPersonalityResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  personalityId: UserId,
  displayName: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png")
  avatarUrl: Option[String],
  politicalParty: Option[String]
)

object QuestionTopIdeaCommentsPersonalityResponse {
  implicit val encoder: Encoder[QuestionTopIdeaCommentsPersonalityResponse] =
    deriveEncoder[QuestionTopIdeaCommentsPersonalityResponse]
}

final case class QuestionTopIdeaCommentsResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: TopIdeaCommentId,
  personality: QuestionTopIdeaCommentsPersonalityResponse,
  comment1: Option[String],
  comment2: Option[String],
  comment3: Option[String],
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = CommentVoteKey.swaggerAllowableValues,
    required = true
  )
  vote: CommentVoteKey,
  @(ApiModelProperty @field)(dataType = "string", allowableValues = CommentQualificationKey.swaggerAllowableValues)
  qualification: Option[CommentQualificationKey]
)

object QuestionTopIdeaCommentsResponse {
  implicit val encoder: Encoder[QuestionTopIdeaCommentsResponse] = deriveEncoder[QuestionTopIdeaCommentsResponse]
}

final case class QuestionTopIdeaWithAvatarAndCommentsResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: TopIdeaId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  ideaId: IdeaId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  name: String,
  label: String,
  scores: TopIdeaScores,
  proposalsCount: Int,
  avatars: Seq[String],
  weight: Float,
  comments: Seq[QuestionTopIdeaCommentsResponse]
)

object QuestionTopIdeaWithAvatarAndCommentsResponse {
  implicit val encoder: Encoder[QuestionTopIdeaWithAvatarAndCommentsResponse] =
    deriveEncoder[QuestionTopIdeaWithAvatarAndCommentsResponse]
}

final case class QuestionTopIdeasResponseWithSeed(questionTopIdeas: Seq[QuestionTopIdeaWithAvatarResponse], seed: Int)

object QuestionTopIdeasResponseWithSeed {
  implicit val encoder: Encoder[QuestionTopIdeasResponseWithSeed] = deriveEncoder[QuestionTopIdeasResponseWithSeed]
}

final case class QuestionTopIdeaResponseWithSeed(
  questionTopIdea: QuestionTopIdeaWithAvatarAndCommentsResponse,
  seed: Int
)

object QuestionTopIdeaResponseWithSeed {
  implicit val encoder: Encoder[QuestionTopIdeaResponseWithSeed] = deriveEncoder[QuestionTopIdeaResponseWithSeed]
}

final case class QuestionListResponse(results: Seq[QuestionOfOperationResponse], total: Long)

object QuestionListResponse {
  implicit val codec: Codec[QuestionListResponse] = deriveCodec[QuestionListResponse]
}

final case class SearchQuestionsResponse(total: Long, results: Seq[SearchQuestionResponse])

object SearchQuestionsResponse {
  implicit val codec: Codec[SearchQuestionsResponse] = deriveCodec
}

final case class SearchQuestionResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  preferredLanguage: Option[Language],
  returnedLanguage: Language,
  question: String Refined NonEmpty,
  slug: String,
  questionShortTitle: Option[String Refined NonEmpty],
  startDate: ZonedDateTime,
  endDate: ZonedDateTime,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = OperationOfQuestion.Status.swaggerAllowableValues
  )
  status: OperationOfQuestion.Status,
  theme: QuestionTheme,
  description: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/consultation-image.png")
  consultationImage: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "consultation image alternative")
  consultationImageAlt: Option[String Refined MaxSize[130]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/description-image.png")
  descriptionImage: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "description image alternative")
  descriptionImageAlt: Option[String Refined MaxSize[130]],
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  countries: NonEmptyList[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  languages: NonEmptyList[Language],
  defaultLanguage: Language,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  operationId: OperationId,
  operationTitle: String,
  operationKind: String,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/about")
  aboutUrl: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/results")
  resultsLink: Option[String],
  proposalsCount: Int,
  participantsCount: Int,
  votesCount: Int,
  actions: Option[String],
  featured: Boolean,
  @(ApiModelProperty @field)(dataType = "double") top20ConsensusThreshold: Option[Double]
)

object SearchQuestionResponse extends CirceFormatters {
  implicit val codec: Codec[SearchQuestionResponse] = deriveCodec

  def apply(
    indexedOperationOfQuestion: IndexedOperationOfQuestion,
    preferredLanguage: Option[Language]
  ): SearchQuestionResponse = {
    val returnedLanguage = preferredLanguage.fold(indexedOperationOfQuestion.defaultLanguage)(
      lang =>
        indexedOperationOfQuestion.languages
          .find(language => language.value == lang.value)
          .getOrElse(indexedOperationOfQuestion.defaultLanguage)
    )
    SearchQuestionResponse(
      questionId = indexedOperationOfQuestion.questionId,
      preferredLanguage = preferredLanguage,
      returnedLanguage = returnedLanguage,
      question = indexedOperationOfQuestion.questions.getTranslationUnsafe(returnedLanguage),
      slug = indexedOperationOfQuestion.slug,
      questionShortTitle = indexedOperationOfQuestion.questionShortTitles.flatMap(_.getTranslation(returnedLanguage)),
      startDate = indexedOperationOfQuestion.startDate,
      endDate = indexedOperationOfQuestion.endDate,
      status = indexedOperationOfQuestion.status,
      theme = indexedOperationOfQuestion.theme,
      description = indexedOperationOfQuestion.descriptions.flatMap(_.getTranslation(returnedLanguage)),
      consultationImage = indexedOperationOfQuestion.consultationImages.flatMap(_.getTranslation(returnedLanguage)),
      consultationImageAlt =
        indexedOperationOfQuestion.consultationImageAlts.flatMap(_.getTranslation(returnedLanguage)),
      descriptionImage = indexedOperationOfQuestion.descriptionImages.flatMap(_.getTranslation(returnedLanguage)),
      descriptionImageAlt = indexedOperationOfQuestion.descriptionImageAlts.flatMap(_.getTranslation(returnedLanguage)),
      countries = indexedOperationOfQuestion.countries,
      languages = indexedOperationOfQuestion.languages,
      defaultLanguage = indexedOperationOfQuestion.defaultLanguage,
      operationId = indexedOperationOfQuestion.operationId,
      operationTitle = indexedOperationOfQuestion.operationTitles.getTranslationUnsafe(returnedLanguage),
      operationKind = indexedOperationOfQuestion.operationKind,
      aboutUrl = indexedOperationOfQuestion.aboutUrls.flatMap(_.getTranslation(returnedLanguage)),
      resultsLink = indexedOperationOfQuestion.resultsLink,
      proposalsCount = indexedOperationOfQuestion.proposalsCount,
      participantsCount = indexedOperationOfQuestion.participantsCount,
      votesCount = indexedOperationOfQuestion.votesCount,
      actions = indexedOperationOfQuestion.actions,
      featured = indexedOperationOfQuestion.featured,
      top20ConsensusThreshold = indexedOperationOfQuestion.top20ConsensusThreshold
    )
  }
}
