/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.question

import cats.data.NonEmptyList
import eu.timepit.refined.auto._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection._
import io.circe.generic.semiauto.deriveCodec
import io.circe.Codec
import io.swagger.annotations.ApiModelProperty
import io.circe.refined._
import org.make.api.operation.ResultsLinkResponse
import org.make.api.proposal.ProposalResponse
import org.make.core.technical.Multilingual
import org.make.core.feature.FeatureSlug
import org.make.core.operation._
import org.make.core.operation.indexed.IndexedOperationOfQuestion
import org.make.core.partner.{Partner, PartnerKind}
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import org.make.core.user.{User, UserId}
import org.make.core.{CirceFormatters, SlugHelper}

import java.time.{LocalDate, ZonedDateTime}
import scala.annotation.meta.field

final case class ModerationQuestionResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: QuestionId,
  slug: String,
  questions: Multilingual[String Refined NonEmpty],
  shortTitles: Option[Multilingual[String Refined NonEmpty]],
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  countries: NonEmptyList[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  languages: NonEmptyList[Language]
)

object ModerationQuestionResponse {

  def apply(question: Question): ModerationQuestionResponse = ModerationQuestionResponse(
    id = question.questionId,
    slug = question.slug,
    questions = question.questions,
    shortTitles = question.shortTitles,
    countries = question.countries,
    languages = question.languages
  )

  implicit val codec: Codec[ModerationQuestionResponse] = deriveCodec
}

final case class MetasResponse(
  title: Option[String],
  description: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/picture.png")
  picture: Option[String]
)

object MetasResponse {
  implicit val codec: Codec[MetasResponse] = deriveCodec

  def apply(metas: Metas, returnedLanguage: Language): MetasResponse =
    MetasResponse(
      title = metas.titles.map(_.getTranslationUnsafe(returnedLanguage)),
      description = metas.descriptions.map(_.getTranslationUnsafe(returnedLanguage)),
      picture = metas.picture
    )
}

final case class WordingResponse(
  title: String,
  question: String Refined NonEmpty,
  description: Option[String],
  metas: MetasResponse
)

object WordingResponse {
  implicit val codec: Codec[WordingResponse] = deriveCodec
}

final case class IntroCardResponse(enabled: Boolean, title: Option[String], description: Option[String])

object IntroCardResponse extends CirceFormatters {
  implicit val codec: Codec[IntroCardResponse] = deriveCodec
}

final case class PushProposalCardResponse(enabled: Boolean)

object PushProposalCardResponse extends CirceFormatters {
  implicit val codec: Codec[PushProposalCardResponse] = deriveCodec
}

final case class FinalCardResponse(
  enabled: Boolean,
  withSharing: Boolean,
  title: Option[String],
  share: Option[String],
  learnMoreTitle: Option[String],
  learnMoreTextButton: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/link")
  linkUrl: Option[String]
)

object FinalCardResponse extends CirceFormatters {
  implicit val codec: Codec[FinalCardResponse] = deriveCodec
}

final case class SequenceCardsConfigurationResponse(
  introCard: IntroCardResponse,
  pushProposalCard: PushProposalCardResponse,
  finalCard: FinalCardResponse
)

object SequenceCardsConfigurationResponse extends CirceFormatters {
  implicit val codec: Codec[SequenceCardsConfigurationResponse] = deriveCodec

  def apply(
    sequenceCardConfiguration: SequenceCardsConfiguration,
    returnedLanguage: Language
  ): SequenceCardsConfigurationResponse = {
    SequenceCardsConfigurationResponse(
      introCard = IntroCardResponse(
        enabled = sequenceCardConfiguration.introCard.enabled,
        title = sequenceCardConfiguration.introCard.titles.flatMap(_.getTranslation(returnedLanguage)),
        description = sequenceCardConfiguration.introCard.descriptions.flatMap(_.getTranslation(returnedLanguage))
      ),
      pushProposalCard = PushProposalCardResponse(enabled = sequenceCardConfiguration.pushProposalCard.enabled),
      finalCard = FinalCardResponse(
        enabled = sequenceCardConfiguration.finalCard.enabled,
        withSharing = sequenceCardConfiguration.finalCard.sharingEnabled,
        title = sequenceCardConfiguration.finalCard.titles.flatMap(_.getTranslation(returnedLanguage)),
        share = sequenceCardConfiguration.finalCard.shareDescriptions.flatMap(_.getTranslation(returnedLanguage)),
        learnMoreTitle = sequenceCardConfiguration.finalCard.learnMoreTitles.flatMap(_.getTranslation(returnedLanguage)),
        learnMoreTextButton =
          sequenceCardConfiguration.finalCard.learnMoreTextButtons.flatMap(_.getTranslation(returnedLanguage)),
        linkUrl = sequenceCardConfiguration.finalCard.linkUrl
      )
    )
  }
}

final case class OrganisationPartnerResponse(organisationId: UserId, slug: String)

object OrganisationPartnerResponse {
  implicit val codec: Codec[OrganisationPartnerResponse] = deriveCodec
}

final case class QuestionPartnerResponse(
  name: String,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/logo.png")
  logo: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/link")
  link: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  organisation: Option[OrganisationPartnerResponse],
  @(ApiModelProperty @field)(dataType = "string", required = true, allowableValues = PartnerKind.swaggerAllowableValues)
  partnerKind: PartnerKind,
  weight: Float
)

object QuestionPartnerResponse extends CirceFormatters {
  def apply(partner: Partner, organisation: Option[User]): QuestionPartnerResponse = QuestionPartnerResponse(
    name = partner.name,
    logo = partner.logo,
    link = partner.link,
    organisation = organisation.map { orga =>
      OrganisationPartnerResponse(
        organisationId = orga.userId,
        slug = orga.organisationName.map(SlugHelper.apply).getOrElse("")
      )
    },
    partnerKind = partner.partnerKind,
    weight = partner.weight
  )

  implicit val codec: Codec[QuestionPartnerResponse] = deriveCodec
}

final case class OperationOfQuestionHighlightsResponse(
  votesTarget: Int,
  votesCount: Int,
  participantsCount: Int,
  proposalsCount: Int
)

object OperationOfQuestionHighlightsResponse {
  implicit val codec: Codec[OperationOfQuestionHighlightsResponse] = deriveCodec
}

final case class TimelineResponse(
  action: Option[TimelineElementResponse],
  result: Option[TimelineElementResponse],
  workshop: Option[TimelineElementResponse]
)

object TimelineResponse extends CirceFormatters {
  implicit val codec: Codec[TimelineResponse] = deriveCodec
}

final case class TimelineElementResponse(date: LocalDate, dateText: String, description: String)

object TimelineElementResponse extends CirceFormatters {
  implicit val codec: Codec[TimelineElementResponse] = deriveCodec
}

final case class QuestionDetailsResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  operationId: OperationId,
  preferredLanguage: Option[Language],
  returnedLanguage: Language,
  wording: WordingResponse,
  question: String Refined NonEmpty,
  shortTitle: Option[String Refined NonEmpty],
  slug: String,
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  countries: NonEmptyList[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  languages: NonEmptyList[Language],
  startDate: ZonedDateTime,
  endDate: ZonedDateTime,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  canPropose: Boolean,
  proposalPrefix: String,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = OperationKind.swaggerAllowableValues
  )
  operationKind: OperationKind,
  sequenceConfig: SequenceCardsConfigurationResponse,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/about")
  aboutUrl: Option[String],
  partners: Seq[QuestionPartnerResponse],
  theme: QuestionThemeResponse,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/consultation.png")
  consultationImage: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "consultation alternative")
  consultationImageAlt: Option[String Refined MaxSize[130]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/description.png")
  descriptionImage: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "description alternative")
  descriptionImageAlt: Option[String Refined MaxSize[130]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/cobranding-logo.png")
  cobrandingLogo: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "cobranding logo alternative")
  cobrandingLogoAlt: Option[String Refined MaxSize[130]],
  displayResults: Boolean,
  reportUrl: Option[String],
  actionsUrl: Option[String],
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  activeFeatures: Seq[FeatureSlug],
  featured: Boolean,
  highlights: OperationOfQuestionHighlightsResponse,
  timeline: TimelineResponse,
  controversyCount: Long,
  topProposalCount: Long,
  activeFeatureData: ActiveFeatureData,
  @Deprecated hasDemographics: Boolean,
  demographicsCardCount: Int
)

object QuestionDetailsResponse extends CirceFormatters {
  def apply(
    question: Question,
    operation: Operation,
    operationOfQuestion: OperationOfQuestion,
    partners: Seq[Partner],
    organisations: Seq[User],
    activeFeatures: Seq[FeatureSlug],
    controversyCount: Long,
    topProposalCount: Long,
    activeFeatureData: ActiveFeatureData,
    demographicsCardCount: Int,
    preferredLanguage: Option[Language],
    returnedLanguage: Language
  ): QuestionDetailsResponse = QuestionDetailsResponse(
    questionId = question.questionId,
    operationId = operation.operationId,
    preferredLanguage = preferredLanguage,
    returnedLanguage = returnedLanguage,
    wording = WordingResponse(
      title = operationOfQuestion.operationTitles.getTranslationUnsafe(returnedLanguage),
      question = question.questions.getTranslationUnsafe(returnedLanguage),
      description = operationOfQuestion.descriptions.flatMap(_.getTranslation(returnedLanguage)),
      metas = MetasResponse(operationOfQuestion.metas, returnedLanguage)
    ),
    question = question.questions.getTranslationUnsafe(returnedLanguage),
    shortTitle = question.shortTitles.flatMap(_.getTranslation(returnedLanguage)),
    slug = question.slug,
    countries = question.countries,
    languages = question.languages,
    startDate = operationOfQuestion.startDate,
    endDate = operationOfQuestion.endDate,
    canPropose = operationOfQuestion.canPropose,
    proposalPrefix = operationOfQuestion.proposalPrefixes.getTranslationUnsafe(returnedLanguage),
    operationKind = operation.operationKind,
    sequenceConfig =
      SequenceCardsConfigurationResponse.apply(operationOfQuestion.sequenceCardsConfiguration, returnedLanguage),
    aboutUrl = operationOfQuestion.aboutUrls.flatMap(_.getTranslation(returnedLanguage)),
    partners =
      partners.map(p => QuestionPartnerResponse(p, organisations.find(o => p.organisationId.contains(o.userId)))),
    theme = QuestionThemeResponse.fromQuestionTheme(operationOfQuestion.theme),
    consultationImage = operationOfQuestion.consultationImages.flatMap(_.getTranslation(returnedLanguage)),
    consultationImageAlt = operationOfQuestion.consultationImageAlts.flatMap(_.getTranslation(returnedLanguage)),
    descriptionImage = operationOfQuestion.descriptionImages.flatMap(_.getTranslation(returnedLanguage)),
    descriptionImageAlt = operationOfQuestion.descriptionImageAlts.flatMap(_.getTranslation(returnedLanguage)),
    cobrandingLogo = operationOfQuestion.cobrandingLogo,
    cobrandingLogoAlt = operationOfQuestion.cobrandingLogoAlt,
    displayResults = operationOfQuestion.resultsLink.isDefined,
    reportUrl = operationOfQuestion.reportUrl,
    actionsUrl = operationOfQuestion.actionsUrl,
    activeFeatures = activeFeatures,
    featured = operationOfQuestion.featured,
    highlights = OperationOfQuestionHighlightsResponse(
      votesTarget = operationOfQuestion.votesTarget,
      votesCount = operationOfQuestion.votesCount,
      participantsCount = operationOfQuestion.participantsCount,
      proposalsCount = operationOfQuestion.proposalsCount
    ),
    timeline = TimelineResponse(
      action = operationOfQuestion.timeline.action.map(
        action =>
          TimelineElementResponse(
            action.date,
            action.dateTexts.getTranslation(returnedLanguage).fold("")(_.value),
            action.descriptions.getTranslation(returnedLanguage).fold("")(_.value)
          )
      ),
      result = operationOfQuestion.timeline.result.map(
        result =>
          TimelineElementResponse(
            result.date,
            result.dateTexts.getTranslation(returnedLanguage).fold("")(_.value),
            result.descriptions.getTranslation(returnedLanguage).fold("")(_.value)
          )
      ),
      workshop = operationOfQuestion.timeline.workshop.map(
        workshop =>
          TimelineElementResponse(
            workshop.date,
            workshop.dateTexts.getTranslation(returnedLanguage).fold("")(_.value),
            workshop.descriptions.getTranslation(returnedLanguage).fold("")(_.value)
          )
      )
    ),
    controversyCount = controversyCount,
    topProposalCount = topProposalCount,
    activeFeatureData = activeFeatureData,
    hasDemographics = demographicsCardCount > 0,
    demographicsCardCount = demographicsCardCount
  )

  implicit val codec: Codec[QuestionDetailsResponse] = deriveCodec
}

final case class QuestionOfOperationResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  questionSlug: String,
  preferredLanguage: Option[Language],
  returnedLanguage: Language,
  question: String Refined NonEmpty,
  shortTitle: Option[String Refined NonEmpty],
  operationTitle: String,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/consultation.png")
  consultationImage: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "consultation image alternative")
  consultationImageAlt: Option[String Refined MaxSize[130]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/description.png")
  descriptionImage: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "description image alternative")
  descriptionImageAlt: Option[String Refined MaxSize[130]],
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  countries: NonEmptyList[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  languages: NonEmptyList[Language],
  startDate: ZonedDateTime,
  endDate: ZonedDateTime,
  theme: QuestionThemeResponse,
  displayResults: Boolean,
  resultsLink: Option[ResultsLinkResponse],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/about")
  aboutUrl: Option[String],
  actions: Option[String],
  featured: Boolean,
  @Deprecated participantsCount: Int,
  proposalsCount: Int,
  votesCount: Int
)

object QuestionOfOperationResponse extends CirceFormatters {
  def apply(
    indexedOperationOfQuestion: IndexedOperationOfQuestion,
    preferredLanguage: Option[Language]
  ): QuestionOfOperationResponse = {
    val returnedLanguage = preferredLanguage.fold(indexedOperationOfQuestion.defaultLanguage)(
      lang =>
        indexedOperationOfQuestion.languages
          .find(language => language.value == lang.value)
          .getOrElse(indexedOperationOfQuestion.defaultLanguage)
    )
    QuestionOfOperationResponse(
      questionId = indexedOperationOfQuestion.questionId,
      questionSlug = indexedOperationOfQuestion.slug,
      preferredLanguage = preferredLanguage,
      returnedLanguage = returnedLanguage,
      question = indexedOperationOfQuestion.questions.getTranslationUnsafe(returnedLanguage),
      shortTitle = indexedOperationOfQuestion.questionShortTitles.flatMap(_.getTranslation(returnedLanguage)),
      operationTitle = indexedOperationOfQuestion.operationTitles.getTranslationUnsafe(returnedLanguage),
      consultationImage = indexedOperationOfQuestion.consultationImages.flatMap(_.getTranslation(returnedLanguage)),
      consultationImageAlt =
        indexedOperationOfQuestion.consultationImageAlts.flatMap(_.getTranslation(returnedLanguage)),
      descriptionImage = indexedOperationOfQuestion.descriptionImages.flatMap(_.getTranslation(returnedLanguage)),
      descriptionImageAlt = indexedOperationOfQuestion.descriptionImageAlts.flatMap(_.getTranslation(returnedLanguage)),
      countries = indexedOperationOfQuestion.countries,
      languages = indexedOperationOfQuestion.languages,
      startDate = indexedOperationOfQuestion.startDate,
      endDate = indexedOperationOfQuestion.endDate,
      theme = QuestionThemeResponse.fromQuestionTheme(indexedOperationOfQuestion.theme),
      displayResults = indexedOperationOfQuestion.resultsLink.isDefined,
      resultsLink = indexedOperationOfQuestion.resultsLink
        .flatMap(ResultsLink.parse)
        .map(ResultsLinkResponse.apply),
      aboutUrl = indexedOperationOfQuestion.aboutUrls.flatMap(_.getTranslation(returnedLanguage)),
      actions = indexedOperationOfQuestion.actions,
      featured = indexedOperationOfQuestion.featured,
      participantsCount = indexedOperationOfQuestion.participantsCount,
      proposalsCount = indexedOperationOfQuestion.proposalsCount,
      votesCount = indexedOperationOfQuestion.votesCount
    )
  }

  implicit val codec: Codec[QuestionOfOperationResponse] = deriveCodec[QuestionOfOperationResponse]
}

final case class QuestionThemeResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "#214284", required = true) gradientStart: String,
  @(ApiModelProperty @field)(dataType = "string", example = "#428421", required = true) gradientEnd: String,
  @(ApiModelProperty @field)(dataType = "string", example = "#842142", required = true) color: String,
  @(ApiModelProperty @field)(dataType = "string", example = "#ff0000", required = true) fontColor: String
)

object QuestionThemeResponse {
  def fromQuestionTheme(theme: QuestionTheme): QuestionThemeResponse = {
    QuestionThemeResponse(
      gradientStart = theme.color,
      gradientEnd = theme.color,
      color = theme.color,
      fontColor = theme.fontColor
    )
  }

  implicit val codec: Codec[QuestionThemeResponse] = deriveCodec
}

final case class ActiveFeatureData(topProposal: Option[ProposalResponse])

object ActiveFeatureData {
  implicit val codec: Codec[ActiveFeatureData] = deriveCodec
}
