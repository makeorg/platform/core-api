/*
 *  Make.org Core API
 *  Copyright (C) 2021 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.demographics

import cats.data.NonEmptyList
import io.circe.generic.semiauto.deriveEncoder
import io.circe.Encoder
import io.swagger.annotations.ApiModelProperty
import org.make.core.Order
import org.make.core.technical.Multilingual
import org.make.core.demographics.{DemographicsCard, DemographicsCardId, LabelValue, LabelsValue}
import org.make.core.demographics.DemographicsCard.Layout
import org.make.core.question.QuestionId
import org.make.core.reference.Language
import org.make.core.technical.Pagination

import scala.annotation.meta.field
import scala.concurrent.Future

trait DemographicsCardService {
  def get(id: DemographicsCardId): Future[Option[DemographicsCard]]

  def list(
    offset: Option[Pagination.Offset],
    end: Option[Pagination.End],
    sort: Option[String],
    order: Option[Order],
    languages: Option[List[Language]],
    dataType: Option[String],
    sessionBindingMode: Option[Boolean]
  ): Future[Seq[DemographicsCard]]

  def create(
    name: String,
    layout: Layout,
    dataType: String,
    languages: NonEmptyList[Language],
    titles: Multilingual[String],
    sessionBindingMode: Boolean,
    parameters: NonEmptyList[LabelsValue]
  ): Future[DemographicsCard]

  def update(
    id: DemographicsCardId,
    name: String,
    layout: Layout,
    dataType: String,
    languages: NonEmptyList[Language],
    titles: Multilingual[String],
    sessionBindingMode: Boolean,
    parameters: NonEmptyList[LabelsValue]
  ): Future[Option[DemographicsCard]]
  def count(
    languages: Option[List[Language]],
    dataType: Option[String],
    sessionBindingMode: Option[Boolean]
  ): Future[Int]
  def generateToken(id: DemographicsCardId, questionId: QuestionId): String

  def isTokenValid(token: String, id: DemographicsCardId, questionId: QuestionId): Boolean

  def getOneRandomCardByQuestion(questionId: QuestionId): Future[Option[DemographicsCardResponse]]

  def getOrPickRandom(
    maybeId: Option[DemographicsCardId],
    maybeToken: Option[String],
    questionId: QuestionId
  ): Future[Option[DemographicsCardResponse]]
}

trait DemographicsCardServiceComponent {
  def demographicsCardService: DemographicsCardService
}

final case class DemographicsCardForSequence(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: DemographicsCardId,
  name: String,
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = DemographicsCard.Layout.swaggerAllowableValues,
    required = true
  )
  layout: DemographicsCard.Layout,
  title: String,
  @(ApiModelProperty @field)(dataType = "object", required = true)
  parameters: NonEmptyList[LabelValue],
  token: String
)

object DemographicsCardForSequence {
  implicit val encoder: Encoder[DemographicsCardForSequence] = deriveEncoder

  def apply(card: DemographicsCardResponse, language: Language): DemographicsCardForSequence =
    DemographicsCardForSequence(
      id = card.id,
      name = card.name,
      layout = card.layout,
      title = card.titles.getTranslationUnsafe(language),
      parameters = card.parameters.map(lv => LabelValue(lv.label.getTranslationUnsafe(language), lv.value)),
      token = card.token
    )
}

final case class DemographicsCardResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: DemographicsCardId,
  name: String,
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = DemographicsCard.Layout.swaggerAllowableValues,
    required = true
  )
  layout: DemographicsCard.Layout,
  titles: Multilingual[String],
  @(ApiModelProperty @field)(dataType = "object", required = true)
  parameters: NonEmptyList[LabelsValue],
  token: String
)

object DemographicsCardResponse {
  implicit val encoder: Encoder[DemographicsCardResponse] = deriveEncoder[DemographicsCardResponse]

  def apply(card: DemographicsCard, token: String): DemographicsCardResponse =
    DemographicsCardResponse(
      id = card.id,
      name = card.name,
      layout = card.layout,
      titles = card.titles,
      parameters = card.parameters,
      token = token
    )
}
