/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.operation

import java.time.ZonedDateTime
import cats.data.NonEmptyList
import cats.implicits._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._
import eu.timepit.refined.collection._
import org.make.api.question.DefaultPersistentQuestionServiceComponent
import org.make.api.tag.DefaultPersistentTagServiceComponent
import org.make.api.technical.DefaultIdGeneratorComponent
import org.make.api.user.DefaultPersistentUserServiceComponent
import org.make.api.{DatabaseTest, TestUtils}
import org.make.core.{DateHelper, Order}
import org.make.core.technical.Multilingual
import org.make.core.operation._
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import org.make.core.tag.{Tag, TagDisplay, TagType}
import org.make.core.user.{Role, User, UserId}
import org.scalatest.concurrent.PatienceConfiguration.Timeout

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import org.make.core.technical.Pagination

class PersistentOperationServiceIT
    extends DatabaseTest
    with DefaultPersistentOperationServiceComponent
    with DefaultPersistentUserServiceComponent
    with DefaultPersistentTagServiceComponent
    with DefaultPersistentQuestionServiceComponent
    with DefaultPersistentOperationOfQuestionServiceComponent
    with DefaultIdGeneratorComponent {

  val userId: UserId = idGenerator.nextUserId()
  val johnDoe: User = TestUtils.user(
    id = userId,
    email = "doe@example.com",
    firstName = Some("John"),
    lastName = Some("Doe"),
    lastIp = Some("0.0.0.0"),
    hashedPassword = Some("ZAEAZE232323SFSSDF"),
    lastConnection = Some(ZonedDateTime.parse("2017-06-01T12:30:40Z")),
    verificationToken = Some("VERIFTOKEN"),
    verificationTokenExpiresAt = Some(ZonedDateTime.parse("2017-06-01T12:30:40Z")),
    roles = Seq(Role.RoleAdmin, Role.RoleCitizen),
    profile = None
  )

  def newTag(label: String): Tag = Tag(
    tagId = idGenerator.nextTagId(),
    label = label,
    display = TagDisplay.Inherit,
    weight = 0f,
    tagTypeId = TagType.LEGACY.tagTypeId,
    operationId = None,
    questionId = None
  )

  val stark: Tag = newTag("Stark")
  val targaryen: Tag = newTag("Targaryen")
  val bolton: Tag = newTag("Bolton")
  val greyjoy: Tag = newTag("Greyjoy")
  val now: ZonedDateTime = DateHelper.now()
  val operationId: OperationId = idGenerator.nextOperationId()

  val fullOperation: Operation = Operation(
    operationId = operationId,
    createdAt = None,
    updatedAt = None,
    slug = "hello-operation",
    operationKind = OperationKind.BusinessConsultation,
    events = List(
      OperationAction(
        date = now,
        makeUserId = userId,
        actionType = OperationActionType.OperationCreateAction.value,
        arguments = Map("arg1" -> "valueArg1")
      )
    ),
    questions = Seq(
      QuestionWithDetails(
        question = Question(
          questionId = QuestionId("some-question"),
          countries = NonEmptyList.of(Country("FR")),
          defaultLanguage = Language("fr"),
          languages = NonEmptyList.of(Language("fr")),
          slug = "hello-fr",
          questions = Multilingual.fromDefault("ça va ?"),
          shortTitles = None,
          operationId = Some(operationId)
        ),
        details = operationOfQuestion(
          questionId = QuestionId("some-question"),
          operationId = operationId,
          startDate = ZonedDateTime.parse("1968-07-03T00:00:00.000Z"),
          endDate = ZonedDateTime.parse("2068-07-03T00:00:00.000Z"),
          operationTitles = Multilingual.fromDefault("bonjour operation"),
          sequenceCardsConfiguration = SequenceCardsConfiguration(
            introCard = IntroCard(enabled = true, titles = None, descriptions = None),
            pushProposalCard = PushProposalCard(enabled = true),
            finalCard = FinalCard(
              enabled = true,
              sharingEnabled = false,
              titles = None,
              shareDescriptions = None,
              learnMoreTitles = None,
              learnMoreTextButtons = None,
              linkUrl = None
            )
          ),
          aboutUrls = None,
          metas = Metas(titles = None, descriptions = None, picture = None),
          theme = QuestionTheme.default,
          descriptions = None,
          consultationImages = None,
          consultationImageAlts = None,
          descriptionImages = None,
          descriptionImageAlts = None,
          resultsLink = None,
          actions = None
        )
      ),
      QuestionWithDetails(
        question = Question(
          questionId = QuestionId("some-question-gb"),
          countries = NonEmptyList.of(Country("GB")),
          defaultLanguage = Language("en"),
          languages = NonEmptyList.of(Language("en")),
          slug = "hello-gb",
          questions = Multilingual(Language("en") -> ("how are you ?": String Refined NonEmpty)),
          shortTitles = None,
          operationId = Some(operationId)
        ),
        details = operationOfQuestion(
          questionId = QuestionId("some-question-gb"),
          operationId = operationId,
          startDate = ZonedDateTime.parse("1968-07-03T00:00:00.000Z"),
          endDate = ZonedDateTime.parse("2068-07-03T00:00:00.000Z"),
          operationTitles = Multilingual.fromDefault("hello operation"),
          sequenceCardsConfiguration = SequenceCardsConfiguration(
            introCard = IntroCard(enabled = true, titles = None, descriptions = None),
            pushProposalCard = PushProposalCard(enabled = true),
            finalCard = FinalCard(
              enabled = true,
              sharingEnabled = false,
              titles = None,
              shareDescriptions = None,
              learnMoreTitles = None,
              learnMoreTextButtons = None,
              linkUrl = None
            )
          ),
          aboutUrls = None,
          metas = Metas(titles = None, descriptions = None, picture = None),
          theme = QuestionTheme.default,
          descriptions = None,
          consultationImages = None,
          consultationImageAlts = None,
          descriptionImages = None,
          descriptionImageAlts = None,
          resultsLink = Some(ResultsLink.Internal.TopIdeas),
          actions = None
        )
      )
    )
  )

  def createQuestions(operationId: OperationId): Future[Unit] = {
    Future
      .traverse(fullOperation.questions) { question =>
        val newQuestionId = idGenerator.nextQuestionId()

        for {
          _ <- persistentQuestionService
            .persist(question.question.copy(questionId = newQuestionId, operationId = Some(operationId)))
          _ <- persistentOperationOfQuestionService.persist(
            question.details
              .copy(operationId = operationId, questionId = newQuestionId)
          )
        } yield {}
      }
      .void
  }

  Feature("An operation can be persisted") {
    Scenario("Persist an operation and get the persisted operation") {
      Given("""
           |an operation with
           |status = Pending
           |slug = "hello-operation"
           |kind = "BUSINESS_CONSULTATION"
           |""".stripMargin)
      When("""I persist it""")
      And("I get the persisted operation")

      val simpleOperation = SimpleOperation(
        operationId = fullOperation.operationId,
        slug = fullOperation.slug,
        operationKind = OperationKind.BusinessConsultation,
        createdAt = None,
        updatedAt = None
      )

      val futureOperations: Future[Seq[Operation]] = for {
        _         <- persistentUserService.persist(johnDoe)
        operation <- persistentOperationService.persist(simpleOperation)
        _ <- persistentOperationService.addActionToOperation(
          operationId = simpleOperation.operationId,
          action = OperationAction(
            date = now,
            makeUserId = johnDoe.userId,
            actionType = "create",
            arguments = Map("arg1" -> "valueArg1")
          )
        )
        _      <- createQuestions(operation.operationId)
        result <- persistentOperationService.find(slug = Some(simpleOperation.slug))
      } yield result

      whenReady(futureOperations, Timeout(3.seconds)) { operations =>
        Then(s"operations should contain operation with operationId ${operationId.value}")
        val operation: Operation = operations.filter(_.slug == simpleOperation.slug).head
        And("""operation status should be Pending""")
        And("""operation slug should be "hello-operation" """)
        operation.slug should be("hello-operation")
        And("""operation kind should be "business" """)
        operation.operationKind should be(OperationKind.BusinessConsultation)
        And("""operation should have 2 questions""")
        operation.questions.size should be(2)
        And("operation events should contain a create event")
        val createEvent: OperationAction = operation.events.filter(_.actionType == "create").head
        createEvent.date.toEpochSecond should be(now.toEpochSecond)
        createEvent.makeUserId should be(userId)
        createEvent.actionType should be("create")
        createEvent.arguments should be(Map("arg1" -> "valueArg1"))
      }
    }

    Scenario("get a persisted operation by id") {

      val operationIdForGetById: OperationId = idGenerator.nextOperationId()

      val operationForGetById: SimpleOperation = SimpleOperation(
        operationId = operationIdForGetById,
        slug = "get-by-id-operation",
        operationKind = OperationKind.BusinessConsultation,
        createdAt = None,
        updatedAt = None
      )

      Given(s""" a persisted operation with id ${operationIdForGetById.value}""")
      When("i get the persisted operation by id")
      Then(" the call success")

      val futureMaybeOperation: Future[Option[Operation]] =
        persistentOperationService.persist(operation = operationForGetById).flatMap { operation =>
          persistentOperationService.getById(operation.operationId)
        }

      whenReady(futureMaybeOperation, Timeout(3.seconds)) { maybeOperation =>
        maybeOperation should be(defined)
      }
    }

    Scenario("get a persisted operation by slug") {

      val operationIdForGetBySlug: OperationId = idGenerator.nextOperationId()
      val operationForGetBySlug: SimpleOperation =
        SimpleOperation(
          operationId = operationIdForGetBySlug,
          slug = "get-by-slug-operation",
          operationKind = OperationKind.BusinessConsultation,
          createdAt = None,
          updatedAt = None
        )

      Given(s""" a persisted operation ${operationIdForGetBySlug.value} """)
      When("i get the persisted operation by slug")
      Then(" the call success")

      val futureMaybeOperation: Future[Option[Operation]] =
        persistentOperationService.persist(operation = operationForGetBySlug).flatMap { operation =>
          persistentOperationService.getBySlug(operation.slug)
        }

      whenReady(futureMaybeOperation, Timeout(3.seconds)) { maybeOperation =>
        maybeOperation should be(defined)
        maybeOperation.get.slug shouldBe "get-by-slug-operation"
      }
    }
  }

  Feature("get simple operation") {
    Scenario("simple operation from full operation") {

      val operationId = OperationId("simple-operation")
      val simpleOperation = SimpleOperation(
        operationId = operationId,
        slug = "simple-operation",
        operationKind = OperationKind.BusinessConsultation,
        createdAt = None,
        updatedAt = None
      )

      val futureSimpleOperation: Future[Option[SimpleOperation]] = for {
        _      <- persistentOperationService.persist(simpleOperation)
        result <- persistentOperationService.getSimpleById(operationId)
      } yield result

      whenReady(futureSimpleOperation, Timeout(3.seconds)) { simpleOperation =>
        simpleOperation.isDefined shouldBe true
        simpleOperation.map(_.operationId) shouldBe Some(operationId)
      }
    }

    Scenario("sorted simple operations") {

      def simpleOperation(operationId: OperationId) = SimpleOperation(
        operationId = operationId,
        slug = s"${operationId.value}-sorted-slug",
        operationKind = OperationKind.BusinessConsultation,
        createdAt = None,
        updatedAt = None
      )

//    The pipe "|" symbol is used here because it has a high ascii code thus allowing us to be determinist in the order.
      val futureSortedSimpleOperations: Future[Seq[SimpleOperation]] = for {
        _ <- persistentOperationService.persist(simpleOperation(OperationId("|BBB operation")))
        _ <- persistentOperationService.persist(simpleOperation(OperationId("|AAA operation")))
        _ <- persistentOperationService.persist(simpleOperation(OperationId("|CCC operation")))
        results <- persistentOperationService.findSimple(
          offset = Pagination.Offset(1),
          end = Some(Pagination.End(3)),
          sort = Some("uuid"),
          order = Some(Order.desc),
          operationKinds = Some(Seq(OperationKind.BusinessConsultation))
        )
      } yield results

      whenReady(futureSortedSimpleOperations, Timeout(3.seconds)) { sortedSimpleOperations =>
        sortedSimpleOperations.size shouldBe 2
        sortedSimpleOperations.map(_.operationId) shouldBe Seq(
          OperationId("|BBB operation"),
          OperationId("|AAA operation")
        )
      }
    }
  }
}
