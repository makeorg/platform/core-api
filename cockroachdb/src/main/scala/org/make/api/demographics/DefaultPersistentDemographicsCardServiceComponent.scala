/*
 *  Make.org Core API
 *  Copyright (C) 2021 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.demographics

import cats.data.NonEmptyList
import cats.implicits._
import org.make.api.extensions.MakeDBExecutionContextComponent
import org.make.api.technical.DatabaseTransactions._
import org.make.api.technical.PersistentServiceUtils._
import org.make.api.technical.ScalikeSupport._
import org.make.api.technical.{PersistentCompanion, ShortenedNames}
import org.make.core.Order
import org.make.core.demographics.{DemographicsCard, DemographicsCardId, LabelsValue}
import org.make.core.reference.Language
import org.make.core.technical.Pagination
import scalikejdbc._

import scala.concurrent.Future

trait DefaultPersistentDemographicsCardServiceComponent extends PersistentDemographicsCardServiceComponent {
  self: MakeDBExecutionContextComponent =>

  implicit val labelsBinder: Binders[NonEmptyList[LabelsValue]] = jsonNelBinder[LabelsValue]

  val CountColumnName = "count"

  override lazy val persistentDemographicsCardService: PersistentDemographicsCardService =
    new PersistentDemographicsCardService with ShortenedNames {

      private val demographicsCards = SQLSyntaxSupportFactory[DemographicsCard]()
      private val dc = demographicsCards.syntax

      private implicit val companion: PersistentCompanion[DemographicsCard, DemographicsCard] =
        new PersistentCompanion[DemographicsCard, DemographicsCard] {
          override val alias: SyntaxProvider[DemographicsCard] = dc
          override val defaultSortColumns: NonEmptyList[SQLSyntax] = NonEmptyList.of(alias.name)
          override val columnNames: Seq[String] =
            Seq("name", "layout", "dataType", "languages", "titles", "sessionBindingMode", "createdAt", "updatedAt")
        }

      override def get(id: DemographicsCardId): Future[Option[DemographicsCard]] = {
        implicit val context: EC = readExecutionContext
        Future(NamedDB("READ").retryableTx { implicit session =>
          withSQL {
            select.from(demographicsCards.as(dc)).where.eq(dc.id, id)
          }.map(demographicsCards.apply(dc.resultName))
            .single()
        })
      }

      override def list(
        offset: Option[Pagination.Offset],
        end: Option[Pagination.End],
        sort: Option[String],
        order: Option[Order],
        languages: Option[List[Language]],
        dataType: Option[String],
        sessionBindingMode: Option[Boolean]
      ): Future[Seq[DemographicsCard]] = {
        implicit val context: EC = readExecutionContext
        Future(NamedDB("READ").retryableTx { implicit session =>
          withSQL {
            sortOrderQuery(
              offset.orZero,
              end,
              sort,
              order,
              select
                .from(demographicsCards.as(dc))
                .where(
                  sqls.toAndConditionOpt(
                    languages.map(l => sqls"${dc.languages} @> ARRAY[${l.map(_.value)}]"),
                    dataType.map(sqls.like(dc.dataType, _)),
                    sessionBindingMode.map(sqls.eq(dc.sessionBindingMode, _))
                  )
                )
            )
          }.map(demographicsCards.apply(dc.resultName))
            .list()
        })
      }

      override def persist(demographicsCard: DemographicsCard): Future[DemographicsCard] = {
        implicit val context: EC = writeExecutionContext
        Future(NamedDB("WRITE").retryableTx { implicit session =>
          withSQL {
            insert.into(demographicsCards).namedValues(autoNamedValues(demographicsCard, demographicsCards.column))
          }.update()
        }).as(demographicsCard)
      }

      override def modify(demographicsCard: DemographicsCard): Future[DemographicsCard] = {
        implicit val context: EC = writeExecutionContext
        Future(NamedDB("WRITE").retryableTx { implicit session =>
          withSQL {
            update(demographicsCards)
              .set(autoNamedValues(demographicsCard, demographicsCards.column, "id", "createdAt"))
              .where
              .eq(dc.id, demographicsCard.id)
          }.update()
        }).as(demographicsCard)
      }

      override def count(
        languages: Option[List[Language]],
        dataType: Option[String],
        sessionBindingMode: Option[Boolean]
      ): Future[Int] = {
        implicit val context: EC = writeExecutionContext
        Future(NamedDB("READ").retryableTx { implicit session =>
          withSQL {
            select(sqls.count)
              .from(demographicsCards.as(dc))
              .where(
                sqls.toAndConditionOpt(
                  languages.map(l => sqls"${dc.languages} @> ARRAY[${l.map(_.value).mkString(",")}]"),
                  dataType.map(sqls.like(dc.dataType, _)),
                  sessionBindingMode.map(sqls.eq(dc.sessionBindingMode, _))
                )
              )
          }.map(_.int(CountColumnName)).single().getOrElse(0)
        })
      }
    }
}
