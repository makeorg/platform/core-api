/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.operation

import java.time.{LocalDate, ZonedDateTime}
import cats.Show
import cats.data.NonEmptyList
import cats.implicits._
import eu.timepit.refined.auto._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection._
import grizzled.slf4j.Logging
import org.make.api.extensions.MakeDBExecutionContextComponent
import org.make.api.operation.DefaultPersistentOperationOfQuestionServiceComponent.PersistentOperationOfQuestion
import org.make.api.operation.DefaultPersistentOperationOfQuestionServiceComponent.PersistentOperationOfQuestion.resultsLinkBinders
import org.make.api.operation.DefaultPersistentOperationServiceComponent.PersistentOperation
import org.make.api.question.DefaultPersistentQuestionServiceComponent.PersistentQuestion
import org.make.api.technical.DatabaseTransactions._
import org.make.api.technical.PersistentServiceUtils.sortOrderQuery
import org.make.api.technical.{PersistentCompanion, ShortenedNames}
import org.make.api.technical.ScalikeSupport._
import org.make.core.{DateHelper, Order}
import org.make.core.Validation._
import org.make.core.technical.{Multilingual, Pagination}
import org.make.core.operation._
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import scalikejdbc._

import scala.concurrent.Future

trait DefaultPersistentOperationOfQuestionServiceComponent extends PersistentOperationOfQuestionServiceComponent {
  this: MakeDBExecutionContextComponent =>

  override lazy val persistentOperationOfQuestionService: DefaultPersistentOperationOfQuestionService =
    new DefaultPersistentOperationOfQuestionService

  class DefaultPersistentOperationOfQuestionService
      extends PersistentOperationOfQuestionService
      with ShortenedNames
      with Logging {

    private val operationOfQuestionAlias = PersistentOperationOfQuestion.alias
    private val operationAlias = PersistentOperation.alias

    override def search(
      offset: Pagination.Offset,
      end: Option[Pagination.End],
      sort: Option[String],
      order: Option[Order],
      questionIds: Option[Seq[QuestionId]],
      operationIds: Option[Seq[OperationId]],
      operationKind: Option[Seq[OperationKind]],
      openAt: Option[ZonedDateTime],
      endAfter: Option[ZonedDateTime],
      slug: Option[String]
    ): Future[scala.Seq[OperationOfQuestion]] = {
      implicit val context: EC = readExecutionContext
      Future(NamedDB("READ").retryableTx { implicit session =>
        withSQL[PersistentOperationOfQuestion] {
          val query: scalikejdbc.PagingSQLBuilder[PersistentOperationOfQuestion] = select
            .from(PersistentOperationOfQuestion.as(PersistentOperationOfQuestion.alias))
            .innerJoin(PersistentOperation.as(operationAlias))
            .on(operationOfQuestionAlias.operationId, operationAlias.uuid)
            .innerJoin(PersistentQuestion.as(PersistentQuestion.alias))
            .on(operationOfQuestionAlias.questionId, PersistentQuestion.alias.questionId)
            .where(
              sqls.toAndConditionOpt(
                operationIds
                  .map(operations => sqls.in(PersistentOperationOfQuestion.alias.operationId, operations.map(_.value))),
                questionIds.map(
                  questionIds => sqls.in(PersistentOperationOfQuestion.alias.questionId, questionIds.map(_.value))
                ),
                operationKind
                  .map(operationKind => sqls.in(operationAlias.operationKind, operationKind)),
                openAt.map(
                  openAt =>
                    sqls
                      .le(PersistentOperationOfQuestion.alias.startDate, openAt)
                      .and(sqls.ge(PersistentOperationOfQuestion.alias.endDate, openAt))
                ),
                endAfter.map(end => sqls.ge(PersistentOperationOfQuestion.alias.endDate, end)),
                slug.map(s       => sqls.like(PersistentQuestion.alias.slug, s"%$s%"))
              )
            )

          sortOrderQuery(offset, end, sort, order, query)
        }.map(PersistentOperationOfQuestion(operationOfQuestionAlias.resultName)).list()
      }).map(_.map(_.toOperationOfQuestion))
    }

    override def persist(operationOfQuestion: OperationOfQuestion): Future[OperationOfQuestion] = {
      implicit val context: EC = writeExecutionContext
      Future(NamedDB("WRITE").retryableTx { implicit session =>
        withSQL {
          val now = DateHelper.now()
          insert
            .into(PersistentOperationOfQuestion)
            .namedValues(
              PersistentOperationOfQuestion.column.questionId -> operationOfQuestion.questionId,
              PersistentOperationOfQuestion.column.operationId -> operationOfQuestion.operationId,
              PersistentOperationOfQuestion.column.startDate -> operationOfQuestion.startDate,
              PersistentOperationOfQuestion.column.endDate -> operationOfQuestion.endDate,
              PersistentOperationOfQuestion.column.operationTitles -> operationOfQuestion.operationTitles,
              PersistentOperationOfQuestion.column.proposalPrefixes -> operationOfQuestion.proposalPrefixes,
              PersistentOperationOfQuestion.column.createdAt -> now,
              PersistentOperationOfQuestion.column.updatedAt -> now,
              PersistentOperationOfQuestion.column.canPropose -> operationOfQuestion.canPropose,
              PersistentOperationOfQuestion.column.introCardEnabled -> operationOfQuestion.sequenceCardsConfiguration.introCard.enabled,
              PersistentOperationOfQuestion.column.introCardTitles -> operationOfQuestion.sequenceCardsConfiguration.introCard.titles,
              PersistentOperationOfQuestion.column.introCardDescriptions -> operationOfQuestion.sequenceCardsConfiguration.introCard.descriptions,
              PersistentOperationOfQuestion.column.pushProposalCardEnabled -> operationOfQuestion.sequenceCardsConfiguration.pushProposalCard.enabled,
              PersistentOperationOfQuestion.column.finalCardEnabled -> operationOfQuestion.sequenceCardsConfiguration.finalCard.enabled,
              PersistentOperationOfQuestion.column.finalCardSharingEnabled -> operationOfQuestion.sequenceCardsConfiguration.finalCard.sharingEnabled,
              PersistentOperationOfQuestion.column.finalCardTitles -> operationOfQuestion.sequenceCardsConfiguration.finalCard.titles,
              PersistentOperationOfQuestion.column.finalCardShareDescriptions -> operationOfQuestion.sequenceCardsConfiguration.finalCard.shareDescriptions,
              PersistentOperationOfQuestion.column.finalCardLearnMoreTitles -> operationOfQuestion.sequenceCardsConfiguration.finalCard.learnMoreTitles,
              PersistentOperationOfQuestion.column.finalCardLearnMoreButtons -> operationOfQuestion.sequenceCardsConfiguration.finalCard.learnMoreTextButtons,
              PersistentOperationOfQuestion.column.finalCardLinkUrl -> operationOfQuestion.sequenceCardsConfiguration.finalCard.linkUrl,
              PersistentOperationOfQuestion.column.aboutUrls -> operationOfQuestion.aboutUrls,
              PersistentOperationOfQuestion.column.metaTitles -> operationOfQuestion.metas.titles,
              PersistentOperationOfQuestion.column.metaDescriptions -> operationOfQuestion.metas.descriptions,
              PersistentOperationOfQuestion.column.metaPicture -> operationOfQuestion.metas.picture,
              PersistentOperationOfQuestion.column.color -> operationOfQuestion.theme.color,
              PersistentOperationOfQuestion.column.fontColor -> operationOfQuestion.theme.fontColor,
              PersistentOperationOfQuestion.column.descriptions -> operationOfQuestion.descriptions,
              PersistentOperationOfQuestion.column.consultationImages -> operationOfQuestion.consultationImages,
              PersistentOperationOfQuestion.column.consultationImageAlts -> operationOfQuestion.consultationImageAlts
                .map(_.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.descriptionImages -> operationOfQuestion.descriptionImages,
              PersistentOperationOfQuestion.column.descriptionImageAlts -> operationOfQuestion.descriptionImageAlts
                .map(_.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.partnersLogos -> operationOfQuestion.partnersLogos,
              PersistentOperationOfQuestion.column.partnersLogosAlt -> operationOfQuestion.partnersLogosAlt,
              PersistentOperationOfQuestion.column.initiatorsLogos -> operationOfQuestion.initiatorsLogos,
              PersistentOperationOfQuestion.column.initiatorsLogosAlt -> operationOfQuestion.initiatorsLogosAlt,
              PersistentOperationOfQuestion.column.consultationHeader -> operationOfQuestion.consultationHeader,
              PersistentOperationOfQuestion.column.consultationHeaderAlts -> operationOfQuestion.consultationHeaderAlts
                .map(_.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.cobrandingLogo -> operationOfQuestion.cobrandingLogo,
              PersistentOperationOfQuestion.column.cobrandingLogoAlt -> operationOfQuestion.cobrandingLogoAlt
                .map(_.value),
              PersistentOperationOfQuestion.column.resultsLink -> operationOfQuestion.resultsLink,
              PersistentOperationOfQuestion.column.proposalsCount -> operationOfQuestion.proposalsCount,
              PersistentOperationOfQuestion.column.participantsCount -> operationOfQuestion.participantsCount,
              PersistentOperationOfQuestion.column.actions -> operationOfQuestion.actions,
              PersistentOperationOfQuestion.column.featured -> operationOfQuestion.featured,
              PersistentOperationOfQuestion.column.votesCount -> operationOfQuestion.votesCount,
              PersistentOperationOfQuestion.column.votesTarget -> operationOfQuestion.votesTarget,
              PersistentOperationOfQuestion.column.actionDate -> operationOfQuestion.timeline.action.map(_.date),
              PersistentOperationOfQuestion.column.actionDateTexts -> operationOfQuestion.timeline.action
                .map(_.dateTexts.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.actionDescriptions -> operationOfQuestion.timeline.action
                .map(_.descriptions.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.resultDate -> operationOfQuestion.timeline.result.map(_.date),
              PersistentOperationOfQuestion.column.resultDateTexts -> operationOfQuestion.timeline.result
                .map(_.dateTexts.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.resultDescriptions -> operationOfQuestion.timeline.result
                .map(_.descriptions.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.workshopDate -> operationOfQuestion.timeline.workshop.map(_.date),
              PersistentOperationOfQuestion.column.workshopDateTexts -> operationOfQuestion.timeline.workshop
                .map(_.dateTexts.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.workshopDescriptions -> operationOfQuestion.timeline.workshop
                .map(_.descriptions.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.sessionBindingMode -> operationOfQuestion.sessionBindingMode,
              PersistentOperationOfQuestion.column.reportUrl -> operationOfQuestion.reportUrl,
              PersistentOperationOfQuestion.column.actionsUrl -> operationOfQuestion.actionsUrl
            )
        }.execute()
      }).as(operationOfQuestion)
    }

    override def modify(operationOfQuestion: OperationOfQuestion): Future[OperationOfQuestion] = {
      implicit val context: EC = writeExecutionContext
      Future(NamedDB("WRITE").retryableTx { implicit session =>
        withSQL {
          val now = DateHelper.now()
          update(PersistentOperationOfQuestion)
            .set(
              PersistentOperationOfQuestion.column.startDate -> operationOfQuestion.startDate,
              PersistentOperationOfQuestion.column.endDate -> operationOfQuestion.endDate,
              PersistentOperationOfQuestion.column.operationTitles -> operationOfQuestion.operationTitles,
              PersistentOperationOfQuestion.column.proposalPrefixes -> operationOfQuestion.proposalPrefixes,
              PersistentOperationOfQuestion.column.updatedAt -> now,
              PersistentOperationOfQuestion.column.canPropose -> operationOfQuestion.canPropose,
              PersistentOperationOfQuestion.column.introCardEnabled -> operationOfQuestion.sequenceCardsConfiguration.introCard.enabled,
              PersistentOperationOfQuestion.column.introCardTitles -> operationOfQuestion.sequenceCardsConfiguration.introCard.titles,
              PersistentOperationOfQuestion.column.introCardDescriptions -> operationOfQuestion.sequenceCardsConfiguration.introCard.descriptions,
              PersistentOperationOfQuestion.column.pushProposalCardEnabled -> operationOfQuestion.sequenceCardsConfiguration.pushProposalCard.enabled,
              PersistentOperationOfQuestion.column.finalCardEnabled -> operationOfQuestion.sequenceCardsConfiguration.finalCard.enabled,
              PersistentOperationOfQuestion.column.finalCardSharingEnabled -> operationOfQuestion.sequenceCardsConfiguration.finalCard.sharingEnabled,
              PersistentOperationOfQuestion.column.finalCardTitles -> operationOfQuestion.sequenceCardsConfiguration.finalCard.titles,
              PersistentOperationOfQuestion.column.finalCardShareDescriptions -> operationOfQuestion.sequenceCardsConfiguration.finalCard.shareDescriptions,
              PersistentOperationOfQuestion.column.finalCardLearnMoreTitles -> operationOfQuestion.sequenceCardsConfiguration.finalCard.learnMoreTitles,
              PersistentOperationOfQuestion.column.finalCardLearnMoreButtons -> operationOfQuestion.sequenceCardsConfiguration.finalCard.learnMoreTextButtons,
              PersistentOperationOfQuestion.column.finalCardLinkUrl -> operationOfQuestion.sequenceCardsConfiguration.finalCard.linkUrl,
              PersistentOperationOfQuestion.column.aboutUrls -> operationOfQuestion.aboutUrls,
              PersistentOperationOfQuestion.column.metaTitles -> operationOfQuestion.metas.titles,
              PersistentOperationOfQuestion.column.metaDescriptions -> operationOfQuestion.metas.descriptions,
              PersistentOperationOfQuestion.column.metaPicture -> operationOfQuestion.metas.picture,
              PersistentOperationOfQuestion.column.color -> operationOfQuestion.theme.color,
              PersistentOperationOfQuestion.column.fontColor -> operationOfQuestion.theme.fontColor,
              PersistentOperationOfQuestion.column.descriptions -> operationOfQuestion.descriptions,
              PersistentOperationOfQuestion.column.consultationImages -> operationOfQuestion.consultationImages,
              PersistentOperationOfQuestion.column.consultationImageAlts -> operationOfQuestion.consultationImageAlts
                .map(_.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.descriptionImages -> operationOfQuestion.descriptionImages,
              PersistentOperationOfQuestion.column.descriptionImageAlts -> operationOfQuestion.descriptionImageAlts
                .map(_.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.partnersLogos -> operationOfQuestion.partnersLogos,
              PersistentOperationOfQuestion.column.partnersLogosAlt -> operationOfQuestion.partnersLogosAlt,
              PersistentOperationOfQuestion.column.initiatorsLogos -> operationOfQuestion.initiatorsLogos,
              PersistentOperationOfQuestion.column.initiatorsLogosAlt -> operationOfQuestion.initiatorsLogosAlt,
              PersistentOperationOfQuestion.column.consultationHeader -> operationOfQuestion.consultationHeader,
              PersistentOperationOfQuestion.column.consultationHeaderAlts -> operationOfQuestion.consultationHeaderAlts
                .map(_.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.cobrandingLogo -> operationOfQuestion.cobrandingLogo,
              PersistentOperationOfQuestion.column.cobrandingLogoAlt -> operationOfQuestion.cobrandingLogoAlt
                .map(_.value),
              PersistentOperationOfQuestion.column.resultsLink -> operationOfQuestion.resultsLink,
              PersistentOperationOfQuestion.column.proposalsCount -> operationOfQuestion.proposalsCount,
              PersistentOperationOfQuestion.column.participantsCount -> operationOfQuestion.participantsCount,
              PersistentOperationOfQuestion.column.actions -> operationOfQuestion.actions,
              PersistentOperationOfQuestion.column.featured -> operationOfQuestion.featured,
              PersistentOperationOfQuestion.column.votesCount -> operationOfQuestion.votesCount,
              PersistentOperationOfQuestion.column.votesTarget -> operationOfQuestion.votesTarget,
              PersistentOperationOfQuestion.column.actionDate -> operationOfQuestion.timeline.action.map(_.date),
              PersistentOperationOfQuestion.column.actionDateTexts -> operationOfQuestion.timeline.action
                .map(_.dateTexts.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.actionDescriptions -> operationOfQuestion.timeline.action
                .map(_.descriptions.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.resultDate -> operationOfQuestion.timeline.result.map(_.date),
              PersistentOperationOfQuestion.column.resultDateTexts -> operationOfQuestion.timeline.result
                .map(_.dateTexts.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.resultDescriptions -> operationOfQuestion.timeline.result
                .map(_.descriptions.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.workshopDate -> operationOfQuestion.timeline.workshop.map(_.date),
              PersistentOperationOfQuestion.column.workshopDateTexts -> operationOfQuestion.timeline.workshop
                .map(_.dateTexts.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.workshopDescriptions -> operationOfQuestion.timeline.workshop
                .map(_.descriptions.mapTranslations(_.value)),
              PersistentOperationOfQuestion.column.sessionBindingMode -> operationOfQuestion.sessionBindingMode,
              PersistentOperationOfQuestion.column.reportUrl -> operationOfQuestion.reportUrl,
              PersistentOperationOfQuestion.column.actionsUrl -> operationOfQuestion.actionsUrl
            )
            .where(sqls.eq(PersistentOperationOfQuestion.column.questionId, operationOfQuestion.questionId))
        }.execute()
      }).as(operationOfQuestion)
    }

    override def getById(id: QuestionId): Future[Option[OperationOfQuestion]] = {
      implicit val context: EC = readExecutionContext
      Future(NamedDB("READ").retryableTx { implicit session =>
        withSQL[PersistentOperationOfQuestion] {
          select
            .from(PersistentOperationOfQuestion.as(PersistentOperationOfQuestion.alias))
            .where(sqls.eq(PersistentOperationOfQuestion.column.questionId, id))
        }.map(PersistentOperationOfQuestion(PersistentOperationOfQuestion.alias.resultName)(_)).single()
      }).map(_.map(_.toOperationOfQuestion))
    }

    override def find(operationId: Option[OperationId]): Future[Seq[OperationOfQuestion]] = {
      implicit val context: EC = readExecutionContext
      Future(NamedDB("READ").retryableTx { implicit session =>
        withSQL[PersistentOperationOfQuestion] {
          select
            .from(PersistentOperationOfQuestion.as(PersistentOperationOfQuestion.alias))
            .where(
              sqls.toAndConditionOpt(
                operationId
                  .map(operation => sqls.eq(PersistentOperationOfQuestion.column.operationId, operation.value))
              )
            )
        }.map(PersistentOperationOfQuestion(PersistentOperationOfQuestion.alias.resultName)).list()
      }).map(_.map(_.toOperationOfQuestion))
    }

    override def delete(questionId: QuestionId): Future[Unit] = {
      implicit val context: EC = readExecutionContext
      Future(NamedDB("WRITE").retryableTx { implicit session =>
        withSQL {
          deleteFrom(PersistentOperationOfQuestion)
            .where(sqls.eq(PersistentOperationOfQuestion.column.questionId, questionId))
        }.execute()
      }).void
    }

    override def count(
      questionIds: Option[Seq[QuestionId]],
      operationIds: Option[Seq[OperationId]],
      openAt: Option[ZonedDateTime],
      endAfter: Option[ZonedDateTime],
      slug: Option[String]
    ): Future[Int] = {
      implicit val context: EC = readExecutionContext
      Future(NamedDB("READ").retryableTx { implicit session =>
        withSQL[PersistentOperationOfQuestion] {
          select(sqls.count)
            .from(PersistentOperationOfQuestion.as(PersistentOperationOfQuestion.alias))
            .innerJoin(PersistentQuestion.as(PersistentQuestion.alias))
            .on(operationOfQuestionAlias.questionId, PersistentQuestion.alias.questionId)
            .where(
              sqls.toAndConditionOpt(
                operationIds
                  .map(opIds => sqls.in(PersistentOperationOfQuestion.alias.operationId, opIds.map(_.value))),
                questionIds
                  .map(qIds => sqls.in(PersistentOperationOfQuestion.alias.questionId, qIds.map(_.value))),
                openAt.map(
                  openAt =>
                    sqls
                      .le(PersistentOperationOfQuestion.alias.startDate, openAt)
                      .and(sqls.ge(PersistentOperationOfQuestion.alias.endDate, openAt))
                ),
                endAfter.map(end => sqls.ge(PersistentOperationOfQuestion.alias.endDate, end)),
                slug.map(s       => sqls.like(PersistentQuestion.alias.slug, s"%$s%"))
              )
            )
        }.map(_.int(1)).single().getOrElse(0)
      })
    }
  }
}

object DefaultPersistentOperationOfQuestionServiceComponent {

  final case class PersistentOperationOfQuestion(
    questionId: String,
    operationId: String,
    startDate: ZonedDateTime,
    endDate: ZonedDateTime,
    operationTitles: Multilingual[String],
    proposalPrefixes: Multilingual[String],
    createdAt: ZonedDateTime,
    updatedAt: ZonedDateTime,
    canPropose: Boolean,
    introCardEnabled: Boolean,
    introCardTitles: Option[Multilingual[String]],
    introCardDescriptions: Option[Multilingual[String]],
    pushProposalCardEnabled: Boolean,
    finalCardEnabled: Boolean,
    finalCardSharingEnabled: Boolean,
    finalCardTitles: Option[Multilingual[String]],
    finalCardShareDescriptions: Option[Multilingual[String]],
    finalCardLearnMoreTitles: Option[Multilingual[String]],
    finalCardLearnMoreButtons: Option[Multilingual[String]],
    finalCardLinkUrl: Option[String],
    aboutUrls: Option[Multilingual[String]],
    metaTitles: Option[Multilingual[String]],
    metaDescriptions: Option[Multilingual[String]],
    metaPicture: Option[String],
    color: String,
    fontColor: String,
    descriptions: Option[Multilingual[String]],
    consultationImages: Option[Multilingual[String]],
    consultationImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
    descriptionImages: Option[Multilingual[String]],
    descriptionImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
    partnersLogos: Option[String],
    partnersLogosAlt: Option[String Refined MaxSize[130]],
    initiatorsLogos: Option[String],
    initiatorsLogosAlt: Option[String Refined MaxSize[130]],
    consultationHeader: Option[String],
    consultationHeaderAlts: Option[Multilingual[String Refined MaxSize[130]]],
    cobrandingLogo: Option[String],
    cobrandingLogoAlt: Option[String Refined MaxSize[130]],
    resultsLink: Option[ResultsLink],
    proposalsCount: Int,
    participantsCount: Int,
    actions: Option[String],
    featured: Boolean,
    votesCount: Int,
    votesTarget: Int,
    actionDate: Option[LocalDate],
    actionDateTexts: Option[Multilingual[String Refined MaxSize[20]]],
    actionDescriptions: Option[Multilingual[String Refined MaxSize[150]]],
    resultDate: Option[LocalDate],
    resultDateTexts: Option[Multilingual[String Refined MaxSize[20]]],
    resultDescriptions: Option[Multilingual[String Refined MaxSize[150]]],
    workshopDate: Option[LocalDate],
    workshopDateTexts: Option[Multilingual[String Refined MaxSize[20]]],
    workshopDescriptions: Option[Multilingual[String Refined MaxSize[150]]],
    sessionBindingMode: Boolean,
    reportUrl: Option[String],
    actionsUrl: Option[String]
  ) {
    def toOperationOfQuestion: OperationOfQuestion = OperationOfQuestion(
      questionId = QuestionId(this.questionId),
      operationId = OperationId(this.operationId),
      startDate = this.startDate,
      endDate = this.endDate,
      operationTitles = this.operationTitles,
      proposalPrefixes = this.proposalPrefixes,
      canPropose = this.canPropose,
      sequenceCardsConfiguration = SequenceCardsConfiguration(
        introCard = IntroCard(
          enabled = this.introCardEnabled,
          titles = this.introCardTitles,
          descriptions = this.introCardDescriptions
        ),
        pushProposalCard = PushProposalCard(enabled = this.pushProposalCardEnabled),
        finalCard = FinalCard(
          enabled = this.finalCardEnabled,
          sharingEnabled = this.finalCardSharingEnabled,
          titles = this.finalCardTitles,
          shareDescriptions = this.finalCardShareDescriptions,
          learnMoreTitles = this.finalCardLearnMoreTitles,
          learnMoreTextButtons = this.finalCardLearnMoreButtons,
          linkUrl = this.finalCardLinkUrl
        )
      ),
      aboutUrls = this.aboutUrls,
      metas = Metas(titles = this.metaTitles, descriptions = this.metaDescriptions, picture = this.metaPicture),
      theme =
        QuestionTheme(color = Colour.fromStringUnsafe(this.color), fontColor = Colour.fromStringUnsafe(this.fontColor)),
      descriptions = this.descriptions,
      consultationImages = this.consultationImages,
      consultationImageAlts = this.consultationImageAlts,
      descriptionImages = this.descriptionImages,
      descriptionImageAlts = this.descriptionImageAlts,
      partnersLogos = this.partnersLogos,
      partnersLogosAlt = this.partnersLogosAlt,
      initiatorsLogos = this.initiatorsLogos,
      initiatorsLogosAlt = this.initiatorsLogosAlt,
      consultationHeader = this.consultationHeader,
      consultationHeaderAlts = this.consultationHeaderAlts,
      cobrandingLogo = this.cobrandingLogo,
      cobrandingLogoAlt = this.cobrandingLogoAlt,
      resultsLink = this.resultsLink,
      proposalsCount = this.proposalsCount,
      participantsCount = this.participantsCount,
      actions = this.actions,
      featured = this.featured,
      votesTarget = this.votesTarget,
      votesCount = this.votesCount,
      timeline = OperationOfQuestionTimeline(
        action = (this.actionDate, this.actionDateTexts, this.actionDescriptions)
          .mapN(TimelineElement(_, _, _)),
        result = (this.resultDate, this.resultDateTexts, this.resultDescriptions)
          .mapN(TimelineElement(_, _, _)),
        workshop = (this.workshopDate, this.workshopDateTexts, this.workshopDescriptions)
          .mapN(TimelineElement(_, _, _))
      ),
      createdAt = this.createdAt,
      sessionBindingMode = this.sessionBindingMode,
      reportUrl = this.reportUrl,
      actionsUrl = this.actionsUrl
    )
  }

  implicit object PersistentOperationOfQuestion
      extends PersistentCompanion[PersistentOperationOfQuestion, OperationOfQuestion]
      with ShortenedNames
      with Logging {

    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    implicit val resultsLinkBinders: Binders[Option[ResultsLink]] =
      Binders.string.xmap(s => Option(s).flatMap(ResultsLink.parse), _.map(Show[ResultsLink].show).orNull)

    final case class FlatQuestionWithDetails(
      questionId: String,
      countries: String,
      defaultLanguage: String,
      languages: NonEmptyList[Language],
      questions: Multilingual[String Refined NonEmpty],
      shortTitles: Option[Multilingual[String Refined NonEmpty]],
      slug: String,
      operationId: String,
      startDate: ZonedDateTime,
      endDate: ZonedDateTime,
      operationTitles: Multilingual[String],
      proposalPrefixes: Multilingual[String],
      canPropose: Boolean,
      introCardEnabled: Boolean,
      introCardTitles: Option[Multilingual[String]],
      introCardDescriptions: Option[Multilingual[String]],
      pushProposalCardEnabled: Boolean,
      finalCardEnabled: Boolean,
      finalCardSharingEnabled: Boolean,
      finalCardTitles: Option[Multilingual[String]],
      finalCardShareDescriptions: Option[Multilingual[String]],
      finalCardLearnMoreTitles: Option[Multilingual[String]],
      finalCardLearnMoreButtons: Option[Multilingual[String]],
      finalCardLinkUrl: Option[String],
      aboutUrls: Option[Multilingual[String]],
      metaTitles: Option[Multilingual[String]],
      metaDescriptions: Option[Multilingual[String]],
      metaPicture: Option[String],
      color: String,
      fontColor: String,
      descriptions: Option[Multilingual[String]],
      consultationImages: Option[Multilingual[String]],
      consultationImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
      descriptionImages: Option[Multilingual[String]],
      descriptionImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
      partnersLogos: Option[String],
      partnersLogosAlt: Option[String Refined MaxSize[130]],
      initiatorsLogos: Option[String],
      initiatorsLogosAlt: Option[String Refined MaxSize[130]],
      consultationHeader: Option[String],
      consultationHeaderAlts: Option[Multilingual[String Refined MaxSize[130]]],
      cobrandingLogo: Option[String],
      cobrandingLogoAlt: Option[String Refined MaxSize[130]],
      resultsLink: Option[ResultsLink],
      proposalsCount: Int,
      participantsCount: Int,
      actions: Option[String],
      featured: Boolean,
      votesCount: Int,
      votesTarget: Int,
      actionDate: Option[LocalDate],
      actionDateTexts: Option[Multilingual[String Refined MaxSize[20]]],
      actionDescriptions: Option[Multilingual[String Refined MaxSize[150]]],
      resultDate: Option[LocalDate],
      resultDateTexts: Option[Multilingual[String Refined MaxSize[20]]],
      resultDescriptions: Option[Multilingual[String Refined MaxSize[150]]],
      workshopDate: Option[LocalDate],
      workshopDateTexts: Option[Multilingual[String Refined MaxSize[20]]],
      workshopDescriptions: Option[Multilingual[String Refined MaxSize[150]]],
      createdAt: ZonedDateTime,
      sessionBindingMode: Boolean,
      reportUrl: Option[String],
      actionsUrl: Option[String]
    ) {
      def toQuestionAndDetails: QuestionWithDetails = {
        QuestionWithDetails(
          question = Question(
            questionId = QuestionId(questionId),
            countries = NonEmptyList.fromListUnsafe(countries.split(',').map(Country(_)).toList),
            defaultLanguage = Language(defaultLanguage),
            languages = languages,
            slug = slug,
            questions = questions,
            shortTitles = shortTitles,
            operationId = Some(OperationId(operationId))
          ),
          details = OperationOfQuestion(
            questionId = QuestionId(questionId),
            operationId = OperationId(operationId),
            startDate = startDate,
            endDate = endDate,
            operationTitles = operationTitles,
            proposalPrefixes = proposalPrefixes,
            canPropose = canPropose,
            sequenceCardsConfiguration = SequenceCardsConfiguration(
              introCard =
                IntroCard(enabled = introCardEnabled, titles = introCardTitles, descriptions = introCardDescriptions),
              pushProposalCard = PushProposalCard(enabled = pushProposalCardEnabled),
              finalCard = FinalCard(
                enabled = finalCardEnabled,
                sharingEnabled = finalCardSharingEnabled,
                titles = finalCardTitles,
                shareDescriptions = finalCardShareDescriptions,
                learnMoreTitles = finalCardLearnMoreTitles,
                learnMoreTextButtons = finalCardLearnMoreButtons,
                linkUrl = finalCardLinkUrl
              )
            ),
            aboutUrls = this.aboutUrls,
            metas = Metas(titles = metaTitles, descriptions = metaDescriptions, picture = metaPicture),
            theme = QuestionTheme(
              color = Colour.fromStringUnsafe(this.color),
              fontColor = Colour.fromStringUnsafe(this.fontColor)
            ),
            descriptions = this.descriptions,
            consultationImages = this.consultationImages,
            consultationImageAlts = this.consultationImageAlts,
            descriptionImages = this.descriptionImages,
            descriptionImageAlts = this.descriptionImageAlts,
            partnersLogos = this.partnersLogos,
            partnersLogosAlt = this.partnersLogosAlt,
            initiatorsLogos = this.initiatorsLogos,
            initiatorsLogosAlt = this.initiatorsLogosAlt,
            consultationHeader = this.consultationHeader,
            consultationHeaderAlts = this.consultationHeaderAlts,
            cobrandingLogo = this.cobrandingLogo,
            cobrandingLogoAlt = this.cobrandingLogoAlt,
            resultsLink = this.resultsLink,
            proposalsCount = this.proposalsCount,
            participantsCount = this.participantsCount,
            actions = this.actions,
            featured = this.featured,
            votesCount = this.votesCount,
            votesTarget = this.votesTarget,
            timeline = OperationOfQuestionTimeline(
              action = (this.actionDate, this.actionDateTexts, this.actionDescriptions)
                .mapN(TimelineElement(_, _, _)),
              result = (this.resultDate, this.resultDateTexts, this.resultDescriptions)
                .mapN(TimelineElement(_, _, _)),
              workshop = (this.workshopDate, this.workshopDateTexts, this.workshopDescriptions)
                .mapN(TimelineElement(_, _, _))
            ),
            createdAt = this.createdAt,
            sessionBindingMode = sessionBindingMode,
            reportUrl = reportUrl,
            actionsUrl = actionsUrl
          )
        )

      }
    }

    @SuppressWarnings(Array("org.wartremover.warts.AsInstanceOf"))
    def withQuestion(
      questionAlias: ResultName[PersistentQuestion],
      operationOfQuestionAlias: ResultName[PersistentOperationOfQuestion]
    )(resultSet: WrappedResultSet): Option[FlatQuestionWithDetails] = {
      for {
        questionId   <- resultSet.stringOpt(operationOfQuestionAlias.questionId)
        questionSlug <- resultSet.stringOpt(questionAlias.slug)
        operationId  <- resultSet.stringOpt(operationOfQuestionAlias.operationId)
        countries    <- resultSet.stringOpt(questionAlias.countries)
        languages <- resultSet
          .arrayOpt(questionAlias.languages)
          .map(_.getArray.asInstanceOf[Array[String]].map(Language(_)))
      } yield FlatQuestionWithDetails(
        questionId = questionId,
        countries = countries,
        defaultLanguage = resultSet.get[String](questionAlias.defaultLanguage),
        languages = NonEmptyList.fromListUnsafe(languages.toList),
        questions = resultSet.get[Multilingual[String Refined NonEmpty]](questionAlias.questions),
        shortTitles = resultSet.get[Option[Multilingual[String Refined NonEmpty]]](questionAlias.shortTitles),
        slug = questionSlug,
        operationId = operationId,
        startDate = resultSet.zonedDateTime(operationOfQuestionAlias.startDate),
        endDate = resultSet.zonedDateTime(operationOfQuestionAlias.endDate),
        operationTitles = resultSet.get[Multilingual[String]](operationOfQuestionAlias.operationTitles),
        proposalPrefixes = resultSet.get[Multilingual[String]](operationOfQuestionAlias.proposalPrefixes),
        canPropose = resultSet.boolean(operationOfQuestionAlias.canPropose),
        introCardEnabled = resultSet.boolean(operationOfQuestionAlias.introCardEnabled),
        introCardTitles = resultSet.get(operationOfQuestionAlias.introCardTitles),
        introCardDescriptions = resultSet.get(operationOfQuestionAlias.introCardDescriptions),
        pushProposalCardEnabled = resultSet.boolean(operationOfQuestionAlias.pushProposalCardEnabled),
        finalCardEnabled = resultSet.boolean(operationOfQuestionAlias.finalCardEnabled),
        finalCardSharingEnabled = resultSet.boolean(operationOfQuestionAlias.finalCardSharingEnabled),
        finalCardTitles = resultSet.get(operationOfQuestionAlias.finalCardTitles),
        finalCardShareDescriptions = resultSet.get(operationOfQuestionAlias.finalCardShareDescriptions),
        finalCardLearnMoreTitles =
          resultSet.get[Option[Multilingual[String]]](operationOfQuestionAlias.finalCardLearnMoreTitles),
        finalCardLearnMoreButtons = resultSet.get(operationOfQuestionAlias.finalCardLearnMoreButtons),
        finalCardLinkUrl = resultSet.stringOpt(operationOfQuestionAlias.finalCardLinkUrl),
        aboutUrls = resultSet.get[Option[Multilingual[String]]](operationOfQuestionAlias.aboutUrls),
        metaTitles = resultSet.get(operationOfQuestionAlias.metaTitles),
        metaDescriptions = resultSet.get(operationOfQuestionAlias.metaDescriptions),
        metaPicture = resultSet.stringOpt(operationOfQuestionAlias.metaPicture),
        color = resultSet.string(operationOfQuestionAlias.color),
        fontColor = resultSet.string(operationOfQuestionAlias.fontColor),
        descriptions = resultSet.get(operationOfQuestionAlias.descriptions),
        consultationImages = resultSet.get(operationOfQuestionAlias.consultationImages),
        consultationImageAlts = resultSet.get(operationOfQuestionAlias.consultationImageAlts),
        descriptionImages = resultSet.get(operationOfQuestionAlias.descriptionImages),
        descriptionImageAlts = resultSet.get(operationOfQuestionAlias.descriptionImageAlts),
        partnersLogos = resultSet.stringOpt(operationOfQuestionAlias.partnersLogos),
        partnersLogosAlt = resultSet.get(operationOfQuestionAlias.partnersLogosAlt),
        initiatorsLogos = resultSet.stringOpt(operationOfQuestionAlias.initiatorsLogos),
        initiatorsLogosAlt = resultSet.get(operationOfQuestionAlias.initiatorsLogosAlt),
        consultationHeader = resultSet.stringOpt(operationOfQuestionAlias.consultationHeader),
        consultationHeaderAlts = resultSet.get(operationOfQuestionAlias.consultationHeaderAlts),
        cobrandingLogo = resultSet.stringOpt(operationOfQuestionAlias.cobrandingLogo),
        cobrandingLogoAlt = resultSet.get(operationOfQuestionAlias.cobrandingLogoAlt),
        resultsLink = resultSet.get(operationOfQuestionAlias.resultsLink),
        proposalsCount = resultSet.int(operationOfQuestionAlias.proposalsCount),
        participantsCount = resultSet.int(operationOfQuestionAlias.participantsCount),
        actions = resultSet.stringOpt(operationOfQuestionAlias.actions),
        featured = resultSet.boolean(operationOfQuestionAlias.featured),
        votesCount = resultSet.int(operationOfQuestionAlias.votesCount),
        votesTarget = resultSet.int(operationOfQuestionAlias.votesTarget),
        actionDate = resultSet.localDateOpt(operationOfQuestionAlias.actionDate),
        actionDateTexts = resultSet.get(operationOfQuestionAlias.actionDateTexts),
        actionDescriptions = resultSet.get(operationOfQuestionAlias.actionDescriptions),
        resultDate = resultSet.localDateOpt(operationOfQuestionAlias.resultDate),
        resultDateTexts = resultSet.get(operationOfQuestionAlias.resultDateTexts),
        resultDescriptions = resultSet.get(operationOfQuestionAlias.resultDescriptions),
        workshopDate = resultSet.localDateOpt(operationOfQuestionAlias.workshopDate),
        workshopDateTexts = resultSet.get(operationOfQuestionAlias.workshopDateTexts),
        workshopDescriptions = resultSet.get(operationOfQuestionAlias.workshopDescriptions),
        createdAt = resultSet.zonedDateTime(operationOfQuestionAlias.createdAt),
        sessionBindingMode = resultSet.boolean(operationOfQuestionAlias.sessionBindingMode),
        reportUrl = resultSet.get[Option[String]](operationOfQuestionAlias.reportUrl),
        actionsUrl = resultSet.get[Option[String]](operationOfQuestionAlias.actionsUrl)
      )
    }

    override val columnNames: Seq[String] =
      Seq(
        "question_id",
        "operation_id",
        "start_date",
        "end_date",
        "operation_titles",
        "proposal_prefixes",
        "created_at",
        "updated_at",
        "can_propose",
        "intro_card_enabled",
        "intro_card_titles",
        "intro_card_descriptions",
        "push_proposal_card_enabled",
        "final_card_enabled",
        "final_card_sharing_enabled",
        "final_card_titles",
        "final_card_share_descriptions",
        "final_card_learn_more_titles",
        "final_card_learn_more_buttons",
        "final_card_link_url",
        "about_urls",
        "meta_titles",
        "meta_descriptions",
        "meta_picture",
        "color",
        "font_color",
        "descriptions",
        "consultation_images",
        "consultation_image_alts",
        "description_images",
        "description_image_alts",
        "partners_logos",
        "partners_logos_alt",
        "initiators_logos",
        "initiators_logos_alt",
        "consultation_header",
        "consultation_header_alts",
        "cobranding_logo",
        "cobranding_logo_alt",
        "results_link",
        "proposals_count",
        "participants_count",
        "actions",
        "featured",
        "votes_count",
        "votes_target",
        "action_date",
        "action_date_texts",
        "action_descriptions",
        "result_date",
        "result_date_texts",
        "result_descriptions",
        "workshop_date",
        "workshop_date_texts",
        "workshop_descriptions",
        "session_binding_mode",
        "report_url",
        "actions_url"
      )
    final val swaggerAllowableValues =
      "question_id,operation_id,start_date,end_date,operation_titles,created_at,updated_at,can_propose,intro_card_enabled,intro_card_titles,intro_card_descriptions,push_proposal_card_enabled,final_card_enabled,final_card_sharing_enabled,final_card_titles,final_card_share_descriptions,final_card_learn_more_titles,final_card_learn_more_buttons,final_card_link_url,about_urls,meta_titles,meta_descriptions,meta_picture,color,font_color,descriptions,consultation_images,consultation_image_alts,description_images,description_image_alts,results_link,proposals_count,participants_count,actions,featured,votes_count,votes_target,action_date,action_date_texts,action_descriptions,result_date,result_date_texts,result_descriptions,workshop_date,workshop_date_texts,workshop_descriptions,session_binding_mode,report_url"

    override val tableName: String = "operation_of_question"

    override lazy val alias: SyntaxProvider[PersistentOperationOfQuestion] = syntax("operationOfquestion")

    override lazy val defaultSortColumns: NonEmptyList[SQLSyntax] = NonEmptyList.of(alias.questionId)

    def apply(
      resultName: ResultName[PersistentOperationOfQuestion] = alias.resultName
    )(resultSet: WrappedResultSet): PersistentOperationOfQuestion = {
      PersistentOperationOfQuestion(
        questionId = resultSet.string(resultName.questionId),
        operationId = resultSet.string(resultName.operationId),
        startDate = resultSet.zonedDateTime(resultName.startDate),
        endDate = resultSet.zonedDateTime(resultName.endDate),
        operationTitles = resultSet.get[Multilingual[String]](resultName.operationTitles),
        proposalPrefixes = resultSet.get[Multilingual[String]](resultName.proposalPrefixes),
        createdAt = resultSet.zonedDateTime(resultName.createdAt),
        updatedAt = resultSet.zonedDateTime(resultName.updatedAt),
        canPropose = resultSet.boolean(resultName.canPropose),
        introCardEnabled = resultSet.boolean(resultName.introCardEnabled),
        introCardTitles = resultSet.get[Option[Multilingual[String]]](resultName.introCardTitles),
        introCardDescriptions = resultSet.get[Option[Multilingual[String]]](resultName.introCardDescriptions),
        pushProposalCardEnabled = resultSet.boolean(resultName.pushProposalCardEnabled),
        finalCardEnabled = resultSet.boolean(resultName.finalCardEnabled),
        finalCardSharingEnabled = resultSet.boolean(resultName.finalCardSharingEnabled),
        finalCardTitles = resultSet.get[Option[Multilingual[String]]](resultName.finalCardTitles),
        finalCardShareDescriptions = resultSet.get[Option[Multilingual[String]]](resultName.finalCardShareDescriptions),
        finalCardLearnMoreTitles = resultSet.get[Option[Multilingual[String]]](resultName.finalCardLearnMoreTitles),
        finalCardLearnMoreButtons = resultSet.get[Option[Multilingual[String]]](resultName.finalCardLearnMoreButtons),
        finalCardLinkUrl = resultSet.stringOpt(resultName.finalCardLinkUrl),
        aboutUrls = resultSet.get[Option[Multilingual[String]]](resultName.aboutUrls),
        metaTitles = resultSet.get[Option[Multilingual[String]]](resultName.metaTitles),
        metaDescriptions = resultSet.get[Option[Multilingual[String]]](resultName.metaDescriptions),
        metaPicture = resultSet.stringOpt(resultName.metaPicture),
        color = resultSet.string(resultName.color),
        fontColor = resultSet.string(resultName.fontColor),
        descriptions = resultSet.get[Option[Multilingual[String]]](resultName.descriptions),
        consultationImages = resultSet.get[Option[Multilingual[String]]](resultName.consultationImages),
        consultationImageAlts =
          resultSet.get[Option[Multilingual[String Refined MaxSize[130]]]](resultName.consultationImageAlts),
        descriptionImages = resultSet.get[Option[Multilingual[String]]](resultName.descriptionImages),
        descriptionImageAlts =
          resultSet.get[Option[Multilingual[String Refined MaxSize[130]]]](resultName.descriptionImageAlts),
        partnersLogos = resultSet.stringOpt(resultName.partnersLogos),
        partnersLogosAlt = resultSet.get[Option[String Refined MaxSize[130]]](resultName.partnersLogosAlt),
        initiatorsLogos = resultSet.stringOpt(resultName.initiatorsLogos),
        initiatorsLogosAlt = resultSet.get[Option[String Refined MaxSize[130]]](resultName.initiatorsLogosAlt),
        consultationHeader = resultSet.stringOpt(resultName.consultationHeader),
        consultationHeaderAlts =
          resultSet.get[Option[Multilingual[String Refined MaxSize[130]]]](resultName.consultationHeaderAlts),
        cobrandingLogo = resultSet.stringOpt(resultName.cobrandingLogo),
        cobrandingLogoAlt = resultSet.get[Option[String Refined MaxSize[130]]](resultName.cobrandingLogoAlt),
        resultsLink = resultSet.get[Option[ResultsLink]](resultName.resultsLink),
        proposalsCount = resultSet.int(resultName.proposalsCount),
        participantsCount = resultSet.int(resultName.participantsCount),
        actions = resultSet.stringOpt(resultName.actions),
        featured = resultSet.boolean(resultName.featured),
        votesCount = resultSet.int(resultName.votesCount),
        votesTarget = resultSet.int(resultName.votesTarget),
        actionDate = resultSet.localDateOpt(resultName.actionDate),
        actionDateTexts = resultSet.get[Option[Multilingual[String Refined MaxSize[20]]]](resultName.actionDateTexts),
        actionDescriptions =
          resultSet.get[Option[Multilingual[String Refined MaxSize[150]]]](resultName.actionDescriptions),
        resultDate = resultSet.localDateOpt(resultName.resultDate),
        resultDateTexts = resultSet.get[Option[Multilingual[String Refined MaxSize[20]]]](resultName.resultDateTexts),
        resultDescriptions =
          resultSet.get[Option[Multilingual[String Refined MaxSize[150]]]](resultName.resultDescriptions),
        workshopDate = resultSet.localDateOpt(resultName.workshopDate),
        workshopDateTexts =
          resultSet.get[Option[Multilingual[String Refined MaxSize[20]]]](resultName.workshopDateTexts),
        workshopDescriptions =
          resultSet.get[Option[Multilingual[String Refined MaxSize[150]]]](resultName.workshopDescriptions),
        sessionBindingMode = resultSet.boolean(resultName.sessionBindingMode),
        reportUrl = resultSet.get[Option[String]](resultName.reportUrl),
        actionsUrl = resultSet.get[Option[String]](resultName.actionsUrl)
      )
    }
  }
}
