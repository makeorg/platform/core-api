package org.make.api.migrations.db

import org.make.api.migrations.db.SqlTools.ImprovedResultSet

import java.sql.Connection

@SuppressWarnings(Array("org.wartremover.warts.TraversableOps"))
class V147__Retro_compatibility_old_question_prefix extends Migration {

  val prefixes: Map[String, String] = Map(
    "fr" -> "Il faut",
    "de" -> "Man sollte",
    "en" -> "We should",
    "bg" -> "Трябва",
    "hr" -> "Potrebno je",
    "da" -> "Vi skal",
    "es" -> "Es necesario",
    "et" -> "On vaja",
    "fi" -> "On toimittava näin:",
    "el" -> "Πρέπει",
    "hu" -> "Muszáj",
    "it" -> "È necessario",
    "lv" -> "Nepieciešams",
    "lt" -> "Būtina",
    "mt" -> "Il faut",
    "nl" -> "Er moet",
    "pl" -> "Należy",
    "pt" -> "É necessário",
    "ro" -> "Trebuie",
    "sk" -> "Je potrebné",
    "sl" -> "Treba je",
    "sv" -> "Vi måste",
    "cs" -> "Je třeba",
    "uk" -> "Необхідно"
  )

  private def getQuestionLanguages(implicit connection: Connection) =
    connection
      .prepareStatement(s"SELECT question_id, languages FROM question")
      .executeQuery
      .toMaps("question_id", "languages")

  private def updateConsultation(proposalPrefixes: String, questionId: String)(implicit connection: Connection) =
    connection
      .prepareStatement(s"""
           |UPDATE operation_of_question
           |SET proposal_prefixes = $$$${$proposalPrefixes}$$$$
           |WHERE question_id = '$questionId'
          """.stripMargin)
      .execute()

  override def migrate(connection: Connection): Unit = {
    implicit val C: Connection = connection
    val questionLanguages = getQuestionLanguages
    questionLanguages.foreach(questionLanguage => {
      val questionId = questionLanguage("question_id")
      val languagesString = questionLanguage("languages")
      val proposalPrefixes =
        languagesString
          .slice(1, languagesString.length - 1)
          .split(',')
          .map(l => l -> prefixes(l))
          .toMap
          .toSeq
          .map {
            case (k, v) => s"\"$k\": \"$v\""
          }
          .mkString(", ")
      updateConsultation(proposalPrefixes, questionId)
    })
  }
}
