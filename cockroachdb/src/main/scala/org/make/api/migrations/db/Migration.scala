/*
 *  Make.org Core API
 *  Copyright (C) 2020 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.migrations.db

import java.sql.{Connection, ResultSet}
import scala.collection.mutable.ArrayBuffer
import org.flywaydb.core.api.migration.{BaseJavaMigration, Context}

abstract class Migration extends BaseJavaMigration {

  override def migrate(context: Context): Unit = {
    val connection = context.getConnection
    val autoCommit = connection.getAutoCommit
    connection.setAutoCommit(false)
    migrate(connection)
    connection.commit()
    connection.setAutoCommit(autoCommit)
  }

  def migrate(connection: Connection): Unit
}

object SqlTools {

  implicit class ImprovedResultSet(set: ResultSet) {

    @SuppressWarnings(Array("org.wartremover.warts.While", "org.wartremover.warts.MutableDataStructures"))
    def toListOfRows(columns: List[String]): ArrayBuffer[List[String]] = {
      val rows = ArrayBuffer[List[String]]()
      while (set.next()) rows.append(columns.map(set.getString))
      rows
    }

    @SuppressWarnings(Array("org.wartremover.warts.While", "org.wartremover.warts.MutableDataStructures"))
    def toMaps(columns: String*): ArrayBuffer[Map[String, String]] = {
      val rows = ArrayBuffer[Map[String, String]]()
      while (set.next()) rows.append(columns.map(k => (k -> set.getString(k))).toMap)
      rows
    }
  }
}
