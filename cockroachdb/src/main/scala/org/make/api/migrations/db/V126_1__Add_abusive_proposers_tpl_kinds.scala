/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.migrations.db

import java.sql.Connection
import java.util.UUID
import scala.collection.mutable.HashSet

class V126_1__Add_abusive_proposers_tpl_kinds extends Migration {

  private def nextId(): String = UUID.randomUUID().toString

  private val isProd = System.getenv("ENV_NAME") == "prod"

  private val templateAbusiveWarnIds: Map[String, (String, String)] =
    Map("fr" -> ("3520961", "3520884"), "en" -> ("3536356", "3536356"), "de" -> ("3536371", "3536371"))

  private val templateAbusiveBlockIds: Map[String, (String, String)] =
    Map("fr" -> ("3520981", "3520933"), "en" -> ("3536302", "3536302"), "de" -> ("3536641", "3536641"))

  private def makeAbusiveWarnTemplateRow(language: String): String = {
    val (preprod, prod) = templateAbusiveWarnIds.getOrElse(language, templateAbusiveWarnIds("fr"))
    s"('${nextId()}', 'MessageToAbusiveWarn', '$language', '${if (isProd) prod else preprod}')"
  }

  private def makeAbusiveBlockTemplateRow(language: String): String = {
    val (preprod, prod) = templateAbusiveBlockIds.getOrElse(language, templateAbusiveBlockIds("fr"))
    s"('${nextId()}', 'MessageToAbusiveBlock', '$language', '${if (isProd) prod else preprod}')"
  }

  @SuppressWarnings(Array("org.wartremover.warts.While", "org.wartremover.warts.MutableDataStructures"))
  override def migrate(connection: Connection): Unit = {

    def insertTplRow(row: String): Unit =
      connection
        .prepareStatement(s"INSERT INTO crm_language_template (id, kind, language, template) VALUES $row")
        .execute()

    val resultSet =
      connection
        .prepareStatement("SELECT language FROM crm_language_template")
        .executeQuery()

    val languages = HashSet.empty[String]

    while (resultSet.next()) {
      languages += resultSet.getString("language")
    }

    languages.foreach(lang => {
      insertTplRow(makeAbusiveWarnTemplateRow(lang))
      insertTplRow(makeAbusiveBlockTemplateRow(lang))
    })
  }
}
