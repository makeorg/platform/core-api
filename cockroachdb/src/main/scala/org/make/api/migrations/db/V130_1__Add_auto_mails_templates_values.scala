/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.migrations.db

import java.sql.Connection
import java.util.UUID

class V130_1__Add_auto_mails_templates_values extends Migration {
  private def nextId(): String = UUID.randomUUID().toString

  private val isProd = System.getenv("ENV_NAME") == "prod"

  private val voteOnlyNoticeIds: (String, String) =
    ("3971766", "3990233")

  private val endConsultationNoticeIds: (String, String) =
    ("3990183", "3990245")

  private def voteOnlyNoticeTemplateRow: String = {
    val (preprod, prod) = voteOnlyNoticeIds
    s"('${nextId()}', 'VoteOnlyNotice', 'fr', '${if (isProd) prod else preprod}')"
  }

  private def endConsultationNoticeTemplateRow: String = {
    val (preprod, prod) = endConsultationNoticeIds
    s"('${nextId()}', 'EndConsultationNotice', 'fr', '${if (isProd) prod else preprod}')"
  }

  @SuppressWarnings(Array("org.wartremover.warts.While", "org.wartremover.warts.MutableDataStructures"))
  override def migrate(connection: Connection): Unit = {

    def insertTplRow(row: String): Unit =
      connection
        .prepareStatement(s"INSERT INTO crm_language_template (id, kind, language, template) VALUES $row")
        .execute()

    insertTplRow(voteOnlyNoticeTemplateRow)
    insertTplRow(endConsultationNoticeTemplateRow)
  }

}
