/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.migrations.db

import java.sql.Connection
import scala.collection.mutable.ArrayBuffer
import SqlTools.ImprovedResultSet

@SuppressWarnings(Array("org.wartremover.warts.IterableOps"))
class V135_1__Add_language_to_crm_tpl extends Migration {

  private def getQuestionIds(implicit connection: Connection): ArrayBuffer[String] =
    connection
      .prepareStatement(s"SELECT question_id FROM crm_question_template")
      .executeQuery
      .toListOfRows(List("question_id"))
      .map(_.head)

  private def getQuestionsLang(questionId: String)(implicit connection: Connection): String =
    connection
      .prepareStatement(s"SELECT default_language FROM question WHERE question_id = '$questionId'")
      .executeQuery
      .toListOfRows(List("default_language"))
      .head
      .head

  private def genUpdateLangQuery(questionId: String, lang: String)(implicit connection: Connection): Unit =
    connection
      .prepareStatement(s"UPDATE crm_question_template SET language = '$lang' WHERE question_id = '$questionId'")
      .execute

  override def migrate(connection: Connection): Unit = {
    implicit val C: Connection = connection
    val questionIds = getQuestionIds
    val addLanguageToTemplate: (String, String) => Unit = genUpdateLangQuery
    questionIds
      .zip(questionIds.map(getQuestionsLang))
      .foreach(addLanguageToTemplate.tupled)
  }
}
