/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.migrations.db

import java.sql.Connection
import scala.util.chaining.scalaUtilChainingOps
import io.circe.syntax._
import io.circe.parser.decode
import io.circe.generic.auto._
import SqlTools.ImprovedResultSet
import org.make.core.technical.Multilingual
import org.make.core.reference.Language
import org.make.core.demographics.LabelsValue

@SuppressWarnings(Array("org.wartremover.warts.TraversableOps"))
class V137_0__Multilingual_demographics extends Migration {

  private case class LegacyLabelValue(label: String, value: String)

  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  private def migrateParameters(params: String, lang: String): String =
    decode[Seq[LegacyLabelValue]](params) match {
      case Right(list) =>
        list
          .map({
            case LegacyLabelValue(label, value) =>
              LabelsValue(Multilingual(Language(lang) -> label), value)
          })
          .asJson
          .noSpaces
      case Left(err) => throw err
    }

  private val addNewColumns: Connection => Unit =
    _.prepareStatement("""
        |ALTER TABLE demographics_card
        |ADD COLUMN languages STRING[], ADD COLUMN titles JSONB
      """.stripMargin)
      .execute()

  private val destroyOldCumns: Connection => Unit =
    _.prepareStatement("ALTER TABLE demographics_card DROP COLUMN language, DROP COLUMN title")
      .execute()

  private def genUpdateLangQuery(cardId: String, lang: String, title: String, params: String)(
    connection: Connection
  ): Unit = {
    val parameters = migrateParameters(params, lang)
    connection
      .prepareStatement(s"""
        |UPDATE demographics_card
        |SET (languages, titles, parameters) = (ARRAY['$lang'], $$$${"$lang": "$title"}$$$$, $$$$${parameters}$$$$)
        |WHERE id = '$cardId'
      """.stripMargin)
      .execute()
  }

  override def migrate(connection: Connection): Unit =
    connection
      .prepareStatement(s"SELECT id, language, title, parameters FROM demographics_card")
      .executeQuery
      .toMaps("id", "language", "title", "parameters")
      .tap(_ => connection.commit())
      .tap(_ => addNewColumns(connection))
      .tap(_ => connection.commit())
      .foreach(
        dataMap =>
          genUpdateLangQuery(dataMap("id"), dataMap("language"), dataMap("title"), dataMap("parameters"))(connection)
      )
      .tap(_ => connection.commit())
      .tap(_ => destroyOldCumns(connection))
}
