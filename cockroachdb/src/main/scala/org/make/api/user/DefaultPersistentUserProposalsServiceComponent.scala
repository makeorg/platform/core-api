/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.user

import cats.data.NonEmptyList
import grizzled.slf4j.Logging
import org.make.api.extensions.MakeDBExecutionContextComponent
import org.make.api.technical.DatabaseTransactions._
import org.make.api.technical.{PersistentCompanion, ShortenedNames}
import org.make.api.technical.ScalikeSupport._
import org.make.core.question.QuestionId
import org.make.core.user._
import scalikejdbc._
import cats.implicits._

import java.util.UUID
import scala.concurrent.Future

trait DefaultPersistentUserProposalsServiceComponent
    extends PersistentUserProposalsServiceComponent
    with ShortenedNames
    with Logging {

  this: MakeDBExecutionContextComponent =>

  override lazy val persistentUserProposalsService: PersistentUserProposalsService =
    new DefaultPersistentUserProposalsService

  class DefaultPersistentUserProposalsService extends PersistentUserProposalsService {

    import DefaultPersistentUserProposalsServiceComponent.UserProposalsCount

    private val userProposalsAlias = UserProposalsCount.alias
    private val column = UserProposalsCount.column

    private def makePredicates(userId: UserId, questionId: QuestionId) =
      sqls
        .eq(column.questionId, questionId.value)
        .and(sqls.eq(column.userId, userId.value))

    private def getCounterOpt(userId: UserId, questionId: QuestionId)(implicit ctx: EC): Future[Option[Int]] =
      Future(NamedDB("READ").retryableTx { implicit session =>
        withSQL {
          select(userProposalsAlias.count)
            .from(UserProposalsCount.as(userProposalsAlias))
            .where(makePredicates(userId, questionId))
        }.map(_.int(1)).single()
      })

    override def getCounter(userId: UserId, questionId: QuestionId): Future[Int] = {
      implicit val ctx: EC = readExecutionContext
      getCounterOpt(userId, questionId).map(_.getOrElse(0))
    }

    private def createUserProposalsRow(userId: UserId, questionId: QuestionId)(implicit ctx: EC): Future[Unit] =
      Future(NamedDB("WRITE").retryableTx { implicit session =>
        withSQL {
          insert
            .into(UserProposalsCount)
            .namedValues(
              column.id -> UUID.randomUUID().toString,
              column.userId -> userId.value,
              column.questionId -> questionId.value,
              column.count -> 1.toString
            )
        }.execute()
      })

    override def incrementCounter(userId: UserId, questionId: QuestionId): Future[Int] = {
      implicit val ctx: EC = writeExecutionContext
      getCounterOpt(userId, questionId).flatMap({
        case Some(count) =>
          Future(NamedDB("WRITE").retryableTx { implicit session =>
            withSQL {
              update(UserProposalsCount)
                .set(column.count -> (count + 1).toString)
                .where(makePredicates(userId, questionId))
            }.executeUpdate()
          }).as(count + 1)
        case None => createUserProposalsRow(userId, questionId).as(1)
      })
    }
  }
}

object DefaultPersistentUserProposalsServiceComponent {

  final case class UserProposalsCount(id: String, userId: String, questionId: String, count: Int)

  object UserProposalsCount
      extends SQLSyntaxSupport[UserProposalsCount]
      with PersistentCompanion[UserProposalsCount, User]
      with ShortenedNames
      with Logging {

    override val columnNames: Seq[String] = Seq("id", "user_id", "question_id", "count")

    override val tableName: String = "votes_per_user_per_question"

    override lazy val alias: SyntaxProvider[UserProposalsCount] = syntax("counts")

    override lazy val defaultSortColumns: NonEmptyList[SQLSyntax] = NonEmptyList.of(alias.questionId)

    def apply(
      userPropositionsResultName: ResultName[UserProposalsCount]
    )(resultSet: WrappedResultSet): UserProposalsCount = {
      UserProposalsCount(
        id = resultSet.string(userPropositionsResultName.id),
        userId = resultSet.string(userPropositionsResultName.userId),
        questionId = resultSet.string(userPropositionsResultName.questionId),
        count = resultSet.int(userPropositionsResultName.count)
      )
    }
  }
}
