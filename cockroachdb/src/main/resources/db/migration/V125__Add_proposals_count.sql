BEGIN;

CREATE TABLE votes_per_user_per_question (
	id UUID PRIMARY KEY,
	user_id STRING NOT NULL REFERENCES make_user (uuid) ON DELETE CASCADE,
	question_id STRING NOT NULL REFERENCES question (question_id) ON DELETE CASCADE,
	count INT
);

COMMIT;
