BEGIN;

UPDATE make_user as u SET crm_country = u.country;
UPDATE make_user as u SET crm_language = u.language;

COMMIT;