BEGIN;

ALTER TABLE operation_of_question ADD COLUMN proposal_prefixes JSONB;

COMMIT;