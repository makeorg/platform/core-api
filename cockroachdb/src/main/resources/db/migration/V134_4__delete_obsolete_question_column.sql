BEGIN;

ALTER TABLE question DROP COLUMN question;
ALTER TABLE question DROP COLUMN short_title;
ALTER TABLE question DROP COLUMN language;

COMMIT;