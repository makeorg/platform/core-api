BEGIN;

UPDATE operation_of_question AS ooq SET language = (SELECT language FROM question WHERE ooq.question_id = question.question_id);

COMMIT;