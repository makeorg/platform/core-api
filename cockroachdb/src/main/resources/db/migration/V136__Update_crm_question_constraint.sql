BEGIN;

ALTER TABLE crm_question_template ADD CONSTRAINT crm_question_template_kind_question_id_language UNIQUE (kind ASC, question_id ASC, language);
DROP INDEX crm_question_template_kind_question_id CASCADE;

COMMIT;