BEGIN;

ALTER TABLE operation_of_question ADD COLUMN partners_logos STRING NULL;
ALTER TABLE operation_of_question ADD COLUMN partners_logos_alt STRING(130) NULL;
ALTER TABLE operation_of_question ADD COLUMN initiators_logos STRING NULL;
ALTER TABLE operation_of_question ADD COLUMN initiators_logos_alt STRING(130) NULL;
ALTER TABLE operation_of_question ADD COLUMN consultation_header STRING NULL;
ALTER TABLE operation_of_question ADD COLUMN consultation_header_alt STRING(130) NULL;

COMMIT;