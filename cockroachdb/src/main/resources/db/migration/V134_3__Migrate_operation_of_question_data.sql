BEGIN;

UPDATE operation_of_question AS ooq SET operation_titles = json_build_object(ooq.language, to_json(ooq.operation_title)) WHERE ooq.operation_title IS NOT NULL;
UPDATE operation_of_question AS ooq SET intro_card_titles = json_build_object(ooq.language, to_json(ooq.intro_card_title)) WHERE ooq.intro_card_title IS NOT NULL;
UPDATE operation_of_question AS ooq SET intro_card_descriptions = json_build_object(ooq.language, to_json(ooq.intro_card_description)) WHERE ooq.intro_card_description IS NOT NULL;
UPDATE operation_of_question AS ooq SET final_card_titles = json_build_object(ooq.language, to_json(ooq.final_card_title)) WHERE ooq.final_card_title IS NOT NULL;
UPDATE operation_of_question AS ooq SET final_card_share_descriptions = json_build_object(ooq.language, to_json(ooq.final_card_share_description)) WHERE ooq.final_card_share_description IS NOT NULL;
UPDATE operation_of_question AS ooq SET final_card_learn_more_titles = json_build_object(ooq.language, to_json(ooq.final_card_learn_more_title)) WHERE ooq.final_card_learn_more_title IS NOT NULL;
UPDATE operation_of_question AS ooq SET final_card_learn_more_buttons = json_build_object(ooq.language, to_json(ooq.final_card_learn_more_button)) WHERE ooq.final_card_learn_more_button IS NOT NULL;
UPDATE operation_of_question AS ooq SET meta_titles = json_build_object(ooq.language, to_json(ooq.meta_title)) WHERE ooq.meta_title IS NOT NULL;
UPDATE operation_of_question AS ooq SET meta_descriptions = json_build_object(ooq.language, to_json(ooq.meta_description)) WHERE ooq.meta_description IS NOT NULL;
UPDATE operation_of_question AS ooq SET consultation_header_alts = json_build_object(ooq.language, to_json(ooq.consultation_header_alt)) WHERE ooq.consultation_header_alt IS NOT NULL;
UPDATE operation_of_question AS ooq SET consultation_images = json_build_object(ooq.language, to_json(ooq.consultation_image)) WHERE ooq.consultation_image IS NOT NULL;
UPDATE operation_of_question AS ooq SET consultation_image_alts = json_build_object(ooq.language, to_json(ooq.consultation_image_alt)) WHERE ooq.consultation_image_alt IS NOT NULL;
UPDATE operation_of_question AS ooq SET descriptions = json_build_object(ooq.language, to_json(ooq.description)) WHERE ooq.description IS NOT NULL;
UPDATE operation_of_question AS ooq SET description_images = json_build_object(ooq.language, to_json(ooq.description_image)) WHERE ooq.description_image IS NOT NULL;
UPDATE operation_of_question AS ooq SET description_image_alts = json_build_object(ooq.language, to_json(ooq.description_image_alt)) WHERE ooq.description_image_alt IS NOT NULL;
UPDATE operation_of_question AS ooq SET action_date_texts = json_build_object(ooq.language, to_json(ooq.action_date_text)) WHERE ooq.action_date_text IS NOT NULL;
UPDATE operation_of_question AS ooq SET action_descriptions = json_build_object(ooq.language, to_json(ooq.action_description)) WHERE ooq.action_description IS NOT NULL;
UPDATE operation_of_question AS ooq SET result_date_texts = json_build_object(ooq.language, to_json(ooq.result_date_text)) WHERE ooq.result_date_text IS NOT NULL;
UPDATE operation_of_question AS ooq SET result_descriptions = json_build_object(ooq.language, to_json(ooq.result_description)) WHERE ooq.result_description IS NOT NULL;
UPDATE operation_of_question AS ooq SET workshop_date_texts = json_build_object(ooq.language, to_json(ooq.workshop_date_text)) WHERE ooq.workshop_date_text IS NOT NULL;
UPDATE operation_of_question AS ooq SET workshop_descriptions = json_build_object(ooq.language, to_json(ooq.workshop_description)) WHERE ooq.workshop_description IS NOT NULL;
UPDATE operation_of_question AS ooq SET about_urls = json_build_object(ooq.language, to_json(ooq.about_url)) WHERE ooq.about_url IS NOT NULL;

COMMIT;