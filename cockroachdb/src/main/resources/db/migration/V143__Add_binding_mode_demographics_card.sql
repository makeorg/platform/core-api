BEGIN;

ALTER TABLE demographics_card ADD COLUMN session_binding_mode BOOL NOT NULL DEFAULT false ;

COMMIT;