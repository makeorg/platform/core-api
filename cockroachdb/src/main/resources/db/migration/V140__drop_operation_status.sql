BEGIN;

ALTER TABLE operation DROP COLUMN status;
ALTER TABLE operation_of_question DROP COLUMN landing_sequence_id;

COMMIT;