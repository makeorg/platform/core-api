BEGIN;

INSERT INTO oauth_client (uuid, secret, allowed_grant_types, scope, redirect_uri, created_at, updated_at, default_user_id, roles, name, token_expiration_seconds, refresh_expiration_seconds, reconnect_expiration_seconds) VALUES
    ('${backofficeClientId}', '${backofficeClientSecret}', 'password,refresh_token', NULL, NULL, '2022-04-15 09:39:11.224216+00:00', '2018-04-15 09:39:11.224216+00:00', NULL, 'ROLE_MODERATOR,ROLE_ADMIN', 'default_backoffice', 14400, 1500, 900);

COMMIT;