BEGIN;

ALTER TABLE crm_user DROP COLUMN IF EXISTS account_creation_country;
ALTER TABLE crm_user DROP COLUMN IF EXISTS countries_activity;
ALTER TABLE crm_user DROP COLUMN IF EXISTS last_country_activity;
ALTER TABLE crm_user DROP COLUMN IF EXISTS last_language_activity;

COMMIT;