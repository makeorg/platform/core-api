BEGIN;

ALTER TABLE make_user ADD COLUMN connection_attempts_since_last_successful INT NOT NULL DEFAULT (INT '0'), ADD COLUMN last_failed_connection_attempt TIMESTAMP;

COMMIT;
