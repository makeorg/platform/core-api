BEGIN;

ALTER TABLE operation_of_question DROP COLUMN operation_title;
ALTER TABLE operation_of_question DROP COLUMN intro_card_title;
ALTER TABLE operation_of_question DROP COLUMN intro_card_description;
ALTER TABLE operation_of_question DROP COLUMN final_card_title;
ALTER TABLE operation_of_question DROP COLUMN final_card_share_description;
ALTER TABLE operation_of_question DROP COLUMN final_card_learn_more_title;
ALTER TABLE operation_of_question DROP COLUMN final_card_learn_more_button;
ALTER TABLE operation_of_question DROP COLUMN meta_title;
ALTER TABLE operation_of_question DROP COLUMN meta_description;
ALTER TABLE operation_of_question DROP COLUMN consultation_header_alt;
ALTER TABLE operation_of_question DROP COLUMN consultation_image;
ALTER TABLE operation_of_question DROP COLUMN consultation_image_alt;
ALTER TABLE operation_of_question DROP COLUMN description;
ALTER TABLE operation_of_question DROP COLUMN description_image;
ALTER TABLE operation_of_question DROP COLUMN description_image_alt;
ALTER TABLE operation_of_question DROP COLUMN action_date_text;
ALTER TABLE operation_of_question DROP COLUMN action_description;
ALTER TABLE operation_of_question DROP COLUMN result_date_text;
ALTER TABLE operation_of_question DROP COLUMN result_description;
ALTER TABLE operation_of_question DROP COLUMN workshop_date_text;
ALTER TABLE operation_of_question DROP COLUMN workshop_description;
ALTER TABLE operation_of_question DROP COLUMN about_url;
ALTER TABLE operation_of_question DROP COLUMN language;

COMMIT;