BEGIN;

ALTER TABLE crm_question_template ADD COLUMN language STRING(3) NULL;

COMMIT;
