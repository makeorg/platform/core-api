BEGIN;

UPDATE question AS q SET default_language = q.language;
UPDATE question AS q SET languages = ARRAY[q.language];
UPDATE question AS q SET questions = json_build_object(q.language, to_json(q.question)) WHERE q.question IS NOT NULL;
UPDATE question AS q SET short_titles = json_build_object(q.language, to_json(q.short_title)) WHERE q.short_title IS NOT NULL;

COMMIT;