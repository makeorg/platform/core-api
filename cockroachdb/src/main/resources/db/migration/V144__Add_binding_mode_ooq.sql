BEGIN;

ALTER TABLE operation_of_question ADD COLUMN session_binding_mode BOOL NOT NULL DEFAULT false ;

COMMIT;
