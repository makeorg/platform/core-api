BEGIN;

ALTER TABLE widget ADD COLUMN sequence_kind STRING DEFAULT 'standard';

COMMIT;