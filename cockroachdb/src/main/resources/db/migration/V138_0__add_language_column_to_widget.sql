BEGIN;

ALTER TABLE widget ADD COLUMN language STRING(3) DEFAULT 'fr':::STRING;

COMMIT;