BEGIN;

ALTER TABLE operation_of_question ADD COLUMN cobranding_logo STRING;
ALTER TABLE operation_of_question ADD COLUMN cobranding_logo_alt STRING(130);

COMMIT;