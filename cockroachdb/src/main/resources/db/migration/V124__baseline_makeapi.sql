BEGIN;

SAVEPOINT cockroach_restart;

CREATE TABLE make_user (
	uuid STRING NOT NULL,
	created_at TIMESTAMPTZ NOT NULL,
	updated_at TIMESTAMPTZ NOT NULL,
	email STRING(255) NOT NULL,
	first_name STRING(512) NULL DEFAULT NULL,
	last_name STRING(512) NULL DEFAULT NULL,
	last_ip STRING(50) NULL DEFAULT NULL,
	hashed_password STRING(512) NULL DEFAULT NULL,
	enabled BOOL NOT NULL,
	email_verified BOOL NOT NULL,
	last_connection TIMESTAMPTZ NULL DEFAULT NULL,
	verification_token STRING(255) NULL DEFAULT NULL,
	verification_token_expires_at TIMESTAMPTZ NULL,
	reset_token STRING(255) NULL DEFAULT NULL,
	reset_token_expires_at TIMESTAMPTZ NULL,
	date_of_birth DATE NULL DEFAULT NULL,
	avatar_url STRING(2048) NULL DEFAULT NULL,
	profession STRING(512) NULL DEFAULT NULL,
	phone_number STRING(50) NULL DEFAULT NULL,
	twitter_id STRING(255) NULL DEFAULT NULL,
	facebook_id STRING(255) NULL DEFAULT NULL,
	google_id STRING(255) NULL DEFAULT NULL,
	gender STRING(1) NULL DEFAULT NULL,
	gender_name STRING(20) NULL DEFAULT NULL,
	postal_code STRING(10) NULL DEFAULT NULL,
	karma_level INT8 NULL DEFAULT 0:::INT8,
	locale STRING(8) NULL DEFAULT NULL,
	opt_in_newsletter BOOL NOT NULL DEFAULT false,
	country STRING(3) NULL DEFAULT 'FR':::STRING,
	language STRING(3) NULL DEFAULT 'fr':::STRING,
	is_hard_bounce BOOL NOT NULL DEFAULT false,
	last_mailing_error_date TIMESTAMPTZ NULL,
	last_mailing_error_message STRING(255) NULL,
	organisation_name STRING(255) NULL,
	description STRING NULL,
	public_profile BOOL NULL DEFAULT false,
	socio_professional_category STRING(1024) NULL,
	register_question_id STRING(256) NULL,
	opt_in_partner BOOL NULL,
	available_questions STRING[] NULL,
	reconnect_token STRING NULL DEFAULT NULL,
	reconnect_token_created_at TIMESTAMPTZ NULL DEFAULT NULL,
	anonymous_participation BOOL NOT NULL DEFAULT false,
	website STRING NULL,
	political_party STRING NULL,
	user_type STRING NULL DEFAULT 'USER':::STRING,
	legal_minor_consent BOOL NULL DEFAULT NULL,
	legal_advisor_approval BOOL NULL DEFAULT NULL,
	privacy_policy_approval_date TIMESTAMPTZ NULL,
	roles STRING[] NULL,
	CONSTRAINT "primary" PRIMARY KEY (uuid ASC),
	UNIQUE INDEX email_unique_index (email ASC),
	INDEX operation_action_user_id_index (uuid ASC),
	INDEX make_user_email_idx (email ASC),
	INDEX make_user_verification_token_idx (verification_token ASC),
	INDEX make_user_reset_token_idx (reset_token ASC),
	INDEX make_user_public_profile_idx (public_profile ASC),
	INDEX index_register_question_id (register_question_id ASC),
	INDEX make_user_created_at_idx (created_at DESC),
	INDEX index_user_type (user_type ASC),
	FAMILY "primary" (uuid, created_at, updated_at, email, first_name, last_name, last_ip, hashed_password, enabled, email_verified, last_connection, verification_token, verification_token_expires_at, reset_token, reset_token_expires_at, date_of_birth, avatar_url, profession, phone_number, twitter_id, facebook_id, google_id, gender, gender_name, postal_code, karma_level, locale, opt_in_newsletter, country, language, is_hard_bounce, last_mailing_error_date, last_mailing_error_message, organisation_name, description, public_profile, socio_professional_category, register_question_id, opt_in_partner, available_questions, reconnect_token, reconnect_token_created_at, anonymous_participation, website, political_party, user_type, legal_minor_consent, legal_advisor_approval, privacy_policy_approval_date, roles)
);

CREATE TABLE oauth_client (
	uuid STRING(256) NOT NULL,
	secret STRING(256) NULL,
	allowed_grant_types STRING(256) NULL,
	scope STRING(2048) NULL,
	redirect_uri STRING(2048) NULL,
	created_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	updated_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	default_user_id STRING(256) NULL,
	roles STRING NOT NULL DEFAULT '':::STRING,
	name STRING NOT NULL DEFAULT '':::STRING,
	token_expiration_seconds INT4 NOT NULL DEFAULT 300:::INT8,
	refresh_expiration_seconds INT8 NOT NULL DEFAULT 1500:::INT8,
	reconnect_expiration_seconds INT8 NOT NULL DEFAULT 900:::INT8,
	CONSTRAINT "primary" PRIMARY KEY (uuid ASC),
	INDEX oauth_client_secret_idx (secret ASC),
	INDEX index_default_user_id (default_user_id ASC),
	FAMILY "primary" (uuid, secret, allowed_grant_types, scope, redirect_uri, created_at, updated_at, default_user_id, roles, name, token_expiration_seconds, refresh_expiration_seconds, reconnect_expiration_seconds)
);

CREATE TABLE access_token (
	access_token STRING(256) NOT NULL,
	refresh_token STRING(256) NULL,
	scope STRING(2048) NULL,
	created_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	updated_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	expires_in INT8 NOT NULL,
	make_user_uuid STRING(256) NOT NULL,
	client_uuid STRING(256) NOT NULL,
	refresh_expires_in INT8 NOT NULL DEFAULT 1500:::INT8,
	CONSTRAINT "primary" PRIMARY KEY (access_token ASC),
	INDEX access_token_auto_index_fk_make_user_uuid_ref_make_user (make_user_uuid ASC),
	INDEX access_token_auto_index_fk_client_uuid_ref_oauth_client (client_uuid ASC),
	INDEX access_token_refresh_token_idx (refresh_token ASC),
	FAMILY "primary" (access_token, refresh_token, scope, created_at, updated_at, expires_in, make_user_uuid, client_uuid, refresh_expires_in)
);

CREATE TABLE demographics_card (
	id STRING NOT NULL,
	name STRING(256) NOT NULL,
	layout STRING NOT NULL,
	data_type STRING NOT NULL,
	language STRING NOT NULL,
	title STRING(64) NOT NULL,
	parameters STRING NOT NULL,
	created_at TIMESTAMPTZ NOT NULL,
	updated_at TIMESTAMPTZ NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	INDEX demographics_card_data_type (data_type ASC),
	INDEX demographics_card_language (language ASC),
	FAMILY "primary" (id, name, layout, data_type, language, title, parameters, created_at, updated_at)
);

CREATE TABLE question (
	question_id STRING(256) NOT NULL,
	countries STRING(256) NOT NULL,
	language STRING(64) NOT NULL,
	created_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	updated_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	operation_id STRING(256) NULL,
	question STRING NULL,
	slug STRING(512) NOT NULL,
	short_title STRING(30) NULL,
	CONSTRAINT "primary" PRIMARY KEY (question_id ASC),
	UNIQUE INDEX question_slug_unique (slug ASC),
	INDEX question_slug_idx (slug ASC),
	FAMILY "primary" (question_id, countries, language, created_at, updated_at, operation_id, question, slug, short_title)
);

CREATE TABLE active_demographics_card (
	id STRING(256) NOT NULL,
	demographics_card_id STRING(256) NOT NULL,
	question_id STRING(256) NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	INDEX active_demographics_card_auto_index_fk_active_demographics_card_demographics_card_id (demographics_card_id ASC),
	INDEX active_demographics_card_auto_index_fk_active_demographics_card_question_id (question_id ASC),
	UNIQUE INDEX question_card_unicity (demographics_card_id ASC, question_id ASC),
	FAMILY "primary" (id, demographics_card_id, question_id)
);

CREATE TABLE feature (
	id STRING NOT NULL,
	slug STRING NOT NULL,
	name STRING NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	UNIQUE INDEX slug_unique_index (slug ASC),
	FAMILY "primary" (id, slug, name)
);

CREATE TABLE active_feature (
	id STRING NOT NULL,
	feature_id STRING NOT NULL,
	question_id STRING NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	INDEX active_feature_auto_index_fk_active_feature_feature_id (feature_id ASC),
	INDEX active_feature_auto_index_fk_active_feature_question_id (question_id ASC),
	UNIQUE INDEX feature_question_unicity (feature_id ASC, question_id ASC),
	FAMILY "primary" (id, feature_id, question_id)
);

CREATE TABLE auth_code (
	authorization_code STRING(256) NOT NULL,
	scope STRING(2048) NULL,
	redirect_uri STRING(2048) NULL,
	created_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	expires_in INT8 NOT NULL,
	make_user_uuid STRING(256) NOT NULL,
	client_uuid STRING(256) NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (authorization_code ASC),
	INDEX auth_code_auto_index_fk_make_user_uuid_ref_make_user (make_user_uuid ASC),
	INDEX auth_code_auto_index_fk_client_uuid_ref_oauth_client (client_uuid ASC),
	FAMILY "primary" (authorization_code, scope, redirect_uri, created_at, expires_in, make_user_uuid, client_uuid)
);

CREATE TABLE crm_template_kind (
	id STRING NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	FAMILY "primary" (id)
);

CREATE TABLE crm_language_template (
	id STRING NOT NULL,
	kind STRING NOT NULL,
	language STRING NOT NULL,
	template STRING NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	UNIQUE INDEX crm_language_template_kind_language (kind ASC, language ASC),
	FAMILY "primary" (id, kind, language, template)
);

CREATE TABLE crm_question_template (
	id STRING NOT NULL,
	kind STRING NOT NULL,
	question_id STRING NOT NULL,
	template STRING NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	UNIQUE INDEX crm_question_template_kind_question_id (kind ASC, question_id ASC),
	INDEX crm_question_template_auto_index_fk_crm_question_template_question_id_ref_question (question_id ASC),
	FAMILY "primary" (id, kind, question_id, template)
);

CREATE TABLE crm_user (
	user_id STRING NOT NULL,
	email STRING NOT NULL,
	firstname STRING NULL,
	zipcode STRING NULL,
	date_of_birth STRING NULL,
	email_validation_status BOOL NULL,
	email_hardbounce_status BOOL NULL,
	unsubscribe_status BOOL NULL,
	account_creation_date STRING NULL,
	account_creation_source STRING NULL,
	account_creation_origin STRING NULL,
	account_creation_operation STRING NULL,
	account_creation_country STRING NULL,
	countries_activity STRING NULL,
	last_country_activity STRING NULL,
	last_language_activity STRING NULL,
	total_number_proposals INT8 NULL,
	total_number_votes INT8 NULL,
	first_contribution_date STRING NULL,
	last_contribution_date STRING NULL,
	operation_activity STRING NULL,
	source_activity STRING NULL,
	days_of_activity INT8 NULL,
	days_of_activity30d INT8 NULL,
	user_type STRING NULL,
	full_name STRING NULL,
	account_type STRING(256) NULL,
	account_creation_location STRING NULL DEFAULT NULL,
	days_before_deletion INT8 NULL,
	last_activity_date STRING NULL,
	sessions_count INT8 NULL,
	events_count INT8 NULL,
	CONSTRAINT "primary" PRIMARY KEY (user_id ASC),
	INDEX crm_user_days_before_deletion_idx (days_before_deletion ASC),
	FAMILY "primary" (user_id, email, firstname, zipcode, date_of_birth, email_validation_status, email_hardbounce_status, unsubscribe_status, account_creation_date, account_creation_source, account_creation_origin, account_creation_operation, account_creation_country, countries_activity, last_country_activity, last_language_activity, total_number_proposals, total_number_votes, first_contribution_date, last_contribution_date, operation_activity, source_activity, days_of_activity, days_of_activity30d, user_type, full_name, account_type, account_creation_location, days_before_deletion, last_activity_date, sessions_count, events_count)
);

CREATE TABLE exploration_sequence_configuration (
	exploration_sequence_configuration_id STRING NOT NULL,
	sequence_size INT8 NOT NULL,
	max_tested_proposal_count INT8 NOT NULL,
	new_ratio DECIMAL NOT NULL,
	controversy_ratio DECIMAL NOT NULL,
	top_sorter STRING NOT NULL,
	controversy_sorter STRING NOT NULL,
	keywords_threshold DECIMAL NOT NULL DEFAULT 0.2:::DECIMAL,
	candidates_pool_size INT8 NOT NULL DEFAULT 10:::INT8,
	CONSTRAINT "primary" PRIMARY KEY (exploration_sequence_configuration_id ASC),
	FAMILY "primary" (exploration_sequence_configuration_id, sequence_size, max_tested_proposal_count, new_ratio, controversy_ratio, top_sorter, controversy_sorter, keywords_threshold, candidates_pool_size)
);

CREATE TABLE followed_user (
	user_id STRING(256) NOT NULL,
	followed_user_id STRING(256) NOT NULL,
	date TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	CONSTRAINT "primary" PRIMARY KEY (user_id ASC, followed_user_id ASC),
	INDEX followed_user_auto_index_fk_followed_user_id_ref_make_user (followed_user_id ASC),
	FAMILY "primary" (user_id, followed_user_id, date)
);

CREATE TABLE idea (
	id STRING(256) NOT NULL,
	name STRING NULL,
	operation_id STRING(256) NULL,
	question STRING(256) NULL,
	created_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	updated_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	status STRING(20) NULL,
	question_id STRING(256) NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	FAMILY "primary" (id, name, operation_id, question, created_at, updated_at, status, question_id)
);

CREATE TABLE keyword (
	question_id STRING NOT NULL,
	key STRING NOT NULL,
	label STRING NOT NULL,
	score FLOAT8 NOT NULL,
	count INT8 NOT NULL,
	top_keyword BOOL NULL DEFAULT false,
	CONSTRAINT "primary" PRIMARY KEY (question_id ASC, key ASC),
	FAMILY "primary" (question_id, key, label, score, count, top_keyword)
);

CREATE TABLE operation (
	uuid STRING(256) NOT NULL,
	status STRING(20) NOT NULL,
	slug STRING(256) NOT NULL,
	created_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	updated_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	operation_kind STRING(256) NULL,
	CONSTRAINT "primary" PRIMARY KEY (uuid ASC),
	UNIQUE INDEX operation_slug_unique_index (slug ASC),
	INDEX operation_slug_idx (slug ASC),
	FAMILY "primary" (uuid, status, slug, created_at, updated_at, operation_kind)
);

CREATE TABLE operation_action (
	operation_uuid STRING(256) NOT NULL,
	make_user_uuid STRING(256) NOT NULL,
	action_date TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	action_type STRING(256) NOT NULL,
	arguments STRING NULL,
	INDEX operation_action_auto_index_fk_operation_uuid_ref_operation (operation_uuid ASC),
	INDEX operation_action_auto_index_fk_make_user_uuid_ref_make_user (make_user_uuid ASC),
	INDEX operation_action_operation_id_index (operation_uuid ASC),
	FAMILY "primary" (operation_uuid, make_user_uuid, action_date, action_type, arguments, rowid)
);

CREATE TABLE operation_of_question (
	question_id STRING(256) NOT NULL,
	operation_id STRING(256) NOT NULL,
	created_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	updated_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	operation_title STRING(512) NULL,
	landing_sequence_id STRING(256) NOT NULL,
	can_propose BOOL NOT NULL DEFAULT true,
	intro_card_enabled BOOL NULL DEFAULT false,
	intro_card_title STRING(256) NULL,
	intro_card_description STRING(1024) NULL,
	push_proposal_card_enabled BOOL NULL DEFAULT false,
	final_card_enabled BOOL NULL DEFAULT false,
	final_card_sharing_enabled BOOL NULL DEFAULT false,
	final_card_title STRING(256) NULL,
	final_card_share_description STRING(1024) NULL,
	final_card_learn_more_title STRING(256) NULL,
	final_card_learn_more_button STRING(256) NULL,
	final_card_link_url STRING(2048) NULL,
	about_url STRING(2048) NULL,
	meta_title STRING(256) NULL,
	meta_description STRING(1024) NULL,
	meta_picture STRING(1024) NULL,
	start_date TIMESTAMP NULL,
	end_date TIMESTAMP NULL,
	color STRING NOT NULL DEFAULT '#000000':::STRING,
	font_color STRING NOT NULL DEFAULT '#FFFFFF':::STRING,
	description STRING(1024) NOT NULL DEFAULT '':::STRING,
	consultation_image STRING NULL,
	description_image STRING NULL,
	results_link STRING NULL DEFAULT NULL,
	proposals_count INT8 NULL DEFAULT 0:::INT8,
	participants_count INT8 NULL DEFAULT 0:::INT8,
	actions STRING NULL DEFAULT NULL,
	featured BOOL NULL DEFAULT false,
	description_image_alt STRING(130) NULL DEFAULT NULL,
	consultation_image_alt STRING(130) NULL DEFAULT NULL,
	votes_target INT8 NOT NULL DEFAULT 100000:::INT8,
	votes_count INT8 NOT NULL DEFAULT 0:::INT8,
	result_date DATE NULL,
	workshop_date DATE NULL,
	action_date DATE NULL,
	action_date_text STRING(20) NULL,
	action_description STRING(150) NULL,
	result_date_text STRING(20) NULL,
	result_description STRING(150) NULL,
	workshop_date_text STRING(20) NULL,
	workshop_description STRING(150) NULL,
	CONSTRAINT "primary" PRIMARY KEY (question_id ASC),
	INDEX operation_of_question_auto_index_fk_question_of_operation_operation (operation_id ASC),
	FAMILY "primary" (question_id, operation_id, created_at, updated_at, operation_title, landing_sequence_id, can_propose, intro_card_enabled, intro_card_title, intro_card_description, push_proposal_card_enabled, final_card_enabled, final_card_sharing_enabled, final_card_title, final_card_share_description, final_card_learn_more_title, final_card_learn_more_button, final_card_link_url, about_url, meta_title, meta_description, meta_picture, start_date, end_date, color, font_color, description, consultation_image, description_image, results_link, proposals_count, participants_count, actions, featured, description_image_alt, consultation_image_alt, votes_target, votes_count, result_date, workshop_date, action_date, action_date_text, action_description, result_date_text, result_description, workshop_date_text, workshop_description)
);

CREATE TABLE partner (
	id STRING NOT NULL,
	name STRING NULL,
	logo STRING NULL,
	link STRING NULL,
	organisation_id STRING NULL,
	partner_kind STRING NOT NULL,
	question_id STRING NOT NULL,
	weight FLOAT8 NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	INDEX partner_auto_index_fk_partner_question_id (question_id ASC),
	INDEX partner_auto_index_fk_partner_organisation_id (organisation_id ASC),
	FAMILY "primary" (id, name, logo, link, organisation_id, partner_kind, question_id, weight)
);

CREATE TABLE personality_role (
	id STRING NOT NULL,
	name STRING NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	UNIQUE INDEX personality_role_name_key (name ASC),
	FAMILY "primary" (id, name)
);

CREATE TABLE personality (
	id STRING NOT NULL,
	user_id STRING NULL,
	question_id STRING NULL,
	personality_role_id STRING NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	INDEX personality_auto_index_fk_personality_user_id (user_id ASC),
	INDEX personality_auto_index_fk_personality_question_id (question_id ASC),
	UNIQUE INDEX index_personality_unicity (user_id ASC, question_id ASC),
	INDEX personality_personality_role_id_idx (personality_role_id ASC),
	FAMILY "primary" (id, user_id, question_id, personality_role_id)
);

CREATE TABLE personality_role_field (
	id STRING NOT NULL,
	personality_role_id STRING NOT NULL,
	name STRING NOT NULL,
	field_type STRING NOT NULL,
	required BOOL NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	UNIQUE INDEX index_name_uniqueness (personality_role_id ASC, name ASC),
	FAMILY "primary" (id, personality_role_id, name, field_type, required)
);

CREATE TABLE specific_sequence_configuration (
	id STRING NOT NULL,
	sequence_size INT8 NULL,
	new_proposals_ratio DECIMAL NULL,
	max_tested_proposal_count INT8 NULL,
	selection_algorithm_name STRING NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	FAMILY "primary" (id, sequence_size, new_proposals_ratio, max_tested_proposal_count, selection_algorithm_name)
);

CREATE TABLE sequence_configuration (
	sequence_id STRING(256) NOT NULL,
	new_proposals_ratio DECIMAL NULL DEFAULT 0.5:::DECIMAL,
	new_proposals_vote_threshold INT8 NULL DEFAULT 100:::INT8,
	tested_proposals_engagement_threshold DECIMAL NULL DEFAULT 0.8:::DECIMAL,
	intra_idea_enabled BOOL NULL DEFAULT true,
	intra_idea_min_count INT8 NULL DEFAULT 3:::INT8,
	intra_idea_proposals_ratio DECIMAL NULL DEFAULT 0.3:::DECIMAL,
	created_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	updated_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	tested_proposals_score_threshold DECIMAL NULL DEFAULT 0.0:::DECIMAL,
	tested_proposals_controversy_threshold DECIMAL NULL DEFAULT 0.0:::DECIMAL,
	inter_idea_competition_enabled BOOL NULL DEFAULT false,
	inter_idea_competition_target_count INT8 NULL DEFAULT 50:::INT8,
	inter_idea_competition_controversial_ratio DECIMAL NULL DEFAULT 0.0:::DECIMAL,
	inter_idea_competition_controversial_count INT8 NULL DEFAULT 50:::INT8,
	tested_proposals_max_votes_threshold INT8 NULL DEFAULT 1500:::INT8,
	max_tested_proposal_count INT8 NULL,
	question_id STRING(256) NOT NULL,
	selection_algorithm_name STRING NOT NULL DEFAULT 'Bandit':::STRING,
	non_sequence_votes_weight DECIMAL NULL DEFAULT 0.5:::DECIMAL,
	main STRING NULL,
	controversial STRING NULL,
	popular STRING NULL,
	keyword STRING NULL,
	CONSTRAINT "primary" PRIMARY KEY (sequence_id ASC),
	INDEX sequence_configuration_question_id_idx (question_id ASC),
	INDEX index_configuration_main (main ASC),
	INDEX index_configuration_controversial (controversial ASC),
	INDEX index_configuration_popular (popular ASC),
	INDEX index_configuration_keyword (keyword ASC),
	FAMILY "primary" (sequence_id, new_proposals_ratio, new_proposals_vote_threshold, tested_proposals_engagement_threshold, intra_idea_enabled, intra_idea_min_count, intra_idea_proposals_ratio, created_at, updated_at, tested_proposals_score_threshold, tested_proposals_controversy_threshold, inter_idea_competition_enabled, inter_idea_competition_target_count, inter_idea_competition_controversial_ratio, inter_idea_competition_controversial_count, tested_proposals_max_votes_threshold, max_tested_proposal_count, question_id, selection_algorithm_name, non_sequence_votes_weight, main, controversial, popular, keyword)
);

CREATE TABLE source (
	id STRING NOT NULL,
	name STRING NOT NULL,
	source STRING NOT NULL,
	created_at TIMESTAMPTZ NOT NULL,
	updated_at TIMESTAMPTZ NOT NULL,
	user_id STRING NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	UNIQUE INDEX source_source_key (source ASC),
	INDEX source_auto_index_fk_source_user_id (user_id ASC),
	FAMILY "primary" (id, name, source, created_at, updated_at, user_id)
);

CREATE TABLE tag_type (
	id STRING(256) NOT NULL,
	label STRING(256) NULL,
	display STRING(64) NOT NULL,
	created_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	updated_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	weight_type INT8 NULL,
	required_for_enrichment BOOL NULL DEFAULT false,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	FAMILY "primary" (id, label, display, created_at, updated_at, weight_type, required_for_enrichment)
);

CREATE TABLE tag (
	id STRING(256) NOT NULL,
	label STRING(256) NULL,
	created_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	updated_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	display STRING(64) NOT NULL DEFAULT 'INHERIT':::STRING,
	tag_type_id STRING(256) NULL,
	operation_id STRING(256) NULL,
	weight FLOAT8 NOT NULL DEFAULT 0.0:::FLOAT8,
	question_id STRING(256) NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	INDEX tag_tag_type_id_idx (tag_type_id ASC),
	INDEX tag_operation_id_idx (operation_id ASC),
	INDEX tag_question_id_idx (question_id ASC),
	FAMILY "primary" (id, label, created_at, updated_at, display, tag_type_id, operation_id, weight, question_id)
);

CREATE TABLE top_idea (
	id STRING NOT NULL,
	idea_id STRING NOT NULL,
	question_id STRING NOT NULL,
	name STRING NULL,
	total_proposals_ratio DECIMAL NULL,
	agreement_ratio DECIMAL NULL,
	like_it_ratio DECIMAL NULL,
	weight DECIMAL NULL,
	label STRING(256) NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	INDEX top_idea_auto_index_fk_top_idea_question_id (question_id ASC),
	INDEX top_idea_auto_index_fk_top_idea_idea_id (idea_id ASC),
	FAMILY "primary" (id, idea_id, question_id, name, total_proposals_ratio, agreement_ratio, like_it_ratio, weight, label)
);

CREATE TABLE top_idea_comment (
	id STRING NOT NULL,
	top_idea_id STRING NOT NULL,
	personality_id STRING NOT NULL,
	comment1 STRING(280) NULL DEFAULT NULL,
	comment2 STRING(280) NULL DEFAULT NULL,
	comment3 STRING(280) NULL DEFAULT NULL,
	vote STRING NULL DEFAULT NULL,
	qualification STRING NULL DEFAULT NULL,
	created_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	updated_at TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	UNIQUE INDEX index_tags_uniqueness (top_idea_id ASC, personality_id ASC),
	INDEX top_idea_comment_auto_index_fk_top_idea_comment_personality_id (personality_id ASC),
	FAMILY "primary" (id, top_idea_id, personality_id, comment1, comment2, comment3, vote, qualification, created_at, updated_at)
);

CREATE TABLE user_to_anonymize (
	email STRING(256) NOT NULL,
	request_date TIMESTAMPTZ NULL DEFAULT now():::TIMESTAMPTZ,
	CONSTRAINT "primary" PRIMARY KEY (email ASC),
	FAMILY "primary" (email, request_date)
);

CREATE TABLE widget (
	id STRING(256) NOT NULL,
	source_id STRING(256) NOT NULL,
	question_id STRING(256) NOT NULL,
	country STRING(3) NOT NULL,
	author STRING(256) NOT NULL,
	version STRING(3) NOT NULL,
	script STRING NOT NULL,
	created_at TIMESTAMPTZ NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	INDEX widget_auto_index_fk_widget_source_id (source_id ASC),
	INDEX widget_auto_index_fk_widget_question_id (question_id ASC),
	INDEX widget_auto_index_fk_widget_author (author ASC),
	FAMILY "primary" (id, source_id, question_id, country, author, version, script, created_at)
);

INSERT INTO make_user (uuid, created_at, updated_at, email, first_name, last_name, last_ip, hashed_password, enabled, email_verified, last_connection, verification_token, verification_token_expires_at, reset_token, reset_token_expires_at, date_of_birth, avatar_url, profession, phone_number, twitter_id, facebook_id, google_id, gender, gender_name, postal_code, karma_level, locale, opt_in_newsletter, country, language, is_hard_bounce, last_mailing_error_date, last_mailing_error_message, organisation_name, description, public_profile, socio_professional_category, register_question_id, opt_in_partner, available_questions, reconnect_token, reconnect_token_created_at, anonymous_participation, website, political_party, user_type, legal_minor_consent, legal_advisor_approval, privacy_policy_approval_date, roles) VALUES
	('11111111-1111-1111-1111-111111111111', '2017-09-15 08:43:30+00:00', '2017-09-15 08:43:30+00:00', '${adminEmail}', '${adminFirstName}', NULL, NULL, '${adminEncryptedPassword}', true, false, '2017-09-15 08:43:30+00:00', NULL, '2017-10-15 08:43:30+00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, 'FR', 'fr', false, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, false, NULL, NULL, 'USER', NULL, NULL, NULL, ARRAY['ROLE_ADMIN','ROLE_CITIZEN','ROLE_SUPER_ADMIN']);

INSERT INTO oauth_client (uuid, secret, allowed_grant_types, scope, redirect_uri, created_at, updated_at, default_user_id, roles, name, token_expiration_seconds, refresh_expiration_seconds, reconnect_expiration_seconds) VALUES
	('${clientId}', '${clientSecret}', 'password,refresh_token', NULL, NULL, '2018-03-06 09:39:11.224216+00:00', '2018-03-06 09:39:11.224216+00:00', NULL, 'ROLE_CITIZEN,ROLE_MODERATOR,ROLE_ADMIN,ROLE_POLITICAL,ROLE_ACTOR', '', 300, 1500, 900);

INSERT INTO feature (id, slug, name) VALUES
	('0aa99246-354c-456f-ad0d-edb1460c3305', 'sequence-custom-data-segment', 'Get the segment from the "segment" custom data'),
	('59d2c4c6-5dab-4675-8754-94784bbd9788', 'display-intro-card-widget', 'Display intro card on widget'),
	('cd344c24-f593-4903-b4e6-116781bf497d', 'segment_from_department', 'Segment from department');

INSERT INTO crm_template_kind (id) VALUES
	('B2BEmailChanged'),
	('B2BForgottenPassword'),
	('B2BProposalAccepted'),
	('B2BProposalRefused'),
	('B2BRegistration'),
	('ForgottenPassword'),
	('MessageToProposer'),
	('ProposalAccepted'),
	('ProposalRefused'),
	('Registration'),
	('ResendRegistration'),
	('Welcome');

INSERT INTO personality_role (id, name) VALUES
	('0c3cbbf4-42c1-4801-b08a-d0e60d136041', 'CANDIDATE');

INSERT INTO tag_type (id, label, display, created_at, updated_at, weight_type, required_for_enrichment) VALUES
	('1edeb3ff-a6fb-4002-99f8-622b8d8655b2', 'Domain', 'DISPLAYED', '2022-02-07 15:41:14.050388+00:00', '2022-02-07 15:41:14.050388+00:00', 60, false),
	('226070ac-51b0-4e92-883a-f0a24d5b8525', 'Target', 'HIDDEN', '2022-02-07 15:41:12.600658+00:00', '2022-02-07 15:41:12.600658+00:00', 40, false),
	('5e539923-c265-45d2-9d0b-77f29c8b0a06', 'Moment', 'HIDDEN', '2022-02-07 15:41:12.600658+00:00', '2022-02-07 15:41:12.600658+00:00', 30, false),
	('8405aba4-4192-41d2-9a0d-b5aa6cb98d37', 'Legacy', 'DISPLAYED', '2022-02-07 15:41:12.600658+00:00', '2022-02-07 15:41:12.600658+00:00', 60, false),
	('982e6860-eb66-407e-bafb-461c2d927478', 'Actor', 'HIDDEN', '2022-02-07 15:41:12.600658+00:00', '2022-02-07 15:41:12.600658+00:00', 50, false),
	('c0d8d858-8b04-4dd9-add6-fa65443b622b', 'Stake', 'DISPLAYED', '2022-02-07 15:41:12.600658+00:00', '2022-02-07 15:41:12.600658+00:00', 10, false),
	('cc6a16a5-cfa7-495b-a235-08affb3551af', 'Solution type', 'DISPLAYED', '2022-02-07 15:41:12.600658+00:00', '2022-02-07 15:41:12.600658+00:00', 20, false);

ALTER TABLE oauth_client ADD CONSTRAINT fk_default_user_id_user FOREIGN KEY (default_user_id) REFERENCES make_user (uuid);
ALTER TABLE access_token ADD CONSTRAINT fk_make_user_uuid_ref_make_user FOREIGN KEY (make_user_uuid) REFERENCES make_user (uuid);
ALTER TABLE access_token ADD CONSTRAINT fk_client_uuid_ref_oauth_client FOREIGN KEY (client_uuid) REFERENCES oauth_client (uuid);
ALTER TABLE active_demographics_card ADD CONSTRAINT fk_active_demographics_card_demographics_card_id FOREIGN KEY (demographics_card_id) REFERENCES demographics_card (id);
ALTER TABLE active_demographics_card ADD CONSTRAINT fk_active_demographics_card_question_id FOREIGN KEY (question_id) REFERENCES question (question_id);
ALTER TABLE active_feature ADD CONSTRAINT fk_active_feature_feature_id FOREIGN KEY (feature_id) REFERENCES feature (id);
ALTER TABLE active_feature ADD CONSTRAINT fk_active_feature_question_id FOREIGN KEY (question_id) REFERENCES question (question_id);
ALTER TABLE auth_code ADD CONSTRAINT fk_make_user_uuid_ref_make_user FOREIGN KEY (make_user_uuid) REFERENCES make_user (uuid);
ALTER TABLE auth_code ADD CONSTRAINT fk_client_uuid_ref_oauth_client FOREIGN KEY (client_uuid) REFERENCES oauth_client (uuid);
ALTER TABLE crm_language_template ADD CONSTRAINT fk_crm_language_template_kind_ref_crm_template_kind FOREIGN KEY (kind) REFERENCES crm_template_kind (id);
ALTER TABLE crm_question_template ADD CONSTRAINT fk_crm_question_template_kind_ref_crm_template_kind FOREIGN KEY (kind) REFERENCES crm_template_kind (id);
ALTER TABLE crm_question_template ADD CONSTRAINT fk_crm_question_template_question_id_ref_question FOREIGN KEY (question_id) REFERENCES question (question_id);
ALTER TABLE followed_user ADD CONSTRAINT fk_followed_user_id_ref_make_user FOREIGN KEY (followed_user_id) REFERENCES make_user (uuid);
ALTER TABLE followed_user ADD CONSTRAINT fk_user_id_ref_make_user FOREIGN KEY (user_id) REFERENCES make_user (uuid);
ALTER TABLE keyword ADD CONSTRAINT fk_keyword_question_id_ref_question FOREIGN KEY (question_id) REFERENCES question (question_id);
ALTER TABLE operation_action ADD CONSTRAINT fk_operation_uuid_ref_operation FOREIGN KEY (operation_uuid) REFERENCES operation (uuid);
ALTER TABLE operation_action ADD CONSTRAINT fk_make_user_uuid_ref_make_user FOREIGN KEY (make_user_uuid) REFERENCES make_user (uuid);
ALTER TABLE operation_of_question ADD CONSTRAINT fk_question_of_operation_operation FOREIGN KEY (operation_id) REFERENCES operation (uuid);
ALTER TABLE operation_of_question ADD CONSTRAINT fk_question_of_operation_question FOREIGN KEY (question_id) REFERENCES question (question_id);
ALTER TABLE partner ADD CONSTRAINT fk_partner_question_id FOREIGN KEY (question_id) REFERENCES question (question_id);
ALTER TABLE partner ADD CONSTRAINT fk_partner_organisation_id FOREIGN KEY (organisation_id) REFERENCES make_user (uuid);
ALTER TABLE personality ADD CONSTRAINT fk_personality_user_id FOREIGN KEY (user_id) REFERENCES make_user (uuid);
ALTER TABLE personality ADD CONSTRAINT fk_personality_question_id FOREIGN KEY (question_id) REFERENCES question (question_id);
ALTER TABLE personality ADD CONSTRAINT fk_personality_role_id FOREIGN KEY (personality_role_id) REFERENCES personality_role (id);
ALTER TABLE personality_role_field ADD CONSTRAINT fk_personality_role_id FOREIGN KEY (personality_role_id) REFERENCES personality_role (id);
ALTER TABLE sequence_configuration ADD CONSTRAINT fk_configuration_main FOREIGN KEY (main) REFERENCES exploration_sequence_configuration (exploration_sequence_configuration_id);
ALTER TABLE sequence_configuration ADD CONSTRAINT fk_configuration_controversial FOREIGN KEY (controversial) REFERENCES specific_sequence_configuration (id);
ALTER TABLE sequence_configuration ADD CONSTRAINT fk_configuration_popular FOREIGN KEY (popular) REFERENCES specific_sequence_configuration (id);
ALTER TABLE sequence_configuration ADD CONSTRAINT fk_configuration_keyword FOREIGN KEY (keyword) REFERENCES specific_sequence_configuration (id);
ALTER TABLE source ADD CONSTRAINT fk_source_user_id FOREIGN KEY (user_id) REFERENCES make_user (uuid);
ALTER TABLE tag ADD CONSTRAINT tag_type_fk FOREIGN KEY (tag_type_id) REFERENCES tag_type (id) ON DELETE RESTRICT;
ALTER TABLE tag ADD CONSTRAINT operation_fk FOREIGN KEY (operation_id) REFERENCES operation (uuid) ON DELETE RESTRICT;
ALTER TABLE top_idea ADD CONSTRAINT fk_top_idea_question_id FOREIGN KEY (question_id) REFERENCES question (question_id);
ALTER TABLE top_idea ADD CONSTRAINT fk_top_idea_idea_id FOREIGN KEY (idea_id) REFERENCES idea (id);
ALTER TABLE top_idea_comment ADD CONSTRAINT fk_top_idea_comment_top_idea_id FOREIGN KEY (top_idea_id) REFERENCES top_idea (id);
ALTER TABLE top_idea_comment ADD CONSTRAINT fk_top_idea_comment_personality_id FOREIGN KEY (personality_id) REFERENCES make_user (uuid);
ALTER TABLE widget ADD CONSTRAINT fk_widget_source_id FOREIGN KEY (source_id) REFERENCES source (id);
ALTER TABLE widget ADD CONSTRAINT fk_widget_question_id FOREIGN KEY (question_id) REFERENCES question (question_id);
ALTER TABLE widget ADD CONSTRAINT fk_widget_author FOREIGN KEY (author) REFERENCES make_user (uuid);

-- Validate foreign key constraints. These can fail if there was unvalidated data during the dump.
ALTER TABLE oauth_client VALIDATE CONSTRAINT fk_default_user_id_user;
ALTER TABLE access_token VALIDATE CONSTRAINT fk_make_user_uuid_ref_make_user;
ALTER TABLE access_token VALIDATE CONSTRAINT fk_client_uuid_ref_oauth_client;
ALTER TABLE active_demographics_card VALIDATE CONSTRAINT fk_active_demographics_card_demographics_card_id;
ALTER TABLE active_demographics_card VALIDATE CONSTRAINT fk_active_demographics_card_question_id;
ALTER TABLE active_feature VALIDATE CONSTRAINT fk_active_feature_feature_id;
ALTER TABLE active_feature VALIDATE CONSTRAINT fk_active_feature_question_id;
ALTER TABLE auth_code VALIDATE CONSTRAINT fk_make_user_uuid_ref_make_user;
ALTER TABLE auth_code VALIDATE CONSTRAINT fk_client_uuid_ref_oauth_client;
ALTER TABLE crm_language_template VALIDATE CONSTRAINT fk_crm_language_template_kind_ref_crm_template_kind;
ALTER TABLE crm_question_template VALIDATE CONSTRAINT fk_crm_question_template_kind_ref_crm_template_kind;
ALTER TABLE crm_question_template VALIDATE CONSTRAINT fk_crm_question_template_question_id_ref_question;
ALTER TABLE followed_user VALIDATE CONSTRAINT fk_followed_user_id_ref_make_user;
ALTER TABLE followed_user VALIDATE CONSTRAINT fk_user_id_ref_make_user;
ALTER TABLE keyword VALIDATE CONSTRAINT fk_keyword_question_id_ref_question;
ALTER TABLE operation_action VALIDATE CONSTRAINT fk_operation_uuid_ref_operation;
ALTER TABLE operation_action VALIDATE CONSTRAINT fk_make_user_uuid_ref_make_user;
ALTER TABLE operation_of_question VALIDATE CONSTRAINT fk_question_of_operation_operation;
ALTER TABLE operation_of_question VALIDATE CONSTRAINT fk_question_of_operation_question;
ALTER TABLE partner VALIDATE CONSTRAINT fk_partner_question_id;
ALTER TABLE partner VALIDATE CONSTRAINT fk_partner_organisation_id;
ALTER TABLE personality VALIDATE CONSTRAINT fk_personality_user_id;
ALTER TABLE personality VALIDATE CONSTRAINT fk_personality_question_id;
ALTER TABLE personality VALIDATE CONSTRAINT fk_personality_role_id;
ALTER TABLE personality_role_field VALIDATE CONSTRAINT fk_personality_role_id;
ALTER TABLE sequence_configuration VALIDATE CONSTRAINT fk_configuration_main;
ALTER TABLE sequence_configuration VALIDATE CONSTRAINT fk_configuration_controversial;
ALTER TABLE sequence_configuration VALIDATE CONSTRAINT fk_configuration_popular;
ALTER TABLE sequence_configuration VALIDATE CONSTRAINT fk_configuration_keyword;
ALTER TABLE source VALIDATE CONSTRAINT fk_source_user_id;
ALTER TABLE tag VALIDATE CONSTRAINT tag_type_fk;
ALTER TABLE tag VALIDATE CONSTRAINT operation_fk;
ALTER TABLE top_idea VALIDATE CONSTRAINT fk_top_idea_question_id;
ALTER TABLE top_idea VALIDATE CONSTRAINT fk_top_idea_idea_id;
ALTER TABLE top_idea_comment VALIDATE CONSTRAINT fk_top_idea_comment_top_idea_id;
ALTER TABLE top_idea_comment VALIDATE CONSTRAINT fk_top_idea_comment_personality_id;
ALTER TABLE widget VALIDATE CONSTRAINT fk_widget_source_id;
ALTER TABLE widget VALIDATE CONSTRAINT fk_widget_question_id;
ALTER TABLE widget VALIDATE CONSTRAINT fk_widget_author;

RELEASE SAVEPOINT cockroach_restart;

COMMIT;