BEGIN;

ALTER TABLE crm_user ADD COLUMN favorite_country STRING;
ALTER TABLE crm_user ADD COLUMN favorite_language STRING;

COMMIT;