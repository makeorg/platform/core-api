BEGIN;

ALTER TABLE question ADD COLUMN default_language STRING;
ALTER TABLE question ADD COLUMN questions JSONB;
ALTER TABLE question ADD COLUMN short_titles JSONB;
ALTER TABLE question ADD COLUMN languages VARCHAR ARRAY;

ALTER TABLE operation_of_question ADD COLUMN operation_titles JSONB;
ALTER TABLE operation_of_question ADD COLUMN intro_card_titles JSONB;
ALTER TABLE operation_of_question ADD COLUMN intro_card_descriptions JSONB;
ALTER TABLE operation_of_question ADD COLUMN final_card_titles JSONB;
ALTER TABLE operation_of_question ADD COLUMN final_card_share_descriptions JSONB;
ALTER TABLE operation_of_question ADD COLUMN final_card_learn_more_titles JSONB;
ALTER TABLE operation_of_question ADD COLUMN final_card_learn_more_buttons JSONB;
ALTER TABLE operation_of_question ADD COLUMN meta_titles JSONB;
ALTER TABLE operation_of_question ADD COLUMN meta_descriptions JSONB;
ALTER TABLE operation_of_question ADD COLUMN consultation_header_alts JSONB;
ALTER TABLE operation_of_question ADD COLUMN consultation_images JSONB;
ALTER TABLE operation_of_question ADD COLUMN consultation_image_alts JSONB;
ALTER TABLE operation_of_question ADD COLUMN descriptions JSONB;
ALTER TABLE operation_of_question ADD COLUMN description_images JSONB;
ALTER TABLE operation_of_question ADD COLUMN description_image_alts JSONB;
ALTER TABLE operation_of_question ADD COLUMN action_date_texts JSONB;
ALTER TABLE operation_of_question ADD COLUMN action_descriptions JSONB;
ALTER TABLE operation_of_question ADD COLUMN result_date_texts JSONB;
ALTER TABLE operation_of_question ADD COLUMN result_descriptions JSONB;
ALTER TABLE operation_of_question ADD COLUMN workshop_date_texts JSONB;
ALTER TABLE operation_of_question ADD COLUMN workshop_descriptions JSONB;
ALTER TABLE operation_of_question ADD COLUMN about_urls JSONB;
ALTER TABLE operation_of_question ADD COLUMN language STRING;

COMMIT;