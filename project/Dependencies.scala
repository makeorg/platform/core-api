/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import sbt._

object Dependencies {

  object Versions {
    // Scala 3 compatiblity annotations: August 2024
    val akka = "2.6.19"
    val akkaHttp = "10.2.9"
    val akkaHttpCirce = "1.39.2" // NOK!
    val akkaHttpSwagger = "1.6.0" // NOK!
    val akkaPersistenceCassandra = "1.0.5"
    val akkaPersistenceInMemory = "2.5.15.2" // NOK!
    val akkaSerializationJackson = "2.6.19" // OK
    val alpakkaFile = "2.0.2" // NOK!
    val alpakkaCsv = "2.0.2"
    val apacheMath = "3.6.1" // Java
    val apacheText = "1.10.0" // Java
    val avro4s = "3.0.5-make3" // ~OK
    val avroSerializer = "3.3.3" // Java
    val caliban = "0.10.1" // OK
    val cassandraQueryBuilder = "4.14.0" // Java
    val cats = "2.10.0" // OK
    val circe = "0.14.6" // OK
    val circeGenericExtras = "0.14.4" // NOK!
    val commonsLoggingBridge = "1.7.36" // Java
    val configuration = "1.4.2" // Java
    val constructr = "0.20.0" // NOK!
    val constructrZookeeper = "0.5.0" // NOK!
    val dockerTestkit = "0.9.9" // ~OK - Requires upgrade to stale beta version
    val elastic4s = "7.15.1" // Requires Elastic upgrade to v8.x
    val enumeratum = "1.7.3" // OK
    val flywaydb = "9.15.0" // Java
    val grizzledSlf4j = "1.3.4" // NOK!
    val jakarta = "2.1.3"
    val jaxRsApi = "2.1.1" // Java
    val jersey = "2.35" // Java
    val jsoniter = "2.30.9" // OK
    val jsonLenses = "0.6.2" // NOK!
    val jsoup = "1.14.3" // Java
    val kafka = "3.8.0" // NOK!
    val kamon = "2.2.3" // More recent versions are causing the compiler to crash
    val kanela = "1.0.18" // Java
    val kryoSerializer = "1.1.5" // OK (after upgrade)
    val log4j = "2.17.2" // Java
    val mockito = "1.17.22" // NOK!
    val netty = "4.1.112.Final" // Java
    val postgresql = "42.7.4" // Java
    val refined = "0.11.2" // OK
    val scalaBcrypt = "4.3.0" // NOK!
    val jacksonDatabind = "2.17.2" // Java
    val scalacacheGuava = "0.28.0" // NOK!
    val scalaCheck = "1.18.0" // OK
    val scalaCsv = "2.0.0" // OK
    val scalaOAuth = "1.6.0" // OK
    val scalaTest = "3.2.18" // OK
    val scalaTestScalaCheck = "3.2.18.0" // OK
    val scalike = "4.0.0" // OK
    val slugify = "3.0.7" // Java
    val stamina = "0.1.6" // NOK!
    val swaggerUi = "3.20.9" // Java
    val swiftClient = "1.1.5" // OK
    val zio = "1.0.14" // OK
  }

  object Runtime {
    val cats: ModuleID = "org.typelevel" %% "cats-core" % Versions.cats

    val grizzledSlf4j: ModuleID = "org.clapper"             %% "grizzled-slf4j"  % Versions.grizzledSlf4j
    val logger: ModuleID = "org.apache.logging.log4j"       % "log4j"            % Versions.log4j
    val loggerBridge: ModuleID = "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j
    val commonsLoggingBridge: ModuleID = "org.slf4j"        % "jcl-over-slf4j"   % Versions.commonsLoggingBridge
    val log4jJul: ModuleID = "org.apache.logging.log4j"     % "log4j-jul"        % Versions.log4j

    val nettyEpoll: ModuleID = ("io.netty" % "netty-transport-native-epoll" % Versions.netty).classifier("linux-x86_64")
    val nettyEpollMac: ModuleID =
      ("io.netty" % "netty-transport-native-kqueue" % Versions.netty).classifier("osx-x86_64")
    val nettyAll: ModuleID = "io.netty" % "netty-all" % Versions.netty

    val circeGeneric: ModuleID = "io.circe"                          %% "circe-generic"        % Versions.circe
    val circeParser = "io.circe"                                     %% "circe-parser"         % Versions.circe
    val circeGenericExtra = "io.circe"                               %% "circe-generic-extras" % Versions.circeGenericExtras
    val jsoniter: ModuleID = "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-core"  % Versions.jsoniter
    val jsoniterMacros
      : ModuleID = "com.github.plokhotnyuk.jsoniter-scala"  %% "jsoniter-scala-macros"       % Versions.jsoniter % "provided"
    val akkaCluster: ModuleID = "com.typesafe.akka"         %% "akka-cluster"                % Versions.akka
    val akkaClusterSharding: ModuleID = "com.typesafe.akka" %% "akka-cluster-sharding-typed" % Versions.akka
    val akkaHttp: ModuleID = "com.typesafe.akka"            %% "akka-http"                   % Versions.akkaHttp
    val akkaHttpTestKit: ModuleID = "com.typesafe.akka"     %% "akka-http-testkit"           % Versions.akkaHttp
    val akkaHttpCirce: ModuleID = "de.heikoseeberger"       %% "akka-http-circe"             % Versions.akkaHttpCirce
    val akkaHttpSwagger: ModuleID = ("com.github.swagger-akka-http" %% "swagger-akka-http" % Versions.akkaHttpSwagger)
      .exclude("javax.ws.rs", "jsr311-api")
    val akkaPersistence: ModuleID = "com.typesafe.akka"      %% "akka-persistence-typed" % Versions.akka
    val akkaPersistenceQuery: ModuleID = "com.typesafe.akka" %% "akka-persistence-query" % Versions.akka
    val akkaPersistenceCassandra: ModuleID =
      ("com.typesafe.akka" %% "akka-persistence-cassandra" % Versions.akkaPersistenceCassandra)
        .exclude("io.netty", "netty-handler")
    val akkaSerializationJackson = "com.typesafe.akka" %% "akka-serialization-jackson" % Versions.akkaSerializationJackson
    val jacksonDatabind = "com.fasterxml.jackson.core" % "jackson-databind"            % Versions.jacksonDatabind
    val akkaSlf4j: ModuleID = "com.typesafe.akka"      %% "akka-slf4j"                 % Versions.akka

    val alpakkaFile: ModuleID = "com.lightbend.akka" %% "akka-stream-alpakka-file" % Versions.alpakkaFile
    val alpakkaCsv: ModuleID = "com.lightbend.akka"  %% "akka-stream-alpakka-csv"  % Versions.alpakkaCsv
    val jaxRsApi: ModuleID = "javax.ws.rs"           % "javax.ws.rs-api"           % Versions.jaxRsApi
    val cassandraQueryBuilder
      : ModuleID = "com.datastax.oss" % "java-driver-query-builder" % Versions.cassandraQueryBuilder

    val kryoSerializer: ModuleID = "io.altoo" %% "akka-kryo-serialization" % Versions.kryoSerializer

    val swaggerUi: ModuleID = "org.webjars" % "swagger-ui" % Versions.swaggerUi

    val kamonCore: ModuleID = ("io.kamon" %% "kamon-core" % Versions.kamon).exclude("ch.qos.logback", "logback-classic")
    val kamonExecutors: ModuleID =
      ("io.kamon" %% "kamon-executors" % Versions.kamon).exclude("ch.qos.logback", "logback-classic")
    val kamonAkka: ModuleID =
      ("io.kamon" %% "kamon-akka" % Versions.kamon).exclude("ch.qos.logback", "logback-classic")
    val kamonScalaFutures: ModuleID = "io.kamon" %% "kamon-scala-future" % Versions.kamon
    val kamonAkkaHttp: ModuleID =
      ("io.kamon" %% "kamon-akka-http" % Versions.kamon).exclude("ch.qos.logback", "logback-classic")
    val kamonSystemMetrics: ModuleID = "io.kamon" %% "kamon-system-metrics" % Versions.kamon
    val kamonPrometheus: ModuleID = "io.kamon"    %% "kamon-prometheus"     % Versions.kamon
    val kamonAnnotations: ModuleID = "io.kamon"   %% "kamon-annotation"     % Versions.kamon

    val kanela: ModuleID = "io.kamon" % "kanela-agent" % Versions.kanela

    val constructr: ModuleID = "org.make.constructr" %% "constructr" % Versions.constructr
    val constructrZookeeper: ModuleID =
      ("org.make.constructr" %% "constructr-coordination-zookeeper" % Versions.constructrZookeeper)
        .exclude("log4j", "log4j")

    val scalaOAuth: ModuleID = "com.nulab-inc"      %% "scala-oauth2-core" % Versions.scalaOAuth
    val scalaBcrypt: ModuleID = "com.github.t3hnar" %% "scala-bcrypt"      % Versions.scalaBcrypt

    val scalike: ModuleID = "org.scalikejdbc"       %% "scalikejdbc"                      % Versions.scalike
    val scalikeMacros: ModuleID = "org.scalikejdbc" %% "scalikejdbc-syntax-support-macro" % Versions.scalike
    val postgresql: ModuleID = "org.postgresql"     % "postgresql"                        % Versions.postgresql
    val flywaydb: ModuleID = "org.flywaydb"         % "flyway-core"                       % Versions.flywaydb

    val slugify: ModuleID = "com.github.slugify" % "slugify" % Versions.slugify

    val swiftClient: ModuleID = "org.make" %% "openstack-swift-client" % Versions.swiftClient

    val jsoup: ModuleID = "org.jsoup" % "jsoup" % Versions.jsoup

    // Kafka + AVRO
    val kafkaClients: ModuleID = "org.apache.kafka" % "kafka-clients" % Versions.kafka
    val avro4s: ModuleID = "org.make"               %% "avro4s-core"  % Versions.avro4s
    val avroSerializer: ModuleID =
      ("io.confluent" % "kafka-avro-serializer" % Versions.avroSerializer)
        .exclude("org.slf4j", "slf4j-log4j12")
        .exclude("io.netty", "netty")

    val configuration: ModuleID = "com.typesafe" % "config" % Versions.configuration

    val elastic4s: ModuleID = "com.sksamuel.elastic4s"      %% "elastic4s-core"        % Versions.elastic4s
    val elastic4sHttp: ModuleID = "com.sksamuel.elastic4s"  %% "elastic4s-client-akka" % Versions.elastic4s
    val elastic4sCirce: ModuleID = "com.sksamuel.elastic4s" %% "elastic4s-json-circe"  % Versions.elastic4s

    val stamina: ModuleID = "com.scalapenos"    %% "stamina-json" % Versions.stamina
    val jsonLenses = "net.virtual-void"         %% "json-lenses"  % Versions.jsonLenses
    val scalaCheck: ModuleID = "org.scalacheck" %% "scalacheck"   % Versions.scalaCheck

    val zio: ModuleID = "dev.zio"                           %% "zio"               % Versions.zio
    val zioStreams: ModuleID = "dev.zio"                    %% "zio-streams"       % Versions.zio
    val caliban: ModuleID = "com.github.ghostdogpr"         %% "caliban"           % Versions.caliban
    val calibanAkkaHttp: ModuleID = "com.github.ghostdogpr" %% "caliban-akka-http" % Versions.caliban

    val apacheMath: ModuleID = "org.apache.commons" % "commons-math3" % Versions.apacheMath
    val apacheText: ModuleID = "org.apache.commons" % "commons-text"  % Versions.apacheText

    // Java API for emails
    val jakartaMail: ModuleID = "jakarta.mail" % "jakarta.mail-api" % Versions.jakarta

    val refinedScala: ModuleID = "eu.timepit"      %% "refined"            % Versions.refined
    val refinedCirce: ModuleID = "io.circe"        %% "circe-refined"      % Versions.circe
    val refinedScalaCheck: ModuleID = "eu.timepit" %% "refined-scalacheck" % Versions.refined

    val enumeratum: ModuleID = "com.beachape"           %% "enumeratum"            % Versions.enumeratum
    val enumeratumCirce: ModuleID = "com.beachape"      %% "enumeratum-circe"      % Versions.enumeratum
    val enumeratumScalacheck: ModuleID = "com.beachape" %% "enumeratum-scalacheck" % Versions.enumeratum

    val scalaCsv: ModuleID = "com.github.tototoshi"    %% "scala-csv"        % Versions.scalaCsv
    val scalacacheGuava: ModuleID = "com.github.cb372" %% "scalacache-guava" % Versions.scalacacheGuava
  }

  object Tests {
    val akkaTest: ModuleID = "com.typesafe.akka"            %% "akka-actor-testkit-typed" % Versions.akka
    val akkaStreamTest: ModuleID = "com.typesafe.akka"      %% "akka-stream-testkit"      % Versions.akka % "it,test"
    val scalaTest: ModuleID = "org.scalatest"               %% "scalatest"                % Versions.scalaTest
    val scalaTestScalaCheck: ModuleID = "org.scalatestplus" %% "scalacheck-1-18"          % Versions.scalaTestScalaCheck
    val akkaHttpTest: ModuleID = "com.typesafe.akka"        %% "akka-http-testkit"        % Versions.akkaHttp % "it,test"
    val mockito: ModuleID = "org.mockito"                   %% "mockito-scala"            % Versions.mockito
    val mockitoScalatest: ModuleID = "org.mockito"          %% "mockito-scala-scalatest"  % Versions.mockito
    val dockerScalatest: ModuleID = "com.whisk"             %% "docker-testkit-scalatest" % Versions.dockerTestkit
    val dockerClient: ModuleID = ("com.whisk" %% "docker-testkit-impl-docker-java" % Versions.dockerTestkit)
      .exclude("io.netty", "netty-handler")
      .exclude("io.netty", "netty-transport-native-epoll")

    // Needed to use the client....
    val jerseyServer: ModuleID = "org.glassfish.jersey.core" % "jersey-server" % Versions.jersey % "it"
    val jerseyHk2: ModuleID = "org.glassfish.jersey.inject"  % "jersey-hk2"    % Versions.jersey % "it"
    val akkaPersistenceInMemory
      : ModuleID = "com.github.dnvriend"            %% "akka-persistence-inmemory" % Versions.akkaPersistenceInMemory
    val staminaTestKit: ModuleID = "com.scalapenos" %% "stamina-testkit"           % Versions.stamina % "test"
  }
}
