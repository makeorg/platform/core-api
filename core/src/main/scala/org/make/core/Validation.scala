/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core

import cats.{Eq, Show}
import cats.data.{ValidatedNec, NonEmptyChain => Nec, NonEmptySeq => Nes}
import cats.implicits._

import java.time.LocalDate
import jakarta.mail.internet.InternetAddress
import enumeratum.values.StringEnum
import eu.timepit.refined.auto._
import eu.timepit.refined.api.{Refined, Validate}
import eu.timepit.refined.boolean.And
import eu.timepit.refined.collection.NonEmpty
import grizzled.slf4j.Logging
import io.circe.generic.semiauto.deriveCodec
import io.circe.{Codec, Decoder, Encoder}
import org.jsoup.Jsoup
import org.jsoup.safety.{Cleaner, Safelist}
import org.make.core.Validator.ValidatedValue
import org.make.core.elasticsearch.ElasticsearchFieldName
import org.make.core.reference.Language
import org.make.core.technical.ValidatedUtils._

import scala.util.matching.Regex
import scala.util.{Failure, Success, Try}
import scala.annotation.nowarn

object Validation extends Logging {

  private def isEmail(str: String): Boolean =
    Try(new InternetAddress(str).validate()).isSuccess

  private val passwordRegex: Regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9 ])(?!.* ).{8,}$".r

  private val colorRegex: Regex = "^#[0-9a-fA-F]{6}$".r

  private val postalCodeRegex: Regex = "^\\d{5}$".r

  private val uploadFileNameRegex: Regex = "[a-zA-Z0-9_.-]+".r

  val minAgeWithLegalConsent: Int = 8
  val minLegalAgeForExternalProposal: Int = 18
  private val maxAgeWithLegalConsent: Int = 16
  val maxAge: Int = 120

  def validateOptional(maybeRequire: Option[Requirement]*): Unit = validate(maybeRequire.flatten: _*)

  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  def validate(require: Requirement*): Unit = {
    val messages: Seq[ValidationError] = require.flatMap { requirement =>
      Try(requirement.condition()) match {
        case Failure(e) =>
          Seq(ValidationError(requirement.field, requirement.key, Option(e.getMessage)))
        case Success(false) =>
          Seq(ValidationError(requirement.field, requirement.key, Option(requirement.message())))
        case _ => Nil
      }
    }
    if (messages.nonEmpty) {
      throw ValidationFailedError(messages)
    }
  }

  def validateField(field: String, key: String, condition: => Boolean, message: => String): Requirement =
    Requirement(field, key, () => condition, () => message)

  // StringWithParsers -> .withMaxLength
  def maxLength(
    field: String,
    maxLength: Int,
    fieldValue: String,
    message: Option[Int => String] = None
  ): Requirement = {

    val computeLength: Int = {
      Option(fieldValue).map(_.length).getOrElse(0)
    }
    val isValid = {
      computeLength <= maxLength
    }

    Requirement(field, "too_long", () => isValid, () => {
      if (isValid) {
        ""
      } else {
        message.map(_(computeLength)).getOrElse(s"$field should not be longer than $maxLength")
      }
    })
  }

  // Should be discarded completely: nulls should not exist, and we have better (more specific)
  // ways of handling Options
  def mandatoryField(fieldName: String, fieldValue: => Any, message: Option[String] = None): Requirement = {
    val condition: () => Boolean = () => {
      val value = fieldValue
      Option(value).isDefined && value != None
    }
    validateField(fieldName, "mandatory", condition(), message.getOrElse(s"$fieldName is mandatory"))
  }

  // OptionWithParsers -> .getValidated
  def requirePresent(fieldName: String, fieldValue: => Option[_], message: Option[String] = None): Requirement = {
    validateField(fieldName, "mandatory", fieldValue.nonEmpty, message.getOrElse(s"$fieldName should not be empty"))
  }

  // StringWithParsers -> .toValidHTML
  def validateUserInput(fieldName: String, fieldValue: => String, message: Option[String]): Requirement = {
    val condition: () => Boolean = () => {
      new Cleaner(Safelist.none()).isValid(Jsoup.parse(fieldValue))
    }
    validateField(fieldName, "invalid_content", condition(), message.getOrElse(s"$fieldName is not a valid user input"))
  }

  // To be replaced with OptionWithParsers -> .getValidated >>= StringWithParsers -> .toValidHTML
  def validateOptionalUserInput(
    fieldName: String,
    fieldValue: => Option[String],
    message: Option[String]
  ): Requirement = {
    val condition: () => Boolean = () => {
      fieldValue.forall(value => new Cleaner(Safelist.none()).isValid(Jsoup.parse(value)))
    }
    validateField(fieldName, "invalid_content", condition(), message.getOrElse(s"$fieldName is not a valid user input"))
  }

  // StringWithParsers -> .toSlug
  def requireValidSlug(
    fieldName: String,
    fieldValue: => Option[String],
    message: Option[String] = None
  ): Requirement = {
    validateField(
      fieldName,
      "invalid_slug",
      fieldValue.getOrElse("") == SlugHelper.apply(fieldValue.getOrElse("")),
      message.getOrElse(s"$fieldName should not be empty")
    )
  }

  // SeqWithParsers -> .toEmpty
  def requireEmpty(fieldName: String, fieldValue: => Seq[_], message: Option[String] = None): Requirement = {
    validateField(fieldName, "non_empty", fieldValue.isEmpty, message.getOrElse(s"$fieldName should be empty"))
  }

  def requireNotPresent(fieldName: String, fieldValue: => Option[_], message: Option[String] = None): Requirement = {
    validateField(fieldName, "non_empty", fieldValue.isEmpty, message.getOrElse(s"$fieldName should be empty"))
  }

  // AnyWithParsers -> .isOneOf
  def validChoices[T](
    fieldName: String,
    message: Option[String] = None,
    userChoices: Seq[T],
    validChoices: Seq[T]
  ): Requirement = {
    val condition: () => Boolean = () => {
      userChoices.forall(validChoices.contains)
    }
    validateField(fieldName, "invalid_value", condition(), message.getOrElse(s"$fieldName is not valid"))
  }

  // BirthDate -> .validatedAge
  def validateAge(fieldName: String, userDateInput: Option[LocalDate], message: Option[String] = None): Requirement = {
    val condition: Boolean =
      userDateInput
        .map(BirthDate(_))
        .forall(_.isBetween(minAgeWithLegalConsent, maxAge))
    validateField(
      fieldName,
      "invalid_age",
      condition,
      message.getOrElse(s"Invalid date: age must be between $minAgeWithLegalConsent and $maxAge")
    )
  }

  // BirthDate -> .validateLegalConsent
  def validateLegalConsent(
    fieldName: String,
    userDateInput: LocalDate,
    userLegalConsent: Option[Boolean],
    message: Option[String] = None
  ): Requirement = {
    val condition: Boolean =
      LocalDate.now().minusYears(maxAgeWithLegalConsent).plusDays(1).isAfter(userDateInput) ||
        BirthDate(userDateInput).isBetween(minAgeWithLegalConsent, maxAgeWithLegalConsent) &&
          userLegalConsent.contains(true)
    validateField(fieldName, "legal_consent", condition, message.getOrElse(s"Field $fieldName must be approved."))
  }

  def validateSort[T <: ElasticsearchFieldName](
    fieldName: String
  )(sort: T)(implicit stringEnum: StringEnum[T]): Requirement = {
    val choices = stringEnum.values.filter(_.sortable)
    Validation.validChoices(
      fieldName = fieldName,
      message = Some(s"Invalid sort. Got $sort but expected one of: ${choices.mkString("\"", "\", \"", "\"")}"),
      Seq(sort),
      choices
    )
  }

  def validateUploadFileName(fileName: String): Requirement = {
    validateField(
      "filename",
      "invalid_filename",
      uploadFileNameRegex.matches(fileName),
      s"File name ${fileName} is invalid. The file name should only contains letters, numbers, hyphen, dot or underscore"
    )
  }

  final case class BirthDate(val birthDate: LocalDate) extends AnyVal {

    def isBetween(inclusiveMin: Int, exclusiveMax: Int): Boolean = {
      LocalDate.now().minusYears(exclusiveMax).isBefore(birthDate) &&
      LocalDate.now().minusYears(inclusiveMin).plusDays(1).isAfter(birthDate)
    }

    def validatedAge(fieldName: String, message: Option[String] = None): ValidatedNec[ValidationError, BirthDate] =
      if (isBetween(minAgeWithLegalConsent, maxAge))
        this.validNec
      else
        ValidationError(
          fieldName,
          "invalid_age",
          message.orElse(Some(s"Invalid date: age must be between $minAgeWithLegalConsent and $maxAge"))
        ).invalidNec

    def validatedLegalConsent(
      fieldName: String,
      userLegalConsent: Option[Boolean]
    ): ValidatedNec[ValidationError, BirthDate] =
      if (LocalDate.now().minusYears(maxAgeWithLegalConsent).plusDays(1).isAfter(birthDate) ||
          isBetween(minAgeWithLegalConsent, maxAgeWithLegalConsent) &&
          userLegalConsent.contains(true))
        this.validNec
      else
        ValidationError(
          fieldName,
          "legal_consent",
          Some(s"${birthDate.toString} does not fit legal consent requirements")
        ).invalidNec
  }

  object BirthDate {

    def fromAge(age: Int): BirthDate =
      BirthDate(DateHelper.computeBirthDate(age))
  }

  implicit class LocalDateExt(val t: LocalDate) extends AnyVal {
    def elapsedYears: Int = LocalDate.now().getYear() - t.getYear()
  }

  implicit val birthDateDecoder: Decoder[BirthDate] = Decoder.decodeLocalDate.emap { t =>
    if (minAgeWithLegalConsent until maxAge contains (t.elapsedYears))
      Right(BirthDate(t))
    else
      Left(s"Invalid date: age must be between $minAgeWithLegalConsent and $maxAge")
  }
  implicit val birthDateEncoder: Encoder[BirthDate] = Encoder.encodeLocalDate.contramap(_.birthDate)

  final case class NonEmptyType[T](elem: T) extends AnyVal

  implicit class EmptyableWithParsers[A <: { def isEmpty: Boolean }](elem: A) {

    def toEmpty(fieldName: String, message: Option[String] = None): ValidatedNec[ValidationError, Unit] =
      if (elem.isEmpty)
        ().validNec
      else
        ValidationError(fieldName, "non_empty", message.orElse(Some("Element was not empty"))).invalidNec

    def toNonEmpty(fieldName: String, message: Option[String] = None): ValidatedNec[ValidationError, NonEmptyType[A]] =
      if (elem.isEmpty)
        ValidationError(fieldName, "empty", message.orElse(Some("Element was empty"))).invalidNec
      else
        NonEmptyType[A](elem).validNec
  }

  final case class SeqSubset[A](values: Iterable[A], superset: Set[A])

  implicit class IterableWithParsers[A](it: Iterable[A]) {

    private lazy val valuesAsSet = it.toSet

    def toEmpty(fieldName: String, message: Option[String]): ValidatedNec[ValidationError, Nil.type] =
      it match {
        case Nil => Nil.validNec
        case _ =>
          ValidationError(fieldName, "empty_seq", message.orElse(Some("Iterable was not empty"))).invalidNec
      }

    def toSubsetOf(choices: Set[A]): ValidatedNec[ValidationError, SeqSubset[A]] =
      choices &~ valuesAsSet match {
        case Nil =>
          SeqSubset(it, choices).validNec
        case rejections =>
          ValidationError("values", s"Values ${rejections.mkString(",")} are not allowed").invalidNec
      }

    @SuppressWarnings(Array("org.wartremover.warts.ListUnapply"))
    def toNel(fieldName: String, key: String, message: Option[String]): ValidatedNec[ValidationError, Nes[A]] =
      it match {
        case head :: next =>
          Nes(head, next).validNec
        case _ =>
          ValidationError(fieldName, key, Some(s"$fieldName must not be empty")).invalidNec
      }
  }

  final case class OneOf[A](value: A, allowedValues: Iterable[A])

  implicit class AnyWithParsers[A](val value: A) extends AnyVal {

    def toOneOf(
      choices: Iterable[A],
      fieldName: String,
      message: Option[String] = None
    ): ValidatedNec[ValidationError, OneOf[A]] =
      if (choices.exists(_ == value)) OneOf(value, choices).validNec
      else
        ValidationError(fieldName, message.getOrElse(s"Value $value is not one of ${choices.mkString(",")}")).invalidNec

    def equalsValidated(other: A, fieldName: String, key: String = "not_equal", message: Option[String] = None)(
      implicit E: Eq[A]
    ): ValidatedNec[ValidationError, A] =
      Either.cond(value === other, value, ValidationError(fieldName, key, message)).toValidatedNec

    def predicateValidated(
      predicate: A => Boolean,
      fieldName: String,
      key: String,
      message: Option[String] = None
    ): ValidatedNec[ValidationError, A] =
      Either.cond(predicate(value), value, ValidationError(fieldName, key, message)).toValidatedNec
  }

  implicit class ElasticsearchFieldNameWithParsers[T <: ElasticsearchFieldName](val value: T) extends AnyVal {

    def sortValidated(fieldName: String, sort: T)(implicit E: StringEnum[T]): ValidatedNec[ValidationError, OneOf[T]] =
      sort.toOneOf(E.values.filter(_.sortable), fieldName)
  }

  final case class StringWithMinLength(value: String, minLength: Int)

  final case class StringWithMaxLength(value: String, maxLength: Int)

  final case class NonEmptyString(value: String) extends AnyVal

  final case class ValidHtml()
  object ValidHtml {
    @inline
    @deprecated("There is no guarantee the parameter is html safe", since = "2023-09-20")
    def fromStringUnsafe(t: String): String Refined ValidHtml = Refined.unsafeApply(t)
  }

  implicit def validateHtml: Validate.Plain[String, ValidHtml] =
    Validate.fromPredicate(
      t => new Cleaner(Safelist.none()).isValid(Jsoup.parse(t)),
      _ => "invalid user input",
      ValidHtml()
    )

  type Name = String Refined (ValidHtml And NonEmpty)

  final case class Email(value: String) extends AnyVal

  implicit val emailDecoder: Decoder[Email] = Decoder.decodeString.emap {
    case t if isEmail(t) => Right(Email(t))
    case _               => Left("Malformed email")
  }
  implicit val emailEncoder: Encoder[Email] = Encoder.encodeString.contramap(_.value)

  final case class Password(value: String) extends AnyVal

  implicit val passwordDecoder: Decoder[Password] = Decoder.decodeString.emap {
    case t if passwordRegex.matches(t) => Right(Password(t))
    case _ =>
      Left("password must be at least 8 characters long including 1 figure, 1 uppercase and 1 special characters")
  }
  implicit val passwordEncoder: Encoder[Password] = Encoder.encodeString.contramap(_.value)

  final case class Slug()
  object Slug {
    @inline
    @deprecated("There is no guarantee the parameter is a valid slug", since = "2023-09-20")
    def fromStringUnsafe(t: String): String Refined Slug = Refined.unsafeApply(t)
  }

  implicit val validateSlug: Validate.Plain[String, Slug] =
    Validate.fromPredicate(
      t => validateHtml.isValid(t) && SlugHelper.apply(t) === t,
      t => s"${t} is not a valid slug",
      Slug()
    )

  final case class PostalCode(value: String) extends AnyVal

  implicit val postalCodeDecoder: Decoder[PostalCode] = Decoder.decodeString.emap {
    case t if (postalCodeRegex.findFirstIn(t).isDefined) => Right(PostalCode(t))
    case t                                               => Left(s"Invalid postal code $t. Must be formatted '01234'")
  }
  implicit val postalCodeEncoder: Encoder[PostalCode] = Encoder.encodeString.contramap(_.value)

  final case class Colour()
  implicit val validateColour: Validate.Plain[String, Colour] =
    Validate.fromPredicate(colorRegex.matches(_), t => s"$t is not a valid colour", Colour())
  object Colour {
    @inline
    @deprecated("There is no guarantee the parameter is a valid colour", since = "2023-09-20")
    def fromStringUnsafe(t: String): String Refined Colour = Refined.unsafeApply(t)

    // NOTE: cannot check at compile-time that our string is a valid colour:
    // https://github.com/fthomas/refined/blob/master/modules/docs/macro_pitfalls.md#solution-2
    @nowarn("cat=deprecation")
    val black: String Refined Colour = Colour.fromStringUnsafe("#000000")
  }

  final case object EmptyString

  implicit class StringWithParsers(val value: String) extends AnyVal {

    def toEmpty(fieldName: String, message: Option[String] = None): ValidatedNec[ValidationError, EmptyString.type] =
      value match {
        case "" => EmptyString.validNec
        case _ =>
          ValidationError(fieldName, "empty_string", message.orElse(Some("String was not empty"))).invalidNec
      }

    def withMinLength(
      minLength: Int,
      fieldName: String,
      language: Option[Language] = None,
      message: Option[String] = None
    ): ValidatedNec[ValidationError, StringWithMinLength] =
      if (value.length >= minLength)
        StringWithMinLength(value, minLength).validNec
      else {
        val forLanguage = language.fold("")(l => s" for the language : ${l.value}")
        ValidationError(
          fieldName,
          "too_short",
          message.orElse(Some(s"$fieldName should not be shorter than $minLength$forLanguage"))
        ).invalidNec
      }

    def withMaxLength(
      maxLength: Int,
      fieldName: String,
      language: Option[Language] = None,
      message: Option[String] = None
    ): ValidatedNec[ValidationError, StringWithMaxLength] =
      if (value.length <= maxLength)
        StringWithMaxLength(value, maxLength).validNec
      else {
        val forLanguage = language.fold("")(l => s" for the language : ${l.value}")
        ValidationError(
          fieldName,
          "too_long",
          message.orElse(Some(s"$fieldName should not be longer than $maxLength$forLanguage"))
        ).invalidNec
      }

    def toNonEmpty(fieldName: String, message: Option[String] = None): ValidatedNec[ValidationError, NonEmptyString] =
      if (value.nonEmpty)
        NonEmptyString(value).validNec
      else
        ValidationError(fieldName, "empty", message.orElse(Some(s"$fieldName should not be an empty string"))).invalidNec

    def toSanitizedInput(
      fieldName: String,
      message: Option[String] = None
    ): ValidatedNec[ValidationError, String Refined ValidHtml] =
      if (new Cleaner(Safelist.none()).isValid(Jsoup.parse(value)))
        ValidHtml.fromStringUnsafe(value).validNec
      else
        ValidationError("user_input", "User provided invalid HTML").invalidNec

    def toEmail: ValidatedNec[ValidationError, Email] =
      value
        .toSanitizedInput("email")
        .flatMap(
          v =>
            if (isEmail(v.value))
              Email(v.value).validNec
            else
              ValidationError("email", "invalid_email", Some("email is not a valid email")).invalidNec
        )

    def toPassword: ValidatedNec[ValidationError, Password] =
      value
        .toSanitizedInput("password")
        .flatMap(
          v =>
            if (passwordRegex.matches(v.value))
              Password(v.value).validNec
            else
              ValidationError(
                "password",
                "invalid_password",
                Some(
                  "password must be at least 8 characters long including 1 figure, 1 uppercase and 1 special characters"
                )
              ).invalidNec
        )

    def toSlug(message: Option[String] = None): ValidatedNec[ValidationError, String Refined Slug] =
      value
        .toSanitizedInput("slug")
        .flatMap(
          v =>
            if (SlugHelper.apply(v.value) === v.value)
              Slug.fromStringUnsafe(v.value).validNec
            else
              ValidationError("slug", s"${v.value} is not a valid slug").invalidNec
        )

    def toValidAge(message: Option[String] = None): ValidatedNec[ValidationError, Int] =
      Try(value.toInt) match {
        case Success(age) if age >= minLegalAgeForExternalProposal => age.validNec
        case Success(_) =>
          ValidationError(
            "age",
            "invalid_age",
            message.orElse(Some(s"$value must be over $minLegalAgeForExternalProposal"))
          ).invalidNec
        case Failure(_) => ValidationError("age", "invalid_format", Some(s"'$value' is not a valid number")).invalidNec
      }

    def toPostalCode: ValidatedNec[ValidationError, PostalCode] =
      if (postalCodeRegex.findFirstIn(value).isDefined)
        PostalCode(value).validNec
      else
        ValidationError(
          "postalCode",
          "invalid_postal_code",
          Some(s"Invalid postal code $value. Must be formatted '01234'")
        ).invalidNec

    def toColour(
      fieldName: String,
      message: Option[String] = None
    ): ValidatedNec[ValidationError, String Refined Colour] =
      if (colorRegex.matches(value)) Colour.fromStringUnsafe(value).validNec
      else ValidationError(fieldName, "invalid_colour", message).invalidNec
  }

  implicit class OptionWithParsers[A](val opt: Option[A]) {

    def getValidated(
      fieldName: String,
      key: Option[String] = None,
      message: Option[String] = None
    ): ValidatedNec[ValidationError, A] =
      opt match {
        case Some(value) => value.validNec
        case None        => ValidationError(fieldName, key.getOrElse("value_absent"), message).invalidNec
      }

    def getNone(fieldName: String, message: Option[String] = None): ValidatedNec[ValidationError, Unit] =
      opt match {
        case Some(value) => ValidationError(fieldName, "value_present", message).invalidNec
        case None        => ().validNec
      }
  }

  implicit class LongWithParsers(val value: Long) {
    def max(fieldName: String, maxValue: Long, message: Option[String] = None): ValidatedNec[ValidationError, Long] = {
      if (value > maxValue) {
        ValidationError(fieldName, "exceeds_max", message.orElse(Some(s"$fieldName should be lower than $maxValue"))).invalidNec
      } else {
        value.validNec
      }
    }
  }

  implicit class MapWithParser(val map: Map[String, String]) extends AnyVal {
    def getNonEmptyString(fieldName: String, message: Option[String]): ValidatedNec[ValidationError, NonEmptyString] =
      map.get(fieldName).getValidated(fieldName, None, message).andThen(_.toNonEmpty(fieldName, message))
  }
}

final case class Requirement(field: String, key: String, condition: () => Boolean, message: () => String)

trait Validator[T] {
  def validate(value: T): ValidatedValue[T]
}

object Validator {
  def apply[T](validator: T => ValidatedValue[T]): Validator[T] = (value: T) => validator(value)

  type ValidatedValue[T] = ValidatedNec[ValidationError, T]
}

final case class ValidationFailedError(errors: Seq[ValidationError]) extends Exception {

  override def getMessage: String = errors.mkString("[", ",", "]")
}

object ValidationFailedError {

  def apply(nec: Nec[ValidationError]): ValidationFailedError =
    new ValidationFailedError(nec.toList)
}

final case class ValidationError(field: String, key: String, message: Option[String] = None) {

  def toThrowable: Throwable = {
    val errMsg = message.getOrElse(s"$field value '$key' was invalid")
    new IllegalArgumentException(errMsg)
  }
}

object ValidationError {

  implicit val codec: Codec[ValidationError] = deriveCodec

  implicit val show: Show[ValidationError] = Show.show(_.toThrowable.toString)
}

final case class SanitizedHtml(html: String) extends AnyRef

object SanitizedHtml {

  private val cleaner = new Cleaner(Safelist.basic)

  def fromString(unsafeHtml: String, maxTextLength: Option[Int] = None): Try[SanitizedHtml] = {
    val sanitized = cleaner.clean(Jsoup.parseBodyFragment(unsafeHtml))
    maxTextLength match {
      case Some(maxLength) if sanitized.text().length > maxLength =>
        Failure(new Error(s"Text exceeded $maxLength characters"))
      case _ =>
        Success(new SanitizedHtml(sanitized.body.children.toString))
    }
  }
}
