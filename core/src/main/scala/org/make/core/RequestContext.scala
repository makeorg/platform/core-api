/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core

import java.time.ZonedDateTime
import enumeratum.values.{StringCirceEnum, StringEnum, StringEnumEntry}
import io.circe.generic.semiauto._
import io.circe.{Codec, Decoder, Encoder}
import io.swagger.annotations.ApiModelProperty
import org.make.core.operation.OperationId
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, Language}
import org.make.core.session.{SessionId, VisitorId}
import org.make.core.user.UserId
import spray.json.DefaultJsonProtocol._
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

import scala.annotation.meta.field

sealed abstract class ApplicationName(val value: String) extends StringEnumEntry

object ApplicationName extends StringEnum[ApplicationName] with StringCirceEnum[ApplicationName] {

  case object Backoffice extends ApplicationName("backoffice")

  case object AssemblyBackoffice extends ApplicationName("assembly-backoffice")

  case object BiBatchs extends ApplicationName("bi-batchs")

  case object Concertation extends ApplicationName("concertation")

  case object ConcertationBeta extends ApplicationName("concertation-beta")

  case object Dial extends ApplicationName("dial")

  case object DialBatchs extends ApplicationName("dial-batchs")

  case object Infrastructure extends ApplicationName("infra")

  case object LegacyFrontend extends ApplicationName("legacy-front")

  case object MainFrontend extends ApplicationName("main-front")

  case object OldBackoffice extends ApplicationName("bo")

  case object Widget extends ApplicationName("widget")

  case object WidgetManager extends ApplicationName("widget-manager")

  override val values: IndexedSeq[ApplicationName] = findValues
  final val swaggerAllowableValues =
    "backoffice,bi-batchs,concertation,concertation-beta,dial,dial-batchs,infra,legacy-front,main-front,bo,widget,widget-manager"
}

final case class RequestContextQuestion(
  question: Option[String] = None,
  questionSlug: Option[String] = None,
  questionId: Option[QuestionId] = None
)

object RequestContextQuestion {
  val empty: RequestContextQuestion = RequestContextQuestion(None, None, None)

  implicit val codec: Codec[RequestContextQuestion] = deriveCodec[RequestContextQuestion]

  implicit val formatter: RootJsonFormat[RequestContextQuestion] = jsonFormat3(RequestContextQuestion.apply)
}

final case class RequestContextLanguage(
  language: Option[Language] = None,
  questionLanguage: Option[Language] = None,
  proposalLanguage: Option[Language] = None,
  clientLanguage: Option[Language] = None
)

object RequestContextLanguage {
  val empty: RequestContextLanguage = RequestContextLanguage(None, None, None, None)

  implicit val codec: Codec[RequestContextLanguage] = deriveCodec[RequestContextLanguage]

  implicit val formatter: RootJsonFormat[RequestContextLanguage] = jsonFormat4(RequestContextLanguage.apply)
}

final case class RequestContext(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  userId: Option[UserId] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  requestId: String,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  sessionId: SessionId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  visitorId: Option[VisitorId] = None,
  visitorCreatedAt: Option[ZonedDateTime] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  externalId: String,
  @(ApiModelProperty @field)(dataType = "string", example = "FR")
  country: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "FR")
  detectedCountry: Option[Country] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "fr")
  languageContext: RequestContextLanguage = RequestContextLanguage.empty,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  operationId: Option[OperationId] = None,
  source: Option[String],
  location: Option[String],
  questionContext: RequestContextQuestion = RequestContextQuestion.empty,
  hostname: Option[String] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "0.0.0.x")
  ipAddress: Option[String] = None,
  ipAddressHash: Option[String] = None,
  getParameters: Option[Map[String, String]] = None,
  userAgent: Option[String] = None,
  @(ApiModelProperty @field)(dataType = "string", allowableValues = ApplicationName.swaggerAllowableValues)
  applicationName: Option[ApplicationName] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "main-front")
  referrer: Option[String] = None,
  customData: Map[String, String] = Map.empty
)

object RequestContext extends CirceFormatters with SprayJsonFormatters {
  implicit val encoder: Encoder[RequestContext] = deriveEncoder[RequestContext]
  implicit val decoder: Decoder[RequestContext] = deriveDecoder[RequestContext]

  val empty: RequestContext =
    RequestContext(
      userId = None,
      requestId = "",
      sessionId = SessionId(""),
      visitorId = None,
      visitorCreatedAt = None,
      externalId = "",
      country = None,
      detectedCountry = None,
      languageContext = RequestContextLanguage.empty,
      operationId = None,
      source = None,
      location = None,
      questionContext = RequestContextQuestion.empty,
      hostname = None,
      ipAddress = None,
      ipAddressHash = None,
      getParameters = None,
      userAgent = None,
      applicationName = None,
      referrer = None,
      customData = Map.empty
    )

  implicit val requestContextFormatter: RootJsonFormat[RequestContext] =
    DefaultJsonProtocol.jsonFormat21(RequestContext.apply)

}
