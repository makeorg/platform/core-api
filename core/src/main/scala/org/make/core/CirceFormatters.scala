/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core

import java.net.URI
import java.time.{LocalDate, ZonedDateTime}
import io.circe._
import io.circe.generic.extras.Configuration

import java.time.format.DateTimeFormatter
import scala.util.{Failure, Success, Try}

trait CirceFormatters {

  implicit val circeConfig: Configuration = Configuration.default

  implicit lazy val zonedDateTimeEncoder: Encoder[ZonedDateTime] =
    (a: ZonedDateTime) => Json.fromString(DateFormatters.default.format(a))
  implicit lazy val zonedDateTimeDecoder: Decoder[ZonedDateTime] =
    Decoder.decodeString.emap { date =>
      Try(ZonedDateTime.from(DateFormatters.default.parse(date)))
        .orElse(Try(ZonedDateTime.parse(date, DateTimeFormatter.ISO_ZONED_DATE_TIME)).map(_.withFixedOffsetZone())) match {
        case Success(parsed) => Right(parsed)
        case Failure(_)      => Left(s"$date is not a valid date, it should match yyyy-MM-ddThh:mm:ss.SSSZ")
      }
    }

  implicit lazy val localDateEncoder: Encoder[LocalDate] = (a: LocalDate) => Json.fromString(a.toString)
  implicit lazy val localDateDecoder: Decoder[LocalDate] =
    Decoder.decodeString.emap { date =>
      Try(LocalDate.parse(date)) match {
        case Success(parsed) => Right(parsed)
        case Failure(_)      => Left(s"$date is not a valid date, it should match yyyy-MM-dd")
      }
    }

  implicit def h[A, B](implicit a: Decoder[A], b: Decoder[B]): Decoder[Either[A, B]] = {
    val l: Decoder[Either[A, B]] = a.map(Left.apply)
    val r: Decoder[Either[A, B]] = b.map(Right.apply)
    l.or(r)
  }

  implicit def stringValueEncoder[A <: StringValue]: Encoder[A] = a => Json.fromString(a.value)

  implicit val urlEncoder: Encoder[URI] = Encoder[String].contramap(_.toString)
  implicit val urlDecoder: Decoder[URI] = Decoder[String].emap { url =>
    Try(new URI(url)) match {
      case Success(parsed) => Right(parsed)
      case Failure(_)      => Left(s"Not a valid URL: $url")
    }
  }

  @SuppressWarnings(Array("org.wartremover.warts.Recursion"))
  implicit def circe2spray(t: Json): spray.json.JsValue =
    t.fold(
      spray.json.JsNull,
      spray.json.JsBoolean(_),
      x => spray.json.JsNumber(x.toDouble),
      spray.json.JsString(_),
      xs => spray.json.JsArray(xs.map(circe2spray)),
      x  => spray.json.JsObject(x.keys.zip(x.values.map(circe2spray)).toMap)
    )

  @SuppressWarnings(Array("org.wartremover.warts.Recursion"))
  implicit def spray2circe(t: spray.json.JsValue): Json =
    t match {
      case spray.json.JsArray(elements) => Json.fromValues(elements.map(spray2circe))
      case spray.json.JsObject(fields)  => Json.fromFields(fields.keys.zip(fields.values.map(spray2circe)))
      case spray.json.JsFalse           => Json.False
      case spray.json.JsTrue            => Json.True
      case spray.json.JsNumber(value)   => Json.fromBigDecimal(value)
      case spray.json.JsString(value)   => Json.fromString(value)
      case spray.json.JsNull            => Json.Null
    }

  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  def circeCodec2sprayFormatter[A](implicit encoder: Encoder[A], decoder: Decoder[A]): spray.json.RootJsonFormat[A] =
    new spray.json.RootJsonFormat[A] {
      override def read(json: spray.json.JsValue): A =
        decoder(HCursor.fromJson(json)) match {
          case Left(error)  => throw error
          case Right(value) => value
        }
      override def write(obj: A): spray.json.JsValue = encoder(obj)
    }
}
