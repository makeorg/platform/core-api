/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.proposal

import cats.implicits._
import com.github.plokhotnyuk.jsoniter_scala.core._
import com.github.plokhotnyuk.jsoniter_scala.macros._
import com.sksamuel.avro4s.AvroSortPriority
import enumeratum.values.{StringCirceEnum, StringEnum, StringEnumEntry}
import io.circe.generic.extras.JsonKey
import io.circe.generic.extras.semiauto.{
  deriveConfiguredCodec   => deriveCodec,
  deriveConfiguredDecoder => deriveDecoder,
  deriveConfiguredEncoder => deriveEncoder
}
import io.circe.{Codec, Decoder, Encoder}
import io.swagger.annotations.ApiModelProperty
import org.make.api.proposal.ProposalScorer
import org.make.api.proposal.ProposalScorer.VotesCounter
import org.make.core._
import org.make.core.idea.IdeaId
import org.make.core.jsoniter.JsoniterEnum
import org.make.core.operation.OperationId
import org.make.core.proposal.ProposalStatus.Accepted
import org.make.core.proposal.QualificationKey.{
  DoNotCare,
  DoNotUnderstand,
  Doable,
  Impossible,
  LikeIt,
  NoOpinion,
  NoWay,
  PlatitudeAgree,
  PlatitudeDisagree
}
import org.make.core.proposal.VoteKey.{Agree, Disagree, Neutral}
import org.make.core.proposal.indexed.Zone
import org.make.core.question.QuestionId
import org.make.core.reference.Language
import org.make.core.tag.{TagId, TagType, TagTypeId}
import org.make.core.technical.Multilingual
import org.make.core.user.UserId
import spray.json.RootJsonFormat

import java.time.ZonedDateTime
import scala.annotation.meta.field

final case class Proposal(
  proposalId: ProposalId,
  slug: String,
  content: String,
  submittedAsLanguage: Option[Language] = None,
  contentTranslations: Option[Multilingual[String]] = None,
  author: UserId,
  status: ProposalStatus = ProposalStatus.Pending,
  refusalReason: Option[String] = None,
  tags: Seq[TagId] = Seq.empty,
  @JsonKey("votes") votingOptions: Option[VotingOptions] = None,
  isAnonymous: Boolean = false,
  organisationIds: Seq[UserId] = Seq.empty,
  questionId: Option[QuestionId] = None,
  creationContext: RequestContext,
  idea: Option[IdeaId] = None,
  operation: Option[OperationId] = None,
  proposalType: ProposalType = ProposalType.ProposalTypeSubmitted,
  override val createdAt: Option[ZonedDateTime],
  override val updatedAt: Option[ZonedDateTime],
  validatedAt: Option[ZonedDateTime] = None,
  postponedAt: Option[ZonedDateTime] = None,
  events: List[ProposalAction],
  initialProposal: Boolean = false,
  keywords: Seq[ProposalKeyword] = Nil
) extends Timestamped
    with MakeSerializable {

  def getZone: Zone =
    votingOptions.map(ProposalScorer(_, VotesCounter.VerifiedVotesVotesCounter, 0.5).zone).getOrElse(Zone.Limbo)
}

object Proposal extends CirceFormatters {
  implicit val encoder: Encoder[Proposal] = deriveEncoder
  implicit val decoder: Decoder[Proposal] = deriveDecoder
  implicit val writer: RootJsonFormat[Proposal] = circeCodec2sprayFormatter

  def needsEnrichment(status: ProposalStatus, tagTypes: Seq[TagType], proposalTagTypes: Seq[TagTypeId]): Boolean = {
    val proposalTypesSet = proposalTagTypes.toSet
    status == Accepted && !tagTypes.filter(_.requiredForEnrichment).map(_.tagTypeId).forall(proposalTypesSet.contains)
  }

}

final case class ProposalId(value: String) extends StringValue

object ProposalId extends CirceFormatters {
  implicit lazy val proposalIdEncoder: Encoder[ProposalId] =
    Encoder.encodeString.contramap(_.value)
  implicit lazy val proposalIdDecoder: Decoder[ProposalId] =
    Decoder.decodeString.map(ProposalId(_))
  implicit lazy val proposalFormat: RootJsonFormat[ProposalId] =
    circeCodec2sprayFormatter

  implicit val proposalIdCodec: JsonValueCodec[ProposalId] =
    StringValue.makeCodec(ProposalId.apply)
}

final case class OrganisationInfo(organisationId: UserId, organisationName: Option[String])

object OrganisationInfo extends CirceFormatters {
  implicit val encoder: Encoder[OrganisationInfo] = deriveEncoder[OrganisationInfo]
  implicit val decoder: Decoder[OrganisationInfo] = deriveDecoder[OrganisationInfo]
  implicit lazy val organisationInfoFormat: RootJsonFormat[OrganisationInfo] =
    circeCodec2sprayFormatter
}

final case class ProposalAction(date: ZonedDateTime, user: UserId, actionType: String, arguments: Map[String, String])

object ProposalAction extends CirceFormatters {
  implicit val codec: Codec[ProposalAction] = deriveCodec
  implicit val proposalActionFormat: RootJsonFormat[ProposalAction] =
    circeCodec2sprayFormatter
}

sealed abstract class ProposalActionType(val value: String) extends StringEnumEntry

object ProposalActionType extends StringEnum[ProposalActionType] {

  final case object ProposalProposeAction extends ProposalActionType("propose")
  final case object ProposalUpdateAction extends ProposalActionType("update")
  final case object ProposalUpdateVoteVerifiedAction extends ProposalActionType("update-votes-verified")
  final case object ProposalAcceptAction extends ProposalActionType("accept")
  final case object ProposalVoteAction extends ProposalActionType("vote")
  final case object ProposalUnvoteAction extends ProposalActionType("unvote")
  final case object ProposalQualifyAction extends ProposalActionType("qualify")
  final case object ProposalUnqualifyAction extends ProposalActionType("unqualify")

  override val values: IndexedSeq[ProposalActionType] = findValues

}

sealed abstract class QualificationKey(val value: String) extends StringEnumEntry with Key

object QualificationKey
    extends StringEnum[QualificationKey]
    with StringCirceEnum[QualificationKey]
    with JsoniterEnum[QualificationKey] {

  final case object LikeIt extends QualificationKey("likeIt")
  final case object Doable extends QualificationKey("doable")
  final case object PlatitudeAgree extends QualificationKey("platitudeAgree")
  final case object NoWay extends QualificationKey("noWay")
  final case object Impossible extends QualificationKey("impossible")
  final case object PlatitudeDisagree extends QualificationKey("platitudeDisagree")
  final case object DoNotUnderstand extends QualificationKey("doNotUnderstand")
  final case object NoOpinion extends QualificationKey("noOpinion")
  final case object DoNotCare extends QualificationKey("doNotCare")

  override def values: IndexedSeq[QualificationKey] = findValues
  final val swaggerAllowableValues =
    "likeIt,doable,platitudeAgree,noWay,impossible,platitudeDisagree,doNotUnderstand,noOpinion,doNotCare"
}

trait BaseQualification extends BaseVoteOrQualification[QualificationKey]

final case class Qualification(
  @(ApiModelProperty @field)(dataType = "string", example = "likeIt") override val key: QualificationKey,
  override val count: Int,
  override val countVerified: Int,
  override val countSequence: Int,
  override val countSegment: Int
) extends BaseQualification

object Qualification extends CirceFormatters {
  implicit val encoder: Encoder[Qualification] = deriveEncoder[Qualification]
  implicit val decoder: Decoder[Qualification] = deriveDecoder[Qualification]
  implicit val qualificationFormat: RootJsonFormat[Qualification] = circeCodec2sprayFormatter
}

trait BaseVoteOrQualification[KeyType <: Key] {
  def key: KeyType
  def count: Int
  def countVerified: Int
  def countSequence: Int
  def countSegment: Int
}

trait BaseVote extends BaseVoteOrQualification[VoteKey]

object BaseVote {

  def rate(votingOptions: VotingOptions, key: VoteKey): Double = {
    val total = votingOptions.counts.totalVotes
    if (total === 0) 0
    else votingOptions.getVote(key).count.toDouble / total
  }
}

final case class Vote(
  @(ApiModelProperty @field)(dataType = "string", example = "agree")
  override val key: VoteKey,
  override val count: Int,
  override val countVerified: Int,
  override val countSequence: Int,
  override val countSegment: Int,
  qualifications: Seq[Qualification] = Seq.empty
) extends BaseVote

object Vote extends CirceFormatters {

  implicit val encoder: Encoder[Vote] = deriveEncoder[Vote]
  implicit val decoder: Decoder[Vote] = deriveDecoder[Vote]
  implicit val voteFormat: RootJsonFormat[Vote] = circeCodec2sprayFormatter

  def empty(key: VoteKey): Vote = Vote(key, 0, 0, 0, 0)
}

sealed abstract class ProposalType(val value: String) extends StringEnumEntry with Product with Serializable

object ProposalType
    extends StringEnum[ProposalType]
    with StringCirceEnum[ProposalType]
    with JsoniterEnum[ProposalType] {

  final case object ProposalTypeSubmitted extends ProposalType("SUBMITTED")
  final case object ProposalTypeExternal extends ProposalType("EXTERNAL")
  final case object ProposalTypeInitial extends ProposalType("INITIAL")

  override def values: IndexedSeq[ProposalType] = findValues

}

sealed trait Key

object Key {
  implicit class CountOps[T <: Key](val key: T) extends AnyVal {
    def count(voteCounts: VoteCounts)(implicit c: HasCount[T]): Int = c.keyCount(voteCounts)
  }

  implicit class SmoothingOps[T <: Key](val key: T) extends AnyVal {
    def smoothing(implicit s: HasSmoothing[T]): Double = s.smoothing()
  }
}

trait HasCount[T] {
  def keyCount(voteCounts: VoteCounts): Int
}

object HasCount {
  implicit val agreeCount: HasCount[Agree.type] = _.agreeVote
  implicit val disagreeCount: HasCount[Disagree.type] = _.disagreeVote
  implicit val neutralCount: HasCount[Neutral.type] = _.neutralVote
  implicit val doNotCareCount: HasCount[DoNotCare.type] = _.doNotCare
  implicit val doNotUnderstandCount: HasCount[DoNotUnderstand.type] = _.doNotUnderstand
  implicit val doableCount: HasCount[Doable.type] = _.doable
  implicit val impossibleCount: HasCount[Impossible.type] = _.impossible
  implicit val likeItCount: HasCount[LikeIt.type] = _.likeIt
  implicit val noOpinionCount: HasCount[NoOpinion.type] = _.noOpinion
  implicit val noWayCount: HasCount[NoWay.type] = _.noWay
  implicit val platitudeAgreeCount: HasCount[PlatitudeAgree.type] = _.platitudeAgree
  implicit val platitudeDisagreeCount: HasCount[PlatitudeDisagree.type] = _.platitudeDisagree
}

trait HasSmoothing[T <: Key] {
  def smoothing(): Double
}

object HasSmoothing {
  implicit def voteSmoothing[T <: VoteKey]: HasSmoothing[T] = new HasSmoothing[T] {
    override def smoothing(): Double = ProposalScorer.votesSmoothing
  }

  implicit def qualificationSmoothing[T <: QualificationKey]: HasSmoothing[T] = new HasSmoothing[T] {
    override def smoothing(): Double = ProposalScorer.qualificationsSmoothing
  }
}
sealed abstract class VoteKey(val value: String) extends StringEnumEntry with Key with Product with Serializable

object VoteKey extends StringEnum[VoteKey] with StringCirceEnum[VoteKey] with JsoniterEnum[VoteKey] {

  final case object Agree extends VoteKey("agree")
  final case object Disagree extends VoteKey("disagree")
  final case object Neutral extends VoteKey("neutral")

  override def values: IndexedSeq[VoteKey] = findValues
  final val swaggerAllowableValues = "agree,disagree,neutral"
}

sealed abstract class ProposalStatus(val value: String) extends StringEnumEntry with Product with Serializable

object ProposalStatus
    extends StringEnum[ProposalStatus]
    with StringCirceEnum[ProposalStatus]
    with JsoniterEnum[ProposalStatus] {

  @AvroSortPriority(5)
  final case object Pending extends ProposalStatus("Pending")

  @AvroSortPriority(1)
  final case object Accepted extends ProposalStatus("Accepted")

  @AvroSortPriority(3)
  final case object Refused extends ProposalStatus("Refused")

  @AvroSortPriority(4)
  final case object Postponed extends ProposalStatus("Postponed")

  @AvroSortPriority(2)
  final case object Archived extends ProposalStatus("Archived")

  override def values: IndexedSeq[ProposalStatus] = findValues
  final val swaggerAllowableValues = "Pending,Accepted,Refused,Postponed,Archived"
}

final case class ProposalKeyword(
  @(ApiModelProperty @field)(dataType = "string", required = true)
  key: ProposalKeywordKey,
  label: String
)

object ProposalKeyword extends CirceFormatters {
  implicit val codec: Codec[ProposalKeyword] = deriveCodec[ProposalKeyword]
  implicit val format: RootJsonFormat[ProposalKeyword] = circeCodec2sprayFormatter

  implicit val proposalKeywordCodec: JsonValueCodec[ProposalKeyword] =
    JsonCodecMaker.makeWithRequiredCollectionFields
}

final case class ProposalKeywordKey(value: String) extends StringValue

object ProposalKeywordKey extends CirceFormatters {

  implicit val codec: Codec[ProposalKeywordKey] =
    Codec.from(Decoder[String].map(ProposalKeywordKey.apply), Encoder[String].contramap(_.value))
  implicit val format: RootJsonFormat[ProposalKeywordKey] = circeCodec2sprayFormatter

  implicit val proposalKeywordKeycodec: JsonValueCodec[ProposalKeywordKey] =
    StringValue.makeCodec(ProposalKeywordKey.apply)
}

sealed abstract class ProposalReportReason(val value: String) extends StringEnumEntry

object ProposalReportReason extends StringEnum[ProposalReportReason] with StringCirceEnum[ProposalReportReason] {

  final case object Inintelligible extends ProposalReportReason("Inintelligible")
  final case object BadTranslation extends ProposalReportReason("BadTranslation")
  final case object IncorrectInformation extends ProposalReportReason("IncorrectInformation")
  final case object Offensive extends ProposalReportReason("Offensive")

  override val values: IndexedSeq[ProposalReportReason] = findValues
  final val swaggerAllowableValues = "Inintelligible,BadTranslation,IncorrectInformation,Offensive"
}
