/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.proposal.indexed

import cats.data.NonEmptyList
import com.github.plokhotnyuk.jsoniter_scala.core._
import com.github.plokhotnyuk.jsoniter_scala.macros._
import enumeratum.values.{StringCirceEnum, StringEnum, StringEnumEntry}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.NonEmpty
import io.circe.generic.semiauto._
import io.circe.{Codec, Decoder, Encoder}
import io.circe.refined._
import io.swagger.annotations.{ApiModel, ApiModelProperty}
import org.make.api.proposal.ProposalScorer
import org.make.core.elasticsearch.ElasticsearchFieldName
import org.make.core.idea.IdeaId
import org.make.core.jsoniter.{JsoniterCodecs, JsoniterEnum}
import org.make.core.operation.{OperationId, OperationKind}
import org.make.core.proposal._
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, Language}
import org.make.core.tag.TagId
import org.make.core.technical.Multilingual
import org.make.core.user.{UserId, UserType}
import org.make.core.{CirceFormatters, RequestContext}

import java.time.ZonedDateTime
import scala.annotation.meta.field

sealed abstract class ProposalElasticsearchFieldName(val value: String, val sortable: Boolean = false)
    extends ElasticsearchFieldName

object ProposalElasticsearchFieldName extends StringEnum[ProposalElasticsearchFieldName] {

  sealed abstract class Simple(val field: String, override val sortable: Boolean = false)
      extends ProposalElasticsearchFieldName(field, sortable) {
    override def parameter: String = field
  }

  sealed abstract class Alias(
    val parameter: String,
    aliased: ProposalElasticsearchFieldName,
    override val sortable: Boolean = false
  ) extends ProposalElasticsearchFieldName(parameter, sortable) {
    override def field: String = aliased.field
  }

  case object id extends Simple("id")

  case object agreementRate extends Simple("agreementRate", sortable = true)
  case object authorUserId extends Simple("author.userId")
  case object authorAge extends Simple("author.age")
  case object authorFirstName extends Simple("author.firstName")
  case object authorPostalCode extends Simple("author.postalCode")
  case object authorUserType extends Simple("author.userType")
  case object authorAvatarUrl extends Simple("author.avatarUrl")
  case object content extends Simple("contentGeneral")
  case object contentBg extends Simple("content.bg.standard")
  case object contentBgStemmed extends Simple("content.bg.stemmed")
  case object contentCs extends Simple("content.cs.standard")
  case object contentCsStemmed extends Simple("content.cs.stemmed")
  case object contentDa extends Simple("content.da.standard")
  case object contentDaStemmed extends Simple("content.da.stemmed")
  case object contentDe extends Simple("content.de.standard")
  case object contentDeStemmed extends Simple("content.de.stemmed")
  case object contentEl extends Simple("content.el.standard")
  case object contentElStemmed extends Simple("content.el.stemmed")
  case object contentEn extends Simple("content.en.standard")
  case object contentEnStemmed extends Simple("content.en.stemmed")
  case object contentEs extends Simple("content.es.standard")
  case object contentEsStemmed extends Simple("content.es.stemmed")
  case object contentEt extends Simple("content.et.standard")
  case object contentFi extends Simple("content.fi.standard")
  case object contentFiStemmed extends Simple("content.fi.stemmed")
  case object contentFr extends Simple("content.fr.standard")
  case object contentFrStemmed extends Simple("content.fr.stemmed")
  case object contentHr extends Simple("content.hr.standard")
  case object contentHu extends Simple("content.hu.standard")
  case object contentHuStemmed extends Simple("content.hu.stemmed")
  case object contentIt extends Simple("content.it.standard")
  case object contentItStemmed extends Simple("content.it.stemmed")
  case object contentLt extends Simple("content.lt.standard")
  case object contentLtStemmed extends Simple("content.lt.stemmed")
  case object contentLv extends Simple("content.lv.standard")
  case object contentLvStemmed extends Simple("content.lv.stemmed")
  case object contentMt extends Simple("content.mt.standard")
  case object contentNl extends Simple("content.nl.standard")
  case object contentNlStemmed extends Simple("content.nl.stemmed")
  case object contentPl extends Simple("content.pl.standard")
  case object contentPlStemmed extends Simple("content.pl.stemmed")
  case object contentPt extends Simple("content.pt.standard")
  case object contentPtStemmed extends Simple("content.pt.stemmed")
  case object contentRo extends Simple("content.ro.standard")
  case object contentRoStemmed extends Simple("content.ro.stemmed")
  case object contentSk extends Simple("content.sk.standard")
  case object contentSl extends Simple("content.sl.standard")
  case object contentUk extends Simple("content.uk.standard")
  case object contentSv extends Simple("content.sv.standard")
  case object contentSvStemmed extends Simple("content.sv.stemmed")
  case object contentKeyword extends Simple("contentKeyword", sortable = true)
  case object contextCountry extends Simple("context.country")
  case object contextLocation extends Simple("context.location")
  case object contextOperation extends Simple("context.operation")
  case object contextQuestionSlug extends Simple("context.questionSlug")
  case object contextSource extends Simple("context.source")
  case object controversyLowerBound extends Simple("scores.controversy.lowerBound")
  case object controversyLowerBoundLegacy extends Alias("scores.controversyLowerBound", controversyLowerBound)
  case object country extends Alias("country", contextCountry, sortable = true)
  case object createdAt extends Simple("createdAt", sortable = true)
  case object ideaId extends Simple("ideaId")
  case object initialProposal extends Simple("initialProposal")
  case object language extends Simple("submittedAsLanguage", sortable = true)
  case object operationId extends Simple("operationId")
  case object operationKind extends Simple("operationKind")
  case object organisationId extends Simple("organisations.organisationId")
  case object organisationName extends Simple("organisations.organisationName")
  case object organisations extends Simple("organisations")
  case object proposalType extends Simple("proposalType")
  case object questionCountries extends Simple("question.countries", sortable = true)
  case object questionId extends Simple("question.questionId")
  case object questionIsOpen extends Simple("question.isOpen")
  case object questionLanguages extends Simple("question.languages", sortable = true)
  case object refusalReason extends Simple("refusalReason")
  case object scores extends Simple("scores")
  case object scoreAgreement extends Simple("scores.agreement.lowerBound")
  case object scoreAgreementLegacy extends Alias("scores.agreement", scoreAgreement)
  case object scoreRealistic extends Simple("scores.realistic.lowerBound")
  case object scoreRealisticLegacy extends Alias("scores.realistic", scoreRealistic)
  case object scoreUpperBound extends Simple("scores.topScore.upperBound")
  case object scoreUpperBoundLegacy extends Alias("scores.scoreUpperBound", scoreUpperBound)
  case object scoreLowerBound extends Simple("scores.topScore.lowerBound")
  case object scoreLowerBoundLegacy extends Alias("scores.scoreLowerBound", scoreLowerBound)
  case object segment extends Simple("segment")
  case object sequenceSegmentPool extends Simple("sequenceSegmentPool")
  case object sequencePool extends Simple("sequencePool")
  case object slug extends Simple("slug", sortable = true)
  case object status extends Simple("status")
  case object tagId extends Simple("tags.tagId")
  case object tags extends Simple("tags")
  case object selectedStakeTagId extends Simple("selectedStakeTag.tagId")
  case object toEnrich extends Simple("toEnrich")
  case object isAnonymous extends Simple("isAnonymous")
  case object topScore extends Simple("scores.topScore.score")
  case object topScoreLegacy extends Alias("scores.topScore", topScore)
  case object topScoreAjustedWithVotesLegacy
      extends Alias("scores.topScoreAjustedWithVotes", scoreLowerBound, sortable = true)
  case object trending extends Simple("trending", sortable = true)
  case object updatedAt extends Simple("updatedAt", sortable = true)
  case object votesCount extends Simple("votesCount")
  case object zone extends Simple("scores.zone")
  case object segmentZone extends Simple("segmentScores.zone")
  case object keywordKey extends Simple("keywords.key")

  override val values: IndexedSeq[ProposalElasticsearchFieldName] = findValues
  final val swaggerAllowableValues =
    "agreementRate,content.keyword,createdAt,country,language,labels,question.countries,question.language,slug,scores.topScoreAjustedWithVotes,trending,updatedAt,submittedAsLanguage"
}

final case class IndexedProposal(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: ProposalId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  submittedAsLanguage: Option[Language],
  content: Multilingual[String],
  contentGeneral: String,
  contentKeyword: String,
  slug: String,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = ProposalStatus.swaggerAllowableValues
  )
  status: ProposalStatus,
  createdAt: ZonedDateTime,
  updatedAt: Option[ZonedDateTime],
  validatedAt: Option[ZonedDateTime],
  postponedAt: Option[ZonedDateTime],
  votes: VotingOptions,
  votesCount: Int,
  votesVerifiedCount: Int,
  votesSequenceCount: Int,
  votesSegmentCount: Int,
  toEnrich: Boolean,
  scores: IndexedScores,
  segmentScores: IndexedScores,
  agreementRate: Double,
  context: Option[IndexedContext],
  isAnonymous: Boolean,
  author: IndexedAuthor,
  organisations: Seq[IndexedOrganisationInfo],
  question: Option[IndexedProposalQuestion],
  tags: Seq[IndexedTag],
  selectedStakeTag: Option[IndexedTag],
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  ideaId: Option[IdeaId],
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  operationId: Option[OperationId],
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = SequencePool.swaggerAllowableValues
  )
  sequencePool: SequencePool,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = SequencePool.swaggerAllowableValues
  )
  sequenceSegmentPool: SequencePool,
  initialProposal: Boolean,
  proposalType: ProposalType,
  refusalReason: Option[String],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = OperationKind.swaggerAllowableValues)
  operationKind: Option[OperationKind],
  segment: Option[String],
  keywords: Seq[IndexedProposalKeyword]
)

object IndexedProposal extends CirceFormatters with JsoniterCodecs {

  implicit val sprayCodec: JsonValueCodec[IndexedProposal] =
    JsonCodecMaker.makeWithRequiredCollectionFields

  implicit val codec: Codec[IndexedProposal] = deriveCodec
}

final case class IndexedProposalQuestion(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  slug: String,
  titles: Multilingual[String],
  questions: Multilingual[String Refined NonEmpty],
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  countries: NonEmptyList[Country],
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  languages: NonEmptyList[Language],
  startDate: ZonedDateTime,
  endDate: ZonedDateTime,
  isOpen: Boolean
)

object IndexedProposalQuestion extends CirceFormatters with JsoniterCodecs {

  implicit val indexedProposalQuestionCodec: JsonValueCodec[IndexedProposalQuestion] =
    JsonCodecMaker.makeWithRequiredCollectionFields

  implicit val circeCodec: Codec[IndexedProposalQuestion] = deriveCodec
}

@ApiModel
final case class IndexedContext(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  operation: Option[OperationId],
  source: Option[String],
  location: Option[String],
  questionSlug: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "FR")
  country: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr")
  questionLanguage: Option[Language],
  proposalLanguage: Option[Language],
  clientLanguage: Option[Language],
  getParameters: Seq[IndexedGetParameters]
)

object IndexedContext {

  def apply(context: RequestContext, isBeforeContextSourceFeature: Boolean = false): IndexedContext =
    IndexedContext(
      operation = context.operationId,
      source = context.source.filter(_.nonEmpty) match {
        case None if isBeforeContextSourceFeature => Some("core")
        case other                                => other
      },
      location = context.location,
      questionSlug = context.questionContext.questionSlug,
      country = context.country,
      questionLanguage = context.languageContext.questionLanguage,
      proposalLanguage = context.languageContext.proposalLanguage,
      clientLanguage = context.languageContext.clientLanguage,
      getParameters = context.getParameters
        .map(_.toSeq.map {
          case (key, value) => IndexedGetParameters(key, value)
        })
        .getOrElse(Seq.empty)
    )

  implicit val codec: JsonValueCodec[IndexedContext] =
    JsonCodecMaker.makeWithRequiredCollectionFields

  implicit val circeCodec: Codec[IndexedContext] = deriveCodec
}

final case class IndexedGetParameters(key: String, value: String)

object IndexedGetParameters {
  implicit val codec: JsonValueCodec[IndexedGetParameters] =
    JsonCodecMaker.makeWithRequiredCollectionFields

  implicit val encoder: Encoder[IndexedGetParameters] = deriveEncoder[IndexedGetParameters]
  implicit val decoder: Decoder[IndexedGetParameters] = deriveDecoder[IndexedGetParameters]
}

final case class IndexedAuthor(
  userId: UserId,
  firstName: Option[String],
  displayName: Option[String],
  organisationName: Option[String],
  organisationSlug: Option[String],
  postalCode: Option[String],
  @(ApiModelProperty @field)(example = "21", dataType = "int")
  age: Option[Int],
  avatarUrl: Option[String],
  @(ApiModelProperty @field)(dataType = "string", required = true, allowableValues = UserType.swaggerAllowableValues)
  userType: UserType,
  profession: Option[String]
)

object IndexedAuthor {
  implicit val codec: JsonValueCodec[IndexedAuthor] =
    JsonCodecMaker.makeWithRequiredCollectionFields

  implicit val circeCodec: Codec[IndexedAuthor] = deriveCodec
}

@ApiModel
final case class IndexedOrganisationInfo(
  @(ApiModelProperty)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  organisationId: UserId,
  organisationName: Option[String],
  organisationSlug: Option[String]
)

object IndexedOrganisationInfo {
  implicit val codec: JsonValueCodec[IndexedOrganisationInfo] =
    JsonCodecMaker.makeWithRequiredCollectionFields

  implicit val encoder: Encoder[IndexedOrganisationInfo] = deriveEncoder[IndexedOrganisationInfo]
  implicit val decoder: Decoder[IndexedOrganisationInfo] = deriveDecoder[IndexedOrganisationInfo]
}

sealed abstract class Zone(val value: String) extends StringEnumEntry

object Zone extends StringEnum[Zone] with StringCirceEnum[Zone] with JsoniterEnum[Zone] {

  final case object Consensus extends Zone("consensus")
  final case object Rejection extends Zone("rejection")
  final case object Limbo extends Zone("limbo")
  final case object Controversy extends Zone("controversy")

  override def values: IndexedSeq[Zone] = findValues
  final val swaggerAllowableValues = "consensus,rejection,limbo,controversy"
}

final case class IndexedScores(
  engagement: IndexedScore,
  agreement: IndexedScore,
  adhesion: IndexedScore,
  realistic: IndexedScore,
  platitude: IndexedScore,
  topScore: IndexedScore,
  controversy: IndexedScore,
  rejection: IndexedScore,
  @(ApiModelProperty @field)(dataType = "string", allowableValues = Zone.swaggerAllowableValues)
  zone: Zone
)

object IndexedScores {
  implicit val codec: JsonValueCodec[IndexedScores] =
    JsonCodecMaker.makeWithRequiredCollectionFields

  implicit val encoder: Encoder[IndexedScores] = deriveEncoder[IndexedScores]
  implicit val decoder: Decoder[IndexedScores] = deriveDecoder[IndexedScores]

  def empty: IndexedScores =
    IndexedScores(
      IndexedScore.empty,
      IndexedScore.empty,
      IndexedScore.empty,
      IndexedScore.empty,
      IndexedScore.empty,
      IndexedScore.empty,
      IndexedScore.empty,
      IndexedScore.empty,
      Zone.Limbo
    )

  def apply(scorer: ProposalScorer): IndexedScores =
    IndexedScores(
      engagement = IndexedScore(scorer.engagement),
      agreement = IndexedScore(scorer.agree),
      adhesion = IndexedScore(scorer.adhesion),
      realistic = IndexedScore(scorer.realistic),
      platitude = IndexedScore(scorer.platitude),
      topScore = IndexedScore(scorer.topScore),
      controversy = IndexedScore(scorer.controversy),
      rejection = IndexedScore(scorer.rejection),
      zone = scorer.zone
    )

}

final case class IndexedScore(score: Double, lowerBound: Double, upperBound: Double)

object IndexedScore {
  implicit val indexedScoreCodec: JsonValueCodec[IndexedScore] =
    JsonCodecMaker.makeWithRequiredCollectionFields
  implicit val codec: Codec[IndexedScore] = deriveCodec
  val empty: IndexedScore = IndexedScore(0, 0, 0)

  def apply(score: ProposalScorer.Score): IndexedScore =
    IndexedScore(score = score.score, lowerBound = score.lowerBound, upperBound = score.upperBound)
}

final case class ProposalsSearchResult(total: Long, results: Seq[IndexedProposal])

object ProposalsSearchResult {
  implicit val codec: JsonValueCodec[ProposalsSearchResult] =
    JsonCodecMaker.makeWithRequiredCollectionFields

  implicit val circeCodec: Codec[ProposalsSearchResult] = deriveCodec

  def empty: ProposalsSearchResult = ProposalsSearchResult(0, Seq.empty)
}

@ApiModel
final case class IndexedTag(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  tagId: TagId,
  label: String,
  display: Boolean
)

object IndexedTag {
  implicit val codec: JsonValueCodec[IndexedTag] =
    JsonCodecMaker.makeWithRequiredCollectionFields

  implicit val circeCodec: Codec[IndexedTag] = deriveCodec
}

sealed abstract class SequencePool(val value: String) extends StringEnumEntry

object SequencePool
    extends StringEnum[SequencePool]
    with StringCirceEnum[SequencePool]
    with JsoniterEnum[SequencePool] {

  case object New extends SequencePool("new")
  case object Tested extends SequencePool("tested")
  case object Excluded extends SequencePool("excluded")

  override def values: IndexedSeq[SequencePool] = findValues
  final val swaggerAllowableValues = "new,tested,excluded"
}

final case class IndexedProposalKeyword(
  @(ApiModelProperty @field)(dataType = "string", required = true)
  key: ProposalKeywordKey,
  label: String
)

object IndexedProposalKeyword {
  implicit val codec: Codec[IndexedProposalKeyword] = deriveCodec[IndexedProposalKeyword]

  def apply(keyword: ProposalKeyword): IndexedProposalKeyword = {
    IndexedProposalKeyword(keyword.key, keyword.label)
  }
}
