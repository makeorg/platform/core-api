/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.proposal

import org.make.core.proposal._
import spray.json._
import io.circe._
import io.circe.syntax._
import io.circe.generic.semiauto._
import org.make.api.proposal.ProposalScorer
import org.make.api.proposal.ProposalScorer.VotesCounter
import org.make.core.proposal.Vote._
import org.make.core.proposal.VoteKey._
import org.make.core.proposal.QualificationKey._

sealed trait VotingOptionWrapper {

  val vote: Vote

  val deprecatedQualificationsSeq: Seq[Qualification]
}

final case class AgreeWrapper(vote: Vote, likeIt: Qualification, platitudeAgree: Qualification, doable: Qualification)
    extends VotingOptionWrapper {

  lazy val deprecatedQualificationsSeq: Seq[Qualification] =
    Seq(likeIt, platitudeAgree, doable)
}

object AgreeWrapper {

  implicit val agreeWrapperFormatter: RootJsonFormat[AgreeWrapper] =
    DefaultJsonProtocol.jsonFormat(AgreeWrapper.apply, "vote", "likeIt", "platitudeAgree", "doable")

  implicit val codec: Codec[AgreeWrapper] = deriveCodec
}

final case class NeutralWrapper(
  vote: Vote,
  noOpinion: Qualification,
  doNotUnderstand: Qualification,
  doNotCare: Qualification
) extends VotingOptionWrapper {

  lazy val deprecatedQualificationsSeq: Seq[Qualification] =
    Seq(noOpinion, doNotUnderstand, doNotCare)
}

object NeutralWrapper {

  implicit val neutralWrapperFormatter: RootJsonFormat[NeutralWrapper] =
    DefaultJsonProtocol.jsonFormat(NeutralWrapper.apply, "vote", "noOpinion", "doNotUnderstand", "doNotCare")

  implicit val codec: Codec[NeutralWrapper] = deriveCodec
}

final case class DisagreeWrapper(
  vote: Vote,
  impossible: Qualification,
  noWay: Qualification,
  platitudeDisagree: Qualification
) extends VotingOptionWrapper {

  lazy val deprecatedQualificationsSeq: Seq[Qualification] =
    Seq(impossible, noWay, platitudeDisagree)
}

object DisagreeWrapper {

  implicit val disagreeWrapperFormatter: RootJsonFormat[DisagreeWrapper] =
    DefaultJsonProtocol.jsonFormat(DisagreeWrapper.apply, "vote", "impossible", "noWay", "platitudeDisagree")

  implicit val codec: Codec[DisagreeWrapper] = deriveCodec
}

final case class VotingOptions(agreeVote: AgreeWrapper, neutralVote: NeutralWrapper, disagreeVote: DisagreeWrapper) {

  val counts: VoteCounts = VoteCounts(this, _.count)
  val sequenceCounts: VoteCounts = VoteCounts(this, _.countSequence)
  val segmentCounts: VoteCounts = VoteCounts(this, _.countSegment)
  val verifiedCounts: VoteCounts = VoteCounts(this, _.countVerified)

  lazy val wrappers = Seq[VotingOptionWrapper](agreeVote, neutralVote, disagreeVote)

  def getWrapper(key: VoteKey): VotingOptionWrapper =
    key match {
      case Agree    => agreeVote
      case Neutral  => neutralVote
      case Disagree => disagreeVote
    }

  lazy val deprecatedVotesSeq: Seq[Vote] = Seq(agreeVote.vote, neutralVote.vote, disagreeVote.vote)

  def getVote(key: VoteKey): Vote = getWrapper(key).vote

  def updateVote(key: VoteKey, fn: Vote => Vote): VotingOptions =
    key match {
      case Agree    => this.copy(agreeVote = agreeVote.copy(vote = fn(agreeVote.vote)))
      case Neutral  => this.copy(neutralVote = neutralVote.copy(vote = fn(neutralVote.vote)))
      case Disagree => this.copy(disagreeVote = disagreeVote.copy(vote = fn(disagreeVote.vote)))
    }

  def getQualification(key: QualificationKey): Qualification =
    key match {
      case LikeIt            => agreeVote.likeIt
      case Doable            => agreeVote.doable
      case PlatitudeAgree    => agreeVote.platitudeAgree
      case DoNotUnderstand   => neutralVote.doNotUnderstand
      case NoOpinion         => neutralVote.noOpinion
      case DoNotCare         => neutralVote.doNotCare
      case NoWay             => disagreeVote.noWay
      case Impossible        => disagreeVote.impossible
      case PlatitudeDisagree => disagreeVote.platitudeDisagree
    }

  def updateQualification(key: QualificationKey, fn: Qualification => Qualification): VotingOptions =
    key match {
      case LikeIt         => this.copy(agreeVote = agreeVote.copy(likeIt = fn(agreeVote.likeIt)))
      case PlatitudeAgree => this.copy(agreeVote = agreeVote.copy(platitudeAgree = fn(agreeVote.platitudeAgree)))
      case Doable         => this.copy(agreeVote = agreeVote.copy(doable = fn(agreeVote.doable)))
      case NoOpinion      => this.copy(neutralVote = neutralVote.copy(noOpinion = fn(neutralVote.noOpinion)))
      case DoNotCare      => this.copy(neutralVote = neutralVote.copy(doNotCare = fn(neutralVote.doNotCare)))
      case DoNotUnderstand =>
        this.copy(neutralVote = neutralVote.copy(doNotUnderstand = fn(neutralVote.doNotUnderstand)))
      case Impossible => this.copy(disagreeVote = disagreeVote.copy(impossible = fn(disagreeVote.impossible)))
      case PlatitudeDisagree =>
        this.copy(disagreeVote = disagreeVote.copy(platitudeDisagree = fn(disagreeVote.platitudeDisagree)))
      case NoWay => this.copy(disagreeVote = disagreeVote.copy(noWay = fn(disagreeVote.noWay)))
    }

  def mapQualifications(fn: Qualification => Qualification): VotingOptions =
    this.copy(
      agreeVote = agreeVote.copy(
        likeIt = fn(agreeVote.likeIt),
        platitudeAgree = fn(agreeVote.platitudeAgree),
        doable = fn(agreeVote.doable)
      ),
      neutralVote = neutralVote.copy(
        noOpinion = fn(neutralVote.noOpinion),
        doNotCare = fn(neutralVote.doNotCare),
        doNotUnderstand = fn(neutralVote.doNotUnderstand)
      ),
      disagreeVote = disagreeVote.copy(
        impossible = fn(disagreeVote.impossible),
        platitudeDisagree = fn(disagreeVote.platitudeDisagree),
        noWay = fn(disagreeVote.noWay)
      )
    )

  def triMapVotes(agreeFn: Vote => Vote, neutralFn: Vote => Vote, disagreeFn: Vote => Vote): VotingOptions =
    this.copy(
      agreeVote = agreeVote.copy(vote = agreeFn(agreeVote.vote)),
      neutralVote = neutralVote.copy(vote = neutralFn(neutralVote.vote)),
      disagreeVote = disagreeVote.copy(vote = disagreeFn(disagreeVote.vote))
    )

  def mapVotes(fn: Vote => Vote): VotingOptions =
    triMapVotes(fn, fn, fn)
}

object VotingOptions {

  implicit object VotingOptionsFormat extends RootJsonFormat[VotingOptions] {

    def write(v: VotingOptions) =
      JsArray(
        JsObject(
          "key" -> JsString(v.agreeVote.vote.key.value),
          "count" -> JsNumber(v.agreeVote.vote.count),
          "countVerified" -> JsNumber(v.agreeVote.vote.countVerified),
          "countSequence" -> JsNumber(v.agreeVote.vote.countSequence),
          "countSegment" -> JsNumber(v.agreeVote.vote.countSegment),
          "qualifications" ->
            JsArray(v.agreeVote.doable.toJson, v.agreeVote.likeIt.toJson, v.agreeVote.platitudeAgree.toJson)
        ),
        JsObject(
          "key" -> JsString(v.neutralVote.vote.key.value),
          "count" -> JsNumber(v.neutralVote.vote.count),
          "countVerified" -> JsNumber(v.neutralVote.vote.countVerified),
          "countSequence" -> JsNumber(v.neutralVote.vote.countSequence),
          "countSegment" -> JsNumber(v.neutralVote.vote.countSegment),
          "qualifications" ->
            JsArray(
              v.neutralVote.doNotCare.toJson,
              v.neutralVote.doNotUnderstand.toJson,
              v.neutralVote.noOpinion.toJson
            )
        ),
        JsObject(
          "key" -> JsString(v.disagreeVote.vote.key.value),
          "count" -> JsNumber(v.disagreeVote.vote.count),
          "countVerified" -> JsNumber(v.disagreeVote.vote.countVerified),
          "countSequence" -> JsNumber(v.disagreeVote.vote.countSequence),
          "countSegment" -> JsNumber(v.disagreeVote.vote.countSegment),
          "qualifications" ->
            JsArray(
              v.disagreeVote.impossible.toJson,
              v.disagreeVote.noWay.toJson,
              v.disagreeVote.platitudeDisagree.toJson
            )
        )
      )

    def read(value: JsValue): VotingOptions =
      value match {
        case JsArray(votes: Vector[_]) =>
          LegacyVotesConverter.convertSeqToVotingOptions(votes.map(_.convertTo[Vote]))
        case str => deserializationError(s"Improper votes format: $str")
      }
  }

  implicit val encodeVotingOptions: Encoder[VotingOptions] = new Encoder[VotingOptions] {
    final def apply(v: VotingOptions): Json =
      Json.arr(
        Json.obj(
          "key" -> Json.fromString(v.agreeVote.vote.key.value),
          "count" -> Json.fromInt(v.agreeVote.vote.count),
          "countVerified" -> Json.fromInt(v.agreeVote.vote.countVerified),
          "countSequence" -> Json.fromInt(v.agreeVote.vote.countSequence),
          "countSegment" -> Json.fromInt(v.agreeVote.vote.countSegment),
          "qualifications" -> v.agreeVote.deprecatedQualificationsSeq.asJson
        ),
        Json.obj(
          "key" -> Json.fromString(v.neutralVote.vote.key.value),
          "count" -> Json.fromInt(v.neutralVote.vote.count),
          "countVerified" -> Json.fromInt(v.neutralVote.vote.countVerified),
          "countSequence" -> Json.fromInt(v.neutralVote.vote.countSequence),
          "countSegment" -> Json.fromInt(v.neutralVote.vote.countSegment),
          "qualifications" -> v.neutralVote.deprecatedQualificationsSeq.asJson
        ),
        Json.obj(
          "key" -> Json.fromString(v.disagreeVote.vote.key.value),
          "count" -> Json.fromInt(v.disagreeVote.vote.count),
          "countVerified" -> Json.fromInt(v.disagreeVote.vote.countVerified),
          "countSequence" -> Json.fromInt(v.disagreeVote.vote.countSequence),
          "countSegment" -> Json.fromInt(v.disagreeVote.vote.countSegment),
          "qualifications" -> v.disagreeVote.deprecatedQualificationsSeq.asJson
        )
      )
  }

  implicit val decodeVotingOptions: Decoder[VotingOptions] = new Decoder[VotingOptions] {
    final def apply(v: HCursor): Decoder.Result[VotingOptions] =
      Decoder
        .decodeSeq[Vote](Vote.decoder)(v)
        .map(LegacyVotesConverter.convertSeqToVotingOptions)
  }

  import com.github.plokhotnyuk.jsoniter_scala.core._
  import com.github.plokhotnyuk.jsoniter_scala.macros._

  @SuppressWarnings(Array("org.wartremover.warts.Null", "org.wartremover.warts.AsInstanceOf"))
  implicit val votingOptionsCodec: JsonValueCodec[VotingOptions] =
    new JsonValueCodec[VotingOptions] {

      private val votesCodec: JsonValueCodec[Array[Vote]] =
        JsonCodecMaker.makeWithRequiredCollectionFields[Array[Vote]]

      def decodeValue(in: JsonReader, default: VotingOptions): VotingOptions =
        LegacyVotesConverter.convertSeqToVotingOptions(votesCodec.decodeValue(in, null).toSeq)

      def encodeValue(v: VotingOptions, out: JsonWriter): Unit = {
        val data =
          Array(
            v.agreeVote.vote
              .copy(qualifications = Seq(v.agreeVote.likeIt, v.agreeVote.doable, v.agreeVote.platitudeAgree)),
            v.neutralVote.vote.copy(qualifications =
              Seq(v.neutralVote.doNotUnderstand, v.neutralVote.noOpinion, v.neutralVote.doNotCare)
            ),
            v.disagreeVote.vote.copy(qualifications =
              Seq(v.disagreeVote.noWay, v.disagreeVote.impossible, v.disagreeVote.platitudeDisagree)
            )
          )
        votesCodec.encodeValue(data, out)
      }

      def nullValue: VotingOptions = null.asInstanceOf[VotingOptions]
    }

  val empty: VotingOptions =
    VotingOptions(
      AgreeWrapper(
        vote = Vote(VoteKey.Agree, 0, 0, 0, 0),
        likeIt = Qualification(QualificationKey.LikeIt, 0, 0, 0, 0),
        platitudeAgree = Qualification(QualificationKey.PlatitudeAgree, 0, 0, 0, 0),
        doable = Qualification(QualificationKey.Doable, 0, 0, 0, 0)
      ),
      NeutralWrapper(
        vote = Vote(VoteKey.Neutral, 0, 0, 0, 0),
        noOpinion = Qualification(QualificationKey.NoOpinion, 0, 0, 0, 0),
        doNotUnderstand = Qualification(QualificationKey.DoNotUnderstand, 0, 0, 0, 0),
        doNotCare = Qualification(QualificationKey.DoNotCare, 0, 0, 0, 0)
      ),
      DisagreeWrapper(
        vote = Vote(VoteKey.Disagree, 0, 0, 0, 0),
        impossible = Qualification(QualificationKey.Impossible, 0, 0, 0, 0),
        noWay = Qualification(QualificationKey.NoWay, 0, 0, 0, 0),
        platitudeDisagree = Qualification(QualificationKey.PlatitudeDisagree, 0, 0, 0, 0)
      )
    )
}

final case class VoteCounts(
  agreeVote: Int,
  disagreeVote: Int,
  neutralVote: Int,
  doNotCare: Int,
  doNotUnderstand: Int,
  doable: Int,
  impossible: Int,
  likeIt: Int,
  noOpinion: Int,
  noWay: Int,
  platitudeAgree: Int,
  platitudeDisagree: Int
) {
  val totalVotes: Int = agreeVote + neutralVote + disagreeVote
}

object VoteCounts {
  def apply(votingOptions: VotingOptions, counter: BaseVoteOrQualification[_] => Int): VoteCounts =
    VoteCounts(
      agreeVote = counter(votingOptions.agreeVote.vote),
      disagreeVote = counter(votingOptions.disagreeVote.vote),
      neutralVote = counter(votingOptions.neutralVote.vote),
      doNotCare = counter(votingOptions.neutralVote.doNotCare),
      doNotUnderstand = counter(votingOptions.neutralVote.doNotUnderstand),
      doable = counter(votingOptions.agreeVote.doable),
      impossible = counter(votingOptions.disagreeVote.impossible),
      likeIt = counter(votingOptions.agreeVote.likeIt),
      noOpinion = counter(votingOptions.neutralVote.noOpinion),
      noWay = counter(votingOptions.disagreeVote.noWay),
      platitudeAgree = counter(votingOptions.agreeVote.platitudeAgree),
      platitudeDisagree = counter(votingOptions.disagreeVote.platitudeDisagree)
    )
}

final case class VotingOptionsScores(agree: Double, neutral: Double, disagree: Double) {
  val get: VoteKey => Double = {
    case Agree    => agree
    case Neutral  => neutral
    case Disagree => disagree
  }
}

object VotingOptionsScores {

  val empty: VotingOptionsScores = VotingOptionsScores(0, 0, 0)

  def apply(votingOptions: VotingOptions, newProposalsVoteThreshold: Int): VotingOptionsScores = {
    if (votingOptions.verifiedCounts.totalVotes == 0) {
      this.empty
    } else if (votingOptions.verifiedCounts.totalVotes < newProposalsVoteThreshold) {
      VotingOptionsScores(
        agree = votingOptions.agreeVote.vote.countVerified.toDouble / votingOptions.verifiedCounts.totalVotes,
        neutral = votingOptions.neutralVote.vote.countVerified.toDouble / votingOptions.verifiedCounts.totalVotes,
        disagree = votingOptions.disagreeVote.vote.countVerified.toDouble / votingOptions.verifiedCounts.totalVotes
      )
    } else {
      val sequenceScorer = ProposalScorer(votingOptions, VotesCounter.SequenceVotesCounter, 0.5)
      val roundingMode = BigDecimal.RoundingMode.HALF_UP
      val agreeScore = BigDecimal(sequenceScorer.agree.score).setScale(2, roundingMode).toDouble
      val disagreeScore = BigDecimal(sequenceScorer.disagree.score).setScale(2, roundingMode).toDouble
      val neutralScore = BigDecimal(1 - (agreeScore + disagreeScore)).setScale(2, roundingMode).toDouble

      VotingOptionsScores(agree = agreeScore, neutral = neutralScore, disagree = disagreeScore)
    }
  }
}
