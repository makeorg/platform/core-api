/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.proposal

import grizzled.slf4j.Logging
import org.make.core.proposal.{Qualification, VotingOptions}
import org.make.core.proposal.QualificationKey._

object LegacyVotesConverter extends Logging {

  private def findVoteOrFallback(votes: Seq[Vote], key: VoteKey): Vote =
    votes.find(_.key == key) match {
      case Some(baseVote) => baseVote
      case None =>
        logger.warn(s"No $key vote found in: ${votes.mkString(", ")}")
        Vote(key, 0, 0, 0, 0)
    }

  private def findQualificationOrFallback(qualifications: Seq[Qualification], key: QualificationKey): Qualification =
    qualifications.find(_.key == key) match {
      case Some(qualification) => qualification
      case None =>
        logger.warn(s"No $key qualification found in: ${qualifications.mkString(", ")}")
        Qualification(key, 0, 0, 0, 0)
    }

  def convertSeqToVotingOptions(votes: Seq[Vote]): VotingOptions = {
    val agreeOption = findVoteOrFallback(votes, VoteKey.Agree)
    val neutralOption = findVoteOrFallback(votes, VoteKey.Neutral)
    val disagreeOption = findVoteOrFallback(votes, VoteKey.Disagree)
    VotingOptions(
      AgreeWrapper(
        vote = agreeOption.copy(qualifications = Seq.empty),
        likeIt = findQualificationOrFallback(agreeOption.qualifications, LikeIt),
        platitudeAgree = findQualificationOrFallback(agreeOption.qualifications, PlatitudeAgree),
        doable = findQualificationOrFallback(agreeOption.qualifications, Doable)
      ),
      NeutralWrapper(
        vote = neutralOption.copy(qualifications = Seq.empty),
        noOpinion = findQualificationOrFallback(neutralOption.qualifications, NoOpinion),
        doNotUnderstand = findQualificationOrFallback(neutralOption.qualifications, DoNotUnderstand),
        doNotCare = findQualificationOrFallback(neutralOption.qualifications, DoNotCare)
      ),
      DisagreeWrapper(
        vote = disagreeOption.copy(qualifications = Seq.empty),
        impossible = findQualificationOrFallback(disagreeOption.qualifications, Impossible),
        noWay = findQualificationOrFallback(disagreeOption.qualifications, NoWay),
        platitudeDisagree = findQualificationOrFallback(disagreeOption.qualifications, PlatitudeDisagree)
      )
    )
  }
}
