/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.sequence

import enumeratum.EnumEntry.Lowercase
import enumeratum.{Circe, Enum, EnumEntry}
import enumeratum.values.{StringCirceEnum, StringEnum, StringEnumEntry}
import eu.timepit.refined.auto._
import eu.timepit.refined.types.numeric._
import io.circe.{Codec, Decoder, Encoder}
import io.swagger.annotations.ApiModelProperty
import org.make.core._
import org.make.core.question.QuestionId
import org.make.core.technical.RefinedTypes.Ratio

import scala.annotation.meta.field

sealed abstract class SelectionAlgorithmName(val value: String) extends StringEnumEntry
object SelectionAlgorithmName extends StringEnum[SelectionAlgorithmName] with StringCirceEnum[SelectionAlgorithmName] {

  final case object Bandit extends SelectionAlgorithmName("Bandit")
  final case object RoundRobin extends SelectionAlgorithmName("RoundRobin")
  final case object Random extends SelectionAlgorithmName("Random")

  override val values: IndexedSeq[SelectionAlgorithmName] = findValues
  final val swaggerAllowableValues = "Bandit,RoundRobin,Random"
}

final case class SequenceConfiguration(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  mainSequence: ExplorationSequenceConfiguration,
  controversial: SpecificSequenceConfiguration,
  popular: SpecificSequenceConfiguration,
  keyword: SpecificSequenceConfiguration,
  newProposalsVoteThreshold: Int,
  @(ApiModelProperty @field)(dataType = "double") testedProposalsEngagementThreshold: Option[Double],
  @(ApiModelProperty @field)(dataType = "double") testedProposalsScoreThreshold: Option[Double],
  @(ApiModelProperty @field)(dataType = "double") testedProposalsControversyThreshold: Option[Double],
  @(ApiModelProperty @field)(dataType = "int") testedProposalsMaxVotesThreshold: Option[Int],
  nonSequenceVotesWeight: Double
)

object SequenceConfiguration {

  val default: SequenceConfiguration = SequenceConfiguration(
    questionId = QuestionId("default-question"),
    mainSequence = ExplorationSequenceConfiguration.default(ExplorationSequenceConfigurationId("default-sequence")),
    controversial =
      SpecificSequenceConfiguration.otherSequenceDefault(SpecificSequenceConfigurationId("default-controversial")),
    popular = SpecificSequenceConfiguration.otherSequenceDefault(SpecificSequenceConfigurationId("default-popular")),
    keyword = SpecificSequenceConfiguration.otherSequenceDefault(SpecificSequenceConfigurationId("default-keyword")),
    newProposalsVoteThreshold = 10,
    testedProposalsEngagementThreshold = None,
    testedProposalsScoreThreshold = None,
    testedProposalsControversyThreshold = None,
    testedProposalsMaxVotesThreshold = Some(1500),
    nonSequenceVotesWeight = 0.5
  )

}

sealed trait BasicSequenceConfiguration {
  def sequenceSize: PosInt
  def maxTestedProposalCount: PosInt
}

final case class ExplorationSequenceConfiguration(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  explorationSequenceConfigurationId: ExplorationSequenceConfigurationId,
  @(ApiModelProperty @field)(dataType = "int", allowableValues = "range[1, infinity]", required = true)
  sequenceSize: PosInt,
  @(ApiModelProperty @field)(dataType = "int", allowableValues = "range[1, infinity]", required = true)
  maxTestedProposalCount: PosInt,
  @(ApiModelProperty @field)(dataType = "double", allowableValues = "range[0, 1]", required = true)
  newRatio: Ratio,
  @(ApiModelProperty @field)(dataType = "double", allowableValues = "range[0, 1]", required = true)
  controversyRatio: Ratio,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = ExplorationSortAlgorithm.swaggerAllowableValues
  )
  topSorter: ExplorationSortAlgorithm,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = ExplorationSortAlgorithm.swaggerAllowableValues
  )
  controversySorter: ExplorationSortAlgorithm,
  @(ApiModelProperty @field)(dataType = "double", allowableValues = "range[0, 1]", required = true)
  keywordsThreshold: Ratio,
  candidatesPoolSize: Int
) extends BasicSequenceConfiguration

object ExplorationSequenceConfiguration {
  def default(id: ExplorationSequenceConfigurationId): ExplorationSequenceConfiguration =
    ExplorationSequenceConfiguration(
      id,
      sequenceSize = 12,
      maxTestedProposalCount = 1000,
      newRatio = 0.3,
      controversyRatio = 0.3,
      topSorter = ExplorationSortAlgorithm.Equalizer,
      controversySorter = ExplorationSortAlgorithm.Equalizer,
      keywordsThreshold = 0.2,
      candidatesPoolSize = 10
    )
}

final case class ExplorationSequenceConfigurationId(value: String) extends StringValue

object ExplorationSequenceConfigurationId {
  implicit val codec: Codec[ExplorationSequenceConfigurationId] =
    Codec.from(Decoder[String].map(ExplorationSequenceConfigurationId.apply), Encoder[String].contramap(_.value))
}

sealed abstract class ExplorationSortAlgorithm(val value: String) extends StringEnumEntry

object ExplorationSortAlgorithm
    extends StringEnum[ExplorationSortAlgorithm]
    with StringCirceEnum[ExplorationSortAlgorithm] {

  case object Bandit extends ExplorationSortAlgorithm("bandit")
  case object Random extends ExplorationSortAlgorithm("random")
  case object Equalizer extends ExplorationSortAlgorithm("equalizer")

  override val values: IndexedSeq[ExplorationSortAlgorithm] = findValues
  final val swaggerAllowableValues = "bandit,random,equalizer"
}

final case class SpecificSequenceConfiguration(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  specificSequenceConfigurationId: SpecificSequenceConfigurationId,
  @(ApiModelProperty @field)(dataType = "int", example = "12", allowableValues = "range[1, infinity]", required = true)
  sequenceSize: PosInt = 12,
  @(ApiModelProperty @field)(dataType = "double", example = "0.3", required = true)
  newProposalsRatio: Double = 0.3,
  @(ApiModelProperty @field)(
    dataType = "int",
    example = "1000",
    allowableValues = "range[1, infinity]",
    required = true
  )
  maxTestedProposalCount: PosInt = 1000,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = SelectionAlgorithmName.swaggerAllowableValues
  )
  selectionAlgorithmName: SelectionAlgorithmName = SelectionAlgorithmName.Bandit
) extends BasicSequenceConfiguration

object SpecificSequenceConfiguration {

  def otherSequenceDefault(id: SpecificSequenceConfigurationId): SpecificSequenceConfiguration =
    SpecificSequenceConfiguration(
      specificSequenceConfigurationId = id,
      newProposalsRatio = 0,
      selectionAlgorithmName = SelectionAlgorithmName.Random
    )
}

final case class SpecificSequenceConfigurationId(value: String) extends StringValue

object SpecificSequenceConfigurationId {
  implicit val specificSequenceConfigurationIdCodec: Codec[SpecificSequenceConfigurationId] =
    Codec.from(Decoder[String].map(SpecificSequenceConfigurationId.apply), Encoder[String].contramap(_.value))
}

sealed abstract class SequenceKind extends EnumEntry with Lowercase

object SequenceKind extends Enum[SequenceKind] {

  case object Standard extends SequenceKind
  case object Consensus extends SequenceKind
  case object Controversy extends SequenceKind
  case object Keyword extends SequenceKind

  override val values: IndexedSeq[SequenceKind] = findValues
  final val swaggerAllowableValues = "Standard,Consensus,Controversy,Keyword"

  implicit val encoder: Encoder[SequenceKind] = Circe.encoderLowercase(this)
  implicit val decoder: Decoder[SequenceKind] = Circe.decodeCaseInsensitive(this)
}
