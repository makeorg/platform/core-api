/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.operation

import enumeratum.values.{StringCirceEnum, StringEnum, StringEnumEntry}
import enumeratum.{Circe, Enum, EnumEntry}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.MaxSize
import io.circe.generic.semiauto._
import io.circe.{Codec, Decoder, Encoder, Json}
import io.circe.refined._
import io.swagger.annotations.ApiModelProperty
import org.make.core.SprayJsonFormatters._
import org.make.core._
import org.make.core.Validation._
import org.make.core.technical.Multilingual
import org.make.core.technical.enumeratum.EnumKeys.StringEnumKeys
import org.make.core.question.{Question, QuestionId}
import org.make.core.user.UserId
import org.make.core.jsoniter.JsoniterEnum
import com.github.plokhotnyuk.jsoniter_scala.core._
import spray.json.DefaultJsonProtocol._
import spray.json.{DefaultJsonProtocol, JsonFormat, RootJsonFormat}

import java.time.{LocalDate, ZonedDateTime}
import scala.annotation.meta.field

final case class QuestionWithDetails(question: Question, details: OperationOfQuestion)

final case class Operation(
  operationId: OperationId,
  slug: String,
  events: List[OperationAction],
  questions: Seq[QuestionWithDetails],
  operationKind: OperationKind,
  override val createdAt: Option[ZonedDateTime],
  override val updatedAt: Option[ZonedDateTime]
) extends MakeSerializable
    with Timestamped

final case class OperationId(value: String) extends StringValue

object OperationId {
  implicit lazy val operationIdEncoder: Encoder[OperationId] =
    (a: OperationId) => Json.fromString(a.value)
  implicit lazy val operationIdDecoder: Decoder[OperationId] =
    Decoder.decodeString.map(OperationId(_))

  implicit val operationIdFormatter: JsonFormat[OperationId] = SprayJsonFormatters.forStringValue(OperationId.apply)

  implicit val operationIdCodec: JsonValueCodec[OperationId] =
    StringValue.makeCodec(OperationId.apply)
}

final case class IntroCard(
  enabled: Boolean,
  titles: Option[Multilingual[String]],
  descriptions: Option[Multilingual[String]]
)

object IntroCard extends CirceFormatters {

  implicit val codec: Codec[IntroCard] = deriveCodec
}

final case class PushProposalCard(enabled: Boolean)

object PushProposalCard extends CirceFormatters {

  implicit val codec: Codec[PushProposalCard] = deriveCodec
}

final case class FinalCard(
  enabled: Boolean,
  sharingEnabled: Boolean,
  titles: Option[Multilingual[String]],
  shareDescriptions: Option[Multilingual[String]],
  learnMoreTitles: Option[Multilingual[String]],
  learnMoreTextButtons: Option[Multilingual[String]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/link")
  linkUrl: Option[String]
)

object FinalCard extends CirceFormatters {

  implicit val codec: Codec[FinalCard] = deriveCodec
}

final case class SequenceCardsConfiguration(
  introCard: IntroCard,
  pushProposalCard: PushProposalCard,
  finalCard: FinalCard
)

object SequenceCardsConfiguration extends CirceFormatters {

  implicit val codec: Codec[SequenceCardsConfiguration] = deriveCodec

  val default: SequenceCardsConfiguration = SequenceCardsConfiguration(
    introCard = IntroCard(enabled = true, titles = None, descriptions = None),
    pushProposalCard = PushProposalCard(enabled = true),
    finalCard = FinalCard(
      enabled = true,
      sharingEnabled = true,
      titles = None,
      shareDescriptions = None,
      learnMoreTitles = None,
      learnMoreTextButtons = None,
      linkUrl = None
    )
  )
}

final case class Metas(
  titles: Option[Multilingual[String]],
  descriptions: Option[Multilingual[String]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/picture.png")
  picture: Option[String]
)

object Metas extends CirceFormatters {

  implicit val codec: Codec[Metas] = deriveCodec
}

final case class QuestionTheme(
  @(ApiModelProperty @field)(dataType = "string", example = "#842142", required = true)
  color: String Refined Colour,
  @(ApiModelProperty @field)(dataType = "string", example = "#ff0000", required = true)
  fontColor: String Refined Colour
)

object QuestionTheme {

  implicit val codec: Codec[QuestionTheme] = deriveCodec

  val default: QuestionTheme = QuestionTheme(color = Colour.black, fontColor = Colour.black)
}

final case class OperationOfQuestion(
  questionId: QuestionId,
  operationId: OperationId,
  startDate: ZonedDateTime,
  endDate: ZonedDateTime,
  operationTitles: Multilingual[String],
  proposalPrefixes: Multilingual[String],
  canPropose: Boolean,
  sequenceCardsConfiguration: SequenceCardsConfiguration,
  aboutUrls: Option[Multilingual[String]],
  metas: Metas,
  theme: QuestionTheme,
  descriptions: Option[Multilingual[String]],
  consultationImages: Option[Multilingual[String]],
  consultationImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
  descriptionImages: Option[Multilingual[String]],
  descriptionImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
  partnersLogos: Option[String],
  partnersLogosAlt: Option[String Refined MaxSize[130]],
  initiatorsLogos: Option[String],
  initiatorsLogosAlt: Option[String Refined MaxSize[130]],
  consultationHeader: Option[String],
  consultationHeaderAlts: Option[Multilingual[String Refined MaxSize[130]]],
  cobrandingLogo: Option[String],
  cobrandingLogoAlt: Option[String Refined MaxSize[130]],
  resultsLink: Option[ResultsLink],
  proposalsCount: Int,
  participantsCount: Int,
  actions: Option[String],
  featured: Boolean,
  votesCount: Int,
  votesTarget: Int,
  timeline: OperationOfQuestionTimeline,
  createdAt: ZonedDateTime,
  sessionBindingMode: Boolean,
  reportUrl: Option[String],
  actionsUrl: Option[String]
) {

  def status: OperationOfQuestion.Status = {
    val now = DateHelper.now()
    if (startDate.isAfter(now)) {
      OperationOfQuestion.Status.Upcoming
    } else if (endDate.isBefore(now)) {
      OperationOfQuestion.Status.Finished
    } else {
      OperationOfQuestion.Status.Open
    }
  }
}

object OperationOfQuestion {
  sealed abstract class Status extends EnumEntry
  object Status extends Enum[Status] {
    case object Upcoming extends Status
    case object Open extends Status
    case object Finished extends Status
    override val values: IndexedSeq[Status] = findValues
    final val swaggerAllowableValues = "upcoming,open,finished"
    implicit val decoder: Decoder[Status] = Circe.decodeCaseInsensitive(this)
    implicit val encoder: Encoder[Status] = Circe.encoderLowercase(this)
  }
}

final case class OperationAction(
  date: ZonedDateTime = DateHelper.now(),
  makeUserId: UserId,
  actionType: String,
  arguments: Map[String, String] = Map.empty
)

object OperationAction {

  implicit val operationActionFormatter: RootJsonFormat[OperationAction] =
    DefaultJsonProtocol.jsonFormat4(OperationAction.apply)
}

sealed abstract class OperationActionType(val value: String) extends StringEnumEntry

object OperationActionType extends StringEnum[OperationActionType] {

  case object OperationCreateAction extends OperationActionType("create")
  case object OperationUpdateAction extends OperationActionType("update")

  override val values: IndexedSeq[OperationActionType] = findValues

}

final case class SimpleOperation(
  operationId: OperationId,
  slug: String,
  operationKind: OperationKind,
  createdAt: Option[ZonedDateTime],
  updatedAt: Option[ZonedDateTime]
)

object SimpleOperation extends CirceFormatters {

  implicit val codec: Codec[SimpleOperation] = deriveCodec
}

sealed abstract class OperationKind(val value: String) extends StringEnumEntry with Product with Serializable

object OperationKind
    extends StringEnum[OperationKind]
    with StringCirceEnum[OperationKind]
    with StringEnumKeys[OperationKind]
    with JsoniterEnum[OperationKind] {

  case object GreatCause extends OperationKind("GREAT_CAUSE")
  case object PrivateConsultation extends OperationKind("PRIVATE_CONSULTATION")
  case object BusinessConsultation extends OperationKind("BUSINESS_CONSULTATION")

  override def values: IndexedSeq[OperationKind] = findValues
  final val swaggerAllowableValues = "GREAT_CAUSE,PRIVATE_CONSULTATION,BUSINESS_CONSULTATION"

  val publicKinds: Seq[OperationKind] = Seq(OperationKind.GreatCause, OperationKind.BusinessConsultation)
}

final case class OperationOfQuestionTimeline(
  action: Option[TimelineElement],
  result: Option[TimelineElement],
  workshop: Option[TimelineElement]
)

object OperationOfQuestionTimeline extends CirceFormatters {

  implicit val codec: Codec[OperationOfQuestionTimeline] = deriveCodec
}

final case class TimelineElement(
  date: LocalDate,
  dateTexts: Multilingual[String Refined MaxSize[20]] = Multilingual.empty,
  descriptions: Multilingual[String Refined MaxSize[150]] = Multilingual.empty
)

object TimelineElement extends CirceFormatters {

  implicit val codec: Codec[TimelineElement] = deriveCodec
}
