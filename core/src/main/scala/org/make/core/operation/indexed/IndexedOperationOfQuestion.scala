/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.operation.indexed

import cats.Show

import java.time.ZonedDateTime
import cats.data.NonEmptyList
import enumeratum.values.StringEnum
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection._
import io.circe.generic.semiauto._
import io.circe.refined._
import io.circe.Codec
import io.swagger.annotations.ApiModelProperty
import org.make.core.elasticsearch.ElasticsearchFieldName
import org.make.core.CirceFormatters
import org.make.core.operation.{OperationId, OperationOfQuestion, QuestionTheme, ResultsLink, SimpleOperation}
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import org.make.core.technical.Multilingual

import scala.annotation.meta.field

sealed abstract class OperationOfQuestionElasticsearchFieldName(val value: String, val sortable: Boolean = false)
    extends ElasticsearchFieldName

object OperationOfQuestionElasticsearchFieldName extends StringEnum[OperationOfQuestionElasticsearchFieldName] {

  sealed abstract class Simple(val field: String, override val sortable: Boolean = false)
      extends OperationOfQuestionElasticsearchFieldName(field, sortable) {
    override def parameter: String = field
  }

  sealed abstract class Alias(
    val parameter: String,
    aliased: OperationOfQuestionElasticsearchFieldName,
    override val sortable: Boolean = false
  ) extends OperationOfQuestionElasticsearchFieldName(parameter, sortable) {
    override def field: String = aliased.field
  }

  case object questionId extends Simple("questionId")
  case object questionKeyword extends Simple("questionKeyword", sortable = true)
  case object question extends Simple("questionGeneral")
  case object questionBg extends Simple("questions.bg.standard")
  case object questionBgStemmed extends Simple("questions.bg.stemmed")
  case object questionCs extends Simple("questions.cs.standard")
  case object questionCsStemmed extends Simple("questions.cs.stemmed")
  case object questionDa extends Simple("questions.da.standard")
  case object questionDaStemmed extends Simple("questions.da.stemmed")
  case object questionDe extends Simple("questions.de.standard")
  case object questionDeStemmed extends Simple("questions.de.stemmed")
  case object questionEl extends Simple("questions.el.standard")
  case object questionElStemmed extends Simple("questions.el.stemmed")
  case object questionEn extends Simple("questions.en.standard")
  case object questionEnStemmed extends Simple("questions.en.stemmed")
  case object questionEs extends Simple("questions.es.standard")
  case object questionEsStemmed extends Simple("questions.es.stemmed")
  case object questionEt extends Simple("questions.et.standard")
  case object questionFi extends Simple("questions.fi.standard")
  case object questionFiStemmed extends Simple("questions.fi.stemmed")
  case object questionFr extends Simple("questions.fr.standard")
  case object questionFrStemmed extends Simple("questions.fr.stemmed")
  case object questionHr extends Simple("questions.hr.standard")
  case object questionHu extends Simple("questions.hu.standard")
  case object questionHuStemmed extends Simple("questions.hu.stemmed")
  case object questionIt extends Simple("questions.it.standard")
  case object questionItStemmed extends Simple("questions.it.stemmed")
  case object questionLt extends Simple("questions.lt.standard")
  case object questionLtStemmed extends Simple("questions.lt.stemmed")
  case object questionLv extends Simple("questions.lv.standard")
  case object questionLvStemmed extends Simple("questions.lv.stemmed")
  case object questionMt extends Simple("questions.mt.standard")
  case object questionNl extends Simple("questions.nl.standard")
  case object questionNlStemmed extends Simple("questions.nl.stemmed")
  case object questionPl extends Simple("questions.pl.standard")
  case object questionPlStemmed extends Simple("questions.pl.stemmed")
  case object questionPt extends Simple("questions.pt.standard")
  case object questionPtStemmed extends Simple("questions.pt.stemmed")
  case object questionRo extends Simple("questions.ro.standard")
  case object questionRoStemmed extends Simple("questions.ro.stemmed")
  case object questionSk extends Simple("questions.sk.standard")
  case object questionSl extends Simple("questions.sl.standard")
  case object questionUk extends Simple("questions.uk.standard")
  case object questionSv extends Simple("questions.sv.standard")
  case object questionSvStemmed extends Simple("questions.sv.stemmed")
  case object slug extends Simple("slug", sortable = true)
  case object startDate extends Simple("startDate", sortable = true)
  case object endDate extends Simple("endDate", sortable = true)
  case object countries extends Simple("countries", sortable = true)
  case object languages extends Simple("languages", sortable = true)
  case object operationKind extends Simple("operationKind", sortable = true)
  case object featured extends Simple("featured")
  case object status extends Simple("status")
  case object participantsCount extends Simple("participantsCount")
  case object proposalsCount extends Simple("proposalsCount")
  case object resultsLink extends Simple("resultsLink")

  override val values: IndexedSeq[OperationOfQuestionElasticsearchFieldName] = findValues
  final val swaggerAllowableValues =
    "question,slug,startDate,endDate,description,countries,country,language,operationKind"
}

final case class IndexedOperationOfQuestion(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  questions: Multilingual[String Refined NonEmpty],
  questionGeneral: String Refined NonEmpty,
  questionKeyword: String Refined NonEmpty,
  slug: String,
  questionShortTitles: Option[Multilingual[String Refined NonEmpty]],
  startDate: ZonedDateTime,
  endDate: ZonedDateTime,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = OperationOfQuestion.Status.swaggerAllowableValues
  )
  status: OperationOfQuestion.Status,
  theme: QuestionTheme,
  descriptions: Option[Multilingual[String]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/consultation-image.png")
  consultationImages: Option[Multilingual[String]],
  @(ApiModelProperty @field)(dataType = "string", example = "consultation image alternative")
  consultationImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/description-image.png")
  descriptionImages: Option[Multilingual[String]],
  @(ApiModelProperty @field)(dataType = "string", example = "description image alternative")
  descriptionImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  countries: NonEmptyList[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  languages: NonEmptyList[Language],
  defaultLanguage: Language,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  operationId: OperationId,
  operationTitles: Multilingual[String],
  operationKind: String,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/about")
  aboutUrls: Option[Multilingual[String]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/results")
  resultsLink: Option[String],
  proposalsCount: Int,
  participantsCount: Int,
  votesCount: Int,
  actions: Option[String],
  featured: Boolean,
  @(ApiModelProperty @field)(dataType = "double") top20ConsensusThreshold: Option[Double]
) {
  val immutableFields: IndexedOperationOfQuestion.ImmutableFields =
    IndexedOperationOfQuestion.ImmutableFields(top20ConsensusThreshold = top20ConsensusThreshold)

  def applyImmutableFields(fields: IndexedOperationOfQuestion.ImmutableFields): IndexedOperationOfQuestion =
    this.copy(top20ConsensusThreshold = fields.top20ConsensusThreshold)
}

object IndexedOperationOfQuestion extends CirceFormatters {

  implicit val codec: Codec[IndexedOperationOfQuestion] = deriveCodec

  def createFromOperationOfQuestion(
    operationOfQuestion: OperationOfQuestion,
    operation: SimpleOperation,
    question: Question
  ): IndexedOperationOfQuestion = {
    IndexedOperationOfQuestion(
      questionId = operationOfQuestion.questionId,
      questions = question.questions,
      questionGeneral = question.questions.getTranslationUnsafe(question.defaultLanguage),
      questionKeyword = question.questions.getTranslationUnsafe(question.defaultLanguage),
      slug = question.slug,
      questionShortTitles = question.shortTitles,
      startDate = operationOfQuestion.startDate,
      endDate = operationOfQuestion.endDate,
      status = operationOfQuestion.status,
      theme = operationOfQuestion.theme,
      descriptions = operationOfQuestion.descriptions,
      consultationImages = operationOfQuestion.consultationImages,
      consultationImageAlts = operationOfQuestion.consultationImageAlts,
      descriptionImages = operationOfQuestion.descriptionImages,
      descriptionImageAlts = operationOfQuestion.descriptionImageAlts,
      countries = question.countries,
      languages = question.languages,
      defaultLanguage = question.defaultLanguage,
      operationId = operationOfQuestion.operationId,
      operationTitles = operationOfQuestion.operationTitles,
      operationKind = operation.operationKind.value,
      aboutUrls = operationOfQuestion.aboutUrls,
      resultsLink = operationOfQuestion.resultsLink.map(Show[ResultsLink].show),
      proposalsCount = operationOfQuestion.proposalsCount,
      participantsCount = operationOfQuestion.participantsCount,
      votesCount = operationOfQuestion.votesCount,
      actions = operationOfQuestion.actions,
      featured = operationOfQuestion.featured,
      top20ConsensusThreshold = None
    )
  }

  final case class ImmutableFields(
    @(ApiModelProperty @field)(dataType = "double")
    top20ConsensusThreshold: Option[Double]
  )

  object ImmutableFields {
    val empty: ImmutableFields = ImmutableFields(top20ConsensusThreshold = None)
  }

}

final case class OperationOfQuestionSearchResult(total: Long, results: Seq[IndexedOperationOfQuestion])

object OperationOfQuestionSearchResult {
  implicit val codec: Codec[OperationOfQuestionSearchResult] = deriveCodec

  def empty: OperationOfQuestionSearchResult = OperationOfQuestionSearchResult(0, Seq.empty)
}
