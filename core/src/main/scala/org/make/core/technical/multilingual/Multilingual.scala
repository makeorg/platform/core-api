/*
 *  Make.org Core API
 *  Copyright (C) 2020 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.technical

import cats.implicits._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.NonEmpty
import org.make.core.reference.Language

final case class Multilingual[T](private val pairs: Map[Language, T]) {

  override def toString: String =
    pairs.map {
      case (Language(l), v) => s"$l: $v"
    }.mkString("\n")

  def getTranslation(lang: Language): Option[T] =
    pairs.get(lang)

  def getTranslationUnsafe(lang: Language): T =
    pairs(lang)

  def addTranslation(lang: Language, translation: T): Multilingual[T] =
    new Multilingual[T](pairs.updated(lang, translation))

  def mapTranslations[U](fn: T => U): Multilingual[U] =
    new Multilingual[U](pairs.view.mapValues(fn).toMap)

  def toMap: Map[Language, T] =
    pairs

  def providedLanguages: Set[Language] =
    pairs.keys.toSet

  def translations: LazyList[T] =
    pairs.values.to(LazyList)

  def isEmpty: Boolean =
    pairs.isEmpty
}

object Multilingual {

  private val BaseLanguage = Language("fr")

  def empty[T]: Multilingual[T] =
    new Multilingual[T](Map.empty)

  def apply[T](elements: (Language, T)*): Multilingual[T] =
    new Multilingual[T](Map(elements: _*))

  @SuppressWarnings(Array("org.wartremover.warts.Null"))
  def fromMap[T](elements: Map[String, T]): Multilingual[T] = elements match {
    case null => new Multilingual[T](Map.empty)
    case _    => new Multilingual[T](elements.map(_.bimap(Language(_), identity)))
  }

  def fromLanguagesMap[T](elements: Map[Language, T]): Multilingual[T] =
    new Multilingual[T](elements)

  def fromDefault[T](value: T): Multilingual[T] =
    apply(BaseLanguage -> value)

  import io.circe.syntax._
  import io.circe.{Decoder, Encoder}
  import io.circe.parser.decode
  import io.circe.refined._

  implicit def circeEncoder[T: Encoder]: Encoder[Multilingual[T]] =
    _.toMap.map(_.bimap(_.value, identity)).asJson

  implicit def circeDecoder[T: Decoder]: Decoder[Multilingual[T]] =
    Decoder.decodeMap[String, T].map(Multilingual.fromMap)

  import scalikejdbc.Binders

  def multilingualConverter[T: Decoder: Encoder]: Binders[Multilingual[T]] =
    Binders.string.xmap(decode[Multilingual[T]](_).getOrElse(Multilingual.empty), circeEncoder.apply(_).noSpaces)

  def optMultilingualConverter[T: Decoder: Encoder]: Binders[Option[Multilingual[T]]] =
    Binders.string.xmap(
      l => decode[Option[Map[String, T]]](l).getOrElse(None).map(Multilingual.fromMap),
      _.map(_.toMap.map(_.bimap(_.value, identity))).asJson.noSpaces
    )

  implicit val multilingualNonEmptyBinder: Binders[Multilingual[String Refined NonEmpty]] =
    multilingualConverter[String Refined NonEmpty]
  implicit val optMultilingualNonEmptyBinder: Binders[Option[Multilingual[String Refined NonEmpty]]] =
    optMultilingualConverter[String Refined NonEmpty]

  implicit val multilingualBinder: Binders[Multilingual[String]] = multilingualConverter[String]

  implicit val optMultilingualBinder: Binders[Option[Multilingual[String]]] = optMultilingualConverter[String]

  import eu.timepit.refined.api.Refined
  import eu.timepit.refined.auto._
  import eu.timepit.refined.collection.MaxSize
  import io.circe.refined._

  implicit val refined130StringMapConverter: Binders[Multilingual[String Refined MaxSize[130]]] =
    multilingualConverter[String Refined MaxSize[130]]
  implicit val refined130OptStringMapConverter: Binders[Option[Multilingual[String Refined MaxSize[130]]]] =
    optMultilingualConverter[String Refined MaxSize[130]]
  implicit val refined20StringMapConverter: Binders[Multilingual[String Refined MaxSize[20]]] =
    multilingualConverter[String Refined MaxSize[20]]
  implicit val refined20OptStringMapConverter: Binders[Option[Multilingual[String Refined MaxSize[20]]]] =
    optMultilingualConverter[String Refined MaxSize[20]]
  implicit val refined150StringMapConverter: Binders[Multilingual[String Refined MaxSize[150]]] =
    multilingualConverter[String Refined MaxSize[150]]
  implicit val refined150OptStringMapConverter: Binders[Option[Multilingual[String Refined MaxSize[150]]]] =
    optMultilingualConverter[String Refined MaxSize[150]]

  import spray.json._

  implicit def multilingualFormatter[T](
    implicit reader: JsonReader[T],
    writer: JsonWriter[T]
  ): RootJsonFormat[Multilingual[T]] =
    new RootJsonFormat[Multilingual[T]] {

      @SuppressWarnings(Array("org.wartremover.warts.Throw"))
      override def read(json: JsValue): Multilingual[T] =
        json match {
          case JsObject(m) => new Multilingual(m.map(_.bimap(Language(_), reader.read(_))))
          case other       => throw new IllegalArgumentException(s"Unable to convert $other")
        }

      override def write(obj: Multilingual[T]): JsValue =
        JsObject(obj.toMap.map(_.bimap(_.value, writer.write)))
    }

  import com.github.plokhotnyuk.jsoniter_scala.core.{
    JsonReader     => JsoniterReader,
    JsonWriter     => JsoniterWriter,
    JsonValueCodec => JsoniterValueCodec
  }
  import com.github.plokhotnyuk.jsoniter_scala.macros.{JsonCodecMaker => JsoniterCodecMaker}

  @SuppressWarnings(Array("org.wartremover.warts.Null", "org.wartremover.warts.AsInstanceOf"))
  def makeCodec[T: JsoniterValueCodec]: JsoniterValueCodec[Multilingual[T]] =
    new JsoniterValueCodec[Multilingual[T]] {

      private val mapCodec: JsoniterValueCodec[Map[String, T]] =
        JsoniterCodecMaker.make[Map[String, T]]

      def decodeValue(in: JsoniterReader, default: Multilingual[T]): Multilingual[T] =
        Multilingual.fromMap(mapCodec.decodeValue(in, null))

      def encodeValue(value: Multilingual[T], out: JsoniterWriter): Unit =
        mapCodec.encodeValue(value.toMap.map(_.bimap(_.value, identity)), out)

      def nullValue: Multilingual[T] =
        null.asInstanceOf[Multilingual[T]]
    }
}
