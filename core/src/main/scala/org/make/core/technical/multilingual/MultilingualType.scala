/*
 *  Make.org Core API
 *  Copyright (C) 2020 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.technical

import cats.data.{NonEmptyList, ValidatedNec}
import org.make.core.reference.Language
import org.make.core.ValidationError

trait MultilingualType {

  val defaultLanguage: Language
  val languages: NonEmptyList[Language]

  private val languagesSet = languages.toList.toSet

  assert(languagesSet.contains(defaultLanguage), "You can’t delete the default language")

  def validateMultilingualFields(
    fields: (Multilingual[_], String)*
  )(optFields: Option[(Multilingual[_], String)]*): ValidatedNec[ValidationError, Unit] =
    MultilingualUtils.hasRequiredTranslations(languagesSet, fields.concat(optFields.flatten).toList)
}
