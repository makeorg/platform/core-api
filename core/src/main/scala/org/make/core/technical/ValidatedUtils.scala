/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.technical

import scala.annotation.tailrec
import cats.Monad
import cats.data.ValidatedNec
import cats.implicits._
import org.make.core.{ValidationError, ValidationFailedError}
import cats.data.Validated._

object ValidatedUtils {

  implicit class ValidatedNecWithUtils[T](value: ValidatedNec[ValidationError, T]) {

    def toValidationEither: Either[ValidationFailedError, T] =
      value.leftMap(ValidationFailedError(_)).toEither

    @SuppressWarnings(Array("org.wartremover.warts.TryPartial"))
    def throwIfInvalid(): T =
      this.toValidationEither.toTry.get
  }

  type ValidatedPartial[A] = ValidatedNec[ValidationError, A]

  implicit def validatedMonad: Monad[ValidatedPartial] = new Monad[ValidatedPartial] {

    override def pure[A](x: A): ValidatedPartial[A] =
      x.validNec

    override def flatMap[A, B](fa: ValidatedPartial[A])(f: A => ValidatedPartial[B]): ValidatedPartial[B] =
      fa match {
        case Invalid(e) => Invalid(e)
        case Valid(a)   => f(a)
      }

    @tailrec
    override def tailRecM[A, B](a: A)(f: A => ValidatedPartial[Either[A, B]]): ValidatedPartial[B] =
      f(a) match {
        case e: Invalid[_]      => e
        case Valid(Left(nextA)) => tailRecM(nextA)(f)
        case Valid(Right(b))    => b.validNec
      }
  }
}
