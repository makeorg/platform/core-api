/*
 *  Make.org Core API
 *  Copyright (C) 2020 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.technical

import scala.util.Try
import eu.timepit.refined.refineV
import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._
import io.circe.Decoder

trait Pagination {

  val value: Int Refined NonNegative

  def extractInt: Int = value
}

object Pagination {

  final case class Offset(value: Int Refined NonNegative) extends Pagination

  private val RefinedZero: Int Refined NonNegative = 0

  private def nonNegativeOrZero(number: Int): Int Refined NonNegative =
    refineV[NonNegative](number).getOrElse(RefinedZero)

  object Offset {

    def zero: Offset = new Offset(RefinedZero)

    def apply(offset: Int): Offset =
      new Offset(nonNegativeOrZero(offset))
  }

  final case class End(value: Int Refined NonNegative) extends Pagination {

    def toLimit(offset: Offset): Limit =
      new Limit(nonNegativeOrZero(value - offset.value))
  }

  object End {

    def apply(end: Int): End =
      new End(nonNegativeOrZero(end))
  }

  final case class Limit(value: Int Refined NonNegative) extends Pagination {

    def toEnd(offset: Offset): End =
      End(value + offset.value)
  }

  object Limit {

    def apply(limit: Int): Limit =
      new Limit(nonNegativeOrZero(limit))
  }

  implicit class RichOptionOffset(val self: Option[Offset]) extends AnyVal {
    def orZero: Offset = self.getOrElse(Offset.zero)
  }

  private val nonNegativeDecoder: Decoder[Int Refined NonNegative] =
    Decoder.decodeString
      .emapTry(str => Try(str.toInt))
      .emap(refineV[NonNegative](_))

  implicit val offsetDecoder: Decoder[Offset] =
    nonNegativeDecoder.map(Offset(_))

  implicit val limitDecoder: Decoder[Limit] =
    nonNegativeDecoder.map(Limit(_))
}
