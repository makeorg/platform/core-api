/*
 *  Make.org Core API
 *  Copyright (C) 2020 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.technical

import cats.data.{NonEmptyList, Validated, ValidatedNec}
import cats.implicits._
import io.circe.{Decoder, DecodingFailure, HCursor}
import org.make.core.ValidationError
import org.make.core.reference.Language
import shapeless.{HList, LabelledGeneric}
import shapeless.ops.record.{Keys, Values}
import scala.annotation.tailrec

object MultilingualUtils {

  def hasRequiredTranslationsFromCsv(
    languagesSet: Set[Language],
    translations: Option[Multilingual[String]],
    originalLanguage: Option[Language],
    lineNumber: Int
  ): ValidatedNec[ValidationError, Option[Multilingual[String]]] = {
    val languages = originalLanguage.toList.toSet ++ translations.toList
      .toSet[Multilingual[String]]
      .flatMap(_.providedLanguages)
    languagesSet == languages match {
      case true => translations.validNec
      case false if languagesSet.sizeIs > languages.size =>
        val missing = languagesSet.diff(languages)
        ValidationError(
          "contents",
          "missing_translation",
          Some(s"There is a missing translations for ${missing.mkString(",")} on line $lineNumber")
        ).invalidNec
      case false if languagesSet.sizeIs < languages.size =>
        val tooMany = languages.diff(languagesSet)
        ValidationError(
          "contents",
          "too_many_translation",
          Some(s"There is too many translations : ${tooMany.mkString(",")} on line $lineNumber")
        ).invalidNec
      case _ =>
        val mismatch = languages.diff(languagesSet)
        ValidationError(
          "content",
          "mismatch_translation",
          Some(s"There is a translations mismatch : ${mismatch.mkString(",")} on line $lineNumber")
        ).invalidNec
    }
  }

  def hasRequiredTranslations(
    languagesSet: Set[Language],
    multilinguals: List[(Multilingual[_], String)]
  ): ValidatedNec[ValidationError, Unit] =
    multilinguals.traverse_({
      case (field, fieldName) =>
        languagesSet == field.providedLanguages match {
          case true => ().validNec
          case false if languagesSet.sizeIs > field.providedLanguages.size =>
            val missing = languagesSet.diff(field.providedLanguages)
            ValidationError(
              fieldName,
              "missing_translation",
              Some(s"Field $fieldName is missing translations for ${missing.mkString(",")}")
            ).invalidNec
          case false if languagesSet.sizeIs < field.providedLanguages.size =>
            val tooMany = field.providedLanguages.diff(languagesSet)
            ValidationError(
              fieldName,
              "too_many_translation",
              Some(s"Field $fieldName has too many translations : ${tooMany.mkString(",")}")
            ).invalidNec
          case _ =>
            val mismatch = field.providedLanguages.diff(languagesSet)
            ValidationError(
              fieldName,
              "mismatch_translation",
              Some(s"Field $fieldName has mismatch translations : ${mismatch.mkString(",")}")
            ).invalidNec
        }
    })

  def validateDecoder[A](decoder: Decoder[A])(
    langsF: A    => Set[Language],
    requiredF: A => Seq[(Multilingual[_], String)],
    optionalF: A => Seq[Option[(Multilingual[_], String)]]
  ): Decoder[A] =
    decoder.flatMap { t =>
      val langs = langsF(t)

      def findErrors(c: HCursor) =
        (requiredF(t).iterator ++ optionalF(t).iterator.flatten).flatMap {
          case (field, fieldName) =>
            langs.diff(field.providedLanguages) match {
              case missing if missing.nonEmpty =>
                val ac = c.downField(fieldName)
                Some(DecodingFailure(s"Missing translations: ${missing.mkString(", ")}", ac.history))

              case _ =>
                None
            }
        }

      new Decoder[A] {
        override def apply(c: HCursor): Decoder.Result[A] =
          findErrors(c).nextOption().toLeft(t)

        override def decodeAccumulating(c: HCursor): Decoder.AccumulatingResult[A] =
          findErrors(c).toList match {
            case hd :: tl =>
              Validated.Invalid(NonEmptyList(hd, tl))

            case Nil =>
              Validated.Valid(t)
          }
      }
    }

  /**
    * Enhances an instance of `Decoder[A]` by checking every fields of `A` which has a
    * type of `Multilingual[_]`, or `Option[Multilingual[_]]`. Every one of these fields
    * must contain a translation for every given languages passed as parameter.
    *
    * You have to pass as parameter `langsF` a function `A => Set[Language]` since it
    * generally depends upon values in `A` instead of a static set of languages.
    */
  def genDecoder[A, Repr <: HList](
    decoder: Decoder[A],
    langsF: A => Set[Language]
  )(implicit gen: LabelledGeneric.Aux[A, Repr], keys: Keys[Repr], values: Values[Repr]): Decoder[A] =
    decoder.flatMap(a => new GenDecoder(a, langsF(a)))

  private class GenDecoder[A, Repr <: HList](a: A, langs: Set[Language])(
    implicit gen: LabelledGeneric.Aux[A, Repr],
    keys: Keys[Repr],
    values: Values[Repr]
  ) extends Decoder[A] {

    override def apply(c: HCursor): Decoder.Result[A] = {

      /**
        * Actually validate a `Multilingual[_]` instance, which is actually stored
        * in some field `k` of `A`.
        */
      def doValidate(x: Multilingual[_], k: Symbol): Either[DecodingFailure, Unit] =
        langs.diff(x.providedLanguages) match {
          case missing if missing.nonEmpty =>
            Left(DecodingFailure(s"Missing translations: ${missing.mkString(", ")}", c.downField(k.name).history))
          case _ => Right(())
        }

      /**
        * Scan through every fields of `A` to find instances of :
        *   - `Multilingual[_]`,
        *   - `Option[Multilingual[_]]`.
        */
      @SuppressWarnings(Array("org.wartremover.warts.TraversableOps", "org.wartremover.warts.IterableOps"))
      @tailrec
      def recApply(ks: List[Any], xs: List[Any]): Decoder.Result[A] =
        ks.headOption.zip(xs.headOption) match {
          case None => Right(a)

          case Some((k: Symbol, x: Multilingual[_])) =>
            doValidate(x, k) match {
              case Left(err) =>
                Left(err)
              case _ =>
                recApply(ks.tail, xs.tail)
            }

          case Some((k: Symbol, Some(x: Multilingual[_]))) =>
            doValidate(x, k) match {
              case Left(err) =>
                Left(err)
              case _ =>
                recApply(ks.tail, xs.tail)
            }

          case _ => recApply(ks.tail, xs.tail)
        }

      // Fetch reflective information generated at compile-time by shapeless,
      // and start to validate `a`.
      recApply(keys().runtimeList, values(gen.to(a)).runtimeList)
    }

  }
}
