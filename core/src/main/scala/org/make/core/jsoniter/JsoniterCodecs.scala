/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.jsoniter

import java.time.ZonedDateTime
import cats.data.{NonEmptyList => Nel}
import com.github.plokhotnyuk.jsoniter_scala.macros._
import com.github.plokhotnyuk.jsoniter_scala.core._
import eu.timepit.refined.refineV
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.NonEmpty
import org.make.core.DateFormatters
import org.make.core.technical.Multilingual

@SuppressWarnings(Array("org.wartremover.warts.Null", "org.wartremover.warts.AsInstanceOf"))
trait JsoniterCodecs {

  implicit val stringCodec: JsonValueCodec[String] = JsonCodecMaker.makeWithRequiredCollectionFields
  implicit val multilingualCodec: JsonValueCodec[Multilingual[String]] = Multilingual.makeCodec

  implicit val refinedNonEmptyCodec: JsonValueCodec[String Refined NonEmpty] =
    new JsonValueCodec[String Refined NonEmpty] {

      def decodeValue(in: JsonReader, default: String Refined NonEmpty): String Refined NonEmpty =
        refineV[NonEmpty](in.readString(null)).getOrElse(default)

      def encodeValue(str: String Refined NonEmpty, out: JsonWriter): Unit =
        out.writeVal(str.value)

      def nullValue: String Refined NonEmpty = null.asInstanceOf[String Refined NonEmpty]
    }

  implicit val multilingualNECodec: JsonValueCodec[Multilingual[String Refined NonEmpty]] = Multilingual.makeCodec

  implicit def nonEmptyListCodec[T: JsonValueCodec]: JsonValueCodec[Nel[T]] =
    new JsonValueCodec[Nel[T]] {

      private val listCodec: JsonValueCodec[List[T]] =
        JsonCodecMaker.makeWithRequiredCollectionFields[List[T]]

      def decodeValue(in: JsonReader, default: Nel[T]): Nel[T] =
        Nel.fromListUnsafe(listCodec.decodeValue(in, null))

      def encodeValue(nel: Nel[T], out: JsonWriter): Unit =
        listCodec.encodeValue(nel.toList, out)

      def nullValue: Nel[T] = null.asInstanceOf[Nel[T]]
    }

  implicit val zonedDateTimeCodec: JsonValueCodec[ZonedDateTime] =
    new JsonValueCodec[ZonedDateTime] {

      def decodeValue(in: JsonReader, default: ZonedDateTime): ZonedDateTime =
        ZonedDateTime.from(DateFormatters.default.parse(in.readString(null)))

      def encodeValue(dateTime: ZonedDateTime, out: JsonWriter): Unit =
        out.writeVal(DateFormatters.default.format(dateTime))

      def nullValue: ZonedDateTime = null.asInstanceOf[ZonedDateTime]
    }
}
