/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.auth

import java.time.ZonedDateTime

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder
import io.swagger.annotations.ApiModelProperty
import org.make.core.CirceFormatters
import org.make.core.user.UserId

import scala.annotation.meta.field

final case class AuthCode(
  authorizationCode: String,
  scope: Option[String],
  redirectUri: Option[String],
  createdAt: ZonedDateTime,
  expiresIn: Int,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  user: UserId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  client: ClientId
)

object AuthCode extends CirceFormatters {
  implicit val encoder: Encoder[AuthCode] = deriveEncoder[AuthCode]
}
