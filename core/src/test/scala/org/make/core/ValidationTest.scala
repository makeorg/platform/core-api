/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core

import java.time.LocalDate
import cats.implicits._
import org.make.core.Validation._
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils

class ValidationTest extends BaseUnitTest {

  Feature("email validation") {

    def validation(emailValue: String): Unit = emailValue.toEmail.throwIfInvalid()

    Scenario("valid email") {

      val emailValue = "yopmail@make.org"
      noException should be thrownBy validation(emailValue)
    }

    Scenario("other valid email") {

      val emailValue = "contact@foo-bar.org"
      noException should be thrownBy validation(emailValue)
    }

    Scenario("mix of uppercase and lowercase") {

      val emailValue = "fOo@baR-bAz.eU"
      noException should be thrownBy validation(emailValue)
    }

    Scenario("invalid email") {

      val emailValue = "invalid"
      an[ValidationFailedError] should be thrownBy validation(emailValue)
    }

    Scenario("invalid email with space") {

      val emailValue = "invalid yopmail@make.org"
      an[ValidationFailedError] should be thrownBy validation(emailValue)
    }
  }

  Feature("user input validation") {

    def validation(inputValue: String): Unit =
      inputValue.toSanitizedInput("userInput").throwIfInvalid()

    Scenario("valid input") {

      val inputValue = "valid input"
      noException should be thrownBy validation(inputValue)
    }

    Scenario("invalid input") {

      val inputValue = "<a>invalid input</a>"
      a[ValidationFailedError] should be thrownBy validation(inputValue)
    }

    Scenario("valid input with < / >") {

      val inputValue = "< & >"
      noException should be thrownBy validation(inputValue)
    }
  }

  Feature("age validation") {
    val fieldName = "dateOfBirth"

    Scenario("valid datesOfBirth") {
      val aged13 = LocalDate.now.minusYears(13)
      def validation13(): Unit = Validation.validate(Validation.validateAge(fieldName, Some(aged13)))
      noException should be thrownBy validation13()

      val aged14 = LocalDate.now.minusYears(14)
      def validation14(): Unit = Validation.validate(Validation.validateAge(fieldName, Some(aged14)))
      noException should be thrownBy validation14()

      val aged119 = LocalDate.now.minusYears(119)
      def validation119(): Unit = Validation.validate(Validation.validateAge(fieldName, Some(aged119)))
      noException should be thrownBy validation119()

      def validationNone(): Unit = Validation.validate(Validation.validateAge(fieldName, None))
      noException should be thrownBy validationNone()
    }

    Scenario("invalid datesOfBirth") {
      val aged7 = LocalDate.now.minusYears(7)
      def validation7(): Unit = Validation.validate(Validation.validateAge(fieldName, Some(aged7)))
      an[ValidationFailedError] should be thrownBy validation7()

      val aged120 = LocalDate.now.minusYears(120)
      def validation120(): Unit = Validation.validate(Validation.validateAge(fieldName, Some(aged120)))
      an[ValidationFailedError] should be thrownBy validation120()

      val aged121 = LocalDate.now.minusYears(121)
      def validation121(): Unit = Validation.validate(Validation.validateAge(fieldName, Some(aged121)))
      an[ValidationFailedError] should be thrownBy validation121()
    }

  }

  Feature("legal consent validation") {
    val fieldName = "legal"

    Scenario("valid") {
      val aged14 = LocalDate.now.minusYears(14)
      def validation14(): Unit = Validation.validate(Validation.validateLegalConsent(fieldName, aged14, Some(true)))
      noException should be thrownBy validation14()

      val aged16 = LocalDate.now.minusYears(16)
      def validation16(): Unit = Validation.validate(Validation.validateLegalConsent(fieldName, aged16, None))
      noException should be thrownBy validation16()

      val aged120 = LocalDate.now.minusYears(120)
      def validation120(): Unit = Validation.validate(Validation.validateLegalConsent(fieldName, aged120, Some(true)))
      noException should be thrownBy validation120()

      val agedReallyOld = LocalDate.now.minusYears(10000)
      def validationReallyOld(): Unit =
        Validation.validate(Validation.validateLegalConsent(fieldName, agedReallyOld, None))
      noException should be thrownBy validationReallyOld()

      val aged8 = LocalDate.now.minusYears(8)
      def validation8(): Unit = Validation.validate(Validation.validateLegalConsent(fieldName, aged8, Some(true)))
      noException should be thrownBy validation8()
    }

    Scenario("invalid") {
      val aged7 = LocalDate.now.minusYears(7)
      def validation7(): Unit = Validation.validate(Validation.validateLegalConsent(fieldName, aged7, Some(true)))
      an[ValidationFailedError] should be thrownBy validation7()

      val aged8 = LocalDate.now.minusYears(8)
      def validation8WithConsent(): Unit =
        Validation.validate(Validation.validateLegalConsent(fieldName, aged8, Some(false)))
      an[ValidationFailedError] should be thrownBy validation8WithConsent()

      def validation8WithoutConsent(): Unit =
        Validation.validate(Validation.validateLegalConsent(fieldName, aged8, None))
      an[ValidationFailedError] should be thrownBy validation8WithoutConsent()

      val aged14 = LocalDate.now.minusYears(14)
      def validation14WithConsent(): Unit =
        Validation.validate(Validation.validateLegalConsent(fieldName, aged14, Some(false)))
      an[ValidationFailedError] should be thrownBy validation14WithConsent()

      def validation14WithoutConsent(): Unit =
        Validation.validate(Validation.validateLegalConsent(fieldName, aged14, None))
      an[ValidationFailedError] should be thrownBy validation14WithoutConsent()
    }

  }

  Feature("color validation") {
    val fieldName = "colorInput"
    Scenario("valid color") {
      "#424242".toColour(fieldName).isValid shouldBe true
    }

    Scenario("invalid color") {

      "red".toColour(fieldName) shouldBe ValidationError(fieldName, "invalid_colour").invalidNec
      "424242".toColour(fieldName) shouldBe ValidationError(fieldName, "invalid_colour").invalidNec
      "#42AHOD".toColour(fieldName) shouldBe ValidationError(fieldName, "invalid_colour").invalidNec
    }
  }

  Feature("HTML sanitization") {

    Scenario("With proper HTML") {

      val html = "<p>foo<br />bar, <strong>baz</strong></p>"
      SanitizedHtml.fromString(html).get.html shouldBe "<p>foo<br>bar, <strong>baz</strong></p>"
    }

    Scenario("With malicious HTML") {

      val html = """<p>foo<script source="evil.js"></p>"""
      SanitizedHtml.fromString(html).get.html shouldBe "<p>foo</p>"
    }

    Scenario("With text below the length limit") {

      val html = "<p>foo</p>"
      SanitizedHtml.fromString(html, Some(4)).get.html shouldBe "<p>foo</p>"
    }

    Scenario("With text above the length limit") {

      val html = "<p>foooo</p>"
      SanitizedHtml.fromString(html, Some(4)).isFailure shouldBe true
    }
  }

  Feature("Filename validation") {
    Scenario("Valid") {
      val name1 = "Name-42_21.pdf"
      noException should be thrownBy Validation.validate(validateUploadFileName(name1))

      val name2 = "JeSuisUnNomDeFichier"
      noException should be thrownBy Validation.validate(validateUploadFileName(name2))
    }

    Scenario("Invalid") {
      val name3 = "Name 42_21!.pdf"
      an[ValidationFailedError] should be thrownBy Validation.validate(validateUploadFileName(name3))

      val name4 = "Accent_é"
      an[ValidationFailedError] should be thrownBy Validation.validate(validateUploadFileName(name4))
    }
  }
}
