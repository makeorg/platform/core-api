/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.core.technical

import org.make.core.BaseUnitTest
import org.make.core.reference.Language
import org.scalatest.EitherValues._

class MultilingualTest extends BaseUnitTest {

  Feature("Multilingual construction and basic features") {

    Scenario("Construct from tuples") {

      val greetings = Multilingual((Language("fr"), "bonjour"), (Language("es"), "hola"))
      greetings.getTranslation(Language("es")) shouldBe Some("hola")
      greetings.getTranslationUnsafe(Language("fr")) shouldBe "bonjour"
      greetings.getTranslation(Language("pl")) shouldBe None

      greetings.providedLanguages shouldEqual Set(Language("es"), Language("fr"))
      greetings.isEmpty shouldBe false
      greetings.toMap shouldBe Map((Language("fr"), "bonjour"), (Language("es"), "hola"))
    }

    Scenario("Construct from map") {

      val greetingsMap = Map((Language("th"), "สวัสดีครับ"), (Language("hi"), "नमस्ते"))
      val greetings = Multilingual.fromLanguagesMap(greetingsMap)
      greetings.translations.toList shouldEqual greetingsMap.values.toList
    }

    Scenario("Add additional translation") {

      val greetingsMap = Map((Language("th"), "สวัสดีครับ"), (Language("hi"), "नमस्ते"))
      val greetings = Multilingual.fromLanguagesMap(greetingsMap)
      val augmentedGrettings = greetings.addTranslation(Language("fa"), "خب سلام")
      augmentedGrettings.translations should contain theSameElementsAs ("خب سلام" :: greetingsMap.values.toList)
    }
  }

  Feature("Multilingual mapping function") {

    val greetings = Multilingual((Language("my"), "ဟယ်လို"), (Language("ta"), "வணக்கம்"))

    Scenario("Map to translation length") {

      val mapped = greetings.mapTranslations(_.size)
      mapped.translations shouldBe List(6, 7)
    }
  }

  Feature("GenDecoder") {
    import io.circe.{Decoder, Json}
    import io.circe.generic.semiauto.deriveDecoder

    final case class SomeRequest(
      langs: Set[Language],
      field1: String,
      field2: Multilingual[Int],
      field3: Option[Multilingual[Boolean]]
    )

    val someRequestDecoder: Decoder[SomeRequest] = MultilingualUtils.genDecoder(deriveDecoder[SomeRequest], _.langs)
    val french = Language("fr")
    val english = Language("en")

    Scenario("on valid data (optional field omitted)") {
      val result = someRequestDecoder.decodeJson(
        Json.obj(
          "langs" -> Json.arr(Json.fromString(french.value), Json.fromString(english.value)),
          "field1" -> Json.fromString("foobar"),
          "field2" -> Json.obj(french.value -> Json.fromInt(1234), english.value -> Json.fromInt(4321))
        )
      )

      result should be(
        Right(
          SomeRequest(
            langs = Set(french, english),
            field1 = "foobar",
            field2 = Multilingual(Map(french -> 1234, english -> 4321)),
            field3 = None
          )
        )
      )
    }

    Scenario("on valid data (optional field specified)") {
      val result = someRequestDecoder.decodeJson(
        Json.obj(
          "langs" -> Json.arr(Json.fromString(french.value), Json.fromString(english.value)),
          "field1" -> Json.fromString("foobar"),
          "field2" -> Json.obj(french.value -> Json.fromInt(1234), english.value -> Json.fromInt(4321)),
          "field3" -> Json.obj(french.value -> Json.True, english.value -> Json.False)
        )
      )

      result should be(
        Right(
          SomeRequest(
            langs = Set(french, english),
            field1 = "foobar",
            field2 = Multilingual(Map(french -> 1234, english -> 4321)),
            field3 = Some(Multilingual(Map(french -> true, english -> false)))
          )
        )
      )
    }

    Scenario("on missing languages") {
      val result = someRequestDecoder.decodeJson(
        Json.obj(
          "langs" -> Json.arr(Json.fromString(french.value), Json.fromString(english.value)),
          "field1" -> Json.fromString("foobar"),
          "field2" -> Json.obj(french.value -> Json.fromInt(1234)),
          "field3" -> Json.obj(french.value -> Json.True)
        )
      )

      result.left.value.message should be("Missing translations: en")
    }
  }
}
