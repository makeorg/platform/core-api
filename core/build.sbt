/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

name := "make-core"

libraryDependencies ++= Seq(
  Dependencies.Runtime.akkaPersistenceQuery,
  Dependencies.Runtime.akkaHttpSwagger, // TODO: import only swagger not akka-http
  Dependencies.Runtime.apacheMath,
  Dependencies.Runtime.elastic4s,
  Dependencies.Runtime.elastic4sHttp,
  Dependencies.Runtime.enumeratum,
  Dependencies.Runtime.enumeratumCirce,
  Dependencies.Runtime.enumeratumScalacheck,
  Dependencies.Runtime.avro4s,
  Dependencies.Runtime.circeGeneric,
  Dependencies.Runtime.circeParser,
  Dependencies.Runtime.circeGenericExtra,
  Dependencies.Runtime.refinedCirce,
  Dependencies.Runtime.refinedScala,
  Dependencies.Runtime.stamina,
  Dependencies.Runtime.slugify,
  Dependencies.Runtime.jakartaMail,
  Dependencies.Runtime.jsoup,
  Dependencies.Runtime.refinedScalaCheck,
  Dependencies.Runtime.scalaCheck,
  Dependencies.Runtime.jsoniter,
  Dependencies.Runtime.jsoniterMacros,
  Dependencies.Runtime.scalike
)
