/*
 *  Make.org Core API
 *  Copyright (C) 2020 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.technical.webflow
import io.circe.parser.decode
import org.make.api.MakeUnitTest

class DeserTest extends MakeUnitTest {

  Feature("webflow item") {
    val sampleItem =
      """{
        |  "isArchived": false,
        |  "isDraft": false,
        |  "fieldData": {
        |   "name": "5 Principles Of Effective Web Design",
        |   "post-body": "<h2>Earum laboriosam ab velit.</h2>",
        |   "post-summary": "Quo eligendi nihil quia voluptas qui.\nNon distinctio voluptatu",
        |   "thumbnail-image": {
        |     "fileId": "580e63ff8c9a982ac9b8b74d",
        |     "url": "https://d1otoma47x30pg.cloudfront.net/580e63fc8c9a982ac9b8b744/580e63ff8c9a982ac9b8b74d_1477338111010-image3.jpg"
        |   },
        |   "main-image": {
        |     "fileId": "580e63fe8c9a982ac9b8b749",
        |     "url": "https://d1otoma47x30pg.cloudfront.net/580e63fc8c9a982ac9b8b744/580e63fe8c9a982ac9b8b749_1477338110257-image20.jpg"
        |   },
        |   "post-date": "2016-10-24T19:41:52.325Z",
        |   "afficher-cet-article-sur-le-site-de-make-org": true,
        |   "slug": "5-principles-of-effective-web-design",
        |   "author": "580e640c8c9a982ac9b8b77a"
        |  },
        |  "createdOn": "2016-10-24T19:41:52.325Z",
        |  "lastUpdated": "2016-10-24T19:42:46.957Z",
        |  "id": "580e64008c9a982ac9b8b754"
        |}""".stripMargin
    Scenario("simple item") {
      val decoded = decode[WebflowPost](sampleItem)
      decoded.isRight shouldBe true
      decoded.map(_.fieldData.slug).toOption should contain("5-principles-of-effective-web-design")
      decoded.map(_.fieldData.displayHome).toOption should contain(Some(true))
    }
  }

  Feature("webflow items") {
    val sampleWebflowItems =
      """{
          |  "items": [
          |  {
          |    "isArchived": false,
          |    "isDraft": false,
          |    "fieldData": {
          |     "name": "5 Principles Of Effective Web Design",
          |     "post-body": "<h2>Earum laboriosam ab velit.</h2>",
          |     "post-summary": "Quo eligendi nihil quia voluptas qui.\nNon distinctio voluptatu",
          |     "thumbnail-image": {
          |       "fileId": "580e63ff8c9a982ac9b8b74d",
          |       "url": "https://d1otoma47x30pg.cloudfront.net/580e63fc8c9a982ac9b8b744/580e63ff8c9a982ac9b8b74d_1477338111010-image3.jpg"
          |     },
          |     "main-image": {
          |       "fileId": "580e63fe8c9a982ac9b8b749",
          |       "url": "https://d1otoma47x30pg.cloudfront.net/580e63fc8c9a982ac9b8b744/580e63fe8c9a982ac9b8b749_1477338110257-image20.jpg"
          |     },
          |     "post-date": "2016-10-24T19:41:52.325Z",
          |     "afficher-cet-article-sur-le-site-de-make-org": true,
          |     "slug": "5-principles-of-effective-web-design",
          |     "author": "580e640c8c9a982ac9b8b77a"
          |    },
          |    "createdOn": "2016-10-24T19:41:52.325Z",
          |    "lastUpdated": "2016-10-24T19:42:46.957Z",
          |    "id": "580e64008c9a982ac9b8b754"
          |  }],
          |  "pagination": {
          |   "limit": 1,
          |   "offset": 0,
          |   "total": 5
          |  }
          |}
  """.stripMargin
    Scenario("simple item") {

      val decoded = decode[WebflowItems](sampleWebflowItems)
      decoded.isRight shouldBe true
      decoded.map(_.items.size).toOption should contain(1)
      decoded.map(_.pagination.total).toOption should contain(5)
      decoded.map(_.items.head.fieldData.slug).toOption should contain("5-principles-of-effective-web-design")
    }
  }
}
