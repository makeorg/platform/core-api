/*
 *  Make.org Core API
 *  Copyright (C) 2021 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.idea

import com.sksamuel.elastic4s.requests.searches.suggestion.Fuzziness
import org.make.core.{Order, RequestContext}
import org.make.core.technical.Pagination
import org.make.core.idea.{IdeaSearchFilters, IdeaSearchQuery, NameSearchFilter, QuestionIdSearchFilter}
import org.make.core.question.QuestionId

final case class IdeaFiltersRequest(
  name: Option[String],
  questionId: Option[QuestionId],
  limit: Option[Pagination.Limit],
  offset: Option[Pagination.Offset],
  sort: Option[String],
  order: Option[Order]
) {

  private lazy val filters: Option[IdeaSearchFilters] = {
    val nameFilter = name.map(NameSearchFilter(_, Some(Fuzziness.Auto)))
    val questionIdFilter = questionId.map(QuestionIdSearchFilter.apply)
    IdeaSearchFilters.parse(nameFilter, questionIdFilter)
  }

  def toSearchQuery(requestContext: RequestContext): IdeaSearchQuery = {
    IdeaSearchQuery(
      filters = filters,
      limit = limit,
      offset = offset,
      sort = sort,
      order = order,
      language = requestContext.languageContext.language
    )
  }
}

object IdeaFiltersRequest {
  val empty: IdeaFiltersRequest = IdeaFiltersRequest(None, None, None, None, None, None)
}
