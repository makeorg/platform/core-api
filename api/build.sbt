/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import java.time.{ZoneOffset, ZonedDateTime}

import com.typesafe.sbt.SbtGit.GitKeys

name := "make-api"

libraryDependencies ++= Seq(
  Dependencies.Runtime.commonsLoggingBridge,
  Dependencies.Runtime.log4jJul,
  Dependencies.Runtime.kamonCore,
  Dependencies.Runtime.kamonAkka,
  Dependencies.Runtime.kamonAkkaHttp,
  Dependencies.Runtime.kamonScalaFutures,
  Dependencies.Runtime.kamonPrometheus,
  Dependencies.Runtime.kamonSystemMetrics,
  Dependencies.Runtime.kanela,
  Dependencies.Runtime.akkaSlf4j,
  Dependencies.Runtime.akkaPersistenceQuery,
  Dependencies.Runtime.akkaHttp,
  Dependencies.Runtime.akkaHttpSwagger,
  Dependencies.Runtime.akkaSerializationJackson,
  Dependencies.Runtime.alpakkaCsv,
  Dependencies.Runtime.caliban,
  Dependencies.Runtime.calibanAkkaHttp,
  Dependencies.Runtime.cassandraQueryBuilder,
  Dependencies.Runtime.zio,
  Dependencies.Runtime.zioStreams,
  Dependencies.Runtime.circeGeneric,
  Dependencies.Runtime.swaggerUi,
  Dependencies.Runtime.elastic4s,
  Dependencies.Runtime.elastic4sHttp,
  Dependencies.Runtime.jaxRsApi,
  Dependencies.Tests.akkaHttpTest,
  Dependencies.Tests.akkaStreamTest,
  Dependencies.Tests.jerseyServer,
  Dependencies.Tests.jerseyHk2
)

// Solving this Akka error, introduced by conflicting versions
// of jackson-databind between Akka and Flyway:
// requires Jackson Databind version >= 2.13.0 and < 2.14.0 - Found jackson-databind version 2.14.0
dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.13.5"

lazy val swaggerUiVersion: SettingKey[String] =
  SettingKey[String]("swaggerUiVersion", "version of swagger ui").withRank(KeyRanks.Invisible)

swaggerUiVersion := {
  Dependencies.Versions.swaggerUi
}

lazy val buildTime: SettingKey[String] = SettingKey[String]("buildTime", "time of build").withRank(KeyRanks.Invisible)

buildTime := ZonedDateTime.now(ZoneOffset.UTC).toString

buildInfoKeys :=
  Seq[BuildInfoKey](
    name,
    version,
    scalaVersion,
    sbtVersion,
    GitKeys.gitHeadCommit,
    GitKeys.gitCurrentBranch,
    buildTime,
    swaggerUiVersion
  )

run / fork := true

val kamonInstrumentationWorkaround = Seq("--add-opens", "java.base/java.util.concurrent=ALL-UNNAMED")

javaOptions in run ++= Seq(
  "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=127.0.0.1:5005",
  "-Djava.util.logging.manager=org.apache.logging.log4j.jul.LogManager"
) ++ kamonInstrumentationWorkaround ++ SbtKanelaRunner.jvmForkOptions.value

javaOptions in Test            ++= kamonInstrumentationWorkaround
javaOptions in IntegrationTest ++= kamonInstrumentationWorkaround

enablePlugins(BuildInfoPlugin)
