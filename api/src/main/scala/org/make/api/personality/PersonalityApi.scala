/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.personality

import cats.implicits._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.string.Url
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import io.circe.refined._
import io.swagger.annotations._

import javax.ws.rs.Path
import org.make.api.idea.TopIdeaServiceComponent
import org.make.api.idea.topIdeaComments.TopIdeaCommentServiceComponent
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.MakeAuthenticationDirectives
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.user.{UserResponse, UserServiceComponent}
import org.make.core.Validation._
import org.make.core._
import org.make.core.idea._
import org.make.core.profile.Profile
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, Language}
import org.make.core.user.{User, UserId}
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils

import scala.annotation.meta.field
import org.make.core.technical.Pagination

@Api(value = "Personalities")
@Path(value = "/personalities")
trait PersonalityApi extends Directives {

  @ApiOperation(value = "get-personality", httpMethod = "GET", code = HttpCodes.OK)
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[UserResponse])))
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "userId",
        paramType = "path",
        dataType = "string",
        example = "11111111-2222-3333-4444-555555555555"
      )
    )
  )
  @Path(value = "/{userId}")
  def getPersonality: Route

  @ApiOperation(value = "get-personality", httpMethod = "GET", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[PersonalityProfileResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "userId",
        paramType = "path",
        dataType = "string",
        example = "11111111-2222-3333-4444-555555555555"
      )
    )
  )
  @Path(value = "/{userId}/profile")
  def getPersonalityProfile: Route

  @ApiOperation(
    value = "update-personality-profile",
    httpMethod = "PUT",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "user", description = "application user"))
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[PersonalityProfileResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.personality.PersonalityProfileRequest"
      )
    )
  )
  @Path(value = "/{userId}/profile")
  def modifyPersonalityProfile: Route

  @Path("/{userId}/comments")
  @ApiOperation(value = "create-top-idea-comments-for-personality", httpMethod = "POST", code = HttpCodes.Created)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.personality.CreateTopIdeaCommentRequest"
      )
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.Created, message = "Created", response = classOf[TopIdeaCommentResponse]))
  )
  def createComment: Route

  @ApiOperation(value = "get-personality-opinions", httpMethod = "GET", code = HttpCodes.OK)
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[PersonalityOpinionResponse]]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "userId",
        paramType = "path",
        dataType = "string",
        example = "11111111-2222-3333-4444-555555555555"
      ),
      new ApiImplicitParam(name = "questionId", paramType = "query", dataType = "string")
    )
  )
  @Path(value = "/{userId}/opinions")
  def getPersonalityOpinions: Route

  def routes: Route =
    getPersonality ~ getPersonalityProfile ~ modifyPersonalityProfile ~ createComment ~ getPersonalityOpinions

}

trait PersonalityApiComponent {
  def personalityApi: PersonalityApi
}

trait DefaultPersonalityApiComponent
    extends PersonalityApiComponent
    with MakeAuthenticationDirectives
    with ParameterExtractors {
  this: MakeDirectivesDependencies
    with UserServiceComponent
    with TopIdeaServiceComponent
    with TopIdeaCommentServiceComponent
    with QuestionPersonalityServiceComponent =>

  override lazy val personalityApi: PersonalityApi = new DefaultPersonalityApi

  class DefaultPersonalityApi extends PersonalityApi {

    val userId: PathMatcher1[UserId] = Segment.map(id => UserId(id))

    override def getPersonality: Route =
      get {
        path("personalities" / userId) { userId =>
          makeOperation("GetPersonality") { _ =>
            userService.getPersonality(userId).asDirectiveOrNotFound { user =>
              complete(UserResponse(user))
            }
          }
        }
      }

    override def getPersonalityProfile: Route = {
      get {
        path("personalities" / userId / "profile") { userId =>
          makeOperation("GetPersonalityProfile") { _ =>
            userService.getPersonality(userId).asDirectiveOrNotFound { user =>
              complete(PersonalityProfileResponse.fromUser(user))
            }
          }
        }
      }
    }

    override def modifyPersonalityProfile: Route = {
      put {
        path("personalities" / userId / "profile") { personalityId =>
          makeOperation("UpdatePersonalityProfile") { requestContext =>
            decodeRequest {
              entity(as[PersonalityProfileRequest]) { request =>
                makeOAuth2 { currentUser =>
                  authorize(currentUser.user.userId == personalityId) {
                    userService.getPersonality(personalityId).asDirectiveOrNotFound { personality =>
                      val modifiedProfile = personality.profile
                        .orElse(Profile.parseProfile())
                        .map(
                          _.copy(
                            avatarUrl = request.avatarUrl.map(_.value),
                            description = request.description,
                            optInNewsletter = request.optInNewsletter,
                            website = request.website.map(_.value),
                            politicalParty = request.politicalParty,
                            crmCountry = request.crmCountry.getOrElse(Country("FR")),
                            crmLanguage = request.crmLanguage.getOrElse(Language("fr"))
                          )
                        )

                      val modifiedPersonality = personality.copy(
                        firstName = Some(request.firstName),
                        lastName = Some(request.lastName),
                        profile = modifiedProfile
                      )

                      userService.update(modifiedPersonality, requestContext).asDirective { result =>
                        complete(PersonalityProfileResponse.fromUser(result))
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def createComment: Route = post {
      path("personalities" / userId / "comments") { userId =>
        makeOperation("CreateTopIdeaComment") { _ =>
          makeOAuth2 { userAuth =>
            authorize(userId == userAuth.user.userId) {
              userService.getPersonality(userId).asDirectiveOrNotFound { _ =>
                decodeRequest {
                  entity(as[CreateTopIdeaCommentRequest]) { request =>
                    topIdeaService.getById(request.topIdeaId).asDirective { maybeTopIdea =>
                      Validation.validate(
                        Validation.validateField(
                          "topIdeaId",
                          "invalid_content",
                          maybeTopIdea.isDefined,
                          s"Top idea ${request.topIdeaId} does not exists."
                        )
                      )
                      topIdeaCommentService
                        .create(
                          topIdeaId = request.topIdeaId,
                          personalityId = userId,
                          comment1 = request.comment1,
                          comment2 = request.comment2,
                          comment3 = request.comment3,
                          vote = request.vote,
                          qualification = request.qualification
                        )
                        .asDirective { comment =>
                          complete(StatusCodes.Created -> TopIdeaCommentResponse(comment))
                        }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def getPersonalityOpinions: Route = get {
      path("personalities" / userId / "opinions") { userId =>
        makeOperation("GetPersonalityOpinions") { _ =>
          parameters("questionId".as[QuestionId].?) { maybeQuestionId: Option[QuestionId] =>
            userService.getPersonality(userId).asDirectiveOrNotFound { _ =>
              questionPersonalityService
                .find(
                  Pagination.Offset.zero,
                  None,
                  None,
                  None,
                  userId = Some(userId),
                  questionId = maybeQuestionId,
                  None
                )
                .asDirective {
                  case Seq() => complete(Seq.empty[PersonalityOpinionResponse])
                  case personalities =>
                    questionPersonalityService
                      .getPersonalitiesOpinionsByQuestions(personalities)
                      .asDirective
                      .apply(complete(_))
                }
            }
          }
        }
      }
    }
  }
}

final case class PersonalityProfileResponse(
  firstName: Option[String],
  lastName: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png")
  avatarUrl: Option[String],
  description: Option[String],
  @(ApiModelProperty @field)(dataType = "boolean") optInNewsletter: Option[Boolean],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String],
  politicalParty: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "FR") crmCountry: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr") crmLanguage: Option[Language]
)

object PersonalityProfileResponse {
  implicit val encoder: Encoder[PersonalityProfileResponse] = deriveEncoder[PersonalityProfileResponse]
  implicit val decoder: Decoder[PersonalityProfileResponse] = deriveDecoder[PersonalityProfileResponse]

  def fromUser(user: User): PersonalityProfileResponse = {
    PersonalityProfileResponse(
      firstName = user.firstName,
      lastName = user.lastName,
      avatarUrl = user.profile.flatMap(_.avatarUrl),
      description = user.profile.flatMap(_.description),
      website = user.profile.flatMap(_.website),
      politicalParty = user.profile.flatMap(_.politicalParty),
      optInNewsletter = user.profile.map(_.optInNewsletter),
      crmCountry = user.profile.map(_.crmCountry),
      crmLanguage = user.profile.map(_.crmLanguage)
    )

  }
}

final case class PersonalityProfileRequest(
  firstName: String,
  lastName: String,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/logo.jpg")
  avatarUrl: Option[String Refined Url],
  description: Option[String],
  optInNewsletter: Boolean,
  @(ApiModelProperty @field)(dataType = "string", example = "https://make.org")
  website: Option[String Refined Url],
  politicalParty: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "FR") crmCountry: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr") crmLanguage: Option[Language]
) {
  private val maxDescriptionLength = 450

  Seq(
    description.map(_.withMaxLength(maxDescriptionLength, "description").void),
    description.map(_.toSanitizedInput("description").void),
    politicalParty.map(_.toSanitizedInput("politicalParty").void)
  ).flatten.foreach(_.throwIfInvalid())

  (
    firstName.toNonEmpty("firstName"),
    firstName.toSanitizedInput("firstName"),
    lastName.toNonEmpty("lastName"),
    lastName.toSanitizedInput("lastName")
  ).tupled.throwIfInvalid()
}

object PersonalityProfileRequest {
  implicit val encoder: Encoder[PersonalityProfileRequest] = deriveEncoder[PersonalityProfileRequest]
  implicit val decoder: Decoder[PersonalityProfileRequest] = deriveDecoder[PersonalityProfileRequest]
}

@ApiModel
final case class CreateTopIdeaCommentRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  topIdeaId: TopIdeaId,
  comment1: Option[String],
  comment2: Option[String],
  comment3: Option[String],
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = CommentVoteKey.swaggerAllowableValues,
    required = true
  )
  vote: CommentVoteKey,
  @(ApiModelProperty @field)(
    dataType = "string",
    example = "doable",
    allowableValues = CommentQualificationKey.swaggerAllowableValues,
    required = true
  )
  qualification: Option[CommentQualificationKey]
) {

  Seq(
    comment1.map(_.withMinLength(3, "comment1").void),
    comment2.map(_.withMinLength(3, "comment2").void),
    comment3.map(_.withMinLength(3, "comment3").void)
  ).flatten.foreach(_.throwIfInvalid())

  qualification
    .filter(_.commentVoteKey == vote)
    .getValidated(
      "qualification",
      Some("wrong_qualifiaction_key"),
      Some("The qualification does no correspond to the vote")
    )
    .throwIfInvalid()
}

object CreateTopIdeaCommentRequest {
  implicit val decoder: Decoder[CreateTopIdeaCommentRequest] = deriveDecoder[CreateTopIdeaCommentRequest]
  implicit val encoder: Encoder[CreateTopIdeaCommentRequest] = deriveEncoder[CreateTopIdeaCommentRequest]
}
