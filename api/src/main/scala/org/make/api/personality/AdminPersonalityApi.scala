/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.personality

import cats.implicits._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.string.Url
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import io.circe.refined._
import io.swagger.annotations._

import javax.ws.rs.Path
import org.make.api.technical.CsvReceptacle._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.user.DefaultPersistentUserServiceComponent.PersistentUser
import org.make.api.user.{PersonalityRegisterData, UserServiceComponent}
import org.make.core._
import org.make.core.Validation._
import org.make.core.technical.Pagination
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils
import org.make.core.Validation.StringWithParsers
import org.make.core.auth.UserRights
import org.make.core.profile.{Gender, Profile}
import org.make.core.reference.{Country, Language}
import org.make.core.user.{User, UserId, UserType}
import scalaoauth2.provider.AuthInfo

import scala.annotation.meta.field

@Api(
  value = "Admin Personalities",
  authorizations = Array(
    new Authorization(
      value = "MakeApi",
      scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
    )
  )
)
@Path(value = "/admin/personalities")
trait AdminPersonalityApi extends Directives {

  @ApiOperation(value = "get-personality", httpMethod = "GET", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[PersonalityResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "userId",
        paramType = "path",
        dataType = "string",
        example = "11111111-2222-3333-4444-555555555555"
      )
    )
  )
  @Path(value = "/{userId}")
  def getPersonality: Route

  @ApiOperation(value = "get-personalities", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = PersistentUser.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "id", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(name = "email", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "firstName", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "lastName", paramType = "query", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[PersonalityResponse]]))
  )
  @Path(value = "/")
  def getPersonalities: Route

  @ApiOperation(value = "create-personality", httpMethod = "POST", code = HttpCodes.Created)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.personality.CreatePersonalityRequest"
      )
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.Created, message = "Created", response = classOf[PersonalityResponse]))
  )
  @Path(value = "/")
  def createPersonality: Route

  @ApiOperation(value = "update-personality", httpMethod = "PUT", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.personality.UpdatePersonalityRequest"
      ),
      new ApiImplicitParam(
        name = "userId",
        paramType = "path",
        dataType = "string",
        example = "11111111-2222-3333-4444-555555555555"
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[PersonalityResponse]))
  )
  @Path(value = "/{userId}")
  def updatePersonality: Route

  def routes: Route = getPersonality ~ getPersonalities ~ createPersonality ~ updatePersonality

}

trait AdminPersonalityApiComponent {
  def adminPersonalityApi: AdminPersonalityApi
}

trait DefaultAdminPersonalityApiComponent
    extends AdminPersonalityApiComponent
    with MakeAuthenticationDirectives
    with ParameterExtractors {
  this: MakeDirectivesDependencies with UserServiceComponent =>

  override lazy val adminPersonalityApi: AdminPersonalityApi = new DefaultAdminPersonalityApi

  class DefaultAdminPersonalityApi extends AdminPersonalityApi {

    val userId: PathMatcher1[UserId] = Segment.map(UserId.apply)

    override def getPersonality: Route = get {
      path("admin" / "personalities" / userId) { userId =>
        makeOperation("GetPersonality") { _ =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireAdminRole(auth.user) {
              userService.getUser(userId).asDirectiveOrNotFound { user =>
                complete(PersonalityResponse(user))
              }
            }
          }
        }
      }
    }

    override def getPersonalities: Route = get {
      path("admin" / "personalities") {
        makeOperation("GetPersonalities") { _ =>
          parameters(
            "_start".as[Pagination.Offset].?,
            "_end".as[Pagination.End].?,
            "_sort".?,
            "_order".as[Order].?,
            "id".csv[UserId],
            "email".?,
            "firstName".?,
            "lastName".?
          ) {
            (
              offset: Option[Pagination.Offset],
              end: Option[Pagination.End],
              sort: Option[String],
              order: Option[Order],
              ids: Option[Seq[UserId]],
              email: Option[String],
              firstName: Option[String],
              lastName: Option[String]
            ) =>
              makeOAuth2 { auth: AuthInfo[UserRights] =>
                requireAdminRole(auth.user) {
                  (
                    userService
                      .adminCountUsers(
                        ids = ids,
                        email = email,
                        firstName = firstName,
                        lastName = lastName,
                        role = None,
                        userType = Some(UserType.UserTypePersonality)
                      )
                      .asDirective,
                    userService
                      .adminFindUsers(
                        offset.orZero,
                        end,
                        sort,
                        order,
                        ids = ids,
                        email = email,
                        firstName = firstName,
                        lastName = lastName,
                        role = None,
                        userType = Some(UserType.UserTypePersonality)
                      )
                      .asDirective
                  ).tupled.apply({
                    case (count, users) =>
                      complete(
                        (StatusCodes.OK, List(`X-Total-Count`(count.toString)), users.map(PersonalityResponse.apply))
                      )
                  })
                }
              }
          }
        }
      }
    }

    override def createPersonality: Route = post {
      path("admin" / "personalities") {
        makeOperation("CreatePersonality") { requestContext =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireAdminRole(auth.user) {
              decodeRequest {
                entity(as[CreatePersonalityRequest]) { request: CreatePersonalityRequest =>
                  userService
                    .registerPersonality(
                      PersonalityRegisterData(
                        email = request.email,
                        firstName = request.firstName,
                        lastName = request.lastName,
                        country = request.country,
                        language = request.language,
                        gender = request.gender,
                        genderName = request.genderName,
                        description = request.description,
                        avatarUrl = request.avatarUrl,
                        website = request.website.map(_.value),
                        politicalParty = request.politicalParty
                      ),
                      requestContext
                    )
                    .asDirective { result =>
                      complete(StatusCodes.Created -> PersonalityResponse(result))
                    }
                }
              }
            }
          }
        }
      }
    }

    override def updatePersonality: Route =
      put {
        path("admin" / "personalities" / userId) { userId =>
          makeOperation("UpdatePersonality") { requestContext =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              requireAdminRole(userAuth.user) {
                decodeRequest {
                  entity(as[UpdatePersonalityRequest]) { request: UpdatePersonalityRequest =>
                    userService.getUser(userId).asDirectiveOrNotFound { personality =>
                      val lowerCasedEmail: String = request.email.getOrElse(personality.email).toLowerCase()
                      userService.getUserByEmail(lowerCasedEmail).asDirective { maybeUser =>
                        maybeUser.foreach { userToCheck =>
                          Validation.validate(
                            Validation.validateField(
                              field = "email",
                              "already_registered",
                              condition = userToCheck.userId.value == personality.userId.value,
                              message = s"Email $lowerCasedEmail already exists"
                            )
                          )
                        }
                        onSuccess(
                          userService.updatePersonality(
                            personality = personality.copy(
                              email = lowerCasedEmail,
                              firstName = request.firstName.orElse(personality.firstName),
                              lastName = request.lastName.orElse(personality.lastName),
                              country = request.country.getOrElse(personality.country),
                              profile = personality.profile
                                .map(
                                  _.copy(
                                    avatarUrl = request.avatarUrl.orElse(personality.profile.flatMap(_.avatarUrl)),
                                    description = request.description.orElse(personality.profile.flatMap(_.description)),
                                    gender = request.gender.orElse(personality.profile.flatMap(_.gender)),
                                    genderName = request.genderName.orElse(personality.profile.flatMap(_.genderName)),
                                    politicalParty =
                                      request.politicalParty.orElse(personality.profile.flatMap(_.politicalParty)),
                                    website =
                                      request.website.map(_.value).orElse(personality.profile.flatMap(_.website))
                                  )
                                )
                                .orElse(
                                  Profile.parseProfile(
                                    avatarUrl = request.avatarUrl,
                                    description = request.description,
                                    gender = request.gender,
                                    genderName = request.genderName,
                                    politicalParty = request.politicalParty,
                                    website = request.website.map(_.value)
                                  )
                                )
                            ),
                            moderatorId = Some(userAuth.user.userId),
                            oldEmail = personality.email.toLowerCase,
                            requestContext
                          )
                        ) { user: User =>
                          complete(StatusCodes.OK -> PersonalityResponse(user))
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
  }

}

final case class CreatePersonalityRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true) email: String,
  firstName: Option[String],
  lastName: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "FR", required = true) country: Country,
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true) language: Language,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png") avatarUrl: Option[String],
  description: Option[String],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = Gender.swaggerAllowableValues) gender: Option[
    Gender
  ],
  genderName: Option[String],
  politicalParty: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String Refined Url]
) {

  (firstName.getValidated("firstName"), email.toEmail).tupled.throwIfInvalid()

  Seq(firstName.map(_.toSanitizedInput("firstName")), lastName.map(_.toSanitizedInput("lastName"))).flatten
    .foreach(_.throwIfInvalid())
}

object CreatePersonalityRequest {
  implicit val decoder: Decoder[CreatePersonalityRequest] = deriveDecoder[CreatePersonalityRequest]
}

final case class UpdatePersonalityRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org") email: Option[String],
  firstName: Option[String],
  lastName: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "FR") country: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png") avatarUrl: Option[String],
  description: Option[String],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = Gender.swaggerAllowableValues) gender: Option[
    Gender
  ],
  genderName: Option[String],
  politicalParty: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String Refined Url]
) {

  private val maxCountryLength = 3

  Seq(
    email.map(_.toEmail.void),
    email.map(_.toSanitizedInput("email").void),
    firstName.map(_.toNonEmpty("firstName", Some("firstName should not be an empty string")).void),
    firstName.map(_.toSanitizedInput("firstName").void),
    lastName.map(_.toSanitizedInput("lastName").void),
    country.map(_.value.withMaxLength(maxCountryLength, "country").void)
  ).flatten.foreach(_.throwIfInvalid())
}

object UpdatePersonalityRequest {
  implicit val decoder: Decoder[UpdatePersonalityRequest] = deriveDecoder[UpdatePersonalityRequest]
}

final case class PersonalityResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true) id: UserId,
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true) email: String Refined ValidHtml,
  firstName: Option[Name],
  lastName: Option[Name],
  @(ApiModelProperty @field)(dataType = "string", example = "FR", required = true) country: Country,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png") avatarUrl: Option[String],
  description: Option[String],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = Gender.swaggerAllowableValues) gender: Option[
    Gender
  ],
  genderName: Option[String],
  politicalParty: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String]
)

object PersonalityResponse extends CirceFormatters {
  implicit val encoder: Encoder[PersonalityResponse] = deriveEncoder[PersonalityResponse]
  implicit val decoder: Decoder[PersonalityResponse] = deriveDecoder[PersonalityResponse]

  def apply(user: User): PersonalityResponse = PersonalityResponse(
    id = user.userId,
    email = Refined.unsafeApply(user.email), // this is ok since data has already been validated before
    firstName = user.firstName.map(Refined.unsafeApply), // this is ok since data has already been validated before
    lastName = user.lastName.map(Refined.unsafeApply), // this is ok since data has already been validated before
    country = user.country,
    avatarUrl = user.profile.flatMap(_.avatarUrl),
    description = user.profile.flatMap(_.description),
    gender = user.profile.flatMap(_.gender),
    genderName = user.profile.flatMap(_.genderName),
    politicalParty = user.profile.flatMap(_.politicalParty),
    website = user.profile.flatMap(_.website)
  )
}
