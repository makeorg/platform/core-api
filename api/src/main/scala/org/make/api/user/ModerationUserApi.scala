/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.user

import cats.implicits._
import akka.http.scaladsl.server._
import grizzled.slf4j.Logging
import io.swagger.annotations._
import scalaoauth2.provider.AuthInfo
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.MakeAuthenticationDirectives
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.question.QuestionServiceComponent
import org.make.api.proposal.MaxPropositionsThresholdsComponent
import org.make.core._
import org.make.core.auth.UserRights
import org.make.core.question.QuestionId
import org.make.core.user._

import javax.ws.rs.Path

@Api(value = "Moderator Users")
@Path(value = "/moderation/users")
trait ModerationUserApi extends Directives {

  @ApiOperation(
    value = "proposals-count",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "moderation", description = "BO Moderation"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "OK", response = classOf[NumberOfProposalsResponse]))
  )
  @Path(value = "/{userId}/{questionId}/proposals-count")
  def getNumberOfProposals: Route

  def routes: Route = getNumberOfProposals
}

trait ModerationUserApiComponent {

  def moderationUserApi: ModerationUserApi
}

trait DefaultModerationUserApiComponent
    extends ModerationUserApiComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {

  this: MakeDirectivesDependencies
    with UserServiceComponent
    with QuestionServiceComponent
    with UserProposalsServiceComponent
    with MaxPropositionsThresholdsComponent =>

  override lazy val moderationUserApi: ModerationUserApi = new DefaultModerationUserApi

  class DefaultModerationUserApi extends ModerationUserApi {

    private val userId = Segment.map(UserId.apply)
    private val questionId = Segment.map(QuestionId.apply)

    private val thresholds =
      MaxPropositionsThresholdsResponse(
        maxPropositionsThresholds.warnThreshold,
        maxPropositionsThresholds.blockThreshold
      )

    override def getNumberOfProposals: Route =
      get {
        path("moderation" / "users" / userId / questionId / "proposals-count") {
          case (userId, questionId) =>
            makeOperation("GetProposalsCounter") { _ =>
              makeOAuth2 { auth: AuthInfo[UserRights] =>
                requireModerationRole(auth.user) {
                  userService.getUser(userId).asDirectiveOrNotFound { _ =>
                    questionService.getQuestion(questionId).asDirectiveOrNotFound { _ =>
                      userProposalsService.getCounter(userId, questionId).asDirective { count =>
                        complete(NumberOfProposalsResponse(count, thresholds))
                      }
                    }
                  }
                }
              }
            }
        }
      }
  }
}

final case class NumberOfProposalsResponse(count: Int, thresholds: MaxPropositionsThresholdsResponse)

object NumberOfProposalsResponse {

  implicit val codec: Codec[NumberOfProposalsResponse] = deriveCodec
}

final case class MaxPropositionsThresholdsResponse(warn: Int, block: Int)

object MaxPropositionsThresholdsResponse {

  implicit val codec: Codec[MaxPropositionsThresholdsResponse] = deriveCodec
}
