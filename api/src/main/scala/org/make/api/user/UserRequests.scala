/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.user

import enumeratum.values.{StringCirceEnum, StringEnum, StringEnumEntry}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.boolean.{And, Or}
import eu.timepit.refined.collection.{Empty, MaxSize, NonEmpty}
import eu.timepit.refined.string.Url
import io.circe.generic.semiauto.{deriveCodec, deriveDecoder}
import io.circe.{Codec, Decoder, Encoder}
import io.circe.refined._
import io.swagger.annotations.ApiModelProperty
import org.make.api.technical.RequestHelper
import org.make.api.user.validation.UserProfileRequestValidation
import org.make.core.CirceFormatters
import org.make.core.Validation._
import org.make.core.profile.{Gender, Profile, SocioProfessionalCategory}
import org.make.core.proposal.ProposalId
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, Language}

import scala.annotation.meta.field

final case class ProfileRequest(
  @(ApiModelProperty @field)(dataType = "date", example = "1970-01-01") dateOfBirth: Option[BirthDate],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png")
  avatarUrl: Option[String Refined (Url And MaxSize[2048] And ValidHtml)],
  profession: Option[String Refined ValidHtml],
  phoneNumber: Option[String Refined ValidHtml],
  @(ApiModelProperty @field)(dataType = "string")
  description: Option[String Refined (MaxSize[450] And ValidHtml)],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = Gender.swaggerAllowableValues) gender: Option[
    Gender
  ],
  genderName: Option[String Refined ValidHtml],
  @(ApiModelProperty @field)(dataType = "string", example = "12345")
  postalCode: Option[String Refined ValidHtml],
  locale: Option[String Refined ValidHtml],
  optInNewsletter: Boolean = true,
  @(ApiModelProperty @field)(dataType = "string", allowableValues = SocioProfessionalCategory.swaggerAllowableValues)
  socioProfessionalCategory: Option[SocioProfessionalCategory] = None,
  @(ApiModelProperty @field)(dataType = "boolean") optInPartner: Option[Boolean] = None,
  politicalParty: Option[String Refined ValidHtml],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String Refined (Url And ValidHtml)]
) {

  def mergeProfile(maybeProfile: Option[Profile]): Option[Profile] = maybeProfile match {
    case None => toProfile
    case Some(profile) =>
      Some(
        profile.copy(
          dateOfBirth = dateOfBirth.map(_.birthDate).orElse(profile.dateOfBirth),
          avatarUrl = RequestHelper.updateValue(profile.avatarUrl, avatarUrl.map(_.value)),
          profession = RequestHelper.updateValue(profile.profession, profession.map(_.value)),
          phoneNumber = RequestHelper.updateValue(profile.phoneNumber, phoneNumber.map(_.value)),
          description = RequestHelper.updateValue(profile.description, description.map(_.value)),
          gender = gender.orElse(profile.gender),
          genderName = RequestHelper.updateValue(profile.genderName, genderName.map(_.value)),
          postalCode = RequestHelper.updateValue(profile.postalCode, postalCode.map(_.value)),
          locale = RequestHelper.updateValue(profile.locale, locale.map(_.value)),
          optInNewsletter = optInNewsletter,
          socioProfessionalCategory = socioProfessionalCategory.orElse(profile.socioProfessionalCategory),
          optInPartner = optInPartner.orElse(profile.optInPartner),
          politicalParty = RequestHelper.updateValue(profile.politicalParty, politicalParty.map(_.value)),
          website = RequestHelper.updateValue(profile.website, website.map(_.value))
        )
      )
  }

  def toProfile: Option[Profile] = Profile.parseProfile(
    dateOfBirth = dateOfBirth.map(_.birthDate),
    avatarUrl = avatarUrl.map(_.value),
    profession = profession.map(_.value),
    phoneNumber = phoneNumber.map(_.value),
    description = description.map(_.value),
    gender = gender,
    genderName = genderName.map(_.value),
    postalCode = postalCode.map(_.value),
    locale = locale.map(_.value),
    optInNewsletter = optInNewsletter,
    socioProfessionalCategory = socioProfessionalCategory,
    optInPartner = optInPartner,
    politicalParty = politicalParty.map(_.value),
    website = website.map(_.value)
  )
}

object ProfileRequest extends CirceFormatters {
  implicit val codec: Codec[ProfileRequest] = deriveCodec[ProfileRequest]

  def validateProfileRequest(profileRequest: ProfileRequest): Unit = ()
}

final case class RegisterUserRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true)
  email: Email,
  @(ApiModelProperty @field)(dataType = "string", example = "p4ssw0rd", required = true)
  password: Password,
  @(ApiModelProperty @field)(dataType = "date", example = "1970-01-01") dateOfBirth: BirthDate,
  firstName: Option[Name],
  lastName: Option[Name],
  profession: Option[String Refined ValidHtml],
  @(ApiModelProperty @field)(dataType = "string", example = "12345")
  postalCode: Option[PostalCode],
  @(ApiModelProperty @field)(dataType = "string", example = "FR") country: Country,
  @(ApiModelProperty @field)(dataType = "string", example = "fr") language: Option[Language],
  @(ApiModelProperty @field)(dataType = "string", example = "FR") crmCountry: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr") crmLanguage: Option[Language],
  @(ApiModelProperty @field)(dataType = "boolean") optIn: Option[Boolean],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = Gender.swaggerAllowableValues) gender: Option[
    Gender
  ],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = SocioProfessionalCategory.swaggerAllowableValues)
  socioProfessionalCategory: Option[SocioProfessionalCategory],
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  questionId: Option[QuestionId],
  @(ApiModelProperty @field)(dataType = "boolean") optInPartner: Option[Boolean],
  politicalParty: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String Refined Url],
  @(ApiModelProperty @field)(dataType = "boolean") legalMinorConsent: Option[Boolean],
  @(ApiModelProperty @field)(dataType = "boolean") legalAdvisorApproval: Option[Boolean],
  @(ApiModelProperty @field)(dataType = "boolean") approvePrivacyPolicy: Option[Boolean]
) extends UserProfileRequestValidation {
  validate(
    validateField(
      "approvePrivacyPolicy",
      "invalid_value",
      approvePrivacyPolicy.exists(identity),
      "Privacy policy must be approved."
    )
  )
}

object RegisterUserRequest extends CirceFormatters {
  @SuppressWarnings(Array("org.wartremover.warts.Option2Iterable"))
  implicit val decoder: Decoder[RegisterUserRequest] = deriveDecoder[RegisterUserRequest]
}

final case class UserProfileRequest(
  firstName: Name,
  lastName: Option[Name],
  dateOfBirth: BirthDate,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/logo.jpg")
  avatarUrl: Option[String Refined Url],
  profession: Option[String Refined ValidHtml],
  description: Option[String Refined (ValidHtml And MaxSize[450])],
  @(ApiModelProperty @field)(dataType = "string", example = "12345") postalCode: Option[PostalCode],
  optInNewsletter: Boolean,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String Refined Url],
  @(ApiModelProperty @field)(dataType = "boolean") legalMinorConsent: Option[Boolean],
  @(ApiModelProperty @field)(dataType = "boolean") legalAdvisorApproval: Option[Boolean],
  @(ApiModelProperty @field)(dataType = "string", example = "FR") crmCountry: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr") crmLanguage: Option[Language]
) extends UserProfileRequestValidation

object UserProfileRequest extends CirceFormatters {
  implicit val codec: Codec[UserProfileRequest] = deriveCodec[UserProfileRequest]
}

final case class UpdateUserRequest(
  @(ApiModelProperty @field)(dataType = "date", example = "1970-01-01") dateOfBirth: Option[BirthDate],
  firstName: Option[Name],
  lastName: Option[String],
  organisationName: Option[Name],
  profession: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "12345") postalCode: Option[PostalCode],
  phoneNumber: Option[String Refined ValidHtml], // TODO: use google libphonenumber?
  description: Option[String Refined (ValidHtml And MaxSize[450])],
  @(ApiModelProperty @field)(dataType = "boolean") optInNewsletter: Option[Boolean],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = Gender.swaggerAllowableValues) gender: Option[
    Gender
  ],
  genderName: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "FR") crmCountry: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr") crmLanguage: Option[Language],
  socioProfessionalCategory: Option[SocioProfessionalCategory],
  politicalParty: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String Refined Or[Url, Empty]]
)

object UpdateUserRequest extends CirceFormatters {
  implicit val decoder: Decoder[UpdateUserRequest] = deriveDecoder[UpdateUserRequest]
}

sealed abstract class SocialProvider(val value: String) extends StringEnumEntry

object SocialProvider extends StringEnum[SocialProvider] with StringCirceEnum[SocialProvider] {
  case object GooglePeople extends SocialProvider("google_people")
  case object Facebook extends SocialProvider("facebook")

  override val values: IndexedSeq[SocialProvider] = findValues
  final val swaggerAllowableValues = "google_people,facebook"

  implicit val decoder: Decoder[SocialProvider] =
    Decoder.decodeString.emap { t =>
      valuesToEntriesMap.get(t).toRight(s"invalid social provider: $t")
    }
  implicit val encoder: Encoder[SocialProvider] =
    Encoder.encodeString.contramap(_.value)
}

final case class SocialLoginRequest(
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = SocialProvider.swaggerAllowableValues,
    required = true
  )
  provider: SocialProvider,
  token: String,
  @(ApiModelProperty @field)(dataType = "string", example = "FR") country: Country,
  @(ApiModelProperty @field)(dataType = "string", example = "fr") language: Option[Language],
  @(ApiModelProperty @field)(dataType = "string", example = "FR") crmCountry: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr") crmLanguage: Option[Language],
  @(ApiModelProperty @field)(dataType = "boolean", example = "true") approvePrivacyPolicy: Option[Boolean],
  @(ApiModelProperty @field)(dataType = "boolean", example = "true") optIn: Option[Boolean]
) {
  validate(
    validateField(
      "approvePrivacyPolicy",
      key = "invalid_value",
      condition = approvePrivacyPolicy.forall(identity),
      message = "Privacy policy must be approved."
    )
  )
}

object SocialLoginRequest {
  implicit val decoder: Decoder[SocialLoginRequest] = deriveDecoder[SocialLoginRequest]
}

final case class ResetPasswordRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true)
  email: Email
)

object ResetPasswordRequest {
  implicit val decoder: Decoder[ResetPasswordRequest] = deriveDecoder[ResetPasswordRequest]
}

final case class ResetPassword(resetToken: String Refined NonEmpty, password: Password)

object ResetPassword {
  implicit val decoder: Decoder[ResetPassword] = deriveDecoder[ResetPassword]
}

final case class ChangePasswordRequest(actualPassword: Option[String], newPassword: Password)

object ChangePasswordRequest {
  implicit val decoder: Decoder[ChangePasswordRequest] = deriveDecoder[ChangePasswordRequest]
}

final case class DeleteUserRequest(password: Option[String])

object DeleteUserRequest {
  implicit val decoder: Decoder[DeleteUserRequest] = deriveDecoder[DeleteUserRequest]
}

final case class SubscribeToNewsLetter(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true)
  email: Email
)

object SubscribeToNewsLetter {
  implicit val decoder: Decoder[SubscribeToNewsLetter] = deriveDecoder[SubscribeToNewsLetter]
}

final case class ResendValidationEmailRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true)
  email: String
)

object ResendValidationEmailRequest {
  implicit val decoder: Decoder[ResendValidationEmailRequest] = deriveDecoder[ResendValidationEmailRequest]
}

final case class SendProposerEmailRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  proposalId: ProposalId,
  @(ApiModelProperty @field)(dataType = "string", example = "hyd bud?", required = true)
  text: String,
  @(ApiModelProperty @field)(dataType = "boolean", example = "false", required = true)
  dryRun: Boolean
)

object SendProposerEmailRequest {
  implicit val decoder: Codec[SendProposerEmailRequest] = deriveCodec
}

final case class PrivacyPolicyRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true)
  email: String,
  password: String
)

object PrivacyPolicyRequest {
  implicit val codec: Codec[PrivacyPolicyRequest] = deriveCodec
}

final case class SocialPrivacyPolicyRequest(
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = SocialProvider.swaggerAllowableValues,
    required = true
  )
  provider: SocialProvider,
  token: String
)

object SocialPrivacyPolicyRequest {
  implicit val decoder: Decoder[SocialPrivacyPolicyRequest] = deriveDecoder[SocialPrivacyPolicyRequest]
}

final case class SocialInfosRequest(
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = SocialProvider.swaggerAllowableValues,
    required = true
  )
  provider: SocialProvider,
  token: String
)

object SocialInfosRequest {
  implicit val decoder: Decoder[SocialInfosRequest] = deriveDecoder[SocialInfosRequest]
}

final case class CheckRegistrationRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true)
  email: Email,
  password: Password
)

object CheckRegistrationRequest {
  implicit val codec: Codec[CheckRegistrationRequest] = deriveCodec
}
