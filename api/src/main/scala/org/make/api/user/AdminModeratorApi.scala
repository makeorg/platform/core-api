/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.user

import cats.implicits._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import grizzled.slf4j.Logging
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import io.swagger.annotations._
import org.make.api.technical.CsvReceptacle._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.user.DefaultPersistentUserServiceComponent.PersistentUser
import org.make.core.Validation._
import org.make.core._
import org.make.core.auth.UserRights
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, Language}
import org.make.core.technical.Pagination
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils
import org.make.core.user.Role.{RoleAdmin, RoleModerator, RoleSuperAdmin}
import org.make.core.user._
import scalaoauth2.provider.AuthInfo

import javax.ws.rs.Path
import scala.annotation.meta.field

@Api(value = "Admin Moderators")
@Path(value = "/admin/moderators")
trait AdminModeratorApi extends Directives {

  @ApiOperation(
    value = "get-moderator",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModeratorResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "moderatorId",
        paramType = "path",
        dataType = "string",
        example = "11111111-2222-3333-4444-555555555555"
      )
    )
  )
  @Path(value = "/{moderatorId}")
  def getModerator: Route

  @ApiOperation(
    value = "get-moderators",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = PersistentUser.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "id", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(name = "email", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "firstName", paramType = "query", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[ModeratorResponse]]))
  )
  @Path(value = "/")
  def getModerators: Route

  @ApiOperation(
    value = "create-moderator",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.CreateModeratorRequest")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModeratorResponse]))
  )
  @Path(value = "/")
  def createModerator: Route

  @ApiOperation(
    value = "update-moderator",
    httpMethod = "PUT",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "moderatorId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.UpdateModeratorRequest")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModeratorResponse]))
  )
  @Path(value = "/{moderatorId}")
  def updateModerator: Route

  def routes: Route =
    getModerator ~ getModerators ~ createModerator ~ updateModerator
}

trait AdminModeratorApiComponent {
  def adminModeratorApi: AdminModeratorApi
}

trait DefaultAdminModeratorApiComponent
    extends AdminModeratorApiComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {

  this: MakeDirectivesDependencies with UserServiceComponent with PersistentUserServiceComponent =>

  override lazy val adminModeratorApi: AdminModeratorApi = new DefaultAdminModeratorApi

  class DefaultAdminModeratorApi extends AdminModeratorApi {

    val moderatorId: PathMatcher1[UserId] = Segment.map(UserId.apply)

    private def isModerator(user: User): Boolean = {
      Set(RoleModerator, RoleAdmin, RoleSuperAdmin).intersect(user.roles.toSet).nonEmpty
    }

    override def getModerator: Route = get {
      path("admin" / "moderators" / moderatorId) { moderatorId =>
        makeOperation("GetModerator") { _ =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireAdminRole(auth.user) {
              userService.getUser(moderatorId).asDirectiveOrNotFound { moderator =>
                if (!isModerator(moderator)) {
                  complete(StatusCodes.NotFound)
                } else {
                  complete(ModeratorResponse(moderator))
                }
              }
            }
          }
        }
      }
    }

    override def getModerators: Route = get {
      path("admin" / "moderators") {
        makeOperation("GetModerators") { _ =>
          parameters(
            "_start".as[Pagination.Offset].?,
            "_end".as[Pagination.End].?,
            "_sort".?,
            "_order".as[Order].?,
            "id".csv[UserId],
            "email".?,
            "firstName".?
          ) {
            (
              offset: Option[Pagination.Offset],
              end: Option[Pagination.End],
              sort: Option[String],
              order: Option[Order],
              ids: Option[Seq[UserId]],
              email: Option[String],
              firstName: Option[String]
            ) =>
              makeOAuth2 { auth: AuthInfo[UserRights] =>
                requireAdminRole(auth.user) {
                  (
                    userService
                      .adminCountUsers(
                        ids = ids,
                        email = email,
                        firstName = firstName,
                        lastName = None,
                        role = Some(Role.RoleModerator),
                        userType = None
                      )
                      .asDirective,
                    userService
                      .adminFindUsers(
                        offset.orZero,
                        end,
                        sort,
                        order,
                        ids = ids,
                        email = email,
                        firstName = firstName,
                        lastName = None,
                        role = Some(Role.RoleModerator),
                        userType = None
                      )
                      .asDirective
                  ).tupled.apply({
                    case (count, users) =>
                      complete(
                        (StatusCodes.OK, List(`X-Total-Count`(count.toString)), users.map(ModeratorResponse.apply))
                      )
                  })
                }
              }
          }
        }
      }
    }

    override def createModerator: Route = post {
      path("admin" / "moderators") {
        makeOperation("CreateModerator") { requestContext =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireAdminRole(auth.user) {
              decodeRequest {
                entity(as[CreateModeratorRequest]) { request: CreateModeratorRequest =>
                  userService
                    .register(
                      UserRegisterData(
                        email = request.email,
                        firstName = request.firstName,
                        lastName = request.lastName,
                        password = request.password,
                        lastIp = None,
                        country = request.country,
                        language = request.language,
                        crmCountry = request.country,
                        crmLanguage = request.language,
                        optIn = Some(false),
                        optInPartner = Some(false),
                        roles = request.roles
                          .map(_.map(Role.apply))
                          .getOrElse(Seq(Role.RoleModerator, Role.RoleCitizen)),
                        availableQuestions = request.availableQuestions,
                        privacyPolicyApprovalDate = Some(DateHelper.now())
                      ),
                      requestContext
                    )
                    .asDirective
                    .apply { result =>
                      complete(StatusCodes.Created -> ModeratorResponse(result))
                    }
                }
              }
            }
          }
        }
      }
    }

    override def updateModerator: Route =
      put {
        path("admin" / "moderators" / moderatorId) { moderatorId =>
          makeOperation("UpdateModerator") { requestContext =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              val isAdmin = Set(RoleAdmin, RoleSuperAdmin).intersect(userAuth.user.roles.toSet).nonEmpty
              val isModerator = userAuth.user.roles.contains(RoleModerator)
              authorize((isModerator && moderatorId == userAuth.user.userId) || isAdmin) {
                decodeRequest {
                  entity(as[UpdateModeratorRequest]) { request: UpdateModeratorRequest =>
                    userService.getUser(moderatorId).asDirectiveOrNotFound { user =>
                      val roles =
                        request.roles.map(_.map(Role.apply)).getOrElse(user.roles)
                      authorize {
                        roles != user.roles && isAdmin || roles == user.roles
                      } {
                        val lowerCasedEmail: String = request.email.getOrElse(user.email).toLowerCase()
                        userService.getUserByEmail(lowerCasedEmail).asDirective { maybeUser =>
                          maybeUser.foreach { userToCheck =>
                            Validation.validate(
                              Validation.validateField(
                                field = "email",
                                "already_registered",
                                condition = userToCheck.userId.value == user.userId.value,
                                message = s"Email $lowerCasedEmail already exists"
                              )
                            )
                          }

                          userService
                            .update(
                              user.copy(
                                email = lowerCasedEmail,
                                firstName = request.firstName.orElse(user.firstName),
                                lastName = request.lastName.orElse(user.lastName),
                                country = request.country.getOrElse(user.country),
                                roles = roles,
                                availableQuestions = request.availableQuestions
                              ),
                              requestContext
                            )
                            .asDirective { user: User =>
                              complete(StatusCodes.OK -> ModeratorResponse(user))
                            }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
  }
}

final case class CreateModeratorRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true) email: String,
  firstName: Option[String],
  lastName: Option[String],
  @(ApiModelProperty @field)(dataType = "list[string]", allowableValues = Role.swaggerAllowableValues) roles: Option[
    Seq[String]
  ],
  password: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "FR", required = true) country: Country,
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true) language: Language,
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  availableQuestions: Seq[QuestionId]
) {

  email.toEmail.throwIfInvalid()

  Validation.validate(
    mandatoryField("firstName", firstName),
    validateOptionalUserInput("firstName", firstName, None),
    validateOptionalUserInput("lastName", lastName, None),
    mandatoryField("country", country),
    validateField(
      "password",
      "invalid_password",
      password.forall(_.length >= 8),
      "Password must be at least 8 characters"
    )
  )
}

object CreateModeratorRequest {
  implicit val decoder: Decoder[CreateModeratorRequest] = deriveDecoder[CreateModeratorRequest]
}

final case class UpdateModeratorRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org") email: Option[String],
  firstName: Option[String],
  lastName: Option[String],
  @(ApiModelProperty @field)(dataType = "list[string]") roles: Option[Seq[String]],
  @(ApiModelProperty @field)(dataType = "string", example = "FR") country: Option[Country],
  @(ApiModelProperty @field)(
    dataType = "list[string]",
    example = "11111111-2222-3333-4444-555555555555",
    required = true
  )
  availableQuestions: Seq[QuestionId]
) {
  private val maxCountryLength = 3

  Seq(
    email.map(_.toSanitizedInput("email").void),
    email.map(_.toEmail.void),
    firstName.map(_.toNonEmpty("firstName").void),
    firstName.map(_.toSanitizedInput("firstName").void),
    lastName.map(_.toSanitizedInput("lastName").void),
    country.map(_.value.withMaxLength(maxCountryLength, "country").void)
  ).flatten.foreach(_.throwIfInvalid())
}

object UpdateModeratorRequest {
  implicit val decoder: Decoder[UpdateModeratorRequest] = deriveDecoder[UpdateModeratorRequest]

  def updateValue(oldValue: Option[String], newValue: Option[String]): Option[String] = {
    newValue match {
      case None     => oldValue
      case Some("") => None
      case value    => value
    }
  }
}

final case class ModeratorResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true) id: UserId,
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true) email: String,
  firstName: Option[String],
  lastName: Option[String],
  @(ApiModelProperty @field)(dataType = "list[string]", required = true, allowableValues = Role.swaggerAllowableValues) roles: Seq[
    Role
  ],
  @(ApiModelProperty @field)(dataType = "string", example = "FR", required = true) country: Country,
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  availableQuestions: Seq[QuestionId]
) {
  Validation.validate(
    validateUserInput("email", email, None),
    validateOptionalUserInput("firstName", firstName, None),
    validateOptionalUserInput("lastName", lastName, None)
  )
}

object ModeratorResponse extends CirceFormatters {
  implicit val encoder: Encoder[ModeratorResponse] = deriveEncoder[ModeratorResponse]
  implicit val decoder: Decoder[ModeratorResponse] = deriveDecoder[ModeratorResponse]

  def apply(user: User): ModeratorResponse = ModeratorResponse(
    id = user.userId,
    email = user.email,
    firstName = user.firstName,
    lastName = user.lastName,
    roles = user.roles,
    country = user.country,
    availableQuestions = user.availableQuestions
  )
}
