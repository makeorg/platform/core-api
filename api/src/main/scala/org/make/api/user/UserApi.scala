/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.user

import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import grizzled.slf4j.Logging
import io.swagger.annotations._
import org.make.api.proposal.{ProposalServiceComponent, ProposalsResultResponse, ProposalsResultSeededResponse}
import org.make.api.question.QuestionServiceComponent
import org.make.api.technical.CsvReceptacle._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical._
import org.make.api.technical.directives.ClientDirectives
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.technical.storage._
import org.make.api.user.UserExceptions.EmailAlreadyRegisteredException
import org.make.api.user.social.SocialServiceComponent
import org.make.api.user.validation.UserRegistrationValidatorComponent
import org.make.api.userhistory._
import org.make.core._
import org.make.core.auth.UserRights
import org.make.core.common.indexed.Sort
import org.make.core.profile.Profile
import org.make.core.proposal._
import org.make.core.proposal.indexed.ProposalElasticsearchFieldName
import org.make.core.question.Question
import org.make.core.reference.{Country, Language}
import org.make.core.technical.Pagination
import org.make.core.user.Role.RoleAdmin
import org.make.core.user._
import scalaoauth2.provider.AuthInfo

import java.time.ZonedDateTime
import javax.ws.rs.Path
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Api(value = "User")
@Path(value = "/user")
trait UserApi extends Directives {

  @Path(value = "/{userId}/profile")
  @ApiOperation(
    value = "get-user-profile",
    httpMethod = "GET",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "user", description = "application user"),
          new AuthorizationScope(scope = "admin", description = "BO Admin")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[UserProfileResponse]))
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string")))
  def getUserProfile: Route

  @Path(value = "/me")
  @ApiOperation(
    value = "get-me",
    httpMethod = "GET",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "user", description = "application user"),
          new AuthorizationScope(scope = "admin", description = "BO Admin")
        )
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[UserResponse])))
  def getMe: Route

  @Path(value = "/current")
  @ApiOperation(
    value = "current-user",
    httpMethod = "GET",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "user", description = "application user"),
          new AuthorizationScope(scope = "admin", description = "BO Admin")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[CurrentUserResponse]))
  )
  def currentUser: Route

  @Path(value = "/login/social")
  @ApiOperation(value = "social-login", httpMethod = "POST", code = HttpCodes.OK)
  @ApiImplicitParams(
    value =
      Array(new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.SocialLoginRequest"))
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[SocialLoginResponse]))
  )
  def socialLogin: Route

  @Path(value = "/{userId}/votes")
  @ApiOperation(value = "voted-proposals", httpMethod = "GET")
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        name = "votes",
        paramType = "query",
        dataType = "string",
        allowableValues = VoteKey.swaggerAllowableValues,
        allowMultiple = true
      ),
      new ApiImplicitParam(
        name = "qualifications",
        paramType = "query",
        dataType = "string",
        allowableValues = QualificationKey.swaggerAllowableValues,
        allowMultiple = true
      ),
      new ApiImplicitParam(name = "preferredLanguage", paramType = "query", dataType = "string", example = "fr"),
      new ApiImplicitParam(
        name = "sort",
        paramType = "query",
        dataType = "string",
        allowableValues = ProposalElasticsearchFieldName.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(name = "skip", paramType = "query", dataType = "int", allowableValues = "range[0, infinity]")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ProposalsResultResponse]))
  )
  def getVotedProposalsByUser: Route

  @ApiOperation(value = "register-user", httpMethod = "POST", code = HttpCodes.Created)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.RegisterUserRequest")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.Created, message = "Created", response = classOf[UserResponse]))
  )
  def register: Route

  @ApiOperation(value = "verifiy-user-email", httpMethod = "POST", code = HttpCodes.NoContent)
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No content")))
  @Path(value = "/{userId}/validate/:verificationToken")
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "verificationToken", paramType = "path", dataType = "string")
    )
  )
  def validateAccountRoute: Route

  @ApiOperation(value = "reset-password-request-token", httpMethod = "POST", code = HttpCodes.NoContent)
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No content")))
  @Path(value = "/reset-password/request-reset")
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "body", paramType = "body", dataType = "org.make.api.user.ResetPasswordRequest")
    )
  )
  def resetPasswordRequestRoute: Route

  @ApiOperation(value = "reset-password-token-check", httpMethod = "POST", code = HttpCodes.NoContent)
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No content")))
  @Path(value = "/reset-password/check-validity/:userId/:resetToken")
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "resetToken", paramType = "path", dataType = "string")
    )
  )
  def resetPasswordCheckRoute: Route

  @ApiOperation(value = "reset-password", httpMethod = "POST", code = HttpCodes.NoContent)
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No content")))
  @Path(value = "/reset-password/change-password/:userId")
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "body", paramType = "body", dataType = "org.make.api.user.ResetPassword")
    )
  )
  def resetPasswordRoute: Route

  @Path(value = "/{userId}/proposals")
  @ApiOperation(
    value = "user-proposals",
    httpMethod = "GET",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "user", description = "application user"),
          new AuthorizationScope(scope = "admin", description = "BO Admin")
        )
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "preferredLanguage", paramType = "query", dataType = "string", example = "fr"),
      new ApiImplicitParam(
        name = "sort",
        paramType = "query",
        dataType = "string",
        allowableValues = ProposalElasticsearchFieldName.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(name = "skip", paramType = "query", dataType = "int", allowableValues = "range[0, infinity]")
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ProposalsResultSeededResponse]))
  )
  def getProposalsByUser: Route

  @ApiOperation(
    value = "user-update",
    httpMethod = "PATCH",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "user", description = "application user"))
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[UserResponse])))
  @ApiImplicitParams(
    value =
      Array(new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.UpdateUserRequest"))
  )
  @Path(value = "/")
  def patchCurrentUser: Route

  @ApiOperation(
    value = "change-password",
    httpMethod = "POST",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "user", description = "application user"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.ChangePasswordRequest")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok")))
  @Path(value = "{userId}/change-password")
  def changePassword: Route

  @ApiOperation(
    value = "delete-user",
    httpMethod = "POST",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "user", description = "application user"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.DeleteUserRequest")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok")))
  @Path(value = "{userId}/delete")
  def deleteUser: Route

  @ApiOperation(
    value = "follow-user",
    httpMethod = "POST",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "user", description = "application user"))
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok")))
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string")))
  @Path(value = "/{userId}/follow")
  def followUser: Route

  @ApiOperation(
    value = "unfollow-user",
    httpMethod = "POST",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "user", description = "application user"))
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok")))
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string")))
  @Path(value = "/{userId}/unfollow")
  def unfollowUser: Route

  @ApiOperation(value = "reconnect-info", httpMethod = "POST")
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ReconnectInfo])))
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string")))
  @Path(value = "/{userId}/reconnect")
  def reconnectInfo: Route

  @ApiOperation(value = "resend-validation-email", httpMethod = "POST", code = HttpCodes.NoContent)
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No Content")))
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "body",
        paramType = "body",
        dataType = "org.make.api.user.ResendValidationEmailRequest"
      )
    )
  )
  @Path(value = "/validation-email")
  def resendValidationEmail: Route

  @ApiOperation(
    value = "upload-avatar",
    httpMethod = "POST",
    code = HttpCodes.OK,
    consumes = "multipart/form-data",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "data", paramType = "formData", dataType = "file")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[UploadResponse])))
  @Path(value = "/{userId}/upload-avatar")
  def uploadAvatar: Route

  @ApiOperation(
    value = "update-user-profile",
    httpMethod = "PUT",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "user", description = "application user"))
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[UserProfileResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.UserProfileRequest")
    )
  )
  @Path(value = "/{userId}/profile")
  def modifyUserProfile: Route

  @Path(value = "/privacy-policy")
  @ApiOperation(value = "get-privacy-policy-approval-date", httpMethod = "POST")
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.PrivacyPolicyRequest")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[UserPrivacyPolicyResponse]))
  )
  def getPrivacyPolicy: Route

  @Path(value = "/social/privacy-policy")
  @ApiOperation(value = "get-social-privacy-policy-approval-date", httpMethod = "POST")
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.user.SocialPrivacyPolicyRequest"
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[UserPrivacyPolicyResponse]))
  )
  def getSocialPrivacyPolicy: Route

  @Path(value = "/check-registration")
  @ApiOperation(value = "check-registration", httpMethod = "POST")
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.CheckRegistrationRequest")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No Content")))
  def checkRegistration: Route

  def routes: Route =
    getMe ~
      currentUser ~
      register ~
      socialLogin ~
      resetPasswordRequestRoute ~
      resetPasswordCheckRoute ~
      resetPasswordRoute ~
      validateAccountRoute ~
      getVotedProposalsByUser ~
      getProposalsByUser ~
      patchCurrentUser ~
      changePassword ~
      deleteUser ~
      followUser ~
      unfollowUser ~
      reconnectInfo ~
      resendValidationEmail ~
      uploadAvatar ~
      getUserProfile ~
      modifyUserProfile ~
      getPrivacyPolicy ~
      getSocialPrivacyPolicy ~
      checkRegistration

  val userId: PathMatcher1[UserId] =
    Segment.map(id => UserId(id))
}

trait UserApiComponent {
  def userApi: UserApi
}

trait DefaultUserApiComponent
    extends UserApiComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {

  this: MakeDirectivesDependencies
    with UserServiceComponent
    with SocialServiceComponent
    with ProposalServiceComponent
    with QuestionServiceComponent
    with EventBusServiceComponent
    with PersistentUserServiceComponent
    with UserHistoryCoordinatorServiceComponent
    with ReadJournalComponent
    with ActorSystemComponent
    with StorageServiceComponent
    with StorageConfigurationComponent
    with UserRegistrationValidatorComponent
    with ClientDirectives =>

  override lazy val userApi: UserApi = new DefaultUserApi

  class DefaultUserApi extends UserApi {

    override def getUserProfile: Route = {
      get {
        path("user" / userId / "profile") { userId =>
          makeOperation("GetUserProfile") { _ =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              authorize(userId == userAuth.user.userId) {
                userService.getUser(userId).asDirectiveOrNotFound { user =>
                  complete(
                    UserProfileResponse(
                      email = user.email,
                      firstName = user.firstName,
                      lastName = user.lastName,
                      dateOfBirth = user.profile.flatMap(_.dateOfBirth),
                      avatarUrl = user.profile.flatMap(_.avatarUrl),
                      profession = user.profile.flatMap(_.profession),
                      description = user.profile.flatMap(_.description),
                      postalCode = user.profile.flatMap(_.postalCode),
                      optInNewsletter = user.profile.forall(_.optInNewsletter),
                      website = user.profile.flatMap(_.website),
                      crmCountry = user.profile.map(_.crmCountry),
                      crmLanguage = user.profile.map(_.crmLanguage)
                    )
                  )
                }
              }
            }
          }
        }
      }
    }

    override def getMe: Route = {
      get {
        path("user" / "me") {
          makeOperation("GetMe") { _ =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              userService.getUser(userAuth.user.userId).asDirectiveOrNotFound { user =>
                userService.getFollowedUsers(user.userId).asDirective { followedUsers =>
                  complete(UserResponse(user, followedUsers))
                }
              }
            }
          }
        }
      }
    }

    override def currentUser: Route = {
      get {
        path("user" / "current") {
          makeOperation("GetCurrentUser") { _ =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              userService.getUser(userAuth.user.userId).asDirectiveOrNotFound { user =>
                complete(
                  CurrentUserResponse(
                    userId = user.userId,
                    email = user.email,
                    displayName = user.displayName,
                    userType = user.userType,
                    roles = user.roles,
                    hasPassword = user.hashedPassword.isDefined,
                    enabled = user.enabled,
                    emailVerified = user.emailVerified,
                    country = user.country,
                    avatarUrl = user.profile.flatMap(_.avatarUrl),
                    privacyPolicyApprovalDate = user.privacyPolicyApprovalDate,
                    crmCountry = user.profile.map(_.crmCountry),
                    crmLanguage = user.profile.map(_.crmLanguage)
                  )
                )
              }
            }
          }
        }
      }
    }

    override def socialLogin: Route = post {
      path("user" / "login" / "social") {
        makeOperation("SocialLogin", EndpointType.CoreOnly) { requestContext =>
          decodeRequest {
            entity(as[SocialLoginRequest]) { request: SocialLoginRequest =>
              extractClientOrDefault { client =>
                val language: Language =
                  request.language.orElse(requestContext.languageContext.language).getOrElse(Language("fr"))

                val futureMaybeQuestion: Future[Option[Question]] = requestContext.questionContext.questionId match {
                  case Some(questionId) => questionService.getQuestion(questionId)
                  case _                => Future.successful(None)
                }
                val privacyPolicyApprovalDate: Option[ZonedDateTime] = request.approvePrivacyPolicy match {
                  case Some(true) => Some(DateHelper.now())
                  case _          => None
                }

                futureMaybeQuestion.asDirective { maybeQuestion =>
                  onSuccess(
                    socialService
                      .login(
                        request.provider,
                        request.token,
                        request.country,
                        language,
                        request.crmCountry.getOrElse(Country("FR")),
                        request.crmLanguage.getOrElse(Language("fr")),
                        maybeQuestion.map(_.questionId),
                        requestContext,
                        client.clientId,
                        privacyPolicyApprovalDate,
                        request.optIn
                      )
                      .flatMap {
                        case (userId, social) =>
                          sessionHistoryCoordinatorService
                            .convertSession(requestContext.sessionId, userId, requestContext)
                            .map(_ => (userId, social))
                      }
                  ) {
                    case (userId, social) =>
                      setMakeSecure(requestContext.applicationName, social.toTokenResponse, userId) {
                        complete(StatusCodes.Created -> social)
                      }
                  }
                }
              }
            }
          }
        }
      }
    }

    /**
      *
      * proposals from themes will not be retrieve since themes are not displayed on front
      */
    override def getVotedProposalsByUser: Route = get {
      path("user" / userId / "votes") { userId: UserId =>
        makeOperation("UserVotedProposals") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            parameters(
              "votes".csv[VoteKey],
              "qualifications".csv[QualificationKey],
              "preferredLanguage".as[Language].?,
              "sort".?,
              "order".as[Order].?,
              "limit".as[Pagination.Limit].?,
              "skip".as[Pagination.Offset].?
            ) {
              (
                votes: Option[Seq[VoteKey]],
                qualifications: Option[Seq[QualificationKey]],
                preferredLanguage: Option[Language],
                sort: Option[String],
                order: Option[Order],
                limit: Option[Pagination.Limit],
                offset: Option[Pagination.Offset]
              ) =>
                if (userAuth.user.userId != userId) {
                  complete(StatusCodes.Forbidden)
                } else {
                  val defaultSort = Some("createdAt")
                  val defaultOrder = Some(Order.desc)
                  proposalService
                    .searchProposalsVotedByUser(
                      userId = userId,
                      filterVotes = votes,
                      filterQualifications = qualifications,
                      preferredLanguage = preferredLanguage,
                      sort = Some(
                        Sort(field = sort.orElse(defaultSort), mode = order.orElse(defaultOrder).map(_.sortOrder))
                      ),
                      limit = limit,
                      offset = offset,
                      requestContext = requestContext
                    )
                    .asDirective
                    .apply(complete(_))
                }
            }
          }
        }
      }
    }

    override def register: Route = post {
      path("user") {
        makeOperation("RegisterUser", EndpointType.Public) { requestContext =>
          decodeRequest {
            entity(as[RegisterUserRequest]) { request: RegisterUserRequest =>
              Validation.validate(userRegistrationValidator.requirements(request): _*)
              val language: Language =
                request.language.orElse(requestContext.languageContext.language).getOrElse(Language("fr"))

              val futureMaybeQuestion: Future[Option[Question]] = request.questionId match {
                case Some(questionId) => questionService.getQuestion(questionId)
                case _                => Future.successful(None)
              }

              futureMaybeQuestion.asDirective { maybeQuestion =>
                val privacyPolicyApprovalDate = request.approvePrivacyPolicy match {
                  case Some(true) => Some(DateHelper.now())
                  case _          => None
                }
                onSuccess(
                  userService
                    .register(
                      UserRegisterData(
                        email = request.email.value.toLowerCase,
                        firstName = request.firstName.map(_.value),
                        lastName = request.lastName.map(_.value),
                        password = Some(request.password.value),
                        lastIp = requestContext.ipAddress,
                        dateOfBirth = Some(request.dateOfBirth.birthDate),
                        profession = request.profession.map(_.value),
                        postalCode = request.postalCode.map(_.value),
                        country = request.country,
                        language = language,
                        crmCountry = request.crmCountry.getOrElse(Country("FR")),
                        crmLanguage = request.crmLanguage.getOrElse(Language("fr")),
                        gender = request.gender,
                        socioProfessionalCategory = request.socioProfessionalCategory,
                        questionId = maybeQuestion.map(_.questionId),
                        optIn = request.optIn,
                        optInPartner = request.optInPartner,
                        politicalParty = request.politicalParty,
                        website = request.website.map(_.value),
                        legalMinorConsent = request.legalMinorConsent,
                        legalAdvisorApproval = request.legalAdvisorApproval,
                        privacyPolicyApprovalDate = privacyPolicyApprovalDate
                      ),
                      requestContext
                    )
                    .flatMap { user =>
                      sessionHistoryCoordinatorService
                        .convertSession(requestContext.sessionId, user.userId, requestContext)
                        .map(_ => user)
                    }
                ) { result =>
                  complete(StatusCodes.Created -> UserResponse(result))
                }
              }
            }
          }
        }
      }
    }

    override def validateAccountRoute: Route = {
      post {
        path("user" / userId / "validate" / Segment) { (userId: UserId, verificationToken: String) =>
          makeOperation("UserValidation", EndpointType.Public) { requestContext =>
            persistentUserService
              .findUserByUserIdAndVerificationToken(userId, verificationToken)
              .asDirectiveOrNotFound { user =>
                if (user.verificationTokenExpiresAt.forall(_.isBefore(DateHelper.now()))) {
                  complete(StatusCodes.BadRequest)
                } else {
                  onSuccess(
                    userService
                      .validateEmail(user = user, verificationToken = verificationToken)
                      .map { token =>
                        eventBusService.publish(
                          UserValidatedAccountEvent(
                            userId = userId,
                            country = user.country,
                            requestContext = requestContext,
                            eventDate = DateHelper.now(),
                            eventId = Some(idGenerator.nextEventId())
                          )
                        )
                        token
                      }
                  ) { token =>
                    setMakeSecure(requestContext.applicationName, token, userId) {
                      complete(StatusCodes.NoContent)
                    }
                  }
                }
              }
          }
        }
      }
    }

    override def resetPasswordRequestRoute: Route = {
      post {
        path("user" / "reset-password" / "request-reset") {
          makeOperation("ResetPasswordRequest", EndpointType.Public) { requestContext =>
            optionalMakeOAuth2 { userAuth: Option[AuthInfo[UserRights]] =>
              decodeRequest(entity(as[ResetPasswordRequest]) { request =>
                persistentUserService.findByEmail(request.email.value.toLowerCase).asDirectiveOrNotFound { user =>
                  onSuccess(userService.requestPasswordReset(user.userId).map { result =>
                    eventBusService.publish(
                      ResetPasswordEvent(
                        userId = user.userId,
                        connectedUserId = userAuth.map(_.user.userId),
                        country = user.country,
                        requestContext = requestContext,
                        eventDate = DateHelper.now(),
                        eventId = Some(idGenerator.nextEventId())
                      )
                    )
                    result
                  }) { _ =>
                    complete(StatusCodes.NoContent)
                  }
                }
              })
            }
          }
        }
      }
    }

    override def resetPasswordCheckRoute: Route = {
      post {
        path("user" / "reset-password" / "check-validity" / userId / Segment) { (userId: UserId, resetToken: String) =>
          makeOperation("ResetPasswordCheck", EndpointType.Public) { _ =>
            persistentUserService.findUserByUserIdAndResetToken(userId, resetToken).asDirectiveOrNotFound { user =>
              if (user.resetTokenExpiresAt.exists(_.isAfter(DateHelper.now()))) {
                complete(StatusCodes.NoContent)
              } else {
                complete(StatusCodes.BadRequest)
              }
            }
          }
        }
      }
    }

    override def resetPasswordRoute: Route = {
      post {
        path("user" / "reset-password" / "change-password" / userId) { userId =>
          makeOperation("ResetPassword", EndpointType.Public) { _ =>
            decodeRequest {
              entity(as[ResetPassword]) { request: ResetPassword =>
                persistentUserService
                  .findUserByUserIdAndResetToken(userId, request.resetToken.value)
                  .asDirectiveOrNotFound { user =>
                    if (user.resetTokenExpiresAt.forall(_.isBefore(DateHelper.now()))) {
                      complete(StatusCodes.BadRequest)
                    } else {
                      onSuccess(
                        userService
                          .updatePassword(
                            userId = userId,
                            resetToken = Some(request.resetToken.value),
                            password = request.password.value
                          )
                      ) { _ =>
                        complete(StatusCodes.NoContent)
                      }
                    }
                  }
              }
            }
          }
        }
      }
    }

    /**
      *
      * proposals from themes will not be retrieve since themes are not displayed on front
      */
    override def getProposalsByUser: Route = get {
      path("user" / userId / "proposals") { userId: UserId =>
        makeOperation("UserProposals") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            parameters(
              "preferredLanguage".as[Language].?,
              "sort".?,
              "order".as[Order].?,
              "limit".as[Pagination.Limit].?,
              "skip".as[Pagination.Offset].?
            ) {
              (
                preferredLanguage: Option[Language],
                sort: Option[String],
                order: Option[Order],
                limit: Option[Pagination.Limit],
                offset: Option[Pagination.Offset]
              ) =>
                val connectedUserId: UserId = userAuth.user.userId
                if (connectedUserId != userId) {
                  complete(StatusCodes.Forbidden)
                } else {
                  val defaultSort = Some("createdAt")
                  val defaultOrder = Some(Order.desc)
                  proposalService
                    .searchForUser(
                      userId = Some(userId),
                      query = SearchQuery(
                        filters = Some(
                          SearchFilters(
                            users = Some(UserSearchFilter(userIds = Seq(userId))),
                            status =
                              Some(StatusSearchFilter(ProposalStatus.values.filter(_ != ProposalStatus.Archived)))
                          )
                        ),
                        sort = Some(
                          Sort(field = sort.orElse(defaultSort), mode = order.orElse(defaultOrder).map(_.sortOrder))
                        ),
                        limit = limit,
                        offset = offset
                      ),
                      requestContext = requestContext,
                      preferredLanguage = preferredLanguage,
                      questionDefaultLanguage = None
                    )
                    .asDirective
                    .apply(complete(_))
                }
            }
          }
        }
      }
    }

    override def patchCurrentUser: Route =
      patch {
        path("user") {
          makeOperation("PatchCurrentUser") { requestContext =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              decodeRequest {
                entity(as[UpdateUserRequest]) { request: UpdateUserRequest =>
                  userService.getUser(userAuth.user.userId).asDirectiveOrNotFound { user =>
                    val optInNewsletterHasChanged: Boolean = (request.optInNewsletter, user.profile) match {
                      case (Some(value), Some(profileValue)) => value != profileValue.optInNewsletter
                      case (Some(_), None)                   => true
                      case _                                 => false
                    }

                    val profile = user.profile.orElse(Profile.parseProfile())

                    val updatedProfile = profile.map(
                      _.copy(
                        dateOfBirth = request.dateOfBirth.map(_.birthDate).orElse(user.profile.flatMap(_.dateOfBirth)),
                        profession = request.profession.orElse(user.profile.flatMap(_.profession)),
                        postalCode = request.postalCode.map(_.value).orElse(user.profile.flatMap(_.postalCode)),
                        phoneNumber = request.phoneNumber.map(_.value).orElse(user.profile.flatMap(_.phoneNumber)),
                        description = request.description.map(_.value).orElse(user.profile.flatMap(_.description)),
                        optInNewsletter = request.optInNewsletter.getOrElse(user.profile.exists(_.optInNewsletter)),
                        gender = request.gender.orElse(user.profile.flatMap(_.gender)),
                        genderName = request.genderName.orElse(user.profile.flatMap(_.genderName)),
                        crmCountry = request.crmCountry.orElse(user.profile.map(_.crmCountry)).getOrElse(Country("FR")),
                        crmLanguage =
                          request.crmLanguage.orElse(user.profile.map(_.crmLanguage)).getOrElse(Language("fr")),
                        socioProfessionalCategory =
                          request.socioProfessionalCategory.orElse(user.profile.flatMap(_.socioProfessionalCategory)),
                        politicalParty = request.politicalParty.orElse(user.profile.flatMap(_.politicalParty)),
                        website = request.website.map(_.value).orElse(user.profile.flatMap(_.website))
                      )
                    )

                    onSuccess(
                      userService.update(
                        user.copy(
                          firstName = request.firstName.map(_.value).orElse(user.firstName),
                          lastName = request.lastName.orElse(user.lastName),
                          organisationName = request.organisationName.map(_.value).orElse(user.organisationName),
                          country = request.crmCountry.getOrElse(user.country),
                          profile = updatedProfile
                        ),
                        requestContext
                      )
                    ) { user: User =>
                      if (optInNewsletterHasChanged && user.userType == UserType.UserTypeUser) {
                        eventBusService.publish(
                          UserUpdatedOptInNewsletterEvent(
                            connectedUserId = Some(userAuth.user.userId),
                            userId = user.userId,
                            requestContext = requestContext,
                            eventDate = DateHelper.now(),
                            country = user.country,
                            optInNewsletter = user.profile.exists(_.optInNewsletter),
                            eventId = Some(idGenerator.nextEventId())
                          )
                        )
                      }
                      if (user.userType == UserType.UserTypeOrganisation) {
                        eventBusService.publish(
                          OrganisationUpdatedEvent(
                            connectedUserId = Some(user.userId),
                            userId = user.userId,
                            requestContext = requestContext,
                            country = user.country,
                            eventDate = DateHelper.now(),
                            eventId = Some(idGenerator.nextEventId())
                          )
                        )
                      }
                      userService.getFollowedUsers(user.userId).asDirective { followedUsers =>
                        complete(StatusCodes.OK -> UserResponse(user, followedUsers))
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

    override def changePassword: Route = post {
      path("user" / userId / "change-password") { userId: UserId =>
        makeOperation("ChangePassword") { _ =>
          decodeRequest {
            entity(as[ChangePasswordRequest]) { request: ChangePasswordRequest =>
              makeOAuth2 { userAuth: AuthInfo[UserRights] =>
                val connectedUserId: UserId = userAuth.user.userId
                if (connectedUserId != userId) {
                  complete(StatusCodes.Forbidden)
                } else {
                  userService.getUserByUserIdAndPassword(userId, request.actualPassword).asDirective {
                    case Some(_) =>
                      userService.updatePassword(userId, None, request.newPassword.value).asDirective { _ =>
                        complete(StatusCodes.OK)
                      }
                    case None =>
                      complete(
                        StatusCodes.BadRequest -> Seq(
                          ValidationError("password", "invalid_password", Some("Wrong password"))
                        )
                      )
                  }
                }
              }
            }
          }
        }
      }
    }

    override def deleteUser: Route = post {
      path("user" / userId / "delete") { userId: UserId =>
        makeOperation("deleteUser") { requestContext =>
          decodeRequest {
            entity(as[DeleteUserRequest]) { request: DeleteUserRequest =>
              makeOAuth2 { userAuth: AuthInfo[UserRights] =>
                if (userAuth.user.userId != userId) {
                  complete(StatusCodes.Forbidden)
                } else {
                  userService.getUserByUserIdAndPassword(userId, request.password).asDirective {
                    case Some(user) =>
                      userService
                        .anonymize(user, userAuth.user.userId, requestContext, Anonymization.Explicit)
                        .asDirective { _ =>
                          oauth2DataHandler.removeTokenByUserId(userId).asDirective { _ =>
                            addCookies(requestContext.applicationName, logoutCookies()) { complete(StatusCodes.OK) }
                          }
                        }
                    case None =>
                      complete(
                        StatusCodes.BadRequest -> Seq(
                          ValidationError("password", "invalid_password", Some("Wrong password"))
                        )
                      )
                  }
                }
              }
            }
          }
        }
      }
    }

    override def followUser: Route =
      post {
        path("user" / userId / "follow") { userId =>
          makeOperation("FollowUser") { requestContext =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              userService.getUser(userId).asDirectiveOrNotFound { user =>
                if (!user.publicProfile) {
                  complete(StatusCodes.Forbidden)
                } else {
                  userService.getFollowedUsers(userAuth.user.userId).asDirective { followedUsers =>
                    if (followedUsers.contains(userId)) {
                      complete(StatusCodes.BadRequest)
                    } else {
                      onSuccess(userService.followUser(userId, userAuth.user.userId, requestContext)) { _ =>
                        complete(StatusCodes.OK)
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

    override def unfollowUser: Route =
      post {
        path("user" / userId / "unfollow") { userId =>
          makeOperation("FollowUser") { requestContext =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              userService.getUser(userId).asDirectiveOrNotFound { _ =>
                userService.getFollowedUsers(userAuth.user.userId).asDirective { followedUsers =>
                  if (!followedUsers.contains(userId)) {
                    complete(
                      StatusCodes.BadRequest -> Seq(
                        ValidationError("userId", "invalid_state", Some("User already unfollowed"))
                      )
                    )
                  } else {
                    onSuccess(userService.unfollowUser(userId, userAuth.user.userId, requestContext)) { _ =>
                      complete(StatusCodes.OK)
                    }
                  }
                }
              }
            }
          }
        }
      }

    override def reconnectInfo: Route = {
      post {
        path("user" / userId / "reconnect") { userId =>
          makeOperation("ReconnectInfo") { _ =>
            userService.reconnectInfo(userId).asDirectiveOrNotFound { reconnectInfo =>
              complete(StatusCodes.OK -> reconnectInfo)
            }
          }
        }
      }
    }

    override def resendValidationEmail: Route = {
      post {
        path("user" / "validation-email") {
          makeOperation("ResendValidationEmail") { requestContext =>
            decodeRequest {
              entity(as[ResendValidationEmailRequest]) { entity =>
                userService.getUserByEmail(entity.email).asDirective { maybeUser =>
                  maybeUser.foreach { user =>
                    eventBusService.publish(
                      ResendValidationEmailEvent(
                        connectedUserId = None,
                        eventDate = DateHelper.now(),
                        userId = user.userId,
                        country = user.country,
                        requestContext = requestContext,
                        eventId = Some(idGenerator.nextEventId())
                      )
                    )
                  }
                  complete(StatusCodes.NoContent)
                }
              }
            }
          }
        }
      }
    }

    override def uploadAvatar: Route = {
      post {
        path("user" / userId / "upload-avatar") { userId =>
          makeOperation("UserUploadAvatar") { requestContext =>
            makeOAuth2 { user =>
              authorize(user.user.userId == userId || user.user.roles.contains(RoleAdmin)) {
                def uploadFile(extension: String, contentType: String, fileContent: Content): Future[String] =
                  storageService.uploadUserAvatar(extension, contentType, fileContent)
                uploadImageAsync("data", uploadFile, sizeLimit = Some(storageConfiguration.maxFileSize)) {
                  (path, file) =>
                    file.delete()
                    userService.getUser(userId).asDirectiveOrNotFound { user =>
                      val modifiedProfile = user.profile match {
                        case Some(profile) => Some(profile.copy(avatarUrl = Some(path)))
                        case None          => Profile.parseProfile(avatarUrl = Some(path))
                      }

                      onSuccess(userService.update(user.copy(profile = modifiedProfile), requestContext)) { _ =>
                        complete(UploadResponse(path))
                      }
                    }
                }
              }
            }
          }
        }
      }
    }

    override def modifyUserProfile: Route = {
      put {
        path("user" / userId / "profile") { userId =>
          makeOperation("modifyUserProfile") { requestContext =>
            makeOAuth2 { user =>
              authorize(user.user.userId == userId) {
                decodeRequest {
                  entity(as[UserProfileRequest]) { entity =>
                    Validation.validate(userRegistrationValidator.requirements(entity): _*)
                    userService.getUser(userId).asDirectiveOrNotFound { user =>
                      val modifiedProfile = user.profile
                        .orElse(Profile.parseProfile())
                        .map(
                          _.copy(
                            dateOfBirth = Some(entity.dateOfBirth.birthDate),
                            avatarUrl = entity.avatarUrl.map(_.value),
                            profession = entity.profession.map(_.value),
                            description = entity.description.map(_.value),
                            postalCode = entity.postalCode.map(_.value),
                            crmCountry = entity.crmCountry.getOrElse(Country("FR")),
                            crmLanguage = entity.crmLanguage.getOrElse(Language("fr")),
                            optInNewsletter = entity.optInNewsletter,
                            website = entity.website.map(_.value)
                          )
                        )

                      val modifiedUser =
                        user.copy(
                          profile = modifiedProfile,
                          firstName = Some(entity.firstName.value),
                          lastName = entity.lastName.map(_.value)
                        )

                      userService.update(modifiedUser, requestContext).asDirective { result =>
                        complete(
                          UserProfileResponse(
                            email = result.email,
                            firstName = result.firstName,
                            lastName = result.lastName,
                            dateOfBirth = result.profile.flatMap(_.dateOfBirth),
                            avatarUrl = result.profile.flatMap(_.avatarUrl),
                            profession = result.profile.flatMap(_.profession),
                            description = result.profile.flatMap(_.description),
                            postalCode = result.profile.flatMap(_.postalCode),
                            optInNewsletter = result.profile.forall(_.optInNewsletter),
                            website = result.profile.flatMap(_.website),
                            crmCountry = result.profile.map(_.crmCountry),
                            crmLanguage = result.profile.map(_.crmLanguage)
                          )
                        )
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def getPrivacyPolicy: Route = {
      post {
        path("user" / "privacy-policy") {
          makeOperation("GetPrivacyPolicy") { _ =>
            decodeRequest {
              entity(as[PrivacyPolicyRequest]) { request =>
                userService.getUserByEmailAndPassword(request.email, request.password).asDirective {
                  case None =>
                    complete(
                      StatusCodes.BadRequest -> Seq(
                        ValidationError("email", "invalid", Some("email or password is invalid."))
                      )
                    )
                  case Some(user) => complete(UserPrivacyPolicyResponse(user.privacyPolicyApprovalDate))
                }
              }
            }
          }
        }
      }
    }

    override def getSocialPrivacyPolicy: Route = {
      post {
        path("user" / "social" / "privacy-policy") {
          makeOperation("GetSocialPrivacyPolicy") { _ =>
            decodeRequest {
              entity(as[SocialPrivacyPolicyRequest]) { request =>
                socialService.getUserByProviderAndToken(request.provider, request.token).asDirective { maybeUser =>
                  complete(UserPrivacyPolicyResponse(maybeUser.flatMap(_.privacyPolicyApprovalDate)))
                }
              }
            }
          }
        }
      }
    }

    override def checkRegistration: Route = {
      post {
        path("user" / "check-registration") {
          makeOperation("CheckRegistration") { _ =>
            decodeRequest {
              entity(as[CheckRegistrationRequest]) { request =>
                userService.getUserByEmail(request.email.value).asDirective {
                  case None    => complete(StatusCodes.NoContent)
                  case Some(_) => failWith(EmailAlreadyRegisteredException(request.email.value))
                }
              }
            }
          }
        }
      }
    }
  }
}
