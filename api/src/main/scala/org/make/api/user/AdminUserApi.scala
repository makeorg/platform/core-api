/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.user

import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.string.Url
import grizzled.slf4j.Logging
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.refined._
import io.circe.{Decoder, Encoder}
import io.swagger.annotations._
import scalaoauth2.provider.AuthInfo
import cats.implicits._
import org.make.api.proposal.ProposalServiceComponent
import org.make.api.question.QuestionServiceComponent
import org.make.api.technical.EventBusServiceComponent
import org.make.api.technical.CsvReceptacle._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.technical.crm.SendMailPublisherServiceComponent
import org.make.api.technical.storage.{Content, StorageConfigurationComponent, StorageServiceComponent, UploadResponse}
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.api.user.DefaultPersistentUserServiceComponent.PersistentUser
import org.make.core._
import org.make.core.Validation._
import org.make.core.auth.UserRights
import org.make.core.job.Job.JobId
import org.make.core.profile.Profile
import org.make.core.question.QuestionId
import org.make.core.reference.Country
import org.make.core.technical.Pagination
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils
import org.make.core.user._

import javax.ws.rs.Path
import scala.annotation.meta.field
import scala.concurrent.Future
import scala.util.{Failure, Success}

@Api(value = "Admin Users")
@Path(value = "/admin/users")
trait AdminUserApi extends Directives {

  @ApiOperation(
    value = "get-users",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = PersistentUser.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "id", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(name = "email", paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "role",
        paramType = "query",
        dataType = "string",
        defaultValue = "ROLE_MODERATOR",
        allowableValues = Role.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "userType",
        paramType = "query",
        dataType = "string",
        defaultValue = "USER",
        allowableValues = UserType.swaggerAllowableValues
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[AdminUserResponse]]))
  )
  @Path(value = "/")
  def getUsers: Route

  @ApiOperation(
    value = "update-user",
    httpMethod = "PUT",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "userId",
        paramType = "path",
        dataType = "string",
        example = "11111111-2222-3333-4444-555555555555"
      ),
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.AdminUpdateUserRequest")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[AdminUserResponse]))
  )
  @Path(value = "/{userId}")
  def updateUser: Route

  @ApiOperation(
    value = "get-user",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[AdminUserResponse]))
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string")))
  @Path(value = "/{userId}")
  def getUser: Route

  @ApiOperation(
    value = "admin-anonymize-user",
    httpMethod = "DELETE",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "userId", paramType = "path", dataType = "string")))
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok")))
  @Path(value = "/{userId}")
  def anonymizeUser: Route

  @ApiOperation(
    value = "admin-anonymize-user-by-email",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.AnonymizeUserRequest")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok")))
  @Path(value = "/anonymize")
  def anonymizeUserByEmail: Route

  @ApiOperation(
    value = "admin-upload-avatar",
    httpMethod = "POST",
    code = HttpCodes.OK,
    consumes = "multipart/form-data",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "userType",
        paramType = "path",
        dataType = "string",
        defaultValue = "USER",
        allowableValues = UserType.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "data", paramType = "formData", dataType = "file")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[UploadResponse])))
  @Path(value = "/upload-avatar/{userType}")
  def adminUploadAvatar: Route

  @ApiOperation(
    value = "update-user-email",
    httpMethod = "POST",
    code = HttpCodes.NoContent,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.AdminUpdateUserEmail")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No content")))
  @Path(value = "/update-user-email")
  def updateUserEmail: Route

  @ApiOperation(
    value = "update-user-roles",
    httpMethod = "POST",
    code = HttpCodes.NoContent,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.user.UpdateUserRolesRequest")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No Content")))
  @Path(value = "/update-user-roles")
  def updateUserRoles: Route

  @ApiOperation(
    value = "anonymize-users",
    httpMethod = "DELETE",
    code = HttpCodes.Accepted,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiResponses(
    value = Array(
      new ApiResponse(code = HttpCodes.Accepted, message = "Accepted"),
      new ApiResponse(code = HttpCodes.Conflict, message = "Conflict")
    )
  )
  @Path(value = "/")
  def anonymizeUsers: Route

  @ApiOperation(
    value = "send-email-to-proposer",
    httpMethod = "POST",
    code = HttpCodes.NoContent,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No Content")))
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "body", paramType = "body", dataType = "org.make.api.user.SendProposerEmailRequest")
    )
  )
  @Path(value = "/send-email-to-proposer")
  def sendEmailToProposer: Route

  def routes: Route =
    getUsers ~ getUser ~ updateUser ~ anonymizeUser ~ anonymizeUserByEmail ~ adminUploadAvatar ~ updateUserEmail ~ updateUserRoles ~ anonymizeUsers ~ sendEmailToProposer

}

trait AdminUserApiComponent {
  def adminUserApi: AdminUserApi
}

trait DefaultAdminUserApiComponent
    extends AdminUserApiComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {

  this: MakeDirectivesDependencies
    with UserServiceComponent
    with ProposalServiceComponent
    with QuestionServiceComponent
    with EventBusServiceComponent
    with PersistentUserServiceComponent
    with SendMailPublisherServiceComponent
    with StorageServiceComponent
    with StorageConfigurationComponent =>

  override lazy val adminUserApi: AdminUserApi = new DefaultAdminUserApi

  class DefaultAdminUserApi extends AdminUserApi {

    val userId: PathMatcher1[UserId] = Segment.map(UserId.apply)

    override def getUsers: Route = get {
      path("admin" / "users") {
        makeOperation("AdminGetUsers") { _ =>
          parameters(
            "_start".as[Pagination.Offset].?,
            "_end".as[Pagination.End].?,
            "_sort".?,
            "_order".as[Order].?,
            "id".csv[UserId],
            "email".?,
            "role".as[String].?,
            "userType".as[UserType].?
          ) {
            (
              offset: Option[Pagination.Offset],
              end: Option[Pagination.End],
              sort: Option[String],
              order: Option[Order],
              ids: Option[Seq[UserId]],
              email: Option[String],
              maybeRole: Option[String],
              userType: Option[UserType]
            ) =>
              makeOAuth2 { auth: AuthInfo[UserRights] =>
                requireSuperAdminRole(auth.user) {
                  val role: Option[Role] = maybeRole.map(Role.apply)
                  (
                    userService
                      .adminCountUsers(
                        ids = ids,
                        email = email,
                        firstName = None,
                        lastName = None,
                        role = role,
                        userType = userType
                      )
                      .asDirective,
                    userService
                      .adminFindUsers(
                        offset.orZero,
                        end,
                        sort,
                        order,
                        ids = ids,
                        email = email,
                        firstName = None,
                        lastName = None,
                        role = role,
                        userType = userType
                      )
                      .asDirective
                  ).tupled.apply {
                    case (count, users) =>
                      complete(
                        (StatusCodes.OK, List(`X-Total-Count`(count.toString)), users.map(AdminUserResponse.apply))
                      )
                  }
                }
              }
          }
        }
      }
    }

    override def updateUser: Route =
      put {
        path("admin" / "users" / userId) { userId =>
          makeOperation("AdminUpdateUser") { requestContext =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              requireSuperAdminRole(userAuth.user) {
                decodeRequest {
                  entity(as[AdminUpdateUserRequest]) { request: AdminUpdateUserRequest =>
                    userService.getUser(userId).asDirectiveOrNotFound { user =>
                      val lowerCasedEmail: String = request.email.getOrElse(user.email).toLowerCase()
                      userService.getUserByEmail(lowerCasedEmail).asDirective { maybeUser =>
                        maybeUser.foreach { userToCheck =>
                          Validation.validate(
                            Validation.validateField(
                              field = "email",
                              "already_registered",
                              condition = userToCheck.userId.value == user.userId.value,
                              message = s"Email $lowerCasedEmail already exists"
                            )
                          )
                        }
                        val profile: Option[Profile] = user.profile
                          .map(
                            _.copy(
                              website = request.website.map(_.value),
                              politicalParty = request.politicalParty,
                              optInNewsletter = request.optInNewsletter,
                              avatarUrl = request.avatarUrl.map(_.value)
                            )
                          )
                          .orElse(
                            Profile.parseProfile(
                              website = request.website.map(_.value),
                              politicalParty = request.politicalParty,
                              optInNewsletter = request.optInNewsletter,
                              avatarUrl = request.avatarUrl.map(_.value)
                            )
                          )

                        onSuccess(
                          userService.update(
                            user.copy(
                              email = lowerCasedEmail,
                              firstName = request.firstName.orElse(user.firstName),
                              lastName = request.lastName.orElse(user.lastName),
                              country = request.country.getOrElse(user.country),
                              organisationName = request.organisationName,
                              userType = request.userType,
                              roles = request.roles.map(_.map(Role.apply)).getOrElse(user.roles),
                              availableQuestions = request.availableQuestions,
                              profile = profile
                            ),
                            requestContext
                          )
                        ) { user: User =>
                          complete(StatusCodes.OK -> AdminUserResponse(user))
                        }
                      }
                    }
                  }
                }
              }

            }
          }
        }
      }

    override def getUser: Route = get {
      path("admin" / "users" / userId) { userId =>
        makeOperation("GetUser") { _ =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireSuperAdminRole(auth.user) {
              userService.getUser(userId).asDirectiveOrNotFound { user =>
                complete(AdminUserResponse(user))
              }
            }
          }
        }
      }
    }

    override def anonymizeUser: Route = delete {
      path("admin" / "users" / userId) { userId: UserId =>
        makeOperation("adminDeleteUser") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireSuperAdminRole(userAuth.user) {
              userService.getUser(userId).asDirectiveOrNotFound { user =>
                userService.anonymize(user, userAuth.user.userId, requestContext, Anonymization.Explicit).asDirective {
                  _ =>
                    oauth2DataHandler.removeTokenByUserId(userId).asDirective { _ =>
                      complete(StatusCodes.OK)
                    }
                }
              }
            }
          }
        }
      }
    }

    override def anonymizeUsers: Route = delete {
      path("admin" / "users") {
        makeOperation("adminDeleteUsers") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireSuperAdminRole(userAuth.user) {
              userService.anonymizeInactiveUsers(userAuth.user.userId, requestContext).asDirective { acceptance =>
                if (acceptance.isAccepted)
                  complete(StatusCodes.Accepted -> JobId.AnonymizeInactiveUsers)
                else
                  complete(StatusCodes.Conflict -> JobId.AnonymizeInactiveUsers)
              }
            }
          }
        }
      }
    }

    override def anonymizeUserByEmail: Route = post {
      path("admin" / "users" / "anonymize") {
        makeOperation("anonymizeUserByEmail") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireSuperAdminRole(userAuth.user) {
              decodeRequest {
                entity(as[AnonymizeUserRequest]) { request =>
                  userService.getUserByEmail(request.email).asDirectiveOrNotFound { user =>
                    userService
                      .anonymize(user, userAuth.user.userId, requestContext, Anonymization.Explicit)
                      .asDirective
                      .apply { _ =>
                        oauth2DataHandler.removeTokenByUserId(user.userId).asDirective { _ =>
                          complete(StatusCodes.OK)
                        }
                      }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def adminUploadAvatar: Route = {
      post {
        path("admin" / "users" / "upload-avatar" / UserType) { userType =>
          makeOperation("AdminUserUploadAvatar") { _ =>
            makeOAuth2 { userAuth =>
              requireAdminRole(userAuth.user) {
                def uploadFile(extension: String, contentType: String, fileContent: Content): Future[String] =
                  storageService.uploadAdminUserAvatar(extension, contentType, fileContent, userType)

                uploadImageAsync("data", uploadFile, sizeLimit = Some(storageConfiguration.maxFileSize)) {
                  (path, file) =>
                    file.delete()
                    complete(UploadResponse(path))
                }
              }
            }
          }
        }
      }
    }

    override def updateUserEmail: Route = {
      post {
        path("admin" / "users" / "update-user-email") {
          makeOperation("AdminUserUpdateEmail") { _ =>
            makeOAuth2 { userAuth =>
              requireSuperAdminRole(userAuth.user) {
                decodeRequest {
                  entity(as[AdminUpdateUserEmail]) {
                    case AdminUpdateUserEmail(oldEmail, newEmail) =>
                      userService.getUserByEmail(oldEmail).asDirective {
                        case Some(user) =>
                          userService
                            .adminUpdateUserEmail(user, newEmail)
                            .asDirective
                            .apply(_ => complete(StatusCodes.NoContent))
                        case None =>
                          complete(
                            StatusCodes.BadRequest ->
                              Seq(
                                ValidationError("oldEmail", "not_found", Some(s"No user found for email '$oldEmail'"))
                              )
                          )
                      }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def updateUserRoles: Route = {
      post {
        path("admin" / "users" / "update-user-roles") {
          makeOperation("UpdateUserRoles") { requestContext =>
            makeOAuth2 { userAuth =>
              requireSuperAdminRole(userAuth.user) {
                decodeRequest {
                  entity(as[UpdateUserRolesRequest]) { request =>
                    userService.getUserByEmail(request.email).asDirective {
                      case None =>
                        complete(
                          StatusCodes.BadRequest ->
                            Seq(
                              ValidationError(
                                field = "email",
                                key = "not_found",
                                message = Some(s"The email ${request.email} was not found")
                              )
                            )
                        )
                      case Some(user) =>
                        userService.update(user.copy(roles = request.roles), requestContext).asDirective { _ =>
                          complete(StatusCodes.NoContent)
                        }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    private val MaximumEmailLength = 1000

    override def sendEmailToProposer: Route = {
      post {
        path("admin" / "users" / "send-email-to-proposer") {
          makeOperation("SendProposerEmail") { requestContext =>
            makeOAuth2 { userAuth =>
              requireAdminRole(userAuth.user) {
                decodeRequest {
                  entity(as[SendProposerEmailRequest]) { entity =>
                    SanitizedHtml.fromString(entity.text, Some(MaximumEmailLength)) match {
                      case Failure(_) =>
                        complete(
                          StatusCodes.BadRequest -> Seq(
                            ValidationError(
                              "text",
                              "too_long",
                              Some(s"The length of the email's body must not exceed $MaximumEmailLength characters")
                            )
                          )
                        )
                      case Success(sanitizedHtml) =>
                        sendMailPublisherService
                          .sendEmailToProposer(
                            entity.proposalId,
                            userAuth.user.userId,
                            sanitizedHtml,
                            entity.dryRun,
                            requestContext
                          )
                          .asDirective
                          .apply(_ => complete(StatusCodes.NoContent))
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

final case class AnonymizeUserRequest(email: String) {

  email.toEmail.throwIfInvalid()
}

object AnonymizeUserRequest {
  implicit val decoder: Decoder[AnonymizeUserRequest] = deriveDecoder[AnonymizeUserRequest]
}

final case class AdminUserResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true) id: UserId,
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true)
  email: String,
  firstName: Option[String],
  lastName: Option[String],
  organisationName: Option[String],
  @(ApiModelProperty @field)(dataType = "string", required = true, allowableValues = UserType.swaggerAllowableValues) userType: UserType,
  @(ApiModelProperty @field)(dataType = "list[string]", required = true, allowableValues = Role.swaggerAllowableValues) roles: Seq[
    Role
  ],
  @(ApiModelProperty @field)(dataType = "string", example = "FR", required = true) country: Country,
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  availableQuestions: Seq[QuestionId],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com")
  website: Option[String],
  politicalParty: Option[String],
  @(ApiModelProperty @field)(dataType = "boolean") optInNewsletter: Option[Boolean],
  avatarUrl: Option[String]
) {
  Validation.validate(validateUserInput("email", email, None))
}

object AdminUserResponse extends CirceFormatters {
  implicit val encoder: Encoder[AdminUserResponse] = deriveEncoder[AdminUserResponse]
  implicit val decoder: Decoder[AdminUserResponse] = deriveDecoder[AdminUserResponse]

  def apply(user: User): AdminUserResponse = AdminUserResponse(
    id = user.userId,
    email = user.email,
    firstName = user.firstName,
    organisationName = user.organisationName,
    userType = user.userType,
    lastName = user.lastName,
    roles = user.roles.map(role => CustomRole(role.value)),
    country = user.country,
    availableQuestions = user.availableQuestions,
    politicalParty = user.profile.flatMap(_.politicalParty),
    website = user.profile.flatMap(_.website),
    optInNewsletter = user.profile.map(_.optInNewsletter),
    avatarUrl = user.profile.flatMap(_.avatarUrl)
  )
}

final case class AdminUpdateUserRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org")
  email: Option[String],
  firstName: Option[String],
  lastName: Option[String],
  organisationName: Option[String],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = UserType.swaggerAllowableValues) userType: UserType,
  roles: Option[Seq[String]],
  @(ApiModelProperty @field)(dataType = "string", example = "FR") country: Option[Country],
  @(ApiModelProperty @field)(
    dataType = "list[string]",
    example = "11111111-2222-3333-4444-555555555555",
    required = true
  )
  availableQuestions: Seq[QuestionId],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String Refined Url],
  politicalParty: Option[String],
  optInNewsletter: Boolean,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png")
  avatarUrl: Option[String Refined Url]
) {
  private val maxCountryLength = 3

  email.foreach(_.toEmail.throwIfInvalid())
  country.foreach(_.value.withMaxLength(maxCountryLength, "country").throwIfInvalid())
}

object AdminUpdateUserRequest {
  implicit lazy val decoder: Decoder[AdminUpdateUserRequest] = deriveDecoder[AdminUpdateUserRequest]
}

final case class AdminUpdateUserEmail(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+old@make.org", required = true) oldEmail: String,
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+new@make.org", required = true) newEmail: String
) {

  (oldEmail.toEmail, newEmail.toEmail).tupled.throwIfInvalid()
}

object AdminUpdateUserEmail {
  implicit val decoder: Decoder[AdminUpdateUserEmail] = deriveDecoder
  implicit val encoder: Encoder[AdminUpdateUserEmail] = deriveEncoder
}

final case class UpdateUserRolesRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true) email: String,
  @(ApiModelProperty @field)(dataType = "list[string]", required = true, allowableValues = Role.swaggerAllowableValues) roles: Seq[
    Role
  ]
) {

  email.toEmail.throwIfInvalid()
}

object UpdateUserRolesRequest {
  implicit lazy val decoder: Decoder[UpdateUserRolesRequest] = deriveDecoder[UpdateUserRolesRequest]
  implicit lazy val encoder: Encoder[UpdateUserRolesRequest] = deriveEncoder[UpdateUserRolesRequest]
}
