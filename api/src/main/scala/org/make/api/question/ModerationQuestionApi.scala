/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.question

import cats.implicits._
import akka.http.scaladsl.model.{MediaTypes, StatusCodes}
import akka.http.scaladsl.server._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.http.scaladsl.unmarshalling.Unmarshaller._
import akka.stream.Materializer
import akka.stream.alpakka.csv.scaladsl.{CsvParsing, CsvToMap}
import akka.stream.scaladsl.{Sink, Source}
import cats.data.Validated.Valid
import cats.data.ValidatedNec
import eu.timepit.refined.api.Refined
import eu.timepit.refined.boolean.And
import eu.timepit.refined.collection.{MaxSize, MinSize}
import grizzled.slf4j.Logging
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import io.circe.refined._
import io.swagger.annotations._

import javax.ws.rs.Path
import org.make.api.operation.{OperationOfQuestionServiceComponent, OperationServiceComponent}
import org.make.api.proposal.{
  ExhaustiveSearchRequest,
  ProposalIdResponse,
  ProposalSearchEngineComponent,
  ProposalServiceComponent,
  RefuseProposalRequest
}
import org.make.api.question.DefaultPersistentQuestionServiceComponent.PersistentQuestion
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.Traverses.RichTraverse
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.technical.storage.{Content, FileType, StorageServiceComponent, UploadResponse}
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.api.user.UserServiceComponent
import org.make.core._
import org.make.core.Validation._
import org.make.core.technical.{Multilingual, MultilingualUtils, Pagination}
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils
import org.make.core.auth.UserRights
import org.make.core.operation.OperationId
import org.make.core.proposal.{
  ContentSearchFilter,
  ProposalType,
  ProposalTypesSearchFilter,
  QuestionSearchFilter,
  SearchFilters,
  SearchQuery,
  UserSearchFilter
}
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import org.make.core.tag.TagId
import org.make.core.user.Role.RoleAdmin
import scalaoauth2.provider.AuthInfo

import scala.annotation.meta.field
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Api(value = "Moderation questions")
@Path(value = "/moderation/questions")
trait ModerationQuestionApi extends Directives {

  @ApiOperation(
    value = "moderation-list-questions",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = PersistentQuestion.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "slug", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "operationId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "country", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "language", paramType = "query", dataType = "string")
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[ModerationQuestionResponse]]))
  )
  @Path(value = "/")
  def listQuestions: Route

  @ApiOperation(
    value = "moderation-create-initial-proposal",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "body",
        paramType = "body",
        dataType = "org.make.api.question.CreateInitialProposalRequest"
      ),
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.Created, message = "Ok", response = classOf[ProposalIdResponse]))
  )
  @Path(value = "/{questionId}/initial-proposals")
  def addInitialProposal: Route

  @ApiOperation(
    value = "moderation-refuse-initial-proposal",
    httpMethod = "POST",
    code = HttpCodes.NoContent,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string")))
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No Content")))
  @Path(value = "/{questionId}/initial-proposals/refuse")
  def refuseInitialProposals: Route

  @ApiOperation(
    value = "moderation-inject-proposal",
    httpMethod = "POST",
    consumes = "multipart/form-data",
    code = HttpCodes.NoContent,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "proposals", paramType = "formData", dataType = "file", required = true),
      new ApiImplicitParam(
        name = "body",
        paramType = "formData",
        dataType = "org.make.api.question.InjectProposalsRequest"
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.Accepted, message = "Accepted")))
  @Path(value = "/{questionId}/inject-proposals")
  def injectProposals: Route

  @ApiOperation(
    value = "moderation-get-question",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModerationQuestionResponse]))
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string")))
  @Path(value = "/{questionId}")
  def getQuestion: Route

  @ApiOperation(
    value = "moderation-upload-operation-consultation-image",
    httpMethod = "POST",
    code = HttpCodes.OK,
    consumes = "multipart/form-data",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "data", paramType = "formData", dataType = "file", required = true)
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[UploadResponse])))
  @Path(value = "/{questionId}/image")
  def uploadQuestionImage: Route

  @ApiOperation(
    value = "moderation-upload-consultation-report",
    httpMethod = "POST",
    code = HttpCodes.OK,
    consumes = "multipart/form-data",
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "data", paramType = "formData", dataType = "file", required = true)
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[UploadResponse])))
  @Path(value = "/{questionId}/report")
  def uploadReport: Route

  def routes: Route =
    listQuestions ~ getQuestion ~ addInitialProposal ~ refuseInitialProposals ~ uploadQuestionImage ~ uploadReport ~ injectProposals

}

trait ModerationQuestionComponent {
  def moderationQuestionApi: ModerationQuestionApi
}

trait DefaultModerationQuestionComponent
    extends ModerationQuestionComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {

  this: MakeDirectivesDependencies
    with QuestionServiceComponent
    with ProposalServiceComponent
    with ProposalSearchEngineComponent
    with StorageServiceComponent
    with OperationOfQuestionServiceComponent
    with OperationServiceComponent
    with UserServiceComponent =>

  override lazy val moderationQuestionApi: ModerationQuestionApi = new DefaultModerationQuestionApi

  class DefaultModerationQuestionApi extends ModerationQuestionApi {

    import Validation.AnyWithParsers

    lazy val questionId: PathMatcher1[QuestionId] = Segment.map(QuestionId.apply)

    @SuppressWarnings(Array("org.wartremover.warts.Throw"))
    override def addInitialProposal: Route = post {
      path("moderation" / "questions" / questionId / "initial-proposals") { questionId =>
        makeOperation("ModerationAddInitialProposalToQuestion") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireAdminRole(userAuth.user) {
              decodeRequest {
                entity(as[CreateInitialProposalRequest]) { request =>
                  questionService.getQuestion(questionId).asDirectiveOrNotFound { question =>
                    val contents =
                      List(
                        (
                          request.contentTranslations
                            .map(_.mapTranslations(_.value))
                            .getOrElse(Multilingual.empty)
                            .addTranslation(request.submittedAsLanguage, request.content.value),
                          "contents"
                        )
                      )
                    elasticsearchProposalAPI
                      .countProposals(
                        SearchQuery(filters = SearchFilters(
                          question = QuestionSearchFilter(Seq(questionId)).some,
                          proposalTypes = ProposalTypesSearchFilter(Seq(ProposalType.ProposalTypeInitial)).some
                        ).some
                        )
                      )
                      .asDirective { initialProposalsCount =>
                        (
                          // NOTE: cannot rewrite as MultilingualUtils.genDecoder since the required languages aren't located in the same data structure
                          MultilingualUtils
                            .hasRequiredTranslations(question.languages.toList.toSet, contents)
                            .toValidationEither,
                          request.country
                            .toOneOf(question.countries.toList, "country")
                            .toValidationEither,
                          initialProposalsCount
                            .max("proposals", 20, Some("Too many initial proposals"))
                            .toValidationEither
                        ).mapN {
                          case (_, country, _) =>
                            proposalService
                              .createInitialProposal(
                                request.content.value,
                                request.contentTranslations.map(_.mapTranslations(_.value)),
                                question,
                                country.value,
                                request.submittedAsLanguage,
                                false,
                                request.tags,
                                request.author,
                                userAuth.user.userId,
                                requestContext
                              )
                        }.fold(failWith, _.asDirective { proposalId =>
                          complete(StatusCodes.Created -> ProposalIdResponse(proposalId))
                        })
                      }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def refuseInitialProposals: Route = post {
      path("moderation" / "questions" / questionId / "initial-proposals" / "refuse") { questionId =>
        makeOperation("ModerationRefuseInitialProposals") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireAdminRole(userAuth.user) {
              val query = ExhaustiveSearchRequest(
                questionIds = Some(Seq(questionId)),
                proposalTypes = Some(Seq(ProposalType.ProposalTypeInitial))
              ).toSearchQuery(requestContext)
              proposalService.searchInIndex(query = query, requestContext = requestContext).asDirective { proposals =>
                Future
                  .traverse(proposals.results.map(_.id)) { proposalId =>
                    proposalService
                      .refuseProposal(
                        proposalId,
                        userAuth.user.userId,
                        requestContext,
                        RefuseProposalRequest(sendNotificationEmail = false, refusalReason = Some("Other"))
                      )
                  }
                  .asDirective { _ =>
                    complete(StatusCodes.NoContent)
                  }
              }
            }
          }
        }
      }
    }

    private case class UserData(firstName: String, age: Option[String])

    private def getDuplicateLanguageWarning(
      translations: Option[Multilingual[String]],
      contentOriginalLanguage: String,
      lineNumber: Int
    ): Option[String] = {
      translations.flatMap { t =>
        Option.when(t.providedLanguages.contains(Language(contentOriginalLanguage)))(
          s"Warning: duplicate language $contentOriginalLanguage on line $lineNumber, proposal from the content column was chosen"
        )
      }
    }

    private def getTranslations(line: Map[String, String]): Option[Map[String, String]] = {
      val notLanguagesHeaders = Seq("content", "contentOriginalLanguage", "firstName", "age", "externalUserId")
      line.filterNot {
        case (key, value) => notLanguagesHeaders.contains(key) || value.isEmpty
      } match {
        case t if t.isEmpty => None
        case t              => Some(t)
      }
    }

    @SuppressWarnings(Array("org.wartremover.warts.MutableDataStructures"))
    private def validateLine(
      line: Map[String, String],
      userData: mutable.Map[String, UserData],
      questionLanguages: List[Language],
      lineNumber: Int
    ): ValidatedNec[
      ValidationError,
      (
        StringWithMaxLength,
        StringWithMinLength,
        NonEmptyString,
        Option[Multilingual[String]],
        NonEmptyString,
        Option[Int],
        NonEmptyString,
        Unit
      )
    ] = {
      val maxProposalLength = BusinessConfig.defaultProposalMaxLength
      val minProposalLength = FrontConfiguration.defaultProposalMinLength
      val translations: Option[Multilingual[String]] = getTranslations(line).map(t => Multilingual.fromMap(t))
      (
        line
          .getOrElse("content", "")
          .withMaxLength(
            maxProposalLength,
            "content",
            None,
            Some(s"content should be less than $maxProposalLength characters on line $lineNumber")
          ),
        line
          .getOrElse("content", "")
          .withMinLength(
            minProposalLength,
            "content",
            None,
            Some(s"content should be more than $minProposalLength characters on line $lineNumber")
          ),
        line.getNonEmptyString(
          "contentOriginalLanguage",
          Some(s"contentOriginalLanguage is required on line $lineNumber")
        ),
        MultilingualUtils
          .hasRequiredTranslationsFromCsv(
            questionLanguages.toSet,
            translations,
            line.get("contentOriginalLanguage").map(Language(_)),
            lineNumber
          ),
        line.getNonEmptyString("firstName", Some(s"firstName is required on line $lineNumber")),
        line
          .get("age")
          .traverse(_.toValidAge(Some(s"age should be over than $minLegalAgeForExternalProposal on line $lineNumber"))),
        line.getNonEmptyString("externalUserId", Some(s"externalUserId is required on line $lineNumber")),
        line.get("externalUserId") match {
          case None => ().validNec
          case Some(id) =>
            userData.get(id) match {
              case Some(UserData(firstName, _)) if !line.get("firstName").contains(firstName) =>
                ValidationError(
                  "firstName",
                  "unexpected_value",
                  Some(s"users with the same externalUserId should have the same firstName on line $lineNumber")
                ).invalidNec
              case Some(UserData(_, age)) if line.get("age") != age =>
                ValidationError(
                  "age",
                  "unexpected_value",
                  Some(s"users with the same externalUserId should have the same age on line $lineNumber")
                ).invalidNec
              case _ =>
                userData += (id -> UserData(line.getOrElse("firstName", ""), line.get("age")))
                ().validNec
            }
        }
      ).tupled
    }

    @SuppressWarnings(Array("org.wartremover.warts.MutableDataStructures"))
    override def injectProposals: Route = post {
      path("moderation" / "questions" / questionId / "inject-proposals") { questionId =>
        makeOperation("ModerationInjectProposals") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireAdminRole(userAuth.user) {
              questionService.getQuestion(questionId).asDirectiveOrNotFound { question =>
                extractRequestContext { ctx =>
                  implicit val materializer: Materializer = ctx.materializer
                  formField("body") { body =>
                    Unmarshal(body).to[InjectProposalsRequest].asDirective { request =>
                      fileUpload("proposals") {
                        case (fileInfo, byteSource) =>
                          Validation.validate(
                            validateField(
                              fileInfo.fieldName,
                              "invalid_format",
                              fileInfo.contentType.mediaType == MediaTypes.`text/csv`,
                              "File must be a csv"
                            )
                          )
                          byteSource
                            .recoverWithRetries(
                              attempts = 1, {
                                case e: Exception =>
                                  logger.error(s"Failed to parse CSV file: ${e.getMessage}")
                                  Source.failed(new IllegalArgumentException("Invalid CSV format"))
                              }
                            )
                            .via(CsvParsing.lineScanner())
                            .via(CsvToMap.toMapAsStrings())
                            .runWith(Sink.seq)
                            .asDirective { lines =>
                              val userData = mutable.Map.empty[String, UserData]
                              lines.zipWithIndex.toList.sequentialTraverse {
                                case (line, index) =>
                                  val lineNumber = index + 2
                                  val trimLine = line.map {
                                    case (key, value) => (key.trim, value.trim)
                                  }
                                  val validation =
                                    validateLine(trimLine, userData, question.languages.toList, lineNumber)
                                  validation match {
                                    case Valid(
                                        (content, _, originalLanguage, translations, firstName, age, externalUserId, _)
                                        ) =>
                                      val warning =
                                        getDuplicateLanguageWarning(translations, originalLanguage.value, lineNumber)
                                      for {
                                        user <- userService
                                          .getUserByEmail(
                                            s"${question.questionId.value}-${externalUserId.value}@example.com"
                                          )
                                        count <- user.fold(Future.successful(0L))(
                                          u =>
                                            elasticsearchProposalAPI
                                              .countProposals(
                                                SearchQuery(filters = SearchFilters(
                                                  content = ContentSearchFilter(content.value).some,
                                                  question = QuestionSearchFilter(Seq(questionId)).some,
                                                  users = UserSearchFilter(Seq(u.userId)).some
                                                ).some
                                                )
                                              )
                                        )
                                        result <- count match {
                                          case 0L =>
                                            proposalService
                                              .createExternalProposal(
                                                content.value,
                                                contentTranslations = translations,
                                                question = question,
                                                country = question.countries.head,
                                                submittedAsLanguage = Language(originalLanguage.value),
                                                isAnonymous = request.anonymous,
                                                externalUserId = externalUserId.value,
                                                author = AuthorRequest(
                                                  firstName = firstName.value,
                                                  age = age,
                                                  lastName = None,
                                                  postalCode = None,
                                                  profession = None
                                                ),
                                                moderator = userAuth.user.userId,
                                                moderatorRequestContext = requestContext,
                                                lineNumber = lineNumber
                                              )
                                              .as((validation, warning))
                                          case _ => Future.successful((validation, None))
                                        }
                                      } yield result
                                    case _ => Future.successful((validation, None))
                                  }
                              }.asDirective { res =>
                                val (validations, warnings) = res.unzip
                                validations.sequence.throwIfInvalid()
                                complete(StatusCodes.Accepted -> warnings.flatten)
                              }
                            }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def listQuestions: Route = get {
      path("moderation" / "questions") {
        makeOperation("ModerationSearchQuestion") { _ =>
          parameters(
            "slug".?,
            "operationId".as[OperationId].?,
            "country".as[Country].?,
            "language".as[Language].?,
            "_start".as[Pagination.Offset].?,
            "_end".as[Pagination.End].?,
            "_sort".?,
            "_order".as[Order].?
          ) { (maybeSlug, operationId, country, language, offset, end, sort, order) =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              requireModerationRole(userAuth.user) {
                val questionIds: Option[Seq[QuestionId]] = if (userAuth.user.roles.contains(RoleAdmin)) {
                  None
                } else {
                  Some(userAuth.user.availableQuestions)
                }
                val request: SearchQuestionRequest = SearchQuestionRequest(
                  maybeQuestionIds = questionIds,
                  maybeOperationIds = operationId.map(op => Seq(op)),
                  country = country,
                  language = language,
                  maybeSlug = maybeSlug,
                  offset = offset,
                  end = end,
                  sort = sort,
                  order = order
                )
                val searchResults: Future[(Int, Seq[Question])] =
                  questionService.countQuestion(request).flatMap { count =>
                    questionService.searchQuestion(request).map(results => count -> results)
                  }

                onSuccess(searchResults) {
                  case (count, results) =>
                    complete(
                      (
                        StatusCodes.OK,
                        Seq(`X-Total-Count`(count.toString)),
                        results.map(ModerationQuestionResponse.apply)
                      )
                    )
                }
              }
            }
          }
        }
      }
    }

    override def getQuestion: Route = get {
      path("moderation" / "questions" / questionId) { questionId =>
        makeOperation("ModerationGetQuestion") { _ =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireModerationRole(userAuth.user) {
              questionService.getQuestion(questionId).asDirectiveOrNotFound { question =>
                complete(ModerationQuestionResponse(question))
              }
            }
          }
        }
      }
    }

    override def uploadQuestionImage: Route = {
      post {
        path("moderation" / "questions" / questionId / "image") { questionId =>
          makeOperation("uploadQuestionConsultationImage") { _ =>
            makeOAuth2 { user =>
              requireAdminRole(user.user) {
                operationOfQuestionService.findByQuestionId(questionId).asDirectiveOrNotFound { operationOfQuestion =>
                  operationService.findOneSimple(operationOfQuestion.operationId).asDirectiveOrNotFound { operation =>
                    def uploadFile(extension: String, contentType: String, fileContent: Content): Future[String] = {
                      storageService
                        .uploadImage(
                          FileType.Operation,
                          s"${operation.slug}/${idGenerator.nextId()}$extension",
                          contentType,
                          fileContent
                        )
                    }
                    uploadImageAsync("data", uploadFile, sizeLimit = None) { (path, file) =>
                      file.delete()
                      complete(UploadResponse(path))
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def uploadReport: Route = {
      post {
        path("moderation" / "questions" / questionId / "report") { questionId =>
          makeOperation("uploadConsultationReport") { _ =>
            makeOAuth2 { user =>
              requireAdminRole(user.user) {
                operationOfQuestionService.findByQuestionId(questionId).asDirectiveOrNotFound { operationOfQuestion =>
                  operationService.findOneSimple(operationOfQuestion.operationId).asDirectiveOrNotFound { operation =>
                    def uploadFile(name: String, contentType: String, fileContent: Content): Future[String] = {
                      storageService
                        .uploadReport(
                          FileType.Report,
                          s"${operation.slug}_${idGenerator.nextId()}_$name",
                          contentType,
                          fileContent
                        )
                    }
                    uploadPDFAsync("data", uploadFile) { (path, file) =>
                      file.delete()
                      complete(UploadResponse(path))
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

final case class CreateInitialProposalRequest(
  content: String Refined (
    MinSize[FrontConfiguration.defaultProposalMinLength]
      And MaxSize[BusinessConfig.defaultProposalMaxLength]
      And ValidHtml
  ),
  contentTranslations: Option[Multilingual[
    String Refined (
      MinSize[FrontConfiguration.defaultProposalMinLength]
        And MaxSize[BusinessConfig.defaultProposalTranslationMaxLength]
        And ValidHtml
    )
  ]],
  @(ApiModelProperty @field)(dataType = "string", example = "FR", required = true)
  country: Country,
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  submittedAsLanguage: Language,
  author: AuthorRequest,
  @(ApiModelProperty @field)(dataType = "list[string]", required = true) tags: Seq[TagId]
)

object CreateInitialProposalRequest {
  implicit val decoder: Decoder[CreateInitialProposalRequest] = deriveDecoder[CreateInitialProposalRequest]
}

final case class InjectProposalsRequest(anonymous: Boolean)

object InjectProposalsRequest {
  implicit val decoder: Decoder[InjectProposalsRequest] = deriveDecoder[InjectProposalsRequest]
}
