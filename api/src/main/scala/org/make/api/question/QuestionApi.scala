/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.question

import cats.implicits._
import akka.http.scaladsl.server._
import akka.http.scaladsl.unmarshalling.Unmarshaller._
import com.sksamuel.elastic4s.requests.searches.suggestion.Fuzziness
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._
import eu.timepit.refined.numeric._
import grizzled.slf4j.Logging
import io.circe.refined._
import io.swagger.annotations._
import org.make.api.demographics.{ActiveDemographicsCardServiceComponent, DemographicsCardServiceComponent}
import org.make.api.feature.{ActiveFeatureServiceComponent, FeatureServiceComponent}
import org.make.api.idea.topIdeaComments.TopIdeaCommentServiceComponent
import org.make.api.keyword.KeywordServiceComponent
import org.make.api.operation.{
  OperationOfQuestionSearchEngineComponent,
  OperationOfQuestionServiceComponent,
  OperationServiceComponent
}
import org.make.api.organisation.{OrganisationServiceComponent, OrganisationsSearchResultResponse}
import org.make.api.partner.PartnerServiceComponent
import org.make.api.personality.PersonalityRoleServiceComponent
import org.make.api.proposal.{
  ProposalResponse,
  ProposalSearchEngineComponent,
  ProposalServiceComponent,
  ProposalsResultResponse,
  ProposalsResultSeededResponse
}
import org.make.api.sequence.{SequenceConfigurationComponent, SequenceServiceComponent}
import org.make.api.tag.TagServiceComponent
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.{EndpointType, MakeAuthenticationDirectives}
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.core.Validation.validateField
import org.make.core.auth.UserRights
import org.make.core.technical.Pagination
import org.make.core.feature.FeatureSlug
import org.make.core.feature.FeatureSlug.DisplayIntroCardWidget
import org.make.core.idea.TopIdeaId
import org.make.core.keyword.Keyword
import org.make.core.operation._
import org.make.core.operation.indexed.{IndexedOperationOfQuestion, OperationOfQuestionElasticsearchFieldName}
import org.make.core.partner.PartnerKind
import org.make.core.personality.PersonalityRoleId
import org.make.core.proposal.{
  MinScoreLowerBoundSearchFilter,
  PopularAlgorithm,
  QuestionSearchFilter,
  SearchFilters,
  SearchQuery,
  SequencePoolSearchFilter,
  ZoneSearchFilter
}
import org.make.core.proposal.indexed.{SequencePool, Zone}
import org.make.core.question.{Question, QuestionId, TopProposalsMode}
import org.make.core.reference.{Country, Language}
import org.make.core.user.{CountrySearchFilter => _, DescriptionSearchFilter => _, LanguageSearchFilter => _, _}
import org.make.core.{HttpCodes, Order, ParameterExtractors, RequestContext, Validation}
import scalaoauth2.provider.AuthInfo

import javax.ws.rs.Path
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait QuestionApiComponent {
  def questionApi: QuestionApi
}

@Api(value = "Questions")
@Path(value = "/questions")
trait QuestionApi extends Directives {

  @ApiOperation(value = "get-question-details", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionSlugOrQuestionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "preferredLanguage", paramType = "query", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[QuestionDetailsResponse]))
  )
  @Path(value = "/{questionSlugOrQuestionId}/details")
  def questionDetails: Route

  @ApiOperation(value = "get-search-question", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionIds", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "questionContent", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "description", paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "operationKinds",
        paramType = "query",
        dataType = "string",
        allowableValues = OperationKind.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "language", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "preferredLanguage", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "country", paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "skip",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "sort",
        paramType = "query",
        dataType = "string",
        allowableValues = OperationOfQuestionElasticsearchFieldName.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[SearchQuestionsResponse]))
  )
  @Path(value = "/search")
  def searchQuestions: Route

  @ApiOperation(value = "get-question-popular-tags", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(name = "skip", paramType = "query", dataType = "int", allowableValues = "range[0, infinity]")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[PopularTagResponse]]))
  )
  @Path(value = "/{questionId}/popular-tags")
  def getPopularTags: Route

  @ApiOperation(value = "get-top-proposals", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "limit", paramType = "query", dataType = "int"),
      new ApiImplicitParam(
        name = "mode",
        paramType = "query",
        dataType = "string",
        allowableValues = TopProposalsMode.swaggerAllowableValues,
        allowEmptyValue = true
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ProposalsResultResponse]))
  )
  @Path(value = "/{questionId}/top-proposals")
  def getTopProposals: Route

  @ApiOperation(value = "get-question-partners", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        name = "sortAlgorithm",
        paramType = "query",
        dataType = "string",
        allowableValues = OrganisationAlgorithmSelector.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "partnerKind",
        paramType = "query",
        dataType = "string",
        allowableValues = PartnerKind.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(name = "skip", paramType = "query", dataType = "int", allowableValues = "range[0, infinity]")
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[OrganisationsSearchResultResponse]))
  )
  @Path(value = "/{questionId}/partners")
  def getPartners: Route

  @ApiOperation(value = "get-question-personalities", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "personalityRoleId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(name = "skip", paramType = "query", dataType = "int", allowableValues = "range[0, infinity]")
    )
  )
  @ApiResponses(
    value = Array(
      new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[QuestionPersonalityResponseWithTotal])
    )
  )
  @Path(value = "/{questionId}/personalities")
  def getPersonalities: Route

  @ApiOperation(value = "get-question-top-ideas", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "skip",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(name = "seed", paramType = "query", dataType = "string")
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[QuestionTopIdeasResponseWithSeed]))
  )
  @Path(value = "/{questionId}/top-ideas")
  def getTopIdeas: Route

  @ApiOperation(value = "get-question-top-idea", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "topIdeaId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "seed", paramType = "query", dataType = "string")
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[QuestionTopIdeaResponseWithSeed]))
  )
  @Path(value = "/{questionId}/top-ideas/{topIdeaId}")
  def getTopIdea: Route

  @ApiOperation(value = "list-questions", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "country", paramType = "query", dataType = "string", example = "FR", required = true),
      new ApiImplicitParam(name = "language", paramType = "query", dataType = "string", example = "fr"),
      new ApiImplicitParam(name = "preferredLanguage", paramType = "query", dataType = "string", example = "fr"),
      new ApiImplicitParam(
        name = "status",
        paramType = "query",
        dataType = "string",
        allowableValues = OperationOfQuestion.Status.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "skip",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "sortAlgorithm",
        paramType = "query",
        dataType = "string",
        allowableValues = SortAlgorithm.swaggerAllowableValues
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[QuestionListResponse]))
  )
  @Path(value = "/")
  def listQuestions: Route

  @ApiOperation(value = "featured-proposals", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        name = "maxPartnerProposals",
        paramType = "query",
        dataType = "int",
        required = true,
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(name = "preferredLanguage", paramType = "query", dataType = "string", example = "fr"),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        required = true,
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(name = "seed", paramType = "query", dataType = "int")
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ProposalsResultSeededResponse]))
  )
  @Path(value = "/{questionId}/featured-proposals")
  def featuredProposals: Route

  @ApiOperation(value = "get-keywords", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        required = true,
        allowableValues = "range[0, infinity]"
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[Keyword]])))
  @Path(value = "/{questionId}/keywords")
  def getKeywords: Route

  def routes: Route =
    questionDetails ~ searchQuestions ~ getPopularTags ~ getTopProposals ~ getPartners ~
      getPersonalities ~ getTopIdeas ~ getTopIdea ~ listQuestions ~ featuredProposals ~ getKeywords
}

trait DefaultQuestionApiComponent
    extends QuestionApiComponent
    with SequenceServiceComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {

  this: MakeDirectivesDependencies
    with DemographicsCardServiceComponent
    with ActiveDemographicsCardServiceComponent
    with ActiveFeatureServiceComponent
    with FeatureServiceComponent
    with KeywordServiceComponent
    with OperationOfQuestionSearchEngineComponent
    with OperationOfQuestionServiceComponent
    with OperationServiceComponent
    with OrganisationServiceComponent
    with PartnerServiceComponent
    with PersonalityRoleServiceComponent
    with ProposalSearchEngineComponent
    with ProposalServiceComponent
    with QuestionServiceComponent
    with SequenceConfigurationComponent
    with TagServiceComponent
    with TopIdeaCommentServiceComponent =>

  override lazy val questionApi: QuestionApi = new DefaultQuestionApi

  class DefaultQuestionApi extends QuestionApi {

    private val questionId: PathMatcher1[QuestionId] = Segment.map(id => QuestionId(id))
    private val topIdeaId: PathMatcher1[TopIdeaId] = Segment.map(id   => TopIdeaId(id))
    private val questionSlugOrQuestionId: PathMatcher1[String] = Segment

    private def zoneCount(zone: Zone, indexedOoq: IndexedOperationOfQuestion, question: Question): Future[Long] =
      elasticsearchProposalAPI.countProposals(
        SearchQuery(filters = Some(
          SearchFilters(
            minScoreLowerBound = Option
              .when(zone == Zone.Consensus)(indexedOoq.top20ConsensusThreshold.map(MinScoreLowerBoundSearchFilter))
              .flatten,
            question = Some(QuestionSearchFilter(Seq(question.questionId))),
            sequencePool = Some(SequencePoolSearchFilter(SequencePool.Tested)),
            zone = Some(ZoneSearchFilter(zone))
          )
        )
        )
      )

    // TODO: remove the public access once authent is handled in server side
    override def questionDetails: Route = get {
      path("questions" / questionSlugOrQuestionId / "details") { questionSlugOrQuestionId =>
        makeOperation("GetQuestionDetails", EndpointType.Public) { _ =>
          parameters("preferredLanguage".as[Language].?) { maybePreferredLanguage =>
            questionService
              .getQuestionByQuestionIdValueOrSlug(questionSlugOrQuestionId)
              .asDirectiveOrNotFound
              .flatMap(
                question =>
                  (
                    operationOfQuestionService
                      .findByQuestionId(question.questionId)
                      .asDirectiveOrNotFound,
                    elasticsearchOperationOfQuestionAPI
                      .findOperationOfQuestionById(question.questionId)
                      .asDirectiveOrNotFound,
                    partnerService
                      .find(
                        questionId = Some(question.questionId),
                        organisationId = None,
                        offset = Pagination.Offset.zero,
                        end = None,
                        sort = Some("weight"),
                        order = Some(Order.desc),
                        partnerKind = None
                      )
                      .asDirective,
                    findActiveFeatureSlugsByQuestionId(question.questionId).asDirective
                  ).tupled.flatMap({
                    case (operationOfQuestion, indexedOoq, partners, activeFeatureSlugs) =>
                      val returnedLanguage =
                        maybePreferredLanguage
                          .filter(question.languages.toList.contains)
                          .getOrElse(question.defaultLanguage)
                      val demographicsCardCountF =
                        if (operationOfQuestion.sessionBindingMode)
                          demographicsCardService.count(None, None, sessionBindingMode = Some(true))
                        else
                          activeDemographicsCardService
                            .count(questionId = Some(question.questionId), cardId = None)
                            .map(c => if (c > 0) 1 else 0)
                      (
                        operationService.findOne(operationOfQuestion.operationId).asDirectiveOrNotFound,
                        findActiveFeatureData(question, activeFeatureSlugs, maybePreferredLanguage).asDirective,
                        organisationService
                          .find(
                            offset = Pagination.Offset.zero,
                            end = Some(Pagination.End(partners.length)),
                            sort = None,
                            order = None,
                            ids = Some(partners.flatMap(_.organisationId)),
                            organisationName = None
                          )
                          .asDirective,
                        zoneCount(Zone.Consensus, indexedOoq, question).asDirective,
                        zoneCount(Zone.Controversy, indexedOoq, question).asDirective,
                        demographicsCardCountF.asDirective
                      ).mapN(
                        (
                          operation,
                          activeFeaturesData,
                          organisations,
                          consensusCount,
                          controversyCount,
                          demographicsCardCount
                        ) =>
                          QuestionDetailsResponse(
                            question,
                            operation,
                            operationOfQuestion,
                            partners,
                            organisations,
                            activeFeatureSlugs,
                            controversyCount,
                            consensusCount,
                            activeFeaturesData,
                            demographicsCardCount,
                            maybePreferredLanguage,
                            returnedLanguage
                          )
                      )
                  })
              )
              .apply(complete(_))
          }
        }
      }
    }

    private def findActiveFeatureSlugsByQuestionId(questionId: QuestionId): Future[Seq[FeatureSlug]] = {
      activeFeatureService.find(maybeQuestionId = Some(Seq(questionId))).flatMap { activeFeatures =>
        featureService.findByFeatureIds(activeFeatures.map(_.featureId)).map(_.map(_.slug))
      }
    }

    private def findActiveFeatureData(
      question: Question,
      features: Seq[FeatureSlug],
      preferredLanguage: Option[Language]
    ): Future[ActiveFeatureData] = {
      val futureTopProposal: Future[Option[ProposalResponse]] = if (features.contains(DisplayIntroCardWidget)) {
        proposalService
          .searchInIndex(
            query = SearchQuery(
              limit = Some(Pagination.Limit(1)),
              sortAlgorithm = Some(PopularAlgorithm),
              filters = Some(SearchFilters(question = Some(QuestionSearchFilter(Seq(question.questionId)))))
            ),
            requestContext = RequestContext.empty
          )
          .map(_.results)
          .flatMap {
            case Seq() => Future.successful(None)
            case proposal +: _ =>
              val futureThreshold: Future[Int] = proposal.question.map(_.questionId) match {
                case Some(questionId) =>
                  sequenceConfigurationService
                    .getSequenceConfigurationByQuestionId(questionId)
                    .map(_.newProposalsVoteThreshold)
                case None => Future.successful(0)
              }

              futureThreshold.map { newProposalsVoteThreshold =>
                Some(
                  ProposalResponse(
                    indexedProposal = proposal,
                    myProposal = false,
                    voteAndQualifications = None,
                    proposalKey = "not-votable",
                    newProposalsVoteThreshold = newProposalsVoteThreshold,
                    preferredLanguage = preferredLanguage,
                    questionDefaultLanguage = Some(question.defaultLanguage)
                  )
                )
              }
          }
      } else {
        Future.successful(None)
      }

      for {
        topProposal <- futureTopProposal
      } yield ActiveFeatureData(topProposal)
    }

    override def searchQuestions: Route = get {
      path("questions" / "search") {
        makeOperation("SearchQuestion") { _ =>
          parameters(
            "questionIds".as[Seq[QuestionId]].?,
            "questionContent".?,
            "description".?,
            "operationKinds".as[Seq[OperationKind]].?,
            "preferredLanguage".as[Language].?,
            "language".as[Language].?,
            "country".as[Country].?,
            "limit".as[Pagination.Limit].?,
            "skip".as[Pagination.Offset].?,
            "sort".as[OperationOfQuestionElasticsearchFieldName].?,
            "order".as[Order].?
          ) {
            (
              questionIds: Option[Seq[QuestionId]],
              questionContent: Option[String],
              description: Option[String],
              operationKinds: Option[Seq[OperationKind]],
              maybePreferredLanguage: Option[Language],
              language: Option[Language],
              country: Option[Country],
              limit: Option[Pagination.Limit],
              offset: Option[Pagination.Offset],
              sort: Option[OperationOfQuestionElasticsearchFieldName],
              order: Option[Order]
            ) =>
              val filters: Option[OperationOfQuestionSearchFilters] = Some(
                OperationOfQuestionSearchFilters(
                  questionIds = questionIds.map(QuestionIdsSearchFilter.apply),
                  question = questionContent.map(QuestionContentSearchFilter(_, Some(Fuzziness.Auto))),
                  description = description.map(DescriptionSearchFilter.apply),
                  country = country.map(CountrySearchFilter.apply),
                  language = language.map(LanguageSearchFilter.apply),
                  operationKinds = operationKinds.map(OperationKindsSearchFilter.apply)
                )
              )
              val searchQuery: OperationOfQuestionSearchQuery =
                OperationOfQuestionSearchQuery(
                  filters = filters,
                  limit = limit,
                  offset = offset,
                  sort = sort,
                  order = order
                )
              operationOfQuestionService.search(searchQuery).asDirective { searchResult =>
                complete(
                  SearchQuestionsResponse(
                    total = searchResult.total,
                    results = searchResult.results.map(SearchQuestionResponse.apply(_, maybePreferredLanguage))
                  )
                )
              }
          }
        }

      }
    }

    override def getPopularTags: Route = get {
      path("questions" / questionId / "popular-tags") { questionId =>
        makeOperation("GetQuestionPopularTags") { _ =>
          parameters("limit".as[Int].?, "skip".as[Int].?) { (limit: Option[Int], offset: Option[Int]) =>
            questionService.getCachedQuestion(questionId).asDirectiveOrNotFound { _ =>
              val offsetOrZero = offset.getOrElse(0)
              val size = limit.fold(Int.MaxValue)(_ + offsetOrZero)

              elasticsearchProposalAPI.getPopularTagsByProposal(questionId, size).asDirective { popularTagsResponse =>
                val popularTags = popularTagsResponse.sortBy(_.proposalCount * -1).drop(offsetOrZero)
                complete(popularTags)
              }
            }
          }
        }
      }
    }

    override def getTopProposals: Route = get {
      path("questions" / questionId / "top-proposals") { questionId =>
        makeOperation("GetTopProposals") { requestContext =>
          parameters("limit".as[Int].?, "mode".as[TopProposalsMode].?) {
            (limit: Option[Int], mode: Option[TopProposalsMode]) =>
              optionalMakeOAuth2 { userAuth: Option[AuthInfo[UserRights]] =>
                questionService.getCachedQuestion(questionId).asDirectiveOrNotFound { _ =>
                  proposalService
                    .getTopProposals(
                      maybeUserId = userAuth.map(_.user.userId),
                      questionId = questionId,
                      size = limit.getOrElse(10),
                      mode = mode,
                      requestContext = requestContext
                    )
                    .asDirective
                    .apply(complete(_))
                }
              }
          }
        }
      }
    }

    override def getPartners: Route = get {
      path("questions" / questionId / "partners") { questionId =>
        makeOperation("GetQuestionPartners") { _ =>
          parameters(
            "sortAlgorithm".as[OrganisationAlgorithmSelector].?,
            "partnerKind".as[PartnerKind].?,
            "limit".as[Pagination.Limit].?,
            "skip".as[Pagination.Offset].?
          ) {
            (
              sortAlgorithm: Option[OrganisationAlgorithmSelector],
              partnerKind: Option[PartnerKind],
              limit: Option[Pagination.Limit],
              offset: Option[Pagination.Offset]
            ) =>
              questionService.getCachedQuestion(questionId).asDirectiveOrNotFound { _ =>
                partnerService
                  .find(
                    offset = Pagination.Offset.zero,
                    end = Some(Pagination.End(1000)),
                    sort = None,
                    order = None,
                    questionId = Some(questionId),
                    organisationId = None,
                    partnerKind = partnerKind
                  )
                  .asDirective { partners =>
                    val organisationsIds = partners.flatMap(_.organisationId)
                    val sortAlgo = sortAlgorithm.flatMap(_.select(Some(questionId)))
                    questionService.getPartners(questionId, organisationsIds, sortAlgo, limit, offset).asDirective {
                      results =>
                        complete(OrganisationsSearchResultResponse.fromOrganisationSearchResult(results))
                    }
                  }
              }
          }
        }
      }
    }

    override def getPersonalities: Route = {
      get {
        path("questions" / questionId / "personalities") { questionId =>
          makeOperation("GetQuestionPersonalities") { _ =>
            parameters("personalityRole".as[String].?, "limit".as[Pagination.Limit].?, "skip".as[Pagination.Offset].?) {
              (personalityRole: Option[String], limit: Option[Pagination.Limit], offset: Option[Pagination.Offset]) =>
                personalityRoleService
                  .find(
                    offset = Pagination.Offset.zero,
                    end = None,
                    sort = None,
                    order = None,
                    roleIds = None,
                    name = personalityRole
                  )
                  .asDirective { roles =>
                    val personalityRoleId: Option[PersonalityRoleId] = roles match {
                      case Seq(role) if personalityRole.nonEmpty => Some(role.personalityRoleId)
                      case _                                     => None
                    }
                    Validation.validate(
                      Validation.validateField(
                        field = "personalityRole",
                        key = "invalid_content",
                        condition = personalityRole.forall(_ => personalityRoleId.isDefined),
                        message = s"$personalityRole is not a valid personality role"
                      )
                    )
                    questionService
                      .getQuestionPersonalities(
                        offset = offset.orZero,
                        end = limit.map(_.toEnd(offset.orZero)),
                        questionId = questionId,
                        personalityRoleId = personalityRoleId
                      )
                      .asDirective { questionPersonalities =>
                        val response = QuestionPersonalityResponseWithTotal(
                          total = questionPersonalities.size,
                          results = questionPersonalities.sortBy(_.lastName)
                        )
                        complete(response)
                      }
                  }
            }
          }
        }
      }
    }

    override def getTopIdeas: Route = {
      get {
        path("questions" / questionId / "top-ideas") { questionId =>
          makeOperation("GetQuestionTopIdeas") { _ =>
            parameters("limit".as[Pagination.Limit].?, "skip".as[Pagination.Offset].?, "seed".as[Int].?) {
              (limit: Option[Pagination.Limit], offset: Option[Pagination.Offset], seed: Option[Int]) =>
                questionService
                  .getTopIdeas(offset.orZero, limit.map(_.toEnd(offset.orZero)), seed, questionId)
                  .asDirective
                  .apply(complete(_))
            }
          }
        }
      }
    }

    override def getTopIdea: Route = {
      get {
        path("questions" / questionId / "top-ideas" / topIdeaId) { (questionId, topIdeaId) =>
          makeOperation("GetQuestionTopIdea") { _ =>
            parameters("seed".as[Int].?) { (seed: Option[Int]) =>
              questionService.getTopIdea(topIdeaId, questionId, seed).asDirectiveOrNotFound { topIdeaResult =>
                topIdeaCommentService
                  .getCommentsWithPersonality(topIdeaIds = Seq(topIdeaResult.topIdea.topIdeaId))
                  .asDirective { comments =>
                    val response = QuestionTopIdeaResponseWithSeed(
                      questionTopIdea = QuestionTopIdeaWithAvatarAndCommentsResponse(
                        id = topIdeaId,
                        ideaId = topIdeaResult.topIdea.ideaId,
                        questionId = topIdeaResult.topIdea.questionId,
                        name = topIdeaResult.topIdea.name,
                        label = topIdeaResult.topIdea.label,
                        scores = topIdeaResult.topIdea.scores,
                        proposalsCount = topIdeaResult.proposalsCount,
                        avatars = topIdeaResult.avatars,
                        weight = topIdeaResult.topIdea.weight,
                        comments = comments
                      ),
                      seed = topIdeaResult.seed
                    )
                    complete(response)
                  }
              }
            }
          }
        }
      }
    }

    override def listQuestions: Route = {
      get {
        path("questions") {
          makeOperation("ListQuestions") { _ =>
            parameters(
              "preferredLanguage".as[Language].?,
              "country".as[Country],
              "language".as[Language].?,
              "status".as[OperationOfQuestion.Status].?,
              "limit".as[Pagination.Limit].?,
              "skip".as[Pagination.Offset].?,
              "sortAlgorithm".as[SortAlgorithm].?
            ) {
              (
                maybePreferredLanguage: Option[Language],
                country: Country,
                maybeLanguage: Option[Language],
                maybeStatus: Option[OperationOfQuestion.Status],
                limit: Option[Pagination.Limit],
                offset: Option[Pagination.Offset],
                sortAlgorithm: Option[SortAlgorithm]
              ) =>
                val filters = OperationOfQuestionSearchFilters(
                  country = Some(CountrySearchFilter(country)),
                  language = maybeLanguage.map(LanguageSearchFilter.apply),
                  status = maybeStatus.map(status => StatusSearchFilter(status))
                )
                operationOfQuestionService
                  .search(
                    OperationOfQuestionSearchQuery(
                      filters = Some(filters),
                      limit = limit,
                      offset = offset,
                      sortAlgorithm = sortAlgorithm
                    )
                  )
                  .asDirective { operationOfQuestions =>
                    complete(
                      QuestionListResponse(
                        results =
                          operationOfQuestions.results.map(QuestionOfOperationResponse.apply(_, maybePreferredLanguage)),
                        total = operationOfQuestions.total
                      )
                    )
                  }
            }
          }
        }
      }
    }

    override def featuredProposals: Route = get {
      path("questions" / questionId / "featured-proposals") { questionId =>
        makeOperation("GetFeaturedProposals") { requestContext =>
          parameters(
            "maxPartnerProposals".as[Int Refined NonNegative],
            "preferredLanguage".as[Language].?,
            "limit".as[Int Refined Positive],
            "seed".as[Int].?
          ) { (maxPartnerProposals, preferredLanguage, limit, seed) =>
            Validation.validate(
              validateField(
                "maxPartnerProposals",
                "invalid_value",
                maxPartnerProposals.value <= limit.value,
                "maxPartnerProposals should be lower or equal to limit"
              )
            )
            optionalMakeOAuth2 { userAuth: Option[AuthInfo[UserRights]] =>
              questionService.getCachedQuestion(questionId).asDirectiveOrNotFound { question =>
                proposalService
                  .questionFeaturedProposals(
                    questionId = questionId,
                    maxPartnerProposals = maxPartnerProposals.value,
                    preferredLanguage = preferredLanguage,
                    questionDefaultLanguage = Some(question.defaultLanguage),
                    limit = new Pagination.Limit(limit),
                    seed = seed,
                    maybeUserId = userAuth.map(_.user.userId),
                    requestContext = requestContext
                  )
                  .asDirective
                  .apply(complete(_))
              }
            }
          }
        }
      }
    }

    override def getKeywords: Route = get {
      path("questions" / questionId / "keywords") { questionId =>
        makeOperation("GetKeywords") { _ =>
          parameters("limit".as[Int]) { (limit: Int) =>
            questionService.getCachedQuestion(questionId).asDirectiveOrNotFound { _ =>
              keywordService
                .findTop(questionId, Pagination.Limit(limit))
                .asDirective
                .apply(complete(_))
            }
          }
        }
      }
    }
  }
}
