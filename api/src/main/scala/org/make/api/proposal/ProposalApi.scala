/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.proposal

import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import akka.http.scaladsl.unmarshalling.Unmarshaller.CsvSeq
import cats.data.Validated.{Invalid, Valid}
import cats.implicits._
import grizzled.slf4j.Logging
import io.swagger.annotations._
import org.make.api.operation.OperationServiceComponent
import org.make.api.question.QuestionServiceComponent
import org.make.api.sequence.SequenceConfigurationComponent
import org.make.api.technical.CsvReceptacle._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.technical.{EventBusServiceComponent, MakeAuthenticationDirectives}
import org.make.api.user.UserServiceComponent
import org.make.core.Validation._
import org.make.core.auth.UserRights
import org.make.core.idea.IdeaId
import org.make.core.operation.{OperationId, OperationKind, SortAlgorithm}
import org.make.core.proposal._
import org.make.core.proposal.indexed.ProposalElasticsearchFieldName
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import org.make.core.tag.TagId
import org.make.core.technical.Pagination
import org.make.core.user.UserType
import org.make.core._
import scalaoauth2.provider.AuthInfo

import javax.ws.rs.Path
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Api(value = "Proposal")
@Path(value = "/proposals")
trait ProposalApi extends Directives {

  @ApiOperation(value = "get-proposal", httpMethod = "GET", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ProposalResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "preferredLanguage", paramType = "query", dataType = "string", example = "fr")
    )
  )
  @Path(value = "/{proposalId}")
  def getProposal: Route

  @ApiOperation(value = "search-proposals", httpMethod = "GET", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ProposalsResultResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "proposalIds", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "questionId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "tagsIds", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "operationId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "content", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "slug", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "seed", paramType = "query", dataType = "int"),
      new ApiImplicitParam(name = "source", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "context", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "location", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "question", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "language", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "country", paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "sort",
        paramType = "query",
        dataType = "string",
        allowableValues = ProposalElasticsearchFieldName.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "skip",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(name = "isRandom", paramType = "query", dataType = "boolean"),
      new ApiImplicitParam(
        name = "sortAlgorithm",
        paramType = "query",
        dataType = "string",
        allowableValues = SortAlgorithm.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "operationKinds",
        paramType = "query",
        dataType = "string",
        allowableValues = OperationKind.swaggerAllowableValues,
        allowMultiple = true
      ),
      new ApiImplicitParam(
        name = "userType",
        paramType = "query",
        dataType = "string",
        allowableValues = UserType.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "ideaIds", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "keywords", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "isNotVoted", paramType = "query", dataType = "boolean")
    )
  )
  def search: Route

  @ApiOperation(
    value = "propose-proposal",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "user", description = "application user"),
          new AuthorizationScope(scope = "admin", description = "BO Admin")
        )
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.proposal.ProposeProposalRequest"
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.Created, message = "Ok", response = classOf[ProposalIdResponse]))
  )
  def postProposal: Route

  @ApiOperation(value = "vote-proposal", httpMethod = "POST", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.proposal.VoteProposalRequest")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[VoteResponse])))
  @Path(value = "/{proposalId}/vote")
  def vote: Route

  @ApiOperation(value = "unvote-proposal", httpMethod = "POST", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.proposal.VoteProposalRequest")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[VoteResponse])))
  @Path(value = "/{proposalId}/unvote")
  def unvote: Route

  @ApiOperation(value = "qualification-vote", httpMethod = "POST", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.proposal.QualificationProposalRequest"
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[QualificationResponse]))
  )
  @Path(value = "/{proposalId}/qualification")
  def qualification: Route

  @ApiOperation(value = "unqualification-vote", httpMethod = "POST", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.proposal.QualificationProposalRequest"
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[QualificationResponse]))
  )
  @Path(value = "/{proposalId}/unqualification")
  def unqualification: Route

  @ApiOperation(value = "report-proposal", httpMethod = "POST", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.proposal.ReportProposalRequest")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "Ok")))
  @Path(value = "/{proposalId}/report")
  def reportProposal: Route

  def routes: Route =
    postProposal ~
      getProposal ~
      search ~
      vote ~
      unvote ~
      qualification ~
      unqualification ~
      reportProposal

}

trait ProposalApiComponent {
  def proposalApi: ProposalApi
}

trait DefaultProposalApiComponent
    extends ProposalApiComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {

  this: MakeDirectivesDependencies
    with EventBusServiceComponent
    with ProposalServiceComponent
    with UserServiceComponent
    with OperationServiceComponent
    with SequenceConfigurationComponent
    with QuestionServiceComponent =>

  override lazy val proposalApi: DefaultProposalApi = new DefaultProposalApi

  class DefaultProposalApi extends ProposalApi {
    val proposalId: PathMatcher1[ProposalId] =
      Segment.map(id => ProposalId(id))

    def getProposal: Route = {
      get {
        path("proposals" / proposalId) { proposalId =>
          makeOperation("GetProposal") { requestContext =>
            parameters("preferredLanguage".as[Language].?) {
              (
                preferredLanguage: Option[Language]
              ) =>
                proposalService
                  .getProposalById(proposalId, requestContext)
                  .asDirectiveOrNotFound
                  .flatMap(
                    proposal =>
                      (
                        proposal.question
                          .fold(Future.successful(Option.empty[Question]))(
                            question => questionService.getQuestion(question.questionId)
                          )
                          .asDirectiveOrNotFound,
                        proposal.question
                          .fold(Future.successful(Option.empty[Int]))(
                            question =>
                              sequenceConfigurationService
                                .getSequenceConfigurationByQuestionId(question.questionId)
                                .map(config => Some(config.newProposalsVoteThreshold))
                          )
                          .asDirectiveOrBadRequest(
                            ValidationError(
                              "questionId",
                              "not_found",
                              Some(s"Question not found for proposal $proposalId.")
                            )
                          ),
                        sessionHistoryCoordinatorService
                          .retrieveVoteAndQualifications(requestContext.sessionId, Seq(proposalId))
                          .asDirective
                      ).mapN((question, newProposalsVoteThreshold, votes) => {
                        val proposalKey =
                          proposalService.generateProposalKeyHash(
                            proposalId,
                            requestContext.sessionId,
                            requestContext.location,
                            securityConfiguration.secureVoteSalt
                          )
                        ProposalResponse(
                          proposal,
                          requestContext.userId.contains(proposal.author.userId),
                          votes.get(proposalId),
                          proposalKey,
                          newProposalsVoteThreshold,
                          preferredLanguage,
                          Some(question.defaultLanguage)
                        )
                      })
                  )
                  .apply(complete(_))
            }
          }
        }
      }
    }

    def search: Route = {
      get {
        path("proposals") {
          makeOperation("Search") { requestContext =>
            optionalMakeOAuth2 { userAuth: Option[AuthInfo[UserRights]] =>
              parameters(
                "proposalIds".as[Seq[ProposalId]].?,
                "questionId".as[Seq[QuestionId]].?,
                "tagsIds".as[Seq[TagId]].?,
                "operationId".as[OperationId].?,
                "content".?,
                "slug".?,
                "source".?,
                "location".?,
                "question".?,
                "language".as[Language].?,
                "country".as[Country].?,
                "operationKinds".csv[OperationKind],
                "userType".as[Seq[UserType]].?,
                "ideaIds".as[Seq[IdeaId]].?,
                "keywords".as[Seq[ProposalKeywordKey]].?,
                "isNotVoted".as[Boolean].?
              ) {
                (
                  proposalIds: Option[Seq[ProposalId]],
                  questionIds: Option[Seq[QuestionId]],
                  tagsIds: Option[Seq[TagId]],
                  operationId: Option[OperationId],
                  content: Option[String],
                  slug: Option[String],
                  source: Option[String],
                  location: Option[String],
                  question: Option[String],
                  language: Option[Language],
                  country: Option[Country],
                  operationKinds: Option[Seq[OperationKind]],
                  userType: Option[Seq[UserType]],
                  ideaIds: Option[Seq[IdeaId]],
                  keywords: Option[Seq[ProposalKeywordKey]],
                  isNotVoted: Option[Boolean]
                ) =>
                  parameters(
                    "sort".as[ProposalElasticsearchFieldName].?,
                    "order".as[Order].?,
                    "limit".as[Pagination.Limit].?,
                    "skip".as[Pagination.Offset].?,
                    "sortAlgorithm".as[AlgorithmSelector].?,
                    "seed".as[Int].?,
                    "preferredLanguage".as[Language].?
                  ) {
                    (
                      sort: Option[ProposalElasticsearchFieldName],
                      order: Option[Order],
                      limit: Option[Pagination.Limit],
                      offset: Option[Pagination.Offset],
                      sortAlgorithm: Option[AlgorithmSelector],
                      seed: Option[Int],
                      preferredLanguage: Option[Language]
                    ) =>
                      Validation.validate(country.map { countryValue =>
                        Validation.validChoices(
                          fieldName = "country",
                          message = Some(
                            s"Invalid country. Expected one of ${BusinessConfig.supportedCountries.map(_.countryCode)}"
                          ),
                          Seq(countryValue),
                          BusinessConfig.supportedCountries.map(_.countryCode)
                        )
                      }.toList: _*)

                      val futureExcludedProposalIds = isNotVoted match {
                        case Some(true) =>
                          sessionHistoryCoordinatorService
                            .retrieveVotedProposals(requestContext.sessionId)
                            .map {
                              case Seq() => None
                              case voted => Some(voted)
                            }
                        case _ => Future.successful(None)
                      }
                      val maybeQuestionId = questionIds match {
                        case Some(Seq(questionId)) => Some(questionId)
                        case _                     => None
                      }
                      (
                        maybeQuestionId.flatTraverse(questionService.getQuestion(_)).asDirective,
                        futureExcludedProposalIds.asDirective
                      ).tupled.flatMap {
                        case (maybeQuestion, excludedProposalIds) =>
                          val contextFilterRequest: Option[ContextFilterRequest] =
                            ContextFilterRequest.parse(operationId, source, location, question)
                          val searchRequest: SearchRequest = SearchRequest(
                            proposalIds = proposalIds,
                            questionIds = questionIds,
                            tagsIds = tagsIds,
                            operationId = operationId,
                            content = content,
                            slug = slug,
                            seed = seed,
                            context = contextFilterRequest,
                            language = language,
                            country = country,
                            sort = sort.map(_.field),
                            order = order,
                            limit = limit,
                            offset = offset,
                            sortAlgorithm = sortAlgorithm.map(_.value),
                            operationKinds = operationKinds.orElse {
                              if (questionIds.exists(_.nonEmpty)) {
                                None
                              } else {
                                Some(OperationKind.publicKinds)
                              }
                            },
                            userTypes = userType,
                            ideaIds = ideaIds,
                            keywords = keywords,
                            excludedProposalIds = excludedProposalIds
                          )
                          proposalService
                            .searchForUser(
                              userId = userAuth.map(_.user.userId),
                              query = searchRequest.toSearchQuery(requestContext),
                              requestContext = requestContext,
                              preferredLanguage = preferredLanguage,
                              questionDefaultLanguage = maybeQuestion.map(_.defaultLanguage)
                            )
                            .asDirective
                      }.apply { proposals =>
                        complete(proposals)
                      }
                  }
              }
            }
          }
        }
      }
    }

    private val maxProposalLength = BusinessConfig.defaultProposalMaxLength
    private val minProposalLength = FrontConfiguration.defaultProposalMinLength

    def postProposal: Route =
      post {
        path("proposals") {
          makeOperation("PostProposal") { requestContext =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              decodeRequest {
                entity(as[ProposeProposalRequest]) { request: ProposeProposalRequest =>
                  (
                    userService.getUser(auth.user.userId).asDirectiveOrNotFound,
                    questionService
                      .getQuestion(request.questionId)
                      .asDirectiveOrBadRequest(
                        ValidationError("question", "mandatory", Some("This proposal refers to no known question"))
                      )
                  ).mapN((user, question) => {
                      (
                        request.content.withMaxLength(maxProposalLength, "content"),
                        request.content.withMinLength(minProposalLength, "content"),
                        request.content.toSanitizedInput("content"),
                        request.language
                          .predicateValidated(
                            question.languages.toList.contains,
                            "language",
                            "unsupported_language",
                            Some(
                              s"Language ${request.language.value} is not supported by question ${request.questionId.value}"
                            )
                          )
                      ).tupled match {
                        case Invalid(errsNec) =>
                          failWith(ValidationFailedError(errsNec.toList)): Directive1[ProposalId]
                        case Valid(a) =>
                          proposalService
                            .propose(
                              user = user,
                              requestContext = requestContext,
                              createdAt = DateHelper.now(),
                              content = request.content,
                              question = question,
                              initialProposal = false,
                              submittedAsLanguage = request.language,
                              isAnonymous = request.isAnonymous,
                              proposalType = ProposalType.ProposalTypeSubmitted
                            )
                            .asDirective
                      }
                    })
                    .flatten
                    .apply(proposalId => complete(StatusCodes.Created -> ProposalIdResponse(proposalId)))
                }
              }
            }
          }
        }
      }

    def vote: Route = post {
      path("proposals" / proposalId / "vote") { proposalId =>
        makeOperation("VoteProposal") { requestContext =>
          optionalMakeOAuth2 { maybeAuth: Option[AuthInfo[UserRights]] =>
            decodeRequest {
              entity(as[VoteProposalRequest]) { request =>
                proposalService
                  .voteProposal(
                    proposalId = proposalId,
                    maybeUserId = maybeAuth.map(_.user.userId),
                    requestContext = requestContext,
                    voteKey = request.voteKey,
                    proposalKey = request.proposalKey
                  )
                  .asDirectiveOrNotFound
                  .apply(
                    votes =>
                      complete(
                        VotesResponse
                          .parseVotes(
                            votes = votes,
                            hasVoted = Map(request.voteKey -> true),
                            votedKey = request.voteKey
                          )
                      )
                  )
              }
            }
          }
        }
      }
    }

    def unvote: Route = post {
      path("proposals" / proposalId / "unvote") { proposalId =>
        makeOperation("UnvoteProposal") { requestContext =>
          optionalMakeOAuth2 { maybeAuth: Option[AuthInfo[UserRights]] =>
            decodeRequest {
              entity(as[VoteProposalRequest]) { request =>
                proposalService
                  .unvoteProposal(
                    proposalId = proposalId,
                    maybeUserId = maybeAuth.map(_.user.userId),
                    requestContext = requestContext,
                    voteKey = request.voteKey,
                    proposalKey = request.proposalKey
                  )
                  .asDirectiveOrNotFound
                  .apply(
                    votes =>
                      complete(
                        VotesResponse.parseVotes(votes = votes, hasVoted = Map.empty, votedKey = request.voteKey)
                      )
                  )
              }
            }
          }
        }
      }
    }

    def qualification: Route = post {
      path("proposals" / proposalId / "qualification") { proposalId =>
        makeOperation("QualificationProposal") { requestContext =>
          optionalMakeOAuth2 { maybeAuth: Option[AuthInfo[UserRights]] =>
            decodeRequest {
              entity(as[QualificationProposalRequest]) { request =>
                proposalService
                  .qualifyVote(
                    proposalId = proposalId,
                    maybeUserId = maybeAuth.map(_.user.userId),
                    requestContext = requestContext,
                    voteKey = request.voteKey,
                    qualificationKey = request.qualificationKey,
                    proposalKey = request.proposalKey
                  )
                  .asDirectiveOrNotFound { qualification: Qualification =>
                    complete(
                      QualificationResponse.parseQualification(qualification = qualification, hasQualified = true)
                    )
                  }
              }
            }
          }
        }
      }
    }

    def unqualification: Route = post {
      path("proposals" / proposalId / "unqualification") { proposalId =>
        makeOperation("UnqualificationProposal") { requestContext =>
          optionalMakeOAuth2 { maybeAuth: Option[AuthInfo[UserRights]] =>
            decodeRequest {
              entity(as[QualificationProposalRequest]) { request =>
                proposalService
                  .unqualifyVote(
                    proposalId = proposalId,
                    maybeUserId = maybeAuth.map(_.user.userId),
                    requestContext = requestContext,
                    voteKey = request.voteKey,
                    qualificationKey = request.qualificationKey,
                    proposalKey = request.proposalKey
                  )
                  .asDirectiveOrNotFound { qualification: Qualification =>
                    complete(
                      QualificationResponse.parseQualification(qualification = qualification, hasQualified = false)
                    )
                  }
              }
            }
          }
        }
      }
    }

    def reportProposal: Route =
      post {
        path("proposals" / proposalId / "report") { proposalId =>
          makeOperation("PostProposal") { requestContext =>
            decodeRequest {
              entity(as[ReportProposalRequest]) { request: ReportProposalRequest =>
                Future(
                  eventBusService.publish(
                    PublishedProposalEvent.ProposalReportedNotice(
                      id = proposalId,
                      proposalLanguage = request.proposalLanguage,
                      reason = request.reason,
                      eventDate = DateHelper.now(),
                      requestContext = requestContext
                    )
                  )
                ).asDirective
                  .apply(_ => complete(StatusCodes.OK))
              }
            }
          }
        }
      }
  }
}
