/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.proposal

import cats.implicits._
import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import akka.http.scaladsl.unmarshalling.Unmarshaller._
import grizzled.slf4j.Logging
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import io.swagger.annotations._
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.idea.IdeaServiceComponent
import org.make.api.operation.OperationServiceComponent
import org.make.api.question.QuestionServiceComponent
import org.make.api.tag.TagServiceComponent
import org.make.api.technical.CsvReceptacle._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.{
  `X-Total-Count`,
  ActorSystemComponent,
  MakeAuthenticationDirectives,
  ReadJournalComponent
}
import org.make.api.user.UserServiceComponent
import org.make.core._
import org.make.core.technical.{Multilingual, MultilingualUtils, Pagination}
import org.make.core.auth.UserRights
import org.make.core.idea.IdeaId
import org.make.core.operation.OperationId
import org.make.core.proposal._
import org.make.core.Validation._
import org.make.core.proposal.{Qualification => ProposalQualification, Vote => ProposalVote}
import org.make.core.proposal.indexed._
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import org.make.core.tag.TagId
import org.make.core.user.Role.RoleAdmin
import org.make.core.user.{UserId, UserType}
import scalaoauth2.provider.AuthInfo

import java.time.ZonedDateTime
import javax.ws.rs.Path
import scala.annotation.meta.field
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Api(value = "ModerationProposal")
@Path("/moderation/proposals")
trait ModerationProposalApi extends Directives {

  @ApiOperation(
    value = "get-moderation-proposal",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModerationProposalResponse]))
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string")))
  @Path(value = "/{proposalId}")
  def getModerationProposal: Route

  @ApiOperation(
    value = "legacy-moderation-search-proposals",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ProposalsSearchResult]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "proposalIds", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "createdBefore", paramType = "query", dataType = "dateTime"),
      new ApiImplicitParam(name = "initialProposal", paramType = "query", dataType = "boolean"),
      new ApiImplicitParam(name = "tagsIds", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "operationId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "questionId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "ideaId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "content", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "source", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "location", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "question", paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "status",
        paramType = "query",
        dataType = "string",
        allowableValues = ProposalStatus.swaggerAllowableValues,
        allowMultiple = true
      ),
      new ApiImplicitParam(
        name = "minVotesCount",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(name = "toEnrich", paramType = "query", dataType = "boolean"),
      new ApiImplicitParam(name = "minScore", paramType = "query", dataType = "float"),
      new ApiImplicitParam(name = "language", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "country", paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "limit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "skip",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "sort",
        paramType = "query",
        dataType = "string",
        example = "createdAt",
        allowableValues = "content,slug,status,createdAt,updatedAt,trending,labels,country,language"
      ),
      new ApiImplicitParam(
        name = "order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "userTypes",
        paramType = "query",
        dataType = "string",
        allowableValues = UserType.swaggerAllowableValues,
        allowMultiple = true
      ),
      new ApiImplicitParam(name = "keywords", paramType = "query", dataType = "string")
    )
  )
  @Path(value = "/legacy")
  def legacySearchAllProposals: Route

  @ApiOperation(
    value = "moderation-search-proposals",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[ProposalListResponse]]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "id", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(name = "questionId", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(
        name = "status",
        paramType = "query",
        dataType = "string",
        allowableValues = ProposalStatus.swaggerAllowableValues,
        allowMultiple = true
      ),
      new ApiImplicitParam(name = "content", paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "userType",
        paramType = "query",
        dataType = "string",
        allowableValues = UserType.swaggerAllowableValues,
        allowMultiple = true
      ),
      new ApiImplicitParam(name = "initialProposal", paramType = "query", dataType = "boolean"),
      new ApiImplicitParam(name = "tagId", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(
        name = "submittedAsLanguages",
        paramType = "query",
        dataType = "string",
        allowMultiple = true
      ),
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = ProposalElasticsearchFieldName.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      )
    )
  )
  def searchAllProposals: Route

  @ApiOperation(
    value = "update-proposal",
    httpMethod = "PUT",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.proposal.UpdateProposalRequest"
      ),
      new ApiImplicitParam(name = "proposalId", paramType = "path", required = true, dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModerationProposalResponse]))
  )
  @Path(value = "/{proposalId}")
  def updateProposal: Route

  @ApiOperation(
    value = "validate-proposal",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.proposal.ValidateProposalRequest"
      ),
      new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModerationProposalResponse]))
  )
  @Path(value = "/{proposalId}/accept")
  def acceptProposal: Route

  @ApiOperation(
    value = "refuse-proposal",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.proposal.RefuseProposalRequest")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModerationProposalResponse]))
  )
  @Path(value = "/{proposalId}/refuse")
  def refuseProposal: Route

  @ApiOperation(
    value = "postpone-proposal",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModerationProposalResponse]))
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string")))
  @Path(value = "/{proposalId}/postpone")
  def postponeProposal: Route

  @ApiOperation(
    value = "lock-proposal",
    httpMethod = "POST",
    code = HttpCodes.NoContent,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string")))
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No Content")))
  @Path(value = "/{proposalId}/lock")
  def lock: Route

  @ApiOperation(
    value = "lock-multiple-proposal",
    httpMethod = "POST",
    code = HttpCodes.NoContent,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.proposal.LockProposalsRequest")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No Content")))
  @Path(value = "/lock")
  def lockMultiple: Route

  @ApiOperation(
    value = "update-proposals-to-idea",
    httpMethod = "POST",
    code = HttpCodes.NoContent,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "moderator", description = "BO Moderator"))
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No Content")))
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "body",
        paramType = "body",
        dataType = "org.make.api.proposal.PatchProposalsIdeaRequest"
      )
    )
  )
  @Path(value = "/change-idea")
  def changeProposalsIdea: Route

  @ApiOperation(
    value = "next-author-to-moderate",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "moderator", description = "BO Moderator"))
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModerationAuthorResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "body",
        paramType = "body",
        dataType = "org.make.api.proposal.NextProposalToModerateRequest"
      )
    )
  )
  @Path(value = "/next-author-to-moderate")
  def nextAuthorToModerate: Route

  @ApiOperation(
    value = "next-proposal-to-moderate",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "moderator", description = "BO Moderator"))
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModerationProposalResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "body",
        paramType = "body",
        dataType = "org.make.api.proposal.NextProposalToModerateRequest"
      )
    )
  )
  @Path(value = "/next")
  def nextProposalToModerate: Route

  @ApiOperation(
    value = "get-tags-for-proposal",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "moderator", description = "BO Moderator"))
      )
    )
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string")))
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[TagsForProposalResponse]))
  )
  @Path(value = "/{proposalId}/tags")
  def getTagsForProposal: Route

  @ApiOperation(
    value = "bulk-refuse-proposal",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.proposal.BulkRefuseProposal")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[BulkActionResponse]))
  )
  @Path(value = "/refuse-initials-proposals")
  def bulkRefuseInitialsProposals: Route

  @ApiOperation(
    value = "bulk-tag-proposal",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.proposal.BulkTagProposal")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[BulkActionResponse]))
  )
  @Path(value = "/tag")
  def bulkTagProposal: Route

  @ApiOperation(
    value = "bulk-delete-tag-proposal",
    httpMethod = "DELETE",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.proposal.BulkDeleteTagProposal")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[BulkActionResponse]))
  )
  @Path(value = "/tag")
  def bulkDeleteTagProposal: Route

  def routes: Route =
    searchAllProposals ~
      legacySearchAllProposals ~
      updateProposal ~
      acceptProposal ~
      refuseProposal ~
      postponeProposal ~
      lock ~
      lockMultiple ~
      changeProposalsIdea ~
      getModerationProposal ~
      nextAuthorToModerate ~
      nextProposalToModerate ~
      getTagsForProposal ~
      bulkTagProposal ~
      bulkDeleteTagProposal ~
      bulkRefuseInitialsProposals
}

trait ModerationProposalApiComponent {
  def moderationProposalApi: ModerationProposalApi
}

trait DefaultModerationProposalApiComponent
    extends ModerationProposalApiComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {
  this: ProposalServiceComponent
    with MakeDirectivesDependencies
    with UserServiceComponent
    with IdeaServiceComponent
    with QuestionServiceComponent
    with OperationServiceComponent
    with ProposalCoordinatorServiceComponent
    with ReadJournalComponent
    with ActorSystemComponent
    with TagServiceComponent =>

  override lazy val moderationProposalApi: DefaultModerationProposalApi = new DefaultModerationProposalApi

  class DefaultModerationProposalApi extends ModerationProposalApi {

    val moderationProposalId: PathMatcher1[ProposalId] = Segment.map(id => ProposalId(id))

    override def getModerationProposal: Route = {
      get {
        path("moderation" / "proposals" / moderationProposalId) { proposalId =>
          makeOperation("GetModerationProposal") { _ =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireModerationRole(auth.user) {
                proposalService
                  .getModerationProposalById(proposalId)
                  .asDirectiveOrNotFound
                  .apply(
                    moderationProposalResponse =>
                      requireRightsOnQuestion(auth.user, moderationProposalResponse.questionId) {
                        complete(moderationProposalResponse)
                      }
                  )
              }
            }
          }
        }
      }
    }

    override def legacySearchAllProposals: Route = {
      get {
        path("moderation" / "proposals" / "legacy") {
          makeOperation("SearchAll") { requestContext =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              requireModerationRole(userAuth.user) {
                parameters(
                  "proposalIds".as[Seq[ProposalId]].?,
                  "createdBefore".as[ZonedDateTime].?,
                  "proposalType".csv[ProposalType],
                  "tagsIds".as[Seq[TagId]].?,
                  "operationId".as[OperationId].?,
                  "questionId".as[Seq[QuestionId]].?,
                  "ideaId".as[Seq[IdeaId]].?,
                  "content".?,
                  "source".?,
                  "location".?,
                  "question".?,
                  "status".csv[ProposalStatus],
                  "minVotesCount".as[Int].?,
                  "toEnrich".as[Boolean].?,
                  "minScore".as[Double].?,
                  "language".as[Language].?,
                  "country".as[Country].?,
                  "userType".csv[UserType],
                  "keywords".as[Seq[ProposalKeywordKey]].?,
                  "userId".as[UserId].?
                ) {
                  (
                    proposalIds: Option[Seq[ProposalId]],
                    createdBefore: Option[ZonedDateTime],
                    proposalTypes: Option[Seq[ProposalType]],
                    tagsIds: Option[Seq[TagId]],
                    operationId: Option[OperationId],
                    questionIds: Option[Seq[QuestionId]],
                    ideaId: Option[Seq[IdeaId]],
                    content: Option[String],
                    source: Option[String],
                    location: Option[String],
                    question: Option[String],
                    status: Option[Seq[ProposalStatus]],
                    minVotesCount: Option[Int],
                    toEnrich: Option[Boolean],
                    minScore: Option[Double],
                    language: Option[Language],
                    country: Option[Country],
                    userTypes: Option[Seq[UserType]],
                    keywords: Option[Seq[ProposalKeywordKey]],
                    userId: Option[UserId]
                  ) =>
                    parameters(
                      "limit".as[Pagination.Limit].?,
                      "skip".as[Pagination.Offset].?,
                      "sort".?,
                      "order".as[Order].?
                    ) {
                      (
                        limit: Option[Pagination.Limit],
                        offset: Option[Pagination.Offset],
                        sort: Option[String],
                        order: Option[Order]
                      ) =>
                        Validation.validate(Seq(country.map { countryValue =>
                          Validation.validChoices(
                            fieldName = "country",
                            message = Some(
                              s"Invalid country. Expected one of ${BusinessConfig.supportedCountries.map(_.countryCode)}"
                            ),
                            Seq(countryValue),
                            BusinessConfig.supportedCountries.map(_.countryCode)
                          )
                        }, sort.map { sortValue =>
                          val choices =
                            Seq(
                              "content",
                              "slug",
                              "status",
                              "createdAt",
                              "updatedAt",
                              "trending",
                              "labels",
                              "country",
                              "language"
                            )
                          Validation.validChoices(
                            fieldName = "sort",
                            message = Some(
                              s"Invalid sort. Got $sortValue but expected one of: ${choices.mkString("\"", "\", \"", "\"")}"
                            ),
                            Seq(sortValue),
                            choices
                          )
                        }).flatten: _*)

                        val resolvedQuestions: Option[Seq[QuestionId]] = {
                          if (userAuth.user.roles.contains(RoleAdmin)) {
                            questionIds
                          } else {
                            questionIds.map { questions =>
                              questions.filter(id => userAuth.user.availableQuestions.contains(id))
                            }.orElse(Some(userAuth.user.availableQuestions))
                          }
                        }

                        val contextFilterRequest: Option[ContextFilterRequest] =
                          ContextFilterRequest.parse(operationId, source, location, question)

                        val exhaustiveSearchRequest: ExhaustiveSearchRequest = ExhaustiveSearchRequest(
                          proposalIds = proposalIds,
                          proposalTypes = proposalTypes,
                          tagsIds = tagsIds,
                          operationId = operationId,
                          questionIds = resolvedQuestions,
                          ideaIds = ideaId,
                          content = content,
                          context = contextFilterRequest,
                          status = status,
                          minVotesCount = minVotesCount,
                          toEnrich = toEnrich,
                          minScore = minScore,
                          language = language,
                          country = country,
                          sort = sort,
                          order = order,
                          limit = limit,
                          offset = offset,
                          createdBefore = createdBefore,
                          userTypes = userTypes,
                          keywords = keywords,
                          userId = userId
                        )
                        val query: SearchQuery = exhaustiveSearchRequest.toSearchQuery(requestContext)
                        proposalService
                          .searchInIndex(query = query, requestContext = requestContext)
                          .asDirective
                          .apply(complete(_))
                    }
                }
              }
            }
          }
        }
      }
    }

    override def searchAllProposals: Route = get {
      path("moderation" / "proposals") {
        makeOperation("ModerationSearchAll") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireModerationRole(userAuth.user) {
              parameters(
                "id".csv[ProposalId],
                "questionId".csv[QuestionId],
                "userId".as[UserId].?,
                "content".?,
                "status".csv[ProposalStatus],
                "userType".csv[UserType],
                "proposalType".csv[ProposalType],
                "tagId".csv[TagId],
                "submittedAsLanguages".csv[Language],
                "_start".as[Pagination.Offset].?,
                "_end".as[Pagination.End].?,
                "_sort".as[ProposalElasticsearchFieldName].?,
                "_order".as[Order].?
              ) {
                (
                  proposalIds: Option[Seq[ProposalId]],
                  questionIds: Option[Seq[QuestionId]],
                  userId: Option[UserId],
                  content: Option[String],
                  status: Option[Seq[ProposalStatus]],
                  userTypes: Option[Seq[UserType]],
                  proposalTypes: Option[Seq[ProposalType]],
                  tagIds: Option[Seq[TagId]],
                  submittedAsLanguages: Option[Seq[Language]],
                  offset: Option[Pagination.Offset],
                  end: Option[Pagination.End],
                  sort: Option[ProposalElasticsearchFieldName],
                  order: Option[Order]
                ) =>
                  val resolvedQuestions: Option[Seq[QuestionId]] = {
                    if (userAuth.user.roles.contains(RoleAdmin)) {
                      questionIds
                    } else {
                      questionIds.map { questions =>
                        questions.filter(id => userAuth.user.availableQuestions.contains(id))
                      }.orElse(Some(userAuth.user.availableQuestions))
                    }
                  }

                  val exhaustiveSearchRequest = ExhaustiveSearchRequest(
                    proposalIds = proposalIds,
                    proposalTypes = proposalTypes,
                    tagsIds = tagIds,
                    questionIds = resolvedQuestions,
                    content = content,
                    status = status,
                    sort = sort.map(_.field),
                    order = order,
                    limit = end.map(_.toLimit(offset.orZero)),
                    offset = offset,
                    userTypes = userTypes,
                    userId = userId,
                    submittedAsLanguages = submittedAsLanguages
                  )
                  proposalService
                    .searchInIndex(exhaustiveSearchRequest.toSearchQuery(requestContext), requestContext)
                    .asDirective
                    .apply(
                      proposals =>
                        complete(
                          (
                            StatusCodes.OK,
                            List(`X-Total-Count`(proposals.total.toString)),
                            proposals.results.map(ProposalListResponse.apply)
                          )
                        )
                    )
              }
            }
          }
        }
      }
    }

    private val maxProposalLength = BusinessConfig.defaultProposalMaxLength
    private val maxProposalTranslationLength = BusinessConfig.defaultProposalTranslationMaxLength
    private val minProposalLength = FrontConfiguration.defaultProposalMinLength

    override def updateProposal: Route = put {
      path("moderation" / "proposals" / moderationProposalId) { proposalId =>
        makeOperation("EditProposal") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireModerationRole(userAuth.user) {
              decodeRequest {
                entity(as[UpdateProposalRequest]) { request =>
                  retrieveQuestion(request.questionId, proposalId).asDirective { question =>
                    requireRightsOnQuestion(userAuth.user, Some(question.questionId)) {
                      proposalService.getModerationProposalById(proposalId).asDirectiveOrNotFound { proposal =>
                        val newContent =
                          request.newContent.fold(Validated.validNec[ValidationError, Unit](()))(
                            content =>
                              (
                                content.withMaxLength(maxProposalLength, "newContent"),
                                content.withMinLength(minProposalLength, "newContent"),
                                content.toSanitizedInput("newContent")
                              ).tupled.void
                          )
                        val newContentTranslations =
                          request.newContentTranslations.fold(Validated.validNec[ValidationError, Unit](()))(
                            _.translations.foldMap(
                              translations =>
                                (
                                  translations.withMaxLength(maxProposalTranslationLength, "newContentTranslations"),
                                  translations.withMinLength(minProposalLength, "newContentTranslations")
                                ).tupled.void
                            )
                          )
                        val translations =
                          request.newContentTranslations
                            .orElse(proposal.contentTranslations)
                            .getOrElse(Multilingual.empty)
                            .addTranslation(
                              proposal.submittedAsLanguage.getOrElse(question.defaultLanguage),
                              request.newContent.getOrElse(proposal.content)
                            )
                        val validatedTranslations =
                          MultilingualUtils
                            .hasRequiredTranslations(question.languages.toList.toSet, List((translations, "contents")))
                        (newContent, newContentTranslations, validatedTranslations).tupled match {
                          case Invalid(errsNec) => failWith(ValidationFailedError(errsNec.toList))
                          case Valid(_) =>
                            proposalService
                              .update(
                                proposalId = proposalId,
                                moderator = userAuth.user.userId,
                                requestContext = requestContext,
                                updatedAt = DateHelper.now(),
                                newContent = request.newContent,
                                newContentTranslations = request.newContentTranslations,
                                question = question,
                                tags = request.tags
                              )
                              .asDirectiveOrNotFound
                              .apply(complete(_))
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    private def retrieveQuestion(maybeQuestionId: Option[QuestionId], proposalId: ProposalId): Future[Question] = {
      maybeQuestionId match {
        case Some(questionId) =>
          questionService.getQuestion(questionId).flatMap {
            case Some(question) => Future.successful(question)
            case _ =>
              Future.failed(
                ValidationFailedError(
                  Seq(ValidationError("questionId", "not_found", Some(s"question '$questionId' not found")))
                )
              )
          }
        case _ =>
          proposalCoordinatorService.getProposal(proposalId).flatMap {
            case Some(proposal) =>
              proposal.questionId match {
                case Some(questionId) =>
                  questionService.getQuestion(questionId).flatMap {
                    case Some(question) => Future.successful(question)
                    case _ =>
                      Future.failed(
                        new IllegalStateException(s"question '$questionId' not found for proposal '$proposalId'")
                      )
                  }
                case _ => Future.failed(new IllegalStateException(s"proposal '$proposalId' has no questionId"))
              }
            case _ =>
              Future.failed(
                ValidationFailedError(
                  Seq(ValidationError("questionId", "not_found", Some(s"proposal '$proposalId' not found")))
                )
              )
          }
      }
    }

    override def acceptProposal: Route = post {
      path("moderation" / "proposals" / moderationProposalId / "accept") { proposalId =>
        makeOperation("ValidateProposal") { requestContext =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireModerationRole(auth.user) {
              decodeRequest {
                entity(as[ValidateProposalRequest]) { request =>
                  retrieveQuestion(request.questionId, proposalId).asDirective { question =>
                    requireRightsOnQuestion(auth.user, Some(question.questionId)) {
                      proposalService.getModerationProposalById(proposalId).asDirectiveOrNotFound { proposal =>
                        val newContent =
                          request.newContent.fold(Validated.validNec[ValidationError, Unit](()))(
                            content =>
                              (
                                content.withMaxLength(maxProposalLength, "Original content"),
                                content.withMinLength(minProposalLength, "Original content"),
                                content.toSanitizedInput("newContent")
                              ).tupled.void
                          )
                        val newContentTranslations =
                          request.newContentTranslations
                            .fold(Validated.validNec[ValidationError, Unit](()))(_.toMap.toList.foldMap {
                              case (language, translations) =>
                                (
                                  translations
                                    .withMaxLength(maxProposalTranslationLength, "Translations", Some(language)),
                                  translations.withMinLength(minProposalLength, "Translations", Some(language))
                                ).tupled.void
                            })
                        val translations =
                          request.newContentTranslations
                            .orElse(proposal.contentTranslations)
                            .getOrElse(Multilingual.empty)
                            .addTranslation(
                              proposal.submittedAsLanguage.getOrElse(question.defaultLanguage),
                              request.newContent.getOrElse(proposal.content)
                            )
                        val validatedTranslations =
                          MultilingualUtils
                            .hasRequiredTranslations(
                              question.languages.toList.toSet,
                              List((translations, "Translations"))
                            )
                        (newContent, newContentTranslations, validatedTranslations).tupled match {
                          case Invalid(errsNec) => failWith(ValidationFailedError(errsNec.toList))
                          case Valid(_) =>
                            proposalService
                              .validateProposal(
                                proposalId = proposalId,
                                moderator = auth.user.userId,
                                requestContext = requestContext,
                                question = question,
                                newContent = request.newContent,
                                newContentTranslations = request.newContentTranslations,
                                sendNotificationEmail = request.sendNotificationEmail,
                                tags = request.tags
                              )
                              .asDirectiveOrNotFound
                              .apply(complete(_))
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def refuseProposal: Route = post {
      path("moderation" / "proposals" / moderationProposalId / "refuse") { proposalId =>
        makeOperation("RefuseProposal") { requestContext =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireModerationRole(auth.user) {
              proposalCoordinatorService.getProposal(proposalId).asDirectiveOrNotFound { proposal =>
                requireRightsOnQuestion(auth.user, proposal.questionId) {
                  decodeRequest {
                    entity(as[RefuseProposalRequest]) { refuseProposalRequest =>
                      proposalService
                        .refuseProposal(
                          proposalId = proposalId,
                          moderator = auth.user.userId,
                          requestContext = requestContext,
                          request = refuseProposalRequest
                        )
                        .asDirectiveOrNotFound
                        .apply(complete(_))
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def postponeProposal: Route = post {
      path("moderation" / "proposals" / moderationProposalId / "postpone") { proposalId =>
        makeOperation("PostponeProposal") { requestContext =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireModerationRole(auth.user) {
              proposalCoordinatorService.getProposal(proposalId).asDirectiveOrNotFound { proposal =>
                requireRightsOnQuestion(auth.user, proposal.questionId) {
                  decodeRequest {
                    proposalService
                      .postponeProposal(
                        proposalId = proposalId,
                        moderator = auth.user.userId,
                        requestContext = requestContext
                      )
                      .asDirectiveOrNotFound
                      .apply(complete(_))
                  }
                }
              }
            }
          }
        }
      }
    }

    override def lock: Route = post {
      path("moderation" / "proposals" / moderationProposalId / "lock") { proposalId =>
        makeOperation("LockProposal") { requestContext =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireModerationRole(auth.user) {
              proposalCoordinatorService.getProposal(proposalId).asDirectiveOrNotFound { proposal =>
                requireRightsOnQuestion(auth.user, proposal.questionId) {
                  userService.getUser(auth.user.userId).asDirectiveOrNotFound { moderator =>
                    proposalService
                      .lockProposal(
                        proposalId = proposalId,
                        moderatorId = moderator.userId,
                        moderatorFullName = moderator.fullName,
                        requestContext = requestContext
                      )
                      .asDirective { _ =>
                        complete(StatusCodes.NoContent)
                      }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def lockMultiple: Route = post {
      path("moderation" / "proposals" / "lock") {
        makeOperation("LockMultipleProposals") { requestContext =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireModerationRole(auth.user) {
              decodeRequest {
                entity(as[LockProposalsRequest]) { request =>
                  val query = SearchQuery(
                    filters = Some(
                      SearchFilters(
                        proposal = Some(ProposalSearchFilter(request.proposalIds.toSeq)),
                        status = Some(StatusSearchFilter(ProposalStatus.values))
                      )
                    ),
                    limit = Some(Pagination.Limit(request.proposalIds.size + 1))
                  )
                  proposalService.searchInIndex(query = query, requestContext = requestContext).asDirective {
                    proposals =>
                      val proposalIds = proposals.results.map(_.id)
                      val notFoundIds = request.proposalIds.diff(proposalIds.toSet)
                      Validation.validate(
                        Validation.validateField(
                          field = "proposalIds",
                          key = "invalid_value",
                          condition = notFoundIds.isEmpty,
                          message = s"Proposals not found: ${notFoundIds.mkString(", ")}"
                        )
                      )
                      requireRightsOnQuestion(auth.user, proposals.results.flatMap(_.question.map(_.questionId))) {
                        userService.getUser(auth.user.userId).asDirectiveOrNotFound { moderator =>
                          proposalService
                            .lockProposals(
                              proposalIds = proposalIds,
                              moderatorId = moderator.userId,
                              moderatorFullName = moderator.fullName,
                              requestContext = requestContext
                            )
                            .asDirective { _ =>
                              complete(StatusCodes.NoContent)
                            }
                        }
                      }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def changeProposalsIdea: Route = post {
      path("moderation" / "proposals" / "change-idea") {
        makeOperation("ChangeProposalsIdea") { _ =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireAdminRole(auth.user) {
              decodeRequest {
                entity(as[PatchProposalsIdeaRequest]) { changes =>
                  ideaService.fetchOne(changes.ideaId).asDirective { maybeIdea =>
                    Validation.validate(
                      Validation
                        .requirePresent(fieldValue = maybeIdea, fieldName = "ideaId", message = Some("Invalid idea id"))
                    )
                    changes.proposalIds.traverse(proposalService.getModerationProposalById).asDirective { proposals =>
                      val invalidProposalIdValues: Seq[String] =
                        changes.proposalIds.map(_.value).diff(proposals.flatten.map(_.proposalId.value))
                      val invalidProposalIdValuesString: String = invalidProposalIdValues.mkString(", ")
                      Validation.validate(
                        Validation.validateField(
                          field = "proposalIds",
                          "invalid_value",
                          message = s"Some proposal ids are invalid: $invalidProposalIdValuesString",
                          condition = invalidProposalIdValues.isEmpty
                        )
                      )
                      proposalService
                        .changeProposalsIdea(changes.proposalIds, moderatorId = auth.user.userId, changes.ideaId)
                        .asDirective { updatedProposals =>
                          val proposalIdsDiff: Seq[String] =
                            changes.proposalIds.map(_.value).diff(updatedProposals.map(_.proposalId.value))
                          if (proposalIdsDiff.nonEmpty) {
                            logger
                              .warn(
                                s"Some proposals are not updated during change idea operation: ${proposalIdsDiff.mkString(", ")}"
                              )
                          }
                          complete(StatusCodes.NoContent)
                        }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def nextAuthorToModerate: Route = post {
      path("moderation" / "proposals" / "next-author-to-moderate") {
        makeOperation("NextAuthorToModerate") { context =>
          makeOAuth2 { auth =>
            requireModerationRole(auth.user) {
              decodeRequest {
                entity(as[NextProposalToModerateRequest]) { request =>
                  request.questionId
                    .map(questionService.getQuestion)
                    .getOrElse(Future.successful(None))
                    .asDirectiveOrNotFound
                    .apply(
                      question =>
                        requireRightsOnQuestion(auth.user, Some(question.questionId)) {
                          userService
                            .getUser(auth.user.userId)
                            .asDirectiveOrNotFound
                            .flatMap(
                              moderator =>
                                proposalService
                                  .searchAndLockAuthorToModerate(
                                    questionId = question.questionId,
                                    moderator = auth.user.userId,
                                    moderatorFullName = moderator.fullName,
                                    languages = request.languages,
                                    requestContext = context,
                                    toEnrich = request.toEnrich,
                                    minVotesCount = request.minVotesCount,
                                    minScore = request.minScore
                                  )
                                  .asDirectiveOrNotFound
                            )
                            .apply(complete(_))
                        }
                    )
                }
              }
            }
          }
        }
      }
    }

    override def nextProposalToModerate: Route = post {
      path("moderation" / "proposals" / "next") {
        makeOperation("NextProposalToModerate") { context =>
          makeOAuth2 { user =>
            requireModerationRole(user.user) {
              decodeRequest {
                entity(as[NextProposalToModerateRequest]) { request =>
                  request.questionId.map { questionId =>
                    questionService.getQuestion(questionId)
                  }.getOrElse(Future.successful(None)).asDirectiveOrNotFound { question =>
                    requireRightsOnQuestion(user.user, Some(question.questionId)) {
                      userService.getUser(user.user.userId).asDirectiveOrNotFound { moderator =>
                        proposalService
                          .searchAndLockProposalToModerate(
                            question.questionId,
                            moderator.userId,
                            moderator.fullName,
                            None,
                            context,
                            request.toEnrich,
                            request.minVotesCount,
                            request.minScore
                          )
                          .asDirectiveOrNotFound
                          .apply(complete(_))
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def getTagsForProposal: Route = get {
      path("moderation" / "proposals" / moderationProposalId / "tags") { moderationProposalId =>
        makeOperation("GetTagsForProposal") { _ =>
          makeOAuth2 { user =>
            requireModerationRole(user.user) {
              proposalCoordinatorService.getProposal(moderationProposalId).asDirectiveOrNotFound { proposal =>
                requireRightsOnQuestion(user.user, proposal.questionId) {
                  proposalService.getTagsForProposal(proposal).asDirective.apply(complete(_))
                }
              }
            }
          }
        }
      }
    }

    override def bulkRefuseInitialsProposals: Route = post {
      path("moderation" / "proposals" / "refuse-initials-proposals") {
        makeOperation("BulkRefuseInitialsProposals") { requestContext =>
          makeOAuth2 { userAuth =>
            requireModerationRole(userAuth.user) {
              decodeRequest {
                entity(as[BulkRefuseProposal]) { request =>
                  proposalService
                    .refuseAll(request.proposalIds, userAuth.user.userId, requestContext)
                    .asDirective
                    .apply(complete(_))
                }
              }
            }
          }
        }
      }
    }

    override def bulkTagProposal: Route = post {
      path("moderation" / "proposals" / "tag") {
        makeOperation("BulkTagProposal") { requestContext =>
          makeOAuth2 { userAuth =>
            requireModerationRole(userAuth.user) {
              decodeRequest {
                entity(as[BulkTagProposal]) { request =>
                  tagService.findByTagIds(request.tagIds).asDirective { foundTags =>
                    Validation.validate(request.tagIds.diff(foundTags.map(_.tagId)).map { tagId =>
                      Validation.validateField(
                        field = "tagId",
                        key = "invalid_value",
                        condition = false,
                        message = s"Tag $tagId does not exist."
                      )
                    }: _*)
                    proposalService
                      .addTagsToAll(request.proposalIds, request.tagIds, userAuth.user.userId, requestContext)
                      .asDirective
                      .apply(complete(_))
                  }
                }
              }
            }
          }
        }
      }
    }

    override def bulkDeleteTagProposal: Route = delete {
      path("moderation" / "proposals" / "tag") {
        makeOperation("BulkDeleteTagProposal") { requestContext =>
          makeOAuth2 { userAuth =>
            requireModerationRole(userAuth.user) {
              decodeRequest {
                entity(as[BulkDeleteTagProposal]) { request =>
                  tagService.getTag(request.tagId).asDirective { maybeTag =>
                    Validation.validate(
                      Validation.validateField(
                        field = "tagId",
                        key = "invalid_value",
                        condition = maybeTag.isDefined,
                        message = s"Tag ${request.tagId} does not exist."
                      )
                    )
                    proposalService
                      .deleteTagFromAll(request.proposalIds, request.tagId, userAuth.user.userId, requestContext)
                      .asDirective
                      .apply(complete(_))
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

final case class ProposalListResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: ProposalId,
  author: ProposalListResponse.Author,
  content: String,
  contentTranslations: Multilingual[String],
  submittedAsLanguage: Option[Language],
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = ProposalStatus.swaggerAllowableValues
  )
  status: ProposalStatus,
  proposalType: ProposalType,
  refusalReason: Option[String],
  tags: Seq[IndexedTag] = Seq.empty,
  createdAt: ZonedDateTime,
  agreementRate: Double,
  context: ProposalListResponse.Context,
  votes: Seq[ProposalListResponse.Vote],
  votesCount: Int
)

object ProposalListResponse extends CirceFormatters {

  def apply(proposal: IndexedProposal): ProposalListResponse =
    ProposalListResponse(
      id = proposal.id,
      author = Author(proposal),
      content = proposal.contentGeneral,
      contentTranslations = proposal.content,
      submittedAsLanguage = proposal.submittedAsLanguage,
      status = proposal.status,
      proposalType = proposal.proposalType,
      refusalReason = proposal.refusalReason,
      tags = proposal.tags,
      createdAt = proposal.createdAt,
      agreementRate = proposal.agreementRate,
      context = Context(proposal.context),
      votes = proposal.votes.deprecatedVotesSeq.map(Vote.apply),
      votesCount = proposal.votesCount
    )

  final case class Author(
    @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
    id: UserId,
    @(ApiModelProperty @field)(dataType = "string", required = true, allowableValues = UserType.swaggerAllowableValues)
    userType: UserType,
    displayName: Option[String],
    @(ApiModelProperty @field)(dataType = "int", example = "42") age: Option[Int]
  )

  object Author {
    def apply(proposal: IndexedProposal): Author =
      Author(
        id = proposal.author.userId,
        userType = proposal.author.userType,
        displayName = proposal.author.displayName,
        age = proposal.author.age
      )
    implicit val codec: Codec[Author] = deriveCodec
  }

  final case class Context(
    source: Option[String],
    questionSlug: Option[String],
    @(ApiModelProperty @field)(dataType = "string", example = "FR") country: Option[Country],
    @(ApiModelProperty @field)(dataType = "string", example = "fr") questionLanguage: Option[Language],
    @(ApiModelProperty @field)(dataType = "string", example = "fr") proposalLanguage: Option[Language],
    @(ApiModelProperty @field)(dataType = "string", example = "fr") clientLanguage: Option[Language]
  )

  object Context {
    def apply(context: Option[IndexedContext]): Context =
      Context(
        source = context.flatMap(_.source),
        questionSlug = context.flatMap(_.questionSlug),
        country = context.flatMap(_.country),
        questionLanguage = context.flatMap(_.questionLanguage),
        proposalLanguage = context.flatMap(_.proposalLanguage),
        clientLanguage = context.flatMap(_.clientLanguage)
      )
    implicit val codec: Codec[Context] = deriveCodec
  }

  final case class Vote(
    @(ApiModelProperty @field)(dataType = "string", required = true, allowableValues = VoteKey.swaggerAllowableValues)
    key: VoteKey,
    count: Int,
    qualifications: Seq[Qualification]
  )

  object Vote {
    def apply(vote: ProposalVote): Vote =
      Vote(key = vote.key, count = vote.count, qualifications = vote.qualifications.map(Qualification.apply))

    implicit val codec: Codec[Vote] = deriveCodec
  }

  final case class Qualification(
    @(ApiModelProperty @field)(
      dataType = "string",
      required = true,
      allowableValues = QualificationKey.swaggerAllowableValues
    )
    key: QualificationKey,
    count: Int
  )

  object Qualification {
    def apply(qualification: ProposalQualification): Qualification =
      Qualification(key = qualification.key, count = qualification.count)
    implicit val codec: Codec[Qualification] = deriveCodec
  }

  implicit val codec: Codec[ProposalListResponse] = deriveCodec

}
