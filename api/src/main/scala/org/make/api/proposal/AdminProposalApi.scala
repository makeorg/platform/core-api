/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.proposal

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directives, PathMatcher1, Route}
import cats.implicits._
import grizzled.slf4j.Logging
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import io.swagger.annotations._
import org.make.api.ConfigComponent
import org.make.api.idea.IdeaServiceComponent
import org.make.api.operation.OperationServiceComponent
import org.make.api.question.QuestionServiceComponent
import org.make.api.tag.TagServiceComponent
import org.make.api.technical.Futures._
import org.make.api.technical.CsvReceptacle._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.user.UserServiceComponent
import org.make.core.technical.{Multilingual, MultilingualUtils, Pagination}
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils
import org.make.core.auth.UserRights
import org.make.core.proposal.indexed.{IndexedContext, IndexedProposal, IndexedTag, ProposalElasticsearchFieldName}
import org.make.core.proposal.{
  Proposal,
  ProposalId,
  ProposalStatus,
  ProposalType,
  QualificationKey,
  SearchQuery,
  VoteKey,
  Qualification => ProposalQualification,
  Vote          => ProposalVote
}
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import org.make.core.tag.TagId
import org.make.core.user.{UserId, UserType}
import org.make.core.{CirceFormatters, DateHelper, HttpCodes, Order, ParameterExtractors, Validation}
import scalaoauth2.provider.AuthInfo

import java.time.ZonedDateTime
import javax.ws.rs.Path
import scala.annotation.meta.field
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

@Api(value = "AdminProposal")
@Path(value = "/admin/proposals")
trait AdminProposalApi extends Directives {

  @ApiOperation(
    value = "admin-search-proposals",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[AdminProposalResponse]]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "id", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(name = "questionId", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(
        name = "status",
        paramType = "query",
        dataType = "string",
        allowableValues = ProposalStatus.swaggerAllowableValues,
        allowMultiple = true
      ),
      new ApiImplicitParam(name = "content", paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "userType",
        paramType = "query",
        dataType = "string",
        allowableValues = UserType.swaggerAllowableValues,
        allowMultiple = true
      ),
      new ApiImplicitParam(name = "initialProposal", paramType = "query", dataType = "boolean"),
      new ApiImplicitParam(name = "tagId", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = ProposalElasticsearchFieldName.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      )
    )
  )
  def search: Route

  @ApiOperation(
    value = "fix-trolled-proposal",
    httpMethod = "PUT",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.proposal.UpdateProposalVotesRequest"
      ),
      new ApiImplicitParam(name = "proposalId", paramType = "path", required = true, value = "", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModerationProposalResponse]))
  )
  @Path(value = "/{proposalId}/fix-trolled-proposal")
  def updateProposalVotes: Route

  @ApiOperation(
    value = "reset-unverified-proposal-votes",
    httpMethod = "POST",
    code = HttpCodes.Accepted,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.Accepted, message = "Accepted")))
  @Path(value = "/reset-votes")
  def resetVotes: Route

  @ApiOperation(
    value = "patch-proposal",
    httpMethod = "PATCH",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ModerationProposalResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "body", paramType = "body", dataType = "org.make.api.proposal.PatchProposalRequest")
    )
  )
  @Path(value = "/{proposalId}")
  def patchProposal: Route

  @ApiOperation(
    value = "add-keywords",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[ProposalKeywordsResponse]]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "body", paramType = "body", dataType = "org.make.api.proposal.ProposalKeywordRequest")
    )
  )
  @Path(value = "/keywords")
  def setProposalKeywords: Route

  @ApiOperation(
    value = "bulk-refuse-proposal",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.proposal.BulkRefuseProposal")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[BulkActionResponse]))
  )
  @Path(value = "/refuse-initials-proposals")
  def bulkRefuseInitialsProposals: Route

  @ApiOperation(
    value = "bulk-tag-proposal",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.proposal.BulkTagProposal")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[BulkActionResponse]))
  )
  @Path(value = "/tag")
  def bulkTagProposal: Route

  @ApiOperation(
    value = "bulk-delete-tag-proposal",
    httpMethod = "DELETE",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.proposal.BulkDeleteTagProposal")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[BulkActionResponse]))
  )
  @Path(value = "/tag")
  def bulkDeleteTagProposal: Route

  @ApiOperation(
    value = "get-proposal-history",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[ProposalActionResponse]]))
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "proposalId", paramType = "path", dataType = "string")))
  @Path(value = "/{proposalId}/history")
  def getProposalHistory: Route

  @ApiOperation(
    value = "submitted-as-language-patch-script",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok")))
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "dry-run", paramType = "query", dataType = "boolean")
    )
  )
  @Path(value = "/submitted-as-language-patch-script/{questionId}")
  def submittedAsLanguagePatchScript: Route

  def routes: Route =
    search ~ patchProposal ~ updateProposalVotes ~ resetVotes ~ setProposalKeywords ~
      bulkTagProposal ~ bulkDeleteTagProposal ~ getProposalHistory ~ bulkRefuseInitialsProposals ~
      submittedAsLanguagePatchScript
}

trait AdminProposalApiComponent {
  def adminProposalApi: AdminProposalApi
}

trait DefaultAdminProposalApiComponent
    extends AdminProposalApiComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {

  this: MakeDirectivesDependencies
    with ConfigComponent
    with ProposalServiceComponent
    with ProposalCoordinatorServiceComponent
    with QuestionServiceComponent
    with IdeaServiceComponent
    with OperationServiceComponent
    with UserServiceComponent
    with TagServiceComponent =>

  override lazy val adminProposalApi: AdminProposalApi = new DefaultAdminProposalApi

  class DefaultAdminProposalApi extends AdminProposalApi {
    val adminProposalId: PathMatcher1[ProposalId] = Segment.map(id => ProposalId(id))

    @Deprecated
    override def search: Route = get {
      path("admin" / "proposals") {
        makeOperation("AdminSearchProposals") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireAdminRole(userAuth.user) {
              parameters(
                "id".csv[ProposalId],
                "questionId".csv[QuestionId],
                "content".?,
                "status".csv[ProposalStatus],
                "userType".csv[UserType],
                "proposalType".csv[ProposalType],
                "tagId".csv[TagId],
                "_start".as[Pagination.Offset].?,
                "_end".as[Pagination.End].?,
                "_sort".as[ProposalElasticsearchFieldName].?,
                "_order".as[Order].?
              ) {
                (
                  proposalIds: Option[Seq[ProposalId]],
                  questionIds: Option[Seq[QuestionId]],
                  content: Option[String],
                  status: Option[Seq[ProposalStatus]],
                  userTypes: Option[Seq[UserType]],
                  proposalTypes: Option[Seq[ProposalType]],
                  tagIds: Option[Seq[TagId]],
                  offset: Option[Pagination.Offset],
                  end: Option[Pagination.End],
                  sort: Option[ProposalElasticsearchFieldName],
                  order: Option[Order]
                ) =>
                  Validation.validate(sort.map(Validation.validateSort("_sort")).toList: _*)

                  val exhaustiveSearchRequest: ExhaustiveSearchRequest = ExhaustiveSearchRequest(
                    proposalIds = proposalIds,
                    proposalTypes = proposalTypes,
                    tagsIds = tagIds,
                    questionIds = questionIds,
                    content = content,
                    status = status,
                    sort = sort.map(_.field),
                    order = order,
                    limit = end.map(_.toLimit(offset.orZero)),
                    offset = offset,
                    userTypes = userTypes
                  )
                  val query: SearchQuery = exhaustiveSearchRequest.toSearchQuery(requestContext)
                  proposalService
                    .searchInIndex(query, requestContext)
                    .asDirective
                    .apply(
                      proposals =>
                        complete(
                          (
                            StatusCodes.OK,
                            List(`X-Total-Count`(proposals.total.toString)),
                            proposals.results.map(AdminProposalResponse.apply)
                          )
                        )
                    )
              }
            }
          }
        }
      }
    }

    def updateProposalVotes: Route = put {
      path("admin" / "proposals" / adminProposalId / "fix-trolled-proposal") { proposalId =>
        makeOperation("EditProposalVotesVerified") { requestContext =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireAdminRole(userAuth.user) {
              decodeRequest {
                entity(as[UpdateProposalVotesRequest]) { request =>
                  retrieveProposalQuestion(proposalId).asDirectiveOrNotFound { _ =>
                    proposalService
                      .updateVotes(
                        proposalId = proposalId,
                        moderator = userAuth.user.userId,
                        requestContext = requestContext,
                        updatedAt = DateHelper.now(),
                        votes = request.votes
                      )
                      .asDirectiveOrNotFound
                      .apply(complete(_))
                  }
                }
              }
            }
          }
        }
      }
    }

    override def patchProposal: Route = {
      patch {
        path("admin" / "proposals" / adminProposalId) { id =>
          makeOperation("PatchProposal") { context =>
            makeOAuth2 { auth =>
              requireAdminRole(auth.user) {
                decodeRequest {
                  entity(as[PatchProposalRequest]) { patch =>
                    (
                      proposalService.getProposalById(id, context).flattenOrFail("Proposal not found"),
                      patch.author.traverse(userService.getUser(_)).map(_.flatten),
                      patch.ideaId.traverse(ideaService.fetchOne(_)).map(_.flatten),
                      patch.operation.traverse(operationService.findOneSimple(_)).map(_.flatten),
                      patch.questionId.traverse(questionService.getQuestion(_)).map(_.flatten),
                      patch.tags.traverse(tagService.findByTagIds(_).map(_.map(_.tagId)))
                    ).tupled.asDirective
                      .flatMap({
                        case (proposal, maybeAuthor, maybeIdea, maybeOperation, maybeQuestion, maybeTags) =>
                          Validation.validateOptional(
                            patch.author.map(_ => {
                              Validation.validateField(
                                field = "author",
                                key = "invalid_value",
                                condition = maybeAuthor.isDefined,
                                message = s"UserId ${patch.author} does not exist."
                              )
                            }),
                            patch.ideaId.map(_ => {
                              Validation.validateField(
                                field = "ideaId",
                                key = "invalid_value",
                                condition = maybeIdea.isDefined,
                                message = s"Idea ${patch.ideaId} does not exist."
                              )
                            }),
                            patch.operation.map(_ => {
                              Validation.validateField(
                                field = "operation",
                                key = "invalid_value",
                                condition = maybeOperation.isDefined,
                                message = s"Operation ${patch.operation} does not exist."
                              )
                            }),
                            patch.questionId.map(_ => {
                              Validation.validateField(
                                field = "questionId",
                                key = "invalid_value",
                                condition = maybeQuestion.isDefined,
                                message = s"Question ${patch.questionId} does not exist."
                              )
                            })
                          )
                          (maybeQuestion, patch.contentTranslations).tupled.foreach({
                            case (question, translations) =>
                              MultilingualUtils
                                .hasRequiredTranslations(
                                  question.languages.toList.toSet - proposal.submittedAsLanguage
                                    .getOrElse(Language("fr")),
                                  List((translations, "proposal_translations"))
                                )
                                .throwIfInvalid()
                          })

                          proposalService
                            .patchProposal(id, auth.user.userId, context, patch.copy(tags = maybeTags))
                            .asDirectiveOrNotFound
                      })
                      .apply(complete(_))
                  }
                }
              }
            }
          }
        }
      }
    }

    private def retrieveProposalQuestion(proposalId: ProposalId): Future[Option[Question]] = {
      proposalCoordinatorService.getProposal(proposalId).flatMap {
        case Some(proposal) =>
          proposal.questionId
            .map(questionService.getQuestion)
            .getOrElse(Future.successful(None))
        case None =>
          Future.successful(None)
      }
    }

    override def resetVotes: Route = post {
      path("admin" / "proposals" / "reset-votes") {
        withoutRequestTimeout {
          makeOperation("ResetVotes") { requestContext =>
            makeOAuth2 { userAuth =>
              requireAdminRole(userAuth.user) {
                proposalService.resetVotes(userAuth.user.userId, requestContext)
                complete(StatusCodes.Accepted)
              }
            }
          }
        }
      }
    }

    override def setProposalKeywords: Route = post {
      path("admin" / "proposals" / "keywords") {
        makeOperation("SetProposalKeywords") { requestContext =>
          makeOAuth2 { userAuth =>
            requireAdminRole(userAuth.user) {
              decodeRequest {
                entity(as[Seq[ProposalKeywordRequest]]) { request =>
                  proposalService
                    .setKeywords(request, requestContext)
                    .asDirective
                    .apply(complete(_))
                }
              }
            }
          }
        }
      }
    }

    @Deprecated
    override def bulkRefuseInitialsProposals: Route = post {
      path("admin" / "proposals" / "refuse-initials-proposals") {
        makeOperation("BulkRefuseInitialsProposals") { requestContext =>
          makeOAuth2 { userAuth =>
            requireAdminRole(userAuth.user) {
              decodeRequest {
                entity(as[BulkRefuseProposal]) { request =>
                  proposalService
                    .refuseAll(request.proposalIds, userAuth.user.userId, requestContext)
                    .asDirective
                    .apply(complete(_))
                }
              }
            }
          }
        }
      }
    }

    @Deprecated
    override def bulkTagProposal: Route = post {
      path("admin" / "proposals" / "tag") {
        makeOperation("BulkTagProposal") { requestContext =>
          makeOAuth2 { userAuth =>
            requireAdminRole(userAuth.user) {
              decodeRequest {
                entity(as[BulkTagProposal]) { request =>
                  tagService.findByTagIds(request.tagIds).asDirective { foundTags =>
                    Validation.validate(request.tagIds.diff(foundTags.map(_.tagId)).map { tagId =>
                      Validation.validateField(
                        field = "tagId",
                        key = "invalid_value",
                        condition = false,
                        message = s"Tag $tagId does not exist."
                      )
                    }: _*)
                    proposalService
                      .addTagsToAll(request.proposalIds, request.tagIds, userAuth.user.userId, requestContext)
                      .asDirective
                      .apply(complete(_))
                  }
                }
              }
            }
          }
        }
      }
    }

    @Deprecated
    override def bulkDeleteTagProposal: Route = delete {
      path("admin" / "proposals" / "tag") {
        makeOperation("BulkDeleteTagProposal") { requestContext =>
          makeOAuth2 { userAuth =>
            requireAdminRole(userAuth.user) {
              decodeRequest {
                entity(as[BulkDeleteTagProposal]) { request =>
                  tagService.getTag(request.tagId).asDirective { maybeTag =>
                    Validation.validate(
                      Validation.validateField(
                        field = "tagId",
                        key = "invalid_value",
                        condition = maybeTag.isDefined,
                        message = s"Tag ${request.tagId} does not exist."
                      )
                    )
                    proposalService
                      .deleteTagFromAll(request.proposalIds, request.tagId, userAuth.user.userId, requestContext)
                      .asDirective
                      .apply(complete(_))
                  }
                }
              }
            }
          }
        }
      }
    }

    private val questionId: PathMatcher1[QuestionId] = Segment.map(QuestionId.apply)

    private val userId = config.getString("make-api.environment") match {
      case "production" => UserId("98c4cc66-d0fd-466b-bf84-e345c1851fcb")
      case _            => UserId("61568b62-2f92-4620-8408-0d22d9d263e2")
    }

    override def submittedAsLanguagePatchScript: Route = post {
      path("admin" / "proposals" / "submitted-as-language-patch-script" / questionId) { questionId =>
        withRequestTimeout(5.minutes) {
          makeOperation("SubmittedAsLanguagePatchScript") { requestContext =>
            parameters("dry-run".as[Boolean].?)(
              dryRun =>
                makeOAuth2 { userAuth =>
                  requireAdminRole(userAuth.user) {
                    questionService
                      .getQuestion(questionId)
                      .flattenOrFail(s"Could not find question $questionId")
                      .map(question => {
                        def modifyFn(p: Proposal): Future[Unit] = {
                          val questionLanguages = question.languages.toList
                          val canPatch = questionLanguages.size === 1
                          val mustPatch = p.submittedAsLanguage.fold(true)(!questionLanguages.contains(_))
                          if (canPatch && mustPatch) {
                            logger.info(s"Patching ${p.proposalId} from question ${question.questionId}")
                            if (dryRun.getOrElse(true)) Future.unit
                            else {
                              val patch = PatchProposalRequest(submittedAsLanguage = Some(question.defaultLanguage))
                              proposalService.patchProposal(p.proposalId, userId, requestContext, patch).void
                            }
                          } else Future.unit
                        }

                        proposalService.modifyForQuestion(questionId, modifyFn, requestContext)
                      })
                      .asDirective
                      .apply(_ => complete(StatusCodes.OK))
                  }
                }
            )
          }
        }
      }
    }

    override def getProposalHistory: Route = get {
      path("admin" / "proposals" / adminProposalId / "history") { id =>
        makeOperation("GetProposalHistory") { _ =>
          makeOAuth2 { userAuth =>
            requireAdminRole(userAuth.user) {
              proposalService.getHistory(id).asDirectiveOrNotFound.apply(complete(_))
            }
          }
        }
      }
    }
  }

}

final case class UpdateProposalVotesRequest(votes: Seq[UpdateVoteRequest])

object UpdateProposalVotesRequest {
  implicit val codec: Codec[UpdateProposalVotesRequest] = deriveCodec
}

final case class AdminProposalResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true) id: ProposalId,
  author: AdminProposalResponse.Author,
  content: String,
  contentTranslations: Multilingual[String],
  submittedAsLanguage: Option[Language],
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = ProposalStatus.swaggerAllowableValues
  ) status: ProposalStatus,
  proposalType: ProposalType,
  tags: Seq[IndexedTag] = Seq.empty,
  createdAt: ZonedDateTime,
  agreementRate: Double,
  context: AdminProposalResponse.Context,
  votes: Seq[AdminProposalResponse.Vote],
  votesCount: Int
)

object AdminProposalResponse extends CirceFormatters {

  def apply(proposal: IndexedProposal): AdminProposalResponse =
    AdminProposalResponse(
      id = proposal.id,
      author = Author(proposal),
      content = proposal.contentGeneral,
      contentTranslations = proposal.content,
      submittedAsLanguage = proposal.submittedAsLanguage,
      status = proposal.status,
      proposalType = proposal.proposalType,
      tags = proposal.tags,
      createdAt = proposal.createdAt,
      agreementRate = proposal.agreementRate,
      context = Context(proposal.context),
      votes = proposal.votes.deprecatedVotesSeq.map(Vote.apply),
      votesCount = proposal.votesCount
    )

  final case class Author(
    @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true) id: UserId,
    @(ApiModelProperty @field)(dataType = "string", required = true, allowableValues = UserType.swaggerAllowableValues) userType: UserType,
    displayName: Option[String],
    postalCode: Option[String],
    @(ApiModelProperty @field)(dataType = "int", example = "42") age: Option[Int],
    profession: Option[String]
  )

  object Author {
    def apply(proposal: IndexedProposal): Author =
      Author(
        id = proposal.author.userId,
        userType = proposal.author.userType,
        displayName = proposal.author.displayName,
        postalCode = proposal.author.postalCode,
        age = proposal.author.age,
        profession = proposal.author.profession
      )
    implicit val codec: Codec[Author] = deriveCodec
  }

  final case class Context(
    source: Option[String],
    questionSlug: Option[String],
    @(ApiModelProperty @field)(dataType = "string", example = "FR") country: Option[Country],
    @(ApiModelProperty @field)(dataType = "string", example = "fr") questionLanguage: Option[Language],
    @(ApiModelProperty @field)(dataType = "string", example = "fr") proposalLanguage: Option[Language],
    @(ApiModelProperty @field)(dataType = "string", example = "fr") clientLanguage: Option[Language]
  )

  object Context {
    def apply(context: Option[IndexedContext]): Context =
      Context(
        source = context.flatMap(_.source),
        questionSlug = context.flatMap(_.questionSlug),
        country = context.flatMap(_.country),
        questionLanguage = context.flatMap(_.questionLanguage),
        proposalLanguage = context.flatMap(_.proposalLanguage),
        clientLanguage = context.flatMap(_.clientLanguage)
      )
    implicit val codec: Codec[Context] = deriveCodec
  }

  final case class Vote(
    @(ApiModelProperty @field)(dataType = "string", required = true, allowableValues = VoteKey.swaggerAllowableValues)
    key: VoteKey,
    count: Int,
    qualifications: Seq[Qualification]
  )

  object Vote {
    def apply(vote: ProposalVote): Vote =
      Vote(key = vote.key, count = vote.count, qualifications = vote.qualifications.map(Qualification.apply))
    implicit val codec: Codec[Vote] = deriveCodec
  }

  final case class Qualification(
    @(ApiModelProperty @field)(
      dataType = "string",
      required = true,
      allowableValues = QualificationKey.swaggerAllowableValues
    )
    key: QualificationKey,
    count: Int
  )

  object Qualification {
    def apply(qualification: ProposalQualification): Qualification =
      Qualification(key = qualification.key, count = qualification.count)
    implicit val codec: Codec[Qualification] = deriveCodec
  }

  implicit val codec: Codec[AdminProposalResponse] = deriveCodec

}
