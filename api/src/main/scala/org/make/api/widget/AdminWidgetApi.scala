/*
 *  Make.org Core API
 *  Copyright (C) 2021 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.widget

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import cats.implicits._
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import io.swagger.annotations.{
  Api,
  ApiImplicitParam,
  ApiImplicitParams,
  ApiModelProperty,
  ApiOperation,
  ApiResponse,
  ApiResponses,
  Authorization,
  AuthorizationScope
}
import org.make.api.question.QuestionServiceComponent
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.user.UserServiceComponent
import org.make.core.{CirceFormatters, HttpCodes, Order, ParameterExtractors, ValidationError}
import org.make.core.auth.UserRights
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, Language}
import org.make.core.sequence.SequenceKind
import org.make.core.technical.Pagination
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils
import org.make.core.user.User
import org.make.core.Validation._
import org.make.core.widget.{SourceId, Widget, WidgetId}
import scalaoauth2.provider.AuthInfo

import java.time.ZonedDateTime
import javax.ws.rs.Path
import scala.annotation.meta.field

@Api(value = "Admin Widgets")
@Path(value = "/admin/widgets")
trait AdminWidgetApi extends Directives {

  @ApiOperation(
    value = "list-widgets",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "sourceId", required = true, paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = "version,country,createdAt"
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[AdminWidgetResponse]]))
  )
  @Path(value = "/")
  def list: Route

  @ApiOperation(
    value = "get-widget-by-id",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "id", paramType = "path", dataType = "string")))
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[AdminWidgetResponse]))
  )
  @Path(value = "/{id}")
  def getById: Route

  @ApiOperation(
    value = "create-widget",
    httpMethod = "POST",
    code = HttpCodes.Created,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.widget.AdminWidgetRequest")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.Created, message = "Ok", response = classOf[AdminWidgetResponse]))
  )
  @Path(value = "/")
  def create: Route

  def routes: Route = list ~ getById ~ create

}

trait AdminWidgetApiComponent {
  def adminWidgetApi: AdminWidgetApi
}

trait DefaultAdminWidgetApiComponent
    extends AdminWidgetApiComponent
    with MakeAuthenticationDirectives
    with ParameterExtractors {
  self: MakeDirectivesDependencies
    with QuestionServiceComponent
    with SourceServiceComponent
    with UserServiceComponent
    with WidgetServiceComponent =>

  override val adminWidgetApi: AdminWidgetApi = new AdminWidgetApi {

    private val id: PathMatcher1[WidgetId] = Segment.map(WidgetId.apply)

    override def list: Route = get {
      path("admin" / "widgets") {
        parameters(
          "sourceId".as[SourceId],
          "_start".as[Pagination.Offset].?,
          "_end".as[Pagination.End].?,
          "_sort".?,
          "_order".as[Order].?
        ) {
          (
            sourceId: SourceId,
            offset: Option[Pagination.Offset],
            end: Option[Pagination.End],
            sort: Option[String],
            order: Option[Order]
          ) =>
            makeOperation("AdminWidgetsList") { _ =>
              makeOAuth2 { userAuth: AuthInfo[UserRights] =>
                requireAdminRole(userAuth.user) {
                  (
                    widgetService.count(sourceId).asDirective,
                    widgetService.list(sourceId, offset, end, sort, order).asDirective
                  ).tupled.apply {
                    case (total, widgets) =>
                      userService.getUsersByUserIds(widgets.map(_.author).distinct).asDirective { authors =>
                        val authorsById = authors.toList.groupByNel(_.userId)
                        complete(
                          StatusCodes.OK,
                          List(`X-Total-Count`(total.toString)),
                          widgets.map(widget => AdminWidgetResponse(widget, authorsById.get(widget.author).map(_.head)))
                        )
                      }
                  }
                }
              }
            }
        }
      }
    }

    override def getById: Route = get {
      path("admin" / "widgets" / id) { id =>
        makeOperation("AdminWidgetsGetById") { _ =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireAdminRole(userAuth.user) {
              widgetService.get(id).asDirectiveOrNotFound { widget =>
                userService.getUser(widget.author).asDirectiveOrNotFound { author =>
                  complete(AdminWidgetResponse(widget, Some(author)))
                }
              }
            }
          }
        }
      }
    }

    override def create: Route = post {
      path("admin" / "widgets") {
        makeOperation("AdminWidgetsCreate") { _ =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireAdminRole(userAuth.user) {
              decodeRequest {
                entity(as[AdminWidgetRequest]) { request: AdminWidgetRequest =>
                  (
                    sourceService
                      .get(request.sourceId)
                      .asDirectiveOrBadRequest(
                        ValidationError(
                          "sourceId",
                          "not_found",
                          Some(s"Source ${request.sourceId.value} doesn't exist")
                        )
                      ),
                    questionService
                      .getQuestion(request.questionId)
                      .asDirectiveOrBadRequest(
                        ValidationError(
                          "questionId",
                          "not_found",
                          Some(s"Question ${request.questionId.value} doesn't exist")
                        )
                      )
                  ).mapN((source, question) => {
                      request.country
                        .predicateValidated(
                          country => question.countries.toList.contains(country),
                          "country",
                          "invalid_value",
                          Some(s"Country ${request.country.value} is not one of question's countries")
                        )
                        .throwIfInvalid()
                      (
                        userService.getUser(userAuth.user.userId).asDirectiveOrNotFound,
                        widgetService
                          .create(
                            source,
                            question,
                            request.country,
                            request.language,
                            request.sequenceKind,
                            userAuth.user.userId
                          )
                          .asDirective
                      ).mapN((author, widget) => AdminWidgetResponse(widget, Some(author)))
                    })
                    .flatten
                    .apply(response => complete(StatusCodes.Created -> response))
                }
              }
            }
          }
        }
      }
    }
  }

}

final case class AdminWidgetRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  sourceId: SourceId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  @(ApiModelProperty @field)(dataType = "string", example = "FR", required = true)
  country: Country,
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  language: Language,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = SequenceKind.swaggerAllowableValues
  )
  sequenceKind: SequenceKind
)

object AdminWidgetRequest {
  implicit val codec: Codec[AdminWidgetRequest] = deriveCodec
}

final case class AdminWidgetResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: WidgetId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  @(ApiModelProperty @field)(dataType = "string", example = "FR", required = true)
  country: Country,
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  language: Language,
  script: String,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = Widget.Version.swaggerAllowableValues
  )
  version: Widget.Version,
  authorDisplayName: Option[String],
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = SequenceKind.swaggerAllowableValues
  )
  sequenceKind: SequenceKind,
  createdAt: ZonedDateTime
)

object AdminWidgetResponse extends CirceFormatters {

  def apply(widget: Widget, author: Option[User]): AdminWidgetResponse =
    AdminWidgetResponse(
      id = widget.id,
      questionId = widget.questionId,
      country = widget.country,
      language = widget.language,
      script = widget.script,
      version = widget.version,
      authorDisplayName = author.flatMap(_.displayName),
      sequenceKind = widget.sequenceKind,
      createdAt = widget.createdAt
    )

  implicit val codec: Codec[AdminWidgetResponse] = deriveCodec

}
