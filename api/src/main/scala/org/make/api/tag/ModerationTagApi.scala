/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.tag

import cats.implicits._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import io.swagger.annotations._
import org.make.api.operation.{OperationOfQuestionServiceComponent, SearchOperationsOfQuestions}

import javax.ws.rs.Path
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.question.QuestionServiceComponent
import org.make.api.tag.DefaultPersistentTagServiceComponent.PersistentTag
import org.make.api.technical.CsvReceptacle.RepeatedCsvFlatteningParamOps
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.core.auth.UserRights
import org.make.core.operation.OperationOfQuestion
import org.make.core.question.QuestionId
import org.make.core.tag.{TagDisplay, TagId, TagTypeId}
import org.make.core.{tag, HttpCodes, Order, ParameterExtractors, Validation}
import org.make.core.Validation.OptionWithParsers
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils
import scalaoauth2.provider.AuthInfo

import scala.annotation.meta.field
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import org.make.core.technical.Pagination._

import java.time.ZonedDateTime
import org.make.core.technical.Pagination
import org.make.core.question.Question

@Api(value = "Moderation Tags")
@Path(value = "/moderation/tags")
trait ModerationTagApi extends Directives {

  @Path(value = "/{tagId}")
  @ApiOperation(
    value = "get-tag",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[TagResponse])))
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "tagId", paramType = "path", dataType = "string")))
  def moderationGetTag: Route

  @ApiOperation(
    value = "create-tag",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiImplicitParams(
    value =
      Array(new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.tag.CreateTagRequest"))
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[TagResponse])))
  @Path(value = "/")
  def moderationCreateTag: Route

  @ApiOperation(
    value = "list-tags",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = PersistentTag.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "label", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "tagTypeId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "questionId", paramType = "query", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[TagResponse]]))
  )
  @Path(value = "/")
  def moderationlistTags: Route

  @ApiOperation(
    value = "update-tag",
    httpMethod = "PUT",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "tagId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(value = "body", paramType = "body", dataType = "org.make.api.tag.UpdateTagRequest")
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[tag.Tag])))
  @Path(value = "/{tagId}")
  def moderationUpdateTag: Route

  def routes: Route = moderationGetTag ~ moderationCreateTag ~ moderationlistTags ~ moderationUpdateTag
}

trait ModerationTagApiComponent {
  def moderationTagApi: ModerationTagApi
}

trait DefaultModerationTagApiComponent
    extends ModerationTagApiComponent
    with MakeAuthenticationDirectives
    with ParameterExtractors {
  this: MakeDirectivesDependencies
    with TagServiceComponent
    with QuestionServiceComponent
    with OperationOfQuestionServiceComponent =>

  override lazy val moderationTagApi: ModerationTagApi = new DefaultModerationTagApi

  class DefaultModerationTagApi extends ModerationTagApi {

    val moderationTagId: PathMatcher1[TagId] = Segment.map(id => TagId(id))

    override def moderationGetTag: Route = {
      get {
        path("moderation" / "tags" / moderationTagId) { tagId =>
          makeOperation("ModerationGetTag") { _ =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              requireModerationRole(userAuth.user) {
                tagService.getTag(tagId).asDirectiveOrNotFound { tag =>
                  complete(TagResponse(tag))
                }
              }
            }
          }
        }
      }
    }

    override def moderationCreateTag: Route = post {
      path("moderation" / "tags") {
        makeOperation("ModerationRegisterTag") { _ =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireAdminRole(userAuth.user) {
              decodeRequest {
                entity(as[CreateTagRequest]) { request: CreateTagRequest =>
                  (
                    tagService.findByLabel(request.label, like = false).asDirective,
                    request.questionId
                      .fold(Future.successful(Option.empty[Question]))(questionService.getQuestion(_))
                      .asDirectiveOrNotFound
                  ).tupled.flatMap({
                    case (tagList, question) =>
                      val duplicateLabel = tagList.find { tag =>
                        tag.questionId.isDefined && tag.questionId == request.questionId
                      }
                      Validation.validate(
                        Validation.requireNotPresent(
                          fieldName = "label",
                          fieldValue = duplicateLabel,
                          message = Some("Tag label already exist in this context. Duplicates are not allowed")
                        ),
                        Validation.validateUserInput("label", request.label, None)
                      )
                      onSuccess(
                        tagService.createTag(
                          label = request.label,
                          tagTypeId = request.tagTypeId,
                          question = question,
                          display = request.display.getOrElse(TagDisplay.Inherit),
                          weight = request.weight.getOrElse(0f)
                        )
                      )
                  })(tag => complete(StatusCodes.Created -> TagResponse(tag)))
                }
              }
            }
          }
        }
      }
    }

    override def moderationlistTags: Route = {
      get {
        path("moderation" / "tags") {
          makeOperation("ModerationSearchTag") { _ =>
            parameters(
              "_start".as[Pagination.Offset].?,
              "_end".as[End].?,
              "_sort".?,
              "_order".as[Order].?,
              "tagId".csv[TagId],
              "label".?,
              "tagTypeId".as[TagTypeId].?,
              "questionId".as[QuestionId].?,
              "questionEndAfter".as[ZonedDateTime].?
            ) {
              (
                offset: Option[Pagination.Offset],
                end: Option[Pagination.End],
                sort: Option[String],
                order: Option[Order],
                tagIds: Option[Seq[TagId]],
                maybeLabel: Option[String],
                maybeTagTypeId: Option[TagTypeId],
                maybeQuestionId: Option[QuestionId],
                questionEndAfter: Option[ZonedDateTime]
              ) =>
                makeOAuth2 { userAuth: AuthInfo[UserRights] =>
                  requireModerationRole(userAuth.user) {
                    val futureQuestions =
                      (maybeQuestionId, questionEndAfter) match {
                        case (None, None) => Future.successful(Option.empty[Seq[OperationOfQuestion]])
                        case _ =>
                          operationOfQuestionService
                            .find(
                              offset = Pagination.Offset.zero,
                              end = None,
                              sort = None,
                              order = None,
                              SearchOperationsOfQuestions(
                                questionIds = maybeQuestionId.map(Seq(_)),
                                endAfter = questionEndAfter
                              )
                            )
                            .map(Option(_))
                      }
                    futureQuestions.asDirective
                      .flatMap(questions => {
                        val filter = TagFilter(
                          tagIds = tagIds,
                          label = maybeLabel,
                          tagTypeId = maybeTagTypeId,
                          questionIds = questions.map(_.map(_.questionId))
                        )
                        (
                          tagService.count(filter).asDirective,
                          tagService
                            .find(offset = offset.orZero, end = end, sort = sort, order = order, tagFilter = filter)
                            .asDirective
                        ).mapN(
                          (count, filteredTags) =>
                            (StatusCodes.OK, List(`X-Total-Count`(count.toString)), filteredTags.map(TagResponse.apply))
                        )
                      })
                      .apply(complete(_))
                  }
                }
            }
          }
        }
      }
    }

    override def moderationUpdateTag: Route = put {
      path("moderation" / "tags" / moderationTagId) { tagId =>
        makeOperation("ModerationUpdateTag") { requestContext =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireAdminRole(auth.user) {
              decodeRequest {
                entity(as[UpdateTagRequest]) { request: UpdateTagRequest =>
                  (
                    tagService
                      .findByLabel(request.label, like = false)
                      .asDirective,
                    request.questionId
                      .map(questionId => questionService.getQuestion(questionId))
                      .getOrElse(Future.successful(None))
                      .asDirectiveOrNotFound
                  ).mapN({
                      case (tagList, question) =>
                        val duplicateLabel = tagList.find { tag =>
                          tag.tagId != tagId && tag.questionId.isDefined && tag.questionId == request.questionId
                        }
                        Validation.validate(
                          Validation.requireNotPresent(
                            fieldName = "label",
                            fieldValue = duplicateLabel,
                            message = Some("Tag label already exist in this context. Duplicates are not allowed")
                          )
                        )
                        tagService
                          .updateTag(
                            tagId = tagId,
                            label = request.label,
                            display = request.display,
                            tagTypeId = request.tagTypeId,
                            weight = request.weight,
                            question = question,
                            requestContext = requestContext
                          )
                          .asDirectiveOrNotFound
                    })
                    .flatten
                    .apply(tag => complete(TagResponse(tag)))
                }
              }
            }
          }
        }
      }
    }
  }
}

final case class CreateTagRequest(
  label: String,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  tagTypeId: TagTypeId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  questionId: Option[QuestionId],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = TagDisplay.swaggerAllowableValues)
  display: Option[TagDisplay],
  @(ApiModelProperty @field)(dataType = "float") weight: Option[Float]
) {

  questionId.getValidated("question", None, Some("question should not be empty")).throwIfInvalid()
}

object CreateTagRequest {
  implicit val decoder: Decoder[CreateTagRequest] = deriveDecoder[CreateTagRequest]
}

final case class UpdateTagRequest(
  label: String,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  tagTypeId: TagTypeId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  questionId: Option[QuestionId],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = TagDisplay.swaggerAllowableValues)
  display: TagDisplay,
  weight: Float
) {

  questionId.getValidated("question", None, Some("question should not be empty")).throwIfInvalid()
}

object UpdateTagRequest {
  implicit val decoder: Decoder[UpdateTagRequest] = deriveDecoder[UpdateTagRequest]
}
