/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.organisation

import cats.implicits._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import eu.timepit.refined.W
import eu.timepit.refined.api.Refined
import eu.timepit.refined.boolean.And
import eu.timepit.refined.collection.MaxSize
import eu.timepit.refined.string.Url
import grizzled.slf4j.Logging
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import io.circe.refined._
import io.swagger.annotations._

import javax.ws.rs.Path
import org.make.api.technical.CsvReceptacle._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.user.DefaultPersistentUserServiceComponent.PersistentUser
import org.make.api.user.{ProfileRequest, ProfileResponse}
import org.make.core.technical.Pagination
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils
import org.make.core.Validation._
import org.make.core.auth.UserRights
import org.make.core.reference.{Country, Language}
import org.make.core.user.{User, UserId}
import org.make.core.{CirceFormatters, HttpCodes, Order, ParameterExtractors}
import scalaoauth2.provider.AuthInfo

import scala.annotation.meta.field

@Api(
  value = "Moderation Organisation",
  authorizations = Array(
    new Authorization(
      value = "MakeApi",
      scopes = Array(
        new AuthorizationScope(scope = "admin", description = "BO Admin"),
        new AuthorizationScope(scope = "moderator", description = "BO Moderator")
      )
    )
  )
)
@Path(value = "/moderation/organisations")
trait ModerationOrganisationApi extends Directives {

  @ApiOperation(value = "post-organisation", httpMethod = "POST", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[OrganisationIdResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "body",
        paramType = "body",
        dataType = "org.make.api.organisation.ModerationCreateOrganisationRequest"
      )
    )
  )
  @Path(value = "/")
  def moderationPostOrganisation: Route

  @ApiOperation(value = "put-organisation", httpMethod = "PUT", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[OrganisationIdResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "body",
        paramType = "body",
        dataType = "org.make.api.organisation.ModerationUpdateOrganisationRequest"
      ),
      new ApiImplicitParam(name = "organisationId", paramType = "path", dataType = "string")
    )
  )
  @Path(value = "/{organisationId}")
  def moderationPutOrganisation: Route

  @ApiOperation(value = "get-organisation", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(new ApiImplicitParam(name = "organisationId", paramType = "path", dataType = "string"))
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[OrganisationResponse]))
  )
  @Path(value = "/{organisationId}")
  def moderationGetOrganisation: Route

  @ApiOperation(value = "get-organisations", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = PersistentUser.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "id", paramType = "query", dataType = "string", allowMultiple = true)
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[OrganisationResponse]]))
  )
  @Path(value = "/")
  def moderationGetOrganisations: Route

  def routes: Route =
    moderationPostOrganisation ~ moderationPutOrganisation ~ moderationGetOrganisation ~ moderationGetOrganisations

}

trait ModerationOrganisationApiComponent {
  def moderationOrganisationApi: ModerationOrganisationApi
}

trait DefaultModerationOrganisationApiComponent
    extends ModerationOrganisationApiComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {
  this: MakeDirectivesDependencies with OrganisationServiceComponent =>

  override lazy val moderationOrganisationApi: ModerationOrganisationApi = new DefaultModerationOrganisationApi

  class DefaultModerationOrganisationApi extends ModerationOrganisationApi {

    private val organisationId: PathMatcher1[UserId] = Segment.map(id => UserId(id))

    def moderationPostOrganisation: Route = {
      post {
        path("moderation" / "organisations") {
          makeOperation("ModerationPostOrganisation") { requestContext =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireAdminRole(auth.user) {
                decodeRequest {
                  entity(as[ModerationCreateOrganisationRequest]) { request: ModerationCreateOrganisationRequest =>
                    onSuccess(
                      organisationService
                        .register(
                          OrganisationRegisterData(
                            name = request.organisationName.value,
                            email = request.email,
                            password = request.password,
                            avatar = request.avatarUrl.map(_.value),
                            description = request.description.map(_.value),
                            country = request.country.orElse(requestContext.country).getOrElse(Country("FR")),
                            language = request.language.getOrElse(Language("fr")),
                            website = request.website.map(_.value)
                          ),
                          requestContext
                        )
                    ) { result =>
                      complete(StatusCodes.Created -> OrganisationIdResponse(result.userId))
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    def moderationPutOrganisation: Route = {
      put {
        path("moderation" / "organisations" / organisationId) { organisationId =>
          makeOperation("ModerationUpdateOrganisation") { requestContext =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireAdminRole(auth.user) {
                decodeRequest {
                  entity(as[ModerationUpdateOrganisationRequest]) { request: ModerationUpdateOrganisationRequest =>
                    organisationService.getOrganisation(organisationId).asDirectiveOrNotFound { organisation =>
                      onSuccess(
                        organisationService
                          .update(
                            organisation = organisation.copy(
                              organisationName = Some(request.organisationName.value),
                              email = request.email.getOrElse(organisation.email).toLowerCase,
                              profile = request.profile.flatMap(_.toProfile)
                            ),
                            moderatorId = Some(auth.user.userId),
                            oldEmail = organisation.email,
                            requestContext = requestContext
                          )
                      ) { organisationId =>
                        complete(StatusCodes.OK -> OrganisationIdResponse(organisationId))
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    def moderationGetOrganisation: Route =
      get {
        path("moderation" / "organisations" / organisationId) { organisationId =>
          makeOperation("ModerationGetOrganisation") { _ =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireAdminRole(auth.user) {
                organisationService.getOrganisation(organisationId).asDirectiveOrNotFound { user =>
                  complete(OrganisationResponse(user))
                }
              }
            }
          }
        }
      }

    def moderationGetOrganisations: Route =
      get {
        path("moderation" / "organisations") {
          makeOperation("ModerationGetOrganisations") { _ =>
            parameters(
              "_start".as[Pagination.Offset].?,
              "_end".as[Pagination.End].?,
              "_sort".?,
              "_order".as[Order].?,
              "id".csv[UserId],
              "organisationName".?
            ) {
              (
                offset: Option[Pagination.Offset],
                end: Option[Pagination.End],
                sort: Option[String],
                order: Option[Order],
                ids: Option[Seq[UserId]],
                organisationName: Option[String]
              ) =>
                makeOAuth2 { auth: AuthInfo[UserRights] =>
                  requireAdminRole(auth.user) {
                    (
                      organisationService.count(ids, organisationName).asDirective,
                      organisationService.find(offset.orZero, end, sort, order, ids, organisationName).asDirective
                    ).tupled.apply({
                      case (count, result) =>
                        complete(
                          (
                            StatusCodes.OK,
                            List(`X-Total-Count`(count.toString)),
                            result.map(OrganisationResponse.apply)
                          )
                        )
                    })
                  }
                }
            }
          }
        }
      }
  }
}

@ApiModel
final case class ModerationCreateOrganisationRequest(
  @(ApiModelProperty @field)(dataType = "string", required = true)
  organisationName: String Refined MaxSize[W.`256`.T],
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true)
  email: String,
  @(ApiModelProperty @field)(dataType = "string")
  password: Option[String],
  @(ApiModelProperty @field)(dataType = "string")
  description: Option[String Refined MaxSize[W.`450`.T]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png")
  avatarUrl: Option[String Refined And[Url, MaxSize[W.`2048`.T]]],
  @(ApiModelProperty @field)(dataType = "string", example = "FR")
  country: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr")
  language: Option[Language],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String Refined Url]
) {
  OrganisationValidation.validateCreate(
    organisationName = organisationName.value,
    email = email,
    description = description.map(_.value)
  )
}

object ModerationCreateOrganisationRequest {
  implicit val decoder: Decoder[ModerationCreateOrganisationRequest] =
    deriveDecoder[ModerationCreateOrganisationRequest]
}

final case class ModerationUpdateOrganisationRequest(
  @(ApiModelProperty @field)(dataType = "string", required = true)
  organisationName: String Refined MaxSize[W.`256`.T],
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org")
  email: Option[String] = None,
  profile: Option[ProfileRequest]
) {
  OrganisationValidation.validateUpdate(
    organisationName = organisationName.value,
    email = email,
    description = profile.flatMap(_.description.map(_.value)),
    profile
  )
}

object ModerationUpdateOrganisationRequest {
  implicit val decoder: Decoder[ModerationUpdateOrganisationRequest] =
    deriveDecoder[ModerationUpdateOrganisationRequest]
}

private object OrganisationValidation {
  def validateCreate(organisationName: String, email: String, description: Option[String]): Unit = {
    email.toEmail.throwIfInvalid()
    organisationName.toSanitizedInput("organisationName").throwIfInvalid()
    description.foreach(_.toSanitizedInput("description").throwIfInvalid())
  }

  def validateUpdate(
    organisationName: String,
    email: Option[String],
    description: Option[String],
    profileRequest: Option[ProfileRequest]
  ): Unit = {
    organisationName.toSanitizedInput("organisationName").throwIfInvalid()
    description.foreach(_.toSanitizedInput("description").throwIfInvalid())
    email.foreach(_.toEmail.throwIfInvalid())
    profileRequest.foreach(ProfileRequest.validateProfileRequest)
  }
}

final case class OrganisationResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: UserId,
  @(ApiModelProperty @field)(dataType = "string", example = "yopmail+test@make.org", required = true)
  email: String,
  organisationName: Option[String],
  profile: Option[ProfileResponse],
  @(ApiModelProperty @field)(dataType = "string", example = "FR", required = true)
  country: Country
)

object OrganisationResponse extends CirceFormatters {
  implicit val encoder: Encoder[OrganisationResponse] = deriveEncoder[OrganisationResponse]
  implicit val decoder: Decoder[OrganisationResponse] = deriveDecoder[OrganisationResponse]

  def apply(user: User): OrganisationResponse = OrganisationResponse(
    id = user.userId,
    email = user.email,
    organisationName = user.organisationName,
    profile = user.profile.map(ProfileResponse.fromProfile),
    country = user.country
  )
}

final case class OrganisationProfileRequest(
  organisationName: String,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png")
  avatarUrl: Option[String Refined Url],
  description: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String Refined Url],
  optInNewsletter: Boolean,
  @(ApiModelProperty @field)(dataType = "string", example = "FR") crmCountry: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr") crmLanguage: Option[Language]
) {
  private val maxDescriptionLength = 450

  Seq(
    description.map(_.withMaxLength(maxDescriptionLength, "description").void),
    description.map(_.toSanitizedInput("description").void)
  ).flatten.foreach(_.throwIfInvalid())

  (organisationName.toNonEmpty("organisationName"), organisationName.toSanitizedInput("firstName")).tupled
    .throwIfInvalid()
}

object OrganisationProfileRequest {
  implicit val encoder: Encoder[OrganisationProfileRequest] = deriveEncoder[OrganisationProfileRequest]
  implicit val decoder: Decoder[OrganisationProfileRequest] = deriveDecoder[OrganisationProfileRequest]
}

final case class OrganisationProfileResponse(
  organisationName: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/avatar.png")
  avatarUrl: Option[String],
  description: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/website")
  website: Option[String],
  optInNewsletter: Boolean,
  @(ApiModelProperty @field)(dataType = "string", example = "FR") crmCountry: Option[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr") crmLanguage: Option[Language]
)

object OrganisationProfileResponse {
  implicit val encoder: Encoder[OrganisationProfileResponse] = deriveEncoder[OrganisationProfileResponse]
  implicit val decoder: Decoder[OrganisationProfileResponse] = deriveDecoder[OrganisationProfileResponse]
}

final case class OrganisationIdResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true) id: UserId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true) userId: UserId
)

object OrganisationIdResponse {
  implicit val encoder: Encoder[OrganisationIdResponse] = deriveEncoder[OrganisationIdResponse]

  def apply(id: UserId): OrganisationIdResponse = OrganisationIdResponse(id, id)
}
