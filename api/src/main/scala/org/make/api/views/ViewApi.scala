/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.views

import cats.data.NonEmptyList
import cats.implicits._
import akka.http.scaladsl.server.{Directives, PathMatcher1, Route}
import com.sksamuel.elastic4s.requests.searches.suggestion.Fuzziness
import grizzled.slf4j.Logging
import io.swagger.annotations._

import javax.ws.rs.Path
import org.make.api.operation._
import org.make.api.organisation.{OrganisationServiceComponent, OrganisationsSearchResultResponse}
import org.make.api.proposal.ProposalServiceComponent
import org.make.api.question.{SearchQuestionResponse, SearchQuestionsResponse}
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.MakeAuthenticationDirectives
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.core.auth.UserRights
import org.make.core.technical.Pagination
import org.make.core.operation._
import org.make.core.proposal.{CountrySearchFilter, LanguageSearchFilter, SearchQuery}
import org.make.core.reference.{Country, Language}
import org.make.core.user.{OrganisationNameSearchFilter, OrganisationSearchFilters, OrganisationSearchQuery}
import org.make.core._
import scalaoauth2.provider.AuthInfo

import scala.concurrent.ExecutionContext.Implicits.global

@Api(value = "Home view")
@Path(value = "/views")
trait ViewApi extends Directives {

  @ApiOperation(value = "get-home-page-view", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "country", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "preferredLanguage", paramType = "query", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[HomePageViewResponse]))
  )
  @Path(value = "/home-page/{country}")
  def homePageView: Route

  @ApiOperation(value = "deprecated-get-home-page-view", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "country", paramType = "path", dataType = "string"),
      new ApiImplicitParam(name = "language", paramType = "path", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[HomePageViewResponse]))
  )
  @Path(value = "/home-page/{country}/{language}")
  def homePageViewWithLanguage: Route

  @ApiOperation(value = "get-search-view", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "content", paramType = "query", dataType = "string", required = true),
      new ApiImplicitParam(
        name = "proposalLimit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, 30000]"
      ),
      new ApiImplicitParam(
        name = "questionLimit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, 30000]"
      ),
      new ApiImplicitParam(
        name = "organisationLimit",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, 30000]"
      ),
      new ApiImplicitParam(name = "country", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "language", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "preferredLanguage", paramType = "query", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[SearchViewResponse]))
  )
  @Path(value = "/search")
  def searchView: Route

  @ApiOperation(value = "list-available-countries", httpMethod = "GET", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[AvailableCountry]]))
  )
  @Path(value = "/countries")
  def listAvailableCountries: Route

  def routes: Route = homePageView ~ homePageViewWithLanguage ~ searchView ~ listAvailableCountries
}

trait ViewApiComponent {
  def viewApi: ViewApi
}

trait DefaultViewApiComponent
    extends ViewApiComponent
    with MakeAuthenticationDirectives
    with ParameterExtractors
    with Logging {
  this: MakeDirectivesDependencies
    with HomeViewServiceComponent
    with OperationOfQuestionServiceComponent
    with ProposalServiceComponent
    with OrganisationServiceComponent =>

  override lazy val viewApi: ViewApi = new DefaultViewApi

  class DefaultViewApi extends ViewApi {

    private val country: PathMatcher1[Country] = Segment.map(Country.apply)
    private val language: PathMatcher1[Language] = Segment.map(Language.apply)

    override def homePageViewWithLanguage: Route = {
      get {
        path("views" / "home-page" / country / language) { (country, language) =>
          makeOperation("GetHomePageView") { _ =>
            homeViewService
              .getHomePageViewResponse(country, Some(language))
              .asDirective
              .apply(complete(_))
          }
        }
      }
    }

    override def homePageView: Route = {
      get {
        path("views" / "home-page" / country) { country =>
          parameters("preferredLanguage".as[Language].?) { maybePreferedLanguage =>
            makeOperation("GetHomePageView") { _ =>
              homeViewService
                .getHomePageViewResponse(country, maybePreferedLanguage)
                .asDirective
                .apply(complete(_))
            }
          }
        }
      }
    }

    override def searchView: Route = {
      get {
        path("views" / "search") {
          makeOperation("GetSearchView") { requestContext =>
            optionalMakeOAuth2 { auth: Option[AuthInfo[UserRights]] =>
              parameters(
                "content",
                "proposalLimit".as[Pagination.Limit].?,
                "questionLimit".as[Pagination.Limit].?,
                "organisationLimit".as[Pagination.Limit].?,
                "preferredLanguage".as[Language].?,
                "country".as[Country].?,
                "language".as[Language].?
              ) { (content, proposalLimit, questionLimit, organisationLimit, preferredLanguage, country, language) =>
                val proposalQuery = SearchQuery(
                  filters = Some(
                    proposal.SearchFilters(
                      content = Some(proposal.ContentSearchFilter(content.toLowerCase)),
                      operationKinds = Some(proposal.OperationKindsSearchFilter(OperationKind.publicKinds)),
                      country = country.map(CountrySearchFilter.apply),
                      languages = language.map(l => NonEmptyList.of(LanguageSearchFilter(l)))
                    )
                  ),
                  limit = proposalLimit,
                  language = requestContext.languageContext.language
                )
                val questionQuery = OperationOfQuestionSearchQuery(
                  filters = Some(
                    OperationOfQuestionSearchFilters(
                      question = Some(QuestionContentSearchFilter(content.toLowerCase, fuzzy = Some(Fuzziness.Auto))),
                      operationKinds = Some(operation.OperationKindsSearchFilter(OperationKind.publicKinds)),
                      country = country.map(operation.CountrySearchFilter.apply),
                      language = language.map(operation.LanguageSearchFilter.apply)
                    )
                  ),
                  limit = questionLimit
                )
                val organisationQuery = OrganisationSearchQuery(
                  filters = Some(
                    OrganisationSearchFilters(
                      organisationName = Some(OrganisationNameSearchFilter(content.toLowerCase)),
                      country = country.map(user.CountrySearchFilter.apply),
                      language = language.map(user.LanguageSearchFilter.apply)
                    )
                  ),
                  limit = organisationLimit
                )
                (
                  proposalService
                    .searchForUser(auth.map(_.user.userId), proposalQuery, requestContext, preferredLanguage, None)
                    .asDirective,
                  operationOfQuestionService.search(questionQuery).asDirective,
                  organisationService
                    .searchWithQuery(organisationQuery)
                    .map(OrganisationsSearchResultResponse.fromOrganisationSearchResult)
                    .asDirective
                ).mapN(
                    (proposals, questions, organisations) =>
                      SearchViewResponse(
                        proposals = proposals,
                        questions = SearchQuestionsResponse(
                          total = questions.total,
                          results = questions.results.map(SearchQuestionResponse.apply(_, preferredLanguage))
                        ),
                        organisations = organisations
                      )
                  )
                  .apply(complete(_))
              }
            }
          }
        }
      }
    }

    override def listAvailableCountries: Route = {
      get {
        path("views" / "countries") {
          makeOperation("ListAvailableCountries") { _ =>
            val query = OperationOfQuestionSearchQuery(filters = Some(
              OperationOfQuestionSearchFilters(
                operationKinds = Some(operation.OperationKindsSearchFilter(OperationKind.publicKinds)),
                status = Some(StatusSearchFilter(OperationOfQuestion.Status.Open, OperationOfQuestion.Status.Finished))
              )
            )
            )
            operationOfQuestionService
              .count(query)
              .flatMap(
                count =>
                  operationOfQuestionService
                    .search(query.copy(limit = Some(Pagination.Limit(count.toInt))))
                    .map(
                      _.results
                        .flatMap(
                          ooq =>
                            ooq.countries
                              .map(_.value)
                              .toList
                              .zip(LazyList.continually(ooq.status == OperationOfQuestion.Status.Open))
                        )
                        .foldLeft(Map.empty[String, AvailableCountry]) {
                          case (map, (country, open)) =>
                            map.updatedWith(country)(
                              existing =>
                                Some(AvailableCountry(country, open || existing.exists(_.activeConsultations)))
                            )
                        }
                        .values
                    )
              )
              .asDirective
              .apply(complete(_))
          }
        }
      }
    }
  }

}
