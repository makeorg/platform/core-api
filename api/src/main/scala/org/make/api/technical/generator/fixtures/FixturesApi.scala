/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.technical.generator.fixtures

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directives, Route}
import cats.data.{NonEmptyList => Nel}
import grizzled.slf4j.Logging
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import io.swagger.annotations._
import org.make.api.operation.OperationServiceComponent
import org.make.api.question.QuestionServiceComponent
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical._
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.core.{HttpCodes, Validation}
import org.make.core.operation.OperationId
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, Language}

import javax.ws.rs.Path
import scala.annotation.meta.field
import scala.concurrent.Future

@Api(value = "Fixtures")
@Path(value = "/fixtures")
trait FixturesApi extends Directives {
  @ApiOperation(value = "generate-fixtures", httpMethod = "POST", code = HttpCodes.Created)
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "body",
        paramType = "body",
        dataType = "org.make.api.technical.generator.fixtures.GenerateFixturesRequest"
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.Created, message = "Ok", response = classOf[FixtureResponse]))
  )
  @Path(value = "/generate")
  def generateFixtures: Route

  def routes: Route = generateFixtures
}

trait FixturesApiComponent {
  def fixturesApi: FixturesApi
}

trait DefaultFixturesApiComponent extends FixturesApiComponent with MakeAuthenticationDirectives with Logging {
  this: MakeDirectivesDependencies
    with FixturesServiceComponent
    with OperationServiceComponent
    with QuestionServiceComponent =>

  override lazy val fixturesApi: FixturesApi = new DefaultFixturesApi

  class DefaultFixturesApi extends FixturesApi {
    override def generateFixtures: Route = post {
      path("fixtures" / "generate") {
        makeOperation("GenerateFixtures") { _ =>
          withoutRequestTimeout {
            decodeRequest {
              entity(as[GenerateFixturesRequest]) { request =>
                val futureOperation = request.operationId match {
                  case Some(operationId) => operationService.findOne(operationId)
                  case None              => Future.successful(None)
                }
                val futureQuestion = request.questionId match {
                  case Some(questionId) => questionService.getQuestion(questionId)
                  case None             => Future.successful(None)
                }
                futureOperation.asDirective { operation =>
                  futureQuestion.asDirective { question =>
                    Validation.validateOptional(
                      request.questionId.map { qId =>
                        Validation.validateField(
                          "questionId",
                          "not_found",
                          question.isDefined,
                          s"Question ${qId.value} doesn't exist"
                        )
                      },
                      request.operationId.map { opId =>
                        Validation.validateField(
                          "operationId",
                          "not_found",
                          operation.isDefined,
                          s"Operation ${opId.value} doesn't exist"
                        )
                      }
                    )
                    fixturesService
                      .generate(
                        maybeOperationId = request.operationId,
                        maybeQuestionId = request.questionId,
                        forceCountries = request.forceCountries,
                        forceLanguage = request.forceLanguage
                      )
                      .asDirective { result =>
                        complete(StatusCodes.Created -> result)
                      }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

}

final case class GenerateFixturesRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  operationId: Option[OperationId],
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  questionId: Option[QuestionId],
  @(ApiModelProperty @field)(dataType = "list[string]")
  forceCountries: Option[Nel[Country]],
  @(ApiModelProperty @field)(dataType = "string", example = "fr")
  forceLanguage: Option[Language]
)

object GenerateFixturesRequest {
  implicit val decoder: Decoder[GenerateFixturesRequest] = deriveDecoder[GenerateFixturesRequest]
}

final case class FixtureResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  operationId: OperationId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId,
  userCount: Int,
  organisationCount: Int,
  partnerCount: Int,
  tagCount: Int,
  proposalCount: Int,
  organisationsVoteCount: Int
)

object FixtureResponse {
  implicit val encoder: Encoder[FixtureResponse] = deriveEncoder[FixtureResponse]
}
