/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.technical.directives

import cats.Monad
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import org.make.core.{ValidationError, ValidationFailedError}

import scala.concurrent.Future
import scala.util.{Failure, Success}

object FutureDirectivesExtensions extends Directives {

  implicit class FutureWithRoutes[T](f: Future[T]) {

    def asDirective: Directive1[T] =
      extract(_ => f).flatMap { fa =>
        onComplete(fa).flatMap {
          case Success(value) => provide(value)
          case Failure(e)     => failWith(e)
        }
      }
  }

  implicit class FutureOptionWithRoutes[T](f: Future[Option[T]]) {

    def asDirectiveOrNotFound: Directive1[T] =
      f.asDirective.flatMap {
        case Some(value) => provide(value)
        case None        => complete(StatusCodes.NotFound)
      }

    def asDirectiveOrBadRequest(err: ValidationError): Directive1[T] =
      f.asDirective.flatMap {
        case Some(value) => provide(value)
        case None        => failWith(ValidationFailedError(Seq(err)))
      }
  }

  implicit val directive1Monad: Monad[Directive1] = new Monad[Directive1] {

    override def pure[A](x: A): Directive1[A] = provide(x)

    override def flatMap[A, B](fa: Directive1[A])(f: A => Directive1[B]): Directive1[B] =
      fa.flatMap(f)

    // This method implementation isn't actually tail-recursive because we were unable to make it
    // so. That completely ruins the point of this method even existing, but it is required, and we
    // figured a flawed implementation would be better than no implementation (`???`).
    @SuppressWarnings(Array("org.wartremover.warts.Recursion"))
    override def tailRecM[A, B](a: A)(f: A => Directive1[Either[A, B]]): Directive1[B] = {
      f(a).flatMap {
        _.fold(tailRecM(_)(f), pure)
      }
    }
  }
}
