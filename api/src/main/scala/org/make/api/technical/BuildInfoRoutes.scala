/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.technical

import akka.http.scaladsl.server.Route
import buildinfo.BuildInfo
import io.circe.{Encoder, Json}
import io.circe.generic.semiauto.deriveEncoder
import io.circe.syntax._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies

import scala.util.Using

trait BuildInfoRoutes extends MakeDirectives {
  this: MakeDirectivesDependencies =>

  val buildRoutes: Route = buildInfo

  private lazy val legacyJsonVersion = BuildInformation().asJson
  private val jsonVersion: Json = Using(scala.io.Source.fromFile("/opt/docker/version")) { source =>
    io.circe.parser.parse(source.mkString).toTry
  }.flatten.getOrElse(legacyJsonVersion)

  def buildInfo: Route = get {
    path("version") {
      makeOperation("version") { _ =>
        complete(jsonVersion)
      }
    }
  }
}

final case class BuildInformation(
  name: String = BuildInfo.name,
  version: String = BuildInfo.version,
  gitHeadCommit: String = BuildInfo.gitHeadCommit.getOrElse("no commit information"),
  gitBranch: String = BuildInfo.gitCurrentBranch,
  buildTime: String = BuildInfo.buildTime
)

object BuildInformation {
  implicit val encoder: Encoder[BuildInformation] = deriveEncoder
}
