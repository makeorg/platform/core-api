/*
 *  Make.org Core API
 *  Copyright (C) 2020 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.technical.graphql

import caliban.Value
import caliban.schema._
import caliban.CalibanError.ExecutionError
import org.make.core.technical.Pagination
import io.circe.Json

object GraphQLArgBuilders {

  implicit val limitArgBuilder: ArgBuilder[Pagination.Limit] = {
    case Value.StringValue(value) =>
      Pagination.limitDecoder
        .decodeJson(Json.fromString(value))
        .fold(ex => Left(ExecutionError(s"Can't parse $value into a Limit")), Right(_))
    case other => Left(ExecutionError(s"Can't build a Limit from input $other"))
  }

  implicit val offsetArgBuilder: ArgBuilder[Pagination.Offset] = {
    case Value.StringValue(value) =>
      Pagination.offsetDecoder
        .decodeJson(Json.fromString(value))
        .fold(ex => Left(ExecutionError(s"Can't parse $value into an Offset")), Right(_))
    case other => Left(ExecutionError(s"Can't build an Offset from input $other"))
  }
}
