/*
 *  Make.org Core API
 *  Copyright (C) 2021 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.demographics

import cats.implicits._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import io.circe.generic.semiauto.{deriveCodec, deriveDecoder}
import io.circe.{Codec, Decoder}
import io.swagger.annotations._
import org.make.api.question.QuestionServiceComponent

import javax.ws.rs.Path
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.core.Validation._
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.core.auth.UserRights
import org.make.core.demographics._
import org.make.core.question.QuestionId
import org.make.core.{HttpCodes, Order, ParameterExtractors, ValidationError}
import scalaoauth2.provider.AuthInfo
import org.make.core.technical.Pagination

import scala.annotation.meta.field

@Api(value = "Admin Active Demographics Cards")
@Path(value = "/admin/active-demographics-cards")
trait AdminActiveDemographicsCardApi extends Directives {

  @ApiOperation(
    value = "get-active-demographics-card",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[ActiveDemographicsCardResponse]))
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "id", paramType = "path", dataType = "string")))
  @Path(value = "/{id}")
  def getById: Route

  @ApiOperation(
    value = "create-active-demographics-Card",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.demographics.CreateActiveDemographicsCardRequest"
      )
    )
  )
  @ApiResponses(
    value = Array(
      new ApiResponse(code = HttpCodes.Created, message = "Ok", response = classOf[ActiveDemographicsCardResponse])
    )
  )
  @Path(value = "/")
  def create: Route

  @ApiOperation(
    value = "list-active-demographics-cards",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = "id, demographicsCardId, questionId"
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "questionId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "cardId", paramType = "query", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(
      new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[ActiveDemographicsCardResponse]])
    )
  )
  @Path(value = "/")
  def list: Route

  @ApiOperation(
    value = "delete-active-demographics-card",
    httpMethod = "DELETE",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "id", paramType = "path", dataType = "string")))
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "")))
  @Path(value = "/{id}")
  def remove: Route

  def routes: Route = getById ~ create ~ list ~ remove
}

trait AdminActiveDemographicsCardApiComponent {
  def adminActiveDemographicsCardApi: AdminActiveDemographicsCardApi
}

trait DefaultAdminActiveDemographicsCardApiComponent
    extends AdminActiveDemographicsCardApiComponent
    with MakeAuthenticationDirectives
    with ParameterExtractors {
  this: MakeDirectivesDependencies
    with ActiveDemographicsCardServiceComponent
    with DemographicsCardServiceComponent
    with QuestionServiceComponent =>

  override lazy val adminActiveDemographicsCardApi: AdminActiveDemographicsCardApi =
    new AdminActiveDemographicsCardApi {

      val id: PathMatcher1[ActiveDemographicsCardId] = Segment.map(ActiveDemographicsCardId.apply)

      override def getById: Route = {
        get {
          path("admin" / "active-demographics-cards" / id) { id =>
            makeOperation("AdminGetActiveDemographicsCard") { _ =>
              makeOAuth2 { userAuth: AuthInfo[UserRights] =>
                requireAdminRole(userAuth.user) {
                  activeDemographicsCardService.get(id).asDirectiveOrNotFound { activeDemographicsCard =>
                    complete(ActiveDemographicsCardResponse(activeDemographicsCard))
                  }
                }
              }
            }
          }
        }
      }

      override def create: Route = post {
        path("admin" / "active-demographics-cards") {
          makeOperation("AdminCreateActiveDemographicsCard") { _ =>
            makeOAuth2 { userAuth: AuthInfo[UserRights] =>
              requireAdminRole(userAuth.user) {
                decodeRequest {
                  entity(as[CreateActiveDemographicsCardRequest]) { request =>
                    val cardIdErr = ValidationError(
                      "demographicsCardId",
                      "not_found",
                      Some(s"Demographics card ${request.demographicsCardId.value} doesn't exist")
                    )
                    val questionIdErr = ValidationError(
                      "questionId",
                      "not_found",
                      Some(s"Question ${request.questionId.value} doesn't exist")
                    )
                    (
                      demographicsCardService
                        .get(request.demographicsCardId)
                        .asDirectiveOrBadRequest(cardIdErr),
                      questionService
                        .getCachedQuestion(request.questionId)
                        .asDirectiveOrBadRequest(questionIdErr),
                      activeDemographicsCardService
                        .list(questionId = Some(request.questionId), cardId = Some(request.demographicsCardId))
                        .asDirective
                    ).mapN(
                        (_, _, list) =>
                          list
                            .toEmpty(
                              "questionId",
                              Some(
                                s"An active card already exists for questionId ${request.questionId} and demographicsCardId ${request.demographicsCardId}"
                              )
                            )
                            .toValidationEither match {
                            case Left(e) => failWith(e): Directive1[ActiveDemographicsCard]
                            case Right(_) =>
                              activeDemographicsCardService
                                .create(request.demographicsCardId, request.questionId)
                                .asDirective
                          }
                      )
                      .flatten
                      .apply(
                        (card: ActiveDemographicsCard) =>
                          complete(StatusCodes.Created -> ActiveDemographicsCardResponse(card))
                      )
                  }
                }
              }
            }
          }
        }
      }

      override def list: Route = {
        get {
          path("admin" / "active-demographics-cards") {
            makeOperation("AdminListActiveDemographicsCard") { _ =>
              parameters(
                "_start".as[Pagination.Offset].?,
                "_end".as[Pagination.End].?,
                "_sort".?,
                "_order".as[Order].?,
                "questionId".as[QuestionId].?,
                "cardId".as[DemographicsCardId].?
              ) {
                (
                  offset: Option[Pagination.Offset],
                  end: Option[Pagination.End],
                  sort: Option[String],
                  order: Option[Order],
                  questionId: Option[QuestionId],
                  cardId: Option[DemographicsCardId]
                ) =>
                  makeOAuth2 { userAuth: AuthInfo[UserRights] =>
                    requireAdminRole(userAuth.user) {
                      (
                        activeDemographicsCardService.count(questionId, cardId).asDirective,
                        activeDemographicsCardService
                          .list(offset, end, sort, order, questionId, cardId)
                          .asDirective
                      ).tupled.apply({
                        case (count: Int, activeDemographicsCards: Seq[ActiveDemographicsCard]) =>
                          complete(
                            (
                              StatusCodes.OK,
                              List(`X-Total-Count`(count.toString)),
                              activeDemographicsCards.map(ActiveDemographicsCardResponse.apply)
                            )
                          )
                      })
                    }
                  }
              }
            }
          }
        }
      }

      override def remove: Route = delete {
        path("admin" / "active-demographics-cards" / id) { id =>
          makeOperation("AdminDeleteActiveDemographicsCard") { _ =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireAdminRole(auth.user) {
                (
                  activeDemographicsCardService.get(id).asDirectiveOrNotFound,
                  activeDemographicsCardService.delete(id).asDirective
                ).tupled.apply(_ => complete(StatusCodes.NoContent))
              }
            }
          }
        }
      }
    }
}

final case class CreateActiveDemographicsCardRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  demographicsCardId: DemographicsCardId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  questionId: QuestionId
)

object CreateActiveDemographicsCardRequest {
  implicit val decoder: Decoder[CreateActiveDemographicsCardRequest] = deriveDecoder
}

final case class ActiveDemographicsCardResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  id: ActiveDemographicsCardId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  demographicsCardId: DemographicsCardId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  questionId: QuestionId
)

object ActiveDemographicsCardResponse {

  def apply(activeDemographicsCard: ActiveDemographicsCard): ActiveDemographicsCardResponse =
    ActiveDemographicsCardResponse(
      id = activeDemographicsCard.id,
      demographicsCardId = activeDemographicsCard.demographicsCardId,
      questionId = activeDemographicsCard.questionId
    )

  implicit val codec: Codec[ActiveDemographicsCardResponse] = deriveCodec

}
