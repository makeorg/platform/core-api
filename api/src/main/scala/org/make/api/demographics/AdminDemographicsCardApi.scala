/*
 *  Make.org Core API
 *  Copyright (C) 2021 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.demographics

import cats.data.NonEmptyList
import cats.implicits._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto.autoUnwrap
import eu.timepit.refined.collection.MaxSize
import io.circe.generic.semiauto.deriveCodec
import io.circe.refined._
import io.circe.Codec
import io.swagger.annotations._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils
import org.make.api.technical.CsvReceptacle._
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.core._
import org.make.core.Validation._
import org.make.core.technical.Multilingual
import org.make.core.auth.UserRights
import org.make.core.demographics.DemographicsCard.Layout
import org.make.core.demographics.{DemographicsCard, DemographicsCardId, LabelsValue}
import org.make.core.reference.Language
import org.make.core.technical.Pagination
import scalaoauth2.provider.AuthInfo

import javax.ws.rs.Path
import scala.annotation.meta.field

@Api(value = "Admin DemographicsCards")
@Path(value = "/admin/demographics-cards")
trait AdminDemographicsCardApi extends Directives {

  @ApiOperation(
    value = "list-demographics-cards",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "operator", description = "BO Operator")
        )
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = "name, layout, dataType, language, title, createdAt, updatedAt"
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "languages", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(name = "dataType", paramType = "query", dataType = "string")
    )
  )
  @ApiResponses(
    value = Array(
      new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[AdminDemographicsCardResponse]])
    )
  )
  @Path(value = "/")
  def list: Route

  @ApiOperation(
    value = "get-demographics-card-by-id",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "id", paramType = "path", dataType = "string")))
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[AdminDemographicsCardResponse]))
  )
  @Path(value = "/{id}")
  def getById: Route

  @ApiOperation(
    value = "create-demographics-card",
    httpMethod = "POST",
    code = HttpCodes.Created,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.demographics.AdminDemographicsCardRequest"
      )
    )
  )
  @ApiResponses(
    value = Array(
      new ApiResponse(code = HttpCodes.Created, message = "Ok", response = classOf[AdminDemographicsCardResponse])
    )
  )
  @Path(value = "/")
  def create: Route

  @ApiOperation(
    value = "update-demographics-card",
    httpMethod = "PUT",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "id", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        dataType = "org.make.api.demographics.AdminDemographicsCardRequest"
      )
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[AdminDemographicsCardResponse]))
  )
  @Path(value = "/{id}")
  def update: Route

  def routes: Route = list ~ getById ~ create ~ update

}

trait AdminDemographicsCardApiComponent {
  def adminDemographicsCardApi: AdminDemographicsCardApi
}

trait DefaultAdminDemographicsCardApiComponent
    extends AdminDemographicsCardApiComponent
    with MakeAuthenticationDirectives
    with ParameterExtractors {
  self: MakeDirectivesDependencies with DemographicsCardServiceComponent with ActiveDemographicsCardServiceComponent =>

  override val adminDemographicsCardApi: AdminDemographicsCardApi = new AdminDemographicsCardApi {

    private val id: PathMatcher1[DemographicsCardId] = Segment.map(DemographicsCardId.apply)

    override def list: Route = get {
      path("admin" / "demographics-cards") {
        parameters(
          "_start".as[Pagination.Offset].?,
          "_end".as[Pagination.End].?,
          "_sort".?,
          "_order".as[Order].?,
          "languages".csv[Language],
          "dataType".?,
          "sessionBindingMode".as[Boolean].?
        ) {
          (
            offset: Option[Pagination.Offset],
            end: Option[Pagination.End],
            sort: Option[String],
            order: Option[Order],
            languages: Option[Seq[Language]],
            dataType: Option[String],
            sessionBindingMode: Option[Boolean]
          ) =>
            makeOperation("AdminDemographicsCardsList") { _ =>
              makeOAuth2 { userAuth: AuthInfo[UserRights] =>
                requireAdminRole(userAuth.user) {
                  val languagesList = languages.map(_.toList)
                  (
                    demographicsCardService.count(languagesList, dataType, sessionBindingMode).asDirective,
                    demographicsCardService
                      .list(offset, end, sort, order, languagesList, dataType, sessionBindingMode)
                      .asDirective
                  ).tupled.apply({
                    case (count: Int, demographicsCards: Seq[DemographicsCard]) =>
                      complete(
                        (
                          StatusCodes.OK,
                          List(`X-Total-Count`(count.toString)),
                          demographicsCards.map(AdminDemographicsCardResponse.apply)
                        )
                      )
                  })
                }
              }
            }
        }
      }
    }

    override def getById: Route = get {
      path("admin" / "demographics-cards" / id) { id =>
        makeOperation("AdminDemographicsCardsGetById") { _ =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireAdminRole(userAuth.user) {
              demographicsCardService
                .get(id)
                .asDirectiveOrNotFound
                .apply(demographicsCard => complete(AdminDemographicsCardResponse(demographicsCard)))
            }
          }
        }
      }
    }

    override def create: Route = post {
      path("admin" / "demographics-cards") {
        makeOperation("AdminDemographicsCardsCreate") { _ =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireOperatorRole(userAuth.user) {
              decodeRequest {
                entity(as[AdminDemographicsCardRequest]) { request: AdminDemographicsCardRequest =>
                  Validation.validate(request.validateParameters: _*)
                  demographicsCardService
                    .create(
                      name = request.name,
                      layout = request.layout,
                      dataType = request.dataType,
                      languages = request.languages,
                      titles = request.titles.mapTranslations(autoUnwrap(_)),
                      sessionBindingMode = request.sessionBindingMode.getOrElse(false),
                      parameters = request.parameters
                    )
                    .asDirective
                    .apply(
                      demographicsCard => complete(StatusCodes.Created, AdminDemographicsCardResponse(demographicsCard))
                    )
                }
              }
            }
          }
        }
      }
    }

    override def update: Route = put {
      path("admin" / "demographics-cards" / id) { id =>
        makeOperation("AdminDemographicsCardsUpdate") { _ =>
          makeOAuth2 { userAuth: AuthInfo[UserRights] =>
            requireOperatorRole(userAuth.user) {
              decodeRequest {
                entity(as[AdminDemographicsCardRequest]) { request: AdminDemographicsCardRequest =>
                  Validation.validate(request.validateParameters: _*)
                  (
                    demographicsCardService.get(id).asDirectiveOrNotFound,
                    activeDemographicsCardService.count(questionId = None, cardId = Some(id)).asDirective
                  ).mapN((demographicsCard, numberOfActiveCards) => {
                      val previousLanguages = demographicsCard.languages.toList.toSet
                      val newLanguages = request.languages.toList.toSet
                      (
                        request.titles.providedLanguages.predicateValidated(
                          providedLanguages => (newLanguages -- providedLanguages).isEmpty,
                          "demographics_card",
                          "missing_translation"
                        ),
                        request.parameters.traverse(
                          _.label.providedLanguages.predicateValidated(
                            providedLanguages => (newLanguages -- providedLanguages).isEmpty,
                            "demographics_card",
                            "missing_translation"
                          )
                        ),
                        (previousLanguages -- newLanguages).predicateValidated(
                          languagesRemoved => languagesRemoved.isEmpty || numberOfActiveCards === 0,
                          "demographics_card",
                          "existing_cards",
                          Some("Cannot remove languages: at least one consultation are using this card")
                        )
                      ).tupled.throwIfInvalid()
                      demographicsCardService
                        .update(
                          id = id,
                          name = request.name,
                          layout = request.layout,
                          dataType = request.dataType,
                          languages = request.languages,
                          titles = request.titles.mapTranslations(autoUnwrap(_)),
                          sessionBindingMode = request.sessionBindingMode.getOrElse(false),
                          parameters = request.parameters
                        )
                        .asDirectiveOrNotFound
                    })
                    .flatten
                    .apply(demographicsCard => complete(AdminDemographicsCardResponse(demographicsCard)))
                }
              }
            }
          }
        }
      }
    }

  }

}

final case class AdminDemographicsCardRequest(
  name: String Refined MaxSize[256],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = Layout.swaggerAllowableValues, required = true)
  layout: Layout,
  @(ApiModelProperty @field)(dataType = "string", allowableValues = "age, gender, region", required = true)
  dataType: String Refined Slug,
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  languages: NonEmptyList[Language],
  titles: Multilingual[String Refined MaxSize[64]],
  sessionBindingMode: Option[Boolean],
  @(ApiModelProperty @field)(dataType = "object", required = true)
  parameters: NonEmptyList[LabelsValue]
) {
  @(ApiModelProperty @field)(hidden = true)
  val validateParameters: Seq[Requirement] = {
    def validLabelsLengths(layout: Layout, labelLength: Int = 64) = {
      Validation.validateField(
        field = "parameters",
        key = "invalid_value",
        condition = parameters.forall(_.label.translations.forall(_.length < labelLength)),
        message = s"At least one parameter label exceeds limit label length of $labelLength for layout $layout"
      )
    }
    def validateSize(layout: Layout, size: Int) =
      Validation.validateField(
        field = "parameters",
        key = "invalid_value",
        condition = parameters.size <= size,
        message = s"$layout card can contain max $size parameters"
      )
    layout match {
      case l @ Layout.OneColumnRadio    => Seq(validLabelsLengths(l), validateSize(l, 3))
      case l @ Layout.ThreeColumnsRadio => Seq(validLabelsLengths(l), validateSize(l, 9))
      case l                            => Seq(validLabelsLengths(l))
    }
  }
}

object AdminDemographicsCardRequest {
  implicit val codec: Codec[AdminDemographicsCardRequest] = deriveCodec
}

final case class AdminDemographicsCardResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  id: DemographicsCardId,
  name: String,
  @(ApiModelProperty @field)(dataType = "string", allowableValues = Layout.swaggerAllowableValues)
  layout: Layout,
  @(ApiModelProperty @field)(dataType = "string", allowableValues = "age,gender,region")
  dataType: String,
  @(ApiModelProperty @field)(dataType = "string", example = "fr")
  languages: NonEmptyList[Language],
  titles: Multilingual[String],
  sessionBindingMode: Boolean,
  @(ApiModelProperty @field)(dataType = "object")
  parameters: NonEmptyList[LabelsValue]
)

object AdminDemographicsCardResponse {

  def apply(card: DemographicsCard): AdminDemographicsCardResponse =
    AdminDemographicsCardResponse(
      id = card.id,
      name = card.name,
      layout = card.layout,
      dataType = card.dataType,
      languages = card.languages,
      titles = card.titles,
      sessionBindingMode = card.sessionBindingMode,
      parameters = card.parameters
    )

  implicit val codec: Codec[AdminDemographicsCardResponse] = deriveCodec

}
