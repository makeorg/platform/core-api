/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.operation

import cats.implicits._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import grizzled.slf4j.Logging
import io.swagger.annotations._
import eu.timepit.refined.auto._
import org.make.api.operation.DefaultPersistentOperationServiceComponent.PersistentOperation

import javax.ws.rs.Path
import org.make.api.sequence.SequenceServiceComponent
import org.make.api.tag.TagServiceComponent
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.api.technical.CsvReceptacle._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.user.UserServiceComponent
import org.make.core.auth.UserRights
import org.make.core.technical.Pagination
import org.make.core.operation._
import org.make.core.{HttpCodes, Order, ParameterExtractors, Validation}
import scalaoauth2.provider.AuthInfo

@Api(
  value = "admin Operation",
  authorizations = Array(
    new Authorization(
      value = "MakeApi",
      scopes = Array(
        new AuthorizationScope(scope = "admin", description = "BO Admin"),
        new AuthorizationScope(scope = "moderator", description = "BO Moderator")
      )
    )
  )
)
@Path(value = "/admin/operations")
trait AdminOperationApi extends Directives {

  @ApiOperation(value = "post-operation", httpMethod = "POST", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.Created, message = "Ok", response = classOf[OperationIdResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "body",
        paramType = "body",
        dataType = "org.make.api.operation.AdminCreateOperationRequest"
      )
    )
  )
  @Path(value = "/")
  def adminPostOperation: Route

  @ApiOperation(value = "put-operation", httpMethod = "PUT", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[OperationIdResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "operationId", paramType = "path", dataType = "string"),
      new ApiImplicitParam(
        name = "body",
        paramType = "body",
        dataType = "org.make.api.operation.AdminUpdateOperationRequest"
      )
    )
  )
  @Path(value = "/{operationId}")
  def adminPutOperation: Route

  @ApiOperation(value = "get-operation", httpMethod = "GET", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[AdminOperationResponse]))
  )
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "operationId", paramType = "path", dataType = "string")))
  @Path(value = "/{operationId}")
  def adminGetOperation: Route

  @ApiOperation(value = "get-operations", httpMethod = "GET", code = HttpCodes.OK)
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[AdminOperationResponse]]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = PersistentOperation.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "slug", paramType = "query", required = false, dataType = "string"),
      new ApiImplicitParam(
        name = "operationKind",
        paramType = "query",
        dataType = "string",
        allowableValues = OperationKind.swaggerAllowableValues,
        allowMultiple = true
      )
    )
  )
  @Path(value = "/")
  def adminGetOperations: Route

  def routes: Route =
    adminPostOperation ~ adminGetOperation ~ adminGetOperations ~ adminPutOperation

  protected val operationId: PathMatcher1[OperationId] = Segment.map(id => OperationId(id))
}

trait AdminOperationApiComponent {
  def adminOperationApi: AdminOperationApi
}

trait DefaultAdminOperationApiComponent
    extends AdminOperationApiComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {

  this: MakeDirectivesDependencies
    with OperationServiceComponent
    with OperationServiceComponent
    with SequenceServiceComponent
    with TagServiceComponent
    with UserServiceComponent =>

  override lazy val adminOperationApi: AdminOperationApi = new DefaultAdminOperationApi

  class DefaultAdminOperationApi extends AdminOperationApi {

    private def allowedSameSlugValidation(slug: String, operationId: String, operationIdOfSlug: String) =
      Validation.validateField(
        "slug",
        "invalid_value",
        operationId === operationIdOfSlug,
        s"Slug '$slug' already exist"
      )

    def adminPostOperation: Route = {
      post {
        path("admin" / "operations") {
          makeOperation("adminPostOperation") { _ =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireAdminRole(auth.user) {
                decodeRequest {
                  entity(as[AdminCreateOperationRequest]) { request: AdminCreateOperationRequest =>
                    operationService.findOneBySlug(request.slug).asDirective { maybeOperation =>
                      Validation.validate(
                        Validation
                          .requireNotPresent("slug", maybeOperation, Some(s"Slug '${request.slug}' already exists"))
                      )
                      onSuccess(
                        operationService
                          .create(userId = auth.user.userId, slug = request.slug, operationKind = request.operationKind)
                      ) { operationId =>
                        complete(StatusCodes.Created -> OperationIdResponse(operationId))
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    def adminPutOperation: Route = {
      put {
        path("admin" / "operations" / operationId) { operationId =>
          makeOperation("adminPutOperation") { _ =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireAdminRole(auth.user) {
                operationService.findOneSimple(operationId).asDirectiveOrNotFound { _ =>
                  decodeRequest {
                    entity(as[AdminUpdateOperationRequest]) { request: AdminUpdateOperationRequest =>
                      operationService.findOneBySlug(request.slug).asDirective { maybeOperation =>
                        maybeOperation.foreach { operation =>
                          Validation.validate(
                            allowedSameSlugValidation(request.slug, operation.operationId.value, operationId.value)
                          )
                        }
                        operationService
                          .update(
                            operationId = operationId,
                            userId = auth.user.userId,
                            slug = Some(request.slug),
                            operationKind = Some(request.operationKind)
                          )
                          .asDirectiveOrNotFound { id =>
                            complete(OperationIdResponse(id))
                          }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    def adminGetOperation: Route = {
      get {
        path("admin" / "operations" / operationId) { operationId =>
          makeOperation("adminGetOperation") { _ =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireAdminRole(auth.user) {
                operationService.findOneSimple(operationId).asDirectiveOrNotFound { operation =>
                  complete(AdminOperationResponse.apply(operation = operation))
                }
              }
            }
          }
        }
      }
    }

    def adminGetOperations: Route = {
      get {
        path("admin" / "operations") {
          makeOperation("adminGetOperations") { _ =>
            parameters(
              "_start".as[Pagination.Offset].?,
              "_end".as[Pagination.End].?,
              "_sort".?,
              "_order".as[Order].?,
              "slug".?,
              "operationKind".csv[OperationKind]
            ) {
              (
                offset: Option[Pagination.Offset],
                end: Option[Pagination.End],
                sort: Option[String],
                order: Option[Order],
                slug: Option[String],
                operationKinds: Option[Seq[OperationKind]]
              ) =>
                makeOAuth2 { auth: AuthInfo[UserRights] =>
                  requireAdminRole(auth.user) {
                    (
                      operationService.count(slug = slug, operationKinds = operationKinds).asDirective,
                      operationService
                        .findSimple(
                          offset = offset.orZero,
                          end = end,
                          sort = sort,
                          order = order,
                          slug = slug,
                          operationKinds = operationKinds
                        )
                        .asDirective
                    ).tupled.apply({
                      case (count, operations) =>
                        val operationResponses: Seq[AdminOperationResponse] =
                          operations.map(AdminOperationResponse(_))
                        complete((StatusCodes.OK, List(`X-Total-Count`(count.toString)), operationResponses))
                    })
                  }
                }
            }
          }
        }
      }
    }
  }
}
