/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.operation

import java.time.{LocalDate, ZonedDateTime}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import cats.implicits._
import cats.data.NonEmptyList
import cats.data.ValidatedNec
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directives, PathMatcher1, Route}
import eu.timepit.refined.refineV
import eu.timepit.refined.auto._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.string.Url
import eu.timepit.refined.collection._
import eu.timepit.refined.boolean.And
import eu.timepit.refined.string.StartsWith
import grizzled.slf4j.Logging
import io.circe.{Codec, Decoder, Encoder, HCursor}
import io.circe.refined._
import io.circe.generic.semiauto._
import io.swagger.annotations._

import javax.ws.rs.Path
import org.make.api.question.QuestionServiceComponent
import org.make.api.technical.{`X-Total-Count`, EventBusServiceComponent, MakeAuthenticationDirectives}
import org.make.api.technical.CsvReceptacle._
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.api.userhistory.{VoteOnlyEvent, VoteOnlyTestEvent}
import org.make.api.widget.WidgetServiceComponent
import org.make.core.Validation
import org.make.core.Validation._
import org.make.api.demographics.ActiveDemographicsCardServiceComponent
import org.make.core.technical.{Multilingual, MultilingualUtils, Pagination}
import org.make.core.technical.ValidatedUtils.ValidatedNecWithUtils
import org.make.core._
import org.make.core.auth.UserRights
import org.make.core.operation.OperationKind.GreatCause
import org.make.core.operation.OperationOfQuestion.{Status => QuestionStatus}
import org.make.core.operation._
import org.make.core.operation.indexed.{
  IndexedOperationOfQuestion,
  OperationOfQuestionElasticsearchFieldName,
  OperationOfQuestionSearchResult
}
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import org.make.core.technical.RefinedTypes
import org.make.core.user.Role.RoleAdmin
import scalaoauth2.provider.AuthInfo
import org.make.api.operation.DefaultModerationOperationOfQuestionApiComponent._
import org.make.api.operation.DefaultPersistentOperationOfQuestionServiceComponent.PersistentOperationOfQuestion
import org.make.api.technical.crm.PersistentCrmUserServiceComponent

import scala.annotation.meta.field

@Api(value = "Moderation Operation of question")
@Path(value = "/moderation/operations-of-questions")
trait ModerationOperationOfQuestionApi extends Directives {

  @ApiOperation(
    value = "get-operations-of-questions-for-operation",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(
      new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[OperationOfQuestionResponse]])
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = PersistentOperationOfQuestion.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "id", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(name = "questionId", paramType = "query", dataType = "string", allowMultiple = true),
      new ApiImplicitParam(name = "operationId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "operationKind",
        paramType = "query",
        dataType = "string",
        allowableValues = OperationKind.swaggerAllowableValues,
        allowMultiple = true
      ),
      new ApiImplicitParam(name = "openAt", paramType = "query", dataType = "dateTime"),
      new ApiImplicitParam(name = "endAfter", paramType = "query", dataType = "dateTime"),
      new ApiImplicitParam(name = "slug", paramType = "query", dataType = "string")
    )
  )
  @Path(value = "/")
  def listOperationOfQuestions: Route

  @ApiOperation(
    value = "get-operation-of-question",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[OperationOfQuestionResponse]))
  )
  @ApiImplicitParams(
    value = Array(new ApiImplicitParam(name = "questionId", paramType = "path", required = true, dataType = "string"))
  )
  @Path(value = "/{questionId}")
  def getOperationOfQuestion: Route

  @ApiOperation(
    value = "modify-operation-of-question",
    httpMethod = "PUT",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[OperationOfQuestionResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "questionId", paramType = "path", required = true, dataType = "string"),
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        required = true,
        dataType = "org.make.api.operation.ModifyOperationOfQuestionRequest"
      )
    )
  )
  @Path(value = "/{questionId}")
  def modifyOperationOfQuestion: Route

  @ApiOperation(
    value = "delete-operation-of-question",
    httpMethod = "DELETE",
    code = HttpCodes.NoContent,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No Content")))
  @ApiImplicitParams(
    value = Array(new ApiImplicitParam(name = "questionId", paramType = "path", required = true, dataType = "string"))
  )
  @Path(value = "/{questionId}")
  def deleteOperationOfQuestionAndQuestion: Route

  @ApiOperation(
    value = "create-operation-of-question",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(
    value =
      Array(new ApiResponse(code = HttpCodes.Created, message = "Ok", response = classOf[OperationOfQuestionResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        value = "body",
        paramType = "body",
        required = true,
        dataType = "org.make.api.operation.CreateOperationOfQuestionRequest"
      )
    )
  )
  @Path(value = "/")
  def createOperationOfQuestionAndQuestion: Route

  @ApiOperation(
    value = "search-operations-of-questions",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(
      new ApiResponse(
        code = HttpCodes.OK,
        message = "Ok",
        response = classOf[Array[ModerationOperationOfQuestionSearchResult]]
      )
    )
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = OperationOfQuestionElasticsearchFieldName.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "slug", paramType = "query", dataType = "string")
    )
  )
  @Path(value = "/search")
  def searchOperationsOfQuestions: Route

  @ApiOperation(
    value = "operations-of-questions-infos",
    httpMethod = "GET",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(
    value = Array(
      new ApiResponse(
        code = HttpCodes.OK,
        message = "Ok",
        response = classOf[Array[ModerationOperationOfQuestionInfosResponse]]
      )
    )
  )
  @ApiImplicitParams(
    value =
      Array(new ApiImplicitParam(name = "moderationMode", paramType = "query", required = true, dataType = "string"))
  )
  @Path(value = "/infos")
  def infosOperationsOfQuestions: Route

  @ApiOperation(
    value = "send-vote-only-test",
    httpMethod = "POST",
    code = HttpCodes.OK,
    authorizations = Array(
      new Authorization(
        value = "MakeApi",
        scopes = Array(
          new AuthorizationScope(scope = "admin", description = "BO Admin"),
          new AuthorizationScope(scope = "moderator", description = "BO Moderator")
        )
      )
    )
  )
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok")))
  @ApiImplicitParams(
    value = Array(new ApiImplicitParam(name = "questionId", paramType = "path", required = true, dataType = "string"))
  )
  @Path(value = "/{questionId}/vote-only-test")
  def sendVoteOnlyTest: Route

  def routes: Route =
    listOperationOfQuestions ~
      infosOperationsOfQuestions ~
      searchOperationsOfQuestions ~
      getOperationOfQuestion ~
      modifyOperationOfQuestion ~
      deleteOperationOfQuestionAndQuestion ~
      createOperationOfQuestionAndQuestion ~
      sendVoteOnlyTest
}

trait ModerationOperationOfQuestionApiComponent {
  def moderationOperationOfQuestionApi: ModerationOperationOfQuestionApi
}

trait DefaultModerationOperationOfQuestionApiComponent
    extends ModerationOperationOfQuestionApiComponent
    with MakeAuthenticationDirectives
    with Logging
    with ParameterExtractors {

  this: MakeDirectivesDependencies
    with EventBusServiceComponent
    with OperationOfQuestionServiceComponent
    with QuestionServiceComponent
    with WidgetServiceComponent
    with OperationServiceComponent
    with ActiveDemographicsCardServiceComponent
    with PersistentCrmUserServiceComponent =>

  private val moderationQuestionId: PathMatcher1[QuestionId] = Segment.map(id => QuestionId(id))

  override lazy val moderationOperationOfQuestionApi: DefaultModerationOperationOfQuestionApi =
    new DefaultModerationOperationOfQuestionApi

  class DefaultModerationOperationOfQuestionApi extends ModerationOperationOfQuestionApi {

    override def listOperationOfQuestions: Route = get {
      path("moderation" / "operations-of-questions") {
        makeOperation("ListOperationsOfQuestions") { _ =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireModerationRole(auth.user) {
              parameters(
                "_start".as[Pagination.Offset].?,
                "_end".as[Pagination.End].?,
                "_sort".?,
                "_order".as[Order].?,
                "id".csv[QuestionId],
                "questionId".csv[QuestionId],
                "operationId".as[OperationId].?,
                "operationKind".csv[OperationKind],
                "openAt".as[ZonedDateTime].?,
                "endAfter".as[ZonedDateTime].?,
                "slug".as[String].?
              ) {
                (
                  offset: Option[Pagination.Offset],
                  end: Option[Pagination.End],
                  sort: Option[String],
                  order: Option[Order],
                  ids,
                  questionIds,
                  operationId,
                  operationKinds,
                  openAt,
                  endAfter,
                  slug
                ) =>
                  val resolvedQuestions: Option[Seq[QuestionId]] = {
                    if (auth.user.roles.contains(RoleAdmin)) {
                      questionIds.plusDistinct(ids)
                    } else {
                      questionIds
                        .plusDistinct(ids)
                        .map(_.filter(auth.user.availableQuestions.contains))
                        .orElse(Some(auth.user.availableQuestions))
                    }
                  }
                  (
                    operationOfQuestionService
                      .find(
                        offset.orZero,
                        end,
                        sort,
                        order,
                        SearchOperationsOfQuestions(
                          questionIds = resolvedQuestions,
                          operationIds = operationId.map(opId => Seq(opId)),
                          operationKind = operationKinds,
                          openAt = openAt,
                          endAfter = endAfter,
                          slug = slug
                        )
                      )
                      .asDirective,
                    operationOfQuestionService
                      .count(
                        SearchOperationsOfQuestions(
                          questionIds = resolvedQuestions,
                          operationIds = operationId.map(opId => Seq(opId)),
                          operationKind = operationKinds,
                          openAt = openAt,
                          endAfter = endAfter,
                          slug = slug
                        )
                      )
                      .asDirective
                  ).tupled.apply({
                    case (result, count) =>
                      questionService.getQuestions(result.map(_.questionId)).asDirective { questions =>
                        val questionsAsMap = questions.map(q => q.questionId -> q).toMap
                        complete(
                          (
                            StatusCodes.OK,
                            List(`X-Total-Count`(count.toString)),
                            result
                              .map(
                                operationOfQuestion =>
                                  OperationOfQuestionResponse(
                                    operationOfQuestion,
                                    questionsAsMap(operationOfQuestion.questionId)
                                  )
                              )
                          )
                        )
                      }
                  })
              }
            }
          }
        }
      }
    }

    override def getOperationOfQuestion: Route = get {
      path("moderation" / "operations-of-questions" / moderationQuestionId) { questionId =>
        makeOperation("GetOperationsOfQuestions") { _ =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireModerationRole(auth.user) {
              (
                operationOfQuestionService.findByQuestionId(questionId).asDirectiveOrNotFound,
                questionService.getQuestion(questionId).asDirectiveOrNotFound
              ).tupled.apply({
                case (operationOfQuestion, question) =>
                  persistentCrmUserService.getActiveConsultationUsersCount(question.slug).asDirective {
                    activesUsersCount =>
                      complete(OperationOfQuestionResponse(operationOfQuestion, question, Some(activesUsersCount)))
                  }
              })
            }
          }
        }
      }
    }

    @SuppressWarnings(Array("org.wartremover.warts.Throw"))
    private def validateModifyRequest(
      request: ModifyOperationOfQuestionRequest,
      question: Question,
      operationOfQuestion: OperationOfQuestion,
      widgetsCount: Int,
      activeCardCount: Int
    ): ValidatedNec[ValidationError, Unit] = {
      val countriesValidated: ValidatedNec[ValidationError, Unit] =
        if (operationOfQuestion.proposalsCount > 0) {
          val deletedCountries = question.countries.toList.toSet -- request.countries.toList
          if (deletedCountries.isEmpty) ().validNec
          else
            ValidationError(
              "countries",
              "removal_forbidden",
              Some(s"You cannot remove existing countries: ${deletedCountries.mkString(", ")}")
            ).invalidNec
        } else ().validNec
      val languagesRemoved = question.languages.toList.toSet -- request.languages.toList
      val languagesRemovedValidated =
        if (languagesRemoved.nonEmpty) {
          if (operationOfQuestion.proposalsCount > 0)
            ValidationError(
              "languages",
              "has_proposition",
              Some("Cannot remove languages from a consultation that already has had proposals made on it")
            ).invalidNec
          else if (widgetsCount > 0)
            ValidationError(
              "languages",
              "has_widget",
              Some("Cannot remove languages from a consultation that has a widget attached to it")
            ).invalidNec
          else ().validNec
        } else ().validNec
      val languagesAdded = request.languages.toList.toSet -- question.languages.toList
      val languagesAddedValidated =
        if (languagesAdded.nonEmpty && operationOfQuestion.proposalsCount > 0) {
          ValidationError(
            "languages",
            "has_proposition",
            Some("Cannot add a language on a consultation that already has had proposals made on it")
          ).invalidNec
        } else ().validNec
      val activeCardsCountValidated =
        if (languagesAdded.nonEmpty && activeCardCount > 0) {
          ValidationError(
            "languages",
            "has_demographics_cards",
            Some("Cannot add languages from a consultation that has demographics cards attached to it")
          ).invalidNec
        } else ().validNec
      val reportUrlValidated =
        request.reportUrl match {
          case None => ().validNec
          case Some(value) =>
            refineV[Url](value)
              .leftMap(
                err =>
                  ValidationError("report_url", "valid_report_url", Some(s"$value is not a valid report url: $err"))
              )
              .toValidatedNec
              .void
        }
      val actionsUrlValidated =
        request.actionsUrl match {
          case None => ().validNec
          case Some(value) =>
            refineV[Url](value)
              .leftMap(
                err =>
                  ValidationError("actions_url", "valid_actions_url", Some(s"$value is not a valid actions url: $err"))
              )
              .toValidatedNec
              .void
        }
      val proposalPrefixesSizeValidated =
        if (operationOfQuestion.proposalsCount > 0
            && operationOfQuestion.proposalPrefixes.toMap.sizeIs != request.proposalPrefixes.toMap.size) {
          ValidationError(
            "proposalPrefixes",
            "has_proposition",
            Some("Cannot add or delete proposal prefixes on a consultation that already has had proposals made on it")
          ).invalidNec
        } else ().validNec
      val proposalPrefixesMerged = (operationOfQuestion.proposalPrefixes.toMap.toSeq ++
        request.proposalPrefixes.mapTranslations(_.value).toMap.toSeq).groupMap(_._1)(_._2)
      val proposalPrefixesValidated =
        if (operationOfQuestion.proposalsCount > 0
            && proposalPrefixesMerged.view.values.exists(v => v.headOption != v.lastOption)) {
          ValidationError(
            "proposalPrefixes",
            "has_proposition",
            Some("Cannot update proposal prefixes on a consultation that already has had proposals made on it")
          ).invalidNec
        } else ().validNec
      (
        countriesValidated,
        languagesRemovedValidated,
        languagesAddedValidated,
        activeCardsCountValidated,
        reportUrlValidated,
        actionsUrlValidated,
        proposalPrefixesSizeValidated,
        proposalPrefixesValidated
      ).tupled.void
    }

    override def sendVoteOnlyTest: Route = post {
      path("moderation" / "operations-of-questions" / moderationQuestionId / "vote-only-test") { questionId =>
        makeOperation("SendVoteOnlyTest") { requestContext =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireAdminRole(auth.user) {
              Future {
                eventBusService.publish(
                  VoteOnlyTestEvent(
                    eventId = Some(idGenerator.nextEventId()),
                    eventDate = DateHelper.now(),
                    userId = auth.user.userId,
                    requestContext = requestContext
                      .copy(questionContext = RequestContextQuestion(questionId = Some(questionId)))
                  )
                )
              }.asDirective.apply(complete(_))
            }
          }
        }
      }
    }

    override def modifyOperationOfQuestion: Route = put {
      path("moderation" / "operations-of-questions" / moderationQuestionId) { questionId =>
        makeOperation("ModifyOperationsOfQuestions") { requestContext =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireAdminRole(auth.user) {
              decodeRequest {
                entity(as[ModifyOperationOfQuestionRequest]) { request =>
                  (
                    questionService.getQuestion(questionId).asDirectiveOrNotFound,
                    operationOfQuestionService.findByQuestionId(questionId).asDirectiveOrNotFound,
                    widgetService.count(questionId).asDirective,
                    activeDemographicsCardService.count(questionId = Some(questionId), cardId = None).asDirective
                  ).tupled
                    .flatMap({
                      case (question, operationOfQuestion, widgetsCount, activeCardCount) =>
                        validateModifyRequest(request, question, operationOfQuestion, widgetsCount, activeCardCount)
                          .throwIfInvalid()
                        val updatedQuestion = question.copy(
                          countries = request.countries,
                          defaultLanguage = request.defaultLanguage,
                          languages = request.languages,
                          questions = request.questions.mapTranslations(RefinedTypes.cast(_)),
                          shortTitles = request.shortTitles
                            .map(_.mapTranslations(title => refineV[NonEmpty].unsafeFrom(title.value)))
                        )
                        val updatedSequenceCardsConfiguration =
                          request.sequenceCardsConfiguration.copy(
                            introCard = request.sequenceCardsConfiguration.introCard
                              .copy(titles = if (request.sequenceCardsConfiguration.introCard.enabled) {
                                request.sequenceCardsConfiguration.introCard.titles
                              } else {
                                None
                              }, descriptions = if (request.sequenceCardsConfiguration.introCard.enabled) {
                                request.sequenceCardsConfiguration.introCard.descriptions
                              } else {
                                None
                              }),
                            pushProposalCard = PushProposalCard(enabled = request.canPropose &&
                              request.sequenceCardsConfiguration.pushProposalCard.enabled
                            ),
                            finalCard = request.sequenceCardsConfiguration.finalCard
                              .copy(sharingEnabled = request.sequenceCardsConfiguration.finalCard.enabled &&
                                request.sequenceCardsConfiguration.finalCard.sharingEnabled
                              )
                          )

                        val closingQuestion = operationOfQuestion.canPropose && !request.canPropose
                        if (closingQuestion) {
                          eventBusService.publish(
                            VoteOnlyEvent(
                              eventId = Some(idGenerator.nextEventId()),
                              eventDate = DateHelper.now(),
                              userId = auth.user.userId,
                              requestContext = requestContext
                                .copy(questionContext = RequestContextQuestion(questionId = Some(questionId)))
                            )
                          )
                        }

                        operationOfQuestionService
                          .updateWithQuestion(
                            operationOfQuestion
                              .copy(
                                startDate = request.startDate,
                                endDate = request.endDate,
                                operationTitles = request.operationTitles.mapTranslations(_.value),
                                proposalPrefixes = request.proposalPrefixes.mapTranslations(_.value),
                                canPropose = request.canPropose,
                                sequenceCardsConfiguration = updatedSequenceCardsConfiguration,
                                aboutUrls = request.aboutUrls.map(_.mapTranslations(_.value)),
                                metas = request.metas,
                                theme = request.theme,
                                descriptions = request.descriptions.map(_.mapTranslations(_.value)),
                                consultationImages = request.consultationImages.map(_.mapTranslations(_.value)),
                                consultationImageAlts = request.consultationImageAlts,
                                descriptionImages = request.descriptionImages.map(_.mapTranslations(_.value)),
                                descriptionImageAlts = request.descriptionImageAlts,
                                partnersLogos = request.partnersLogos.map(_.value),
                                partnersLogosAlt = request.partnersLogosAlt,
                                initiatorsLogos = request.initiatorsLogos.map(_.value),
                                initiatorsLogosAlt = request.initiatorsLogosAlt,
                                consultationHeader = request.consultationHeader.map(_.value),
                                consultationHeaderAlts = request.consultationHeaderAlts,
                                cobrandingLogo = request.cobrandingLogo.map(_.value),
                                cobrandingLogoAlt = request.cobrandingLogoAlt,
                                resultsLink = Option
                                  .when(request.displayResults)(request.resultsLink.flatMap(_.resultsLink))
                                  .flatten,
                                actions = request.actions,
                                featured = request.featured,
                                votesTarget = request.votesTarget,
                                timeline = request.timeline.toOOQTimeline,
                                sessionBindingMode = request.sessionBindingMode,
                                reportUrl = request.reportUrl,
                                actionsUrl = request.actionsUrl
                              ),
                            updatedQuestion
                          )
                          .asDirective
                          .map(result => StatusCodes.OK -> OperationOfQuestionResponse(result, updatedQuestion))
                    })
                    .apply(complete(_))
                }
              }
            }
          }
        }
      }
    }

    override def deleteOperationOfQuestionAndQuestion: Route = delete {
      path("moderation" / "operations-of-questions" / moderationQuestionId) { questionId =>
        makeOperation("DeleteOperationsOfQuestions") { _ =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireAdminRole(auth.user) {
              operationOfQuestionService.delete(questionId).asDirective { _ =>
                complete(StatusCodes.NoContent)
              }
            }
          }
        }
      }
    }

    override def createOperationOfQuestionAndQuestion: Route = post {
      path("moderation" / "operations-of-questions") {
        makeOperation("CreateOperationsOfQuestions") { _ =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireAdminRole(auth.user) {
              decodeRequest {
                entity(as[CreateOperationOfQuestionRequest]) { body =>
                  (
                    operationService.findOneSimple(body.operationId).asDirective,
                    operationOfQuestionService.findByQuestionSlug(body.questionSlug).asDirective
                  ).mapN((maybeOperation, maybeQuestion) => {
                      (
                        body.operationTitle.toSanitizedInput("operationTitle"),
                        body.question.value.toSanitizedInput("question"),
                        body.questionSlug.toSanitizedInput("questionSlug"),
                        maybeQuestion.toEmpty("slug", Some(s"Slug '${body.questionSlug}' already exists")),
                        maybeOperation
                          .toNonEmpty("operationId", Some(s"Operation '${body.operationId}' does not exists"))
                      ).tupled.throwIfInvalid()
                      operationOfQuestionService
                        .create(
                          CreateOperationOfQuestion(
                            operationId = body.operationId,
                            startDate = body.startDate,
                            endDate = body.endDate,
                            operationTitle = body.operationTitle,
                            slug = body.questionSlug,
                            countries = body.countries,
                            defaultLanguage = body.defaultLanguage,
                            question = body.question,
                            proposalPrefix = body.proposalPrefix.value,
                            featured = maybeOperation.exists(_.operationKind == GreatCause)
                          )
                        )
                        .asDirective
                    })
                    .flatten
                    .apply(
                      operationOfQuestion =>
                        questionService
                          .getQuestion(operationOfQuestion.questionId)
                          .asDirectiveOrNotFound
                          .apply(
                            question =>
                              complete(
                                StatusCodes.Created -> OperationOfQuestionResponse(operationOfQuestion, question)
                              )
                          )
                    )
                }
              }
            }
          }
        }
      }
    }

    override def searchOperationsOfQuestions: Route = get {
      path("moderation" / "operations-of-questions" / "search") {
        makeOperation("SearchOperationsOfQuestions") { _ =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireModerationRole(auth.user) {
              parameters(
                "_start".as[Pagination.Offset].?,
                "_end".as[Pagination.End].?,
                "_sort".as[OperationOfQuestionElasticsearchFieldName].?,
                "_order".as[Order].?,
                "slug".as[String].?
              ) {
                (
                  offset: Option[Pagination.Offset],
                  end: Option[Pagination.End],
                  sort: Option[OperationOfQuestionElasticsearchFieldName],
                  order: Option[Order],
                  slug
                ) =>
                  Validation.validate(sort.map(Validation.validateSort("_sort")).toList: _*)
                  val questions: Option[Seq[QuestionId]] =
                    Option.unless(auth.user.roles.contains(RoleAdmin))(auth.user.availableQuestions)
                  operationOfQuestionService
                    .search(
                      OperationOfQuestionSearchQuery(
                        filters = Some(
                          OperationOfQuestionSearchFilters(
                            questionIds = questions.map(QuestionIdsSearchFilter),
                            slug = slug.map(SlugSearchFilter),
                            operationKinds = Some(OperationKindsSearchFilter(OperationKind.values))
                          )
                        ),
                        limit = end.map(_.toLimit(offset.orZero)),
                        offset = offset,
                        sort = sort,
                        order = order
                      )
                    )
                    .asDirective
                    .apply { result: OperationOfQuestionSearchResult =>
                      complete(
                        (
                          StatusCodes.OK,
                          List(`X-Total-Count`(result.total.toString)),
                          result.results.map(ModerationOperationOfQuestionSearchResult.apply)
                        )
                      )
                    }
              }
            }
          }
        }
      }
    }

    override def infosOperationsOfQuestions: Route = get {
      path("moderation" / "operations-of-questions" / "infos") {
        makeOperation("InfosOperationsOfQuestions") { _ =>
          makeOAuth2 { auth: AuthInfo[UserRights] =>
            requireModerationRole(auth.user) {
              parameters("moderationMode".as[ModerationMode], "minVotesCount".as[Int].?, "minScore".as[Double].?) {
                (moderationMode: ModerationMode, minVotesCount: Option[Int], minScore: Option[Double]) =>
                  val questionIds: Option[Seq[QuestionId]] =
                    Option.unless(auth.user.roles.contains(RoleAdmin))(auth.user.availableQuestions)
                  operationOfQuestionService
                    .getQuestionsInfos(questionIds, moderationMode, minVotesCount, minScore)
                    .asDirective { infos =>
                      complete((StatusCodes.OK, List(`X-Total-Count`(infos.size.toString)), infos))
                    }
              }
            }
          }
        }
      }
    }

  }

}

object DefaultModerationOperationOfQuestionApiComponent {
  implicit class PlusDistinctRichOptionSeq[T](val self: Option[Seq[T]]) extends AnyVal {
    def plusDistinct(other: Option[Seq[T]]): Option[Seq[T]] =
      Option.when(self.orElse(other).isDefined) {
        (self.getOrElse(Seq.empty) ++ other.getOrElse(Seq.empty)).distinct
      }
  }
}

@ApiModel
final case class ModifyOperationOfQuestionRequest(
  val defaultLanguage: Language,
  val languages: NonEmptyList[Language],
  startDate: ZonedDateTime,
  endDate: ZonedDateTime,
  operationTitles: Multilingual[String Refined ValidHtml],
  proposalPrefixes: Multilingual[String Refined (NonEmpty And ValidHtml And MaxSize[15])],
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  countries: NonEmptyList[Country],
  questions: Multilingual[String Refined (NonEmpty And ValidHtml)],
  @(ApiModelProperty @field)(dataType = "string")
  shortTitles: Option[Multilingual[String Refined (NonEmpty And MaxSize[30] And ValidHtml)]],
  canPropose: Boolean,
  sequenceCardsConfiguration: SequenceCardsConfiguration,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/about")
  aboutUrls: Option[Multilingual[String Refined Url]],
  metas: Metas,
  theme: QuestionTheme,
  descriptions: Option[Multilingual[String Refined ValidHtml]],
  displayResults: Boolean,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/consultation-image.png")
  consultationImages: Option[Multilingual[String Refined (Url And StartsWith["https://"])]],
  @(ApiModelProperty @field)(dataType = "string", example = "consultation image alternative")
  consultationImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/description-image.png")
  descriptionImages: Option[Multilingual[String Refined (Url And StartsWith["https://"])]],
  @(ApiModelProperty @field)(dataType = "string", example = "description image alternative")
  descriptionImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/partners-logos.png")
  partnersLogos: Option[String Refined (Url And StartsWith["https://"])],
  @(ApiModelProperty @field)(dataType = "string", example = "partners logos alternative")
  partnersLogosAlt: Option[String Refined MaxSize[130]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/initiators-logos.png")
  initiatorsLogos: Option[String Refined (Url And StartsWith["https://"])],
  @(ApiModelProperty @field)(dataType = "string", example = "initiators logos alternative")
  initiatorsLogosAlt: Option[String Refined MaxSize[130]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/consultation-header.png")
  consultationHeader: Option[String Refined (Url And StartsWith["https://"])],
  @(ApiModelProperty @field)(dataType = "string", example = "consultation header alternative")
  consultationHeaderAlts: Option[Multilingual[String Refined MaxSize[130]]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/cobranding-logo.png")
  cobrandingLogo: Option[String Refined (Url And StartsWith["https://"])],
  @(ApiModelProperty @field)(dataType = "string", example = "cobranding logo alternative")
  cobrandingLogoAlt: Option[String Refined MaxSize[130]],
  resultsLink: Option[ResultsLinkRequest],
  actions: Option[String],
  featured: Boolean,
  votesTarget: Int,
  timeline: OperationOfQuestionTimelineContract,
  sessionBindingMode: Boolean,
  reportUrl: Option[String],
  actionsUrl: Option[String]
)

object ModifyOperationOfQuestionRequest extends CirceFormatters {
  implicit val decoder: Decoder[ModifyOperationOfQuestionRequest] =
    MultilingualUtils.genDecoder(deriveDecoder[ModifyOperationOfQuestionRequest], _.languages.toList.toSet)

  implicit val codec: Codec[ModifyOperationOfQuestionRequest] = deriveCodec
}

@ApiModel
final case class CreateOperationOfQuestionRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  operationId: OperationId,
  startDate: ZonedDateTime,
  endDate: ZonedDateTime,
  operationTitle: String,
  proposalPrefix: String Refined (NonEmpty And ValidHtml And MaxSize[15]),
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  countries: NonEmptyList[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  defaultLanguage: Language,
  question: String Refined NonEmpty,
  questionSlug: String
)

object CreateOperationOfQuestionRequest extends CirceFormatters {

  implicit val codec: Codec[CreateOperationOfQuestionRequest] = deriveCodec
}

final case class OperationOfQuestionTimelineContract(
  action: TimelineElementContract,
  result: TimelineElementContract,
  workshop: TimelineElementContract
) {
  def toOOQTimeline: OperationOfQuestionTimeline = OperationOfQuestionTimeline(
    action = action.toTimelineElement,
    result = result.toTimelineElement,
    workshop = workshop.toTimelineElement
  )
}

object OperationOfQuestionTimelineContract extends CirceFormatters {
  implicit val codec: Codec[OperationOfQuestionTimelineContract] =
    deriveCodec[OperationOfQuestionTimelineContract]

  def apply(timeline: OperationOfQuestionTimeline): OperationOfQuestionTimelineContract =
    OperationOfQuestionTimelineContract(
      action = TimelineElementContract(timeline.action),
      result = TimelineElementContract(timeline.result),
      workshop = TimelineElementContract(timeline.workshop)
    )
}

final case class TimelineElementContract(
  defined: Boolean,
  date: Option[LocalDate],
  @(ApiModelProperty @field)(dataType = "string", required = true)
  dateTexts: Multilingual[String Refined MaxSize[20]],
  @(ApiModelProperty @field)(dataType = "string", required = true)
  descriptions: Multilingual[String Refined MaxSize[150]]
) {
  @SuppressWarnings(Array("org.wartremover.warts.OptionPartial"))
  def toTimelineElement: Option[TimelineElement] =
    if (!defined) {
      None
    } else {
      (
        date.getValidated("timeline.date"),
        dateTexts.toNonEmpty("timeline.dateTexts"),
        descriptions.toNonEmpty("timeline.descriptions")
      ).tupled.throwIfInvalid()

      date.map(d => TimelineElement(date = d, dateTexts = dateTexts, descriptions = descriptions))
    }
}

object TimelineElementContract extends CirceFormatters {

  implicit val encoder: Encoder[TimelineElementContract] = deriveEncoder

  implicit val decoder: Decoder[TimelineElementContract] = new Decoder[TimelineElementContract] {
    final def apply(c: HCursor): Decoder.Result[TimelineElementContract] =
      c.downField("defined")
        .as[Boolean]
        .flatMap({
          case true =>
            for {
              date         <- c.downField("date").as[Option[LocalDate]]
              dateTexts    <- c.downField("dateTexts").as[Multilingual[String Refined MaxSize[20]]]
              descriptions <- c.downField("descriptions").as[Multilingual[String Refined MaxSize[150]]]
            } yield TimelineElementContract(true, date, dateTexts, descriptions)
          case false =>
            Either.right(TimelineElementContract(false, None, Multilingual.empty, Multilingual.empty))
        })
  }

  def apply(timelineElement: Option[TimelineElement]): TimelineElementContract = TimelineElementContract(
    defined = timelineElement.isDefined,
    date = timelineElement.map(_.date),
    dateTexts = timelineElement.fold(Multilingual.empty[String Refined MaxSize[20]])(_.dateTexts),
    descriptions = timelineElement.fold(Multilingual.empty[String Refined MaxSize[150]])(_.descriptions)
  )
}

@ApiModel
final case class OperationOfQuestionResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: QuestionId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  operationId: OperationId,
  startDate: ZonedDateTime,
  endDate: ZonedDateTime,
  operationTitles: Multilingual[String],
  proposalPrefixes: Multilingual[String],
  slug: String,
  questions: Multilingual[String Refined NonEmpty],
  shortTitles: Option[Multilingual[String Refined NonEmpty]],
  @(ApiModelProperty @field)(dataType = "list[string]", required = true)
  countries: NonEmptyList[Country],
  @(ApiModelProperty @field)(dataType = "string", example = "fr", required = true)
  defaultLanguage: Language,
  languages: NonEmptyList[Language],
  canPropose: Boolean,
  sequenceCardsConfiguration: SequenceCardsConfiguration,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/about")
  aboutUrls: Option[Multilingual[String]],
  metas: Metas,
  theme: QuestionTheme,
  descriptions: Option[Multilingual[String]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/consultation-image.png")
  consultationImages: Option[Multilingual[String]],
  @(ApiModelProperty @field)(dataType = "string", example = "consultation image alternative")
  consultationImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/description-image.png")
  descriptionImages: Option[Multilingual[String]],
  @(ApiModelProperty @field)(dataType = "string", example = "description image alternative")
  descriptionImageAlts: Option[Multilingual[String Refined MaxSize[130]]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/partners-logos.png")
  partnersLogos: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "partners logos alternative")
  partnersLogosAlt: Option[String Refined MaxSize[130]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/initiators-logos.png")
  initiatorsLogos: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "initiators logos alternative")
  initiatorsLogosAlt: Option[String Refined MaxSize[130]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/consultation-header.png")
  consultationHeader: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "consultation header alternative")
  consultationHeaderAlts: Option[Multilingual[String Refined MaxSize[130]]],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/cobranding-logo.png")
  cobrandingLogo: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "cobranding logo alternative")
  cobrandingLogoAlt: Option[String Refined MaxSize[130]],
  displayResults: Boolean,
  resultsLink: Option[ResultsLinkResponse],
  actions: Option[String],
  featured: Boolean,
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = QuestionStatus.swaggerAllowableValues,
    required = true
  )
  status: QuestionStatus,
  votesTarget: Int,
  timeline: OperationOfQuestionTimelineContract,
  createdAt: ZonedDateTime,
  sessionBindingMode: Boolean,
  activesUsersCount: Option[Int],
  reportUrl: Option[String],
  actionsUrl: Option[String]
)

object OperationOfQuestionResponse extends CirceFormatters {

  implicit val codec: Codec[OperationOfQuestionResponse] = deriveCodec

  def apply(
    operationOfQuestion: OperationOfQuestion,
    question: Question,
    activesUsersCount: Option[Int] = None
  ): OperationOfQuestionResponse = {
    OperationOfQuestionResponse(
      id = operationOfQuestion.questionId,
      operationId = operationOfQuestion.operationId,
      operationTitles = operationOfQuestion.operationTitles,
      proposalPrefixes = operationOfQuestion.proposalPrefixes,
      startDate = operationOfQuestion.startDate,
      endDate = operationOfQuestion.endDate,
      slug = question.slug,
      questions = question.questions,
      shortTitles = question.shortTitles,
      countries = question.countries,
      defaultLanguage = question.defaultLanguage,
      languages = question.languages,
      canPropose = operationOfQuestion.canPropose,
      sequenceCardsConfiguration = operationOfQuestion.sequenceCardsConfiguration,
      aboutUrls = operationOfQuestion.aboutUrls,
      metas = operationOfQuestion.metas,
      theme = operationOfQuestion.theme,
      descriptions = operationOfQuestion.descriptions,
      consultationImages = operationOfQuestion.consultationImages,
      consultationImageAlts = operationOfQuestion.consultationImageAlts,
      descriptionImages = operationOfQuestion.descriptionImages,
      descriptionImageAlts = operationOfQuestion.descriptionImageAlts,
      partnersLogos = operationOfQuestion.partnersLogos,
      partnersLogosAlt = operationOfQuestion.partnersLogosAlt,
      initiatorsLogos = operationOfQuestion.initiatorsLogos,
      initiatorsLogosAlt = operationOfQuestion.initiatorsLogosAlt,
      consultationHeader = operationOfQuestion.consultationHeader,
      consultationHeaderAlts = operationOfQuestion.consultationHeaderAlts,
      cobrandingLogo = operationOfQuestion.cobrandingLogo,
      cobrandingLogoAlt = operationOfQuestion.cobrandingLogoAlt,
      displayResults = operationOfQuestion.resultsLink.isDefined,
      resultsLink = operationOfQuestion.resultsLink.map(ResultsLinkResponse.apply),
      actions = operationOfQuestion.actions,
      featured = operationOfQuestion.featured,
      status = operationOfQuestion.status,
      votesTarget = operationOfQuestion.votesTarget,
      timeline = OperationOfQuestionTimelineContract(operationOfQuestion.timeline),
      createdAt = operationOfQuestion.createdAt,
      sessionBindingMode = operationOfQuestion.sessionBindingMode,
      activesUsersCount = activesUsersCount,
      reportUrl = operationOfQuestion.reportUrl,
      actionsUrl = operationOfQuestion.actionsUrl
    )
  }
}

@ApiModel
final case class ModerationOperationOfQuestionSearchResult(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555") id: QuestionId,
  slug: String
)

object ModerationOperationOfQuestionSearchResult {

  def apply(ooq: IndexedOperationOfQuestion): ModerationOperationOfQuestionSearchResult =
    ModerationOperationOfQuestionSearchResult(ooq.questionId, ooq.slug)

  implicit val codec: Codec[ModerationOperationOfQuestionSearchResult] = deriveCodec
}
