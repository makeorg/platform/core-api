/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.operation

import java.time.ZonedDateTime
import io.circe.generic.semiauto.{deriveCodec, deriveEncoder}
import io.circe.{Codec, Encoder}
import io.swagger.annotations.{ApiModel, ApiModelProperty}
import org.make.core.CirceFormatters
import org.make.core.operation._

import scala.annotation.meta.field
@ApiModel
final case class OperationIdResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: OperationId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  operationId: OperationId
)

object OperationIdResponse {
  implicit val encoder: Encoder[OperationIdResponse] = deriveEncoder[OperationIdResponse]

  def apply(id: OperationId): OperationIdResponse = OperationIdResponse(id, id)
}

@ApiModel
final case class AdminOperationResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: OperationId,
  slug: String,
  createdAt: Option[ZonedDateTime],
  updatedAt: Option[ZonedDateTime],
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = OperationKind.swaggerAllowableValues
  )
  operationKind: OperationKind
)

object AdminOperationResponse extends CirceFormatters {
  implicit val codec: Codec[AdminOperationResponse] = deriveCodec

  def apply(operation: SimpleOperation): AdminOperationResponse = {
    AdminOperationResponse(
      id = operation.operationId,
      slug = operation.slug,
      createdAt = operation.createdAt,
      updatedAt = operation.updatedAt,
      operationKind = operation.operationKind
    )
  }
}
