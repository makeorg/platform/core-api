/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.operation

import java.net.URI

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.refined._
import eu.timepit.refined.api.Refined
import io.swagger.annotations.{ApiModel, ApiModelProperty}
import org.make.core.Validation._
import org.make.core.operation.{OperationKind, ResultsLink}

import scala.annotation.meta.field
import scala.util.Try

@ApiModel
final case class AdminCreateOperationRequest(
  slug: String Refined Slug,
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = OperationKind.swaggerAllowableValues,
    required = true
  )
  operationKind: OperationKind
)

object AdminCreateOperationRequest {
  implicit val decoder: Decoder[AdminCreateOperationRequest] = deriveDecoder
}
@ApiModel
final case class AdminUpdateOperationRequest(
  slug: String Refined Slug,
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = OperationKind.swaggerAllowableValues,
    required = true
  )
  operationKind: OperationKind
)

object AdminUpdateOperationRequest {
  implicit val decoder: Decoder[AdminUpdateOperationRequest] = deriveDecoder
}

final case class ResultsLinkRequest(
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = ResultsLink.Internal.swaggerAllowableValues
  ) kind: ResultsLinkKind,
  @(ApiModelProperty @field)(dataType = "string", required = true, example = "results") value: String
) {

  @ApiModelProperty(hidden = true)
  val resultsLink: Option[ResultsLink] = ResultsLink.parse(value)

}

object ResultsLinkRequest {
  // FIXME: provide a generic implementation with shapeless of discriminated ADTs
  private val baseDecoder: Decoder[ResultsLinkRequest] = deriveDecoder
  implicit val decoder: Decoder[ResultsLinkRequest] = baseDecoder.emap {
    case t @ ResultsLinkRequest(ResultsLinkKind.External, value) =>
      if (Try(new URI(value)).isSuccess)
        Right(t)
      else
        Left(s"Invalid URI: '${value}'")

    case t @ ResultsLinkRequest(ResultsLinkKind.Internal, value) =>
      if (ResultsLink.Internal.withValueOpt(value).nonEmpty)
        Right(t)
      else
        Left(s"Invalid internal link: '${value}'")
  }

  implicit val encoder: Encoder[ResultsLinkRequest] = deriveEncoder
}
