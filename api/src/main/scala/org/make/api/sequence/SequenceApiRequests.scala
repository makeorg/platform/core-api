/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.sequence

import eu.timepit.refined.types.numeric.PosInt
import io.circe.refined._
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import io.swagger.annotations.ApiModelProperty
import org.make.core.question.QuestionId
import org.make.core.sequence._
import org.make.core.technical.RefinedTypes.Ratio

import scala.annotation.meta.field

final case class SequenceConfigurationRequest(
  main: ExplorationSequenceConfigurationRequest,
  controversial: SpecificSequenceConfigurationRequest,
  popular: SpecificSequenceConfigurationRequest,
  keyword: SpecificSequenceConfigurationRequest,
  @(ApiModelProperty @field)(dataType = "int", example = "100", required = true) newProposalsVoteThreshold: Int,
  @(ApiModelProperty @field)(dataType = "double", example = "0.8")
  testedProposalsEngagementThreshold: Option[Double],
  @(ApiModelProperty @field)(dataType = "double", example = "0.0")
  testedProposalsScoreThreshold: Option[Double],
  @(ApiModelProperty @field)(dataType = "double", example = "0.0") testedProposalsControversyThreshold: Option[Double],
  @(ApiModelProperty @field)(dataType = "int", example = "1500") testedProposalsMaxVotesThreshold: Option[Int],
  @(ApiModelProperty @field)(dataType = "double", example = "0.5", required = true) nonSequenceVotesWeight: Double
) {

  def toSequenceConfiguration(questionId: QuestionId): SequenceConfiguration = {
    SequenceConfiguration(
      questionId = questionId,
      mainSequence = main.toExplorationConfiguration,
      controversial = controversial.toSpecificSequenceConfiguration,
      popular = popular.toSpecificSequenceConfiguration,
      keyword = keyword.toSpecificSequenceConfiguration,
      newProposalsVoteThreshold = newProposalsVoteThreshold,
      testedProposalsEngagementThreshold = testedProposalsEngagementThreshold,
      testedProposalsScoreThreshold = testedProposalsScoreThreshold,
      testedProposalsControversyThreshold = testedProposalsControversyThreshold,
      testedProposalsMaxVotesThreshold = testedProposalsMaxVotesThreshold,
      nonSequenceVotesWeight = nonSequenceVotesWeight
    )
  }
}

object SequenceConfigurationRequest {
  implicit val decoder: Decoder[SequenceConfigurationRequest] = deriveDecoder[SequenceConfigurationRequest]
}

final case class ExplorationSequenceConfigurationRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  explorationSequenceConfigurationId: ExplorationSequenceConfigurationId,
  @(ApiModelProperty @field)(dataType = "int", example = "12", required = true, allowableValues = "range[1, infinity]")
  sequenceSize: PosInt,
  @(ApiModelProperty @field)(
    dataType = "int",
    example = "1000",
    required = true,
    allowableValues = "range[1, infinity]"
  )
  maxTestedProposalCount: PosInt,
  @(ApiModelProperty @field)(dataType = "double", example = "0.5", required = true, allowableValues = "range[0, 1]")
  newRatio: Ratio,
  @(ApiModelProperty @field)(dataType = "double", example = "0.1", required = true, allowableValues = "range[0, 1]")
  controversyRatio: Ratio,
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = ExplorationSortAlgorithm.swaggerAllowableValues,
    required = true
  )
  topSorter: ExplorationSortAlgorithm,
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = ExplorationSortAlgorithm.swaggerAllowableValues,
    required = true
  )
  controversySorter: ExplorationSortAlgorithm,
  @(ApiModelProperty @field)(dataType = "double", example = "0.2", required = true, allowableValues = "range[0, 1]")
  keywordsThreshold: Ratio,
  @(ApiModelProperty @field)(dataType = "int", example = "2", required = true)
  candidatesPoolSize: Int
) {
  def toExplorationConfiguration: ExplorationSequenceConfiguration = ExplorationSequenceConfiguration(
    explorationSequenceConfigurationId = explorationSequenceConfigurationId,
    sequenceSize = sequenceSize,
    maxTestedProposalCount = maxTestedProposalCount,
    newRatio = newRatio,
    controversyRatio = controversyRatio,
    topSorter = topSorter,
    controversySorter = controversySorter,
    keywordsThreshold = keywordsThreshold,
    candidatesPoolSize = candidatesPoolSize
  )
}

object ExplorationSequenceConfigurationRequest {
  implicit val decoder: Decoder[ExplorationSequenceConfigurationRequest] = deriveDecoder
}

final case class SpecificSequenceConfigurationRequest(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  specificSequenceConfigurationId: SpecificSequenceConfigurationId,
  @(ApiModelProperty @field)(dataType = "int", example = "12", required = true, allowableValues = "range[1, infinity]")
  sequenceSize: PosInt,
  @(ApiModelProperty @field)(dataType = "double", example = "0.5", required = true, allowableValues = "range[0, 1]")
  newProposalsRatio: Double,
  @(ApiModelProperty @field)(
    dataType = "int",
    example = "1000",
    required = true,
    allowableValues = "range[1, infinity]"
  )
  maxTestedProposalCount: PosInt,
  @(ApiModelProperty @field)(
    dataType = "string",
    required = true,
    allowableValues = SelectionAlgorithmName.swaggerAllowableValues
  )
  selectionAlgorithmName: SelectionAlgorithmName
) {
  def toSpecificSequenceConfiguration: SpecificSequenceConfiguration = {
    SpecificSequenceConfiguration(
      specificSequenceConfigurationId = specificSequenceConfigurationId,
      sequenceSize = sequenceSize,
      newProposalsRatio = newProposalsRatio,
      maxTestedProposalCount = maxTestedProposalCount,
      selectionAlgorithmName = selectionAlgorithmName
    )
  }
}

object SpecificSequenceConfigurationRequest {
  implicit val decoder: Decoder[SpecificSequenceConfigurationRequest] =
    deriveDecoder[SpecificSequenceConfigurationRequest]
}
