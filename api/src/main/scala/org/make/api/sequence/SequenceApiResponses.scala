/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.sequence

import eu.timepit.refined.types.numeric.PosInt
import io.circe.refined._
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder
import io.swagger.annotations.{ApiModel, ApiModelProperty}
import org.make.api.demographics.DemographicsCardForSequence
import org.make.api.proposal.ProposalResponse
import org.make.core.question.QuestionId
import org.make.core.sequence.{
  ExplorationSequenceConfiguration,
  ExplorationSequenceConfigurationId,
  ExplorationSortAlgorithm,
  SelectionAlgorithmName,
  SequenceConfiguration,
  SpecificSequenceConfiguration,
  SpecificSequenceConfigurationId
}
import org.make.core.technical.RefinedTypes.Ratio

import scala.annotation.meta.field

final case class ExplorationSequenceConfigurationResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  explorationSequenceConfigurationId: ExplorationSequenceConfigurationId,
  @(ApiModelProperty @field)(dataType = "int", example = "12", allowableValues = "range[1, infinity]", required = true)
  sequenceSize: PosInt,
  @(ApiModelProperty @field)(
    dataType = "int",
    example = "1000",
    allowableValues = "range[1, infinity]",
    required = true
  )
  maxTestedProposalCount: PosInt,
  @(ApiModelProperty @field)(dataType = "double", example = "0.5", allowableValues = "range[0, 1]", required = true)
  newRatio: Ratio,
  @(ApiModelProperty @field)(dataType = "double", example = "0.1", allowableValues = "range[0, 1]", required = true)
  controversyRatio: Ratio,
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = ExplorationSortAlgorithm.swaggerAllowableValues,
    required = true
  )
  topSorter: ExplorationSortAlgorithm,
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = ExplorationSortAlgorithm.swaggerAllowableValues,
    required = true
  )
  controversySorter: ExplorationSortAlgorithm,
  @(ApiModelProperty @field)(dataType = "double", example = "0.2", allowableValues = "range[0, 1]", required = true)
  keywordsThreshold: Ratio,
  @(ApiModelProperty @field)(dataType = "int", example = "2", required = true)
  candidatesPoolSize: Int
)

object ExplorationSequenceConfigurationResponse {
  implicit val encoder: Encoder[ExplorationSequenceConfigurationResponse] = deriveEncoder

  def fromExplorationConfiguration(conf: ExplorationSequenceConfiguration): ExplorationSequenceConfigurationResponse =
    ExplorationSequenceConfigurationResponse(
      explorationSequenceConfigurationId = conf.explorationSequenceConfigurationId,
      sequenceSize = conf.sequenceSize,
      maxTestedProposalCount = conf.maxTestedProposalCount,
      newRatio = conf.newRatio,
      controversyRatio = conf.controversyRatio,
      topSorter = conf.topSorter,
      controversySorter = conf.controversySorter,
      keywordsThreshold = conf.keywordsThreshold,
      candidatesPoolSize = conf.candidatesPoolSize
    )
}

@ApiModel
final case class SequenceConfigurationResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: QuestionId,
  main: ExplorationSequenceConfigurationResponse,
  controversial: SpecificSequenceConfigurationResponse,
  popular: SpecificSequenceConfigurationResponse,
  keyword: SpecificSequenceConfigurationResponse,
  @(ApiModelProperty @field)(dataType = "int", example = "100", allowableValues = "range[0, infinity]", required = true)
  newProposalsVoteThreshold: Int,
  @(ApiModelProperty @field)(dataType = "double", example = "0.8")
  testedProposalsEngagementThreshold: Option[Double],
  @(ApiModelProperty @field)(dataType = "double", example = "0.0")
  testedProposalsScoreThreshold: Option[Double],
  @(ApiModelProperty @field)(dataType = "double", example = "0.0")
  testedProposalsControversyThreshold: Option[Double],
  @(ApiModelProperty @field)(dataType = "int", example = "1500")
  testedProposalsMaxVotesThreshold: Option[Int],
  @(ApiModelProperty @field)(dataType = "double", example = "0.5", required = true)
  nonSequenceVotesWeight: Double
)

object SequenceConfigurationResponse {
  implicit val encoder: Encoder[SequenceConfigurationResponse] = deriveEncoder[SequenceConfigurationResponse]

  def fromSequenceConfiguration(configuration: SequenceConfiguration): SequenceConfigurationResponse = {
    SequenceConfigurationResponse(
      id = configuration.questionId,
      main = ExplorationSequenceConfigurationResponse.fromExplorationConfiguration(configuration.mainSequence),
      controversial =
        SpecificSequenceConfigurationResponse.fromSpecificSequenceConfiguration(configuration.controversial),
      popular = SpecificSequenceConfigurationResponse.fromSpecificSequenceConfiguration(configuration.popular),
      keyword = SpecificSequenceConfigurationResponse.fromSpecificSequenceConfiguration(configuration.keyword),
      newProposalsVoteThreshold = configuration.newProposalsVoteThreshold,
      testedProposalsEngagementThreshold = configuration.testedProposalsEngagementThreshold,
      testedProposalsScoreThreshold = configuration.testedProposalsScoreThreshold,
      testedProposalsControversyThreshold = configuration.testedProposalsControversyThreshold,
      testedProposalsMaxVotesThreshold = configuration.testedProposalsMaxVotesThreshold,
      nonSequenceVotesWeight = configuration.nonSequenceVotesWeight
    )
  }
}

final case class SpecificSequenceConfigurationResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  specificSequenceConfigurationId: SpecificSequenceConfigurationId,
  @(ApiModelProperty @field)(dataType = "int", example = "12", allowableValues = "range[1, infinity]", required = true)
  sequenceSize: PosInt,
  @(ApiModelProperty @field)(dataType = "double", example = "0.5", allowableValues = "range[0, 1]", required = true)
  newProposalsRatio: Double,
  @(ApiModelProperty @field)(
    dataType = "int",
    example = "1000",
    allowableValues = "range[1, infinity]",
    required = true
  )
  maxTestedProposalCount: PosInt,
  @(ApiModelProperty @field)(
    dataType = "string",
    allowableValues = SelectionAlgorithmName.swaggerAllowableValues,
    required = true
  )
  selectionAlgorithmName: SelectionAlgorithmName
)

object SpecificSequenceConfigurationResponse {
  implicit val encoder: Encoder[SpecificSequenceConfigurationResponse] = deriveEncoder

  def fromSpecificSequenceConfiguration(
    configuration: SpecificSequenceConfiguration
  ): SpecificSequenceConfigurationResponse = {
    SpecificSequenceConfigurationResponse(
      specificSequenceConfigurationId = configuration.specificSequenceConfigurationId,
      sequenceSize = configuration.sequenceSize,
      newProposalsRatio = configuration.newProposalsRatio,
      maxTestedProposalCount = configuration.maxTestedProposalCount,
      selectionAlgorithmName = configuration.selectionAlgorithmName
    )
  }
}

final case class KeywordSequenceResult(
  key: String,
  label: String,
  proposals: Seq[ProposalResponse],
  demographics: Seq[DemographicsCardForSequence]
)

object KeywordSequenceResult {
  implicit val encoder: Encoder[KeywordSequenceResult] = deriveEncoder
}

final case class FirstProposalResponse(
  proposal: ProposalResponse,
  @(ApiModelProperty @field)(dataType = "int", allowableValues = "range[1, infinity]", required = true)
  sequenceSize: PosInt
)

object FirstProposalResponse {
  implicit val encoder: Encoder[FirstProposalResponse] = deriveEncoder[FirstProposalResponse]
}
