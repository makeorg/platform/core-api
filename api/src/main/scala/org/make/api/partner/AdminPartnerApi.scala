/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.partner

import cats.implicits._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import io.circe.{Decoder, Encoder}
import io.swagger.annotations._

import javax.ws.rs.Path
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import org.make.api.partner.DefaultPersistentPartnerServiceComponent.PersistentPartner
import org.make.api.technical.MakeDirectives.MakeDirectivesDependencies
import org.make.api.technical.{`X-Total-Count`, MakeAuthenticationDirectives}
import org.make.api.technical.directives.FutureDirectivesExtensions._
import org.make.core.{HttpCodes, Order, ParameterExtractors}
import org.make.core.technical.Pagination
import org.make.core.auth.UserRights
import org.make.core.partner.{Partner, PartnerId, PartnerKind}
import org.make.core.question.QuestionId
import org.make.core.user.UserId
import scalaoauth2.provider.AuthInfo

import scala.annotation.meta.field

@Api(
  value = "Admin Partner",
  authorizations = Array(
    new Authorization(
      value = "MakeApi",
      scopes = Array(new AuthorizationScope(scope = "admin", description = "BO Admin"))
    )
  )
)
@Path(value = "/admin/partners")
trait AdminPartnerApi extends Directives {

  @ApiOperation(value = "post-partner", httpMethod = "POST", code = HttpCodes.Created)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.Created, message = "Created", response = classOf[PartnerIdResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "body", paramType = "body", dataType = "org.make.api.partner.CreatePartnerRequest")
    )
  )
  @Path(value = "/")
  def adminPostPartner: Route

  @ApiOperation(value = "put-partner", httpMethod = "PUT", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[PartnerIdResponse]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(name = "body", paramType = "body", dataType = "org.make.api.partner.UpdatePartnerRequest"),
      new ApiImplicitParam(name = "partnerId", paramType = "path", dataType = "string")
    )
  )
  @Path(value = "/{partnerId}")
  def adminPutPartner: Route

  @ApiOperation(value = "get-partners", httpMethod = "GET", code = HttpCodes.OK)
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[Array[PartnerResponse]]))
  )
  @ApiImplicitParams(
    value = Array(
      new ApiImplicitParam(
        name = "_start",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_end",
        paramType = "query",
        dataType = "int",
        allowableValues = "range[0, infinity]"
      ),
      new ApiImplicitParam(
        name = "_sort",
        paramType = "query",
        dataType = "string",
        allowableValues = PersistentPartner.swaggerAllowableValues
      ),
      new ApiImplicitParam(
        name = "_order",
        paramType = "query",
        dataType = "string",
        allowableValues = Order.swaggerAllowableValues
      ),
      new ApiImplicitParam(name = "questionId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(name = "organisationId", paramType = "query", dataType = "string"),
      new ApiImplicitParam(
        name = "partnerKind",
        paramType = "query",
        dataType = "string",
        allowableValues = PartnerKind.swaggerAllowableValues
      )
    )
  )
  @Path(value = "/")
  def adminGetPartners: Route

  @ApiOperation(value = "get-partner", httpMethod = "GET", code = HttpCodes.OK)
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "partnerId", paramType = "path", dataType = "string")))
  @ApiResponses(
    value = Array(new ApiResponse(code = HttpCodes.OK, message = "Ok", response = classOf[PartnerResponse]))
  )
  @Path(value = "/{partnerId}")
  def adminGetPartner: Route

  @ApiOperation(value = "delete-partner", httpMethod = "DELETE", code = HttpCodes.NoContent)
  @ApiResponses(value = Array(new ApiResponse(code = HttpCodes.NoContent, message = "No Content")))
  @ApiImplicitParams(value = Array(new ApiImplicitParam(name = "partnerId", paramType = "path", dataType = "string")))
  @Path(value = "/{partnerId}")
  def adminDeletePartner: Route

  def routes: Route =
    adminGetPartner ~ adminGetPartners ~ adminPostPartner ~ adminPutPartner ~ adminDeletePartner
}

trait AdminPartnerApiComponent {
  def adminPartnerApi: AdminPartnerApi
}

trait DefaultAdminPartnerApiComponent
    extends AdminPartnerApiComponent
    with MakeAuthenticationDirectives
    with ParameterExtractors {
  this: MakeDirectivesDependencies with PartnerServiceComponent =>

  override lazy val adminPartnerApi: AdminPartnerApi = new DefaultAdminPartnerApi

  class DefaultAdminPartnerApi extends AdminPartnerApi {

    private val partnerId: PathMatcher1[PartnerId] = Segment.map(id => PartnerId(id))

    override def adminPostPartner: Route = {
      post {
        path("admin" / "partners") {
          makeOperation("ModerationPostPartner") { _ =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireAdminRole(auth.user) {
                decodeRequest {
                  entity(as[CreatePartnerRequest]) { request =>
                    onSuccess(partnerService.createPartner(request)) { result =>
                      complete(StatusCodes.Created -> PartnerIdResponse(result.partnerId))
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def adminPutPartner: Route = {
      put {
        path("admin" / "partners" / partnerId) { partnerId =>
          makeOperation("ModerationPutPartner") { _ =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireAdminRole(auth.user) {
                decodeRequest {
                  entity(as[UpdatePartnerRequest]) { request =>
                    partnerService.updatePartner(partnerId, request).asDirectiveOrNotFound { result =>
                      complete(StatusCodes.OK -> PartnerIdResponse(result.partnerId))
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    override def adminGetPartners: Route = {
      get {
        path("admin" / "partners") {
          makeOperation("ModerationGetPartners") { _ =>
            parameters(
              "_start".as[Pagination.Offset].?,
              "_end".as[Pagination.End].?,
              "_sort".?,
              "_order".as[Order].?,
              "questionId".as[QuestionId].?,
              "organisationId".as[UserId].?,
              "partnerKind".as[PartnerKind].?
            ) {
              (
                offset: Option[Pagination.Offset],
                end: Option[Pagination.End],
                sort: Option[String],
                order: Option[Order],
                questionId: Option[QuestionId],
                organisationId: Option[UserId],
                partnerKind: Option[PartnerKind]
              ) =>
                makeOAuth2 { auth: AuthInfo[UserRights] =>
                  requireAdminRole(auth.user) {
                    (
                      partnerService
                        .find(offset.orZero, end, sort, order, questionId, organisationId, partnerKind)
                        .asDirective,
                      partnerService.count(questionId, organisationId, partnerKind).asDirective
                    ).tupled.apply {
                      case (result, count) =>
                        complete(
                          (StatusCodes.OK, List(`X-Total-Count`(count.toString)), result.map(PartnerResponse.apply))
                        )
                    }
                  }
                }
            }
          }
        }
      }
    }

    override def adminGetPartner: Route = {
      get {
        path("admin" / "partners" / partnerId) { partnerId =>
          makeOperation("ModerationGetPartner") { _ =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireAdminRole(auth.user) {
                partnerService.getPartner(partnerId).asDirectiveOrNotFound { partner =>
                  complete(PartnerResponse(partner))
                }
              }
            }
          }
        }
      }
    }

    override def adminDeletePartner: Route = {
      delete {
        path("admin" / "partners" / partnerId) { partnerId =>
          makeOperation("ModerationDeletePartner") { _ =>
            makeOAuth2 { auth: AuthInfo[UserRights] =>
              requireAdminRole(auth.user) {
                partnerService.deletePartner(partnerId).asDirective { _ =>
                  complete(StatusCodes.NoContent)
                }
              }
            }
          }
        }
      }
    }
  }
}

final case class PartnerResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: PartnerId,
  name: String,
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/logo.png")
  logo: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "https://example.com/link")
  link: Option[String],
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  organisationId: Option[UserId],
  @(ApiModelProperty @field)(dataType = "string", allowableValues = PartnerKind.swaggerAllowableValues)
  partnerKind: PartnerKind,
  weight: Float
)

object PartnerResponse {
  def apply(partner: Partner): PartnerResponse = PartnerResponse(
    id = partner.partnerId,
    name = partner.name,
    logo = partner.logo,
    link = partner.link,
    organisationId = partner.organisationId,
    partnerKind = partner.partnerKind,
    weight = partner.weight
  )

  implicit val decoder: Decoder[PartnerResponse] = deriveDecoder[PartnerResponse]
  implicit val encoder: Encoder[PartnerResponse] = deriveEncoder[PartnerResponse]
}

final case class PartnerIdResponse(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  id: PartnerId,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555", required = true)
  partnerId: PartnerId
)

object PartnerIdResponse {
  implicit val encoder: Encoder[PartnerIdResponse] = deriveEncoder[PartnerIdResponse]

  def apply(id: PartnerId): PartnerIdResponse = PartnerIdResponse(id, id)
}
