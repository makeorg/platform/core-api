include "default-application.conf"

akka.http.parsing.illegal-header-warnings = off
akka.debug.lifecycle = off
akka.debug.log-sent-messages = off

# Check https://doc.akka.io/docs/akka-enhancements/current/split-brain-resolver.html for more informations on this config
akka.cluster {
  downing-provider-class = "akka.cluster.sbr.SplitBrainResolverProvider"
  down-removal-margin = 7s
  down-removal-margin = ${?DOWN_REMOVAL_MARGIN}
  split-brain-resolver {
    active-strategy = static-quorum
    stable-after = 7s
    stable-after = ${?STABLE_AFTER}
    down-all-when-unstable = on
    static-quorum {
      quorum-size = 2
      quorum-size = ${?QUORUM_SIZE}
    }
  }
}

akka.persistence.cassandra {
  journal {
    replication-factor = 3
  }
  snapshot {
    replication-factor = 3
  }
}

datastax-java-driver.basic {
  contact-points = []
  contact-points += ${?CASSANDRA_0}":"${datastax-java-driver.basic.port}
  contact-points += ${?CASSANDRA_1}":"${datastax-java-driver.basic.port}
  contact-points += ${?CASSANDRA_2}":"${datastax-java-driver.basic.port}
  contact-points += ${?CASSANDRA_3}":"${datastax-java-driver.basic.port}
  contact-points += ${?CASSANDRA_4}":"${datastax-java-driver.basic.port}
}

make-api {

  authorized-cors-uri: [
    "http://localhost:9009",
    "http://localhost:4242",
    "http://localhost:3000",
    "https://local.makeorg.tech:3000",
    "https://www.preprod.makeorg.tech",
    "https://bo.preprod.makeorg.tech",
    "https://backoffice.preprod.makeorg.tech",
    "https://cdn.preprod.makeorg.tech",
    "https://widget.preprod.makeorg.tech",
    "https://accessible.preprod.makeorg.tech",
    "https://app.preprod.makeorg.tech",
    "https://about.make.org",
    "https://concertation.webflow.io",
    "https://concertation.preprod.makeorg.tech",
    "https://www.concertation.webflow.io",
    "https://www.concertation.preprod.makeorg.tech",
    "https://assembly.preprod.makeorg.tech",
    "https://content.preprod.makeorg.tech",
    "https://backoffice.assembly.preprod.makeorg.tech",
    "https://playground.preprod.makeorg.tech",
    "https://status.makeorg.tech",
    "https://make-ui.preprod.makeorg.tech",
    "https://make-bo.preprod.makeorg.tech",
    "https://make-widget.preprod.makeorg.tech",
    "https://dialogue-ui.preprod.makeorg.tech",
    "https://content-api.preprod.makeorg.tech",
    "https://panoramic-bo.preprod.makeorg.tech"
  ]

  cookie-session {
    is-secure = true
  }

  cookie-secure {
    is-secure = true
  }

  cookie-visitor {
    is-secure = true
  }

  cookie-user-id {
    is-secure = true
  }

  default-admin {
    first-name: "admin"
    first-name: ${?APP_ADMIN_DEFAULT_FIRST_NAME}
    email: "admin@make.org"
    email: ${?APP_ADMIN_DEFAULT_EMAIL}
    password: "vie34baliM22l?mais"
    password: ${?APP_ADMIN_DEFAULT_PASSWORD}
  }

  database {
    migration {
      init-schema = false
    }
  }

  environment = "preproduction"

  http {
    ssl = true
  }

  secrets-configuration-path = "/var/run/secrets/make-api.conf"

  storage {
    base-url = "https://assets.preprod.makeorg.tech"
  }

  urls {
    front = "https://www.preprod.makeorg.tech"
    backoffice = "https://backoffice.preprod.makeorg.tech"
    widget = "https://widget.preprod.makeorg.tech"
  }

  user-proposals-thresholds {
    warn = 8
    block = 10
  }

  report-proposal {
    template-id = 4527992
    recipient = "technique@make.org"
  }
}

