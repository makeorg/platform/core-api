/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.user

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.headers.{Authorization, OAuth2BearerToken}
import org.make.api.{MakeApi, MakeApiTestBase, TestUtils}
import org.make.api.proposal.{MaxPropositionsThresholds, MaxPropositionsThresholdsComponent}
import org.make.api.question.{QuestionService, QuestionServiceComponent}
import org.make.core.question.QuestionId
import org.make.core.user._

import scala.concurrent.Future

class ModerationUserApiTest
    extends MakeApiTestBase
    with UserServiceComponent
    with QuestionServiceComponent
    with UserProposalsServiceComponent
    with MaxPropositionsThresholdsComponent
    with DefaultModerationUserApiComponent
    with PersistentUserProposalsServiceComponent {

  override val userService: UserService = mock[UserService]
  override val questionService: QuestionService = mock[QuestionService]
  override val userProposalsService: UserProposalsService = mock[UserProposalsService]
  override val maxPropositionsThresholds: MaxPropositionsThresholds = mock[MaxPropositionsThresholds]

  when(maxPropositionsThresholds.warnThreshold).thenReturn(80)
  when(maxPropositionsThresholds.blockThreshold).thenReturn(100)

  override val persistentUserProposalsService: PersistentUserProposalsService =
    mock[PersistentUserProposalsService]

  private val routes: Route = sealRoute(handleRejections(MakeApi.rejectionHandler) {
    moderationUserApi.routes
  })

  private val user = TestUtils.user(id = UserId("123"))
  private val unexistingUserId = UserId("foo")
  private val userIdString = user.userId.value
  private val questionId = QuestionId("456")
  private val unexistingQuestionId = QuestionId("789")

  when(userService.getUser(user.userId)).thenReturn(Future.successful(Some(user)))
  when(userService.getUser(unexistingUserId)).thenReturn(Future.successful(None))

  when(questionService.getQuestion(questionId)).thenReturn(Future.successful(Some(question(questionId))))
  when(questionService.getQuestion(unexistingQuestionId)).thenReturn(Future.successful(None))

  when(userProposalsService.getCounter(user.userId, questionId)).thenReturn(Future.successful(42))

  Feature("get the connected user") {
    Scenario("no auth token") {
      Get(s"/moderation/users/$userIdString/${questionId.value}/proposals-count") ~> routes ~> check {

        status should be(StatusCodes.Unauthorized)
      }
    }

    Scenario("connected citizen") {
      Get(s"/moderation/users/$userIdString/${questionId.value}/proposals-count")
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {

        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("moderator authtoken") {
      Get(s"/moderation/users/$userIdString/${questionId.value}/proposals-count")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {

        status should be(StatusCodes.OK)

        val response = entityAs[NumberOfProposalsResponse]
        response.count should be(42)
        response.thresholds.warn should be(80)
        response.thresholds.block should be(100)
      }
    }

    Scenario("moderator authtoken, unexisting user") {
      Get(s"/moderation/users/${unexistingUserId.value}/${questionId.value}/proposals-count")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {

        status should be(StatusCodes.NotFound)
      }
    }

    Scenario("moderator authtoken, unexisting question") {
      Get(s"/moderation/users/$userIdString/${unexistingQuestionId.value}/proposals-count")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {

        status should be(StatusCodes.NotFound)
      }
    }
  }
}
