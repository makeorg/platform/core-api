/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.operation
import java.time.ZonedDateTime
import akka.http.scaladsl.model.headers.{Authorization, OAuth2BearerToken}
import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import cats.data.NonEmptyList
import eu.timepit.refined.auto._
import io.circe.syntax._
import org.make.api.MakeApiTestBase
import org.make.api.demographics.{ActiveDemographicsCardService, ActiveDemographicsCardServiceComponent}
import org.make.api.widget.{WidgetService, WidgetServiceComponent}
import org.make.api.question.{QuestionService, QuestionServiceComponent, SearchQuestionRequest}
import org.make.api.technical.crm.{PersistentCrmUserService, PersistentCrmUserServiceComponent}
import org.make.core.technical.Multilingual
import org.make.core.auth.UserRights
import org.make.core.demographics.DemographicsCardId
import org.make.core.operation.OperationKind.GreatCause
import org.make.core.{Order, ValidationError}
import org.make.core.operation._
import org.make.core.operation.indexed.{OperationOfQuestionElasticsearchFieldName, OperationOfQuestionSearchResult}
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}

import scala.concurrent.{Await, Future}
import org.make.core.technical.Pagination
import org.make.core.user.Role
import scalaoauth2.provider.AuthInfo

import scala.concurrent.duration.DurationInt

class DefaultModerationOperationOfQuestionApiComponentTest
    extends MakeApiTestBase
    with DefaultModerationOperationOfQuestionApiComponent
    with OperationOfQuestionServiceComponent
    with QuestionServiceComponent
    with WidgetServiceComponent
    with OperationServiceComponent
    with ActiveDemographicsCardServiceComponent
    with PersistentCrmUserServiceComponent {

  override val operationOfQuestionService: OperationOfQuestionService = mock[OperationOfQuestionService]
  override val questionService: QuestionService = mock[QuestionService]
  override val widgetService: WidgetService = mock[WidgetService]
  override val operationService: OperationService = mock[OperationService]
  override val activeDemographicsCardService: ActiveDemographicsCardService = mock[ActiveDemographicsCardService]
  override val persistentCrmUserService: PersistentCrmUserService = mock[PersistentCrmUserService]

  when(operationOfQuestionService.create(any[CreateOperationOfQuestion])).thenAnswer {
    request: CreateOperationOfQuestion =>
      Future.successful(
        operationOfQuestion(
          questionId = QuestionId("some-question"),
          operationId = request.operationId,
          startDate = request.startDate,
          endDate = request.endDate,
          operationTitles = Multilingual(request.defaultLanguage -> request.operationTitle),
          featured = request.featured
        ).copy(proposalsCount = 1)
      )
  }

  when(operationOfQuestionService.findByQuestionId(any[QuestionId])).thenAnswer { questionId: QuestionId =>
    Future.successful(Some(operationOfQuestion(questionId = questionId, operationId = OperationId("some-operation"))))
  }

  when(operationOfQuestionService.updateWithQuestion(any[OperationOfQuestion], any[Question])).thenAnswer {
    (ooq: OperationOfQuestion, _: Question) =>
      Future.successful(ooq)
  }

  when(questionService.getQuestion(any[QuestionId])).thenAnswer { questionId: QuestionId =>
    Future.successful(
      Some(
        question(
          id = questionId,
          slug = "some-question",
          countries = NonEmptyList.of(Country("BE"), Country("FR")),
          defaultLanguage = Language("fr"),
          languages = NonEmptyList.of(Language("fr"), Language("nl"))
        )
      )
    )
  }

  when(operationOfQuestionService.delete(any[QuestionId])).thenReturn(Future.unit)

  when(operationOfQuestionService.findByOperationId(any[OperationId])).thenAnswer { operationId: OperationId =>
    Future.successful(
      Seq(
        operationOfQuestion(
          questionId = QuestionId("question-1"),
          operationId = operationId,
          operationTitles = Multilingual.fromDefault("opération en Français")
        ),
        operationOfQuestion(
          questionId = QuestionId("question-2"),
          operationId = operationId,
          operationTitles = Multilingual.fromDefault("Operation in English")
        )
      )
    )
  }

  when(questionService.searchQuestion(any[SearchQuestionRequest])).thenAnswer { request: SearchQuestionRequest =>
    Future.successful(
      Seq(
        question(
          id = QuestionId("question-1"),
          countries = NonEmptyList.of(Country("FR")),
          languages = NonEmptyList.of(Language("fr")),
          slug = "question-1",
          operationId = request.maybeOperationIds.flatMap(_.headOption)
        ),
        question(
          id = QuestionId("question-2"),
          countries = NonEmptyList.of(Country("IE")),
          languages = NonEmptyList.of(Language("en")),
          slug = "question-2",
          operationId = request.maybeOperationIds.flatMap(_.headOption)
        )
      )
    )
  }

  when(questionService.getQuestions(any[Seq[QuestionId]])).thenAnswer { ids: Seq[QuestionId] =>
    Future.successful(ids.map { questionId =>
      question(
        id = questionId,
        slug = questionId.value,
        countries = NonEmptyList.of(Country("FR")),
        languages = NonEmptyList.of(Language("fr"))
      )
    })
  }

  when(
    operationOfQuestionService
      .find(
        any[Pagination.Offset],
        any[Option[Pagination.End]],
        any[Option[String]],
        any[Option[Order]],
        any[SearchOperationsOfQuestions]
      )
  ).thenReturn(
    Future.successful(
      Seq(
        operationOfQuestion(questionId = QuestionId("question-1"), operationId = OperationId("operation-1")),
        operationOfQuestion(questionId = QuestionId("question-2"), operationId = OperationId("operation-2"))
      )
    )
  )

  when(operationOfQuestionService.count(any[SearchOperationsOfQuestions])).thenReturn(Future.successful(2))

  when(activeDemographicsCardService.count(any[Option[QuestionId]], any[Option[DemographicsCardId]]))
    .thenReturn(Future.successful(0))

  when(persistentCrmUserService.getActiveConsultationUsersCount(any[String])).thenReturn(Future.successful(42))

  val routes: Route = sealRoute(moderationOperationOfQuestionApi.routes)

  Feature("access control") {
    Scenario("unauthenticated user") {
      Get("/moderation/operations-of-questions") ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }

      Post("/moderation/operations-of-questions") ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }

      Get("/moderation/operations-of-questions/some-question") ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }

      Put("/moderation/operations-of-questions/some-question") ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }

      Delete("/moderation/operations-of-questions/some-question") ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }
    }

    Scenario("invalid token") {

      Get("/moderation/operations-of-questions")
        .withHeaders(Authorization(OAuth2BearerToken("invalid-token"))) ~> routes ~> check {

        status should be(StatusCodes.Unauthorized)
      }

      Post("/moderation/operations-of-questions")
        .withHeaders(Authorization(OAuth2BearerToken("invalid-token"))) ~> routes ~> check {

        status should be(StatusCodes.Unauthorized)
      }

      Get("/moderation/operations-of-questions/some-question")
        .withHeaders(Authorization(OAuth2BearerToken("invalid-token"))) ~> routes ~> check {

        status should be(StatusCodes.Unauthorized)
      }

      Put("/moderation/operations-of-questions/some-question")
        .withHeaders(Authorization(OAuth2BearerToken("invalid-token"))) ~> routes ~> check {

        status should be(StatusCodes.Unauthorized)
      }

      Delete("/moderation/operations-of-questions/some-question")
        .withHeaders(Authorization(OAuth2BearerToken("invalid-token"))) ~> routes ~> check {

        status should be(StatusCodes.Unauthorized)
      }
    }

    Scenario("citizen user") {

      Get("/moderation/operations-of-questions")
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {

        status should be(StatusCodes.Forbidden)
      }

      Post("/moderation/operations-of-questions")
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {

        status should be(StatusCodes.Forbidden)
      }

      Get("/moderation/operations-of-questions/some-question")
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {

        status should be(StatusCodes.Forbidden)
      }

      Put("/moderation/operations-of-questions/some-question")
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {

        status should be(StatusCodes.Forbidden)
      }

      Delete("/moderation/operations-of-questions/some-question")
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {

        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("admin-only endpoints") {
      Post("/moderation/operations-of-questions")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {

        status should be(StatusCodes.Forbidden)
      }

      Put("/moderation/operations-of-questions/some-question")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {

        status should be(StatusCodes.Forbidden)
      }

      Delete("/moderation/operations-of-questions/some-question")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {

        status should be(StatusCodes.Forbidden)
      }
    }

  }

  Feature("create operationOfQuestion") {
    when(operationService.findOneSimple(eqTo(OperationId("some-operation"))))
      .thenReturn(Future.successful(Some(simpleOperation(OperationId("some-operation"), operationKind = GreatCause))))
    val existingQuestion = operationOfQuestion(
      questionId = QuestionId("existing-question"),
      operationId = OperationId("operation"),
      operationTitles = Multilingual.fromDefault("opération en Français")
    )

    Scenario("create as moderator") {
      when(operationOfQuestionService.findByQuestionSlug("make-the-world-great-again"))
        .thenReturn(Future.successful(None))

      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Post("/moderation/operations-of-questions")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(
            ContentTypes.`application/json`,
            CreateOperationOfQuestionRequest(
              operationId = OperationId("some-operation"),
              startDate = ZonedDateTime.parse("2018-12-01T10:15:30+00:00"),
              endDate = ZonedDateTime.parse("2068-07-03T00:00:00.000Z"),
              operationTitle = "my-operation",
              proposalPrefix = "Il faut",
              countries = NonEmptyList.of(Country("FR")),
              defaultLanguage = Language("fr"),
              question = "how to save the world?",
              questionSlug = "make-the-world-great-again"
            ).asJson.toString()
          ) ~> routes ~> check {

          status should be(StatusCodes.Created)
          val operationOfQuestion = entityAs[OperationOfQuestionResponse]
          operationOfQuestion.canPropose shouldBe true
          operationOfQuestion.featured shouldBe true
        }
      }
    }

    Scenario("slug already exists") {
      when(operationOfQuestionService.findByQuestionSlug("existing-question"))
        .thenReturn(Future.successful(Some(existingQuestion)))
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Post("/moderation/operations-of-questions")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(
            ContentTypes.`application/json`,
            CreateOperationOfQuestionRequest(
              operationId = OperationId("some-operation"),
              startDate = ZonedDateTime.parse("2018-12-01T10:15:30+00:00"),
              endDate = ZonedDateTime.parse("2068-07-03T00:00:00.000Z"),
              operationTitle = "my-operation",
              proposalPrefix = "Il faut",
              countries = NonEmptyList.of(Country("FR")),
              defaultLanguage = Language("fr"),
              question = "how to save the world?",
              questionSlug = "existing-question"
            ).asJson.toString()
          ) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          val contentError = errors.find(_.field == "slug")
          contentError should be(
            Some(ValidationError("slug", "non_empty", Some("Slug 'existing-question' already exists")))
          )
        }
      }
    }

    Scenario("create as moderator with bad consultationImage format") {
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Post("/moderation/operations-of-questions")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(ContentTypes.`application/json`, """{
            |"operationId": "some-operation",
            |"startDate": "2018-12-01T10:15:30+00:00",
            |"question": "question ?",
            |"questionSlug": "question-slug",
            |"operationTitle": "my-operation",
            |"country": "FR",
            |"language": "fr",
            |"consultationImage": "wrongurlformat"
            |}""".stripMargin) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
        }
      }
    }

    Scenario("operation does no exist") {
      when(operationService.findOneSimple(eqTo(OperationId("fake")))).thenReturn(Future.successful(None))
      when(operationOfQuestionService.findByQuestionSlug("whatever-question"))
        .thenReturn(Future.successful(None))
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Post("/moderation/operations-of-questions")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(
            ContentTypes.`application/json`,
            CreateOperationOfQuestionRequest(
              operationId = OperationId("fake"),
              startDate = ZonedDateTime.parse("2018-12-01T10:15:30+00:00"),
              endDate = ZonedDateTime.parse("2068-07-03T00:00:00.000Z"),
              operationTitle = "my-operation",
              proposalPrefix = "Il faut",
              countries = NonEmptyList.of(Country("FR")),
              defaultLanguage = Language("fr"),
              question = "how to save the world?",
              questionSlug = "whatever-question"
            ).asJson.toString()
          ) ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          errors.size should be(1)
          errors.head.field shouldBe "operationId"
        }
      }
    }
  }

  Feature("update operationOfQuestion") {

    val updateRequest = ModifyOperationOfQuestionRequest(
      defaultLanguage = Language("fr"),
      languages = NonEmptyList.of(Language("fr"), Language("nl")),
      startDate = ZonedDateTime.parse("2018-12-01T10:15:30+00:00"),
      endDate = ZonedDateTime.parse("2068-07-03T00:00:00.000Z"),
      canPropose = true,
      questions = Multilingual((Language("fr"), "question ?"), (Language("nl"), "question nl")),
      operationTitles = Multilingual((Language("fr"), "new title"), (Language("nl"), "operation nl")),
      proposalPrefixes = Multilingual((Language("fr"), "Il faut"), (Language("nl"), "Il faut en nl")),
      countries = NonEmptyList.of(Country("BE"), Country("FR")),
      shortTitles = None,
      sequenceCardsConfiguration = SequenceCardsConfiguration.default,
      aboutUrls = None,
      metas = Metas(titles = None, descriptions = None, picture = None),
      theme = QuestionTheme.default,
      descriptions = None,
      displayResults = true,
      consultationImages = None,
      consultationImageAlts = None,
      descriptionImages = None,
      descriptionImageAlts = None,
      partnersLogos = None,
      partnersLogosAlt = None,
      initiatorsLogos = None,
      initiatorsLogosAlt = None,
      consultationHeader = None,
      consultationHeaderAlts = None,
      cobrandingLogo = None,
      cobrandingLogoAlt = None,
      resultsLink = Some(ResultsLinkRequest(ResultsLinkKind.External, "https://example.com/results")),
      actions = None,
      featured = false,
      votesTarget = 100_000,
      timeline = OperationOfQuestionTimelineContract(
        TimelineElementContract(defined = false, None, Multilingual.empty, Multilingual.empty),
        TimelineElementContract(defined = false, None, Multilingual.empty, Multilingual.empty),
        TimelineElementContract(defined = false, None, Multilingual.empty, Multilingual.empty)
      ),
      sessionBindingMode = false,
      reportUrl = None,
      actionsUrl = None
    )

    when(operationOfQuestionService.findByQuestionId(any[QuestionId])).thenAnswer { questionId: QuestionId =>
      Future.successful(
        Some(
          operationOfQuestion(questionId = questionId, operationId = OperationId("some-operation"), proposalsCount = 0)
        )
      )
    }

    when(widgetService.count(any[QuestionId])).thenAnswer { (_: QuestionId) =>
      Future.successful(0)
    }

    Scenario("update as moderator") {
      when(operationOfQuestionService.findByQuestionId(any[QuestionId])).thenAnswer { questionId: QuestionId =>
        Future.successful(
          Some(
            operationOfQuestion(
              questionId = questionId,
              operationId = OperationId("some-operation"),
              proposalsCount = 0
            )
          )
        )
      }
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(ContentTypes.`application/json`, updateRequest.asJson.toString()) ~> routes ~> check {

          status should be(StatusCodes.OK)
        }
      }
    }

    Scenario("add a country") {
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(
            ContentTypes.`application/json`,
            updateRequest.copy(countries = updateRequest.countries :+ Country("DE")).asJson.toString()
          ) ~> routes ~> check {

          status should be(StatusCodes.OK)
        }
      }
    }

    Scenario("try removing a country when a proposal has already been made") {
      when(operationOfQuestionService.findByQuestionId(any[QuestionId])).thenAnswer { questionId: QuestionId =>
        Future.successful(
          Some(operationOfQuestion(questionId = questionId, operationId = OperationId("some-operation")))
        )
      }
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(
            ContentTypes.`application/json`,
            updateRequest.copy(countries = NonEmptyList.of(Country("FR"))).asJson.toString()
          ) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          errors should contain(
            ValidationError("countries", "removal_forbidden", Some("You cannot remove existing countries: BE"))
          )
        }
      }
    }

    Scenario("try removing a language when proposals have already been made") {
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(
            ContentTypes.`application/json`,
            updateRequest.copy(languages = NonEmptyList.of(Language("fr"))).asJson.toString()
          ) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          errors should contain(
            ValidationError(
              "languages",
              "has_proposition",
              Some("Cannot remove languages from a consultation that already has had proposals made on it")
            )
          )
        }
      }
    }

    Scenario("try adding a language when proposals have already been made") {
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(
            ContentTypes.`application/json`,
            updateRequest
              .copy(
                languages = updateRequest.languages.append(Language("pl")),
                questions =
                  Multilingual.fromLanguagesMap(updateRequest.questions.toMap + (Language("pl") -> "question pl")),
                operationTitles = Multilingual
                  .fromLanguagesMap(updateRequest.operationTitles.toMap + (Language("pl") -> "operation pl"))
              )
              .asJson
              .toString()
          ) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          errors should contain(
            ValidationError(
              "languages",
              "has_proposition",
              Some("Cannot add a language on a consultation that already has had proposals made on it")
            )
          )
        }
      }
    }

    Scenario("try adding a bad reportUrl") {
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(
            ContentTypes.`application/json`,
            updateRequest
              .copy(reportUrl = Some("foo-bar"))
              .asJson
              .toString()
          ) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          errors should contain(
            ValidationError(
              "report_url",
              "valid_report_url",
              Some("foo-bar is not a valid report url: Url predicate failed: URI is not absolute")
            )
          )
        }
      }
    }

    Scenario("try adding a bad actionsUrl") {
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(
            ContentTypes.`application/json`,
            updateRequest
              .copy(actionsUrl = Some("foo-bar"))
              .asJson
              .toString()
          ) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          errors should contain(
            ValidationError(
              "actions_url",
              "valid_actions_url",
              Some("foo-bar is not a valid actions url: Url predicate failed: URI is not absolute")
            )
          )
        }
      }
    }

    Scenario("try removing a language when widgets are attached to consultation") {

      when(operationOfQuestionService.findByQuestionId(any[QuestionId])).thenAnswer { qId: QuestionId =>
        Future.successful(
          Some(operationOfQuestion(questionId = qId, operationId = OperationId("foo")).copy(proposalsCount = 0))
        )
      }

      when(widgetService.count(any[QuestionId])).thenAnswer { (_: QuestionId) =>
        Future.successful(1)
      }

      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(
            ContentTypes.`application/json`,
            updateRequest.copy(languages = NonEmptyList.of(Language("fr"))).asJson.toString()
          ) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          errors shouldBe Seq(
            ValidationError(
              "languages",
              "has_widget",
              Some("Cannot remove languages from a consultation that has a widget attached to it")
            )
          )
        }
      }
    }

    Scenario("update with bad color") {

      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(ContentTypes.`application/json`, """{
            | "defaultLanguage": "fr",
            | "languages": ["fr"],
            | "startDate": "2018-12-01T10:15:30.000Z",
            | "endDate": "2068-12-01T10:15:30.000Z",
            | "canPropose": true,
            | "questions": {"fr": "question ?"},
            | "operationTitles": {"fr": "title"},
            | "proposalPrefixes": {"fr": "Il faut"},
            | "countries": ["BE", "FR"],
            | "sequenceCardsConfiguration": {
            |   "introCard": { "enabled": true },
            |   "pushProposalCard": { "enabled": true },
            |   "finalCard": {
            |     "enabled": true,
            |     "sharingEnabled": true
            |   }
            | },
            | "metas": { "titles": {"fr": "metas"} },
            | "theme": {
            |   "color": "#000000",
            |   "fontColor": "wrongFormattedColor"
            | },
            | "descriptions": {"fr": "description"},
            | "displayResults": false,
            | "consultationImages": {"fr": "https://example"},
            | "featured": true,
            | "votesTarget": 100000,
            | "timeline": {
            |   "action": {
            |     "defined": true,
            |     "date": "2168-12-01",
            |     "dateTexts": {"fr": "in a long time"},
            |     "descriptions": {"fr": "action description"}
            |   },
            |   "result": {
            |     "defined": false,
            |     "date": "2168-12-01",
            |     "dateTexts": {"fr": "in a long time"},
            |     "descriptions": {"fr": "results description"}
            |   },
            |   "workshop": {
            |     "defined": true,
            |     "date": "2168-12-01",
            |     "dateTexts": {"fr": "in a long time"},
            |     "descriptions": {"fr": "workshop description"}
            |   }
            | },
            | "sessionBindingMode": false
            |}""".stripMargin) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          println(errors.head.field)
          errors.size should be(1)
          errors.head.field shouldBe "theme.fontColor"
        }
      }
    }

    Scenario("update with bad shortTitle") {

      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(
            ContentTypes.`application/json`,
            """{
           | "defaultLanguage": "fr",
           | "languages": ["fr"],
           | "startDate": "2018-12-01T10:15:30.000Z",
           | "endDate": "2068-12-01T10:15:30.000Z",
           | "canPropose": true,
           | "questions": {"fr": "question ?"},
           | "operationTitles": {"fr": "title"},
           | "proposalPrefixes": {"fr": "Il faut"},
           | "countries": ["BE", "FR"],
           | "shortTitles": {"fr": "Il s'agit d'un short title de plus de 30 charactères !"},
           | "sequenceCardsConfiguration": {
           |   "introCard": { "enabled": true },
           |   "pushProposalCard": { "enabled": true },
           |   "finalCard": {
           |     "enabled": true,
           |     "sharingEnabled": true
           |   }
           | },
           | "metas": { "titles": {"fr": "metas"} },
           | "theme": {
           |   "color": "#000000",
           |   "fontColor": "#000000"
           | },
           | "descriptions": {"fr": "description"},
           | "displayResults": false,
           | "consultationImages": {"fr": "https://example.com"},
           | "featured": true,
           | "votesTarget": 100000,
           | "timeline": {
           |   "action": {
           |     "defined": true,
           |     "date": "2168-12-01",
           |     "dateTexts": {"fr": "in a long time"},
           |     "descriptions": {"fr": "action description"}
           |   },
           |   "result": {
           |     "defined": false
           |   },
           |   "workshop": {
           |     "defined": false
           |   }
           | },
           | "sessionBindingMode": false
           |}""".stripMargin
          ) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          errors.size should be(1)
          errors.head.field shouldBe "shortTitles.fr"
        }
      }
    }

    Scenario("update image as moderator with incorrect URL") {
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(ContentTypes.`application/json`, """{
                                                       | "defaultLanguage": "fr",
                                                       | "languages": ["fr"],
                                                       | "startDate": "2018-12-01T10:15:30.000Z",
                                                       | "endDate": "2068-12-01T10:15:30.000Z",
                                                       | "canPropose": true,
                                                       | "questions": {"fr": "question ?"},
                                                       | "operationTitles": {"fr": "title"},
                                                       | "proposalPrefixes": {"fr": "Il faut"},
                                                       | "countries": ["BE", "FR"],
                                                       | "sequenceCardsConfiguration": {
                                                       |   "introCard": { "enabled": true },
                                                       |   "pushProposalCard": { "enabled": true },
                                                       |   "finalCard": {
                                                       |     "enabled": true,
                                                       |     "sharingEnabled": true
                                                       |   }
                                                       | },
                                                       | "metas": { "titles": {"fr": "metas"} },
                                                       | "theme": {
                                                       |   "gradientStart": "#000000",
                                                       |   "gradientEnd": "#000000",
                                                       |   "color": "#000000",
                                                       |   "fontColor": "#000000"
                                                       | },
                                                       | "descriptions": {"fr": "description"},
                                                       | "displayResults": false,
                                                       | "consultationImages": {"fr": "wrong URL"},
                                                       | "descriptionImages": {"fr": "wrong URL"},
                                                       | "featured": true,
                                                       | "votesTarget": 100000,
                                                       | "timeline": {
                                                       |   "action": {
                                                       |     "defined": false
                                                       |   },
                                                       |   "result": {
                                                       |     "defined": false
                                                       |   },
                                                       |   "workshop": {
                                                       |     "defined": false
                                                       |   }
                                                       | },
                                                       | "sessionBindingMode": false
                                                       |}""".stripMargin) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          errors.size should be(2)
          errors.head.field shouldBe "consultationImages.fr"
          errors(1).field shouldBe "descriptionImages.fr"
        }
      }
    }

    Scenario("update with invalid timeline") {

      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Put("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(ContentTypes.`application/json`, """{
            | "defaultLanguage": "fr",
            | "languages": ["fr"],
            | "startDate": "2018-12-01T10:15:30.000Z",
            | "endDate": "2068-12-01T10:15:30.000Z",
            | "canPropose": true,
            | "questions": {"fr": "question ?"},
            | "operationTitles": {"fr": "title"},
            | "proposalPrefixes": {"fr": "Il faut"},
            | "countries": ["BE", "FR"],
            | "shortTitles": {"fr": "short title"},
            | "sequenceCardsConfiguration": {
            |   "introCard": { "enabled": true },
            |   "pushProposalCard": { "enabled": true },
            |   "finalCard": {
            |     "enabled": true,
            |     "sharingEnabled": true
            |   }
            | },
            | "metas": { "titles": {"fr": "metas"} },
            | "theme": {
            |   "color": "#000000",
            |   "fontColor": "#000000"
            | },
            | "descriptions": {"fr": "description"},
            | "displayResults": false,
            | "consultationImages": {"fr": "https://example.com"},
            | "featured": true,
            | "votesTarget": 100000,
            | "timeline": {
            |   "action": {
            |     "defined": true,
            |     "date": "2168-12-01",
            |     "dateTexts": {"fr": "missing description"}
            |   },
            |   "result": {
            |     "defined": false,
            |     "date": "2168-12-01",
            |     "dateTexts": {"fr": "in a long time"},
            |     "descriptions": {"fr": "results description"}
            |   },
            |   "workshop": {
            |     "defined": true,
            |     "date": "2168-12-01",
            |     "dateTexts": {"fr": "in a long time"},
            |     "descriptions": {"fr": "workshop description"}
            |   }
            | },
            | "sessionBindingMode": false
            |}""".stripMargin) ~> routes ~> check {

          status should be(StatusCodes.BadRequest)
          val errors = entityAs[Seq[ValidationError]]
          errors.size should be(1)
          errors.head.field shouldBe "timeline.action.descriptions"
        }
      }
    }
  }

  Feature("delete operationOfQuestion") {
    Scenario("delete as admin") {
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Delete("/moderation/operations-of-questions/my-question")
          .withHeaders(Authorization(OAuth2BearerToken(token))) ~> routes ~> check {

          status should be(StatusCodes.NoContent)
        }
      }
    }
  }

  Feature("get by operation of question") {
    Scenario("get as moderator") {
      Get("/moderation/operations-of-questions/some-question")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {

        status should be(StatusCodes.OK)
      }
    }
  }

  Feature("list operation of question") {
    Scenario("list as moderator") {
      Get("/moderation/operations-of-questions?openAt=2018-01-01")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {

        status should be(StatusCodes.OK)
      }
    }
  }

  Feature("search operation of question") {
    Scenario("forbidden when not moderator or admin") {
      for (token <- Seq(None, Some(tokenCitizen))) {
        Get("/moderation/operations-of-questions/search")
          .withHeaders(token.map(t => Authorization(OAuth2BearerToken(t))).toList) ~> routes ~> check {
          status should be(token.fold(StatusCodes.Unauthorized)(_ => StatusCodes.Forbidden))
        }
      }
    }
    Scenario("works for moderators") {
      when(
        operationOfQuestionService.search(
          eqTo(
            OperationOfQuestionSearchQuery(
              filters = Some(
                OperationOfQuestionSearchFilters(
                  questionIds = Some(QuestionIdsSearchFilter(Nil)),
                  operationKinds = Some(OperationKindsSearchFilter(OperationKind.values))
                )
              ),
              sort = Some(OperationOfQuestionElasticsearchFieldName.slug)
            )
          )
        )
      ).thenReturn(
        Future.successful(
          OperationOfQuestionSearchResult(
            1,
            Seq(indexedOperationOfQuestion(QuestionId("foo"), OperationId("bar"), slug = "foobar"))
          )
        )
      )
      Get("/moderation/operations-of-questions/search?_sort=slug")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.OK)
        header("x-total-count").map(_.value) should be(Some("1"))
        val results = entityAs[Seq[ModerationOperationOfQuestionSearchResult]]
        results.size should be(1)
        results.head.id.value should be("foo")
        results.head.slug should be("foobar")

      }
    }
    Scenario("works for admins") {
      when(
        operationOfQuestionService.search(
          eqTo(
            OperationOfQuestionSearchQuery(
              filters = Some(
                OperationOfQuestionSearchFilters(operationKinds = Some(OperationKindsSearchFilter(OperationKind.values))
                )
              ),
              sort = Some(OperationOfQuestionElasticsearchFieldName.slug),
              limit = Some(Pagination.Limit(7)),
              offset = Some(Pagination.Offset(14))
            )
          )
        )
      ).thenReturn(
        Future.successful(
          OperationOfQuestionSearchResult(
            1,
            Seq(indexedOperationOfQuestion(QuestionId("baz"), OperationId("buz"), slug = "bazbuz"))
          )
        )
      )

      // TODO questions ACL assume that superadmins have admin role too, we should abstract these ACL better
      // in the meantime, grant admin role to superadmin for this test
      val superAdminToken = Await.result(oauth2DataHandler.findAccessToken(tokenSuperAdmin), 3.seconds).get
      when(oauth2DataHandler.findAuthInfoByAccessToken(eqTo(superAdminToken))).thenReturn(
        Future.successful(
          Some(
            AuthInfo(
              UserRights(
                userId = defaultSuperAdminUser.userId,
                roles = defaultSuperAdminUser.roles :+ Role.RoleAdmin,
                availableQuestions = defaultSuperAdminUser.availableQuestions,
                emailVerified = defaultSuperAdminUser.emailVerified
              ),
              None,
              None,
              None
            )
          )
        )
      )

      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Get("/moderation/operations-of-questions/search?_sort=slug&_end=21&_start=14")
          .withHeaders(Authorization(OAuth2BearerToken(token))) ~> routes ~> check {
          status should be(StatusCodes.OK)
          header("x-total-count").map(_.value) should be(Some("1"))
          val results = entityAs[Seq[ModerationOperationOfQuestionSearchResult]]
          results.size should be(1)
          results.head.id.value should be("baz")
          results.head.slug should be("bazbuz")
        }
      }
    }
  }

  Feature("ooq infos") {
    Scenario("forbidden when not moderator or admin") {
      for (token <- Seq(None, Some(tokenCitizen))) {
        Get("/moderation/operations-of-questions/infos?moderationMode=Enrichment")
          .withHeaders(token.map(t => Authorization(OAuth2BearerToken(t))).toList) ~> routes ~> check {
          status should be(token.fold(StatusCodes.Unauthorized)(_ => StatusCodes.Forbidden))
        }
      }
    }

    Scenario("works for moderators") {
      when(
        operationOfQuestionService.getQuestionsInfos(
          questionIds = eqTo(Some(defaultModeratorUser.availableQuestions)),
          moderationMode = eqTo(ModerationMode.Moderation),
          minVotesCount = eqTo(None),
          minScore = eqTo(None)
        )
      ).thenReturn(Future.successful(Seq.empty))
      Get("/moderation/operations-of-questions/infos?moderationMode=Moderation")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.OK)
        header("x-total-count").map(_.value) should be(Some("0"))
        val results = entityAs[Seq[ModerationOperationOfQuestionSearchResult]]
        results shouldBe empty
      }
    }

    Scenario("works for admins") {
      when(
        operationOfQuestionService
          .getQuestionsInfos(
            questionIds = eqTo(None),
            moderationMode = eqTo(ModerationMode.Enrichment),
            minVotesCount = eqTo(None),
            minScore = eqTo(None)
          )
      ).thenReturn(
        Future.successful(
          Seq(
            ModerationOperationOfQuestionInfosResponse(
              indexedOperationOfQuestion(QuestionId("q-id-infos"), OperationId("o-id-infos")),
              42,
              420,
              true
            )
          )
        )
      )

      Get("/moderation/operations-of-questions/infos?moderationMode=Enrichment")
        .withHeaders(Authorization(OAuth2BearerToken(tokenAdmin))) ~> routes ~> check {
        status should be(StatusCodes.OK)
        header("x-total-count").map(_.value) should be(Some("1"))
        val results = entityAs[Seq[ModerationOperationOfQuestionInfosResponse]]
        results.size should be(1)
        results.head.questionId shouldBe QuestionId("q-id-infos")
        results.head.proposalToModerateCount shouldBe 42
      }
    }
  }
}
