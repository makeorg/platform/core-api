/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.question
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Authorization, OAuth2BearerToken}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.RouteTestTimeout
import akka.util.ByteString
import cats.data.NonEmptyList
import org.make.api.{MakeApiTestBase, TestUtils}
import org.make.api.operation.{
  OperationOfQuestionService,
  OperationOfQuestionServiceComponent,
  OperationService,
  OperationServiceComponent
}
import org.make.api.proposal._
import org.make.api.technical.storage.Content.FileContent
import org.make.api.technical.storage.{FileType, StorageService, StorageServiceComponent, UploadResponse}
import org.make.api.user.{UserService, UserServiceComponent}
import org.make.core.operation._
import org.make.core.proposal.ProposalStatus.Accepted
import org.make.core.proposal.indexed._
import org.make.core.proposal.{ProposalId, _}
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import org.make.core.tag.TagId
import org.make.core.technical.Multilingual
import org.make.core.user.UserId
import org.make.core.{DateHelper, RequestContext, RequestContextLanguage, ValidationError}

import scala.concurrent.Future
import scala.concurrent.duration._

class ModerationQuestionApiTest
    extends MakeApiTestBase
    with DefaultModerationQuestionComponent
    with ProposalServiceComponent
    with ProposalSearchEngineComponent
    with QuestionServiceComponent
    with StorageServiceComponent
    with OperationOfQuestionServiceComponent
    with OperationServiceComponent
    with UserServiceComponent {

  override val questionService: QuestionService = mock[QuestionService]

  val routes: Route = sealRoute(moderationQuestionApi.routes)

  override lazy val proposalService: ProposalService = mock[ProposalService]
  override lazy val elasticsearchProposalAPI: ProposalSearchEngine = mock[ProposalSearchEngine]
  override lazy val storageService: StorageService = mock[StorageService]
  override lazy val operationService: OperationService = mock[OperationService]
  override lazy val operationOfQuestionService: OperationOfQuestionService = mock[OperationOfQuestionService]
  override lazy val userService: UserService = mock[UserService]

  val baseSimpleOperation: SimpleOperation =
    TestUtils.simpleOperation(id = OperationId("operation-id"))
  val baseQuestion: Question =
    TestUtils.question(
      id = QuestionId("question-id"),
      operationId = Some(OperationId("operation-id")),
      languages = NonEmptyList.of(Language("fr"), Language("en"))
    )
  val baseOperationOfQuestion: OperationOfQuestion =
    TestUtils.operationOfQuestion(QuestionId("question-id"), OperationId("operation-id"))

  Feature("list questions") {

    when(questionService.countQuestion(any[SearchQuestionRequest])).thenReturn(Future.successful(42))
    when(questionService.searchQuestion(any[SearchQuestionRequest])).thenReturn(Future.successful(Seq(baseQuestion)))

    val uri = "/moderation/questions?start=0&end=1&operationId=foo&country=FR&language=fr"

    Scenario("authenticated list questions") {
      Get(uri).withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.OK)
        header("x-total-count").isDefined shouldBe true
        val questions: Seq[ModerationQuestionResponse] = entityAs[Seq[ModerationQuestionResponse]]
        questions.size should be(1)
        questions.head.id.value should be(baseQuestion.questionId.value)
      }

    }
    Scenario("unauthorized list questions") {
      Get(uri) ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }
    }
    Scenario("forbidden list questions") {
      Get(uri).withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }
  }

  Feature("get question") {
    def uri(id: String = "question-id"): String = s"/moderation/questions/$id"

    when(questionService.getQuestion(any[QuestionId]))
      .thenReturn(Future.successful(None))

    when(questionService.getQuestion(QuestionId("question-id")))
      .thenReturn(Future.successful(Some(baseQuestion)))

    Scenario("authenticated get question") {
      Get(uri()).withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.OK)
      }
    }
    Scenario("unauthorized get question") {
      Get(uri()) ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }
    }
    Scenario("forbidden get question") {
      Get(uri()).withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }
    Scenario("not found get question") {
      Get(uri("not-found"))
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.NotFound)
      }
    }
  }

  Feature("refuse initial proposals") {
    val uri: String = "/moderation/questions/question-id/initial-proposals/refuse"

    Scenario("unauthorized refuse proposals") {
      Post(uri) ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }
    }

    Scenario("forbidden refuse proposal (citizen)") {
      Post(uri)
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("forbidden create question (moderator)") {
      Post(uri)
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("authenticated refuse initial proposals") {
      when(proposalService.searchInIndex(any[SearchQuery], any[RequestContext])).thenReturn(
        Future.successful(
          ProposalsSearchResult(
            total = 1,
            results = Seq(
              TestUtils.indexedProposal(
                id = ProposalId("aaa-bbb-ccc"),
                userId = UserId("foo-bar"),
                content = "il faut fou",
                createdAt = DateHelper.now(),
                updatedAt = None,
                toEnrich = true,
                initialProposal = true
              )
            )
          )
        )
      )

      when(
        proposalService
          .refuseProposal(eqTo(ProposalId("aaa-bbb-ccc")), any[UserId], any[RequestContext], any[RefuseProposalRequest])
      ).thenReturn(
        Future.successful(
          Some(
            ModerationProposalResponse(
              id = ProposalId("aaa-bbb-ccc"),
              proposalId = ProposalId("aaa-bbb-ccc"),
              content = "il faut fou",
              contentTranslations = None,
              submittedAsLanguage = None,
              slug = "il-faut-fou",
              author = ModerationProposalAuthorResponse(
                UserId("Georges RR Martin"),
                firstName = Some("Georges"),
                lastName = Some("Martin"),
                displayName = Some("Georges Martin"),
                organisationName = None,
                postalCode = None,
                age = None,
                avatarUrl = None,
                organisationSlug = None,
                reachable = false
              ),
              status = Accepted,
              tags = Seq(),
              votes = Seq(
                ModerationVoteResponse(voteKey = VoteKey.Agree, 0, Seq.empty),
                ModerationVoteResponse(voteKey = VoteKey.Disagree, 0, Seq.empty),
                ModerationVoteResponse(voteKey = VoteKey.Neutral, 0, Seq.empty)
              ),
              context = RequestContext.empty.copy(
                country = Some(Country("FR")),
                languageContext = RequestContextLanguage(language = Some(Language("fr")))
              ),
              proposalType = ProposalType.ProposalTypeSubmitted,
              createdAt = Some(DateHelper.now()),
              updatedAt = Some(DateHelper.now()),
              events = Nil,
              idea = None,
              ideaProposals = Seq.empty,
              operationId = None,
              questionId = Some(QuestionId("my-question")),
              keywords = Nil,
              zone = Zone.Controversy
            )
          )
        )
      )

      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Post(uri)
          .withHeaders(Authorization(OAuth2BearerToken(token))) ~> routes ~> check {
          status should be(StatusCodes.NoContent)
        }
      }
    }
  }

  Feature("create initial proposals") {
    val uri = "/moderation/questions/question-id/initial-proposals"
    val request =
      """
      |{
      | "content": "Il faut test",
      | "contentTranslations": {"en":"Need to test"},
      | "country": "FR",
      | "submittedAsLanguage": "fr",
      | "author":
      | {
      |  "age": "42",
      |  "firstName": "name"
      | },
      | "tags": []
      |}
    """.stripMargin

    val requestWithoutCountry =
      """
      |{
      | "content": "Il faut test",
      | "author":
      | {
      |  "age": "42",
      |  "firstName": "name"
      | },
      | "tags": []
      |}
    """.stripMargin

    val requestWithBadCountry =
      """
      |{
      | "content": "Il faut test",
      | "country": "ES",
      | "author":
      | {
      |  "age": "42",
      |  "firstName": "name"
      | },
      | "tags": []
      |}
    """.stripMargin

    val badRequest1 =
      """
        |{
        | "content": "Il faut test",
        | "author":
        | {
        |  "age": "42",
        |  "lastName": "name"
        | },
        | "tags": []
        |}
      """.stripMargin

    val badRequest2 =
      """
        |{
        | "content": "Il faut test",
        | "author":
        | {
        |  "age": "42",
        |  "firstName": "",
        |  "lastName": "name"
        | },
        | "tags": []
        |}
      """.stripMargin

    val badRequest3 =
      """
      |{
      | "content": "",
      | "author":
      | {
      |  "age": "42",
      |  "firstName": "name"
      | },
      | "tags": []
      |}
    """.stripMargin

    val badRequest4 =
      """
      |{
      | "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent arcu ligula, placerat pellentesque vestibulum vitae, tincidunt vel quam. Etiam mattis sed odio et interdum. Cras at velit vitae diam volutpat vulputate. Nunc semper felis nec justo rutrum accumsan. Mauris eget leo sit amet lorem ullamcorper lacinia quis sit amet orci. Pellentesque porta, sapien vitae porta malesuada, velit risus dapibus orci, non porttitor eros tortor in odio. Aliquam ut pulvinar lacus.",
      | "author":
      | {
      |  "age": "42",
      |  "firstName": "name"
      | },
      | "tags": []
      |}
    """.stripMargin

    val badRequest5 =
      """
      |{
      | "content": "<script>alert('0wn€d');</script>",
      | "author":
      | {
      |  "age": "42",
      |  "firstName": "name"
      | },
      | "tags": []
      |}
    """.stripMargin

    val badRequest6 =
      """
        |{
        | "content": "Il faut test",
        | "contentTranslations": {"en":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent arcu ligula, placerat pellentesque vestibulum vitae, tincidunt vel quam. Etiam mattis sed odio et interdum. Cras at velit vitae diam volutpat vulputate. Nunc semper felis nec justo rutrum accumsan. Mauris eget leo sit amet lorem ullamcorper lacinia quis sit amet orci. Pellentesque porta, sapien vitae porta malesuada, velit risus dapibus orci, non porttitor eros tortor in odio. Aliquam ut pulvinar lacus."},
        | "country": "FR",
        | "submittedAsLanguage": "fr",
        | "author":
        | {
        |  "age": "42",
        |  "firstName": "name"
        | },
        | "tags": []
        |}
    """.stripMargin

    when(questionService.getQuestion(QuestionId("question-id")))
      .thenReturn(Future.successful(Some(baseQuestion)))

    when(
      proposalService.createInitialProposal(
        any[String],
        any[Option[Multilingual[String]]],
        any[Question],
        any[Country],
        any[Language],
        any[Boolean],
        any[Seq[TagId]],
        any[AuthorRequest],
        any[UserId],
        any[RequestContext]
      )
    ).thenReturn(Future.successful(ProposalId("proposal-id")))

    when(
      proposalService.createExternalProposal(
        any[String],
        any[Option[Multilingual[String]]],
        any[Question],
        any[Country],
        any[Language],
        any[Boolean],
        any[String],
        any[AuthorRequest],
        any[UserId],
        any[RequestContext],
        any[Int]
      )
    ).thenReturn(Future.successful(ProposalId("proposal-id")))

    when(elasticsearchProposalAPI.countProposals(any[SearchQuery])).thenReturn(Future.successful(0))

    Scenario("authenticated create proposal") {
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Post(uri)
          .withHeaders(Authorization(OAuth2BearerToken(token)))
          .withEntity(HttpEntity(ContentTypes.`application/json`, request)) ~> routes ~> check {
          status should be(StatusCodes.Created)
          val proposalIdResponse: ProposalIdResponse = entityAs[ProposalIdResponse]
          proposalIdResponse.id.value should be("proposal-id")
          proposalIdResponse.proposalId.value should be("proposal-id")
        }
      }
    }
    Scenario("unauthorized create proposal") {
      Post(uri)
        .withEntity(HttpEntity(ContentTypes.`application/json`, request)) ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }
    }
    Scenario("forbidden create proposal (citizen)") {
      Post(uri)
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen)))
        .withEntity(HttpEntity(ContentTypes.`application/json`, request)) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }
    Scenario("forbidden create proposal (moderator)") {
      Post(uri)
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator)))
        .withEntity(HttpEntity(ContentTypes.`application/json`, request)) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }
    for ((badRequest, testName) <- List(
           badRequest1 -> "firstName None",
           badRequest2 -> "firstName empty string",
           requestWithoutCountry -> "no country",
           requestWithBadCountry -> "country not in question",
           badRequest3 -> "malformed content (too short)",
           badRequest4 -> "malformed content (too long)",
           badRequest5 -> "malformed content (html entities)",
           badRequest6 -> "malformed translations (too long)"
         ))
      Scenario(s"bad request create proposal: $testName") {
        for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
          Post(uri)
            .withHeaders(Authorization(OAuth2BearerToken(token)))
            .withEntity(HttpEntity(ContentTypes.`application/json`, badRequest)) ~> routes ~> check {
            status should be(StatusCodes.BadRequest)
          }
        }
      }

    Scenario("missing languages") {
      val missing =
        """
          |{
          | "content": "Il faut test",
          | "country": "FR",
          | "submittedAsLanguage": "fr",
          | "author":
          | {
          |  "age": "42",
          |  "firstName": "name"
          | },
          | "tags": []
          |}
    """.stripMargin

      Post(uri)
        .withHeaders(Authorization(OAuth2BearerToken(tokenAdmin)))
        .withEntity(HttpEntity(ContentTypes.`application/json`, missing)) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
        val error = entityAs[Seq[ValidationError]]
        error.head.message.get should be("Field contents is missing translations for en")
      }
    }

    Scenario("too many languages") {
      val missing =
        """
          |{
          | "content": "Il faut test",
          | "contentTranslations": {
          |   "en": "Need to test",
          |   "de": "Need to test in DE"
          | },
          | "country": "FR",
          | "submittedAsLanguage": "fr",
          | "author":
          | {
          |  "age": "42",
          |  "firstName": "name"
          | },
          | "tags": []
          |}
    """.stripMargin

      Post(uri)
        .withHeaders(Authorization(OAuth2BearerToken(tokenAdmin)))
        .withEntity(HttpEntity(ContentTypes.`application/json`, missing)) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
        val error = entityAs[Seq[ValidationError]]
        error.head.message.get should be("Field contents has too many translations : de")
      }
    }

    Scenario("mismatch languages") {
      val missing =
        """
          |{
          | "content": "Il faut test",
          | "contentTranslations": {
          |   "de": "Need to test in DE"
          | },
          | "country": "FR",
          | "submittedAsLanguage": "fr",
          | "author":
          | {
          |  "age": "42",
          |  "firstName": "name"
          | },
          | "tags": []
          |}
    """.stripMargin

      Post(uri)
        .withHeaders(Authorization(OAuth2BearerToken(tokenAdmin)))
        .withEntity(HttpEntity(ContentTypes.`application/json`, missing)) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
        val error = entityAs[Seq[ValidationError]]
        error.head.message.get should be("Field contents has mismatch translations : de")
      }
    }
  }

  Feature("Inject proposals") {
    when(userService.getUserByEmail(any[String])).thenReturn(Future.successful(None))

    val uri = "/moderation/questions/question-id/inject-proposals"
    val request: Multipart = Multipart.FormData(fields = Map(
      "proposals" -> HttpEntity
        .Strict(ContentTypes.`text/csv(UTF-8)`, ByteString("some csv file")),
      "body" ->
        """{
          |  "anonymous": false
          |}""".stripMargin
    )
    )

    Scenario("authenticated inject proposals") {
      Post(uri, request) ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }
    }

    Scenario("forbidden citizen") {
      Post(uri, request)
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("forbidden moderator") {
      Post(uri, request)
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("incorrect file type") {
      val request: Multipart =
        Multipart.FormData(
          Multipart.FormData.BodyPart
            .Strict(
              "proposals",
              HttpEntity(ContentType(MediaTypes.`image/jpeg`), ByteString("image")),
              Map("filename" -> "image.jpeg")
            ),
          Multipart.FormData.BodyPart
            .Strict("body", HttpEntity(ContentTypes.`application/json`, """{
                |  "anonymous": false
                |}""".stripMargin))
        )

      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Post(uri, request)
          .withHeaders(Authorization(OAuth2BearerToken(token))) ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }
    }

    val csvErrorScenarios = Seq(
      ("content too short", """content,contentOriginalLanguage,firstName,age
          |Il faut,fr,Charley,42
          |""".stripMargin, "content should be more than 12 characters on line 2"),
      ("missing translations",
        """content,contentOriginalLanguage,firstName,age
          |Il faut faire quelque chose,fr,Charley,42
          |""".stripMargin, "There is a missing translations for en on line 2"),
      ("too many translations",
        """content,contentOriginalLanguage,firstName,age,en,de
          |Il faut faire quelque chose,fr,Charley,42,We should test,Es ist notwendig zu testen
          |""".stripMargin, "There is too many translations : de on line 2"),
      ("mismatch translations",
        """content,contentOriginalLanguage,firstName,age,de
          |Il faut faire quelque chose,fr,Charley,42,Es ist notwendig zu testen
          |""".stripMargin, "There is a translations mismatch : de on line 2"),
      ("missing required field", """content,contentOriginalLanguage,en,age
          |Il faut faire quelque chose,fr,We should do something,42
          |""".stripMargin, "firstName is required on line 2"),
      ("invalid age", """content,contentOriginalLanguage,en,firstName,age
          |Il faut faire quelque chose,fr,We should do something,Charley,4
          |""".stripMargin, "age should be over than 18 on line 2")
    )

    for (scenario <- csvErrorScenarios) {
      Scenario(s"CSV validation error - ${scenario._1}") {
        val request: Multipart =
          Multipart.FormData(
            Multipart.FormData.BodyPart
              .Strict(
                "proposals",
                HttpEntity(ContentTypes.`text/csv(UTF-8)`, scenario._2),
                Map("filename" -> "test.csv")
              ),
            Multipart.FormData.BodyPart
              .Strict("body", HttpEntity(ContentTypes.`application/json`, """{
                                                                            |  "anonymous": false
                                                                            |}""".stripMargin))
          )

        Post(uri, request)
          .withHeaders(Authorization(OAuth2BearerToken(tokenAdmin))) ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
          val res = entityAs[Seq[ValidationError]]
          res.head.message should be(Some(scenario._3))
        }
      }
    }

    Scenario("Warning") {

      val csv = """content,contentOriginalLanguage,firstName,age,externalUserId,fr,en
                  |Il faut test,fr,Charley,42,1,Il faut test encore,We should test again
                  |""".stripMargin
      val request: Multipart =
        Multipart.FormData(
          Multipart.FormData.BodyPart
            .Strict(
              "proposals",
              HttpEntity(ContentTypes.`text/csv(UTF-8)`, csv),
              Map("filename" -> "test.csv")
            ),
          Multipart.FormData.BodyPart
            .Strict("body", HttpEntity(ContentTypes.`application/json`, """{
                                                                          |  "anonymous": false
                                                                          |}""".stripMargin))
        )

      Post(uri, request)
        .withHeaders(Authorization(OAuth2BearerToken(tokenAdmin))) ~> routes ~> check {
        status should be(StatusCodes.Accepted)
        val res = entityAs[List[String]]
        res should be(List("Warning: duplicate language fr on line 2, proposal from the content column was chosen"))
      }
    }
  }

  Feature("upload image") {
    implicit val timeout: RouteTestTimeout = RouteTestTimeout(300.seconds)
    def uri(id: String = "question-id") = s"/moderation/questions/$id/image"

    when(questionService.getQuestion(QuestionId("question-id-no-operation")))
      .thenReturn(Future.successful(Some(baseQuestion.copy(operationId = None))))
    when(operationOfQuestionService.findByQuestionId(QuestionId("fake-question")))
      .thenReturn(Future.successful(None))
    when(operationOfQuestionService.findByQuestionId(QuestionId("question-id")))
      .thenReturn(Future.successful(Some(baseOperationOfQuestion)))
    when(operationService.findOneSimple(OperationId("operation-id")))
      .thenReturn(Future.successful(Some(baseSimpleOperation)))

    Scenario("unauthorized not connected") {
      Post(uri()) ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }
    }

    Scenario("forbidden citizen") {
      Post(uri())
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("forbidden moderator") {
      Post(uri())
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("question not found") {
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Post(uri("fake-question"))
          .withHeaders(Authorization(OAuth2BearerToken(token))) ~> routes ~> check {
          status should be(StatusCodes.NotFound)
        }
      }
    }

    Scenario("incorrect file type") {
      val request: Multipart = Multipart.FormData(fields = Map(
        "data" -> HttpEntity
          .Strict(ContentTypes.`application/x-www-form-urlencoded`, ByteString("incorrect file type"))
      )
      )

      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Post(uri(), request)
          .withHeaders(Authorization(OAuth2BearerToken(token))) ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }
    }

    Scenario("storage unavailable") {
      when(storageService.uploadImage(eqTo(FileType.Operation), any[String], any[String], any[FileContent]))
        .thenReturn(Future.failed(new Exception("swift client error")))
      val request: Multipart =
        Multipart.FormData(
          Multipart.FormData.BodyPart
            .Strict(
              "data",
              HttpEntity.Strict(ContentType(MediaTypes.`image/jpeg`), ByteString("image")),
              Map("filename" -> "image.jpeg")
            )
        )

      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Post(uri(), request)
          .withHeaders(Authorization(OAuth2BearerToken(token))) ~> routes ~> check {
          status should be(StatusCodes.InternalServerError)
        }
      }
    }

    Scenario("large file successfully uploaded and returned by admin") {
      when(storageService.uploadImage(eqTo(FileType.Operation), any[String], any[String], any[FileContent]))
        .thenReturn(Future.successful("path/to/uploaded/image.jpeg"))

      def entityOfSize(size: Int): Multipart = Multipart.FormData(
        Multipart.FormData.BodyPart
          .Strict(
            "data",
            HttpEntity.Strict(ContentType(MediaTypes.`image/jpeg`), ByteString("0" * size)),
            Map("filename" -> "image.jpeg")
          )
      )
      for (token <- Seq(tokenAdmin, tokenSuperAdmin)) {
        Post(uri(), entityOfSize(256000 + 1))
          .withHeaders(Authorization(OAuth2BearerToken(token))) ~> routes ~> check {
          status should be(StatusCodes.OK)

          val path: UploadResponse = entityAs[UploadResponse]
          path.path shouldBe "path/to/uploaded/image.jpeg"
        }
      }
    }
  }
}
