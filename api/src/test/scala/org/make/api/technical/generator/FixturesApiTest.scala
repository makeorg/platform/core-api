/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.technical.generator

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.Route
import cats.data.{NonEmptyList => Nel}
import org.make.api.MakeApiTestBase
import org.make.api.operation.{OperationService, OperationServiceComponent}
import org.make.api.question.{QuestionService, QuestionServiceComponent}
import org.make.api.technical.generator.fixtures.{
  DefaultFixturesApiComponent,
  FixtureResponse,
  FixturesService,
  FixturesServiceComponent
}
import org.make.core.operation.OperationId
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, Language}

import scala.concurrent.Future

class FixturesApiTest
    extends MakeApiTestBase
    with DefaultFixturesApiComponent
    with FixturesServiceComponent
    with OperationServiceComponent
    with QuestionServiceComponent {

  override val fixturesService: FixturesService = mock[FixturesService]
  override val operationService: OperationService = mock[OperationService]
  override val questionService: QuestionService = mock[QuestionService]

  val routes: Route = sealRoute(fixturesApi.routes)

  Feature("generate") {
    val opId = OperationId("op-id")
    val qId = QuestionId("q-id")
    when(operationService.findOne(eqTo(opId))).thenReturn(Future.successful(Some(operation(opId))))
    when(operationService.findOne(eqTo(OperationId("invalid")))).thenReturn(Future.successful(None))
    when(questionService.getQuestion(eqTo(qId))).thenReturn(Future.successful(Some(question(qId))))
    when(questionService.getQuestion(eqTo(QuestionId("invalid")))).thenReturn(Future.successful(None))

    when(
      fixturesService.generate(
        any[Option[OperationId]],
        any[Option[QuestionId]],
        any[Option[Int]],
        any[Option[Nel[Country]]],
        any[Option[Language]]
      )
    ).thenReturn(Future.successful(FixtureResponse(opId, qId, 0, 0, 0, 0, 0, 0)))

    Scenario("invalid operation") {
      Post("/fixtures/generate").withEntity(
        HttpEntity(
          ContentTypes.`application/json`,
          """{"operationId": "invalid", "questionId": "q-id", "forceCountries": ["FR", "GB"], "forceLanguage": "fr"}"""
        )
      ) ~>
        routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    Scenario("invalid question") {
      Post("/fixtures/generate").withEntity(
        HttpEntity(
          ContentTypes.`application/json`,
          """{"operationId": "op-id", "questionId": "invalid", "forceCountries": ["FR", "GB"], "forceLanguage": "fr"}"""
        )
      ) ~>
        routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    Scenario("empty Nel") {
      Post("/fixtures/generate").withEntity(
        HttpEntity(
          ContentTypes.`application/json`,
          """{"operationId": "op-id", "questionId": "q-id", "forceCountries": [], "forceLanguage": "fr"}"""
        )
      ) ~>
        routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    Scenario("successful") {
      Post("/fixtures/generate").withEntity(
        HttpEntity(
          ContentTypes.`application/json`,
          """{"operationId": "op-id", "questionId": "q-id", "forceCountries": ["FR", "GB"], "forceLanguage": "fr"}"""
        )
      ) ~>
        routes ~> check {
        status should be(StatusCodes.Created)
      }
    }
  }
}
