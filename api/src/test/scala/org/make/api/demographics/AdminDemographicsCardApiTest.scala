/*
 *  Make.org Core API
 *  Copyright (C) 2021 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.demographics

import cats.data.NonEmptyList
import akka.http.scaladsl.model.headers.{Authorization, OAuth2BearerToken}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.Route
import org.make.api.MakeApiTestBase
import org.make.core.ValidationError
import org.make.core.technical.Multilingual
import org.make.core.demographics.DemographicsCard.Layout
import org.make.core.demographics.DemographicsCardId
import org.make.core.reference.Language

import scala.concurrent.Future
import org.make.core.demographics.LabelsValue

class AdminDemographicsCardApiTest
    extends MakeApiTestBase
    with DefaultAdminDemographicsCardApiComponent
    with DemographicsCardServiceComponent
    with ActiveDemographicsCardServiceComponent {

  override val demographicsCardService: DemographicsCardService = mock[DemographicsCardService]
  override val activeDemographicsCardService: ActiveDemographicsCardService = mock[ActiveDemographicsCardService]

  val routes: Route = sealRoute(adminDemographicsCardApi.routes)

  Feature("list demographicsCards") {

    val card = demographicsCard(DemographicsCardId("id-age-fr"))
    when(demographicsCardService.list(any, any, any, any, eqTo(Some(List(Language("fr")))), eqTo(Some("age")), any))
      .thenReturn(Future.successful(Seq(card)))

    when(demographicsCardService.get(eqTo(DemographicsCardId("id-update"))))
      .thenReturn(Future.successful(Some(card)))

    when(activeDemographicsCardService.count(eqTo(None), eqTo(Some(DemographicsCardId("id-update")))))
      .thenReturn(Future.successful(1))

    when(demographicsCardService.count(eqTo(Some(List(Language("fr")))), eqTo(Some("age")), any))
      .thenReturn(Future.successful(1))

    Scenario("unauthorize unauthenticated") {
      Get("/admin/demographics-cards") ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }
    }

    Scenario("forbid authenticated citizen") {
      Get("/admin/demographics-cards").withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("forbid authenticated moderator") {
      Get("/admin/demographics-cards").withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("allow authenticated admin") {
      for (token <- Seq(tokenAdmin, tokenOperator, tokenSuperAdmin)) {
        Get("/admin/demographics-cards?languages=fr&dataType=age")
          .withHeaders(Authorization(OAuth2BearerToken(token))) ~> routes ~> check {
          status should be(StatusCodes.OK)
          val demographicsCards = entityAs[Seq[AdminDemographicsCardResponse]]
          demographicsCards should be(Seq(AdminDemographicsCardResponse(card)))
        }
      }
    }
  }

  Feature("get demographicsCard by id") {

    val card = demographicsCard(DemographicsCardId("demo-id"))

    when(demographicsCardService.get(card.id)).thenReturn(Future.successful(Some(card)))
    when(demographicsCardService.get(DemographicsCardId("fake"))).thenReturn(Future.successful(None))

    Scenario("unauthorize unauthenticated") {
      Get("/admin/demographics-cards/demo-id") ~> routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }
    }

    Scenario("forbid authenticated citizen") {
      Get("/admin/demographics-cards/demo-id").withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("forbid authenticated moderator") {
      Get("/admin/demographics-cards/demo-id").withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("allow authenticated admin on existing demographicsCard") {
      for (token <- Seq(tokenAdmin, tokenOperator, tokenSuperAdmin)) {
        Get("/admin/demographics-cards/demo-id")
          .withHeaders(Authorization(OAuth2BearerToken(token))) ~> routes ~> check {
          status should be(StatusCodes.OK)
          val demographicsCard = entityAs[AdminDemographicsCardResponse]
          demographicsCard should be(AdminDemographicsCardResponse(card))
        }
      }
    }

    Scenario("not found and allow authenticated admin on a non existing demographicsCard") {
      for (token <- Seq(tokenAdmin, tokenOperator, tokenSuperAdmin)) {
        Get("/admin/demographics-cards/fake")
          .withHeaders(Authorization(OAuth2BearerToken(token))) ~> routes ~> check {
          status should be(StatusCodes.NotFound)
        }
      }
    }

  }

  Feature("create a demographicsCard") {
    val created = demographicsCard(
      id = DemographicsCardId("id-create"),
      name = "Demo name create",
      layout = Layout.ThreeColumnsRadio,
      dataType = "create",
      languages = NonEmptyList.of(Language("fr")),
      titles = Multilingual.fromDefault("Demo title create"),
      parameters = NonEmptyList.of(LabelsValue(Multilingual.fromDefault("Option create"), "value-create"))
    )

    when(
      demographicsCardService.create(
        eqTo("Demo name create"),
        eqTo(Layout.ThreeColumnsRadio),
        eqTo("create"),
        eqTo(created.languages),
        eqTo(created.titles),
        eqTo(created.sessionBindingMode),
        eqTo(created.parameters)
      )
    ).thenReturn(Future.successful(created))

    Scenario("unauthorize unauthenticated") {
      Post("/admin/demographics-cards") ~>
        routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }
    }

    Scenario("forbid authenticated citizen") {
      Post("/admin/demographics-cards")
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("forbid authenticated moderator") {
      Post("/admin/demographics-cards")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("forbid authenticated admin") {
      Post("/admin/demographics-cards")
        .withHeaders(Authorization(OAuth2BearerToken(tokenAdmin))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("allow authenticated operator") {
      Post("/admin/demographics-cards")
        .withEntity(
          HttpEntity(
            ContentTypes.`application/json`,
            """{
            |  "name": "Demo name create",
            |  "layout": "ThreeColumnsRadio",
            |  "dataType": "create",
            |  "languages": ["fr"],
            |  "titles": {"fr": "Demo title create"},
            |  "sessionBindingMode": false,
            |  "parameters": [{"label":{"fr": "Option create"}, "value":"value-create"}]
            |}""".stripMargin
          )
        )
        .withHeaders(Authorization(OAuth2BearerToken(tokenOperator))) ~> routes ~> check {
        status should be(StatusCodes.Created)
      }
    }

    Scenario("allow authenticated super admin") {
      Post("/admin/demographics-cards")
        .withEntity(
          HttpEntity(
            ContentTypes.`application/json`,
            """{
            |  "name": "Demo name create",
            |  "layout": "ThreeColumnsRadio",
            |  "dataType": "create",
            |  "languages": ["fr"],
            |  "titles": {"fr":"Demo title create"},
            |  "sessionBindingMode": false,
            |  "parameters": [{"label":{"fr": "Option create"}, "value":"value-create"}]
            |}""".stripMargin
          )
        )
        .withHeaders(Authorization(OAuth2BearerToken(tokenSuperAdmin))) ~> routes ~> check {
        status should be(StatusCodes.Created)
      }
    }

    Scenario("bad requests") {
      Post("/admin/demographics-cards")
        .withEntity(
          HttpEntity(
            ContentTypes.`application/json`,
            """{
            |  "name": "Demo name create above 256 characters. 1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890",
            |  "layout": "OneColumnRadio",
            |  "dataType": "not a slug",
            |  "languages": ["fr"],
            |  "titles": {"fr":"Demo title create above 64 characters. 123456789012345678901234567890"},
            |  "sessionBindingMode": false,
            |  "parameters": [
            |    {"label":{"fr": "Option create above 64 characters. 123456789012345678901234567890"}, "value":"value-create"},
            |    {"label":{"fr": "too much"}, "value":"value-create"},
            |    {"label":{"fr": "options"}, "value":"value-create"},
            |    {"label":{"fr": "for this"}, "value":"value-create"},
            |    {"label":{"fr": "type of layout"}, "value":"value-create"}
            |  ]
            |}""".stripMargin
          )
        )
        .withHeaders(Authorization(OAuth2BearerToken(tokenSuperAdmin))) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
        val errors = entityAs[Seq[ValidationError]]
        errors.map(_.field) should be(Seq("name", "dataType", "titles.fr"))
      }

      Post("/admin/demographics-cards")
        .withEntity(
          HttpEntity(
            ContentTypes.`application/json`,
            """{
            |  "name": "Demo name create above 256 characters.",
            |  "layout": "OneColumnRadio",
            |  "dataType": "not-a-slug",
            |  "languages": ["fr"],
            |  "titles": {"fr":"Demo title create above 64 characters."},
            |  "sessionBindingMode": false,
            |  "parameters": [
            |    {"label":{"fr": "Option create above 64 characters. 123456789012345678901234567890"}, "value":"value-create"},
            |    {"label":{"fr": "too much"}, "value":"value-create"},
            |    {"label":{"fr": "options"}, "value":"value-create"},
            |    {"label":{"fr": "for this"}, "value":"value-create"},
            |    {"label":{"fr": "type of layout"}, "value":"value-create"}
            |  ]
            |}""".stripMargin
          )
        )
        .withHeaders(Authorization(OAuth2BearerToken(tokenSuperAdmin))) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
        val errors = entityAs[Seq[ValidationError]]
        errors.map(_.field) should be(Seq("parameters", "parameters"))
      }

      Post("/admin/demographics-cards")
        .withEntity(
          HttpEntity(
            ContentTypes.`application/json`,
            """{
            |  "name": "Demo name",
            |  "layout": "fake",
            |  "dataType": "data-type",
            |  "languages": ["fr"],
            |  "titles": {"fr": "Demo title"},
            |  "sessionBindingMode": false,
            |  "parameters": [{"label":{"fr": "Option create"}, "value":"value-create"}]
            |}""".stripMargin
          )
        )
        .withHeaders(Authorization(OAuth2BearerToken(tokenSuperAdmin))) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
        val errors = entityAs[Seq[ValidationError]]
        errors.size should be(1)
        errors.head.field shouldBe "layout"
      }

    }
  }

  Feature("update a demographicsCard") {
    val updated = demographicsCard(
      id = DemographicsCardId("id-update"),
      name = "Demo name update",
      layout = Layout.ThreeColumnsRadio,
      dataType = "update",
      languages = NonEmptyList.of(Language("fr"), Language("de")),
      titles = Multilingual((Language("fr") -> "Demo title update"), (Language("de") -> "German title")),
      sessionBindingMode = true,
      parameters = NonEmptyList.of(
        LabelsValue(
          Multilingual((Language("fr"), "Option update"), (Language("de"), "Option update de")),
          "value-update"
        )
      )
    )

    when(
      demographicsCardService.update(
        eqTo(DemographicsCardId("id-update")),
        eqTo("Demo name update"),
        eqTo(Layout.ThreeColumnsRadio),
        eqTo("update"),
        eqTo(updated.languages),
        eqTo(updated.titles),
        eqTo(updated.sessionBindingMode),
        eqTo(updated.parameters)
      )
    ).thenReturn(Future.successful(Some(updated)))

    Scenario("unauthorize unauthenticated") {
      Put("/admin/demographics-cards/id-update") ~>
        routes ~> check {
        status should be(StatusCodes.Unauthorized)
      }
    }

    Scenario("forbid authenticated citizen") {
      Put("/admin/demographics-cards/id-update")
        .withHeaders(Authorization(OAuth2BearerToken(tokenCitizen))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("forbid authenticated moderator") {
      Put("/admin/demographics-cards/id-update")
        .withHeaders(Authorization(OAuth2BearerToken(tokenModerator))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("forbid authenticated admin") {
      Put("/admin/demographics-cards/id-update")
        .withHeaders(Authorization(OAuth2BearerToken(tokenAdmin))) ~> routes ~> check {
        status should be(StatusCodes.Forbidden)
      }
    }

    Scenario("allow authenticated operator") {
      Put("/admin/demographics-cards/id-update")
        .withEntity(
          HttpEntity(
            ContentTypes.`application/json`,
            """{
            |  "name": "Demo name update",
            |  "layout": "ThreeColumnsRadio",
            |  "dataType": "update",
            |  "languages": ["fr", "de"],
            |  "titles": {"fr":"Demo title update", "de":"German title"},
            |  "sessionBindingMode": true,
            |  "parameters": [{"label":{"fr": "Option update", "de": "Option update de"}, "value":"value-update"}]
            |}""".stripMargin
          )
        )
        .withHeaders(Authorization(OAuth2BearerToken(tokenOperator))) ~> routes ~> check {
        status should be(StatusCodes.OK)
      }
    }
    Scenario("allow authenticated admin") {
      Put("/admin/demographics-cards/id-update")
        .withEntity(
          HttpEntity(
            ContentTypes.`application/json`,
            """{
            |  "name": "Demo name update",
            |  "layout": "ThreeColumnsRadio",
            |  "dataType": "update",
            |  "languages": ["fr", "de"],
            |  "titles": {"fr":"Demo title update", "de":"German title"},
            |  "sessionBindingMode": true,
            |  "parameters": [{"label":{"fr": "Option update", "de": "Option update de"}, "value":"value-update"}]
            |}""".stripMargin
          )
        )
        .withHeaders(Authorization(OAuth2BearerToken(tokenSuperAdmin))) ~> routes ~> check {
        status should be(StatusCodes.OK)
      }
    }

    Scenario("bad requests") {
      Put("/admin/demographics-cards/id-update")
        .withEntity(
          HttpEntity(
            ContentTypes.`application/json`,
            """{
            |  "name": "Demo name update above 256 characters. 1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890",
            |  "layout": "OneColumnRadio",
            |  "dataType": "not a slug",
            |  "languages": ["fr"],
            |  "titles": {"fr":"Demo title update above 64 characters. 123456789012345678901234567890"},
            |  "sessionBindingMode": true,
            |  "parameters": [
            |    {"label":{"fr": "Option create above 64 characters. 123456789012345678901234567890"}, "value":"value-create"},
            |    {"label":{"fr": "too much"}, "value":"value-create"},
            |    {"label":{"fr": "options"}, "value":"value-create"},
            |    {"label":{"fr": "for this"}, "value":"value-create"},
            |    {"label":{"fr": "type of layout"}, "value":"value-create"}
            |  ]
            |}""".stripMargin
          )
        )
        .withHeaders(Authorization(OAuth2BearerToken(tokenSuperAdmin))) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
        val errors = entityAs[Seq[ValidationError]]
        errors.map(_.field) should be(Seq("name", "dataType", "titles.fr"))
      }

      Put("/admin/demographics-cards/id-update")
        .withEntity(
          HttpEntity(
            ContentTypes.`application/json`,
            """{
            |  "name": "Demo name update above 256 characters.",
            |  "layout": "OneColumnRadio",
            |  "dataType": "not-a-slug",
            |  "languages": ["fr"],
            |  "titles": {"fr":"Demo title update above 64 characters."},
            |  "sessionBindingMode": true,
            |  "parameters": [
            |    {"label":{"fr": "Option create above 64 characters. 123456789012345678901234567890"}, "value":"value-create"},
            |    {"label":{"fr": "too much"}, "value":"value-create"},
            |    {"label":{"fr": "options"}, "value":"value-create"},
            |    {"label":{"fr": "for this"}, "value":"value-create"},
            |    {"label":{"fr": "type of layout"}, "value":"value-create"}
            |  ]
            |}""".stripMargin
          )
        )
        .withHeaders(Authorization(OAuth2BearerToken(tokenSuperAdmin))) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
        val errors = entityAs[Seq[ValidationError]]
        errors.map(_.field) should be(Seq("parameters", "parameters"))
      }

      Put("/admin/demographics-cards/id-update")
        .withEntity(
          HttpEntity(
            ContentTypes.`application/json`,
            """{
        |  "name": "Demo name",
        |  "layout": "fake",
        |  "dataType": "data-type",
        |  "languages": ["fr"],
        |  "titles": {"fr":"Demo title update"},
        |  "sessionBindingMode": true,
        |  "parameters": [{"label":{"fr": "Option update"}, "value":"value-update"}]
        |}""".stripMargin
          )
        )
        .withHeaders(Authorization(OAuth2BearerToken(tokenSuperAdmin))) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
        val errors = entityAs[Seq[ValidationError]]
        errors.size should be(1)
        errors.head.field shouldBe "layout"
      }

      Put("/admin/demographics-cards/id-update")
        .withEntity(HttpEntity(ContentTypes.`application/json`, """{
        |  "name": "Demo name",
        |  "layout": "Select",
        |  "dataType": "data-type",
        |  "languages":  ["fr"],
        |  "titles": {"fr":"Demo title"},
        |  "sessionBindingMode": true,
        |  "parameters": [{"value":"Option update"}]
        |}""".stripMargin))
        .withHeaders(Authorization(OAuth2BearerToken(tokenSuperAdmin))) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
        val errors = entityAs[Seq[ValidationError]]
        errors.size should be(1)
        errors.head.field shouldBe "parameters.label"
      }
    }
  }

}
