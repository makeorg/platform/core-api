package org.make.api.sessionhistory

import org.make.api.ActorTest
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import org.make.api.MakeUnitTest
import org.make.api.technical.DefaultIdGeneratorComponent
import org.make.api.docker.DockerCassandraService
import org.make.api.extensions.MakeSettingsComponent
import org.make.api.technical.ActorSystemComponent
import org.make.api.extensions.MakeSettings
import akka.actor.typed.ActorRef
import org.make.api.userhistory.DefaultUserHistoryCoordinatorServiceComponent
import org.make.api.userhistory.UserHistoryCoordinatorComponent
import org.make.api.userhistory.UserHistoryCommand
import org.make.api.userhistory.UserHistoryCoordinator
import org.make.core.RequestContext
import org.make.core.proposal.VoteKey
import org.make.core.history.HistoryActions
import org.make.api.technical.DefaultReadJournalComponent
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import scala.concurrent.duration.DurationInt
import akka.stream.scaladsl.Source
import akka.stream.scaladsl.Sink
import org.make.core.DateHelper

class SessionHistoryCoordinatorIT
    extends ActorTest(SessionHistoryCoordinatorIT.actorSystem)
    with MakeUnitTest
    with DefaultIdGeneratorComponent
    with DefaultSessionHistoryCoordinatorServiceComponent
    with DefaultUserHistoryCoordinatorServiceComponent
    with DefaultReadJournalComponent
    with DockerCassandraService
    with MakeSettingsComponent
    with ActorSystemComponent
    with UserHistoryCoordinatorComponent
    with SessionHistoryCoordinatorComponent {

  override def afterAll(): Unit = {
    testKit.shutdownTestKit()
    super.afterAll()
  }

  override val cassandraExposedPort: Int = SessionHistoryCoordinatorIT.cassandraExposedPort
  override val makeSettings: MakeSettings = mock[MakeSettings]
  override val userHistoryCoordinator: ActorRef[UserHistoryCommand] = UserHistoryCoordinator(actorSystem)
  override val sessionHistoryCoordinator: ActorRef[SessionHistoryCommand] =
    SessionHistoryCoordinator(actorSystem, userHistoryCoordinator, idGenerator, makeSettings)

  Feature("Snapshots") {
    Scenario("Not too many snapshots are kept") {
      // given
      val trust = HistoryActions.VoteTrust.Trusted
      val requestCtx = RequestContext.empty
      val sessionId = idGenerator.nextSessionId()
      val proposalId = idGenerator.nextProposalId()

      // when
      val fut = Source(0 until 500)
        .mapAsync(5) { i =>
          val voteKey = VoteKey.values(i % VoteKey.values.length)
          val action =
            SessionAction(
              date = DateHelper.now(),
              actionType = "vote",
              arguments = SessionVote(proposalId, voteKey, trust)
            )

          sessionHistoryCoordinatorService.logTransactionalHistory(LogSessionVoteEvent(sessionId, requestCtx, action))
        }
        .runWith(Sink.ignore)
      whenReady(fut, Timeout(2.minutes))(_ => ())

      // then
      val eventCount =
        sessionJournal
          .currentEventsByPersistenceId(sessionId.value, 0, Long.MaxValue)
          .runFold(0)((acc, e) => acc + 1)
      whenReady(eventCount, Timeout(5.seconds))(_ shouldBe 20)
    }
  }
}

object SessionHistoryCoordinatorIT {
  val cassandraExposedPort: Int = 15004
  val port: Int = 15104
  val configuration: String =
    s"""
       |akka {
       |  cluster.seed-nodes = ["akka://SessionHistoryCoordinatorIT@localhost:$port"]
       |  cluster.jmx.multi-mbeans-in-same-jvm = on
       |
       |  persistence {
       |    journal {
       |      plugin = "make-api.event-sourcing.technical.journal"
       |    }
       |    snapshot-store {
       |      plugin = "make-api.event-sourcing.technical.snapshot"
       |    }
       |    role = "worker"
       |  }
       |
       |  remote.artery.canonical {
       |    port = $port
       |    hostname = "localhost"
       |  }
       |
       |  test {
       |    timefactor = 10.0
       |  }
       |}
       |
       |datastax-java-driver.basic {
       |  contact-points = ["127.0.0.1:$cassandraExposedPort"]
       |  load-balancing-policy.local-datacenter = "datacenter1"
       |}
       |
       |make-api {
       |  event-sourcing {
       |    sessions {
       |      events-by-tag.enabled = true
       |    }
       |  }
       |  kafka {
       |    connection-string = "nowhere:-1"
       |    schema-registry = "http://nowhere:-1"
       |  }
       |
       |  cookie-session.lifetime = "600 milliseconds"
       |}
    """.stripMargin

  val fullConfiguration: Config =
    ConfigFactory
      .parseString(configuration)
      .withFallback(ConfigFactory.load("default-application.conf"))
      .resolve()

  val actorSystem: ActorSystem[Nothing] =
    ActorSystem[Nothing](Behaviors.empty, "SessionHistoryCoordinatorIT", fullConfiguration)
}
