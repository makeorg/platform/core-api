package org.make.api.proposal

import com.typesafe.config.ConfigFactory
import org.make.core.BaseUnitTest
import org.make.api.technical.security.SecurityConfiguration
import org.scalatest.OptionValues._
import stamina.Persisted

import java.time.{LocalDate, LocalTime, ZoneOffset, ZonedDateTime}
import akka.util.ByteString

class ProposalStateTest extends BaseUnitTest {
  val serializers = ProposalSerializers(
    new SecurityConfiguration(ConfigFactory.parseString("""
secure-hash-salt = "jxu-qdimuh-yi-42"
secure-vote-salt = "d3f4u17v0735417"
aes-secret-key = "G9pPOCayHYlBnNAq1mCVqA=="
"""))
  )

  Feature("deserialization") {
    Scenario("createdAt field formatted as zoned date time") {
      val json =
        """{"proposal":{"author":"my-user-id","content":"My proposal","createdAt":"4000-11-30T20:17:29Z[UTC]","creationContext":{"customData":{},"externalId":"","requestId":"","sessionId":""},"proposalType": "SUBMITTED","events":[{"actionType":"qualification","arguments":{"argument":"value"},"date":"2018-03-01T16:09:30.441Z","user":"my-user-id"}],"idea":"idea-id","initialProposal":false,"keywords":[],"labels":["star"],"operation":"operation-id","organisationIds":[],"organisations":[],"proposalId":"proposal-id","refusalReason":"because","slug":"my-proposal","status":"Accepted","tags":["tag-1","tag-2"],"theme":"theme-id","updatedAt":"2018-03-01T16:09:30.441Z","votes":[{"count":20,"countSegment":0,"countSequence":20,"countVerified":20,"key":"agree","qualifications":[{"count":12,"countSegment":0,"countSequence":12,"countVerified":12,"key":"doable"}]}]}}"""

      val state =
        serializers.proposalStateSerializer.unpersist(Persisted("proposalState", 10, ByteString.fromString(json)))

      state.proposal.value.createdAt.value shouldEqual (ZonedDateTime
        .of(LocalDate.of(4000, 11, 30), LocalTime.of(20, 17, 29), ZoneOffset.UTC))
    }
  }
}
