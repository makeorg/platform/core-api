/*
 *  Make.org Core API
 *  Copyright (C) 2021 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.proposal

import io.circe.{Codec, Decoder, Encoder}
import io.circe.generic.semiauto.{deriveCodec, deriveDecoder, deriveEncoder}
import io.swagger.annotations.ApiModelProperty
import org.make.core.{ApplicationName, CirceFormatters}
import org.make.core.technical.Multilingual
import org.make.core.idea.IdeaId
import org.make.core.operation.OperationId
import org.make.core.proposal.{ProposalKeyword, ProposalStatus}
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, LabelId, Language}
import org.make.core.session.{SessionId, VisitorId}
import org.make.core.tag.TagId
import org.make.core.user.UserId

import java.time.ZonedDateTime
import scala.annotation.meta.field

final case class PatchProposalRequest(
  slug: Option[String] = None,
  content: Option[String] = None,
  contentTranslations: Option[Multilingual[String]] = None,
  submittedAsLanguage: Option[Language] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  author: Option[UserId] = None,
  @(ApiModelProperty @field)(dataType = "list[string]")
  labels: Option[Seq[LabelId]] = None,
  @(ApiModelProperty @field)(dataType = "string", allowableValues = ProposalStatus.swaggerAllowableValues)
  status: Option[ProposalStatus] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "other")
  refusalReason: Option[String] = None,
  @(ApiModelProperty @field)(dataType = "boolean", example = "false")
  isAnonymous: Option[Boolean] = None,
  @(ApiModelProperty @field)(dataType = "list[string]")
  tags: Option[Seq[TagId]] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  questionId: Option[QuestionId] = None,
  creationContext: Option[PatchRequestContext] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  ideaId: Option[IdeaId] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  operation: Option[OperationId] = None,
  @(ApiModelProperty @field)(dataType = "boolean")
  initialProposal: Option[Boolean] = None,
  keywords: Option[Seq[ProposalKeyword]] = None
)

object PatchProposalRequest {

  implicit val codec: Codec[PatchProposalRequest] = deriveCodec
}

final case class PatchRequestContext(
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  requestId: Option[String] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  sessionId: Option[SessionId] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  visitorId: Option[VisitorId] = None,
  visitorCreatedAt: Option[ZonedDateTime] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  externalId: Option[String] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "FR")
  country: Option[Country] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "FR")
  detectedCountry: Option[Country] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "fr")
  language: Option[Language] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  operation: Option[OperationId] = None,
  source: Option[String] = None,
  location: Option[String] = None,
  question: Option[String] = None,
  hostname: Option[String] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "0.0.0.x")
  ipAddress: Option[String] = None,
  ipAddressHash: Option[String] = None,
  getParameters: Option[Map[String, String]] = None,
  userAgent: Option[String] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "11111111-2222-3333-4444-555555555555")
  questionId: Option[QuestionId] = None,
  @(ApiModelProperty @field)(dataType = "string", allowableValues = ApplicationName.swaggerAllowableValues)
  applicationName: Option[ApplicationName] = None,
  @(ApiModelProperty @field)(dataType = "string", example = "main-front")
  referrer: Option[String] = None,
  customData: Option[Map[String, String]] = None
)

object PatchRequestContext extends CirceFormatters {
  implicit val encoder: Encoder[PatchRequestContext] = deriveEncoder[PatchRequestContext]
  implicit val decoder: Decoder[PatchRequestContext] = deriveDecoder[PatchRequestContext]
}
