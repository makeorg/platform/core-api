/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.proposal

import org.make.api.proposal.ProposalActor.State
import org.make.api.proposal.ProposalEvent._
import org.make.api.proposal.PublishedProposalEvent._
import org.make.api.technical.MakeEventSerializer
import org.make.api.technical.security.SecurityConfiguration
import org.make.core.proposal._
import org.make.core.question.QuestionId
import org.make.core.reference.Language
import org.make.core.technical.Multilingual
import org.make.core.user.UserId
import org.make.core.{RequestContextLanguage, RequestContextQuestion, SprayJsonFormatters}
import spray.json.DefaultJsonProtocol._
import spray.json.lenses.JsonLenses._
import spray.json.{DefaultJsonProtocol, JsObject, RootJsonFormat}
import stamina._
import stamina.json._

import java.time.ZonedDateTime
import scala.util.chaining.scalaUtilChainingOps

final class ProposalSerializers(securityConfiguration: SecurityConfiguration) extends SprayJsonFormatters {

  private val proposalProposedSerializer: JsonPersister[ProposalProposed, V9] =
    persister[ProposalProposed, V9](
      "proposal-proposed",
      from[V1]
        .to[V2](
          _.update("language" ! set[Option[String]](Some("fr")))
            .update("country" ! set[Option[String]](Some("FR")))
        )
        .to[V3] {
          _.update("initialProposal" ! set[Boolean](false))
        }
        .to[V4](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V5](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V6] { json =>
          val language = json.extract[String]("language".?)
          json.update("submittedAsLanguage" ! set(language))
        }
        .to[V7] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
        .to[V8] {
          _.update("isAnonymous" ! set[Boolean](false))
        }
        .to[V9] { json =>
          val isInitial = json.extract[Boolean]("initialProposal")
          if (isInitial) {
            json.update("proposalType" ! set[ProposalType](ProposalType.ProposalTypeInitial))
          } else {
            json.update("proposalType" ! set[ProposalType](ProposalType.ProposalTypeSubmitted))
          }
        }
    )

  private val proposalViewedSerializer: JsonPersister[ProposalViewed, V4] =
    persister[ProposalViewed, V4](
      "proposal-viewed",
      from[V1]
        .to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val proposalUpdatedSerializer: JsonPersister[ProposalUpdated, V4] =
    persister[ProposalUpdated, V4](
      "proposal-updated",
      from[V1]
        .to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val proposalAcceptedSerializer: JsonPersister[ProposalAccepted, V4] =
    persister[ProposalAccepted, V4](
      "proposal-accepted",
      from[V1]
        .to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val proposalRefusedSerializer: JsonPersister[ProposalRefused, V4] =
    persister[ProposalRefused, V4](
      "proposal-refused",
      from[V1]
        .to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val proposalPostponedSerializer: JsonPersister[ProposalPostponed, V4] =
    persister[ProposalPostponed, V4](
      "proposal-postponed",
      from[V1]
        .to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val proposalVotesVerifiedUpdatedSerializer: JsonPersister[ProposalVotesVerifiedUpdated, V4] =
    persister[ProposalVotesVerifiedUpdated, V4](
      "proposal-votes-verified-updated",
      from[V1]
        .to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val updatedVerifiedVotes = json.extract[Seq[Vote]]("votesVerified")
          json.update(
            "updatedVerifiedVotes" ! set[Option[VotingOptions]](
              Some(LegacyVotesConverter.convertSeqToVotingOptions(updatedVerifiedVotes))
            )
          )
        }
    )

  private val proposalVotesUpdatedSerializer: JsonPersister[ProposalVotesUpdated, V4] =
    persister[ProposalVotesUpdated, V4](
      "proposal-votes-updated",
      from[V1]
        .to[V2](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V3] { json =>
          val updatedVotes = json.extract[Seq[Vote]]("newVotes")
          json.update(
            "updatedVotes" ! set[Option[VotingOptions]](
              Some(LegacyVotesConverter.convertSeqToVotingOptions(updatedVotes))
            )
          )
        }
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val proposalVotedSerializer: JsonPersister[ProposalVoted, V7] =
    persister[ProposalVoted, V7](
      "proposal-voted",
      from[V1]
        .to[V2](_.update("organisationInfo" ! set[Option[OrganisationInfo]](None)))
        .to[V3] { json =>
          val organisationId =
            json.extract[Option[OrganisationInfo]]("organisationInfo".?).flatMap(_.map(_.organisationId.value))
          organisationId.fold(json) { id =>
            json.update("maybeOrganisationId" ! set[String](id))
          }
        }
        .to[V4](_.update("voteTrust" ! set[String]("trusted")))
        .to[V5](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V6](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V7] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val proposalUnvotedSerializer: JsonPersister[ProposalUnvoted, V7] =
    persister[ProposalUnvoted, V7](
      "proposal-unvoted",
      from[V1]
        .to[V2](_.update("organisationInfo" ! set[Option[OrganisationInfo]](None)))
        .to[V3] { json =>
          val organisationInfo =
            json.extract[Option[OrganisationInfo]]("organisationInfo".?).flatMap(_.map(_.organisationId.value))
          organisationInfo.fold(json) { id =>
            json.update("maybeOrganisationId" ! set[String](id))
          }
        }
        .to[V4](_.update("voteTrust" ! set[String]("trusted")))
        .to[V5](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V6](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V7] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val proposalQualifiedSerializer: JsonPersister[ProposalQualified, V5] =
    persister[ProposalQualified, V5](
      "proposal-qualified",
      from[V1]
        .to[V2](_.update("voteTrust" ! set[String]("trusted")))
        .to[V3](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V5] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val proposalUnqualifiedSerializer: JsonPersister[ProposalUnqualified, V5] =
    persister[ProposalUnqualified, V5](
      "proposal-unqualified",
      from[V1]
        .to[V2](_.update("voteTrust" ! set[String]("trusted")))
        .to[V3](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V5] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val similarProposalsAddedSerializer: JsonPersister[SimilarProposalsAdded, V4] =
    persister[SimilarProposalsAdded, V4](
      "similar-proposals-added",
      from[V1]
        .to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val proposalLockedSerializer: JsonPersister[ProposalLocked, V4] =
    persister[ProposalLocked, V4](
      "proposal-locked",
      from[V1]
        .to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json
            .update(
              "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private[proposal] val proposalStateSerializer: JsonPersister[State, V16] = {
    final case class QualificationV5(key: QualificationKey, count: Int)
    implicit val qualificationV5Formatter: RootJsonFormat[QualificationV5] =
      DefaultJsonProtocol.jsonFormat2(QualificationV5.apply)

    final case class VoteV5(key: VoteKey, count: Int, qualifications: Seq[QualificationV5])
    implicit val voteV5Formatter: RootJsonFormat[VoteV5] =
      DefaultJsonProtocol.jsonFormat3(VoteV5.apply)

    final case class QualificationV6(key: QualificationKey, count: Int, countVerified: Int)
    implicit val qualificationV6formatter: RootJsonFormat[QualificationV6] =
      DefaultJsonProtocol.jsonFormat3(QualificationV6.apply)

    final case class VoteV6(key: VoteKey, count: Int, countVerified: Int, qualifications: Seq[QualificationV6])
    implicit val voteV6formatter: RootJsonFormat[VoteV6] =
      DefaultJsonProtocol.jsonFormat4(VoteV6.apply)

    final case class QualificationV8(
      key: QualificationKey,
      count: Int,
      countVerified: Int,
      countSequence: Int,
      countSegment: Int
    )
    implicit val qualificationV8formatter: RootJsonFormat[QualificationV8] =
      DefaultJsonProtocol.jsonFormat5(QualificationV8.apply)

    final case class VoteV8(
      key: VoteKey,
      count: Int,
      countVerified: Int,
      countSequence: Int,
      countSegment: Int,
      qualifications: Seq[QualificationV8]
    )

    implicit val voteV8formatter: RootJsonFormat[VoteV8] =
      DefaultJsonProtocol.jsonFormat6(VoteV8.apply)

    persister[State, V16](
      "proposalState",
      from[V1]
        .to[V2](
          _.update("proposal" / "language" ! set[Option[String]](Some("fr")))
            .update("proposal" / "country" ! set[Option[String]](Some("FR")))
        )
        .to[V3](_.update("proposal" / "organisations" ! set[Seq[OrganisationInfo]](Seq.empty)))
        .to[V4] { json =>
          val organisationInfos = json.extract[Seq[OrganisationInfo]]("proposal" / "organisations")
          json.update("proposal" / "organisationIds" ! set[Seq[UserId]](organisationInfos.map(_.organisationId)))
        }
        .to[V5] {
          _.update("proposal" / "initialProposal" ! set[Boolean](false))
        }
        .to[V6] { json =>
          val votes: Seq[VoteV6] = json
            .extract[Seq[VoteV5]]("proposal" / "votes")
            .map(vote => {
              val qualifications: Seq[QualificationV6] = vote.qualifications
                .map(qualification => QualificationV6(qualification.key, qualification.count, qualification.count))
              VoteV6(vote.key, vote.count, vote.count, qualifications)
            })
          json.update("proposal" / "votes", votes)
        }
        .to[V7](_.update("proposal" / "creationContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V8] { json =>
          val votes: Seq[VoteV8] = json
            .extract[Seq[VoteV6]]("proposal" / "votes")
            .map(vote => {
              val qualifications: Seq[QualificationV8] = vote.qualifications
                .map(
                  qualification =>
                    QualificationV8(
                      key = qualification.key,
                      count = qualification.count,
                      countVerified = qualification.countVerified,
                      countSequence = qualification.countVerified,
                      countSegment = 0
                    )
                )
              VoteV8(
                key = vote.key,
                count = vote.count,
                countVerified = vote.countVerified,
                countSequence = vote.countVerified,
                countSegment = 0,
                qualifications = qualifications
              )
            })
          json.update("proposal" / "votes", votes)
        }
        .to[V9] { json =>
          json.update("proposal" / "keywords", Seq.empty[ProposalKeyword])
        }
        // format: off
        .to[V10](
          _.update(
            "proposal" / "creationContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V11] { json =>
          json
            .asJsObject.fields.get("proposal")
            .fold(json)(
              _
                .extract[Seq[VoteV8]]("votes")
                .map(vote =>
                  Vote(
                    key = vote.key,
                    count = vote.count,
                    countVerified = vote.countVerified,
                    countSequence = vote.countSequence,
                    countSegment = vote.countSegment,
                    qualifications =
                      vote.qualifications.map(q =>
                        Qualification(
                          key = q.key,
                          count = q.count,
                          countVerified = q.countVerified,
                          countSequence = q.countSequence,
                          countSegment = q.countSegment
                        )
                      )
                  )
                )
                .pipe(LegacyVotesConverter.convertSeqToVotingOptions)
                .pipe(json.update("proposal" / "votes", _))
            )
        }
        .to[V12] { json =>
          json
            .asJsObject.fields.get("proposal")
            .fold(json)(
              _.extract[Language]("creationContext" / "language".?)
                .pipe(json.update("proposal" / "submittedAsLanguage", _)
                  .update("proposal" / "contentTranslations" ! set[Option[Multilingual[String]]](None)))
            )
        }
        .to[V13] { json =>
          json
            .asJsObject.fields.get("proposal")
            .fold(json) { j =>
              (j.extract[Language]("creationContext" / "language".?),
              j.extract[String]("creationContext" / "question".?),
              j.extract[QuestionId]("creationContext" / "questionId".?)).pipe {
                case (language, question, questionId) =>
                  json.update("proposal" / "creationContext" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty.copy(question = question, questionId = questionId)))
                    .update("proposal" / "creationContext" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty.copy(language = language)))
              }
            }
        }
        .to[V14] { json =>
          json
            .asJsObject.fields.get("proposal")
            .fold(json)(_ => json.update("proposal" / "isAnonymous" ! set[Boolean](false)))
        }
        .to[V15] {
          _.update("proposal".? / "validatedAt" ! set[Option[ZonedDateTime]](None))
            .update("proposal".? / "postponedAt" ! set[Option[ZonedDateTime]](None))
        }
        .to[V16] { json =>
          json.asJsObject.fields.get("proposal").fold(json)(_ => {
          val isInitial = json.extract[Boolean]("proposal" / "initialProposal")
          if (isInitial) {
            json.update("proposal" / "proposalType" ! set[ProposalType](ProposalType.ProposalTypeInitial))
          } else {
            json.update("proposal" / "proposalType" ! set[ProposalType](ProposalType.ProposalTypeSubmitted))
          }})
        }
    )
  }

  private val similarProposalRemovedSerializer: JsonPersister[SimilarProposalRemoved, V4] =
    persister[SimilarProposalRemoved, V4](
      "similar-proposal-removed",
      from[V1].to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](_.update("requestContext" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))))
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json.update("requestContext" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty.copy(question = question, questionId = questionId)))
            .update("requestContext" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty.copy(language = language)))
        }
    )

  private val similarProposalsClearedSerializer: JsonPersister[SimilarProposalsCleared, V4] =
    persister[SimilarProposalsCleared, V4](
      "similar-proposals-cleared",
      from[V1].to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](_.update("requestContext" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))))
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json.update("requestContext" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty.copy(question = question, questionId = questionId)))
            .update("requestContext" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty.copy(language = language)))
        }
    )

  private val proposalPatchedSerializer: JsonPersister[ProposalPatched, V14] = {

    final case class QualificationV4(key: QualificationKey, count: Int)
    implicit val qualificationV4Formatter: RootJsonFormat[QualificationV4] =
      DefaultJsonProtocol.jsonFormat2(QualificationV4.apply)

    final case class VoteV4(key: VoteKey, count: Int, qualifications: Seq[QualificationV4])
    implicit val voteV4Formatter: RootJsonFormat[VoteV4] =
      DefaultJsonProtocol.jsonFormat3(VoteV4.apply)

    final case class QualificationV5(key: QualificationKey, count: Int, countVerified: Int)
    implicit val qualificationV5Formatter: RootJsonFormat[QualificationV5] =
      DefaultJsonProtocol.jsonFormat3(QualificationV5.apply)

    final case class VoteV5(key: VoteKey, count: Int, countVerified: Int, qualifications: Seq[QualificationV5])
    implicit val voteV5Formatter: RootJsonFormat[VoteV5] =
      DefaultJsonProtocol.jsonFormat4(VoteV5.apply)

    final case class QualificationV7(
      key: QualificationKey,
      count: Int,
      countVerified: Int,
      countSequence: Int,
      countSegment: Int
    )
    implicit val qualificationV7formatter: RootJsonFormat[QualificationV7] =
      DefaultJsonProtocol.jsonFormat5(QualificationV7.apply)

    final case class VoteV7(
      key: VoteKey,
      count: Int,
      countVerified: Int,
      countSequence: Int,
      countSegment: Int,
      qualifications: Seq[QualificationV7]
    )

    implicit val voteV7formatter: RootJsonFormat[VoteV7] =
      DefaultJsonProtocol.jsonFormat6(VoteV7.apply)

    persister[ProposalPatched, V14](
      "proposal-tags-updated",
      from[V1]
        .to[V2](_.update("proposal" / "organisations" ! set[Seq[OrganisationInfo]](Seq.empty)))
        .to[V3] { json =>
          val organisationInfos = json.extract[Seq[OrganisationInfo]]("proposal" / "organisations")
          json.update("proposal" / "organisationIds" ! set[Seq[UserId]](organisationInfos.map(_.organisationId)))
        }
        .to[V4] {
          _.update("proposal" / "initialProposal" ! set[Boolean](false))
        }
        .to[V5] { json =>
          val votes: Seq[VoteV5] = json
            .extract[Seq[VoteV4]]("proposal" / "votes")
            .map(vote => {
              val qualifications: Seq[QualificationV5] = vote.qualifications
                .map(qualification => QualificationV5(qualification.key, qualification.count, qualification.count))
              VoteV5(vote.key, vote.count, vote.count, qualifications)
            })
          json.update("proposal" / "votes", votes)
        }
        .to[V6](
          _.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty))
            .update("proposal" / "creationContext" / "customData" ! set[Map[String, String]](Map.empty))
        )
        .to[V7] { json =>
          val votes: Seq[VoteV7] = json
            .extract[Seq[VoteV5]]("proposal" / "votes")
            .map(vote => {
              val qualifications: Seq[QualificationV7] = vote.qualifications
                .map(
                  qualification =>
                    QualificationV7(
                      key = qualification.key,
                      count = qualification.countVerified,
                      countVerified = qualification.countVerified,
                      countSequence = qualification.count,
                      countSegment = 0
                    )
                )
              VoteV7(
                key = vote.key,
                count = vote.count,
                countVerified = vote.countVerified,
                countSequence = vote.countVerified,
                countSegment = 0,
                qualifications = qualifications
              )
            })
          json.update("proposal" / "votes", votes)
        }
        .to[V8] { json =>
          json.update("proposal" / "keywords", Seq.empty[ProposalKeyword])
        }
        .to[V9](_.update("requestContext" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)))
          .update("proposal" / "creationContext" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)))
        )
        .to[V10] { json =>
          val language = json.extract[Language]("proposal" / "creationContext" / "language".?)
          json.update("proposal" / "submittedAsLanguage" ! set[Option[Language]](language))
            .update("proposal" / "contentTranslations" ! set[Option[Multilingual[String]]](None))
        }
        .to[V11] { json =>
          val language = json.extract[Language]("proposal" / "creationContext" / "language".?)
          val question = json.extract[String]("proposal" / "creationContext" / "question".?)
          val questionId = json.extract[QuestionId]("proposal" / "creationContext" / "questionId".?)
          json.update("proposal" / "creationContext" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty.copy(question = question, questionId = questionId)))
            .update("proposal" / "creationContext" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty.copy(language = language)))
            .update("requestContext" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty.copy(question = question, questionId = questionId)))
            .update("requestContext" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty.copy(language = language)))
        }
        .to[V12] {
          _.update("proposal" / "isAnonymous" ! set[Boolean](false))
        }
        .to[V13] {
          _.update("proposal" / "validatedAt" ! set[Option[ZonedDateTime]](None))
            .update("proposal" / "postponedAt" ! set[Option[ZonedDateTime]](None))
        }
        .to[V14] { json =>
          val isInitial = json.extract[Boolean]("proposal" / "initialProposal")
          if (isInitial) {
            json.update("proposal" / "proposalType" ! set[ProposalType](ProposalType.ProposalTypeInitial))
          } else {
            json.update("proposal" / "proposalType" ! set[ProposalType](ProposalType.ProposalTypeSubmitted))
          }
        }
    )
  }

  private val proposalAnonymizedSerializer: JsonPersister[ProposalAnonymized, V4] =
    persister[ProposalAnonymized, V4](
      "proposal-anonymized",
      from[V1].to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](_.update("requestContext" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))))
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json.update("requestContext" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty.copy(question = question, questionId = questionId)))
            .update("requestContext" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty.copy(language = language)))
        }
    )

  private val proposalAddedToOperationSerializer: JsonPersister[ProposalAddedToOperation, V4] =
    persister[ProposalAddedToOperation, V4](
      "proposal-added-to-operation",
      from[V1].to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](_.update("requestContext" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))))
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json.update("requestContext" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty.copy(question = question, questionId = questionId)))
            .update("requestContext" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty.copy(language = language)))
        }
    )

  private val proposalRemovedFromOperationSerializer: JsonPersister[ProposalRemovedFromOperation, V4] =
    persister[ProposalRemovedFromOperation, V4](
      "proposal-removed-from-operation",
      from[V1].to[V2](_.update("requestContext" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](_.update("requestContext" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))))
        .to[V4] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json.update("requestContext" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty.copy(question = question, questionId = questionId)))
            .update("requestContext" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty.copy(language = language)))
        }
    )

  private val proposalKeywordsSetSerializer: JsonPersister[ProposalKeywordsSet, V3] =
    persister[ProposalKeywordsSet, V3]("proposal-keywords-set",
      from[V1].to[V2](_.update("requestContext" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))))
        .to[V3] { json =>
          val language = json.extract[Language]("requestContext" / "language".?)
          val question = json.extract[String]("requestContext" / "question".?)
          val questionId = json.extract[QuestionId]("requestContext" / "questionId".?)
          json.update("requestContext" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty.copy(question = question, questionId = questionId)))
            .update("requestContext" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty.copy(language = language)))
        }
  )

  private val proposalSerializer: JsonPersister[Proposal, V8] =
    persister[Proposal, V8]("proposal-entity", from[V1].to[V2](_.update("keywords", Seq.empty[ProposalKeyword]))
      .to[V3](_.update("creationContext" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))))
      .to[V4] { json =>
        val language = json.extract[Language]("creationContext" / "language".?)
        json.update("submittedAsLanguage" ! set[Option[Language]](language))
          .update("contentTranslations" ! set[Option[Multilingual[String]]](None))
      }
      .to[V5] { json =>
        val language = json.extract[Language]("creationContext" / "language".?)
        val question = json.extract[String]("creationContext" / "question".?)
        val questionId = json.extract[QuestionId]("creationContext" / "questionId".?)
        json.update("creationContext" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty.copy(question = question, questionId = questionId)))
          .update("creationContext" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty.copy(language = language)))
      }
      .to[V6] {
        _.update("isAnonymous" ! set[Boolean](false))
      }
      .to[V7] {
        _.update("validatedAt" ! set[Option[ZonedDateTime]](None))
          .update("postponedAt" ! set[Option[ZonedDateTime]](None))
      }
      .to[V8] { json =>
        val isInitial = json.extract[Boolean]("initialProposal")
        if (isInitial) {
          json.update("proposalType" ! set[ProposalType](ProposalType.ProposalTypeInitial))
        } else {
          json.update("proposalType" ! set[ProposalType](ProposalType.ProposalTypeSubmitted))
        }
      }
    )

  val serializers: Seq[JsonPersister[_, _]] =
    Seq(
      proposalProposedSerializer,
      proposalViewedSerializer,
      proposalUpdatedSerializer,
      proposalAcceptedSerializer,
      proposalRefusedSerializer,
      proposalPostponedSerializer,
      proposalVotesVerifiedUpdatedSerializer,
      proposalVotesUpdatedSerializer,
      proposalVotedSerializer,
      proposalUnvotedSerializer,
      proposalQualifiedSerializer,
      proposalUnqualifiedSerializer,
      proposalLockedSerializer,
      proposalStateSerializer,
      similarProposalsAddedSerializer,
      similarProposalRemovedSerializer,
      similarProposalsClearedSerializer,
      proposalPatchedSerializer,
      proposalAnonymizedSerializer,
      proposalAddedToOperationSerializer,
      proposalRemovedFromOperationSerializer,
      proposalKeywordsSetSerializer,
      proposalSerializer
    )
}

object ProposalSerializers {
  def apply(securityConfiguration: SecurityConfiguration): ProposalSerializers =
    new ProposalSerializers(securityConfiguration)
}
