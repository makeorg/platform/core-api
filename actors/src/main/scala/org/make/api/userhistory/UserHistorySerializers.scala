/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.userhistory

import org.make.api.technical.MakeEventSerializer
import org.make.api.technical.security.SecurityConfiguration
import org.make.api.user.Anonymization
import org.make.api.userhistory.UserVotesAndQualifications
import org.make.core.question.QuestionId
import org.make.core.reference.Language

import java.time.ZonedDateTime
import org.make.core.{RequestContextLanguage, RequestContextQuestion, SprayJsonFormatters}
import spray.json.DefaultJsonProtocol._
import spray.json.lenses.JsonLenses._
import spray.json.{JsArray, JsObject, JsString, JsValue}
import stamina._
import stamina.json._

final class UserHistorySerializers(securityConfiguration: SecurityConfiguration) extends SprayJsonFormatters {

  val countryFixDate: ZonedDateTime = ZonedDateTime.parse("2018-09-01T00:00:00Z")
  private val logRegisterCitizenEventSerializer: JsonPersister[LogRegisterCitizenEvent, V7] =
    json.persister[LogRegisterCitizenEvent, V7](
      "user-history-registered",
      from[V1]
        .to[V2](
          _.update("action" / "arguments" / "country" ! set[String]("FR"))
            .update("action" / "arguments" / "language" ! set[String]("fr"))
        )
        .to[V3](identity)
        .to[V4] { json =>
          val actionDate: ZonedDateTime = ZonedDateTime.parse(json.extract[String]("action" / "date"))

          if (actionDate.isBefore(countryFixDate)) {

            json.extract[JsObject]("context").getFields("language") match {

              case Seq(JsString("fr")) =>
                json
                  .update("context" / "source" ! set[String]("core"))
                  .update("context" / "country" ! set[String]("FR"))
              case Seq(JsString("it")) =>
                json
                  .update("context" / "source" ! set[String]("core"))
                  .update("context" / "country" ! set[String]("IT"))
              case Seq(JsString("en")) =>
                json
                  .update("context" / "source" ! set[String]("core"))
                  .update("context" / "country" ! set[String]("GB"))
              case _ =>
                json
                  .update("context" / "source" ! set[String]("core"))
                  .update("context" / "country" ! set[String]("FR"))
                  .update("context" / "language" ! set[String]("fr"))
            }
          } else {
            json
          }
        }
        .to[V5] {
          _.update("context" / "customData" ! set[Map[String, String]](Map.empty))
        }
        .to[V6](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V7] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logSearchProposalsEventSerializer: JsonPersister[LogUserSearchProposalsEvent, V4] =
    json.persister[LogUserSearchProposalsEvent, V4](
      "user-history-searched",
      from[V1]
        .to[V2](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logAcceptProposalEventSerializer: JsonPersister[LogAcceptProposalEvent, V4] =
    json.persister[LogAcceptProposalEvent, V4](
      "user-history-accepted-proposal",
      from[V1]
        .to[V2](
          _.update("context" / "customData" ! set[Map[String, String]](Map.empty))
            .update("action" / "arguments" / "requestContext" / "customData" ! set[Map[String, String]](Map.empty))
        )
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          ).update(
            "action" / "arguments" / "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logRefuseProposalEventSerializer: JsonPersister[LogRefuseProposalEvent, V4] =
    json.persister[LogRefuseProposalEvent, V4](
      "user-history-refused-proposal",
      from[V1]
        .to[V2](
          _.update("context" / "customData" ! set[Map[String, String]](Map.empty))
            .update("action" / "arguments" / "requestContext" / "customData" ! set[Map[String, String]](Map.empty))
        )
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          ).update(
            "action" / "arguments" / "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logPostponeProposalEventSerializer: JsonPersister[LogPostponeProposalEvent, V4] =
    json.persister[LogPostponeProposalEvent, V4](
      "user-history-postponed-proposal",
      from[V1]
        .to[V2](
          _.update("context" / "customData" ! set[Map[String, String]](Map.empty))
            .update("action" / "arguments" / "requestContext" / "customData" ! set[Map[String, String]](Map.empty))
        )
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          ).update(
            "action" / "arguments" / "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logLockProposalEventSerializer: JsonPersister[LogLockProposalEvent, V4] =
    json.persister[LogLockProposalEvent, V4](
      "user-history-lock-proposal",
      from[V1]
        .to[V2](
          _.update("context" / "customData" ! set[Map[String, String]](Map.empty))
            .update("action" / "arguments" / "requestContext" / "customData" ! set[Map[String, String]](Map.empty))
        )
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          ).update(
            "action" / "arguments" / "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserProposalEventSerializer: JsonPersister[LogUserProposalEvent, V4] =
    json.persister[LogUserProposalEvent, V4](
      "user-history-sent-proposal",
      from[V1]
        .to[V2](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserVoteEventSerializer: JsonPersister[LogUserVoteEvent, V5] =
    json.persister[LogUserVoteEvent, V5](
      "user-history-vote-proposal",
      from[V1]
        .to[V2](_.update("action" / "arguments" / "trust" ! set[String]("trusted")))
        .to[V3](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V5] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserUnvoteEventSerializer: JsonPersister[LogUserUnvoteEvent, V5] =
    json.persister[LogUserUnvoteEvent, V5](
      "user-history-unvote-proposal",
      from[V1]
        .to[V2](_.update("action" / "arguments" / "trust" ! set[String]("trusted")))
        .to[V3](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V5] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserQualificationEventSerializer: JsonPersister[LogUserQualificationEvent, V5] =
    json.persister[LogUserQualificationEvent, V5](
      "user-history-qualification-vote",
      from[V1]
        .to[V2](_.update("action" / "arguments" / "trust" ! set[String]("trusted")))
        .to[V3](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V5] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserUnqualificationEventSerializer: JsonPersister[LogUserUnqualificationEvent, V5] =
    json.persister[LogUserUnqualificationEvent, V5](
      "user-history-unqualification-vote",
      from[V1]
        .to[V2](_.update("action" / "arguments" / "trust" ! set[String]("trusted")))
        .to[V3](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V5] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserCreateSequenceEventSerializer: JsonPersister[LogUserCreateSequenceEvent, V4] =
    json.persister[LogUserCreateSequenceEvent, V4](
      "user-history-create-sequence",
      from[V1]
        .to[V2](
          _.update("context" / "customData" ! set[Map[String, String]](Map.empty))
            .update("action" / "arguments" / "requestContext" / "customData" ! set[Map[String, String]](Map.empty))
        )
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          ).update(
            "action" / "arguments" / "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserAddProposalsSequenceEventSerializer: JsonPersister[LogUserAddProposalsSequenceEvent, V4] =
    json.persister[LogUserAddProposalsSequenceEvent, V4](
      "user-history-add-proposals-sequence",
      from[V1]
        .to[V2](
          _.update("context" / "customData" ! set[Map[String, String]](Map.empty))
            .update("action" / "arguments" / "requestContext" / "customData" ! set[Map[String, String]](Map.empty))
        )
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          ).update(
            "action" / "arguments" / "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserRemoveSequenceEventSerializer: JsonPersister[LogUserRemoveProposalsSequenceEvent, V4] =
    json.persister[LogUserRemoveProposalsSequenceEvent, V4](
      "user-history-remove-proposals-sequence",
      from[V1]
        .to[V2](
          _.update("context" / "customData" ! set[Map[String, String]](Map.empty))
            .update("action" / "arguments" / "requestContext" / "customData" ! set[Map[String, String]](Map.empty))
        )
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          ).update(
            "action" / "arguments" / "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logGetProposalDuplicatesEventSerializer: JsonPersister[LogGetProposalDuplicatesEvent, V4] =
    json.persister[LogGetProposalDuplicatesEvent, V4](
      "user-history-get-proposals-duplicate",
      from[V1]
        .to[V2](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserUpdateSequenceEventSerializer: JsonPersister[LogUserUpdateSequenceEvent, V4] =
    json.persister[LogUserUpdateSequenceEvent, V4](
      "user-history-update-sequence",
      from[V1]
        .to[V2](
          _.update("context" / "customData" ! set[Map[String, String]](Map.empty))
            .update("action" / "arguments" / "requestContext" / "customData" ! set[Map[String, String]](Map.empty))
        )
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          ).update(
            "action" / "arguments" / "requestContext" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "action" / "arguments" / "requestContext" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserStartSequenceEventSerializer: JsonPersister[LogUserStartSequenceEvent, V6] =
    json.persister[LogUserStartSequenceEvent, V6](
      "user-history-start-sequence",
      from[V1]
        .to[V2](_.update("action" / "arguments" / "includedProposals" ! set[Seq[String]](Seq.empty)))
        .to[V3](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](_.update("action" / "questionId" ! set[Option[String]](None)))
        .to[V5](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V6] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserAnonymizedEventSerializer: JsonPersister[LogUserAnonymizedEvent, V5] =
    json.persister[LogUserAnonymizedEvent, V5](
      "user-anonymized",
      from[V1]
        .to[V2](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V4]("action" / "arguments" / "mode" ! set(Anonymization.Automatic.value))
        .to[V5] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserOptInNewsletterEventSerializer: JsonPersister[LogUserOptInNewsletterEvent, V4] =
    json.persister[LogUserOptInNewsletterEvent, V4](
      "user-opt-in-newsletter",
      from[V1]
        .to[V2](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserOptOutNewsletterEventSerializer: JsonPersister[LogUserOptOutNewsletterEvent, V4] =
    json.persister[LogUserOptOutNewsletterEvent, V4](
      "user-opt-out-newsletter",
      from[V1]
        .to[V2](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserConnectedEventSerializer: JsonPersister[LogUserConnectedEvent, V3] =
    json.persister[LogUserConnectedEvent, V3](
      "user-connected",
      from[V1]
        .to[V2](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V3] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logUserUploadedAvatarEventSerializer: JsonPersister[LogUserUploadedAvatarEvent, V3] =
    json.persister[LogUserUploadedAvatarEvent, V3](
      "user-uploaded-avatar",
      from[V1]
        .to[V2](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V3] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logOrganisationEmailChangedEventSerializer: JsonPersister[LogOrganisationEmailChangedEvent, V3] =
    json.persister[LogOrganisationEmailChangedEvent, V3](
      "organisation-email-changed",
      from[V1]
        .to[V2](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V3] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  val defaultVoteDate: ZonedDateTime = ZonedDateTime.parse("2018-10-10T00:00:00Z")
  private val userVotesAndQualifications: JsonPersister[UserVotesAndQualifications, V3] =
    json.persister[UserVotesAndQualifications, V3](
      "user-votes-and-qualifications",
      from[V1]
        .to[V2](_.update("votesAndQualifications" ! modify[Map[String, JsValue]] { voteAndQualifications =>
          voteAndQualifications.map {
            case (key, value) =>
              key -> value.update("date" ! set[ZonedDateTime](defaultVoteDate))
          }
        }))
        .to[V3](
          _.update("votesAndQualifications" ! modify[Map[String, JsObject]] {
            voteAndQualifications =>
              voteAndQualifications.map {
                case (key, proposalVotes) =>
                  val fields: Map[String, JsValue] = proposalVotes.fields
                  @SuppressWarnings(Array("org.wartremover.warts.AsInstanceOf"))
                  val qualifications =
                    fields("qualificationKeys").asInstanceOf[JsArray].elements.map(_.asInstanceOf[JsString])
                  val newQualifications: Map[String, JsValue] = qualifications.map(_.value -> JsString("trusted")).toMap
                  val modifiedFields: Map[String, JsValue] = fields + ("qualificationKeys" -> JsObject(
                    newQualifications
                  )) + ("trust" -> JsString("trusted"))
                  key -> JsObject(modifiedFields)
              }
          })
        )
    )

  val serializers: Seq[JsonPersister[_, _]] =
    Seq(
      logRegisterCitizenEventSerializer,
      logSearchProposalsEventSerializer,
      logAcceptProposalEventSerializer,
      logRefuseProposalEventSerializer,
      logPostponeProposalEventSerializer,
      logLockProposalEventSerializer,
      logUserProposalEventSerializer,
      logUserVoteEventSerializer,
      logUserUnvoteEventSerializer,
      logUserQualificationEventSerializer,
      logUserUnqualificationEventSerializer,
      logUserCreateSequenceEventSerializer,
      logUserUpdateSequenceEventSerializer,
      logUserAddProposalsSequenceEventSerializer,
      logUserRemoveSequenceEventSerializer,
      logGetProposalDuplicatesEventSerializer,
      logUserStartSequenceEventSerializer,
      userVotesAndQualifications,
      logUserAnonymizedEventSerializer,
      logUserOptInNewsletterEventSerializer,
      logUserOptOutNewsletterEventSerializer,
      logUserConnectedEventSerializer,
      logUserUploadedAvatarEventSerializer,
      logOrganisationEmailChangedEventSerializer
    )
}

object UserHistorySerializers {
  def apply(securityConfiguration: SecurityConfiguration): UserHistorySerializers =
    new UserHistorySerializers(securityConfiguration)
}
