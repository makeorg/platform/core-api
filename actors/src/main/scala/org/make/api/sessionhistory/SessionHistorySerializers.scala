/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.sessionhistory

import org.make.api.technical.MakeEventSerializer
import org.make.api.technical.security.SecurityConfiguration
import org.make.core.{RequestContextLanguage, RequestContextQuestion, SprayJsonFormatters}
import org.make.core.question.QuestionId
import org.make.core.reference.Language
import spray.json.DefaultJsonProtocol._
import spray.json.JsObject
import spray.json.lenses.JsonLenses._
import stamina._
import stamina.json.{from, JsonPersister}

final class SessionHistorySerializers(securityConfiguration: SecurityConfiguration) extends SprayJsonFormatters {

  private val activeSessionSerializer: JsonPersister[Active, V2] = json.persister[Active, V2](
    "active-session",
    from[V1]
      .to[V2](
        _.update(
          "sessionHistory" / "events" / * / "context" / "questionContext" ! set[RequestContextQuestion](
            RequestContextQuestion.empty
          )
        ).update(
          "sessionHistory" / "events" / * / "context" / "languageContext" ! set[RequestContextLanguage](
            RequestContextLanguage.empty
          )
        )
      )
  )

  private val closedSessionSerializer: JsonPersister[Closed, V1] = json.persister[Closed]("closed-session")

  private val transformingSessionSerializer: JsonPersister[Transforming, V2] =
    json.persister[Transforming, V2](
      "transforming-session",
      from[V1]
        .to[V2](
          _.update(
            "sessionHistory" / "events" / * / "context" / "questionContext" ! set[RequestContextQuestion](
              RequestContextQuestion.empty
            )
          ).update(
              "sessionHistory" / "events" / * / "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty
              )
            )
            .update("requestContext" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty))
            .update("requestContext" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty))
        )
    )

  private val expiredSessionSerializer: JsonPersister[Expired, V1] = json.persister[Expired]("expired-session")

  private val logSessionSearchEventSerializer: JsonPersister[LogSessionSearchProposalsEvent, V4] =
    json.persister[LogSessionSearchProposalsEvent, V4](
      "session-history-search-proposal",
      from[V1]
        .to[V2](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logSessionVoteEventSerializer: JsonPersister[LogSessionVoteEvent, V5] =
    json.persister[LogSessionVoteEvent, V5](
      "session-history-vote-proposal",
      from[V1]
        .to[V2](_.update("action" / "arguments" / "trust" ! set[String]("trusted")))
        .to[V3](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V5] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logSessionUnvoteEventSerializer: JsonPersister[LogSessionUnvoteEvent, V5] =
    json.persister[LogSessionUnvoteEvent, V5](
      "session-history-unvote-proposal",
      from[V1]
        .to[V2](_.update("action" / "arguments" / "trust" ! set[String]("trusted")))
        .to[V3](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V5] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logSessionQualificationEventSerializer: JsonPersister[LogSessionQualificationEvent, V5] =
    json.persister[LogSessionQualificationEvent, V5](
      "session-history-qualificaion-vote",
      from[V1]
        .to[V2](_.update("action" / "arguments" / "trust" ! set[String]("trusted")))
        .to[V3](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V5] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logSessionUnqualificationEventSerializer: JsonPersister[LogSessionUnqualificationEvent, V5] =
    json.persister[LogSessionUnqualificationEvent, V5](
      "session-history-unqualificaion-vote",
      from[V1]
        .to[V2](_.update("action" / "arguments" / "trust" ! set[String]("trusted")))
        .to[V3](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V5] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logSessionTransformedEventSerializer: JsonPersister[SessionTransformed, V4] =
    json.persister[SessionTransformed, V4](
      "session-transformed",
      from[V1]
        .to[V2](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logSessionExpiredSerializer: JsonPersister[SessionExpired, V3] =
    json.persister[SessionExpired, V3](
      "session-expired",
      from[V1]
        .to[V2](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V3] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val logSessionStartSequenceEventSerializer: JsonPersister[LogSessionStartSequenceEvent, V4] =
    json.persister[LogSessionStartSequenceEvent, V4](
      "session-history-start-sequence",
      from[V1]
        .to[V2](_.update("context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V3](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V4] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val SessionHistorySerializer: JsonPersister[SessionHistory, V5] =
    json.persister[SessionHistory, V5](
      "session-history",
      from[V1]
        .to[V2] { json =>
          val eventsToMigrate = Set(
            "LogSessionVoteEvent",
            "LogSessionUnvoteEvent",
            "LogSessionQualificationEvent",
            "LogSessionUnqualificationEvent"
          )
          json.update(
            "events" /
              filter("type".is[String](value => eventsToMigrate.contains(value))) /
              "action" /
              "arguments" /
              "trust" !
              set[String]("trusted")
          )
        }
        .to[V3](_.update("events" / * / "context" / "customData" ! set[Map[String, String]](Map.empty)))
        .to[V4](
          _.update(
            "events" / * / "context" ! modify[JsObject](
              MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt)
            )
          )
        )
        .to[V5](
          _.update(
            "events" / * / "context" / "questionContext" ! set[RequestContextQuestion](RequestContextQuestion.empty)
          ).update(
            "events" / * / "context" / "languageContext" ! set[RequestContextLanguage](RequestContextLanguage.empty)
          )
        )
    )

  private val saveLastEventDateSerializer: JsonPersister[SaveLastEventDate, V3] =
    json.persister[SaveLastEventDate, V3](
      "save-last-event-date",
      from[V1]
        .to[V2](
          _.update(
            "context" ! modify[JsObject](MakeEventSerializer.setIpAddressAndHash(securityConfiguration.secureHashSalt))
          )
        )
        .to[V3] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  private val sessionTransformingEventSerializer: JsonPersister[SessionTransforming, V2] =
    json.persister[SessionTransforming, V2](
      "session-transforming-event",
      from[V1]
        .to[V2] { json =>
          val language = json.extract[Language]("context" / "language".?)
          val question = json.extract[String]("context" / "question".?)
          val questionId = json.extract[QuestionId]("context" / "questionId".?)
          json
            .update(
              "context" / "questionContext" ! set[RequestContextQuestion](
                RequestContextQuestion.empty.copy(question = question, questionId = questionId)
              )
            )
            .update(
              "context" / "languageContext" ! set[RequestContextLanguage](
                RequestContextLanguage.empty.copy(language = language)
              )
            )
        }
    )

  val serializers: Seq[JsonPersister[_, _]] =
    Seq(
      activeSessionSerializer,
      closedSessionSerializer,
      transformingSessionSerializer,
      expiredSessionSerializer,
      logSessionSearchEventSerializer,
      logSessionVoteEventSerializer,
      logSessionUnvoteEventSerializer,
      logSessionQualificationEventSerializer,
      logSessionUnqualificationEventSerializer,
      logSessionTransformedEventSerializer,
      logSessionExpiredSerializer,
      logSessionStartSequenceEventSerializer,
      saveLastEventDateSerializer,
      SessionHistorySerializer,
      sessionTransformingEventSerializer
    )
}

object SessionHistorySerializers {
  def apply(securityConfiguration: SecurityConfiguration): SessionHistorySerializers =
    new SessionHistorySerializers(securityConfiguration)
}
