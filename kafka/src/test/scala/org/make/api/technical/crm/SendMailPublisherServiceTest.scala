/*
 *  Make.org Core API
 *  Copyright (C) 2020 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.technical.crm

import cats.data.NonEmptyList
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._
import eu.timepit.refined.collection._
import com.typesafe.config.Config
import org.make.api.{ConfigComponent, MakeUnitTest, TestUtils}
import org.make.api.crmTemplates.{CrmTemplatesService, CrmTemplatesServiceComponent}
import org.make.api.extensions.MailJetTemplateConfiguration
import org.make.core.proposal.ProposalReportReason
import org.make.api.operation.{
  OperationOfQuestionService,
  OperationOfQuestionServiceComponent,
  OperationService,
  OperationServiceComponent
}
import org.make.api.proposal.{
  ProposalCoordinatorService,
  ProposalCoordinatorServiceComponent,
  ProposalService,
  ProposalServiceComponent
}
import org.make.api.question.{QuestionService, QuestionServiceComponent}
import org.make.api.technical.{EventBusService, ValueNotFoundException}
import org.make.api.user.{UserService, UserServiceComponent}
import org.make.core.technical.Multilingual
import org.make.core.crmTemplate.CrmTemplateKind.MessageToProposer
import org.make.core.{
  RequestContext,
  RequestContextLanguage,
  RequestContextQuestion,
  SanitizedHtml,
  Validation,
  ValidationError,
  ValidationFailedError
}
import org.make.core.crmTemplate.{CrmTemplateKind, TemplateId}
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, Language}
import org.make.core.proposal.{ProposalId, ProposalStatus}
import org.make.core.user.{User, UserId, UserType}
import org.mockito.{ArgumentCaptor, Mockito}
import org.scalatest.concurrent.PatienceConfiguration.Timeout

import java.time.ZonedDateTime
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import org.make.core.operation.OperationId
import org.make.core.operation.SequenceCardsConfiguration
import org.make.core.operation.Metas
import org.make.core.operation.QuestionTheme
import org.make.core.operation.OperationOfQuestionTimeline
import org.make.core.operation.OperationOfQuestion
import org.make.core.operation.Operation
import org.make.core.operation.OperationKind

class SendMailPublisherServiceTest
    extends MakeUnitTest
    with DefaultSendMailPublisherServiceComponent
    with CrmTemplatesServiceComponent
    with OperationOfQuestionServiceComponent
    with ProposalCoordinatorServiceComponent
    with ProposalServiceComponent
    with OperationServiceComponent
    with QuestionServiceComponent
    with CrmClientComponent
    with PersistentCrmUserServiceComponent
    with ConfigComponent
    with UserServiceComponent {

  override val crmTemplatesService: CrmTemplatesService = mock[CrmTemplatesService]
  override val eventBusService: EventBusService = mock[EventBusService]
  override val mailJetTemplateConfiguration: MailJetTemplateConfiguration = mock[MailJetTemplateConfiguration]
  override val operationService: OperationService = mock[OperationService]
  override val operationOfQuestionService: OperationOfQuestionService = mock[OperationOfQuestionService]
  override val proposalCoordinatorService: ProposalCoordinatorService = mock[ProposalCoordinatorService]
  override val proposalService: ProposalService = mock[ProposalService]
  override val questionService: QuestionService = mock[QuestionService]
  override val userService: UserService = mock[UserService]
  override val persistentCrmUserService: PersistentCrmUserService = mock[PersistentCrmUserService]
  override val crmClient: CrmClient = mock[CrmClient]
  override val config: Config = mock[Config]

  when(config.getInt("make-api.report-proposal.template-id")).thenReturn(11)
  when(config.getString("make-api.report-proposal.recipient")).thenReturn("foobar@make.org")

  private val question = TestUtils.question(
    id = QuestionId("questionId"),
    countries = NonEmptyList.of(Country("DE"), Country("LU")),
    defaultLanguage = Language("de"),
    languages = NonEmptyList.of(Language("de")),
    questions = Multilingual(Language("de") -> ("How to ask a question ?": String Refined NonEmpty))
  )
  private val organisation =
    TestUtils.user(id = UserId("organisationId"), country = Country("BE"), userType = UserType.UserTypeOrganisation)
  private val user = TestUtils.user(id = UserId("userId"), country = Country("FR"))
  private val requestContext =
    RequestContext.empty.copy(
      country = Some(Country("DE")),
      questionContext = RequestContextQuestion(questionId = Some(question.questionId)),
      languageContext = RequestContextLanguage(language = Some(Language("de")))
    )
  private val organisationProposal =
    proposal(
      id = ProposalId("B2BProposalId"),
      author = organisation.userId,
      questionId = question.questionId,
      refusalReason = Some("invalid"),
      requestContext = requestContext.copy(country = Some(Country("LU")))
    )
  private val userProposal =
    proposal(
      id = ProposalId("proposalId"),
      author = user.userId,
      questionId = question.questionId,
      refusalReason = Some("gibberish"),
      requestContext = requestContext
    )
  private val indexedUserProposal =
    indexedProposal(id = ProposalId("proposalId"), requestContext = Some(requestContext))
  private val evilHtml =
    SanitizedHtml
      .fromString("""<p>coucou<br /></p><p id="foo">Guten tag</p><script source="méchant" />""")
      .get

  when(crmTemplatesService.find(any[CrmTemplateKind], any[Option[QuestionId]], any[Option[Language]])).thenAnswer(
    (kind: CrmTemplateKind, _: Option[QuestionId], _: Option[Language]) =>
      Future.successful(Some(TemplateId(CrmTemplateKind.values.indexOf(kind).toString)))
  )

  when(proposalCoordinatorService.getProposal(organisationProposal.proposalId))
    .thenReturn(Future.successful(Some(organisationProposal)))
  when(proposalCoordinatorService.getProposal(userProposal.proposalId))
    .thenReturn(Future.successful(Some(userProposal)))

  when(proposalService.getProposalById(ProposalId("B2BProposalId"), RequestContext.empty))
    .thenReturn(Future.successful(Some(indexedUserProposal)))
  when(
    proposalService.getProposalById(
      ProposalId("B2BProposalId"),
      RequestContext.empty.copy(
        country = Some(Country("DE")),
        languageContext = RequestContextLanguage(language = Some(Language("de"))),
        questionContext = RequestContextQuestion(questionId = Some(QuestionId("questionId")))
      )
    )
  ).thenReturn(Future.successful(Some(indexedUserProposal)))

  when(questionService.getQuestion(QuestionId("question-id")))
    .thenReturn(Future.successful(Some(question)))

  when(questionService.getQuestion(question.questionId))
    .thenReturn(Future.successful(Some(question)))

  when(userService.changeEmailVerificationTokenIfNeeded(any[UserId])).thenAnswer { id: UserId =>
    Future.successful(
      Some(
        user(
          id,
          verificationToken = Some("verification"),
          verificationTokenExpiresAt = Some(ZonedDateTime.now().plusDays(1))
        )
      )
    )
  }
  when(userService.getUser(organisation.userId)).thenReturn(Future.successful(Some(organisation)))
  when(userService.getUser(user.userId)).thenReturn(Future.successful(Some(user)))
  when(userService.getUser(UserId("author"))).thenReturn(Future.successful(Some(user)))

  Feature("send vote only campaign") {

    val operationOfQuestion = OperationOfQuestion(
      questionId = question.questionId,
      operationId = OperationId("foo"),
      startDate = ZonedDateTime.now(),
      endDate = ZonedDateTime.now(),
      operationTitles = Multilingual(Language("de") -> "Topkek"),
      proposalPrefixes = Multilingual(Language("de") -> "Il fuat en de"),
      canPropose = true,
      sequenceCardsConfiguration = SequenceCardsConfiguration.default,
      aboutUrls = None,
      metas = Metas.apply(None, None, None),
      theme = QuestionTheme(Validation.Colour.black, Validation.Colour.black),
      descriptions = None,
      consultationImages = None,
      consultationImageAlts = None,
      descriptionImages = Some(Multilingual(Language("de") -> "description")),
      descriptionImageAlts = None,
      partnersLogos = Some("partner logo jpg"),
      partnersLogosAlt = None,
      initiatorsLogos = Some("Initiator logo"),
      initiatorsLogosAlt = None,
      consultationHeader = None,
      consultationHeaderAlts = None,
      cobrandingLogo = None,
      cobrandingLogoAlt = None,
      resultsLink = None,
      proposalsCount = 1,
      participantsCount = 1,
      actions = None,
      featured = false,
      votesCount = 1,
      votesTarget = 1,
      timeline = OperationOfQuestionTimeline(None, None, None),
      createdAt = ZonedDateTime.now(),
      sessionBindingMode = false,
      reportUrl = None,
      actionsUrl = None
    )

    when(operationOfQuestionService.findByQuestionId(question.questionId))
      .thenReturn(Future.successful(Some(operationOfQuestion)))

    val defaultUser = PersistentCrmUser(
      userId = "1234",
      fullName = "Toto la Carotte",
      email = "test@make.org",
      firstname = "the user",
      zipcode = Some("12345"),
      dateOfBirth = Some("1970-01-01"),
      emailHardbounceStatus = false,
      emailValidationStatus = false,
      unsubscribeStatus = false,
      accountCreationDate = Some("2019-07-01T16:16:16Z"),
      accountCreationOrigin = Some("origin"),
      accountCreationOperation = Some("weeuropeans-fr"),
      accountCreationSource = Some("source"),
      accountCreationLocation = Some("location"),
      favoriteCountry = Some("FR"),
      favoriteLanguage = Some("de"),
      totalNumberProposals = Some(5),
      totalNumberVotes = Some(42),
      firstContributionDate = Some("2019-07-01T16:16:16Z"),
      lastContributionDate = Some("2019-07-01T16:16:16Z"),
      operationActivity = Some("weeuropeans-fr,weeuropeans-de"),
      sourceActivity = Some("source"),
      daysOfActivity = Some(20),
      daysOfActivity30d = Some(2),
      userType = Some("B2C"),
      accountType = Some("USER"),
      daysBeforeDeletion = Some(3),
      lastActivityDate = Some("2019-07-01T16:16:16Z"),
      sessionsCount = Some(42),
      eventsCount = Some(124)
    )

    when(persistentCrmUserService.getActiveConsultationUsers(question.slug, Country("DE")))
      .thenReturn(Future.successful(List(defaultUser, defaultUser.copy(userId = "12345"))))
    when(persistentCrmUserService.getActiveConsultationUsers(question.slug, Country("LU")))
      .thenReturn(Future.successful(List(defaultUser, defaultUser.copy(userId = "23456"))))
    when(operationService.findOne(OperationId("foo")))
      .thenReturn(
        Future.successful(
          Some(Operation(OperationId("foo"), "slug", List.empty, Seq.empty, OperationKind.GreatCause, None, None))
        )
      )

    Scenario("Sending vote only notice") {
      whenReady(sendMailPublisherService.sendVoteOnlyNotice(requestContext))(_ shouldBe ())
    }
  }

  Feature("publish email events") {

    for {
      (kind, call, user, tests) <- Seq(
        (
          CrmTemplateKind.Welcome,
          sendMailPublisherService.publishWelcome(_, _),
          user,
          Seq(Map("registration_context" -> "unknown"), Map("registration_context" -> "question-slug"))
        ),
        (
          CrmTemplateKind.Registration,
          sendMailPublisherService.publishRegistration(_, _),
          user.copy(verificationToken = Some("verification")),
          Seq(
            Map(
              "email_validation_url" -> "/FR/account-activation/userId/verification?country=FR&utm_content=cta&utm_campaign=core&utm_medium=email&utm_term=validation&utm_source=crm&question="
            ),
            Map(
              "email_validation_url" -> "/FR/account-activation/userId/verification?country=DE&utm_content=cta&utm_campaign=question-slug&utm_medium=email&utm_term=validation&utm_source=crm&question=questionId"
            )
          )
        ),
        (
          CrmTemplateKind.ResendRegistration,
          sendMailPublisherService.resendRegistration(_, _),
          user.copy(verificationToken = Some("verification")),
          Seq(
            Map(
              "email_validation_url" -> "/FR/account-activation/userId/verification?country=FR&utm_content=cta&utm_campaign=core&utm_medium=email&utm_term=validation&utm_source=crm&question="
            ),
            Map(
              "email_validation_url" -> "/FR/account-activation/userId/verification?country=DE&utm_content=cta&utm_campaign=question-slug&utm_medium=email&utm_term=validation&utm_source=crm&question=questionId"
            )
          )
        ),
        (
          CrmTemplateKind.ForgottenPassword,
          sendMailPublisherService.publishForgottenPassword(_, _),
          user.copy(resetToken = Some("reset")),
          Seq(
            Map("forgotten_password_url" -> "/FR/password-recovery/userId/reset"),
            Map("forgotten_password_url" -> "/FR/password-recovery/userId/reset")
          )
        ),
        (
          CrmTemplateKind.B2BForgottenPassword,
          sendMailPublisherService.publishForgottenPasswordOrganisation(_, _),
          organisation.copy(resetToken = Some("reset")),
          Seq(
            Map("forgotten_password_url" -> "/BE/password-recovery/organisationId/reset"),
            Map("forgotten_password_url" -> "/BE/password-recovery/organisationId/reset")
          )
        ),
        (
          CrmTemplateKind.B2BEmailChanged,
          sendMailPublisherService.publishEmailChanged(_, _, "newemail@example.com"),
          organisation,
          Seq(Map("email" -> "newemail@example.com"), Map("email" -> "newemail@example.com"))
        ),
        (
          CrmTemplateKind.B2BRegistration,
          sendMailPublisherService.publishRegistrationB2B(_, _),
          organisation.copy(resetToken = Some("reset")),
          Seq(
            Map("forgotten_password_url" -> "/BE/password-recovery/organisationId/reset"),
            Map("forgotten_password_url" -> "/BE/password-recovery/organisationId/reset")
          )
        ),
        (
          CrmTemplateKind.ProposalAccepted,
          (_: User, _: RequestContext) => sendMailPublisherService.publishAcceptProposal(ProposalId("proposalId")),
          user,
          Seq(
            Map(
              "proposal_url" -> "/DE/consultation/question-slug/proposal/proposalId/il-faut-tester-l-indexation-des-propositions?utm_source=crm-transac&utm_content=cta_share&utm_campaign=question-slug&utm_medium=email&utm_term=publication",
              "sequence_url" -> "/DE/consultation/question-slug/selection?introCard=false&utm_source=crm-transac&utm_content=cta&utm_campaign=question-slug&utm_medium=email&utm_term=publication",
              "is_anonymous_proposal" -> "false"
            ),
            Map(
              "proposal_url" -> "/DE/consultation/question-slug/proposal/proposalId/il-faut-tester-l-indexation-des-propositions?utm_source=crm-transac&utm_content=cta_share&utm_campaign=question-slug&utm_medium=email&utm_term=publication",
              "sequence_url" -> "/DE/consultation/question-slug/selection?introCard=false&utm_source=crm-transac&utm_content=cta&utm_campaign=question-slug&utm_medium=email&utm_term=publication",
              "is_anonymous_proposal" -> "false"
            )
          )
        ),
        (
          CrmTemplateKind.ProposalRefused,
          (_: User, _: RequestContext) => sendMailPublisherService.publishRefuseProposal(ProposalId("proposalId")),
          user,
          Seq(
            Map(
              "refusal_reason" -> "gibberish",
              "sequence_url" -> "/DE/consultation/question-slug/selection?introCard=false&utm_source=crm-transac&utm_content=cta&utm_campaign=question-slug&utm_medium=email&utm_term=refus"
            ),
            Map(
              "refusal_reason" -> "gibberish",
              "sequence_url" -> "/DE/consultation/question-slug/selection?introCard=false&utm_source=crm-transac&utm_content=cta&utm_campaign=question-slug&utm_medium=email&utm_term=refus"
            )
          )
        ),
        (
          CrmTemplateKind.B2BProposalAccepted,
          (_: User, _: RequestContext) => sendMailPublisherService.publishAcceptProposal(ProposalId("B2BProposalId")),
          organisation,
          Seq(
            Map(
              "proposal_url" -> "/LU/consultation/question-slug/proposal/B2BProposalId/il-faut-tester-l-indexation-des-propositions?utm_source=crm-transac&utm_content=cta_share&utm_campaign=question-slug&utm_medium=email&utm_term=publicationacteur",
              "sequence_url" -> "/LU/consultation/question-slug/selection?introCard=false&utm_source=crm-transac&utm_content=cta&utm_campaign=question-slug&utm_medium=email&utm_term=publicationacteur"
            ),
            Map(
              "proposal_url" -> "/LU/consultation/question-slug/proposal/B2BProposalId/il-faut-tester-l-indexation-des-propositions?utm_source=crm-transac&utm_content=cta_share&utm_campaign=question-slug&utm_medium=email&utm_term=publicationacteur",
              "sequence_url" -> "/LU/consultation/question-slug/selection?introCard=false&utm_source=crm-transac&utm_content=cta&utm_campaign=question-slug&utm_medium=email&utm_term=publicationacteur"
            )
          )
        ),
        (
          CrmTemplateKind.MessageToProposer,
          (user: User, requestContext: RequestContext) =>
            sendMailPublisherService
              .sendEmailToProposer(ProposalId("B2BProposalId"), user.userId, evilHtml, true, requestContext),
          organisation,
          Seq(
            Map(
              "firstname" -> "Joe",
              "question" -> "How to ask a question ?",
              "proposal" -> "Il faut tester l'indexation des propositions",
              "body" -> """|<p>coucou<br></p>
                           |<p>Guten tag</p>""".stripMargin
            ),
            Map(
              "firstname" -> "Joe",
              "question" -> "How to ask a question ?",
              "proposal" -> "Il faut tester l'indexation des propositions",
              "body" -> """|<p>coucou<br></p>
                           |<p>Guten tag</p>""".stripMargin
            )
          )
        ),
        (
          CrmTemplateKind.MessageToAbusiveWarn,
          (user: User, requestContext: RequestContext) =>
            sendMailPublisherService.publishAbusiveProposerWarn(question.questionId, user.userId, requestContext),
          organisation,
          Seq(
            Map("firstname" -> "Joe", "question" -> "How to ask a question ?"),
            Map("firstname" -> "Joe", "question" -> "How to ask a question ?")
          )
        ),
        (
          CrmTemplateKind.B2BProposalRefused,
          (_: User, _: RequestContext) => sendMailPublisherService.publishRefuseProposal(ProposalId("B2BProposalId")),
          organisation,
          Seq(
            Map(
              "refusal_reason" -> "invalid",
              "sequence_url" -> "/LU/consultation/question-slug/selection?introCard=false&utm_source=crm-transac&utm_content=cta&utm_campaign=question-slug&utm_medium=email&utm_term=refusacteur"
            ),
            Map(
              "refusal_reason" -> "invalid",
              "sequence_url" -> "/LU/consultation/question-slug/selection?introCard=false&utm_source=crm-transac&utm_content=cta&utm_campaign=question-slug&utm_medium=email&utm_term=refusacteur"
            )
          )
        )
      )
      (variables, (label, context)) <- tests.zip(Seq(("empty", RequestContext.empty), ("question", requestContext)))
    } {
      Scenario(s"${kind.entryName} with $label context") {
        Mockito.clearInvocations(eventBusService)
        whenReady(call(user, context)) { _ =>
          val captor = ArgumentCaptor.forClass[SendMessages, SendMessages](classOf[SendMessages])
          verify(eventBusService).publish(captor.capture())
          val event = captor.getValue
          event.messages.head.templateId shouldBe Some(CrmTemplateKind.values.indexOf(kind))
          variables.foreach {
            case (key, value) => event.messages.head.variables.flatMap(_.get(key)) shouldBe Some(value)
          }
        }
      }
    }
  }

  Feature("ProposalReportedNotice publication") {

    Scenario("send proposal report notice") {

      val call =
        sendMailPublisherService
          .publishProposalReportedNotice(userProposal.proposalId, Language("en"), ProposalReportReason.Inintelligible)
      val expectedVariables = Map(
        "reason" -> "Inintelligible",
        "proposal_id" -> userProposal.proposalId.value,
        "proposal_language" -> "en",
        "proposal_text" -> "Il faut tester l'indexation des propositions",
        "proposal_url" -> s"/#/moderation/proposals/${userProposal.proposalId.value}"
      )

      Mockito.clearInvocations(eventBusService)
      whenReady(call) { _ =>
        val captor = ArgumentCaptor.forClass[SendMessages, SendMessages](classOf[SendMessages])
        verify(eventBusService).publish(captor.capture())
        captor.getValue.messages.head.variables shouldBe Some(expectedVariables)
      }
    }
  }

  Feature("sendEmailToProposer errors") {

    val fakeProposalId = ProposalId("fake")
    val refusedProposalId = ProposalId("refused")
    val fakeProposer = ProposalId("fake-proposer-proposalId")
    val hardBounceProposer = ProposalId("hard-bounce-proposer-proposalId")
    val noQuestion = ProposalId("no-question")
    val fakeQuestion = ProposalId("fake-question")
    val noTemplate = ProposalId("no-template")
    when(proposalService.getProposalById(fakeProposalId, RequestContext.empty)).thenReturn(Future.successful(None))
    when(proposalService.getProposalById(refusedProposalId, RequestContext.empty))
      .thenReturn(Future.successful(Some(indexedProposal(refusedProposalId, status = ProposalStatus.Refused))))
    when(proposalService.getProposalById(fakeProposer, RequestContext.empty))
      .thenReturn(Future.successful(Some(indexedProposal(fakeProposer, userId = UserId("fake-proposer")))))
    when(proposalService.getProposalById(hardBounceProposer, RequestContext.empty))
      .thenReturn(Future.successful(Some(indexedProposal(hardBounceProposer, userId = UserId("hard-bounce-proposer")))))
    when(proposalService.getProposalById(noQuestion, RequestContext.empty))
      .thenReturn(Future.successful(Some(indexedProposal(noQuestion).copy(question = None))))
    when(proposalService.getProposalById(fakeQuestion, RequestContext.empty))
      .thenReturn(Future.successful(Some(indexedProposal(fakeQuestion, questionId = QuestionId("fake")))))
    val noTemplateQuestion = QuestionId("no-template-question")
    when(proposalService.getProposalById(noTemplate, RequestContext.empty))
      .thenReturn(Future.successful(Some(indexedProposal(noTemplate, questionId = noTemplateQuestion))))

    when(userService.getUser(UserId("fake-proposer"))).thenReturn(Future.successful(None))
    when(userService.getUser(UserId("hard-bounce-proposer")))
      .thenReturn(Future.successful(Some(user(id = UserId("hard-bounce-proposer"), isHardBounce = true))))
    when(userService.getUser(UserId("fake-modo"))).thenReturn(Future.successful(None))

    when(questionService.getQuestion(QuestionId("fake"))).thenReturn(Future.successful(None))
    when(questionService.getQuestion(noTemplateQuestion))
      .thenReturn(Future.successful(Some(question(noTemplateQuestion))))

    when(crmTemplatesService.find(MessageToProposer, Some(noTemplateQuestion), None))
      .thenReturn(Future.successful(None))

    val proposalNotFound =
      sendMailPublisherService.sendEmailToProposer(fakeProposalId, user.userId, evilHtml, true, RequestContext.empty)
    val proposerNotFound =
      sendMailPublisherService.sendEmailToProposer(fakeProposer, user.userId, evilHtml, true, RequestContext.empty)
    val modoNotFound = sendMailPublisherService.sendEmailToProposer(
      ProposalId("B2BProposalId"),
      UserId("fake-modo"),
      evilHtml,
      true,
      RequestContext.empty
    )
    val proposalQuestionNotFound = sendMailPublisherService.sendEmailToProposer(
      ProposalId("no-question"),
      user.userId,
      evilHtml,
      true,
      RequestContext.empty
    )
    val questionNotFound =
      sendMailPublisherService.sendEmailToProposer(fakeQuestion, user.userId, evilHtml, true, RequestContext.empty)
    val templateNotFound =
      sendMailPublisherService.sendEmailToProposer(noTemplate, user.userId, evilHtml, true, RequestContext.empty)

    for {
      (call, exceptionMessage) <- Seq(
        (proposalNotFound, "Proposal fake not found"),
        (proposerNotFound, "User fake-proposer not found"),
        (modoNotFound, "User fake-modo not found"),
        (proposalQuestionNotFound, "Proposal ProposalId(no-question)'s question no longer exists"),
        (questionNotFound, "Question QuestionId(fake) does not exist"),
        (templateNotFound, "Template for no-template-question and country FR does not exist")
      )
    } yield {
      Scenario(exceptionMessage) {
        whenReady(call.failed, Timeout(3.seconds)) { exception =>
          exception should be(a[ValueNotFoundException])
          exception.getMessage should be(exceptionMessage)
        }
      }
    }

    val proposerIsHardBounce =
      sendMailPublisherService.sendEmailToProposer(
        hardBounceProposer,
        user.userId,
        evilHtml,
        true,
        RequestContext.empty
      )
    val proposalNotAccepted =
      sendMailPublisherService.sendEmailToProposer(refusedProposalId, user.userId, evilHtml, true, RequestContext.empty)

    for {
      (call, key, errorMessage) <- Seq(
        (proposerIsHardBounce, "reachable", "User hard-bounce-proposer must not be hard bounce"),
        (proposalNotAccepted, "status", "Proposal must be Accepted")
      )
    } yield {
      Scenario(errorMessage) {
        whenReady(call.failed, Timeout(3.seconds)) { error =>
          error should be(a[ValidationFailedError])
          error should be(ValidationFailedError(Seq(ValidationError(key, "invalid_value", Some(errorMessage)))))
        }
      }
    }
  }
}
