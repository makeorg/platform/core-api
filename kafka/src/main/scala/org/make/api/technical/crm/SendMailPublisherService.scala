/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.technical.crm

import cats.implicits._
import io.netty.handler.codec.http.QueryStringEncoder
import org.make.api.ConfigComponent
import org.make.api.crmTemplates.CrmTemplatesServiceComponent
import org.make.api.extensions.MailJetTemplateConfigurationComponent
import org.make.api.operation.{OperationOfQuestionServiceComponent, OperationServiceComponent}
import org.make.api.proposal.{ProposalCoordinatorServiceComponent, ProposalServiceComponent}
import org.make.api.question.QuestionServiceComponent
import org.make.api.technical.EventBusServiceComponent
import org.make.api.technical.Futures.FutureOfOption
import org.make.api.technical.crm.DefaultSendMailPublisherServiceComponent.Utm
import org.make.api.technical.security.SecurityHelper
import org.make.api.user.UserServiceComponent
import org.make.core.BusinessConfig._
import org.make.core.operation.{Operation, OperationOfQuestion}
import org.make.core.sequence.SequenceKind
import org.make.core.crmTemplate.CrmTemplateKind._
import org.make.core.crmTemplate.{CrmTemplateKind, MonitoringCategory, TemplateId}
import org.make.core.proposal.{Proposal, ProposalId, ProposalReportReason, ProposalStatus}
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.Country
import org.make.core.user.{User, UserId, UserType}
import org.make.core._

import java.util.Locale
import java.time.format.DateTimeFormatter
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import org.make.core.reference.Language

trait SendMailPublisherServiceComponent {
  def sendMailPublisherService: SendMailPublisherService
}

trait SendMailPublisherService {
  def publishWelcome(user: User, requestContext: RequestContext): Future[Unit]

  def publishRegistration(user: User, requestContext: RequestContext): Future[Unit]

  def sendEmailToProposer(
    proposalId: ProposalId,
    senderId: UserId,
    sanitizedHtml: SanitizedHtml,
    dryRun: Boolean,
    requestContext: RequestContext
  ): Future[Unit]

  def publishRegistrationB2B(user: User, requestContext: RequestContext): Future[Unit]

  def publishForgottenPassword(user: User, requestContext: RequestContext): Future[Unit]

  def publishForgottenPasswordOrganisation(organisation: User, requestContext: RequestContext): Future[Unit]

  def publishEmailChanged(user: User, requestContext: RequestContext, newEmail: String): Future[Unit]

  def publishAcceptProposal(proposalId: ProposalId): Future[Unit]

  def publishRefuseProposal(proposalId: ProposalId): Future[Unit]

  def publishAbusiveProposerWarn(
    questionId: QuestionId,
    proposerId: UserId,
    requestContext: RequestContext
  ): Future[Unit]
  def publishAbusiveProposerBlock(
    questionId: QuestionId,
    proposerId: UserId,
    requestContext: RequestContext
  ): Future[Unit]

  def resendRegistration(user: User, requestContext: RequestContext): Future[Unit]

  def sendVoteOnlyNotice(context: RequestContext): Future[Unit]

  def sendVoteOnlyTest(user: UserId, context: RequestContext): Future[Unit]

  def publishProposalReportedNotice(
    proposalId: ProposalId,
    proposalLanguage: Language,
    reason: ProposalReportReason
  ): Future[Unit]
}

trait DefaultSendMailPublisherServiceComponent
    extends SendMailPublisherServiceComponent
    with MailJetTemplateConfigurationComponent
    with EventBusServiceComponent {
  this: UserServiceComponent
    with ProposalCoordinatorServiceComponent
    with ProposalServiceComponent
    with QuestionServiceComponent
    with OperationServiceComponent
    with CrmClientComponent
    with PersistentCrmUserServiceComponent
    with CrmTemplatesServiceComponent
    with ConfigComponent
    with OperationOfQuestionServiceComponent =>

  private def buildUrl(base: String, path: String, maybeUtm: Option[Utm], others: (String, String)*): String = {
    val builder = new QueryStringEncoder(path)
    (maybeUtm
      .fold(Map.empty[String, String])(
        utm =>
          Map(
            "utm_source" -> utm.source,
            "utm_medium" -> utm.medium,
            "utm_campaign" -> utm.campaign,
            "utm_term" -> utm.term,
            "utm_content" -> utm.content
          )
      ) ++ others.toMap).foreachEntry(builder.addParam)
    s"$base/${builder.toString}"
  }

  private def adaptUtmTerm(term: String, userType: UserType): String = {
    if (userType != UserType.UserTypeUser) {
      s"${term}acteur"
    } else {
      term
    }
  }

  private def getProposalUrl(proposal: Proposal, question: Question, maybeUtm: Option[Utm]): String = {
    val country: String = proposal.creationContext.country.getOrElse(question.countries.head).value

    buildUrl(
      base = mailJetTemplateConfiguration.mainFrontendUrl,
      path = s"$country/consultation/${question.slug}/proposal/${proposal.proposalId.value}/${proposal.slug}",
      maybeUtm
    )
  }

  private def getAccountValidationUrl(
    user: User,
    verificationToken: String,
    requestContext: RequestContext,
    utmCampaign: String
  ): String = {
    val country: String = requestContext.country.map(_.value).getOrElse("FR")
    val questionIdValue: String = requestContext.questionContext.questionId.map(_.value).getOrElse("")

    buildUrl(
      base = mailJetTemplateConfiguration.mainFrontendUrl,
      path = s"${user.country.value}/account-activation/${user.userId.value}/$verificationToken",
      maybeUtm = Some(Utm(campaign = utmCampaign, term = "validation", content = "cta")),
      "country" -> country,
      "question" -> questionIdValue
    )
  }

  private def getForgottenPasswordUrl(
    user: User,
    resetToken: String,
    baseApplicationName: Option[ApplicationName]
  ): String = {
    val appPath = s"password-recovery/${user.userId.value}/$resetToken"

    val (base, path) = baseApplicationName match {
      case Some(ApplicationName.Backoffice) =>
        (mailJetTemplateConfiguration.backofficeUrl, s"#/$appPath")
      case _ =>
        (mailJetTemplateConfiguration.mainFrontendUrl, s"${user.country}/$appPath")
    }

    buildUrl(base = base, path = path, maybeUtm = None)
  }

  private def sequenceUrlForProposal(questionSlug: String, proposal: Proposal, maybeUtm: Option[Utm]): String = {
    val country = proposal.creationContext.country.fold("FR")(_.value)

    buildUrl(
      base = mailJetTemplateConfiguration.mainFrontendUrl,
      path = s"$country/consultation/$questionSlug/selection",
      maybeUtm,
      "introCard" -> "false"
    )
  }

  private def buildSequenceUrl(
    questionSlug: String,
    sequenceKind: SequenceKind,
    country: Country,
    userType: Option[UserType] = None
  ): String = {
    val suffix = sequenceKind match {
      case SequenceKind.Consensus   => "-popular"
      case SequenceKind.Controversy => "-controversial"
      case _                        => ""
    }
    buildUrl(
      base = mailJetTemplateConfiguration.mainFrontendUrl,
      path = s"${country.value}/consultation/$questionSlug/selection$suffix",
      maybeUtm =
        Some(Utm(content = "cta", campaign = questionSlug, term = userType.fold("voteonly")(adaptUtmTerm("", _))))
    )
  }

  private def getUtmCampaignFromQuestionId(questionId: Option[QuestionId]): Future[String] = {
    questionId match {
      case Some(QuestionId("")) => Future.successful("unknown")
      case Some(id)             => questionService.getQuestion(id).map(_.fold("unknown")(_.slug))
      case None                 => Future.successful("core")
    }
  }

  def resolveQuestionSlug(country: Country, requestContext: RequestContext): Future[String] = {
    requestContext.questionContext.questionId
      .map(questionService.getQuestion)
      .orElse {
        requestContext.operationId.map(
          operationId =>
            questionService
              .findQuestion(maybeOperationId = Some(operationId), country = country, language = country.language)
        )
      } match {
      case Some(futureMaybeQuestion) => futureMaybeQuestion.map(_.map(_.slug).getOrElse("unknown"))
      case None                      => Future.successful("unknown")
    }
  }

  private def generateEmail(
    recipient: User,
    templateId: TemplateId,
    monitoringCategory: String,
    variables: Map[String, String]
  ): SendMessages = {
    SendMessages(
      SendEmail.create(
        templateId = Some(templateId.value.toInt),
        recipients = Seq(Recipient(email = recipient.email, name = recipient.fullName)),
        variables = Some(variables),
        customCampaign = None,
        monitoringCategory = Some(monitoringCategory)
      )
    )
  }

  override lazy val sendMailPublisherService: SendMailPublisherService = new DefaultSendMailPublisherService

  class DefaultSendMailPublisherService extends SendMailPublisherService {
    override def publishWelcome(user: User, requestContext: RequestContext): Future[Unit] = {
      val questionId = requestContext.questionContext.questionId

      resolveQuestionSlug(user.country, requestContext).flatMap { questionSlug =>
        crmTemplatesService
          .find(Welcome, questionId, user.profile.map(_.crmLanguage))
          .map(_.foreach { templateId =>
            val variables =
              Map(
                "firstname" -> user.firstName.getOrElse(""),
                "registration_context" -> questionSlug,
                "operation" -> requestContext.operationId.map(_.value).getOrElse(""),
                "question" -> requestContext.questionContext.question.getOrElse(""),
                "location" -> requestContext.location.getOrElse(""),
                "source" -> requestContext.source.getOrElse("")
              )
            eventBusService.publish(generateEmail(user, templateId, MonitoringCategory.welcome, variables))
          })
      }
    }

    private def localDateFormatter(country: Country): DateTimeFormatter =
      DateTimeFormatter
        .ofPattern("d MMMM yyyy")
        .withLocale(new Locale(country.value.toLowerCase))

    private val MailjetMaximumBatchSize = 50

    private def makeVoteOnlyVariables(
      country: Country,
      operation: Operation,
      opQuestion: OperationOfQuestion,
      question: Question,
      language: Language
    ): Map[String, String] =
      Map(
        "operation_header" -> opQuestion.consultationHeader.getOrElse(""),
        "operation_header_alt" -> opQuestion.consultationHeaderAlts
          .flatMap(_.getTranslation(language))
          .fold("")(_.value),
        "proposal_count" -> opQuestion.proposalsCount.toString,
        "vote_count" -> opQuestion.votesCount.toString,
        "end_date" -> localDateFormatter(country).format(opQuestion.endDate),
        "sequence_url" -> buildSequenceUrl(question.slug, SequenceKind.Standard, country),
        "controversial_sequence_url" -> buildSequenceUrl(question.slug, SequenceKind.Controversy, country),
        "popular_sequence_url" -> buildSequenceUrl(question.slug, SequenceKind.Consensus, country),
        "question" -> question.questions.getTranslationUnsafe(language).value,
        "operation_title" -> opQuestion.operationTitles
          .getTranslationUnsafe(language),
        "operation_type" -> operation.operationKind.value.toLowerCase,
        "partners_logos" -> opQuestion.partnersLogos.getOrElse(""),
        "partners_logos_alt" -> opQuestion.partnersLogosAlt.fold("")(_.value),
        "initiators_logos" -> opQuestion.initiatorsLogos.getOrElse(""),
        "initiators_logos_alt" -> opQuestion.initiatorsLogosAlt.fold("")(_.value)
      )

    override def sendVoteOnlyNotice(context: RequestContext): Future[Unit] =
      context.questionContext.questionId match {
        case None => Future.failed(new Exception("QuestionId missing"))
        case Some(questionId) =>
          (
            questionService
              .getQuestion(questionId)
              .flattenOrFail(s"Could not find question $questionId"),
            operationOfQuestionService
              .findByQuestionId(questionId)
              .flattenOrFail(s"Could not find operation for question $questionId")
          ).mapN(
              (question: Question, opQuestion: OperationOfQuestion) =>
                operationService
                  .findOne(opQuestion.operationId)
                  .flattenOrFail(s"Couldn't find operation ${opQuestion.operationId}")
                  .flatMap(
                    operation =>
                      question.countries
                        .traverse_(
                          country =>
                            persistentCrmUserService
                              .getActiveConsultationUsers(question.slug, country)
                              .flatMap(users => {
                                users
                                  .groupBy(_.favoriteLanguage.fold(Language("fr"))(Language(_)))
                                  .toList
                                  .traverse_({
                                    case (language, users) =>
                                      crmTemplatesService
                                        .find(VoteOnlyNotice, Some(question.questionId), Some(language))
                                        .flattenOrFail(s"Template ${VoteOnlyNotice.entryName} for $language not found")
                                        .flatMap(template => {
                                          val variables =
                                            makeVoteOnlyVariables(country, operation, opQuestion, question, language)
                                          val mailjetVariables = MailjetGlobalsVariables(
                                            templateId = Some(template.value.toInt),
                                            variables = Some(variables),
                                            monitoringCategory = Some(MonitoringCategory.account)
                                          )
                                          users
                                            .sliding(MailjetMaximumBatchSize, MailjetMaximumBatchSize)
                                            .map(batch => {
                                              SendMessages(
                                                id = SecurityHelper.anonymizeEmail(batch.headOption.fold("-")(_.email)),
                                                globals = Some(mailjetVariables),
                                                messages = batch.map(
                                                  c =>
                                                    SendEmail
                                                      .create(recipients = Seq(Recipient(c.email, Some(c.fullName))))
                                                ),
                                                sandboxMode = None
                                              )
                                            })
                                            .toList
                                            .traverse_(messages => Future(eventBusService.publish(messages)))
                                        })
                                  })
                              })
                        )
                  )
            )
            .flatten
      }

    override def sendVoteOnlyTest(userId: UserId, context: RequestContext): Future[Unit] =
      context.questionContext.questionId match {
        case None => Future.failed(new Exception("QuestionId missing"))
        case Some(questionId) =>
          (
            userService.getUser(userId).flattenOrFail(s"Could not find user $userId"),
            questionService
              .getQuestion(questionId)
              .flattenOrFail(s"Could not find question $questionId"),
            operationOfQuestionService
              .findByQuestionId(questionId)
              .flattenOrFail(s"Could not find operation for question $questionId")
          ).mapN(
            (user: User, question: Question, opQuestion: OperationOfQuestion) =>
              operationService
                .findOne(opQuestion.operationId)
                .flattenOrFail(s"Couldn't find operation ${opQuestion.operationId}")
                .flatMap(operation => {
                  question.languages.traverse_(
                    language =>
                      crmTemplatesService
                        .find(VoteOnlyNotice, Some(questionId), Some(language))
                        .flattenOrFail(s"Template ${VoteOnlyNotice.entryName} for $language not found")
                        .flatMap(templateId => {
                          val country = question.countries.head
                          val variables = makeVoteOnlyVariables(country, operation, opQuestion, question, language)
                          val mailjetVariables = MailjetGlobalsVariables(
                            templateId = Some(templateId.value.toInt),
                            variables = Some(variables),
                            monitoringCategory = Some(MonitoringCategory.account)
                          )
                          Future {
                            eventBusService.publish(
                              SendMessages(
                                id = SecurityHelper.anonymizeEmail(user.email),
                                globals = Some(mailjetVariables),
                                messages =
                                  List(SendEmail.create(recipients = Seq(Recipient(user.email, user.fullName)))),
                                sandboxMode = None
                              )
                            )
                          }
                        })
                  )
                })
          )
      }

    override def publishRegistration(user: User, requestContext: RequestContext): Future[Unit] = {
      val questionId = requestContext.questionContext.questionId

      user.verificationToken match {
        case Some(verificationToken) =>
          crmTemplatesService.find(Registration, questionId, user.profile.map(_.crmLanguage)).flatMap {
            case Some(templateId) =>
              getUtmCampaignFromQuestionId(questionId).map { utmCampaign =>
                eventBusService.publish(
                  SendMessages(
                    SendEmail.create(
                      templateId = Some(templateId.value.toInt),
                      recipients = Seq(Recipient(email = user.email, name = user.fullName)),
                      variables = Some(
                        Map(
                          "firstname" -> user.firstName.getOrElse(""),
                          "email_validation_url" -> getAccountValidationUrl(
                            user,
                            verificationToken,
                            requestContext,
                            utmCampaign
                          ),
                          "operation" -> requestContext.operationId.map(_.value).getOrElse(""),
                          "question" -> requestContext.questionContext.question.getOrElse(""),
                          "location" -> requestContext.location.getOrElse(""),
                          "source" -> requestContext.source.getOrElse("")
                        )
                      ),
                      customCampaign = None,
                      monitoringCategory = Some(MonitoringCategory.account)
                    )
                  )
                )
              }
            case None => Future.unit
          }
        case _ =>
          Future.failed(
            new IllegalStateException(s"verification token required but not provided for user ${user.userId.value}")
          )
      }
    }

    def publishResendRegistration(user: User, requestContext: RequestContext): Future[Unit] = {
      val questionId = requestContext.questionContext.questionId

      user.verificationToken match {
        case Some(verificationToken) =>
          crmTemplatesService.find(ResendRegistration, questionId, user.profile.map(_.crmLanguage)).flatMap {
            case Some(templateId) =>
              getUtmCampaignFromQuestionId(questionId).map { utmCampaign =>
                eventBusService.publish(
                  SendMessages(
                    SendEmail.create(
                      templateId = Some(templateId.value.toInt),
                      recipients = Seq(Recipient(email = user.email, name = user.fullName)),
                      variables = Some(
                        Map(
                          "firstname" -> user.firstName.getOrElse(""),
                          "email_validation_url" -> getAccountValidationUrl(
                            user,
                            verificationToken,
                            requestContext,
                            utmCampaign
                          )
                        )
                      ),
                      customCampaign = None,
                      monitoringCategory = Some(MonitoringCategory.account)
                    )
                  )
                )
              }
            case None => Future.unit
          }
        case _ =>
          Future.failed(
            new IllegalStateException(s"verification token required but not provided for user ${user.userId.value}")
          )
      }
    }

    override def publishForgottenPassword(user: User, requestContext: RequestContext): Future[Unit] = {
      val questionId = requestContext.questionContext.questionId

      user.resetToken match {
        case Some(resetToken) =>
          crmTemplatesService
            .find(ForgottenPassword, questionId, user.profile.map(_.crmLanguage))
            .map(_.foreach { templateId =>
              eventBusService.publish(
                SendMessages(
                  SendEmail.create(
                    templateId = Some(templateId.value.toInt),
                    recipients = Seq(Recipient(email = user.email, name = user.fullName)),
                    variables = Some(
                      Map(
                        "firstname" -> user.firstName.getOrElse(""),
                        "forgotten_password_url" -> getForgottenPasswordUrl(
                          user,
                          resetToken,
                          requestContext.applicationName
                        ),
                        "operation" -> requestContext.operationId.map(_.value).getOrElse(""),
                        "question" -> requestContext.questionContext.question.getOrElse(""),
                        "location" -> requestContext.location.getOrElse(""),
                        "source" -> requestContext.source.getOrElse("")
                      )
                    ),
                    customCampaign = None,
                    monitoringCategory = Some(MonitoringCategory.account)
                  )
                )
              )
            })
        case _ =>
          Future.failed(
            new IllegalStateException(s"reset token required but not provided for user ${user.userId.value}")
          )
      }
    }

    override def publishForgottenPasswordOrganisation(
      organisation: User,
      requestContext: RequestContext
    ): Future[Unit] = {
      val questionId = requestContext.questionContext.questionId

      organisation.resetToken match {
        case Some(resetToken) =>
          crmTemplatesService
            .find(B2BForgottenPassword, questionId, organisation.profile.map(_.crmLanguage))
            .map(_.foreach { templateId =>
              eventBusService.publish(
                SendMessages(
                  SendEmail.create(
                    templateId = Some(templateId.value.toInt),
                    recipients = Seq(Recipient(email = organisation.email, name = organisation.fullName)),
                    variables = Some(
                      Map(
                        "forgotten_password_url" -> getForgottenPasswordUrl(
                          organisation,
                          resetToken,
                          requestContext.applicationName
                        ),
                        "operation" -> requestContext.operationId.map(_.value).getOrElse(""),
                        "question" -> requestContext.questionContext.question.getOrElse(""),
                        "location" -> requestContext.location.getOrElse(""),
                        "source" -> requestContext.source.getOrElse("")
                      )
                    ),
                    customCampaign = None,
                    monitoringCategory = Some(MonitoringCategory.account)
                  )
                )
              )
            })
        case _ =>
          Future.failed(
            new IllegalStateException(
              s"reset token required but not provided for organisation ${organisation.userId.value}"
            )
          )
      }
    }

    override def publishEmailChanged(user: User, requestContext: RequestContext, newEmail: String): Future[Unit] = {
      val questionId = requestContext.questionContext.questionId

      crmTemplatesService
        .find(B2BEmailChanged, questionId, user.profile.map(_.crmLanguage))
        .map(_.foreach { templateId =>
          eventBusService.publish(generateEmail(user, templateId, MonitoringCategory.account, Map("email" -> newEmail)))
        })
    }

    private def publishModerationEmail(
      proposalId: ProposalId,
      variables: (Question, OperationOfQuestion, User, Proposal) => Map[String, String],
      templateKind: UserType                                     => CrmTemplateKind
    ): Future[Unit] = {

      val publishSendEmail = for {
        proposal <- proposalCoordinatorService
          .getProposal(proposalId)
          .flattenOrFail(s"Proposal ${proposalId.value} not found")
        user <- userService
          .getUser(proposal.author)
          .flattenOrFail(s"user ${proposal.author.value}, author of proposal ${proposalId.value} not found")
        questionId <- Future
          .successful(proposal.questionId)
          .flattenOrFail(s"proposal ${proposal.proposalId} doesn't have a question!")
        question <- questionService
          .getQuestion(questionId)
          .flattenOrFail(
            s"question ${proposal.questionId.fold("''")(_.value)} not found, it is on proposal ${proposal.proposalId}"
          )
        operationOfQuestion <- operationOfQuestionService
          .findByQuestionId(questionId)
          .flattenOrFail(
            s"question ${proposal.questionId.fold("''")(_.value)} not found, it is on proposal ${proposal.proposalId}"
          )
        templateId <- crmTemplatesService
          .find(templateKind(user.userType), Some(questionId), user.profile.map(_.crmLanguage))
          .flattenOrFail(
            s"no $templateKind crm template for question ${questionId.value} and country ${user.country.value}"
          )
      } yield {
        eventBusService.publish(
          SendMessages(
            SendEmail.create(
              templateId = Some(templateId.value.toInt),
              recipients = Seq(Recipient(email = user.email, name = user.fullName)),
              variables = Some(variables(question, operationOfQuestion, user, proposal)),
              customCampaign = None,
              monitoringCategory = Some(MonitoringCategory.moderation)
            )
          )
        )
      }

      publishSendEmail.orRaise(
        new IllegalStateException(
          s"Something went wrong unexpectedly while trying to send moderation email for proposal ${proposalId.value}"
        )
      )
    }

    override def publishAcceptProposal(proposalId: ProposalId): Future[Unit] = {

      def variables(
        question: Question,
        operationOfQuestion: OperationOfQuestion,
        user: User,
        proposal: Proposal
      ): Map[String, String] = {
        val term = "publication"
        Map(
          "proposal_url" -> getProposalUrl(
            proposal,
            question,
            Some(
              Utm(
                campaign = question.slug,
                term = adaptUtmTerm("publication", user.userType),
                content = "cta_share",
                source = "crm-transac"
              )
            )
          ),
          "proposal_text" -> proposal.content,
          "firstname" -> user.firstName.getOrElse(""),
          "organisation_name" -> user.organisationName.getOrElse(""),
          "operation" -> question.operationId.map(_.value).getOrElse(""),
          "question" -> question.questionId.value,
          "location" -> proposal.creationContext.location.getOrElse(""),
          "source" -> proposal.creationContext.source.getOrElse(""),
          "sequence_url" -> sequenceUrlForProposal(
            question.slug,
            proposal,
            Some(
              Utm(
                content = "cta",
                campaign = question.slug,
                term = adaptUtmTerm(term, user.userType),
                source = "crm-transac"
              )
            )
          ),
          "is_anonymous_proposal" -> proposal.isAnonymous.toString,
          "is_consultation_vote_only" -> (!operationOfQuestion.canPropose).toString
        )
      }

      def kind(userType: UserType): CrmTemplateKind = {
        if (userType == UserType.UserTypeUser) {
          ProposalAccepted
        } else {
          B2BProposalAccepted
        }
      }

      publishModerationEmail(proposalId, variables, kind)

    }

    override def publishRefuseProposal(proposalId: ProposalId): Future[Unit] = {

      def variables(
        question: Question,
        operationOfQuestion: OperationOfQuestion,
        user: User,
        proposal: Proposal
      ): Map[String, String] = {
        val term = "refus"
        Map(
          "proposal_text" -> proposal.content,
          "refusal_reason" -> proposal.refusalReason.getOrElse(""),
          "firstname" -> user.firstName.getOrElse(""),
          "organisation_name" -> user.organisationName.getOrElse(""),
          "operation" -> question.operationId.map(_.value).getOrElse(""),
          "question" -> question.questions
            .getTranslation(user.profile.map(_.crmLanguage).getOrElse(question.defaultLanguage))
            .fold(question.slug)(_.value),
          "location" -> proposal.creationContext.location.getOrElse(""),
          "source" -> proposal.creationContext.source.getOrElse(""),
          "sequence_url" -> sequenceUrlForProposal(
            question.slug,
            proposal,
            Some(
              Utm(
                content = "cta",
                campaign = question.slug,
                term = adaptUtmTerm(term, user.userType),
                source = "crm-transac"
              )
            )
          ),
          "is_consultation_vote_only" -> (!operationOfQuestion.canPropose).toString
        )
      }

      def kind(userType: UserType): CrmTemplateKind = {
        if (userType == UserType.UserTypeUser) {
          ProposalRefused
        } else {
          B2BProposalRefused
        }
      }

      publishModerationEmail(proposalId, variables, kind)
    }

    override def resendRegistration(user: User, requestContext: RequestContext): Future[Unit] = {

      userService.changeEmailVerificationTokenIfNeeded(user.userId).flatMap {
        case Some(modifiedUser) => publishResendRegistration(modifiedUser, requestContext)
        case None               => Future.unit
      }
    }

    override def sendEmailToProposer(
      proposalId: ProposalId,
      senderId: UserId,
      sanitizedHtml: SanitizedHtml,
      dryRun: Boolean,
      requestContext: RequestContext
    ): Future[Unit] =
      for {
        proposal <- proposalService
          .getProposalById(proposalId, requestContext)
          .flattenOrFail(s"Proposal ${proposalId.value} not found")
        _ <- if (proposal.status == ProposalStatus.Accepted)
          Future.unit
        else
          Future.failed(
            ValidationFailedError(Seq(ValidationError("status", "invalid_value", Some("Proposal must be Accepted"))))
          )
        proposer <- userService
          .getUser(proposal.author.userId)
          .flattenOrFail(s"User ${proposal.author.userId.value} not found")
        _ <- if (!proposer.isHardBounce)
          Future.unit
        else
          Future.failed(
            ValidationFailedError(
              Seq(
                ValidationError(
                  "reachable",
                  "invalid_value",
                  Some(s"User ${proposer.userId.value} must not be hard bounce")
                )
              )
            )
          )
        recipient <- if (dryRun)
          userService
            .getUser(senderId)
            .flattenOrFail(s"User ${senderId.value} not found")
        else
          Future.successful(proposer)
        questionId <- Future
          .successful(proposal.question.map(_.questionId))
          .flattenOrFail(s"Proposal $proposalId's question no longer exists")
        question <- questionService
          .getQuestion(questionId)
          .flattenOrFail(s"Question $questionId does not exist")
        templateId <- crmTemplatesService
          .find(MessageToProposer, Some(questionId), proposer.profile.map(_.crmLanguage))
          .flattenOrFail(s"Template for ${questionId.value} and country ${recipient.country.value} does not exist")
      } yield {
        val variables = Map(
          "firstname" -> proposer.displayName.getOrElse(""),
          "question" -> question.questions
            .getTranslationUnsafe(requestContext.languageContext.language.getOrElse(question.defaultLanguage))
            .value,
          "proposal" -> proposal.content
            .getTranslationUnsafe(proposal.submittedAsLanguage.getOrElse(question.defaultLanguage)),
          "body" -> sanitizedHtml.html
        )
        eventBusService.publish(generateEmail(recipient, templateId, MonitoringCategory.moderation, variables))
      }

    private def publishWarningToAbusiveProposer(
      template: CrmTemplateKind,
      questionId: QuestionId,
      proposerId: UserId,
      languageOpt: Option[Language]
    ): Future[Unit] =
      for {
        proposer <- userService
          .getUser(proposerId)
          .flattenOrFail(s"User ${proposerId.value} not found")
        question <- questionService
          .getQuestion(questionId)
          .flattenOrFail(s"Question $questionId does not exist")
        templateId <- crmTemplatesService
          .find(template, Some(questionId), proposer.profile.map(_.crmLanguage))
          .flattenOrFail(s"Template for ${questionId.value} and country ${proposer.country.value} does not exist")
      } yield {
        val language = languageOpt.getOrElse(question.defaultLanguage)
        val variables =
          Map(
            "firstname" -> proposer.displayName.orElse(proposer.firstName).getOrElse(""),
            "question" -> question.questions.getTranslationUnsafe(language).value,
            "sequence_url" -> buildSequenceUrl(
              question.slug,
              SequenceKind.Standard,
              proposer.country,
              Some(proposer.userType)
            )
          )
        eventBusService.publish(generateEmail(proposer, templateId, MonitoringCategory.moderation, variables))
      }

    override def publishAbusiveProposerWarn(
      questionId: QuestionId,
      proposerId: UserId,
      requestContext: RequestContext
    ): Future[Unit] =
      publishWarningToAbusiveProposer(
        MessageToAbusiveWarn,
        questionId,
        proposerId,
        requestContext.languageContext.language
      )

    override def publishAbusiveProposerBlock(
      questionId: QuestionId,
      proposerId: UserId,
      requestContext: RequestContext
    ): Future[Unit] =
      publishWarningToAbusiveProposer(
        MessageToAbusiveBlock,
        questionId,
        proposerId,
        requestContext.languageContext.language
      )

    override def publishRegistrationB2B(user: User, requestContext: RequestContext): Future[Unit] = {
      user.resetToken match {
        case Some(resetToken) =>
          crmTemplatesService
            .find(B2BRegistration, requestContext.questionContext.questionId, user.profile.map(_.crmLanguage))
            .map(_.foreach { templateId =>
              eventBusService.publish(
                SendMessages(
                  SendEmail.create(
                    templateId = Some(templateId.value.toInt),
                    recipients = Seq(Recipient(email = user.email, name = user.fullName)),
                    variables = Some(
                      Map(
                        "mailto" -> user.email,
                        "forgotten_password_url" -> getForgottenPasswordUrl(
                          user,
                          resetToken,
                          Some(ApplicationName.MainFrontend)
                        )
                      )
                    ),
                    customCampaign = None,
                    monitoringCategory = Some(MonitoringCategory.account)
                  )
                )
              )
            })
        case _ =>
          Future.failed(
            new IllegalStateException(s"reset token required but not provided for user ${user.userId.value}")
          )
      }
    }

    override def publishProposalReportedNotice(
      proposalId: ProposalId,
      proposalLanguage: Language,
      reason: ProposalReportReason
    ): Future[Unit] =
      proposalCoordinatorService
        .getProposal(proposalId)
        .flattenOrFail(s"Proposal ${proposalId.value} not found")
        .flatMap(
          proposal =>
            Future(
              eventBusService.publish(
                SendMessages(
                  SendEmail.create(
                    templateId = Some(config.getInt("make-api.report-proposal.template-id")),
                    variables = Some(
                      Map(
                        "reason" -> reason.toString,
                        "proposal_id" -> proposalId.value,
                        "proposal_language" -> proposalLanguage.value,
                        "proposal_text" ->
                          proposal.contentTranslations
                            .flatMap(_.getTranslation(proposalLanguage))
                            .getOrElse(proposal.content),
                        "proposal_url" -> buildUrl(
                          mailJetTemplateConfiguration.backofficeUrl,
                          s"#/moderation/proposals/${proposalId.value}",
                          None
                        )
                      )
                    ),
                    recipients =
                      Seq(Recipient(config.getString("make-api.report-proposal.recipient"), Some("Moderators")))
                  )
                )
              )
            )
        )
  }
}

object DefaultSendMailPublisherServiceComponent {
  final case class Utm(
    source: String = "crm",
    medium: String = "email",
    campaign: String,
    term: String,
    content: String
  )
}
