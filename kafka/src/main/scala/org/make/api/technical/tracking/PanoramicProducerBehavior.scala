/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.technical.tracking

import akka.actor.typed.Behavior
import org.make.api.technical.KafkaProducerBehavior
import org.make.core.MakeSerializable
import org.make.core.technical.IdGenerator

class PanoramicProducerBehavior extends KafkaProducerBehavior[PanoramicEvent, PanoramicEventWrapper] {
  override protected val topicKey: String = PanoramicProducerBehavior.topicKey
  override protected def wrapEvent(event: PanoramicEvent): PanoramicEventWrapper = {
    PanoramicEventWrapper(
      version = MakeSerializable.V1,
      id = IdGenerator.uuidGenerator.nextId(),
      date = event.createdAt,
      eventType = "PanoramicEvent",
      event = event
    )
  }
}

object PanoramicProducerBehavior {
  def apply(): Behavior[PanoramicEvent] = new PanoramicProducerBehavior().createBehavior(name)
  val name: String = "panoramic-events-producer"
  val topicKey: String = "tracking-panoramic"
}
