/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.proposal

import com.typesafe.config.Config
import org.make.api.technical.ActorSystemComponent
import org.make.api.extensions.ConfigurationSupport
import akka.actor.typed.{ActorSystem, Extension, ExtensionId}

class MaxPropositionsThresholds(override protected val configuration: Config)
    extends Extension
    with ConfigurationSupport {

  val warnThreshold: Int = configuration.getInt("warn")
  val blockThreshold: Int = configuration.getInt("block")
}

object MaxPropositionsThresholds extends ExtensionId[MaxPropositionsThresholds] {

  override def createExtension(system: ActorSystem[_]): MaxPropositionsThresholds =
    new MaxPropositionsThresholds(system.settings.config.getConfig("make-api.user-proposals-thresholds"))
}

trait MaxPropositionsThresholdsComponent {
  def maxPropositionsThresholds: MaxPropositionsThresholds
}

trait DefaultMaxPropositionsThresholdsComponent extends MaxPropositionsThresholdsComponent {
  this: ActorSystemComponent =>

  override lazy val maxPropositionsThresholds: MaxPropositionsThresholds =
    MaxPropositionsThresholds(actorSystem)
}
