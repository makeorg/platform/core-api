/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.proposal

import akka.Done
import akka.stream.scaladsl.{Sink, Source}
import cats.data.NonEmptyList
import cats.implicits._
import com.sksamuel.elastic4s.requests.searches.sort.SortOrder
import grizzled.slf4j.Logging
import kamon.Kamon
import kamon.tag.TagSet
import org.make.api.idea.IdeaServiceComponent
import org.make.api.partner.PartnerServiceComponent
import org.make.api.proposal.ProposalActorResponse.VotesActorResponse
import org.make.api.question.{AuthorRequest, QuestionServiceComponent}
import org.make.api.segment.SegmentServiceComponent
import org.make.api.sequence.SequenceConfigurationComponent
import org.make.api.sessionhistory._
import org.make.api.tag.TagServiceComponent
import org.make.api.tagtype.TagTypeServiceComponent
import org.make.api.technical.crm.QuestionResolver
import org.make.api.technical.job.JobCoordinatorServiceComponent
import org.make.api.technical.security.{SecurityConfigurationComponent, SecurityHelper}
import org.make.api.technical._
import org.make.api.user.UserServiceComponent
import org.make.api.userhistory._
import org.make.core._
import org.make.core.common.indexed.Sort
import org.make.core.history.HistoryActions.VoteTrust._
import org.make.core.history.HistoryActions.{VoteAndQualifications, VoteTrust}
import org.make.core.idea.IdeaId
import org.make.core.job.Job.JobId.SubmittedAsLanguagePatch
import org.make.core.partner.Partner
import org.make.core.proposal.ProposalStatus.Pending
import org.make.core.proposal._
import org.make.core.proposal.indexed.{IndexedProposal, ProposalElasticsearchFieldName, ProposalsSearchResult}
import org.make.core.question.TopProposalsMode.IdeaMode
import org.make.core.question.{Question, QuestionId, TopProposalsMode}
import org.make.core.reference.{Country, Language}
import org.make.core.session.SessionId
import org.make.core.tag.{Tag, TagId, TagType}
import org.make.core.technical.Pagination.Offset
import org.make.core.technical.{Multilingual, Pagination}
import org.make.core.user._

import java.time.ZonedDateTime
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

trait DefaultProposalServiceComponent extends ProposalServiceComponent with CirceFormatters with Logging {
  this: ProposalServiceComponent
    with ActorSystemComponent
    with EventBusServiceComponent
    with IdeaServiceComponent
    with IdGeneratorComponent
    with PartnerServiceComponent
    with ProposalCoordinatorServiceComponent
    with ProposalSearchEngineComponent
    with QuestionServiceComponent
    with ReadJournalComponent
    with SecurityConfigurationComponent
    with SegmentServiceComponent
    with SequenceConfigurationComponent
    with SessionHistoryCoordinatorServiceComponent
    with JobCoordinatorServiceComponent
    with TagServiceComponent
    with TagTypeServiceComponent
    with UserHistoryCoordinatorServiceComponent
    with UserServiceComponent =>

  override lazy val proposalService: DefaultProposalService = new DefaultProposalService

  class DefaultProposalService extends ProposalService {

    override def createInitialProposal(
      content: String,
      contentTranslations: Option[Multilingual[String]],
      question: Question,
      country: Country,
      submittedAsLanguage: Language,
      isAnonymous: Boolean,
      tags: Seq[TagId],
      author: AuthorRequest,
      moderator: UserId,
      moderatorRequestContext: RequestContext
    ): Future[ProposalId] = {

      for {
        user           <- userService.retrieveOrCreateVirtualUser(author, country)
        requestContext <- buildRequestContext(user, country, question, moderatorRequestContext)
        proposalId <- propose(
          user,
          requestContext,
          DateHelper.now(),
          content,
          question,
          isAnonymous,
          initialProposal = true,
          submittedAsLanguage = submittedAsLanguage,
          proposalType = ProposalType.ProposalTypeInitial
        )
        _ <- validateProposal(
          proposalId = proposalId,
          moderator = moderator,
          requestContext = moderatorRequestContext,
          question = question,
          newContent = None,
          newContentTranslations = contentTranslations,
          sendNotificationEmail = false,
          tags = tags
        )
      } yield proposalId
    }

    override def createExternalProposal(
      content: String,
      contentTranslations: Option[Multilingual[String]],
      question: Question,
      country: Country,
      submittedAsLanguage: Language,
      isAnonymous: Boolean,
      externalUserId: String,
      author: AuthorRequest,
      moderator: UserId,
      moderatorRequestContext: RequestContext,
      lineNumber: Int
    ): Future[ProposalId] = {
      val email = s"${question.questionId.value}-$externalUserId@example.com"

      for {
        user           <- userService.retrieveOrCreateExternalUser(email, author, country, lineNumber)
        requestContext <- buildRequestContext(user, country, question, moderatorRequestContext)
        proposalId <- propose(
          user,
          requestContext,
          DateHelper.now(),
          content,
          question,
          isAnonymous,
          initialProposal = false,
          submittedAsLanguage = submittedAsLanguage,
          proposalType = ProposalType.ProposalTypeExternal
        )
        _ <- validateProposal(
          proposalId = proposalId,
          moderator = moderator,
          requestContext = moderatorRequestContext,
          question = question,
          newContent = None,
          newContentTranslations = contentTranslations,
          sendNotificationEmail = false,
          tags = Seq.empty
        )
      } yield proposalId
    }

    private def buildRequestContext(
      user: User,
      country: Country,
      question: Question,
      requestContext: RequestContext
    ): Future[RequestContext] = Future.successful(
      RequestContext.empty.copy(
        userId = Some(user.userId),
        country = Some(country),
        questionContext = RequestContextQuestion(questionId = Some(question.questionId)),
        languageContext = RequestContextLanguage(language = BusinessConfig.supportedCountries.collectFirst {
          case CountryConfiguration(`country`, language, _) => language
        }),
        operationId = question.operationId,
        applicationName = requestContext.applicationName
      )
    )

    override def searchProposalsVotedByUser(
      userId: UserId,
      filterVotes: Option[Seq[VoteKey]],
      filterQualifications: Option[Seq[QualificationKey]],
      preferredLanguage: Option[Language],
      sort: Option[Sort],
      limit: Option[Pagination.Limit],
      offset: Option[Pagination.Offset],
      requestContext: RequestContext
    ): Future[ProposalsResultResponse] = {
      val votedProposals: Future[Map[ProposalId, VoteAndQualifications]] =
        for {
          proposalIds <- userHistoryCoordinatorService.retrieveVotedProposals(
            RequestUserVotedProposals(userId = userId, filterVotes, filterQualifications)
          )
          withVote <- userHistoryCoordinatorService.retrieveVoteAndQualifications(userId, proposalIds)
        } yield withVote

      votedProposals.flatMap {
        case proposalIdsWithVotes if proposalIdsWithVotes.isEmpty =>
          Future.successful((0L, Seq.empty))
        case proposalIdsWithVotes =>
          val proposalIds: Seq[ProposalId] = proposalIdsWithVotes.toSeq.sortWith {
            case ((_, firstVotesAndQualifications), (_, nextVotesAndQualifications)) =>
              firstVotesAndQualifications.date.isAfter(nextVotesAndQualifications.date)
          }.map {
            case (proposalId, _) => proposalId
          }
          proposalService
            .searchForUser(
              Some(userId),
              SearchQuery(
                filters = Some(SearchFilters(proposal = Some(ProposalSearchFilter(proposalIds = proposalIds)))),
                sort = sort,
                limit = limit,
                offset = offset
              ),
              requestContext,
              preferredLanguage,
              questionDefaultLanguage = None
            )
            .map { proposalResultSeededResponse =>
              val proposalResult = proposalResultSeededResponse.results.sortWith {
                case (first, next) => proposalIds.indexOf(first.id) < proposalIds.indexOf(next.id)
              }
              (proposalResultSeededResponse.total, proposalResult)
            }
      }.map {
        case (total, proposalResult) =>
          ProposalsResultResponse(total = total, results = proposalResult)
      }
    }

    override def getProposalById(
      proposalId: ProposalId,
      requestContext: RequestContext
    ): Future[Option[IndexedProposal]] = {
      proposalCoordinatorService.viewProposal(proposalId, requestContext)
      elasticsearchProposalAPI.findProposalById(proposalId)
    }

    override def getProposalsById(
      proposalIds: Seq[ProposalId],
      requestContext: RequestContext
    ): Future[Seq[IndexedProposal]] = {
      Source(proposalIds).mapAsync(5)(id => getProposalById(id, requestContext)).runWith(Sink.seq).map(_.flatten)
    }

    override def getModerationProposalById(proposalId: ProposalId): Future[Option[ModerationProposalResponse]] = {
      toModerationProposalResponse(proposalCoordinatorService.getProposal(proposalId))
    }

    override def getEventSourcingProposal(
      proposalId: ProposalId,
      requestContext: RequestContext
    ): Future[Option[Proposal]] = {
      proposalCoordinatorService.viewProposal(proposalId, requestContext)
    }

    override def searchInIndex(query: SearchQuery, requestContext: RequestContext): Future[ProposalsSearchResult] = {
      val logSearchContent: Future[Unit] = query.filters.flatMap(_.content) match {
        case Some(contentFilter) =>
          sessionHistoryCoordinatorService.logTransactionalHistory(
            LogSessionSearchProposalsEvent(
              requestContext.sessionId,
              requestContext,
              SessionAction(
                DateHelper.now(),
                LogSessionSearchProposalsEvent.action,
                SessionSearchParameters(contentFilter.text)
              )
            )
          )
        case _ => Future.unit
      }
      logSearchContent >> elasticsearchProposalAPI.searchProposals(query)
    }

    override def modifyForQuestion(
      questionId: QuestionId,
      modifyFn: Proposal => Future[Unit],
      requestContext: RequestContext
    ): Unit = {
      val startTime: Long = System.currentTimeMillis()

      jobCoordinatorService.start(SubmittedAsLanguagePatch) { _ =>
        val batchSize = 200
        val applyChanges = StreamUtils.asyncPageToPageSource { offset =>
          val searchQuery =
            SearchQuery(
              filters = Some(
                SearchFilters(
                  question = Some(QuestionSearchFilter(Seq(questionId))),
                  status = Some(StatusSearchFilter(ProposalStatus.values))
                )
              ),
              offset = Some(Pagination.Offset(offset)),
              limit = Some(Pagination.Limit(offset + batchSize))
            )

          this
            .searchInIndex(searchQuery, requestContext)
            .map(_.results.map(_.id))
        }.mapAsync(3) {
            _.traverse_(
              this
                .getEventSourcingProposal(_, requestContext)
                .flatMap(_.fold(Future.unit)(modifyFn)) // Ignoring proposals that can't be retrieved
            )
          }
          .run()
          .void
        applyChanges.onComplete {
          case Failure(exception) =>
            logger.error("submittedAsLanguage patching script failed with:", exception)
          case Success(_) =>
            val elapsed = System.currentTimeMillis() - startTime
            logger.info(s"submittedAsLanguage patching script succeeded in ${elapsed.toString}ms")
        }

        applyChanges
      }
    }

    private def enrich(
      searchFn: (RequestContext) => Future[ProposalsSearchResult],
      maybeUserId: Option[UserId],
      requestContext: RequestContext,
      preferredLanguage: Option[Language],
      questionDefaultLanguage: Option[Language]
    ): Future[ProposalsResultResponse] = {
      searchFn(requestContext).flatMap { searchResult =>
        maybeUserId
          .fold(
            sessionHistoryCoordinatorService
              .retrieveVoteAndQualifications(sessionId = requestContext.sessionId, searchResult.results.map(_.id))
          ) { userId =>
            userHistoryCoordinatorService.retrieveVoteAndQualifications(userId = userId, searchResult.results.map(_.id))
          }
          .flatMap { votes =>
            val questionIds: Seq[QuestionId] = searchResult.results.flatMap(_.question.map(_.questionId)).distinct
            Future.traverse(questionIds)(sequenceConfigurationService.getSequenceConfigurationByQuestionId).map {
              configs =>
                val proposals = searchResult.results.map { indexedProposal =>
                  val newProposalsVoteThreshold = configs
                    .find(config => indexedProposal.question.map(_.questionId).contains(config.questionId))
                    .fold(0)(_.newProposalsVoteThreshold)
                  val proposalKey =
                    generateProposalKeyHash(
                      indexedProposal.id,
                      requestContext.sessionId,
                      requestContext.location,
                      securityConfiguration.secureVoteSalt
                    )
                  ProposalResponse(
                    indexedProposal,
                    myProposal = maybeUserId.contains(indexedProposal.author.userId),
                    votes.get(indexedProposal.id),
                    proposalKey,
                    newProposalsVoteThreshold,
                    preferredLanguage,
                    questionDefaultLanguage
                  )
                }
                ProposalsResultResponse(searchResult.total, proposals)
            }
          }
      }
    }

    override def searchForUser(
      maybeUserId: Option[UserId],
      query: SearchQuery,
      requestContext: RequestContext,
      preferredLanguage: Option[Language] = None,
      questionDefaultLanguage: Option[Language]
    ): Future[ProposalsResultSeededResponse] = {
      enrich(searchInIndex(query, _), maybeUserId, requestContext, preferredLanguage, questionDefaultLanguage).map(
        proposalResultResponse =>
          ProposalsResultSeededResponse(proposalResultResponse.total, proposalResultResponse.results, query.getSeed)
      )
    }

    override def getTopProposals(
      maybeUserId: Option[UserId],
      questionId: QuestionId,
      size: Int,
      mode: Option[TopProposalsMode],
      requestContext: RequestContext
    ): Future[ProposalsResultResponse] = {
      val search = mode match {
        case Some(IdeaMode) =>
          elasticsearchProposalAPI.getTopProposals(questionId, size, ProposalElasticsearchFieldName.ideaId)
        case _ =>
          elasticsearchProposalAPI.getTopProposals(questionId, size, ProposalElasticsearchFieldName.selectedStakeTagId)
      }
      enrich(
        _ => search.map(results => ProposalsSearchResult(results.size, results)),
        maybeUserId,
        requestContext,
        preferredLanguage = None,
        questionDefaultLanguage = None
      ).map(
        proposalResultResponse => ProposalsResultResponse(proposalResultResponse.total, proposalResultResponse.results)
      )
    }

    override def propose(
      user: User,
      requestContext: RequestContext,
      createdAt: ZonedDateTime,
      content: String,
      question: Question,
      isAnonymous: Boolean,
      initialProposal: Boolean,
      submittedAsLanguage: Language,
      proposalType: ProposalType
    ): Future[ProposalId] = {

      proposalCoordinatorService.propose(
        proposalId = idGenerator.nextProposalId(),
        requestContext = requestContext,
        user = user,
        createdAt = createdAt,
        content = content,
        submittedAsLanguage = submittedAsLanguage,
        question = question,
        isAnonymous = isAnonymous,
        initialProposal = initialProposal,
        proposalType = proposalType
      )
    }

    override def update(
      proposalId: ProposalId,
      moderator: UserId,
      requestContext: RequestContext,
      updatedAt: ZonedDateTime,
      newContent: Option[String],
      newContentTranslations: Option[Multilingual[String]],
      question: Question,
      tags: Seq[TagId]
    ): Future[Option[ModerationProposalResponse]] = {
      toModerationProposalResponse(
        proposalCoordinatorService.update(
          moderator = moderator,
          proposalId = proposalId,
          requestContext = requestContext,
          updatedAt = updatedAt,
          newContent = newContent,
          newTranslations = newContentTranslations,
          question = question,
          tags = tags
        )
      )
    }

    private def getEvents(proposal: Proposal): Future[Seq[ProposalActionResponse]] = {
      val eventsUserIds: Seq[UserId] = proposal.events.map(_.user).distinct
      val futureEventsUsers: Future[Seq[User]] = userService.getUsersByUserIds(eventsUserIds)

      futureEventsUsers.map { eventsUsers =>
        val userById = eventsUsers.map(u => u.userId -> u.displayName).toMap
        proposal.events.map { action =>
          ProposalActionResponse(
            date = action.date,
            user = userById.get(action.user).map(name => ProposalActionAuthorResponse(action.user, name)),
            actionType = action.actionType,
            arguments = action.arguments
          )
        }
      }
    }

    private def toModerationProposalResponse(
      futureProposal: Future[Option[Proposal]]
    ): Future[Option[ModerationProposalResponse]] = {
      def getThreshold(questionId: Option[QuestionId]): Future[Option[Int]] = {
        questionId match {
          case None => Future.successful(None)
          case Some(qId) =>
            sequenceConfigurationService
              .getSequenceConfigurationByQuestionId(qId)
              .map(config => Some(config.newProposalsVoteThreshold))
        }
      }
      def getLanguage(questionId: Option[QuestionId]): Future[Option[Language]] =
        questionId.fold(Future.successful(Option(Language("fr"))))(
          questionService.getQuestion(_).map(_.map(_.defaultLanguage))
        )

      futureProposal.flatMap({
        case None => Future.successful(None)
        case Some(proposal) =>
          (
            userService.getUser(proposal.author),
            getLanguage(proposal.questionId),
            getThreshold(proposal.questionId),
            getEvents(proposal)
          ).tupled.map({
            case (Some(author), Some(language), Some(newProposalsVoteThreshold), events) =>
              val votes = proposal.votingOptions.map { votingOptions =>
                val scores = VotingOptionsScores(votingOptions, newProposalsVoteThreshold)
                votingOptions.wrappers.map(wrapper => ModerationVoteResponse(wrapper, scores.get(wrapper.vote.key)))
              }.getOrElse(Seq.empty)
              Some(
                ModerationProposalResponse(
                  id = proposal.proposalId,
                  proposalId = proposal.proposalId,
                  slug = proposal.slug,
                  content = proposal.content,
                  contentTranslations = proposal.contentTranslations,
                  submittedAsLanguage = Some(proposal.submittedAsLanguage.getOrElse(language)),
                  author = ModerationProposalAuthorResponse(author),
                  status = proposal.status,
                  proposalType = proposal.proposalType,
                  refusalReason = proposal.refusalReason,
                  tags = proposal.tags,
                  votes = votes,
                  context = proposal.creationContext,
                  createdAt = proposal.createdAt,
                  updatedAt = proposal.updatedAt,
                  events = events,
                  idea = proposal.idea,
                  ideaProposals = Seq.empty,
                  operationId = proposal.operation,
                  questionId = proposal.questionId,
                  keywords = proposal.keywords,
                  zone = proposal.getZone
                )
              )
            case _ => None
          })
      })
    }

    override def updateVotes(
      proposalId: ProposalId,
      moderator: UserId,
      requestContext: RequestContext,
      updatedAt: ZonedDateTime,
      votes: Seq[UpdateVoteRequest]
    ): Future[Option[ModerationProposalResponse]] = {
      toModerationProposalResponse(
        proposalCoordinatorService.updateVotes(
          moderator = moderator,
          proposalId = proposalId,
          requestContext = requestContext,
          updatedAt = updatedAt,
          votes = votes
        )
      )
    }

    override def validateProposal(
      proposalId: ProposalId,
      moderator: UserId,
      requestContext: RequestContext,
      question: Question,
      newContent: Option[String],
      newContentTranslations: Option[Multilingual[String]],
      sendNotificationEmail: Boolean,
      tags: Seq[TagId]
    ): Future[Option[ModerationProposalResponse]] = {
      toModerationProposalResponse(
        proposalCoordinatorService.accept(
          proposalId = proposalId,
          moderator = moderator,
          requestContext = requestContext,
          sendNotificationEmail = sendNotificationEmail,
          newContent = newContent,
          newTranslations = newContentTranslations,
          question = question,
          tags = tags
        )
      )
    }

    override def refuseProposal(
      proposalId: ProposalId,
      moderator: UserId,
      requestContext: RequestContext,
      request: RefuseProposalRequest
    ): Future[Option[ModerationProposalResponse]] = {
      toModerationProposalResponse(
        proposalCoordinatorService.refuse(
          proposalId = proposalId,
          moderator = moderator,
          requestContext = requestContext,
          sendNotificationEmail = request.sendNotificationEmail,
          refusalReason = request.refusalReason
        )
      )
    }

    override def postponeProposal(
      proposalId: ProposalId,
      moderator: UserId,
      requestContext: RequestContext
    ): Future[Option[ModerationProposalResponse]] = {

      toModerationProposalResponse(
        proposalCoordinatorService
          .postpone(proposalId = proposalId, moderator = moderator, requestContext = requestContext)
      )
    }

    private def retrieveVoteHistory(
      proposalId: ProposalId,
      maybeUserId: Option[UserId],
      sessionId: SessionId
    ): Future[Map[ProposalId, VoteAndQualifications]] = {
      val votesHistory = maybeUserId match {
        case Some(userId) =>
          userHistoryCoordinatorService.retrieveVoteAndQualifications(userId, Seq(proposalId))
        case None =>
          sessionHistoryCoordinatorService
            .retrieveVoteAndQualifications(sessionId = sessionId, proposalIds = Seq(proposalId))
      }
      votesHistory
    }

    private def retrieveUser(maybeUserId: Option[UserId]): Future[Option[User]] = {
      maybeUserId.map { userId =>
        userService.getUser(userId)
      }.getOrElse(Future.successful(None))
    }

    private def incrementTrollCounter(requestContext: RequestContext) = {
      Kamon
        .counter("vote_trolls")
        .withTags(
          TagSet.from(
            Map(
              "application" -> requestContext.applicationName.fold("unknown")(_.value),
              "location" -> requestContext.location.flatMap(_.split(" ").headOption).getOrElse("unknown")
            )
          )
        )
        .increment()
    }

    private val sequenceLocations: Set[String] = Set("sequence", "widget", "sequence-popular", "sequence-controversial")

    def resolveVoteTrust(
      proposalKey: Option[String],
      proposalId: ProposalId,
      maybeUserSegment: Option[String],
      maybeProposalSegment: Option[String],
      requestContext: RequestContext
    ): VoteTrust = {

      val newHash =
        generateProposalKeyHash(
          proposalId,
          requestContext.sessionId,
          requestContext.location,
          securityConfiguration.secureVoteSalt
        )
      val page = requestContext.location.flatMap(_.split(" ").headOption)

      val isInSegment = (
        for {
          userSegment     <- maybeUserSegment
          proposalSegment <- maybeProposalSegment
        } yield userSegment == proposalSegment
      ).exists(identity)

      val inSequence = page.exists(sequenceLocations.contains)
      (proposalKey, proposalKey.contains(newHash), inSequence, isInSegment) match {
        case (None, _, _, _) =>
          logger.warn(s"No proposal key for proposal ${proposalId.value}, on context ${requestContext.toString}")
          incrementTrollCounter(requestContext)
          Troll
        case (Some(_), false, _, _) =>
          logger.warn(s"Bad proposal key found for proposal ${proposalId.value}, on context ${requestContext.toString}")
          incrementTrollCounter(requestContext)
          Troll
        case (Some(_), true, true, true) => Segment
        case (Some(_), true, true, _)    => Sequence
        case (Some(_), true, _, _)       => Trusted
      }
    }

    private def getSegmentForProposal(proposalId: ProposalId): Future[Option[String]] = {
      proposalCoordinatorService.getProposal(proposalId).flatMap {
        case None           => Future.successful(None)
        case Some(proposal) => segmentService.resolveSegment(proposal.creationContext)
      }
    }

    private def fVoteProposal(
      proposalId: ProposalId,
      maybeUserId: Option[UserId],
      requestContext: RequestContext,
      voteKey: VoteKey,
      proposalKey: Option[String],
      fVote: VoteProposalParams => Future[Option[VotesActorResponse]]
    ): Future[Option[VotesActorResponse]] = {

      def runVote: Future[Option[VotesActorResponse]] = {
        val futureVotes = retrieveVoteHistory(proposalId, maybeUserId, requestContext.sessionId)
        val futureUser = retrieveUser(maybeUserId)
        val futureProposalSegment = getSegmentForProposal(proposalId)
        val futureUserSegment = segmentService.resolveSegment(requestContext)

        proposalCoordinatorService.getProposal(proposalId).flatMap {
          case None => Future.successful(None)
          case Some(proposal) =>
            proposal.questionId match {
              case None => Future.successful(None)
              case Some(questionId) =>
                for {
                  votes                <- futureVotes
                  user                 <- futureUser
                  maybeProposalSegment <- futureProposalSegment
                  maybeUserSegment     <- futureUserSegment
                  config               <- sequenceConfigurationService.getSequenceConfigurationByQuestionId(questionId)
                  vote <- fVote(
                    VoteProposalParams(
                      proposalId = proposalId,
                      maybeUserId = maybeUserId,
                      requestContext = requestContext,
                      voteKey = voteKey,
                      maybeOrganisationId = user.filter(_.userType == UserType.UserTypeOrganisation).map(_.userId),
                      vote = votes.get(proposalId),
                      voteTrust = resolveVoteTrust(
                        proposalKey,
                        proposalId,
                        maybeUserSegment,
                        maybeProposalSegment,
                        requestContext
                      ),
                      newProposalsVoteThreshold = config.newProposalsVoteThreshold
                    )
                  )
                } yield vote
            }
        }
      }

      val result = for {
        _    <- sessionHistoryCoordinatorService.lockSessionForVote(requestContext.sessionId, proposalId)
        vote <- runVote
        _    <- sessionHistoryCoordinatorService.unlockSessionForVote(requestContext.sessionId, proposalId)
      } yield vote

      result.recoverWith {
        case e: ConcurrentModification => Future.failed(e)
        case other =>
          sessionHistoryCoordinatorService.unlockSessionForVote(requestContext.sessionId, proposalId).flatMap { _ =>
            Future.failed(other)
          }
      }

    }

    override def voteProposal(
      proposalId: ProposalId,
      maybeUserId: Option[UserId],
      requestContext: RequestContext,
      voteKey: VoteKey,
      proposalKey: Option[String]
    ): Future[Option[VotesActorResponse]] =
      fVoteProposal(proposalId, maybeUserId, requestContext, voteKey, proposalKey, proposalCoordinatorService.vote)

    override def unvoteProposal(
      proposalId: ProposalId,
      maybeUserId: Option[UserId],
      requestContext: RequestContext,
      voteKey: VoteKey,
      proposalKey: Option[String]
    ): Future[Option[VotesActorResponse]] =
      fVoteProposal(proposalId, maybeUserId, requestContext, voteKey, proposalKey, proposalCoordinatorService.unvote)

    override def qualifyVote(
      proposalId: ProposalId,
      maybeUserId: Option[UserId],
      requestContext: RequestContext,
      voteKey: VoteKey,
      qualificationKey: QualificationKey,
      proposalKey: Option[String]
    ): Future[Option[Qualification]] = {

      val result = for {
        _ <- sessionHistoryCoordinatorService.lockSessionForQualification(
          requestContext.sessionId,
          proposalId,
          qualificationKey
        )
        votes                <- retrieveVoteHistory(proposalId, maybeUserId, requestContext.sessionId)
        maybeUserSegment     <- segmentService.resolveSegment(requestContext)
        maybeProposalSegment <- getSegmentForProposal(proposalId)
        qualify <- proposalCoordinatorService.qualification(
          proposalId = proposalId,
          maybeUserId = maybeUserId,
          requestContext = requestContext,
          voteKey = voteKey,
          qualificationKey = qualificationKey,
          vote = votes.get(proposalId),
          voteTrust = resolveVoteTrust(proposalKey, proposalId, maybeUserSegment, maybeProposalSegment, requestContext)
        )
        _ <- sessionHistoryCoordinatorService.unlockSessionForQualification(
          requestContext.sessionId,
          proposalId,
          qualificationKey
        )
      } yield qualify

      result.recoverWith {
        case e: ConcurrentModification => Future.failed(e)
        case other =>
          sessionHistoryCoordinatorService
            .unlockSessionForQualification(requestContext.sessionId, proposalId, qualificationKey)
            .flatMap { _ =>
              Future.failed(other)
            }
      }

    }

    override def unqualifyVote(
      proposalId: ProposalId,
      maybeUserId: Option[UserId],
      requestContext: RequestContext,
      voteKey: VoteKey,
      qualificationKey: QualificationKey,
      proposalKey: Option[String]
    ): Future[Option[Qualification]] = {

      val result = for {
        _ <- sessionHistoryCoordinatorService.lockSessionForQualification(
          requestContext.sessionId,
          proposalId,
          qualificationKey
        )
        votes                <- retrieveVoteHistory(proposalId, maybeUserId, requestContext.sessionId)
        maybeUserSegment     <- segmentService.resolveSegment(requestContext)
        maybeProposalSegment <- getSegmentForProposal(proposalId)
        removeQualification <- proposalCoordinatorService.unqualification(
          proposalId = proposalId,
          maybeUserId = maybeUserId,
          requestContext = requestContext,
          voteKey = voteKey,
          qualificationKey = qualificationKey,
          vote = votes.get(proposalId),
          voteTrust = resolveVoteTrust(proposalKey, proposalId, maybeUserSegment, maybeProposalSegment, requestContext)
        )
        _ <- sessionHistoryCoordinatorService.unlockSessionForQualification(
          requestContext.sessionId,
          proposalId,
          qualificationKey
        )
      } yield removeQualification

      result.recoverWith {
        case e: ConcurrentModification => Future.failed(e)
        case other =>
          sessionHistoryCoordinatorService
            .unlockSessionForQualification(requestContext.sessionId, proposalId, qualificationKey)
            .flatMap { _ =>
              Future.failed(other)
            }
      }

    }

    override def lockProposal(
      proposalId: ProposalId,
      moderatorId: UserId,
      moderatorFullName: Option[String],
      requestContext: RequestContext
    ): Future[Unit] = {
      proposalCoordinatorService
        .lock(
          proposalId = proposalId,
          moderatorId = moderatorId,
          moderatorName = moderatorFullName,
          requestContext = requestContext
        )
        .void
    }

    override def lockProposals(
      proposalIds: Seq[ProposalId],
      moderatorId: UserId,
      moderatorFullName: Option[String],
      requestContext: RequestContext
    ): Future[Unit] = {
      Source(proposalIds)
        .mapAsync(5)(
          id =>
            proposalCoordinatorService.lock(
              proposalId = id,
              moderatorId = moderatorId,
              moderatorName = moderatorFullName,
              requestContext = requestContext
            )
        )
        .runWith(Sink.seq)
        .void
    }

    // Very permissive (no event generated etc), use carefully and only for one shot migrations.
    override def patchProposal(
      proposalId: ProposalId,
      userId: UserId,
      requestContext: RequestContext,
      changes: PatchProposalRequest
    ): Future[Option[ModerationProposalResponse]] = {
      toModerationProposalResponse(
        proposalCoordinatorService
          .patch(proposalId = proposalId, userId = userId, changes = changes, requestContext = requestContext)
      )
    }

    override def changeProposalsIdea(
      proposalIds: Seq[ProposalId],
      moderatorId: UserId,
      ideaId: IdeaId
    ): Future[Seq[Proposal]] = {
      Future
        .sequence(proposalIds.map { proposalId =>
          proposalCoordinatorService.patch(
            proposalId = proposalId,
            userId = moderatorId,
            changes = PatchProposalRequest(ideaId = Some(ideaId)),
            requestContext = RequestContext.empty
          )
        })
        .map(_.flatten)
    }

    private def getSearchFilters(
      questionId: QuestionId,
      languages: Option[NonEmptyList[Language]],
      toEnrich: Boolean,
      minVotesCount: Option[Int],
      minScore: Option[Double]
    ): SearchFilters = {
      if (toEnrich) {
        SearchFilters(
          question = Some(QuestionSearchFilter(Seq(questionId))),
          languages = languages.map(_.map(org.make.core.proposal.LanguageSearchFilter)),
          status = Some(StatusSearchFilter(Seq(ProposalStatus.Accepted))),
          toEnrich = Some(ToEnrichSearchFilter(toEnrich)),
          minVotesCount = minVotesCount.map(MinVotesCountSearchFilter.apply),
          minScore = minScore.map(MinScoreSearchFilter.apply)
        )
      } else {
        SearchFilters(
          question = Some(QuestionSearchFilter(Seq(questionId))),
          languages = languages.map(_.map(org.make.core.proposal.LanguageSearchFilter)),
          status = Some(StatusSearchFilter(Seq(ProposalStatus.Pending)))
        )
      }
    }

    override def searchAndLockAuthorToModerate(
      questionId: QuestionId,
      moderatorId: UserId,
      moderatorFullName: Option[String],
      languages: Option[NonEmptyList[Language]],
      requestContext: RequestContext,
      toEnrich: Boolean,
      minVotesCount: Option[Int],
      minScore: Option[Double]
    ): Future[Option[ModerationAuthorResponse]] = {
      val searchFilters = getSearchFilters(questionId, languages, toEnrich, minVotesCount, minScore)
      searchInIndex(
        requestContext = requestContext,
        query = SearchQuery(
          filters = Some(searchFilters),
          sort = Some(Sort(Some(ProposalElasticsearchFieldName.createdAt.field), Some(SortOrder.ASC))),
          limit = Some(Pagination.Limit(1000)),
          language = None,
          sortAlgorithm = Some(B2BFirstAlgorithm)
        )
      ).flatMap { searchResults =>
        // Group proposals to moderate by author, in a LinkedHashMap to transfer proposal search sort order to authors
        @SuppressWarnings(Array("org.wartremover.warts.MutableDataStructures"))
        val candidates =
          searchResults.results.foldLeft(mutable.LinkedHashMap.empty[UserId, NonEmptyList[IndexedProposal]]) {
            case (map, proposal) =>
              val list = map.get(proposal.author.userId).fold(NonEmptyList.of(proposal))(_ :+ proposal)
              map.put(proposal.author.userId, list)
              map
          }

        // For current author candidate, try to lock and turn every proposal into a moderation response
        def futureMaybeSuccessfulLocks(
          indexedProposals: NonEmptyList[IndexedProposal],
          tags: Seq[Tag],
          tagTypes: Seq[TagType]
        ): Future[NonEmptyList[Option[ModerationProposalResponse]]] = {
          indexedProposals.traverse { proposal =>
            getModerationProposalById(proposal.id).flatMap {
              case None => Future.successful(None)
              case Some(proposal) =>
                val isValid: Boolean = if (toEnrich) {
                  val proposalTags = tags.filter(tag => proposal.tags.contains(tag.tagId))
                  Proposal.needsEnrichment(proposal.status, tagTypes, proposalTags.map(_.tagTypeId))
                } else {
                  proposal.status == Pending
                }

                if (!isValid) {
                  // Current proposal was moderated in the meantime, do nothing
                  Future.successful(None)
                } else {
                  proposalCoordinatorService
                    .lock(proposal.proposalId, moderatorId, moderatorFullName, requestContext)
                    .map { _ =>
                      searchFilters.status.foreach(
                        filter =>
                          if (!filter.status.contains(proposal.status)) {
                            logger.error(
                              s"Proposal id=${proposal.proposalId.value} with status=${proposal.status} incorrectly candidate for moderation, questionId=${questionId.value} moderator=${moderatorId.value} toEnrich=$toEnrich searchFilters=$searchFilters requestContext=$requestContext"
                            )
                          }
                      )
                      Some(proposal)
                    }
                    .recoverWith { case _ => Future.successful(None) }
                }
            }
          }
        }

        def getAndLockFirstAuthor(tags: Seq[Tag], tagTypes: Seq[TagType]) =
          Source(candidates.view.values.toSeq)
            .foldAsync[Option[NonEmptyList[ModerationProposalResponse]]](None) {
              case (None, indexedProposals) =>
                // For current author entry, build a future list of options of locked proposals, each element being
                // a moderation response defined if corresponding proposal was still valid and could be locked.
                // Then turn it into a future option of a list of locks if every one succeeded.
                futureMaybeSuccessfulLocks(indexedProposals, tags, tagTypes).map(_.sequence)
              case (Some(list), _) =>
                Future.successful(Some(list))
            }
            // Find the first author for which all proposals could be locked and were turned into a list of moderation responses
            .collect { case Some(list) => list }
            .runWith(Sink.headOption)
            .map(_.map { list =>
              ModerationAuthorResponse(list.head.author, list.toList, searchResults.total.toInt)
            })

        for {
          tags           <- tagService.findByQuestionId(questionId)
          tagTypes       <- tagTypeService.findAll(requiredForEnrichmentFilter = Some(true))
          authorResponse <- getAndLockFirstAuthor(tags, tagTypes)
        } yield authorResponse

      }
    }

    override def searchAndLockProposalToModerate(
      questionId: QuestionId,
      moderator: UserId,
      moderatorFullName: Option[String],
      languages: Option[NonEmptyList[Language]] = None,
      requestContext: RequestContext,
      toEnrich: Boolean,
      minVotesCount: Option[Int],
      minScore: Option[Double]
    ): Future[Option[ModerationProposalResponse]] = {
      val defaultNumberOfProposals = 50
      val searchFilters = getSearchFilters(questionId, None, toEnrich, minVotesCount, minScore)
      searchInIndex(
        requestContext = requestContext,
        query = SearchQuery(
          filters = Some(searchFilters),
          sort = Some(Sort(Some(ProposalElasticsearchFieldName.createdAt.field), Some(SortOrder.ASC))),
          limit = Some(Pagination.Limit(defaultNumberOfProposals)),
          language = None,
          sortAlgorithm = Some(B2BFirstAlgorithm)
        )
      ).flatMap { results =>
        @SuppressWarnings(Array("org.wartremover.warts.Recursion"))
        def recursiveLock(availableProposals: List[ProposalId]): Future[Option[ModerationProposalResponse]] = {
          availableProposals match {
            case Nil => Future.successful(None)
            case currentProposalId :: otherProposalIds =>
              getModerationProposalById(currentProposalId).flatMap {
                // If, for some reason, the proposal is not found in event sourcing, ignore it
                case None => recursiveLock(otherProposalIds)
                case Some(proposal) =>
                  val isValid: Future[Boolean] = if (toEnrich) {
                    for {
                      tags     <- tagService.findByTagIds(proposal.tags)
                      tagTypes <- tagTypeService.findAll(requiredForEnrichmentFilter = Some(true))
                    } yield {
                      Proposal.needsEnrichment(proposal.status, tagTypes, tags.map(_.tagTypeId))
                    }
                  } else {
                    Future.successful(proposal.status == Pending)
                  }

                  isValid.flatMap {
                    case false => recursiveLock(otherProposalIds)
                    case true =>
                      proposalCoordinatorService
                        .lock(proposal.proposalId, moderator, moderatorFullName, requestContext)
                        .map { _ =>
                          searchFilters.status.foreach(
                            filter =>
                              if (!filter.status.contains(proposal.status)) {
                                logger.error(
                                  s"Proposal id=${proposal.proposalId.value} with status=${proposal.status} incorrectly candidate for moderation, questionId=${questionId.value} moderator=${moderator.value} toEnrich=$toEnrich searchFilters=$searchFilters requestContext=$requestContext"
                                )
                              }
                          )
                          Some(proposal)
                        }
                        .recoverWith { case _ => recursiveLock(otherProposalIds) }
                  }
              }
          }
        }
        recursiveLock(results.results.map(_.id).toList)
      }
    }

    override def getTagsForProposal(proposal: Proposal): Future[TagsForProposalResponse] = {
      proposal.questionId.fold(Future.successful(TagsForProposalResponse.empty)) { questionId =>
        val futureTags: Future[Seq[Tag]] = tagService.findByQuestionId(questionId)
        futureTags.map { questionTags =>
          val tags = questionTags.map { tag =>
            val checked = proposal.tags.contains(tag.tagId)
            TagForProposalResponse(tag = tag, checked = checked, predicted = false)
          }
          TagsForProposalResponse(tags = tags, modelName = "none")
        }
      }
    }

    private def trolledQualification(qualification: Qualification): Boolean =
      qualification.count != qualification.countVerified

    private def trolledVote(voteWrapper: VotingOptionWrapper): Boolean =
      voteWrapper.vote.count != voteWrapper.vote.countVerified ||
        voteWrapper.deprecatedQualificationsSeq.exists(trolledQualification)

    def needVoteReset(proposal: Proposal): Boolean =
      proposal.status == ProposalStatus.Accepted &&
        proposal.votingOptions.exists { votingOptions =>
          trolledVote(votingOptions.agreeVote) ||
          trolledVote(votingOptions.neutralVote) ||
          trolledVote(votingOptions.disagreeVote)
        }

    override def resetVotes(adminUserId: UserId, requestContext: RequestContext): Future[Done] = {
      val start = System.currentTimeMillis()

      proposalJournal
        .currentPersistenceIds()
        .mapAsync(4) { id =>
          proposalCoordinatorService.getProposal(ProposalId(id))
        }
        .collect {
          case Some(proposal) if needVoteReset(proposal) => proposal
        }
        .mapAsync(4) { proposal =>
          val voteWrappers = proposal.votingOptions.fold(Seq.empty[VotingOptionWrapper])(_.wrappers)
          proposalCoordinatorService.updateVotes(
            moderator = adminUserId,
            proposalId = proposal.proposalId,
            requestContext = requestContext,
            updatedAt = DateHelper.now(),
            votes = voteWrappers
              .filter(trolledVote)
              .map(
                voteWrapper =>
                  UpdateVoteRequest(
                    key = voteWrapper.vote.key,
                    count = Some(voteWrapper.vote.countVerified),
                    countVerified = None,
                    countSequence = None,
                    countSegment = None,
                    qualifications = voteWrapper.deprecatedQualificationsSeq.collect {
                      case qualification if trolledQualification(qualification) =>
                        UpdateQualificationRequest(
                          key = qualification.key,
                          count = Some(qualification.countVerified),
                          countVerified = None,
                          countSequence = None,
                          countSegment = None
                        )
                    }
                  )
              )
          )
        }
        .runWith(Sink.ignore)
        .map { res =>
          val time = System.currentTimeMillis() - start
          logger.info(s"ResetVotes ended in $time ms")
          res
        }
    }

    override def resolveQuestionFromVoteEvent(
      resolver: QuestionResolver,
      context: RequestContext,
      proposalId: ProposalId
    ): Future[Option[Question]] = {
      resolver
        .extractQuestionWithOperationFromRequestContext(context) match {
        case Some(question) => Future.successful(Some(question))
        case None =>
          proposalCoordinatorService
            .getProposal(proposalId)
            .map { maybeProposal =>
              resolver.findQuestionWithOperation { question =>
                maybeProposal.flatMap(_.questionId).contains(question.questionId)
              }
            }
      }
    }

    private def searchUserProposals(userId: UserId): Future[ProposalsSearchResult] = {
      val filters =
        SearchFilters(
          users = Some(UserSearchFilter(Seq(userId))),
          status = Some(StatusSearchFilter(ProposalStatus.values))
        )
      elasticsearchProposalAPI
        .countProposals(SearchQuery(filters = Some(filters)))
        .flatMap { count =>
          if (count == 0) {
            Future.successful(ProposalsSearchResult(0L, Seq.empty))
          } else {
            elasticsearchProposalAPI
              .searchProposals(SearchQuery(filters = Some(filters), limit = Some(Pagination.Limit(count.intValue()))))
          }
        }
    }

    override def resolveQuestionFromUserProposal(
      questionResolver: QuestionResolver,
      requestContext: RequestContext,
      userId: UserId,
      eventDate: ZonedDateTime
    ): Future[Option[Question]] = {
      questionResolver
        .extractQuestionWithOperationFromRequestContext(requestContext) match {
        case Some(question) => Future.successful(Some(question))
        case None           =>
          // If we can't resolve the question, retrieve the user proposals,
          // and search for the one proposed at the event date
          searchUserProposals(userId).map { proposalResult =>
            proposalResult.results
              .find(_.createdAt == eventDate)
              .flatMap(_.question)
              .map(_.questionId)
              .flatMap { questionId =>
                questionResolver
                  .findQuestionWithOperation(question => questionId == question.questionId)
              }
          }
      }
    }

    override def questionFeaturedProposals(
      questionId: QuestionId,
      maxPartnerProposals: Int,
      preferredLanguage: Option[Language],
      questionDefaultLanguage: Option[Language],
      limit: Pagination.Limit,
      seed: Option[Int],
      maybeUserId: Option[UserId],
      requestContext: RequestContext
    ): Future[ProposalsResultSeededResponse] = {
      val randomSeed: Int = seed.getOrElse(MakeRandom.nextInt())
      val futurePartnerProposals: Future[ProposalsResultSeededResponse] =
        Math.min(maxPartnerProposals, limit.extractInt) match {
          case 0 => Future.successful(ProposalsResultSeededResponse.empty)
          case posInt =>
            partnerService
              .find(
                offset = Offset.zero,
                end = None,
                sort = None,
                order = None,
                questionId = Some(questionId),
                organisationId = None,
                partnerKind = None
              )
              .flatMap { partners =>
                partners.collect {
                  case Partner(_, _, _, _, Some(orgaId), _, _, _) => orgaId
                } match {
                  case Seq() => Future.successful(ProposalsResultSeededResponse.empty)
                  case orgaIds =>
                    searchForUser(
                      maybeUserId,
                      query = SearchQuery(
                        filters = Some(
                          SearchFilters(
                            question = Some(QuestionSearchFilter(Seq(questionId))),
                            users = Some(UserSearchFilter(orgaIds))
                          )
                        ),
                        sortAlgorithm = Some(RandomAlgorithm(randomSeed)),
                        limit = Some(Pagination.Limit(posInt))
                      ),
                      requestContext = requestContext,
                      preferredLanguage = preferredLanguage,
                      questionDefaultLanguage = questionDefaultLanguage
                    )
                }
              }
        }
      val futureProposalsRest: Future[ProposalsResultSeededResponse] = {
        enrich(
          _ =>
            elasticsearchProposalAPI.getFeaturedProposals(
              SearchQuery(
                filters = Some(
                  SearchFilters(
                    question = Some(QuestionSearchFilter(Seq(questionId))),
                    userTypes = Some(UserTypesSearchFilter(Seq(UserType.UserTypeUser)))
                  )
                ),
                limit = Some(limit)
              )
            ),
          maybeUserId,
          requestContext,
          preferredLanguage,
          questionDefaultLanguage
        ).map(r => ProposalsResultSeededResponse(r.total, r.results, None))
      }
      for {
        partnerProposals <- futurePartnerProposals
        rest             <- futureProposalsRest
      } yield ProposalsResultSeededResponse(
        partnerProposals.total + rest.total,
        partnerProposals.results ++ rest.results.take(limit.extractInt - partnerProposals.results.size),
        Some(randomSeed)
      )

    }

    private def setProposalKeywords(
      proposalId: ProposalId,
      keywords: Seq[ProposalKeyword],
      requestContext: RequestContext
    ): Future[ProposalKeywordsResponse] = {
      proposalCoordinatorService.setKeywords(proposalId, keywords, requestContext).map {
        case Some(proposal) =>
          ProposalKeywordsResponse(proposal.proposalId, status = ProposalKeywordsResponseStatus.Ok, message = None)
        case None =>
          ProposalKeywordsResponse(
            proposalId,
            status = ProposalKeywordsResponseStatus.Error,
            message = Some(s"Proposal ${proposalId.value} not found")
          )
      }
    }

    override def setKeywords(
      proposalKeywordsList: Seq[ProposalKeywordRequest],
      requestContext: RequestContext
    ): Future[Seq[ProposalKeywordsResponse]] = {
      Source(proposalKeywordsList)
        .mapAsync(3) { proposalKeywords =>
          setProposalKeywords(proposalKeywords.proposalId, proposalKeywords.keywords, requestContext)
        }
        .runWith(Sink.seq)
    }

    override def refuseAll(
      proposalIds: Seq[ProposalId],
      moderator: UserId,
      requestContext: RequestContext
    ): Future[BulkActionResponse] = {
      bulkAction(refuseAction(moderator, requestContext), proposalIds)
    }

    override def addTagsToAll(
      proposalIds: Seq[ProposalId],
      tagIds: Seq[TagId],
      moderator: UserId,
      requestContext: RequestContext
    ): Future[BulkActionResponse] = {
      bulkAction(updateTagsAction(moderator, requestContext, current => (current ++ tagIds).distinct), proposalIds)
    }

    override def deleteTagFromAll(
      proposalIds: Seq[ProposalId],
      tagId: TagId,
      moderator: UserId,
      requestContext: RequestContext
    ): Future[BulkActionResponse] = {
      bulkAction(updateTagsAction(moderator, requestContext, _.filterNot(_ == tagId)), proposalIds)
    }

    private def refuseAction(
      moderator: UserId,
      requestContext: RequestContext
    )(proposal: IndexedProposal, question: Question): Future[Option[Proposal]] =
      if (proposal.initialProposal) {
        proposalCoordinatorService
          .refuse(
            proposalId = proposal.id,
            moderator = moderator,
            requestContext = requestContext,
            sendNotificationEmail = false,
            refusalReason = Some("other")
          )
      } else {
        Future.failed(
          ValidationFailedError(
            Seq(ValidationError(field = "initial", key = "not-initial", message = Some("The proposal is not initial")))
          )
        )
      }

    private def updateTagsAction(
      moderator: UserId,
      requestContext: RequestContext,
      transformTags: Seq[TagId] => Seq[TagId]
    )(proposal: IndexedProposal, question: Question): Future[Option[Proposal]] =
      proposalCoordinatorService
        .update(
          moderator = moderator,
          proposalId = proposal.id,
          requestContext = requestContext,
          updatedAt = DateHelper.now(),
          newContent = None,
          newTranslations = None,
          tags = transformTags(proposal.tags.map(_.tagId)),
          question = question
        )

    private def bulkAction(
      action: (IndexedProposal, Question) => Future[Option[Proposal]],
      proposalIds: Seq[ProposalId]
    ): Future[BulkActionResponse] = {
      val getProposals: Future[Map[ProposalId, (IndexedProposal, Option[QuestionId])]] =
        elasticsearchProposalAPI
          .searchProposals(
            SearchQuery(
              filters = Some(
                SearchFilters(
                  proposal = Some(ProposalSearchFilter(proposalIds)),
                  status = Some(StatusSearchFilter(ProposalStatus.values))
                )
              ),
              limit = Some(Pagination.Limit(proposalIds.size + 1))
            )
          )
          .map(_.results.map(p => (p.id, (p, p.question.map(_.questionId)))).toMap)

      def getQuestions(proposalMap: Map[ProposalId, (IndexedProposal, Option[QuestionId])]): Future[Seq[Question]] =
        questionService.getQuestions(proposalMap.values.collect { case (_, Some(id)) => id }.toSeq.distinct)

      def runActionOnAll(
        action: (IndexedProposal, Question) => Future[Option[Proposal]],
        proposalIds: Seq[ProposalId],
        proposals: Map[ProposalId, (IndexedProposal, Option[QuestionId])],
        questions: Seq[Question]
      ): Future[Seq[SingleActionResponse]] = {
        Source(proposalIds).map { id =>
          proposals.get(id) match {
            case Some((p, Some(qId))) => (id, Some(p), questions.find(_.questionId == qId), Some(qId))
            case Some((p, None))      => (id, Some(p), None, None)
            case None                 => (id, None, None, None)
          }
        }.mapAsync(5) {
            case (_, Some(proposal), Some(question), _) => runAction(proposal.id, action(proposal, question))
            case (id, None, _, _) =>
              Future.successful(SingleActionResponse(id, ActionKey.NotFound, Some("Proposal not found")))
            case (id, Some(_), _, qId) =>
              Future.successful(
                SingleActionResponse(id, ActionKey.QuestionNotFound, Some(s"Question not found from id $qId"))
              )
          }
          .runWith(Sink.seq)
      }

      def runAction(id: ProposalId, action: Future[Option[Proposal]]): Future[SingleActionResponse] = {
        action.map {
          case None => SingleActionResponse(id, ActionKey.NotFound, Some(s"Proposal not found"))
          case _    => SingleActionResponse(id, ActionKey.OK, None)
        }.recover {
          case ValidationFailedError(Seq(error)) =>
            SingleActionResponse(id, ActionKey.ValidationError(error.key), error.message)
          case e =>
            SingleActionResponse(id, ActionKey.Unknown, Some(e.getMessage))
        }
      }

      for {
        proposalMap <- getProposals
        questions   <- getQuestions(proposalMap)
        actions     <- runActionOnAll(action, proposalIds, proposalMap, questions)
      } yield {
        val (successes, failures) = actions.partition(_.key == ActionKey.OK)
        BulkActionResponse(successes.map(_.proposalId), failures)
      }
    }

    override def getHistory(proposalId: ProposalId): Future[Option[Seq[ProposalActionResponse]]] = {
      for {
        proposal <- proposalCoordinatorService.getProposal(proposalId)
        events   <- proposal.traverse(getEvents)
      } yield events
    }

    override def generateProposalKeyHash(
      proposalId: ProposalId,
      sessionId: SessionId,
      location: Option[String],
      salt: String
    ): String = {
      val rawString: String = s"${proposalId.value}${sessionId.value}${location.getOrElse("")}"
      SecurityHelper.generateHash(value = rawString, salt = salt)
    }
  }
}
