/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.sequence

import akka.util.Timeout
import org.make.api.proposal.{ProposalResponse, ProposalServiceComponent}
import org.make.api.sequence.SequenceBehaviour.ConsensusParam
import org.make.api.sequence.SequenceCacheManager.{GetConsensusProposal, GetControversyProposal, GetProposal}
import org.make.api.technical.{ActorSystemComponent, TimeSettings}
import org.make.api.technical.BetterLoggingActors.BetterLoggingTypedActorRef
import org.make.api.technical.security.SecurityConfigurationComponent
import org.make.core.RequestContext
import org.make.core.reference.Language
import org.make.core.proposal.indexed.IndexedProposal
import org.make.core.question.QuestionId

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

trait DefaultSequenceCacheManagerServiceComponent extends SequenceCacheManagerServiceComponent {
  this: SequenceCacheManagerComponent
    with ActorSystemComponent
    with ProposalServiceComponent
    with SecurityConfigurationComponent
    with SequenceConfigurationComponent =>

  override lazy val sequenceCacheManagerService: SequenceCacheManagerService = new DefaultSequenceCacheManagerService

  class DefaultSequenceCacheManagerService extends SequenceCacheManagerService {
    implicit val timeout: Timeout = TimeSettings.defaultTimeout

    // These methods are meant to be used by the widget.
    // Widget does not support cookies thus users will not be authenticated.
    // There's no need to populate `myProposal` and `voteAndQualifications`
    override def getProposal(
      id: QuestionId,
      requestContext: RequestContext,
      preferredLanguage: Option[Language],
      questionDefaultLanguage: Language
    ): Future[ProposalResponse] =
      (sequenceCacheManager ?? (GetProposal(id, _)))
        .flatMap(shapeResponse(_, requestContext, preferredLanguage, questionDefaultLanguage))

    override def getControversyProposal(
      id: QuestionId,
      requestContext: RequestContext,
      preferredLanguage: Option[Language],
      questionDefaultLanguage: Language
    ): Future[ProposalResponse] =
      (sequenceCacheManager ?? (GetControversyProposal(id, _)))
        .flatMap(shapeResponse(_, requestContext, preferredLanguage, questionDefaultLanguage))

    override def getConsensusProposal(
      id: QuestionId,
      threshold: () => Future[ConsensusParam],
      requestContext: RequestContext,
      preferredLanguage: Option[Language],
      questionDefaultLanguage: Language
    ): Future[ProposalResponse] =
      (sequenceCacheManager ?? (GetConsensusProposal(id, threshold, _)))
        .flatMap(shapeResponse(_, requestContext, preferredLanguage, questionDefaultLanguage))

    def shapeResponse(
      proposal: IndexedProposal,
      requestContext: RequestContext,
      preferredLanguage: Option[Language],
      questionDefaultLanguage: Language
    ): Future[ProposalResponse] = {
      val proposalKey =
        proposalService.generateProposalKeyHash(
          proposal.id,
          requestContext.sessionId,
          requestContext.location,
          securityConfiguration.secureVoteSalt
        )

      val futureThreshold = proposal.question.map(_.questionId) match {
        case None => Future.successful(0)
        case Some(questionId) =>
          sequenceConfigurationService
            .getSequenceConfigurationByQuestionId(questionId)
            .map(_.newProposalsVoteThreshold)
      }

      futureThreshold.map(
        newProposalsVoteThreshold =>
          ProposalResponse(
            indexedProposal = proposal,
            myProposal = false,
            voteAndQualifications = None,
            proposalKey = proposalKey,
            newProposalsVoteThreshold = newProposalsVoteThreshold,
            preferredLanguage = preferredLanguage,
            questionDefaultLanguage = Some(questionDefaultLanguage)
          )
      )
    }
  }
}
