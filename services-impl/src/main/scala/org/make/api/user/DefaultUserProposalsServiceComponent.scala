/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.feature

import org.make.api.technical._
import org.make.core.user.UserId
import org.make.api.user._
import org.make.core.question.QuestionId

import scala.concurrent.Future

trait DefaultUserProposalsServiceComponent extends UserProposalsServiceComponent with ShortenedNames {

  this: PersistentUserProposalsServiceComponent =>

  override lazy val userProposalsService: UserProposalsService = new DefaultUserProposalsService

  class DefaultUserProposalsService extends UserProposalsService {

    override def getCounter(userId: UserId, questionId: QuestionId): Future[Int] =
      persistentUserProposalsService.getCounter(userId, questionId)

    override def incrementCounter(userId: UserId, questionId: QuestionId): Future[Int] =
      persistentUserProposalsService.incrementCounter(userId, questionId)
  }
}
