/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.user

import akka.http.scaladsl.model.ContentType
import akka.stream.scaladsl.Sink
import cats.implicits._
import com.github.t3hnar.bcrypt._
import grizzled.slf4j.Logging
import org.make.api.extensions.{MailJetConfigurationComponent, MakeSettingsComponent}
import org.make.api.partner.PersistentPartnerServiceComponent
import org.make.api.proposal.ProposalServiceComponent
import org.make.api.proposal.PublishedProposalEvent.ReindexProposal
import org.make.api.question.AuthorRequest
import org.make.api.technical.Futures.FutureOfOption
import org.make.api.technical._
import org.make.api.technical.auth.{
  MakeDataHandlerComponent,
  TokenGeneratorComponent,
  TokenResponse,
  UserTokenGeneratorComponent
}
import org.make.api.technical.crm.{CrmServiceComponent, PersistentCrmUserServiceComponent}
import org.make.api.technical.job.JobActor.Protocol.Response.JobAcceptance
import org.make.api.technical.job.JobCoordinatorServiceComponent
import org.make.api.technical.security.SecurityHelper
import org.make.api.technical.storage.Content.FileContent
import org.make.api.technical.storage.StorageServiceComponent
import org.make.api.user.UserExceptions.{EmailAlreadyRegisteredException, EmailNotAllowed}
import org.make.api.user.social.models.UserInfo
import org.make.api.user.validation.UserRegistrationValidatorComponent
import org.make.api.userhistory._
import org.make.core._
import org.make.core.auth.UserRights
import org.make.core.job.Job.JobId.AnonymizeInactiveUsers
import org.make.core.profile.Gender.{Female, Male, Other}
import org.make.core.profile.Profile
import org.make.core.proposal._
import org.make.core.question.QuestionId
import org.make.core.reference.{Country, Language}
import org.make.core.technical.Pagination
import org.make.core.user.Role.RoleCitizen
import org.make.core.user._
import scalaoauth2.provider.AuthInfo

import java.io.File
import java.nio.file.Files
import java.time.{LocalDate, ZonedDateTime}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

trait DefaultUserServiceComponent extends UserServiceComponent with ShortenedNames with Logging {
  this: ActorSystemComponent
    with IdGeneratorComponent
    with MakeDataHandlerComponent
    with UserTokenGeneratorComponent
    with PersistentUserServiceComponent
    with PersistentCrmUserServiceComponent
    with ProposalServiceComponent
    with CrmServiceComponent
    with EventBusServiceComponent
    with TokenGeneratorComponent
    with MakeSettingsComponent
    with UserRegistrationValidatorComponent
    with StorageServiceComponent
    with DownloadServiceComponent
    with UserHistoryCoordinatorServiceComponent
    with JobCoordinatorServiceComponent
    with MailJetConfigurationComponent
    with DateHelperComponent
    with PersistentPartnerServiceComponent =>

  override lazy val userService: UserService = new DefaultUserService

  class DefaultUserService extends UserService {

    val validationTokenExpiresIn: Long = makeSettings.validationTokenExpiresIn.toSeconds
    val resetTokenExpiresIn: Long = makeSettings.resetTokenExpiresIn.toSeconds
    val resetTokenB2BExpiresIn: Long = makeSettings.resetTokenB2BExpiresIn.toSeconds

    private lazy val batchSize: Int = mailJetConfiguration.userListBatchSize

    override def getUser(userId: UserId): Future[Option[User]] = {
      persistentUserService.get(userId)
    }

    override def getPersonality(id: UserId): Future[Option[User]] = {
      persistentUserService.findByUserIdAndUserType(id, UserType.UserTypePersonality)
    }

    override def getUserByEmail(email: String): Future[Option[User]] = {
      persistentUserService.findByEmail(email.trim)
    }

    override def getUserByUserIdAndPassword(userId: UserId, password: Option[String]): Future[Option[User]] = {
      persistentUserService.findByUserIdAndPassword(userId, password)
    }

    override def getUserByEmailAndPassword(email: String, password: String): Future[Option[User]] = {
      persistentUserService.findByEmailAndPassword(email.trim, password)
    }

    override def getUsersByUserIds(ids: Seq[UserId]): Future[Seq[User]] = {
      persistentUserService.findAllByUserIds(ids)
    }

    override def adminFindUsers(
      offset: Pagination.Offset,
      end: Option[Pagination.End],
      sort: Option[String],
      order: Option[Order],
      ids: Option[Seq[UserId]],
      email: Option[String],
      firstName: Option[String],
      lastName: Option[String],
      role: Option[Role],
      userType: Option[UserType]
    ): Future[Seq[User]] = {
      persistentUserService.adminFindUsers(
        offset,
        end,
        sort,
        order,
        ids,
        email.map(_.trim),
        firstName.map(_.trim),
        lastName.map(_.trim),
        role,
        userType
      )
    }

    private def registerUser(
      userRegisterData: UserRegisterData,
      lowerCasedEmail: String,
      profile: Option[Profile],
      hashedVerificationToken: String
    ): Future[User] = {
      val user = User(
        userId = idGenerator.nextUserId(),
        email = lowerCasedEmail,
        firstName = userRegisterData.firstName,
        lastName = userRegisterData.lastName,
        lastIp = userRegisterData.lastIp,
        hashedPassword = userRegisterData.password.map(_.boundedBcrypt),
        enabled = true,
        emailVerified = false,
        lastConnection = Some(dateHelper.now()),
        verificationToken = Some(hashedVerificationToken),
        verificationTokenExpiresAt = Some(dateHelper.now().plusSeconds(validationTokenExpiresIn)),
        resetToken = None,
        resetTokenExpiresAt = None,
        roles = userRegisterData.roles,
        country = userRegisterData.country,
        language = userRegisterData.language,
        profile = profile,
        availableQuestions = userRegisterData.availableQuestions,
        userType = UserType.UserTypeUser,
        publicProfile = userRegisterData.publicProfile,
        privacyPolicyApprovalDate = userRegisterData.privacyPolicyApprovalDate
      )

      persistentUserService.persist(user)
    }

    private def persistPersonality(
      personalityRegisterData: PersonalityRegisterData,
      lowerCasedEmail: String,
      profile: Option[Profile],
      resetToken: String
    ): Future[User] = {

      val user = User(
        userId = idGenerator.nextUserId(),
        email = lowerCasedEmail,
        firstName = personalityRegisterData.firstName,
        lastName = personalityRegisterData.lastName,
        lastIp = None,
        hashedPassword = None,
        enabled = true,
        emailVerified = true,
        lastConnection = None,
        verificationToken = None,
        verificationTokenExpiresAt = None,
        resetToken = Some(resetToken),
        resetTokenExpiresAt = Some(dateHelper.now().plusSeconds(resetTokenB2BExpiresIn)),
        roles = Seq(Role.RoleCitizen),
        country = personalityRegisterData.country,
        language = personalityRegisterData.language,
        profile = profile,
        availableQuestions = Seq.empty,
        userType = UserType.UserTypePersonality,
        publicProfile = true,
        privacyPolicyApprovalDate = Some(DateHelper.now())
      )

      persistentUserService.persist(user)
    }

    private def persistVirtualUser(
      virtualUserRegisterData: VirtualUserRegisterData,
      lowerCasedEmail: String
    ): Future[User] = {
      val dateOfBirth = virtualUserRegisterData.age.map(age => LocalDate.now().minusYears(age).withDayOfYear(1))
      val profile = Profile.parseProfile(dateOfBirth = dateOfBirth)

      val user = User(
        userId = idGenerator.nextUserId(),
        email = lowerCasedEmail,
        firstName = virtualUserRegisterData.firstName,
        lastName = None,
        lastIp = None,
        hashedPassword = None,
        enabled = true,
        emailVerified = false,
        lastConnection = None,
        verificationToken = None,
        verificationTokenExpiresAt = None,
        resetToken = None,
        resetTokenExpiresAt = None,
        roles = Seq(Role.RoleCitizen),
        country = virtualUserRegisterData.country,
        language = virtualUserRegisterData.language,
        profile = profile,
        availableQuestions = Seq.empty,
        userType = UserType.UserTypeVirtual,
        privacyPolicyApprovalDate = Some(DateHelper.now())
      )

      persistentUserService.persist(user)
    }

    private def persistExternalUser(
      externalUserRegisterData: ExternalUserRegisterData,
      lowerCasedEmail: String
    ): Future[User] = {
      val dateOfBirth = externalUserRegisterData.age.map(age => LocalDate.now().minusYears(age).withDayOfYear(1))
      val profile = Profile.parseProfile(dateOfBirth = dateOfBirth)

      val user = User(
        userId = idGenerator.nextUserId(),
        email = lowerCasedEmail,
        firstName = externalUserRegisterData.firstName,
        lastName = None,
        lastIp = None,
        hashedPassword = None,
        enabled = true,
        emailVerified = false,
        lastConnection = None,
        verificationToken = None,
        verificationTokenExpiresAt = None,
        resetToken = None,
        resetTokenExpiresAt = None,
        roles = Seq(Role.RoleCitizen),
        country = externalUserRegisterData.country,
        language = externalUserRegisterData.language,
        profile = profile,
        availableQuestions = Seq.empty,
        userType = UserType.UserTypeExternal,
        privacyPolicyApprovalDate = Some(DateHelper.now())
      )

      persistentUserService.persist(user)
    }

    private def validateAccountCreation(
      emailExists: Boolean,
      canRegister: Boolean,
      lowerCasedEmail: String
    ): Future[Unit] = {
      if (emailExists) {
        Future.failed(EmailAlreadyRegisteredException(lowerCasedEmail))
      } else if (!canRegister) {
        Future.failed(EmailNotAllowed(lowerCasedEmail))
      } else {
        Future.unit
      }
    }

    private def generateVerificationToken(): Future[String] = {
      userTokenGenerator.generateVerificationToken().map {
        case (_, token) => token
      }
    }

    private def generateResetToken(): Future[String] = {
      userTokenGenerator.generateResetToken().map {
        case (_, token) => token
      }
    }

    private def generateReconnectToken(): Future[String] = {
      userTokenGenerator.generateReconnectToken().map {
        case (_, token) => token
      }
    }

    override def register(userRegisterData: UserRegisterData, requestContext: RequestContext): Future[User] = {
      val trimmedRegisterData = userRegisterData.trim
      val lowerCasedEmail: String = trimmedRegisterData.email.toLowerCase()
      val profile: Option[Profile] =
        Profile.parseProfile(
          dateOfBirth = trimmedRegisterData.dateOfBirth,
          profession = trimmedRegisterData.profession,
          postalCode = trimmedRegisterData.postalCode,
          gender = trimmedRegisterData.gender,
          crmCountry = trimmedRegisterData.crmCountry,
          crmLanguage = trimmedRegisterData.crmLanguage,
          socioProfessionalCategory = trimmedRegisterData.socioProfessionalCategory,
          registerQuestionId = trimmedRegisterData.questionId,
          optInNewsletter = trimmedRegisterData.optIn.getOrElse(true),
          optInPartner = trimmedRegisterData.optInPartner,
          politicalParty = trimmedRegisterData.politicalParty,
          website = trimmedRegisterData.website
        )

      val result = for {
        emailExists             <- persistentUserService.emailExists(lowerCasedEmail)
        canRegister             <- userRegistrationValidator.canRegister(trimmedRegisterData)
        _                       <- validateAccountCreation(emailExists, canRegister, lowerCasedEmail)
        hashedVerificationToken <- generateVerificationToken()
        user                    <- registerUser(trimmedRegisterData, lowerCasedEmail, profile, hashedVerificationToken)
      } yield user

      result.map { user =>
        eventBusService.publish(
          UserRegisteredEvent(
            connectedUserId = Some(user.userId),
            userId = user.userId,
            requestContext = requestContext,
            firstName = user.firstName,
            country = user.country,
            optInPartner = user.profile.flatMap(_.optInPartner),
            registerQuestionId = user.profile.flatMap(_.registerQuestionId),
            eventDate = dateHelper.now(),
            eventId = Some(idGenerator.nextEventId())
          )
        )
        user
      }
    }

    override def registerPersonality(
      personalityRegisterData: PersonalityRegisterData,
      requestContext: RequestContext
    ): Future[User] = {
      val trimmedRegisterData = personalityRegisterData.trim
      val lowerCasedEmail: String = trimmedRegisterData.email.toLowerCase()
      val profile: Option[Profile] =
        Profile.parseProfile(
          gender = trimmedRegisterData.gender,
          genderName = trimmedRegisterData.genderName,
          description = trimmedRegisterData.description,
          avatarUrl = trimmedRegisterData.avatarUrl,
          optInNewsletter = false,
          optInPartner = Some(false),
          politicalParty = trimmedRegisterData.politicalParty,
          website = trimmedRegisterData.website
        )

      val result = for {
        emailExists <- persistentUserService.emailExists(lowerCasedEmail)
        _           <- validateAccountCreation(emailExists, canRegister = true, lowerCasedEmail)
        resetToken  <- generateResetToken()
        user        <- persistPersonality(trimmedRegisterData, lowerCasedEmail, profile, resetToken)
      } yield user

      result.map { user =>
        eventBusService.publish(
          PersonalityRegisteredEvent(
            connectedUserId = Some(user.userId),
            userId = user.userId,
            requestContext = requestContext,
            email = user.email,
            country = user.country,
            eventDate = dateHelper.now(),
            eventId = Some(idGenerator.nextEventId())
          )
        )
        user
      }
    }

    override def registerVirtualUser(
      virtualUserRegisterData: VirtualUserRegisterData,
      requestContext: RequestContext
    ): Future[User] = {
      val trimmedRegisterData = virtualUserRegisterData.trim
      val lowerCasedEmail: String = trimmedRegisterData.email.toLowerCase()

      for {
        emailExists <- persistentUserService.emailExists(lowerCasedEmail)
        _           <- validateAccountCreation(emailExists, canRegister = true, lowerCasedEmail)
        user        <- persistVirtualUser(trimmedRegisterData, lowerCasedEmail)
      } yield user
    }

    override def registerExternalUser(
      externalUserRegisterData: ExternalUserRegisterData,
      requestContext: RequestContext
    ): Future[User] = {
      val trimmedRegisterData = externalUserRegisterData.trim
      val lowerCasedEmail: String = trimmedRegisterData.email.toLowerCase()

      for {
        emailExists <- persistentUserService.emailExists(lowerCasedEmail)
        _           <- validateAccountCreation(emailExists, canRegister = true, lowerCasedEmail)
        user        <- persistExternalUser(trimmedRegisterData, lowerCasedEmail)
      } yield user
    }

    override def createOrUpdateUserFromSocial(
      userInfo: UserInfo,
      questionId: Option[QuestionId],
      country: Country,
      language: Language,
      crmCountry: Country,
      crmLanguage: Language,
      requestContext: RequestContext,
      privacyPolicyApprovalDate: Option[ZonedDateTime],
      optIn: Option[Boolean]
    ): Future[(User, Boolean)] = {

      userInfo.email
        .filter(_ != "")
        .map(_.trim.toLowerCase())
        .map { lowerCasedEmail =>
          persistentUserService.findByEmail(lowerCasedEmail).flatMap {
            case Some(user) =>
              updateUserFromSocial(user, userInfo, requestContext.ipAddress, privacyPolicyApprovalDate)
                .map((_, false))
            case None =>
              createUserFromSocial(
                lowerCasedEmail,
                requestContext,
                userInfo,
                country,
                language,
                crmCountry,
                crmLanguage,
                questionId,
                privacyPolicyApprovalDate,
                optIn
              ).map((_, true))
          }
        }
        .getOrElse {
          logger.error(
            s"We couldn't find any email on social login. UserInfo: $userInfo, requestContext: $requestContext"
          )
          Future
            .failed(ValidationFailedError(Seq(ValidationError("email", "missing", Some("No email found for user")))))
        }
    }

    private def createUserFromSocial(
      lowerCasedEmail: String,
      requestContext: RequestContext,
      userInfo: UserInfo,
      country: Country,
      language: Language,
      crmCountry: Country,
      crmLanguage: Language,
      questionId: Option[QuestionId],
      privacyPolicyApprovalDate: Option[ZonedDateTime],
      optIn: Option[Boolean]
    ): Future[User] = {

      val profile: Option[Profile] =
        Profile.parseProfile(
          facebookId = userInfo.facebookId,
          googleId = userInfo.googleId,
          avatarUrl = userInfo.picture,
          gender = userInfo.gender.map {
            case "male"   => Male
            case "female" => Female
            case _        => Other
          },
          genderName = userInfo.gender,
          crmCountry = crmCountry,
          crmLanguage = crmLanguage,
          registerQuestionId = questionId,
          dateOfBirth = userInfo.dateOfBirth,
          optInNewsletter = optIn.getOrElse(true)
        )

      val user = User(
        userId = idGenerator.nextUserId(),
        email = lowerCasedEmail,
        firstName = userInfo.firstName.map(_.trim),
        lastName = None,
        lastIp = requestContext.ipAddress,
        hashedPassword = None,
        enabled = true,
        emailVerified = true,
        lastConnection = Some(dateHelper.now()),
        verificationToken = None,
        verificationTokenExpiresAt = None,
        resetToken = None,
        resetTokenExpiresAt = None,
        roles = Seq(Role.RoleCitizen),
        country = country,
        language = language,
        profile = profile,
        availableQuestions = Seq.empty,
        userType = UserType.UserTypeUser,
        privacyPolicyApprovalDate = privacyPolicyApprovalDate
      )

      persistentUserService.persist(user).map { user =>
        publishCreateEventsFromSocial(user = user, requestContext = requestContext)
        user
      }
    }

    private def updateUserFromSocial(
      user: User,
      userInfo: UserInfo,
      clientIp: Option[String],
      privacyPolicyApprovalDate: Option[ZonedDateTime]
    ): Future[User] = {
      val hashedPassword = if (!user.emailVerified) None else user.hashedPassword

      val profile: Option[Profile] = user.profile.orElse(Profile.parseProfile())
      val updatedProfile: Option[Profile] = profile.map(
        _.copy(facebookId = userInfo.facebookId, googleId = userInfo.googleId, gender = userInfo.gender.map {
          case "male"   => Male
          case "female" => Female
          case _        => Other
        }, genderName = userInfo.gender, dateOfBirth = profile.flatMap(_.dateOfBirth).orElse(userInfo.dateOfBirth))
      )
      val updatedUser: User =
        user.copy(
          firstName = userInfo.firstName.map(_.trim),
          lastIp = clientIp,
          profile = updatedProfile,
          hashedPassword = hashedPassword,
          emailVerified = true,
          lastConnection = Some(DateHelper.now()),
          privacyPolicyApprovalDate = privacyPolicyApprovalDate.orElse(user.privacyPolicyApprovalDate)
        )

      persistentUserService.updateSocialUser(updatedUser).map { userUpdated =>
        if (userUpdated) updatedUser else user
      }
    }

    private def publishCreateEventsFromSocial(user: User, requestContext: RequestContext): Unit = {
      eventBusService.publish(
        UserRegisteredEvent(
          connectedUserId = Some(user.userId),
          userId = user.userId,
          requestContext = requestContext,
          firstName = user.firstName,
          country = user.country,
          isSocialLogin = true,
          optInPartner = user.profile.flatMap(_.optInPartner),
          registerQuestionId = user.profile.flatMap(_.registerQuestionId),
          eventDate = dateHelper.now(),
          eventId = Some(idGenerator.nextEventId())
        )
      )
      eventBusService.publish(
        UserValidatedAccountEvent(
          userId = user.userId,
          country = user.country,
          requestContext = requestContext,
          isSocialLogin = true,
          eventDate = dateHelper.now(),
          eventId = Some(idGenerator.nextEventId())
        )
      )
      user.profile.flatMap(_.avatarUrl).foreach { avatarUrl =>
        eventBusService.publish(
          UserUploadAvatarEvent(
            connectedUserId = Some(user.userId),
            userId = user.userId,
            country = user.country,
            requestContext = requestContext,
            avatarUrl = avatarUrl,
            eventDate = dateHelper.now(),
            eventId = Some(idGenerator.nextEventId())
          )
        )
      }
    }

    override def requestPasswordReset(userId: UserId): Future[Boolean] = {
      for {
        resetToken <- generateResetToken()
        result <- persistentUserService.requestResetPassword(
          userId,
          resetToken,
          Some(dateHelper.now().plusSeconds(resetTokenExpiresIn))
        )
      } yield result
    }

    override def updatePassword(userId: UserId, resetToken: Option[String], password: String): Future[Boolean] = {
      persistentUserService.updatePassword(userId, resetToken, password.boundedBcrypt)
    }

    override def validateEmail(user: User, verificationToken: String): Future[TokenResponse] = {

      for {
        emailVerified <- persistentUserService.validateEmail(verificationToken)
        accessToken <- oauth2DataHandler.createAccessToken(authInfo = AuthInfo(
          user = UserRights(user.userId, user.roles, user.availableQuestions, emailVerified),
          clientId = None,
          scope = None,
          redirectUri = None
        )
        )
      } yield {
        TokenResponse.fromAccessToken(accessToken)
      }
    }

    override def updateOptInNewsletter(userId: UserId, optInNewsletter: Boolean): Future[Boolean] = {
      persistentUserService.updateOptInNewsletter(userId, optInNewsletter).map { result =>
        if (result) {
          eventBusService.publish(
            UserUpdatedOptInNewsletterEvent(
              connectedUserId = Some(userId),
              eventDate = dateHelper.now(),
              userId = userId,
              requestContext = RequestContext.empty,
              optInNewsletter = optInNewsletter,
              eventId = Some(idGenerator.nextEventId())
            )
          )
        }
        result
      }
    }

    override def updateIsHardBounce(userId: UserId, isHardBounce: Boolean): Future[Boolean] = {
      persistentUserService.updateIsHardBounce(userId, isHardBounce)
    }

    override def updateOptInNewsletter(email: String, optInNewsletter: Boolean): Future[Boolean] = {
      getUserByEmail(email).flatMap { maybeUser =>
        persistentUserService.updateOptInNewsletter(email, optInNewsletter).map { result =>
          if (result) {
            maybeUser.foreach { user =>
              val userId = user.userId
              eventBusService.publish(
                UserUpdatedOptInNewsletterEvent(
                  connectedUserId = Some(userId),
                  userId = userId,
                  eventDate = dateHelper.now(),
                  requestContext = RequestContext.empty,
                  optInNewsletter = optInNewsletter,
                  eventId = Some(idGenerator.nextEventId())
                )
              )
            }
          }
          result
        }
      }
    }

    override def updateIsHardBounce(email: String, isHardBounce: Boolean): Future[Boolean] = {
      persistentUserService.updateIsHardBounce(email, isHardBounce)
    }

    override def updateLastMailingError(email: String, lastMailingError: Option[MailingErrorLog]): Future[Boolean] = {
      persistentUserService.updateLastMailingError(email, lastMailingError)
    }

    override def updateLastMailingError(userId: UserId, lastMailingError: Option[MailingErrorLog]): Future[Boolean] = {
      persistentUserService.updateLastMailingError(userId, lastMailingError)
    }

    override def getUsersWithoutRegisterQuestion: Future[Seq[User]] = {
      persistentUserService.findUsersWithoutRegisterQuestion
    }

    private def updateProposalVotedByOrganisation(user: User): Future[Unit] = {
      if (user.userType == UserType.UserTypeOrganisation) {
        proposalService
          .searchProposalsVotedByUser(
            userId = user.userId,
            filterVotes = None,
            filterQualifications = None,
            preferredLanguage = None,
            sort = None,
            limit = None,
            offset = None,
            RequestContext.empty
          )
          .map(
            result =>
              result.results.foreach(
                proposal =>
                  eventBusService
                    .publish(
                      ReindexProposal(
                        proposal.id,
                        dateHelper.now(),
                        RequestContext.empty,
                        Some(idGenerator.nextEventId())
                      )
                    )
              )
          )
      } else {
        Future.unit
      }
    }

    private def updateProposalsSubmitByUser(user: User, requestContext: RequestContext): Future[Unit] = {
      proposalService
        .searchForUser(
          userId = Some(user.userId),
          query = SearchQuery(filters = Some(
            SearchFilters(
              users = Some(UserSearchFilter(userIds = Seq(user.userId))),
              status = Some(StatusSearchFilter(ProposalStatus.values))
            )
          )
          ),
          requestContext = requestContext,
          preferredLanguage = None,
          questionDefaultLanguage = None
        )
        .map(
          result =>
            result.results.foreach(
              proposal =>
                eventBusService
                  .publish(
                    ReindexProposal(
                      proposal.id,
                      dateHelper.now(),
                      RequestContext.empty,
                      Some(idGenerator.nextEventId())
                    )
                  )
            )
        )
    }

    private def handleEmailChangeIfNecessary(user: User, oldEmail: Option[String]): Future[Unit] = {
      oldEmail match {
        case Some(oldEmail) if oldEmail != user.email =>
          for {
            _ <- persistentUserService.findByEmail(user.email).flatMap {
              case None => Future.unit
              case _    => Future.failed(EmailAlreadyRegisteredException(user.email))
            }
            _ <- crmService.deleteRecipient(oldEmail)
          } yield {}
        case _ => Future.unit
      }
    }

    override def update(user: User, requestContext: RequestContext): Future[User] = {
      for {
        previousUser <- persistentUserService.get(user.userId)
        updatedUser  <- persistentUserService.updateUser(user)
        _            <- handleEmailChangeIfNecessary(user, previousUser.map(_.email))
        _            <- updateProposalVotedByOrganisation(updatedUser)
        _            <- updateProposalsSubmitByUser(updatedUser, requestContext)
      } yield updatedUser
    }

    private def updatePersonalityEmail(
      personality: User,
      moderatorId: Option[UserId],
      newEmail: Option[String],
      oldEmail: String,
      requestContext: RequestContext
    ): Unit =
      newEmail.foreach(
        email =>
          eventBusService.publish(
            PersonalityEmailChangedEvent(
              connectedUserId = moderatorId,
              userId = personality.userId,
              requestContext = requestContext,
              country = personality.country,
              eventDate = dateHelper.now(),
              oldEmail = oldEmail,
              newEmail = email,
              eventId = Some(idGenerator.nextEventId())
            )
          )
      )

    override def updatePersonality(
      personality: User,
      moderatorId: Option[UserId],
      oldEmail: String,
      requestContext: RequestContext
    ): Future[User] = {

      val newEmail: Option[String] = personality.email match {
        case email if email.toLowerCase == oldEmail.toLowerCase => None
        case email                                              => Some(email)
      }

      val updateUserWithNewEmailIfNecessary = persistentUserService
        .updateUser(personality)
        .map(user => {
          updatePersonalityEmail(user, moderatorId, newEmail, oldEmail, requestContext)
          user
        })

      for {
        updatedUser <- updateUserWithNewEmailIfNecessary
        _           <- updateProposalVotedByOrganisation(updatedUser)
        _           <- updateProposalsSubmitByUser(updatedUser, requestContext)
      } yield updatedUser
    }

    override def anonymize(
      user: User,
      adminId: UserId,
      requestContext: RequestContext,
      mode: Anonymization
    ): Future[Unit] = {

      val anonymizedUser: User = user.copy(
        email = s"${user.userId.value.trim}@example.com",
        firstName = if (mode == Anonymization.Explicit) Some("DELETE_REQUESTED") else user.firstName.map(_.trim),
        lastName = Some("DELETE_REQUESTED"),
        lastIp = None,
        hashedPassword = None,
        enabled = false,
        emailVerified = false,
        lastConnection = None,
        verificationToken = None,
        verificationTokenExpiresAt = None,
        resetToken = None,
        resetTokenExpiresAt = None,
        roles = Seq(RoleCitizen),
        profile = Profile.parseProfile(optInNewsletter = false),
        isHardBounce = true,
        lastMailingError = None,
        organisationName = None,
        userType = if (mode == Anonymization.Explicit) UserType.UserTypeAnonymous else user.userType,
        createdAt = Some(dateHelper.now()),
        publicProfile = false
      )

      def unlinkUser(user: User): Future[Unit] = {
        val unlinkPartners = user.userType match {
          case UserType.UserTypeOrganisation =>
            persistentPartnerService
              .find(
                offset = Pagination.Offset.zero,
                end = None,
                sort = None,
                order = None,
                questionId = None,
                organisationId = Some(user.userId),
                partnerKind = None
              )
              .flatMap { partners =>
                Future.traverse(partners) { partner =>
                  persistentPartnerService.modify(partner.copy(organisationId = None))
                }
              }
          case _ => Future.unit
        }
        for {
          _ <- unlinkPartners
          _ <- persistentUserService.removeAnonymizedUserFromFollowedUserTable(user.userId)
        } yield {}
      }

      val futureDelete: Future[Unit] = for {
        _ <- persistentUserService.updateUser(anonymizedUser)
        _ <- unlinkUser(user)
        _ <- userHistoryCoordinatorService.delete(user.userId)
        _ <- updateProposalsSubmitByUser(user, requestContext)
      } yield {}
      futureDelete.as(
        eventBusService.publish(
          UserAnonymizedEvent(
            connectedUserId = Some(adminId),
            userId = user.userId,
            requestContext = requestContext,
            country = user.country,
            eventDate = dateHelper.now(),
            adminId = adminId,
            mode = mode,
            eventId = Some(idGenerator.nextEventId())
          )
        )
      )
    }

    override def anonymizeInactiveUsers(adminId: UserId, requestContext: RequestContext): Future[JobAcceptance] = {
      val startTime: Long = System.currentTimeMillis()

      jobCoordinatorService.start(AnonymizeInactiveUsers) { _ =>
        val anonymizeUsers = StreamUtils
          .asyncPageToPageSource(persistentCrmUserService.findInactiveUsers(_, batchSize))
          .mapAsync(1) { crmUsers =>
            persistentUserService.findAllByUserIds(crmUsers.map(user => UserId(user.userId)))
          }
          .mapConcat(identity)
          .mapAsync(1) { user =>
            anonymize(user, adminId, requestContext, Anonymization.Automatic)
          }
          .runWith(Sink.ignore)
          .void

        anonymizeUsers.onComplete {
          case Failure(exception) =>
            logger.error(s"Inactive users anonymization failed:", exception)
          case Success(_) =>
            logger.info(s"Inactive users anonymization succeeded in ${System.currentTimeMillis() - startTime}ms")
        }

        anonymizeUsers
      }
    }

    override def getFollowedUsers(userId: UserId): Future[Seq[UserId]] = {
      persistentUserService.getFollowedUsers(userId).map(_.map(UserId(_)))
    }

    override def followUser(followedUserId: UserId, userId: UserId, requestContext: RequestContext): Future[UserId] = {
      persistentUserService.followUser(followedUserId, userId).map(_ => followedUserId).map { value =>
        eventBusService.publish(
          UserFollowEvent(
            connectedUserId = Some(userId),
            eventDate = dateHelper.now(),
            userId = userId,
            requestContext = requestContext,
            followedUserId = followedUserId,
            eventId = Some(idGenerator.nextEventId())
          )
        )
        value
      }
    }

    override def unfollowUser(
      followedUserId: UserId,
      userId: UserId,
      requestContext: RequestContext
    ): Future[UserId] = {
      persistentUserService.unfollowUser(followedUserId, userId).map(_ => followedUserId).map { value =>
        eventBusService.publish(
          UserUnfollowEvent(
            connectedUserId = Some(userId),
            userId = userId,
            eventDate = dateHelper.now(),
            requestContext = requestContext,
            unfollowedUserId = followedUserId,
            eventId = Some(idGenerator.nextEventId())
          )
        )
        value
      }
    }

    override def retrieveOrCreateVirtualUser(userInfo: AuthorRequest, country: Country): Future[User] = {
      // Take only 50 chars to avoid having values too large for the column
      val fullHash: String = tokenGenerator.tokenToHash(s"$userInfo")
      val hash = fullHash.substring(0, Math.min(50, fullHash.length)).toLowerCase()
      val email = s"$hash@example.com"
      getUserByEmail(email).flatMap {
        case Some(user) => Future.successful(user)
        case None =>
          userService
            .registerVirtualUser(
              VirtualUserRegisterData(
                email = email,
                firstName = Some(userInfo.firstName),
                age = userInfo.age,
                country = country,
                language = Language("fr")
              ),
              RequestContext.empty
            )
      }
    }

    override def retrieveOrCreateExternalUser(
      email: String,
      userInfo: AuthorRequest,
      country: Country,
      lineNumber: Int
    ): Future[User] = {
      getUserByEmail(email.trim).flatMap {
        case Some(user) if !user.firstName.contains(userInfo.firstName) =>
          Future.failed(
            ValidationFailedError(
              Seq(
                ValidationError(
                  "firstName",
                  "unexpected_value",
                  Some(s"users with the same externalUserId should have the same firstName on line $lineNumber")
                )
              )
            )
          )
        case Some(user)
            if user.profile
              .flatMap(_.dateOfBirth)
              .isEmpty && userInfo.age.isEmpty =>
          Future.successful(user)
        case Some(user)
            if !user.profile
              .flatMap(_.dateOfBirth)
              .exists(userInfo.age.map(age => LocalDate.now().minusYears(age).withDayOfYear(1)).contains) =>
          Future.failed(
            ValidationFailedError(
              Seq(
                ValidationError(
                  "age",
                  "unexpected_value",
                  Some(s"users with the same externalUserId should have the same age on line $lineNumber")
                )
              )
            )
          )
        case Some(user) =>
          Future.successful(user)
        case None =>
          userService
            .registerExternalUser(
              ExternalUserRegisterData(
                email = email.trim,
                firstName = Some(userInfo.firstName),
                age = userInfo.age,
                country = country,
                language = Language("fr")
              ),
              RequestContext.empty
            )
      }
    }

    override def adminCountUsers(
      ids: Option[Seq[UserId]],
      email: Option[String],
      firstName: Option[String],
      lastName: Option[String],
      role: Option[Role],
      userType: Option[UserType]
    ): Future[Int] = {
      persistentUserService.adminCountUsers(
        ids,
        email.map(_.trim),
        firstName.map(_.trim),
        lastName.map(_.trim),
        role,
        userType
      )
    }

    private def getConnectionModes(user: User): Seq[ConnectionMode] = {
      Map(
        ConnectionMode.Facebook -> user.profile.flatMap(_.facebookId),
        ConnectionMode.Google -> user.profile.flatMap(_.googleId),
        ConnectionMode.Mail -> user.hashedPassword
      ).collect {
        case (mode, Some(_)) => mode
      }.toSeq
    }

    override def reconnectInfo(userId: UserId): Future[Option[ReconnectInfo]] = {
      val futureReconnectInfo = for {
        user           <- persistentUserService.get(userId)
        reconnectToken <- generateReconnectToken()
        _              <- persistentUserService.updateReconnectToken(userId, reconnectToken, dateHelper.now())
      } yield (user, reconnectToken)

      futureReconnectInfo.map {
        case (maybeUser, reconnectToken) =>
          maybeUser.map { user =>
            val hiddenEmail = SecurityHelper.anonymizeEmail(user.email)
            ReconnectInfo(
              reconnectToken,
              user.firstName,
              user.profile.flatMap(_.avatarUrl),
              hiddenEmail,
              getConnectionModes(user)
            )
          }
      }
    }

    override def changeEmailVerificationTokenIfNeeded(userId: UserId): Future[Option[User]] = {
      getUser(userId).flatMap {
        case Some(user)
            // if last verification token was changed more than 10 minutes ago
            if user.verificationTokenExpiresAt
              .forall(_.minusSeconds(validationTokenExpiresIn).plusMinutes(10).isBefore(dateHelper.now()))
              && !user.emailVerified =>
          userTokenGenerator.generateVerificationToken().flatMap {
            case (_, token) =>
              persistentUserService
                .updateUser(
                  user.copy(
                    verificationToken = Some(token),
                    verificationTokenExpiresAt = Some(dateHelper.now().plusSeconds(validationTokenExpiresIn))
                  )
                )
                .map(Some(_))
          }
        case _ => Future.successful(None)
      }
    }

    def changeAvatarForUser(
      userId: UserId,
      avatarUrl: String,
      requestContext: RequestContext,
      eventDate: ZonedDateTime
    ): Future[Unit] = {
      def extension(contentType: ContentType): String = contentType.mediaType.subType
      def destFn(contentType: ContentType): File =
        Files.createTempFile("user-upload-avatar", s".${extension(contentType)}").toFile

      downloadService
        .downloadImage(avatarUrl, destFn)
        .flatMap {
          case (contentType, tempFile) =>
            tempFile.deleteOnExit()
            storageService
              .uploadUserAvatar(extension(contentType), contentType.value, FileContent(tempFile))
              .map(Option.apply)
        }
        .recover {
          case _: ImageUnavailable => None
        }
        .flatMap(
          path =>
            getUser(userId).flatMap {
              case Some(user) =>
                val newProfile: Option[Profile] = user.profile match {
                  case Some(profile) => Some(profile.copy(avatarUrl = path))
                  case None          => Profile.parseProfile(avatarUrl = path)
                }
                update(user.copy(profile = newProfile), requestContext).map(_ => path)
              case None =>
                logger.warn(s"Could not find user $userId to update avatar")
                Future.successful(path)
            }
        )
        .as(
          userHistoryCoordinatorService.logHistory(
            LogUserUploadedAvatarEvent(
              userId = userId,
              requestContext = requestContext,
              action = UserAction(
                date = eventDate,
                actionType = LogUserUploadedAvatarEvent.action,
                arguments = UploadedAvatar(avatarUrl = avatarUrl)
              )
            )
          )
        )

    }

    override def adminUpdateUserEmail(user: User, email: String): Future[Unit] = {
      persistentUserService.updateUser(user.copy(email = email)).void
    }

    private val FailedLoginAttemptsBeforeBan = 5
    private val LoginBanDuration = 5 // Minutes

    override def isThrottled(username: String): Future[Boolean] =
      getUserByEmail(username)
        .flattenOrFail(s"User $username does not exist")
        .map(
          user =>
            user.connectionAttemptsSinceLastSuccessful > FailedLoginAttemptsBeforeBan &&
              user.lastFailedConnectionAttempt.exists(ZonedDateTime.now().minusMinutes(LoginBanDuration).isBefore)
        )

    override def registerFailedConnectionAttempt(username: String): Future[Unit] =
      getUserByEmail(username).flatMap({
        case None => Future.unit
        case Some(previousUser) =>
          val updatedUser = previousUser.copy(
            connectionAttemptsSinceLastSuccessful = previousUser.connectionAttemptsSinceLastSuccessful + 1,
            lastFailedConnectionAttempt = Some(ZonedDateTime.now())
          )
          persistentUserService.updateUser(updatedUser).void
      })
  }
}
