/*
 *  Make.org Core API
 *  Copyright (C) 2018 Make.org
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.make.api.views

import java.time.ZonedDateTime
import java.util.UUID
import eu.timepit.refined.auto._
import org.make.api.MakeUnitTest
import org.make.api.operation._
import org.make.api.post.{PostService, PostServiceComponent}
import org.make.api.question.QuestionOfOperationResponse
import org.make.api.user.{UserService, UserServiceComponent}
import org.make.api.views.Highlights
import org.make.core.technical.{Multilingual, Pagination}
import org.make.core.operation.indexed.{IndexedOperationOfQuestion, OperationOfQuestionSearchResult}
import org.make.core.operation._
import org.make.core.post.PostId
import org.make.core.post.indexed._
import org.make.core.question.{Question, QuestionId}
import org.make.core.reference.{Country, Language}
import org.make.core.user.{Role, UserId, UserType}
import org.make.core.{DateHelper, Order}
import org.scalatest.concurrent.PatienceConfiguration.Timeout

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

class HomeViewServiceComponentTest
    extends MakeUnitTest
    with DefaultHomeViewServiceComponent
    with OperationOfQuestionSearchEngineComponent
    with UserServiceComponent
    with PostServiceComponent {

  override val elasticsearchOperationOfQuestionAPI: OperationOfQuestionSearchEngine =
    mock[OperationOfQuestionSearchEngine]
  override val userService: UserService = mock[UserService]
  override val postService: PostService = mock[PostService]

  val userId: UserId = UserId(UUID.randomUUID().toString)
  val now: ZonedDateTime = DateHelper.now()
  val defaultOperationId: OperationId = OperationId("default")
  val defaultQuestionId: QuestionId = QuestionId("default")

  val operation1: SimpleOperation =
    simpleOperation(id = OperationId("ope1"), slug = "ope1", operationKind = OperationKind.BusinessConsultation)
  val operation2: SimpleOperation =
    simpleOperation(id = OperationId("ope2"), slug = "ope2", operationKind = OperationKind.BusinessConsultation)
  val operation3: SimpleOperation =
    simpleOperation(id = OperationId("ope3"), slug = "ope3", operationKind = OperationKind.GreatCause)
  val operation4: SimpleOperation =
    simpleOperation(id = OperationId("ope4"), slug = "ope4", operationKind = OperationKind.PrivateConsultation)
  val operation5: SimpleOperation =
    simpleOperation(id = OperationId("ope5"), slug = "ope5", operationKind = OperationKind.BusinessConsultation)
  val operation6: SimpleOperation =
    simpleOperation(id = OperationId("ope6"), slug = "ope6", operationKind = OperationKind.BusinessConsultation)
  val question1: Question = question(
    id = QuestionId("question1"),
    slug = "question1",
    questions = Multilingual.fromDefault("question 1 ?"),
    operationId = Some(operation1.operationId)
  )
  val question2: Question =
    question(id = QuestionId("question2"), slug = "question2", operationId = Some(operation2.operationId))
  val question3: Question =
    question(id = QuestionId("question3"), slug = "question3", operationId = Some(operation3.operationId))
  val question4: Question =
    question(id = QuestionId("question4"), slug = "question4", operationId = Some(operation4.operationId))
  val question5: Question =
    question(id = QuestionId("question5"), slug = "question5", operationId = Some(operation5.operationId))
  val question6: Question =
    question(id = QuestionId("question6"), slug = "question6", operationId = Some(operation6.operationId))
  val question7: Question =
    question(id = QuestionId("question7"), slug = "question7", operationId = Some(operation6.operationId))
  val question8: Question =
    question(id = QuestionId("question8"), slug = "question8", operationId = Some(operation6.operationId))
  val operationOfQuestion1: IndexedOperationOfQuestion =
    indexedOperationOfQuestion(
      questionId = question1.questionId,
      operationId = operation1.operationId,
      startDate = now.minusDays(20),
      endDate = now.plusDays(10),
      status = OperationOfQuestion.Status.Finished
    )
  val operationOfQuestion2: IndexedOperationOfQuestion =
    indexedOperationOfQuestion(
      questionId = question2.questionId,
      operationId = operation2.operationId,
      status = OperationOfQuestion.Status.Open,
      startDate = now.minusDays(20),
      endDate = now.plusDays(10),
      featured = false
    )
  val operationOfQuestion3: IndexedOperationOfQuestion =
    indexedOperationOfQuestion(
      questionId = question3.questionId,
      operationId = operation3.operationId,
      endDate = now.plusDays(11),
      startDate = now.minusDays(20),
      status = OperationOfQuestion.Status.Open
    )
  val operationOfQuestion4: IndexedOperationOfQuestion =
    indexedOperationOfQuestion(
      questionId = question4.questionId,
      operationId = operation4.operationId,
      startDate = now.minusDays(20),
      endDate = now.plusDays(10),
      status = OperationOfQuestion.Status.Finished,
      featured = false
    )
  val operationOfQuestion5: IndexedOperationOfQuestion =
    indexedOperationOfQuestion(
      questionId = question5.questionId,
      operationId = operation5.operationId,
      startDate = now.plusDays(5),
      endDate = now.plusDays(10),
      status = OperationOfQuestion.Status.Upcoming,
      featured = false
    )
  val operationOfQuestion6: IndexedOperationOfQuestion =
    indexedOperationOfQuestion(
      questionId = question6.questionId,
      operationId = operation6.operationId,
      startDate = now.minusDays(8),
      endDate = now.minusDays(1),
      status = OperationOfQuestion.Status.Upcoming,
      featured = false
    )
  val operationOfQuestion7: IndexedOperationOfQuestion =
    indexedOperationOfQuestion(
      questionId = question7.questionId,
      operationId = operation6.operationId,
      startDate = now.minusDays(8),
      endDate = now.minusDays(1),
      status = OperationOfQuestion.Status.Finished,
      resultsLink = Some("https://example.com/results"),
      featured = false
    )
  val operationOfQuestion8: IndexedOperationOfQuestion =
    indexedOperationOfQuestion(
      questionId = question8.questionId,
      operationId = operation6.operationId,
      startDate = now.minusDays(8),
      endDate = now.minusDays(2),
      status = OperationOfQuestion.Status.Finished,
      featured = false
    )

  val operations: Seq[SimpleOperation] = Seq(operation1, operation2, operation3, operation4, operation5, operation6)
  val questions: Seq[Question] = Seq(question1, question2, question3, question4, question5, question6)
  val operationOfQuestions: OperationOfQuestionSearchResult = OperationOfQuestionSearchResult(
    total = 6L,
    results = Seq(
      operationOfQuestion1,
      operationOfQuestion2,
      operationOfQuestion3,
      operationOfQuestion4,
      operationOfQuestion5,
      operationOfQuestion6,
      operationOfQuestion7,
      operationOfQuestion8
    )
  )

  Feature("home view") {
    Scenario("home-page view") {

      when(
        userService.adminCountUsers(
          any[Option[Seq[UserId]]],
          any[Option[String]],
          any[Option[String]],
          any[Option[String]],
          any[Option[Role]],
          eqTo(Some(UserType.UserTypeOrganisation))
        )
      ).thenReturn(Future.successful(9001))

      when(elasticsearchOperationOfQuestionAPI.highlights())
        .thenReturn(Future.successful(Highlights(42, 84, 0)))

      when(
        elasticsearchOperationOfQuestionAPI
          .searchOperationOfQuestions(
            argThat(
              (arg: OperationOfQuestionSearchQuery) =>
                arg match {
                  case OperationOfQuestionSearchQuery(
                      Some(
                        OperationOfQuestionSearchFilters(
                          _,
                          _,
                          _,
                          _,
                          _,
                          _,
                          _,
                          _,
                          _,
                          Some(FeaturedSearchFilter(true)),
                          _,
                          _
                        )
                      ),
                      _,
                      _,
                      _,
                      _,
                      _
                      ) =>
                    true
                  case _ => false
                }
            )
          )
      ).thenReturn(Future.successful {
        val results = operationOfQuestions.results.filter(_.featured).sortBy(_.endDate).reverse
        OperationOfQuestionSearchResult(results.size, results)
      })

      when(
        elasticsearchOperationOfQuestionAPI
          .searchOperationOfQuestions(
            argThat(
              (arg: OperationOfQuestionSearchQuery) =>
                arg match {
                  case OperationOfQuestionSearchQuery(
                      Some(
                        OperationOfQuestionSearchFilters(_, _, _, _, _, _, _, _, _, _, Some(StatusSearchFilter(_)), _)
                      ),
                      _,
                      _,
                      _,
                      _,
                      _
                      ) =>
                    true
                  case _ => false
                }
            )
          )
      ).thenReturn(Future.successful {
        val results = operationOfQuestions.results
          .filter(_.status == OperationOfQuestion.Status.Open)
          .sortBy(e => (e.featured, e.endDate))
          .reverse
        OperationOfQuestionSearchResult(results.size, results)
      })

      when(
        elasticsearchOperationOfQuestionAPI
          .searchOperationOfQuestions(
            argThat((arg: OperationOfQuestionSearchQuery) => arg.filters.flatMap(_.hasResults).isDefined)
          )
      ).thenReturn(Future.successful {
        val results = operationOfQuestions.results
          .filter(_.status == OperationOfQuestion.Status.Finished)
          .filter(_.resultsLink.isDefined)
          .sortBy(_.endDate)
          .reverse
          .take(4)
        OperationOfQuestionSearchResult(results.size, results)
      })

      val indexedPosts: Seq[IndexedPost] =
        Seq(indexedPost(PostId("post-id-1")), indexedPost(PostId("post-id-2")))
      val postsResponses: Seq[HomePageViewResponse.PostResponse] =
        indexedPosts.map(HomePageViewResponse.PostResponse.fromIndexedPost)

      when(
        postService.search(
          eqTo(
            PostSearchQuery(
              filters = Some(
                PostSearchFilters(
                  displayHome = Some(DisplayHomeSearchFilter(true)),
                  country = Some(PostCountryFilter(Country("FR")))
                )
              ),
              sort = Some(PostElasticsearchFieldNames.postDate),
              order = Some(Order.desc),
              limit = Some(Pagination.Limit(3))
            )
          )
        )
      ).thenReturn(Future.successful(PostSearchResult(2, indexedPosts)))

      val futureHomePageViewResponse: Future[HomePageViewResponse] =
        homeViewService.getHomePageViewResponse(Country("FR"), Some(Language("fr")))
      whenReady(futureHomePageViewResponse, Timeout(3.seconds)) { homePageViewResponse =>
        homePageViewResponse.posts should be(postsResponses)
        homePageViewResponse.currentQuestions should be(
          Seq(operationOfQuestion3, operationOfQuestion2)
            .map(QuestionOfOperationResponse.apply(_, Some(Language("fr"))))
        )
        homePageViewResponse.pastQuestions should be(
          Seq(operationOfQuestion7).map(QuestionOfOperationResponse.apply(_, Some(Language("fr"))))
        )
        homePageViewResponse.featuredQuestions should be(
          Seq(operationOfQuestion3, operationOfQuestion1)
            .map(QuestionOfOperationResponse.apply(_, Some(Language("fr"))))
        )
        homePageViewResponse.highlights should be(Highlights(42, 84, 9001))
      }

    }
  }
}
