include:
  - project: 'makeorg/infrastructure/pipeline/templates'
    file: 'pipelines/scala.yml'

# Variable
variables:
  IMAGE_NAME: $NEXUS_URL/make-api
  CI_BUILD: "true"

# Stage
stages:
  - check
  - build
  - deploy
  - reindex
  - non-regression
  - merge

# Workflow
workflow:
  rules:
    - !reference [.rules, on-merge-request]
    - !reference [.rules, on-protected-branch]

# Template
.with-non-regression-job:
  before_script:
    - export RUNDECK_JOB_ID=$RUNDECK_NON_REGRESSION_JOB_ID

.with-reindex-job:
  before_script:
    - export RUNDECK_JOB_ID=$RUNDECK_REINDEX_JOB_ID

# Check
check:
  extends:
  - .check
  - .with-coverage-artifact
  image: makeorg/sbt-builder:2.13.14
  tags:
    - large
  coverage: '/Statement coverage[A-Za-z\.*]\s*:\s*([^%]+)/'
  before_script:
    - mkdir -p /var/lib/docker
    - mount -t tmpfs -o size=12G tmpfs /var/lib/docker
    - if [ ! -e /var/run/docker.sock ]; then DOCKER_DRIVER=overlay2 dockerd & fi
    - until docker ps; do echo "waiting for docker to be up..."; sleep 0.5; done
  script:
    - sbt ';clean;coverage;test;it:test;coverageAggregate' --batch --debug
    - cp -r target/scala-*/scoverage-report/ coverage
    - cp target/scala-*/coverage-report/cobertura.xml coverage/.

# Build
build:
  image: makeorg/sbt-builder:2.13.8
  extends: .build

coverage:
  extends: .coverage

pages:
  extends: .publish-coverage

# Deploy
deploy-preproduction:
  extends:
    - .deploy
    - .preprod

deploy-production:
  extends:
    - .deploy
    - .prod

# Non-regression tests
non-regression-tests:
  stage: non-regression
  extends:
    - .run
    - .preprod
    - .with-non-regression-job

# Reindex
reindex-preproduction:
  stage: reindex
  extends:
  - .run
  - .preprod
  - .with-reindex-job

reindex-production:
  stage: reindex
  extends:
  - .run
  - .prod
  - .with-reindex-job

# Merge
merge-to-prod:
  extends: .merge
